﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m27778AFEA29EBFB2F93D7DAF5231562AB231A0A7 ();
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m17515D41590B32DBE35F8DB27EE7A707B8EC741F ();
// 0x00000003 System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern void ExtensionAttribute__ctor_mDFD2F429C7343734A87997E02A88CF0FAF105A43 ();
// 0x00000004 T[] Newtonsoft.Json.IArrayPool`1::Rent(System.Int32)
// 0x00000005 System.Void Newtonsoft.Json.IArrayPool`1::Return(T[])
// 0x00000006 System.Boolean Newtonsoft.Json.IJsonLineInfo::HasLineInfo()
// 0x00000007 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LineNumber()
// 0x00000008 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LinePosition()
// 0x00000009 System.Type Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterType()
extern void JsonContainerAttribute_get_ItemConverterType_mE247536B5BEC32B46087979E3F46FED189F595B8 ();
// 0x0000000A System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterParameters()
extern void JsonContainerAttribute_get_ItemConverterParameters_m2C959CD76DF456F824FE3667E8049CFDAFECBD53 ();
// 0x0000000B System.Type Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyType()
extern void JsonContainerAttribute_get_NamingStrategyType_m8062CFA021E0C0E2F6AD2B7C6E35B339A84D6BAE ();
// 0x0000000C System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyParameters()
extern void JsonContainerAttribute_get_NamingStrategyParameters_m0073A60B3047867D3E8A42A23E347E1852BF6BC6 ();
// 0x0000000D Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyInstance()
extern void JsonContainerAttribute_get_NamingStrategyInstance_mBFBF66995F1D203594C8E4DCA4A86283D6F6D2A2 ();
// 0x0000000E System.Void Newtonsoft.Json.JsonContainerAttribute::set_NamingStrategyInstance(Newtonsoft.Json.Serialization.NamingStrategy)
extern void JsonContainerAttribute_set_NamingStrategyInstance_m8A24903D4D3C02368A34C5F80788B2961144AFF5 ();
// 0x0000000F Newtonsoft.Json.Serialization.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::get_DefaultSettings()
extern void JsonConvert_get_DefaultSettings_m7A51B49A26CCAB79B2842D64AACA0A0F187B2A6B ();
// 0x00000010 System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern void JsonConvert_ToString_m85F40BA04EBCB76CAA8C7802BD2794AFCC5C2B75 ();
// 0x00000011 System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern void JsonConvert_ToString_m3E819C2B1AEB5FC21BF0B3BB667470D50F1C6108 ();
// 0x00000012 System.String Newtonsoft.Json.JsonConvert::ToString(System.Single,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_m0EFDE4B65CDDFD6A6976B15F23C7C470581F803C ();
// 0x00000013 System.String Newtonsoft.Json.JsonConvert::EnsureFloatFormat(System.Double,System.String,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_EnsureFloatFormat_m812AD27101DA26F048B62B38D33838BA328CC278 ();
// 0x00000014 System.String Newtonsoft.Json.JsonConvert::ToString(System.Double,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_m943E2BF8B9545B98B0E595DA6917F9ECABC7A905 ();
// 0x00000015 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern void JsonConvert_EnsureDecimalPlace_mFFACDA0550E29FA22B4EA3C397BE0D489098845E ();
// 0x00000016 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern void JsonConvert_EnsureDecimalPlace_m17FAC0CA5604012B71ACFF26841873C0413057A0 ();
// 0x00000017 System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern void JsonConvert_ToString_m0E19BB1A182B366F36C84A4A0BAED73BA8434728 ();
// 0x00000018 System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern void JsonConvert_ToString_m2A288CD49DF4E4523E5691E1528F00246EB572D2 ();
// 0x00000019 System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern void JsonConvert_ToString_m05ED1765E01EF5867E0CA77B0C0D1B502A477B7D ();
// 0x0000001A System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char,Newtonsoft.Json.StringEscapeHandling)
extern void JsonConvert_ToString_m8D0C39C163082C374EB859D21966C63394FF63F6 ();
// 0x0000001B T Newtonsoft.Json.JsonConvert::DeserializeObject(System.String)
// 0x0000001C T Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,Newtonsoft.Json.JsonSerializerSettings)
// 0x0000001D System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_DeserializeObject_m9602AE19A4B7F9174F8806B512BC6F8587012AAD ();
// 0x0000001E System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern void JsonConvert__cctor_m11E6C468A0C459067679F97598B8838BEFC4CBF2 ();
// 0x0000001F System.Void Newtonsoft.Json.JsonConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000020 System.Object Newtonsoft.Json.JsonConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000021 System.Boolean Newtonsoft.Json.JsonConverter::CanConvert(System.Type)
// 0x00000022 System.Boolean Newtonsoft.Json.JsonConverter::get_CanRead()
extern void JsonConverter_get_CanRead_mC164472AB2A3D8CF999F6AB08763F3A5E956773B ();
// 0x00000023 System.Boolean Newtonsoft.Json.JsonConverter::get_CanWrite()
extern void JsonConverter_get_CanWrite_m47CB9438FEF36B3ABBCC9F5C21AE1BDB368ED4DC ();
// 0x00000024 System.Void Newtonsoft.Json.JsonConverter::.ctor()
extern void JsonConverter__ctor_m6BE8348C559C7CF2F1B5DBEC1A5EC1D64ED07FEA ();
// 0x00000025 System.Type Newtonsoft.Json.JsonConverterAttribute::get_ConverterType()
extern void JsonConverterAttribute_get_ConverterType_m87171B070A7AD07E273B6B6528833B93C3C5C28F ();
// 0x00000026 System.Object[] Newtonsoft.Json.JsonConverterAttribute::get_ConverterParameters()
extern void JsonConverterAttribute_get_ConverterParameters_mA5C42F619005153B6F11E36D0B35FDA90CCCB382 ();
// 0x00000027 System.Void Newtonsoft.Json.JsonConverterCollection::.ctor()
extern void JsonConverterCollection__ctor_mEDBE661FFC67E24DFBC0552B8A5F012C69AA8447 ();
// 0x00000028 System.Void Newtonsoft.Json.JsonException::.ctor()
extern void JsonException__ctor_m04617A66F6768F7EB0573832A44E6BD97EB236EB ();
// 0x00000029 System.Void Newtonsoft.Json.JsonException::.ctor(System.String)
extern void JsonException__ctor_mB8EC1AB459DCA248D08C000DC4D7B398E0F6F1A2 ();
// 0x0000002A System.Void Newtonsoft.Json.JsonException::.ctor(System.String,System.Exception)
extern void JsonException__ctor_mF445915497264D23BCBB6BAB8E8559333A51C6D0 ();
// 0x0000002B System.Void Newtonsoft.Json.JsonException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException__ctor_m07EC4A4F6A1E1460BCF94B0B264D713FC3B9CDFF ();
// 0x0000002C System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_WriteData()
extern void JsonExtensionDataAttribute_get_WriteData_m55EA3188DFD617C8BC6E3C351BF7153F639838F0 ();
// 0x0000002D System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_ReadData()
extern void JsonExtensionDataAttribute_get_ReadData_m2F77D189509DABD5490FF853B8E72E41D3319BF0 ();
// 0x0000002E Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::get_MemberSerialization()
extern void JsonObjectAttribute_get_MemberSerialization_m189BEA95020E8AF10964DC3F517C57D18C7108C0 ();
// 0x0000002F System.Void Newtonsoft.Json.JsonPosition::.ctor(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition__ctor_m26690EBA4C4BED257622AE08B7E3FA140DA88299_AdjustorThunk ();
// 0x00000030 System.Int32 Newtonsoft.Json.JsonPosition::CalculateLength()
extern void JsonPosition_CalculateLength_m921A1A2BA11CC900A695B0B7CE87FEE74C3B208B_AdjustorThunk ();
// 0x00000031 System.Void Newtonsoft.Json.JsonPosition::WriteTo(System.Text.StringBuilder,System.IO.StringWriter&,System.Char[]&)
extern void JsonPosition_WriteTo_mF191367903AA33ED43CAE4E7423CE97C67C628DD_AdjustorThunk ();
// 0x00000032 System.Boolean Newtonsoft.Json.JsonPosition::TypeHasIndex(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition_TypeHasIndex_mE4996BF80B64FE1672BE49DA7D4437B7E6C3BC47 ();
// 0x00000033 System.String Newtonsoft.Json.JsonPosition::BuildPath(System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>,System.Nullable`1<Newtonsoft.Json.JsonPosition>)
extern void JsonPosition_BuildPath_m26DAF8152ECFEFAEC0555F0BD7529DD8D670BF43 ();
// 0x00000034 System.String Newtonsoft.Json.JsonPosition::FormatMessage(Newtonsoft.Json.IJsonLineInfo,System.String,System.String)
extern void JsonPosition_FormatMessage_mA577A748D63EA8C15126615206685FE9333BF2A3 ();
// 0x00000035 System.Void Newtonsoft.Json.JsonPosition::.cctor()
extern void JsonPosition__cctor_m53C4B48E7ACF35D0FB697888F556D2F2E12E9A88 ();
// 0x00000036 System.Type Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterType()
extern void JsonPropertyAttribute_get_ItemConverterType_mFF23E0D4F0731DA4E2220CBD2454DE4865221D24 ();
// 0x00000037 System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterParameters()
extern void JsonPropertyAttribute_get_ItemConverterParameters_m9B11B09E89A231E9D8DB1B4B71CE0EE5785D6F35 ();
// 0x00000038 System.Type Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyType()
extern void JsonPropertyAttribute_get_NamingStrategyType_m5FF2DFA6BBC0869E3D6C66C9DCD2125AAE9E8BC9 ();
// 0x00000039 System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyParameters()
extern void JsonPropertyAttribute_get_NamingStrategyParameters_m0EFA60F0B795DB1623E73758F46A3A52EBE364F2 ();
// 0x0000003A System.String Newtonsoft.Json.JsonPropertyAttribute::get_PropertyName()
extern void JsonPropertyAttribute_get_PropertyName_m4A8575B86821B1B2252871C4ED6BA04B3BC8B566 ();
// 0x0000003B Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::get_CurrentState()
extern void JsonReader_get_CurrentState_m7137D79D19A88A8A0BB692E5632145B150F421C1 ();
// 0x0000003C System.Boolean Newtonsoft.Json.JsonReader::get_CloseInput()
extern void JsonReader_get_CloseInput_m54E593028B735ECDA8A3EA6CB8B78AB64C4101A6 ();
// 0x0000003D System.Void Newtonsoft.Json.JsonReader::set_CloseInput(System.Boolean)
extern void JsonReader_set_CloseInput_m529A2FC45821DD045671D3DF5DE2DFD46EBA8423 ();
// 0x0000003E System.Boolean Newtonsoft.Json.JsonReader::get_SupportMultipleContent()
extern void JsonReader_get_SupportMultipleContent_m1D2BBF3EC1E9A912385B5A804CFE42A13111FE40 ();
// 0x0000003F System.Void Newtonsoft.Json.JsonReader::set_SupportMultipleContent(System.Boolean)
extern void JsonReader_set_SupportMultipleContent_mDEF71D6622D5F7C4CF05915E11DAC682A4ADBC74 ();
// 0x00000040 Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::get_DateTimeZoneHandling()
extern void JsonReader_get_DateTimeZoneHandling_mAE60084628AA490FE0B4FCA92362F2B75FDAD77A ();
// 0x00000041 System.Void Newtonsoft.Json.JsonReader::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonReader_set_DateTimeZoneHandling_mCE5F53C4170A7C09A7E10FA369DDA2E5A26E20D0 ();
// 0x00000042 Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::get_DateParseHandling()
extern void JsonReader_get_DateParseHandling_mD96560D6DDEF9DB0E5F31364375634F521B5EDD4 ();
// 0x00000043 System.Void Newtonsoft.Json.JsonReader::set_DateParseHandling(Newtonsoft.Json.DateParseHandling)
extern void JsonReader_set_DateParseHandling_m3BDAD515C46CD0E0628DCE6F19F46CF72B21E0C8 ();
// 0x00000044 Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::get_FloatParseHandling()
extern void JsonReader_get_FloatParseHandling_m90A0D2DDFD585F12AAC37E5B8AEBEE37BD89E1FB ();
// 0x00000045 System.Void Newtonsoft.Json.JsonReader::set_FloatParseHandling(Newtonsoft.Json.FloatParseHandling)
extern void JsonReader_set_FloatParseHandling_m9F542DCC5F1DACBDE7C8CB7A967E3B125F3400C3 ();
// 0x00000046 System.String Newtonsoft.Json.JsonReader::get_DateFormatString()
extern void JsonReader_get_DateFormatString_m366C5819D59C177FDEEE86F0B7882C88F92688C6 ();
// 0x00000047 System.Void Newtonsoft.Json.JsonReader::set_DateFormatString(System.String)
extern void JsonReader_set_DateFormatString_m7DE5E6C831217CC8D1510DE35AF5A44B8CF0DA9F ();
// 0x00000048 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::get_MaxDepth()
extern void JsonReader_get_MaxDepth_m1FF61F2318B1B585359077611E0AE14D5BD36745 ();
// 0x00000049 System.Void Newtonsoft.Json.JsonReader::set_MaxDepth(System.Nullable`1<System.Int32>)
extern void JsonReader_set_MaxDepth_m704C66DBD74CB34326B665517FC615DD8EB09E8D ();
// 0x0000004A Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::get_TokenType()
extern void JsonReader_get_TokenType_m5C79518C15B34E8EA0477DB44A254BC08A965D7E ();
// 0x0000004B System.Object Newtonsoft.Json.JsonReader::get_Value()
extern void JsonReader_get_Value_m6F9E83E46D30C32A199DD6A66AFE64A481C87990 ();
// 0x0000004C System.Type Newtonsoft.Json.JsonReader::get_ValueType()
extern void JsonReader_get_ValueType_mF1653CD43A265AA9BCF1B2A1CDE3CF121863C592 ();
// 0x0000004D System.Int32 Newtonsoft.Json.JsonReader::get_Depth()
extern void JsonReader_get_Depth_mD92B3C30A95CF265C184E5FAC2416A0F58350AEA ();
// 0x0000004E System.String Newtonsoft.Json.JsonReader::get_Path()
extern void JsonReader_get_Path_m1CA607D98B8A79EF2B81EA5AA8BAC24A7261383F ();
// 0x0000004F System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::get_Culture()
extern void JsonReader_get_Culture_m5AFC9DFD8CDC6C5DA061D77ED9F6203B750C8940 ();
// 0x00000050 System.Void Newtonsoft.Json.JsonReader::set_Culture(System.Globalization.CultureInfo)
extern void JsonReader_set_Culture_mAF8782A53436254EE5C2F208001B96786C490AAC ();
// 0x00000051 Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::GetPosition(System.Int32)
extern void JsonReader_GetPosition_m1208A6618F915264EBD1BA8DE7C1B948F535E154 ();
// 0x00000052 System.Void Newtonsoft.Json.JsonReader::.ctor()
extern void JsonReader__ctor_m59A90ADA802F2EFF746A7DD483F4CA9A3183CA98 ();
// 0x00000053 System.Void Newtonsoft.Json.JsonReader::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonReader_Push_m625D9CE02FF4797FEC06F8C54352D346C9F6A34B ();
// 0x00000054 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Pop()
extern void JsonReader_Pop_m616115E094F08CB9C30D549B82434E203B7B7A44 ();
// 0x00000055 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Peek()
extern void JsonReader_Peek_mE56F27ECB561C6D968A2C1E1269A354A9FC7AA47 ();
// 0x00000056 System.Boolean Newtonsoft.Json.JsonReader::Read()
// 0x00000057 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadAsInt32()
extern void JsonReader_ReadAsInt32_m30435B2A03B75919709F6C403D3712A624EB23BE ();
// 0x00000058 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadInt32String(System.String)
extern void JsonReader_ReadInt32String_mB470677B0B7AA5E8E7AE1F5C679CC8737124A2B0 ();
// 0x00000059 System.String Newtonsoft.Json.JsonReader::ReadAsString()
extern void JsonReader_ReadAsString_m51465F67EF867AFADE155660F6F74FA61754A10F ();
// 0x0000005A System.Byte[] Newtonsoft.Json.JsonReader::ReadAsBytes()
extern void JsonReader_ReadAsBytes_m1D32146809343093FBD44557E38A8631BA224D9E ();
// 0x0000005B System.Byte[] Newtonsoft.Json.JsonReader::ReadArrayIntoByteArray()
extern void JsonReader_ReadArrayIntoByteArray_m7CB88A67D6F3C25F04226EDBFA2CAA44AB519109 ();
// 0x0000005C System.Boolean Newtonsoft.Json.JsonReader::ReadArrayElementIntoByteArrayReportDone(System.Collections.Generic.List`1<System.Byte>)
extern void JsonReader_ReadArrayElementIntoByteArrayReportDone_m5C3E9A1EF463D84C841A4907BE5C97905FAC552C ();
// 0x0000005D System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadAsDouble()
extern void JsonReader_ReadAsDouble_m83268BFC80A00441F323BA5061E3966B64E3F888 ();
// 0x0000005E System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadDoubleString(System.String)
extern void JsonReader_ReadDoubleString_m2D1A3F0C8BBD14167AA4FAC6778D4855C3600839 ();
// 0x0000005F System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadAsBoolean()
extern void JsonReader_ReadAsBoolean_mA10EBF792B907950634E3DEB01BB3B8DC508563A ();
// 0x00000060 System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadBooleanString(System.String)
extern void JsonReader_ReadBooleanString_m02ED9E94360662EB82270A89B3707128FE2F19E6 ();
// 0x00000061 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadAsDecimal()
extern void JsonReader_ReadAsDecimal_mE742BBAF80F128E6AA398A31AEC88D02B60C5200 ();
// 0x00000062 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadDecimalString(System.String)
extern void JsonReader_ReadDecimalString_m1BBE5C2F7B7A5EA1E805AB9ACD0726D234BED8CE ();
// 0x00000063 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadAsDateTime()
extern void JsonReader_ReadAsDateTime_m22FA3C0C7A0446FFC85F9E2C01458F59138B89C2 ();
// 0x00000064 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadDateTimeString(System.String)
extern void JsonReader_ReadDateTimeString_mB325EABD3F79C841702FCE7D71ECE3F436D72373 ();
// 0x00000065 System.Void Newtonsoft.Json.JsonReader::ReaderReadAndAssert()
extern void JsonReader_ReaderReadAndAssert_m8DD8A00BA33CC38E5A35E591EB6154D076FFF28D ();
// 0x00000066 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReader::CreateUnexpectedEndException()
extern void JsonReader_CreateUnexpectedEndException_mE4921EC6BB31F24E9AEB31BAAC482A12691692D2 ();
// 0x00000067 System.Void Newtonsoft.Json.JsonReader::ReadIntoWrappedTypeObject()
extern void JsonReader_ReadIntoWrappedTypeObject_m318F9BCA007EE91D36504262CCC11DFCE4A88502 ();
// 0x00000068 System.Void Newtonsoft.Json.JsonReader::Skip()
extern void JsonReader_Skip_mDCD4C526C7585E9770A7BC743CA5D343E5F3F08B ();
// 0x00000069 System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_SetToken_m5F5EF6634DCA35E6716067519E61BBDE796F70CC ();
// 0x0000006A System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonReader_SetToken_mCC04B1E8B024780C4F694583AF108E47E551029B ();
// 0x0000006B System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object,System.Boolean)
extern void JsonReader_SetToken_m248478B3F3CDD09A4C130FB76AF0B39346ED4820 ();
// 0x0000006C System.Void Newtonsoft.Json.JsonReader::SetPostValueState(System.Boolean)
extern void JsonReader_SetPostValueState_mA97D816A4790946B281DB4F80990673553493367 ();
// 0x0000006D System.Void Newtonsoft.Json.JsonReader::UpdateScopeWithFinishedValue()
extern void JsonReader_UpdateScopeWithFinishedValue_m8A3473E4CE3F41A3516BC5AF352D5FC68F269D4B ();
// 0x0000006E System.Void Newtonsoft.Json.JsonReader::ValidateEnd(Newtonsoft.Json.JsonToken)
extern void JsonReader_ValidateEnd_mA795A30C6B3DCE82DF05588A348B613BE456B415 ();
// 0x0000006F System.Void Newtonsoft.Json.JsonReader::SetStateBasedOnCurrent()
extern void JsonReader_SetStateBasedOnCurrent_m11B66BF4989AE85BB078446B73DF6ED5EC4E08EC ();
// 0x00000070 System.Void Newtonsoft.Json.JsonReader::SetFinished()
extern void JsonReader_SetFinished_mB806E429B0BF925682207E3CB68B1072706BB8F3 ();
// 0x00000071 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::GetTypeForCloseToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_GetTypeForCloseToken_m84355671714B89B9C097B65DA1C984FB730DA882 ();
// 0x00000072 System.Void Newtonsoft.Json.JsonReader::System.IDisposable.Dispose()
extern void JsonReader_System_IDisposable_Dispose_mEB1D0200A57984B87AED7CF72E8F82B5062FC7B1 ();
// 0x00000073 System.Void Newtonsoft.Json.JsonReader::Dispose(System.Boolean)
extern void JsonReader_Dispose_mD2989BD191EABEDCC1148B25D4DE3ACB353205E4 ();
// 0x00000074 System.Void Newtonsoft.Json.JsonReader::Close()
extern void JsonReader_Close_mD880192B68AD39E14C2BCC8664D29EFF24354508 ();
// 0x00000075 System.Void Newtonsoft.Json.JsonReader::ReadAndAssert()
extern void JsonReader_ReadAndAssert_mA886FCD64978D1151939E4E92731D067084DA245 ();
// 0x00000076 System.Void Newtonsoft.Json.JsonReader::ReadForTypeAndAssert(Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern void JsonReader_ReadForTypeAndAssert_m557EFF224CFBD0E3D488551FB09EDE35CEE227E1 ();
// 0x00000077 System.Boolean Newtonsoft.Json.JsonReader::ReadForType(Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern void JsonReader_ReadForType_mAA23254DBB10D0337A5C943D11DB959757C11150 ();
// 0x00000078 System.Boolean Newtonsoft.Json.JsonReader::ReadAndMoveToContent()
extern void JsonReader_ReadAndMoveToContent_m69F5B676251E94F06C5132A912A7AFB8EDB9FBD6 ();
// 0x00000079 System.Boolean Newtonsoft.Json.JsonReader::MoveToContent()
extern void JsonReader_MoveToContent_m5428A5678ADAFF27A0E33F6926E155646A5D0A7F ();
// 0x0000007A Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::GetContentToken()
extern void JsonReader_GetContentToken_m6941DE6621388E2DFF0C28E307480453678EEEEB ();
// 0x0000007B System.Void Newtonsoft.Json.JsonReaderException::.ctor()
extern void JsonReaderException__ctor_m107001396704CF5FBC499BA1D194B6877C0E41FC ();
// 0x0000007C System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonReaderException__ctor_mD095417DE2CCEEF32741D8047DF5A4E53C6CD9BD ();
// 0x0000007D System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.String,System.Int32,System.Int32,System.Exception)
extern void JsonReaderException__ctor_mF555EF160344C3E8705C34ABD274DC3AD8E6C0B1 ();
// 0x0000007E Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonReaderException_Create_m542C24E20D0DC9BBAC727B7DC80370C07866BFB9 ();
// 0x0000007F Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonReaderException_Create_mFCA48801BFC5C07D621439B7ADF501B97D028A88 ();
// 0x00000080 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonReaderException_Create_m758736D31A286C8F7DCC9F167DC15B65AC1E62D1 ();
// 0x00000081 System.Void Newtonsoft.Json.JsonSerializationException::.ctor()
extern void JsonSerializationException__ctor_m7C7629D1D0ACA3C627B2C0955D294D5CDBCC923F ();
// 0x00000082 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String)
extern void JsonSerializationException__ctor_m1528BD4200A330AD611F936EAA8F85C6014F084E ();
// 0x00000083 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String,System.Exception)
extern void JsonSerializationException__ctor_m2004EB22E7C185FB64905773BE3D65AE5A8CE914 ();
// 0x00000084 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonSerializationException__ctor_m0CD549EE0A76E02AA44B70253324F5E19213B96F ();
// 0x00000085 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializationException_Create_m4242A77595098B9E7687291CB3BFF96BD2249AC7 ();
// 0x00000086 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonSerializationException_Create_mC6637C74A9EA6C2527F5971694AAC40671C302B6 ();
// 0x00000087 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonSerializationException_Create_m0EBB0905A5B2993F6ACF9AEA8D7535ECD1D6981C ();
// 0x00000088 System.Void Newtonsoft.Json.JsonSerializer::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_add_Error_m4C1E51CD3049D684A8DFC014EB07C645F3A73EA0 ();
// 0x00000089 System.Void Newtonsoft.Json.JsonSerializer::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_remove_Error_mA3D7CB2D7BB623F79D8D66B905BD9C6D2E4E791E ();
// 0x0000008A System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializer_set_ReferenceResolver_m9C44638869949098A898108C468CB92ECC9E591D ();
// 0x0000008B System.Void Newtonsoft.Json.JsonSerializer::set_SerializationBinder(Newtonsoft.Json.Serialization.ISerializationBinder)
extern void JsonSerializer_set_SerializationBinder_m799247A598583D710FCE9250225839218B43069A ();
// 0x0000008C Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::get_TraceWriter()
extern void JsonSerializer_get_TraceWriter_m252481C00E908F419AD49ABC148583D81E2DCD7C ();
// 0x0000008D System.Void Newtonsoft.Json.JsonSerializer::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializer_set_TraceWriter_m99646D2DC5C7E49CA2969BEF5A486CA04A2BE62D ();
// 0x0000008E System.Void Newtonsoft.Json.JsonSerializer::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializer_set_EqualityComparer_m2529D197EF0628A78D26896FDCA79B32EDB4A9C0 ();
// 0x0000008F System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializer_set_TypeNameHandling_mA485C2838EE7A8A5F62F98B25E345A28B8EBF42C ();
// 0x00000090 System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameAssemblyFormatHandling(Newtonsoft.Json.TypeNameAssemblyFormatHandling)
extern void JsonSerializer_set_TypeNameAssemblyFormatHandling_mFD586B0CBC504194A5676BC66B4DD9F64400BF58 ();
// 0x00000091 System.Void Newtonsoft.Json.JsonSerializer::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializer_set_PreserveReferencesHandling_m18165AAA840BFFD9DDFB1236BFBBA91B7059013F ();
// 0x00000092 System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializer_set_ReferenceLoopHandling_m1F02FBB0C85842D5ECA0DCBCCDDE40DF3005CFE5 ();
// 0x00000093 System.Void Newtonsoft.Json.JsonSerializer::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializer_set_MissingMemberHandling_m9F0C807503161CF6E56EE00EB5BC752EA7A79780 ();
// 0x00000094 Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::get_NullValueHandling()
extern void JsonSerializer_get_NullValueHandling_mF5D54BAE0A41E747D189FE1D29B5324A148807D6 ();
// 0x00000095 System.Void Newtonsoft.Json.JsonSerializer::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializer_set_NullValueHandling_m756283732ECF267EE17F62B796EC8DFAC9E3D5EA ();
// 0x00000096 System.Void Newtonsoft.Json.JsonSerializer::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializer_set_DefaultValueHandling_m4E4DDD012B764BA8508BBDC59CA3DF1950B93C72 ();
// 0x00000097 System.Void Newtonsoft.Json.JsonSerializer::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializer_set_ObjectCreationHandling_m7241DE5D3F047FF01E71558AD1C93F4B5B942DF6 ();
// 0x00000098 System.Void Newtonsoft.Json.JsonSerializer::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializer_set_ConstructorHandling_m84FE6147D6CB78C9E577AF9A20D72AABDDD6FA21 ();
// 0x00000099 Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::get_MetadataPropertyHandling()
extern void JsonSerializer_get_MetadataPropertyHandling_m9B7DA33D364F04F505137464A3D357F1A8BE6C36 ();
// 0x0000009A System.Void Newtonsoft.Json.JsonSerializer::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializer_set_MetadataPropertyHandling_mFADA3DF2590CE9903A3AF650F7551A0DD57D78DB ();
// 0x0000009B Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::get_Converters()
extern void JsonSerializer_get_Converters_m42E233DF91EB26C264DF4F59695DA967C5D6FC40 ();
// 0x0000009C Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::get_ContractResolver()
extern void JsonSerializer_get_ContractResolver_mC697373564D447BC54DCAB5CFD9E7E4BBC2C5C0C ();
// 0x0000009D System.Void Newtonsoft.Json.JsonSerializer::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializer_set_ContractResolver_m133F62A635EF77449897DB0E807B67687B72592A ();
// 0x0000009E System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::get_Context()
extern void JsonSerializer_get_Context_mC39BC437E1C60E44F1653793240F69529A7D125A ();
// 0x0000009F System.Void Newtonsoft.Json.JsonSerializer::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializer_set_Context_m4E23EB3BA8F7D8DB39E158A86ADC73B552F86B67 ();
// 0x000000A0 System.Boolean Newtonsoft.Json.JsonSerializer::get_CheckAdditionalContent()
extern void JsonSerializer_get_CheckAdditionalContent_m400A6A99C0B24E39E1399333CD2DFD3001F21201 ();
// 0x000000A1 System.Void Newtonsoft.Json.JsonSerializer::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializer_set_CheckAdditionalContent_m26695B93ADF9337A6411567D4F920BDD94FA4159 ();
// 0x000000A2 System.Boolean Newtonsoft.Json.JsonSerializer::IsCheckAdditionalContentSet()
extern void JsonSerializer_IsCheckAdditionalContentSet_m17A7688B3A6BEB62518563B64EB90652920F3321 ();
// 0x000000A3 System.Void Newtonsoft.Json.JsonSerializer::.ctor()
extern void JsonSerializer__ctor_m4672AF4CB21A63D3C972037EC9F961D1242A0DAF ();
// 0x000000A4 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create()
extern void JsonSerializer_Create_m96314D601FD9AE91A028AF7A124ECE4BAEC043F7 ();
// 0x000000A5 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_Create_m88DFAF350F847F16FD4DF5AE26A7FBD3ACED81E9 ();
// 0x000000A6 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault()
extern void JsonSerializer_CreateDefault_m8EDABDD16C11669D570E4770825D6A45B602AC3C ();
// 0x000000A7 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_CreateDefault_m8E8A552229E1FC82C3CB5C2B6D6EB0EA9819F48A ();
// 0x000000A8 System.Void Newtonsoft.Json.JsonSerializer::ApplySerializerSettings(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_ApplySerializerSettings_mB4CDAF7E955A016F12E649B90F24018A24AD729A ();
// 0x000000A9 T Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader)
// 0x000000AA System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_Deserialize_m878EA724D695FEF23CB17BD1CF936B5525AEFAB8 ();
// 0x000000AB System.Object Newtonsoft.Json.JsonSerializer::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_DeserializeInternal_mED9A9B045D22C3F6A562E3111311FEF41FE13F5C ();
// 0x000000AC System.Void Newtonsoft.Json.JsonSerializer::SetupReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo&,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>&,System.Nullable`1<Newtonsoft.Json.DateParseHandling>&,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>&,System.Nullable`1<System.Int32>&,System.String&)
extern void JsonSerializer_SetupReader_mC5D4862F24009A92E7ACDD270AA27E27DCE5924E ();
// 0x000000AD System.Void Newtonsoft.Json.JsonSerializer::ResetReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>,System.Nullable`1<Newtonsoft.Json.DateParseHandling>,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>,System.Nullable`1<System.Int32>,System.String)
extern void JsonSerializer_ResetReader_m223B0B5AE2C4008BA0959DF8D2531C0508988345 ();
// 0x000000AE System.Void Newtonsoft.Json.JsonSerializer::Serialize(System.IO.TextWriter,System.Object)
extern void JsonSerializer_Serialize_mBCD908364A9781189431063E2244C6550D958D61 ();
// 0x000000AF System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_Serialize_mE85324845C96E090A70AF7E694DFC6F23EB76C1F ();
// 0x000000B0 System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializer_Serialize_mCD7C1A951F04FE743A705EF75605BBAEDFFFB924 ();
// 0x000000B1 Newtonsoft.Json.Serialization.TraceJsonReader Newtonsoft.Json.JsonSerializer::CreateTraceJsonReader(Newtonsoft.Json.JsonReader)
extern void JsonSerializer_CreateTraceJsonReader_mE43FCABF651C601A28EE652C206DC03AADC629D2 ();
// 0x000000B2 System.Void Newtonsoft.Json.JsonSerializer::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_SerializeInternal_mBC5C380C16B6A80C6370EE1FF281711B6DF27F1B ();
// 0x000000B3 Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::GetReferenceResolver()
extern void JsonSerializer_GetReferenceResolver_m3F61A527E858F13FC216EE1478E16B249D612E66 ();
// 0x000000B4 Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Type)
extern void JsonSerializer_GetMatchingConverter_mFCF1E2F5CBF9DBEAE9FB855DA2D75E3360E4D80B ();
// 0x000000B5 Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern void JsonSerializer_GetMatchingConverter_mBD8F2C669C9EC1DAEC036A33B0939D1A4EFF52F9 ();
// 0x000000B6 System.Void Newtonsoft.Json.JsonSerializer::OnError(Newtonsoft.Json.Serialization.ErrorEventArgs)
extern void JsonSerializer_OnError_m62D77DC78C986C7A32ED7D90E75AF367301502ED ();
// 0x000000B7 Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern void JsonSerializerSettings_get_ReferenceLoopHandling_mE90131C366549F5BBBAD1AEDE36DAA0BAD36B104 ();
// 0x000000B8 Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern void JsonSerializerSettings_get_MissingMemberHandling_m8A3F395640F090D05033DFCBB23DC79DCEB8506B ();
// 0x000000B9 Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern void JsonSerializerSettings_get_ObjectCreationHandling_mB6921D82F2C6332399521EB1057352F20E6F96D0 ();
// 0x000000BA Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern void JsonSerializerSettings_get_NullValueHandling_mC18A872C75FBF2C09D2D9CC56E89E1993BEC872F ();
// 0x000000BB Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern void JsonSerializerSettings_get_DefaultValueHandling_m343E75B4FAD9F44D98B92DC31E613353559C7DC9 ();
// 0x000000BC System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern void JsonSerializerSettings_get_Converters_m14000B0C3DFBE0870A478DB0F678051E5A7AD033 ();
// 0x000000BD Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern void JsonSerializerSettings_get_PreserveReferencesHandling_mD53D2909FE7D9E3731CA95D694152388A8CD0438 ();
// 0x000000BE Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern void JsonSerializerSettings_get_TypeNameHandling_m2851204FA3BBCF7F62124FD872403A6DABCCC14C ();
// 0x000000BF Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializerSettings::get_MetadataPropertyHandling()
extern void JsonSerializerSettings_get_MetadataPropertyHandling_mCE8C028F265D3778FAF94AC0EAF5DCB1410BE9F9 ();
// 0x000000C0 Newtonsoft.Json.TypeNameAssemblyFormatHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormatHandling()
extern void JsonSerializerSettings_get_TypeNameAssemblyFormatHandling_mC8836F149AA061CE1F2909B73D68600D85A31ECA ();
// 0x000000C1 Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern void JsonSerializerSettings_get_ConstructorHandling_m8B8571BD9474ACB01581705244C173C3CA69ED5E ();
// 0x000000C2 Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern void JsonSerializerSettings_get_ContractResolver_m22DEFBEC3CF3363020E57D703E4DA1D69975F7DF ();
// 0x000000C3 System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::get_EqualityComparer()
extern void JsonSerializerSettings_get_EqualityComparer_m8045CC9A277D7E5226DB2B51C923F7BF44DE58B0 ();
// 0x000000C4 Newtonsoft.Json.Serialization.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolverProvider()
extern void JsonSerializerSettings_get_ReferenceResolverProvider_mA50EA59821E514DE8568035E320E0CD130AC383A ();
// 0x000000C5 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::get_TraceWriter()
extern void JsonSerializerSettings_get_TraceWriter_m05A1F310CEC7BD2B071CE9E4E5BA3EFB8797669B ();
// 0x000000C6 Newtonsoft.Json.Serialization.ISerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_SerializationBinder()
extern void JsonSerializerSettings_get_SerializationBinder_m862F8E0DD9619EFAAD5A65761A50928DE3851D1A ();
// 0x000000C7 System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern void JsonSerializerSettings_get_Error_m73767217D978A8BA0146618E99DA60A2C71B4EE2 ();
// 0x000000C8 System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern void JsonSerializerSettings_get_Context_m4FEFE07F9642722F31D58FBDF27A26CCB033A4D0 ();
// 0x000000C9 System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern void JsonSerializerSettings__cctor_mA71EC3E06741A2AB3A9F55053C34B10C8A711792 ();
// 0x000000CA System.Void Newtonsoft.Json.JsonTextReader::.ctor(System.IO.TextReader)
extern void JsonTextReader__ctor_m4D88467341528476FF90C1568A3D97EB975CC64F ();
// 0x000000CB System.Void Newtonsoft.Json.JsonTextReader::EnsureBufferNotEmpty()
extern void JsonTextReader_EnsureBufferNotEmpty_m54B4E0FFEBFA8327B5632F6D64E8B324A6CCF450 ();
// 0x000000CC System.Void Newtonsoft.Json.JsonTextReader::SetNewLine(System.Boolean)
extern void JsonTextReader_SetNewLine_m7203F86741F5805DA318D24EB612724C797C0AB5 ();
// 0x000000CD System.Void Newtonsoft.Json.JsonTextReader::OnNewLine(System.Int32)
extern void JsonTextReader_OnNewLine_m7EFA0ACCA08A95509D53140AEE047139F1302C7F ();
// 0x000000CE System.Void Newtonsoft.Json.JsonTextReader::ParseString(System.Char,Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseString_m43BC25BD4AF552BC2C36CEDB5827DE4490DAB979 ();
// 0x000000CF System.Void Newtonsoft.Json.JsonTextReader::ParseReadString(System.Char,Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseReadString_mB4A8084EB35794F7DE92621BB52173B369329151 ();
// 0x000000D0 System.Void Newtonsoft.Json.JsonTextReader::BlockCopyChars(System.Char[],System.Int32,System.Char[],System.Int32,System.Int32)
extern void JsonTextReader_BlockCopyChars_m9839762FBEEDF7B76D996744877DF2440CFA4069 ();
// 0x000000D1 System.Void Newtonsoft.Json.JsonTextReader::ShiftBufferIfNeeded()
extern void JsonTextReader_ShiftBufferIfNeeded_m06CB7B34C1D1077566E07D58A63E457C267000D1 ();
// 0x000000D2 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean)
extern void JsonTextReader_ReadData_m977DAED45CE58AB40DD8F72C097AF2A62071CEE2 ();
// 0x000000D3 System.Void Newtonsoft.Json.JsonTextReader::PrepareBufferForReadData(System.Boolean,System.Int32)
extern void JsonTextReader_PrepareBufferForReadData_m058FF10DB9EFDAE275E386AD8149316109E8778C ();
// 0x000000D4 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean,System.Int32)
extern void JsonTextReader_ReadData_m412C6F0F22279EF7B3DCF4FAC1653285C9AC5FFB ();
// 0x000000D5 System.Boolean Newtonsoft.Json.JsonTextReader::EnsureChars(System.Int32,System.Boolean)
extern void JsonTextReader_EnsureChars_mD8C8A52585F1D31237A1CB4428A824911818A436 ();
// 0x000000D6 System.Boolean Newtonsoft.Json.JsonTextReader::ReadChars(System.Int32,System.Boolean)
extern void JsonTextReader_ReadChars_m1141C4FEB25E8737EEB8C9F5D8630B2D88D7A394 ();
// 0x000000D7 System.Boolean Newtonsoft.Json.JsonTextReader::Read()
extern void JsonTextReader_Read_m080ED282D233E7ED81438BC6A5F65719CA5FDBF5 ();
// 0x000000D8 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonTextReader::ReadAsInt32()
extern void JsonTextReader_ReadAsInt32_m1B3CAE80968C62B56936F4AEDCBB680791710D23 ();
// 0x000000D9 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonTextReader::ReadAsDateTime()
extern void JsonTextReader_ReadAsDateTime_m053B184CB8275C71869C338F57769278ED316B45 ();
// 0x000000DA System.String Newtonsoft.Json.JsonTextReader::ReadAsString()
extern void JsonTextReader_ReadAsString_m20AAE0EC8E2BF25F5890165F0E1FA83FF8DA0A92 ();
// 0x000000DB System.Byte[] Newtonsoft.Json.JsonTextReader::ReadAsBytes()
extern void JsonTextReader_ReadAsBytes_m90A5243866287CED1D8F8F80D48E2DE884C4B417 ();
// 0x000000DC System.Object Newtonsoft.Json.JsonTextReader::ReadStringValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadStringValue_m75E13102F75E2FE147B8A182067BAFFEFA5F4C9B ();
// 0x000000DD System.Object Newtonsoft.Json.JsonTextReader::FinishReadQuotedStringValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_FinishReadQuotedStringValue_m76C893F7E2FBD095BBCD560E208885D4014F427A ();
// 0x000000DE Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::CreateUnexpectedCharacterException(System.Char)
extern void JsonTextReader_CreateUnexpectedCharacterException_m1C9768E827FD0384DA5F37F2E83796409909D572 ();
// 0x000000DF System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonTextReader::ReadAsBoolean()
extern void JsonTextReader_ReadAsBoolean_m480742572E0AE3C94ACE3B0815D650B3C0FEE765 ();
// 0x000000E0 System.Void Newtonsoft.Json.JsonTextReader::ProcessValueComma()
extern void JsonTextReader_ProcessValueComma_mE0791D6EE713682643050D1B20E47D54E37F55FF ();
// 0x000000E1 System.Object Newtonsoft.Json.JsonTextReader::ReadNumberValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadNumberValue_mD0AAE37DC1C36D7D1DAB52DBA117FEAE345A0DD0 ();
// 0x000000E2 System.Object Newtonsoft.Json.JsonTextReader::FinishReadQuotedNumber(Newtonsoft.Json.ReadType)
extern void JsonTextReader_FinishReadQuotedNumber_m0BFF9FB4F493BDFEA21C95C6A3FF73004857B433 ();
// 0x000000E3 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonTextReader::ReadAsDecimal()
extern void JsonTextReader_ReadAsDecimal_m39EAEF985CFCD5358EFB1E5E2376B2068813E798 ();
// 0x000000E4 System.Nullable`1<System.Double> Newtonsoft.Json.JsonTextReader::ReadAsDouble()
extern void JsonTextReader_ReadAsDouble_m42B3D6EDC5AE3208F89078349E6CFEE1126D3017 ();
// 0x000000E5 System.Void Newtonsoft.Json.JsonTextReader::HandleNull()
extern void JsonTextReader_HandleNull_m3183FB4988AB7B5162A32464F0813C6D7DD05B1B ();
// 0x000000E6 System.Void Newtonsoft.Json.JsonTextReader::ReadFinished()
extern void JsonTextReader_ReadFinished_m5644EFA44232CE01BD8E44A128EE65D4E1697891 ();
// 0x000000E7 System.Boolean Newtonsoft.Json.JsonTextReader::ReadNullChar()
extern void JsonTextReader_ReadNullChar_m0B1EF00095EB4E85577747F312283ADCAF221ACE ();
// 0x000000E8 System.Void Newtonsoft.Json.JsonTextReader::EnsureBuffer()
extern void JsonTextReader_EnsureBuffer_m59BEA692315A45A060C23405E989AF9F1CF2B1A8 ();
// 0x000000E9 System.Void Newtonsoft.Json.JsonTextReader::ReadStringIntoBuffer(System.Char)
extern void JsonTextReader_ReadStringIntoBuffer_m02374811831B62F971B253AE56735ED16AB9EE2E ();
// 0x000000EA System.Void Newtonsoft.Json.JsonTextReader::FinishReadStringIntoBuffer(System.Int32,System.Int32,System.Int32)
extern void JsonTextReader_FinishReadStringIntoBuffer_mF8FE2C5A91A1B7FF1B48586B40733C1CFE46F624 ();
// 0x000000EB System.Void Newtonsoft.Json.JsonTextReader::WriteCharToBuffer(System.Char,System.Int32,System.Int32)
extern void JsonTextReader_WriteCharToBuffer_m2BA0C961F6D7DC874A612DC40766DBB27C62658B ();
// 0x000000EC System.Char Newtonsoft.Json.JsonTextReader::ConvertUnicode(System.Boolean)
extern void JsonTextReader_ConvertUnicode_mC1B823CEF49BA1E687A0D9A776F9F706716D167F ();
// 0x000000ED System.Char Newtonsoft.Json.JsonTextReader::ParseUnicode()
extern void JsonTextReader_ParseUnicode_mED7DCFA3C78AD4489D7A828BF0E3520ECC47DA1A ();
// 0x000000EE System.Void Newtonsoft.Json.JsonTextReader::ReadNumberIntoBuffer()
extern void JsonTextReader_ReadNumberIntoBuffer_m301AC99974646240887D3A7EDC534A4BF54ABA8B ();
// 0x000000EF System.Boolean Newtonsoft.Json.JsonTextReader::ReadNumberCharIntoBuffer(System.Char,System.Int32)
extern void JsonTextReader_ReadNumberCharIntoBuffer_mCAA2EE0EB25CBB44EB5EF464EED5C73FEA233D7E ();
// 0x000000F0 System.Void Newtonsoft.Json.JsonTextReader::ClearRecentString()
extern void JsonTextReader_ClearRecentString_m11B91928C0C2029A03A84086D8549D029C3CA6C8 ();
// 0x000000F1 System.Boolean Newtonsoft.Json.JsonTextReader::ParsePostValue(System.Boolean)
extern void JsonTextReader_ParsePostValue_mC39E826C3F2FB2CE5DAE30156BD600FEC4C4F701 ();
// 0x000000F2 System.Boolean Newtonsoft.Json.JsonTextReader::ParseObject()
extern void JsonTextReader_ParseObject_mB231A4928ABEEFCA075079A1D22C686EBC46F121 ();
// 0x000000F3 System.Boolean Newtonsoft.Json.JsonTextReader::ParseProperty()
extern void JsonTextReader_ParseProperty_mE55E07230DE0236AF32DE37CEA5D54E8240F2D4E ();
// 0x000000F4 System.Boolean Newtonsoft.Json.JsonTextReader::ValidIdentifierChar(System.Char)
extern void JsonTextReader_ValidIdentifierChar_m659AABF49B26DB1CF2F8597A92734EEFC0DDA1C2 ();
// 0x000000F5 System.Void Newtonsoft.Json.JsonTextReader::ParseUnquotedProperty()
extern void JsonTextReader_ParseUnquotedProperty_m2895E5CD129B6D17B27522A877A8EFCC455313C9 ();
// 0x000000F6 System.Boolean Newtonsoft.Json.JsonTextReader::ReadUnquotedPropertyReportIfDone(System.Char,System.Int32)
extern void JsonTextReader_ReadUnquotedPropertyReportIfDone_m25CC9BADD96170C2203B43C7F35599C9199914B0 ();
// 0x000000F7 System.Boolean Newtonsoft.Json.JsonTextReader::ParseValue()
extern void JsonTextReader_ParseValue_m8E626BD3A71365BADA85811C1F248C7E82B8340D ();
// 0x000000F8 System.Void Newtonsoft.Json.JsonTextReader::ProcessLineFeed()
extern void JsonTextReader_ProcessLineFeed_m504D07288A3E04F5798F457C78072BAA551EBE07 ();
// 0x000000F9 System.Void Newtonsoft.Json.JsonTextReader::ProcessCarriageReturn(System.Boolean)
extern void JsonTextReader_ProcessCarriageReturn_mE5331F5AFA92FADF90BF8A1E7D6D490B1A886FBE ();
// 0x000000FA System.Void Newtonsoft.Json.JsonTextReader::EatWhitespace()
extern void JsonTextReader_EatWhitespace_m4597FEBF59765A8B124EEA32825F593D74DBD36E ();
// 0x000000FB System.Void Newtonsoft.Json.JsonTextReader::ParseConstructor()
extern void JsonTextReader_ParseConstructor_m8C37823F833B2B0093CD0522400D59D62C671B4F ();
// 0x000000FC System.Void Newtonsoft.Json.JsonTextReader::ParseNumber(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumber_mD1CF3F5EDF2DE1608DB250ECAC2FCF41A6769703 ();
// 0x000000FD System.Void Newtonsoft.Json.JsonTextReader::ParseReadNumber(Newtonsoft.Json.ReadType,System.Char,System.Int32)
extern void JsonTextReader_ParseReadNumber_m5783A5B687A11F1E961C386A7713771FC475C00E ();
// 0x000000FE Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::ThrowReaderError(System.String,System.Exception)
extern void JsonTextReader_ThrowReaderError_mF93C319790CE54EB11457EDB965524B1BD3C3DD8 ();
// 0x000000FF System.Void Newtonsoft.Json.JsonTextReader::ParseComment(System.Boolean)
extern void JsonTextReader_ParseComment_m7C6442B7246754DBC9A245F75507724D96264ADF ();
// 0x00000100 System.Void Newtonsoft.Json.JsonTextReader::EndComment(System.Boolean,System.Int32,System.Int32)
extern void JsonTextReader_EndComment_m19EC51BE1DEF5944076FB728C646AB92E53D3B28 ();
// 0x00000101 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.String)
extern void JsonTextReader_MatchValue_m075D6C448798134363261D015AFF537E294F2B8B ();
// 0x00000102 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Boolean,System.String)
extern void JsonTextReader_MatchValue_mD289A110DB4617E234A506899DEB563B500CCEFF ();
// 0x00000103 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValueWithTrailingSeparator(System.String)
extern void JsonTextReader_MatchValueWithTrailingSeparator_m9775B25A1A7D5B9F119413B2E66A458FDE2CE2F1 ();
// 0x00000104 System.Boolean Newtonsoft.Json.JsonTextReader::IsSeparator(System.Char)
extern void JsonTextReader_IsSeparator_mC404377FA0891D92378FD9FCA0005E7712837C04 ();
// 0x00000105 System.Void Newtonsoft.Json.JsonTextReader::ParseTrue()
extern void JsonTextReader_ParseTrue_m75E55C52D9BF6A9FC040ADF6A7E50719045C8F57 ();
// 0x00000106 System.Void Newtonsoft.Json.JsonTextReader::ParseNull()
extern void JsonTextReader_ParseNull_m5CF3333EE6A2D8586952E9FB7756199B50F683D1 ();
// 0x00000107 System.Void Newtonsoft.Json.JsonTextReader::ParseUndefined()
extern void JsonTextReader_ParseUndefined_m7FBD87AB1DE479AE17B82A56C30CF7054DFF53AF ();
// 0x00000108 System.Void Newtonsoft.Json.JsonTextReader::ParseFalse()
extern void JsonTextReader_ParseFalse_mBE167CBD38442D77369563636898D7B9409522AA ();
// 0x00000109 System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNegativeInfinity_m0C8A6152F3D0597137063B4E620A6B6AADB9524F ();
// 0x0000010A System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberNegativeInfinity_m4F8830D306A2E64E22369CFE0997A86B96A12FCB ();
// 0x0000010B System.Object Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberPositiveInfinity_mFD3E75CE17E87E772FF9A67CEAD6BF7D0BB8F3CD ();
// 0x0000010C System.Object Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberPositiveInfinity_mB11A5EE3FBE60BA4B2241850D03EF40BDC5668C9 ();
// 0x0000010D System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNaN(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNaN_mD56D70675DEDC4E82D6079F46A186FB8984AD29E ();
// 0x0000010E System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNaN(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberNaN_m02B73A3C6D19D102D2448795D274BEEEC9990E05 ();
// 0x0000010F System.Void Newtonsoft.Json.JsonTextReader::Close()
extern void JsonTextReader_Close_mDD3B622618D8DD4911730B8C20759FA3F0A09AAD ();
// 0x00000110 System.Boolean Newtonsoft.Json.JsonTextReader::HasLineInfo()
extern void JsonTextReader_HasLineInfo_m9EBB382ADC2BC19E83C8D8698170763CDFE447BF ();
// 0x00000111 System.Int32 Newtonsoft.Json.JsonTextReader::get_LineNumber()
extern void JsonTextReader_get_LineNumber_mBF433F4F37657825DCFDFCC13CF514DD41E7A0EB ();
// 0x00000112 System.Int32 Newtonsoft.Json.JsonTextReader::get_LinePosition()
extern void JsonTextReader_get_LinePosition_m2811031A40E62E7D3686347D4E29975D8888F860 ();
// 0x00000113 Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::get_Base64Encoder()
extern void JsonTextWriter_get_Base64Encoder_m57373C368E5458E437787B6A3705D68AE29E86AA ();
// 0x00000114 System.Char Newtonsoft.Json.JsonTextWriter::get_QuoteChar()
extern void JsonTextWriter_get_QuoteChar_mD730D52F805F2A8B69B035DEF7FB4732E577C809 ();
// 0x00000115 System.Void Newtonsoft.Json.JsonTextWriter::.ctor(System.IO.TextWriter)
extern void JsonTextWriter__ctor_m344EB779AA3C19BF0538BE547ABD227C3F5617B1 ();
// 0x00000116 System.Void Newtonsoft.Json.JsonTextWriter::Close()
extern void JsonTextWriter_Close_m428AE4CFCEE10215B1F278EB88F663559DA619B4 ();
// 0x00000117 System.Void Newtonsoft.Json.JsonTextWriter::CloseBufferAndWriter()
extern void JsonTextWriter_CloseBufferAndWriter_m060795449BCECD4D638656BDFEDADDD5563B5AC1 ();
// 0x00000118 System.Void Newtonsoft.Json.JsonTextWriter::WriteStartObject()
extern void JsonTextWriter_WriteStartObject_mCC70DDACF3B7DF872F72B9787D103A2A58E438BF ();
// 0x00000119 System.Void Newtonsoft.Json.JsonTextWriter::WriteStartArray()
extern void JsonTextWriter_WriteStartArray_m58CBDA8044E630C48FE274471DACB6518FF07B76 ();
// 0x0000011A System.Void Newtonsoft.Json.JsonTextWriter::WriteStartConstructor(System.String)
extern void JsonTextWriter_WriteStartConstructor_m2A4B67F48DA26E616AE399EE55008D58F04DE6C8 ();
// 0x0000011B System.Void Newtonsoft.Json.JsonTextWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteEnd_m86043CD2D5FC2CAEBBA4992383428CF4A3243506 ();
// 0x0000011C System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String)
extern void JsonTextWriter_WritePropertyName_mB45CF50C07961253C42A67C22209394440B43512 ();
// 0x0000011D System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonTextWriter_WritePropertyName_m08D42DA59D2A7BD46B4AD2B0427AD8C6A7A13973 ();
// 0x0000011E System.Void Newtonsoft.Json.JsonTextWriter::OnStringEscapeHandlingChanged()
extern void JsonTextWriter_OnStringEscapeHandlingChanged_mE0319659D85FE4584C6B98169D78AD1F8C131561 ();
// 0x0000011F System.Void Newtonsoft.Json.JsonTextWriter::UpdateCharEscapeFlags()
extern void JsonTextWriter_UpdateCharEscapeFlags_m698893BD314E970F16482C5121F84A53FAFE93C3 ();
// 0x00000120 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndent()
extern void JsonTextWriter_WriteIndent_m720E03FC8FEEDEFC73A4913F412C0D7B419C2F57 ();
// 0x00000121 System.Int32 Newtonsoft.Json.JsonTextWriter::SetIndentChars()
extern void JsonTextWriter_SetIndentChars_mE772CA2DB92EBCBA4E70A97069EA528111F8DC34 ();
// 0x00000122 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueDelimiter()
extern void JsonTextWriter_WriteValueDelimiter_mECB01176B06080C499E9A384D8822B88C0B980A0 ();
// 0x00000123 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndentSpace()
extern void JsonTextWriter_WriteIndentSpace_mC6C57B5BEEEBA13006C4319CFFF6B6E3DC25ADA6 ();
// 0x00000124 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueInternal(System.String,Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteValueInternal_m01D0B3050E7A6AA87EADEB8303436A780080BE2F ();
// 0x00000125 System.Void Newtonsoft.Json.JsonTextWriter::WriteNull()
extern void JsonTextWriter_WriteNull_m38E23776B07F0CEAA0B18E1A6549EFBD19A74039 ();
// 0x00000126 System.Void Newtonsoft.Json.JsonTextWriter::WriteUndefined()
extern void JsonTextWriter_WriteUndefined_m8C51DAE7C8D431138869D55B1CB07FC9A3BC5607 ();
// 0x00000127 System.Void Newtonsoft.Json.JsonTextWriter::WriteRaw(System.String)
extern void JsonTextWriter_WriteRaw_m9D0661F55FBA967E80D81C909132EC02FD900A20 ();
// 0x00000128 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.String)
extern void JsonTextWriter_WriteValue_m82176C2D11A1170B97B816F2593BC0DEA70A81A1 ();
// 0x00000129 System.Void Newtonsoft.Json.JsonTextWriter::WriteEscapedString(System.String,System.Boolean)
extern void JsonTextWriter_WriteEscapedString_m30678B63E78451A849F0AEB24ED9FC2151B23AEC ();
// 0x0000012A System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int32)
extern void JsonTextWriter_WriteValue_m4E04EB74C546C46C347DED9E6BF9AE3ED62F0104 ();
// 0x0000012B System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt32)
extern void JsonTextWriter_WriteValue_m63662223218A01B53F5C0975D2D67670B24A6A0E ();
// 0x0000012C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int64)
extern void JsonTextWriter_WriteValue_mDA1793E6E5373553630623375855D61B25DCEBD3 ();
// 0x0000012D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt64)
extern void JsonTextWriter_WriteValue_m338EA13FEA3F463A7DE53AE70843D2E41239D52F ();
// 0x0000012E System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Single)
extern void JsonTextWriter_WriteValue_mCDE58C488EE7E625A4D717742E3C022B2901C12E ();
// 0x0000012F System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonTextWriter_WriteValue_m481470E98AE70FCF7CD58F8BA0EE9BA2F2502E54 ();
// 0x00000130 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Double)
extern void JsonTextWriter_WriteValue_mA141F35C7779FF0A1AA670D2724B4CED3D518713 ();
// 0x00000131 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonTextWriter_WriteValue_mFE1BF06AEBEC9F698B48E96C2AE423E7AF57623A ();
// 0x00000132 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Boolean)
extern void JsonTextWriter_WriteValue_m4F51794EE4EC3EF365DFDAFB5F26E69597DAB756 ();
// 0x00000133 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int16)
extern void JsonTextWriter_WriteValue_m1B51029CB7DD9647EE829D8BD9037385FB83046F ();
// 0x00000134 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt16)
extern void JsonTextWriter_WriteValue_m81C8CFF4CF18D659589FB9B01DA4E6CE82D21C96 ();
// 0x00000135 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Char)
extern void JsonTextWriter_WriteValue_mFF5E65F22BB05609D008EC79B36F8F31F4600F5B ();
// 0x00000136 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte)
extern void JsonTextWriter_WriteValue_mF3554EDD8E41154FC681F1554EE63EC9391D3F61 ();
// 0x00000137 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.SByte)
extern void JsonTextWriter_WriteValue_m65E22B01DC2DB73EF7F6AD2AB9815F52EEC5CC03 ();
// 0x00000138 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Decimal)
extern void JsonTextWriter_WriteValue_m5B65B37BD44AC87786A8FC1A2BEB74F0CC46E6A2 ();
// 0x00000139 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTime)
extern void JsonTextWriter_WriteValue_mC781EDF21381D8F56AD0E54B380A9BB1326C114A ();
// 0x0000013A System.Int32 Newtonsoft.Json.JsonTextWriter::WriteValueToBuffer(System.DateTime)
extern void JsonTextWriter_WriteValueToBuffer_mDAACC45FFA18C0753922C2A3501BBE842FE6FD25 ();
// 0x0000013B System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte[])
extern void JsonTextWriter_WriteValue_mEEBD3192B5F5B435CF8CC5D78CB3373002E35964 ();
// 0x0000013C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Guid)
extern void JsonTextWriter_WriteValue_mAE66E72A49374060D63EBE30B6F28114C5378410 ();
// 0x0000013D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.TimeSpan)
extern void JsonTextWriter_WriteValue_m7F58D32F28AF92F19CCD0DDB0EF5E6672400CAB7 ();
// 0x0000013E System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Uri)
extern void JsonTextWriter_WriteValue_mF0F4E1389F887C149F68696E9CFDF56ED944BF67 ();
// 0x0000013F System.Void Newtonsoft.Json.JsonTextWriter::WriteComment(System.String)
extern void JsonTextWriter_WriteComment_mB5C03EE5DD6EF551AD141607CAEE32F75B518934 ();
// 0x00000140 System.Void Newtonsoft.Json.JsonTextWriter::EnsureWriteBuffer()
extern void JsonTextWriter_EnsureWriteBuffer_mC30FED151F0DE8E7465B4815F6340524A43AEE15 ();
// 0x00000141 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.Int64)
extern void JsonTextWriter_WriteIntegerValue_m28AD8FEC60B9718FEDB0960FBDE81A3A4C92F77A ();
// 0x00000142 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.UInt64,System.Boolean)
extern void JsonTextWriter_WriteIntegerValue_mF4EDEEBCCCE1D247DD7030350F5AA9D21FAED539 ();
// 0x00000143 System.Int32 Newtonsoft.Json.JsonTextWriter::WriteNumberToBuffer(System.UInt64,System.Boolean)
extern void JsonTextWriter_WriteNumberToBuffer_mE7207F1CFA6F3440ADB854B1B412D95F0FF04DB6 ();
// 0x00000144 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.Int32)
extern void JsonTextWriter_WriteIntegerValue_mCB85257874EC4E329E1B3245B910CCB789451D1D ();
// 0x00000145 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.UInt32,System.Boolean)
extern void JsonTextWriter_WriteIntegerValue_m021DA68672908538141F1135D375B41E4514D81C ();
// 0x00000146 System.Int32 Newtonsoft.Json.JsonTextWriter::WriteNumberToBuffer(System.UInt32,System.Boolean)
extern void JsonTextWriter_WriteNumberToBuffer_m615D27EE31DE94794B4EC346AEC67C6D353B20A0 ();
// 0x00000147 Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::BuildStateArray()
extern void JsonWriter_BuildStateArray_m73173DCEE0FE8D86A4A6B8560BF87DE22C47FC1A ();
// 0x00000148 System.Void Newtonsoft.Json.JsonWriter::.cctor()
extern void JsonWriter__cctor_mB16995CBB9819622E08CA8A6D5DA7BE7661C153B ();
// 0x00000149 System.Boolean Newtonsoft.Json.JsonWriter::get_CloseOutput()
extern void JsonWriter_get_CloseOutput_mD0D99EC3BAB0F616E15B9FE03C2ABC947E5C4151 ();
// 0x0000014A System.Void Newtonsoft.Json.JsonWriter::set_CloseOutput(System.Boolean)
extern void JsonWriter_set_CloseOutput_mA722DE122AFF55E4B8F2CD836D81CA859AF6BF49 ();
// 0x0000014B System.Boolean Newtonsoft.Json.JsonWriter::get_AutoCompleteOnClose()
extern void JsonWriter_get_AutoCompleteOnClose_m6D283F8153209F11A7FB7FCD1425A3A7596C1A0F ();
// 0x0000014C System.Void Newtonsoft.Json.JsonWriter::set_AutoCompleteOnClose(System.Boolean)
extern void JsonWriter_set_AutoCompleteOnClose_m613DA95AD0296DAA59BE38E8EFAF114491C2DFE5 ();
// 0x0000014D System.Int32 Newtonsoft.Json.JsonWriter::get_Top()
extern void JsonWriter_get_Top_m06FE406A5BEAD226E28F5C206CC802F72E771CD4 ();
// 0x0000014E Newtonsoft.Json.WriteState Newtonsoft.Json.JsonWriter::get_WriteState()
extern void JsonWriter_get_WriteState_mA40CDDF21B3B472F7BF9486CEA68744F9418D007 ();
// 0x0000014F System.String Newtonsoft.Json.JsonWriter::get_ContainerPath()
extern void JsonWriter_get_ContainerPath_mA9290A7DF7EFF28ABA0A159A4B00237259DB9546 ();
// 0x00000150 System.String Newtonsoft.Json.JsonWriter::get_Path()
extern void JsonWriter_get_Path_m965DB09A52D16CC2D6E6C7AEC5235B68740EEB8F ();
// 0x00000151 Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::get_Formatting()
extern void JsonWriter_get_Formatting_m85DCC26F9351ECF7C2A4D72A943951C58F1FD55F ();
// 0x00000152 System.Void Newtonsoft.Json.JsonWriter::set_Formatting(Newtonsoft.Json.Formatting)
extern void JsonWriter_set_Formatting_mA1623232BC3D41CEB26AB7AA7193DDA40EE85074 ();
// 0x00000153 Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::get_DateFormatHandling()
extern void JsonWriter_get_DateFormatHandling_mABE02A23C0FCB901F53121FE1F74420223BB3E82 ();
// 0x00000154 System.Void Newtonsoft.Json.JsonWriter::set_DateFormatHandling(Newtonsoft.Json.DateFormatHandling)
extern void JsonWriter_set_DateFormatHandling_m1A9CC51681E4BA63EE773D9DB3257459DF1D843F ();
// 0x00000155 Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::get_DateTimeZoneHandling()
extern void JsonWriter_get_DateTimeZoneHandling_m05CDCC55984513F2F487BB40CBFEB692BBA8EBCA ();
// 0x00000156 System.Void Newtonsoft.Json.JsonWriter::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonWriter_set_DateTimeZoneHandling_m6785061AE10DBB8B1E03586004858C8B8203967F ();
// 0x00000157 Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::get_StringEscapeHandling()
extern void JsonWriter_get_StringEscapeHandling_mE8AB4C70B2D840C2E04375184395850A09A47BF8 ();
// 0x00000158 System.Void Newtonsoft.Json.JsonWriter::set_StringEscapeHandling(Newtonsoft.Json.StringEscapeHandling)
extern void JsonWriter_set_StringEscapeHandling_mCD72BC5B15385CC32DF1F4CD28BF0552A56668E5 ();
// 0x00000159 System.Void Newtonsoft.Json.JsonWriter::OnStringEscapeHandlingChanged()
extern void JsonWriter_OnStringEscapeHandlingChanged_mF207B9E57E6A5D4D730C0D487587D7E9D8F96E97 ();
// 0x0000015A Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::get_FloatFormatHandling()
extern void JsonWriter_get_FloatFormatHandling_m1249DAD3B85F6E0F7EE3AD5F2C853B032B779489 ();
// 0x0000015B System.Void Newtonsoft.Json.JsonWriter::set_FloatFormatHandling(Newtonsoft.Json.FloatFormatHandling)
extern void JsonWriter_set_FloatFormatHandling_mF337661FAA8A51D0502CF709EB1B243F24C3C0B5 ();
// 0x0000015C System.String Newtonsoft.Json.JsonWriter::get_DateFormatString()
extern void JsonWriter_get_DateFormatString_m773103CB571412471A793CAFC440B3C385F6C673 ();
// 0x0000015D System.Void Newtonsoft.Json.JsonWriter::set_DateFormatString(System.String)
extern void JsonWriter_set_DateFormatString_m945500D83A209F1D550C3948DDAB6752DE4C15DB ();
// 0x0000015E System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::get_Culture()
extern void JsonWriter_get_Culture_m73229B8C6B57D3440A1DAD8FFADEC0B6AD83AD6E ();
// 0x0000015F System.Void Newtonsoft.Json.JsonWriter::set_Culture(System.Globalization.CultureInfo)
extern void JsonWriter_set_Culture_m0DCE408EEE12A4422C24107BF6B95F563272293C ();
// 0x00000160 System.Void Newtonsoft.Json.JsonWriter::.ctor()
extern void JsonWriter__ctor_m3299A3DDE82465D2B0CF736D685C642B9E9CD417 ();
// 0x00000161 System.Void Newtonsoft.Json.JsonWriter::UpdateScopeWithFinishedValue()
extern void JsonWriter_UpdateScopeWithFinishedValue_mC11A0DD0AEC7842A9289957EB2B682093677D364 ();
// 0x00000162 System.Void Newtonsoft.Json.JsonWriter::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_Push_m83EACD5DE9A372717B9A4FA7EC8F8BE9EA33E57B ();
// 0x00000163 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Pop()
extern void JsonWriter_Pop_m467BB7D43DBC26C95815EB1CB6A9CEFB64757F3E ();
// 0x00000164 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Peek()
extern void JsonWriter_Peek_m1464615041C35045232D46F737F8F8C5DD02BABA ();
// 0x00000165 System.Void Newtonsoft.Json.JsonWriter::Close()
extern void JsonWriter_Close_m6AEF1A5E237CBBA4F632CA0669FAEC2E8334968B ();
// 0x00000166 System.Void Newtonsoft.Json.JsonWriter::WriteStartObject()
extern void JsonWriter_WriteStartObject_m5132FC69631815FD70E05A7A710B9E9DB4D0CDA0 ();
// 0x00000167 System.Void Newtonsoft.Json.JsonWriter::WriteEndObject()
extern void JsonWriter_WriteEndObject_mA156066911ABC6B4B7E1E4B8180726FA6A59855A ();
// 0x00000168 System.Void Newtonsoft.Json.JsonWriter::WriteStartArray()
extern void JsonWriter_WriteStartArray_m3265437EEF4FD8503C8F9EC2FBEFE66F7226DFA1 ();
// 0x00000169 System.Void Newtonsoft.Json.JsonWriter::WriteEndArray()
extern void JsonWriter_WriteEndArray_m16BFF8EC6210580D4675C857E513584A45379E28 ();
// 0x0000016A System.Void Newtonsoft.Json.JsonWriter::WriteStartConstructor(System.String)
extern void JsonWriter_WriteStartConstructor_m7AF6B811E053566BC90E91B05E2AE7846EA94174 ();
// 0x0000016B System.Void Newtonsoft.Json.JsonWriter::WriteEndConstructor()
extern void JsonWriter_WriteEndConstructor_mB3474CF26705EB9FF31779043ABAD5F52DCC1E47 ();
// 0x0000016C System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String)
extern void JsonWriter_WritePropertyName_m7157900D394C7C8A2FB41374846519B945EA6F9F ();
// 0x0000016D System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonWriter_WritePropertyName_mFE8E1614C66BCA8F196ADE8181CD9BCCB02880D4 ();
// 0x0000016E System.Void Newtonsoft.Json.JsonWriter::WriteEnd()
extern void JsonWriter_WriteEnd_m107687C7DCB87829E9B589FE0378C84AE3BE25C4 ();
// 0x0000016F System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader)
extern void JsonWriter_WriteToken_mD30A47F97A0D909ED2CC9A7AF476AFA00AD986D5 ();
// 0x00000170 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean)
extern void JsonWriter_WriteToken_mA24011E9186AF80407F0A4AE02F12B74224F7BB9 ();
// 0x00000171 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonWriter_WriteToken_m6DAE89D82DE4C4680272134E3E126C6C16A5819C ();
// 0x00000172 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean,System.Boolean,System.Boolean)
extern void JsonWriter_WriteToken_m0A8DA0C65F411D80FF7EDF7A0C3D42D2E25BE400 ();
// 0x00000173 System.Int32 Newtonsoft.Json.JsonWriter::CalculateWriteTokenInitialDepth(Newtonsoft.Json.JsonReader)
extern void JsonWriter_CalculateWriteTokenInitialDepth_mB04EB72397DF45F3EC98D98E366C9F5033D8179C ();
// 0x00000174 System.Int32 Newtonsoft.Json.JsonWriter::CalculateWriteTokenFinalDepth(Newtonsoft.Json.JsonReader)
extern void JsonWriter_CalculateWriteTokenFinalDepth_mDD1C7CF54990FD094A2CF58C71BA34AD4D83F97A ();
// 0x00000175 System.Void Newtonsoft.Json.JsonWriter::WriteConstructorDate(Newtonsoft.Json.JsonReader)
extern void JsonWriter_WriteConstructorDate_m042928008784ABD220DD6B8E29090428C087FD5A ();
// 0x00000176 System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_WriteEnd_m2B22EB9E3621F9EAB6564C5721134B43AF2E90F4 ();
// 0x00000177 System.Void Newtonsoft.Json.JsonWriter::AutoCompleteAll()
extern void JsonWriter_AutoCompleteAll_m63DC7F3D3FAE18A5964DCE8F8ED77335F319AAA2 ();
// 0x00000178 Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonWriter::GetCloseTokenForType(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_GetCloseTokenForType_m8B135504923FDAEC378F298C42644A7E261DC0D7 ();
// 0x00000179 System.Void Newtonsoft.Json.JsonWriter::AutoCompleteClose(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_AutoCompleteClose_m95D49303152DA56B437CB194C59897D49085D485 ();
// 0x0000017A System.Int32 Newtonsoft.Json.JsonWriter::CalculateLevelsToComplete(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_CalculateLevelsToComplete_m6F70D9434240904414722E1FCE1F9E8A9FF5C735 ();
// 0x0000017B System.Void Newtonsoft.Json.JsonWriter::UpdateCurrentState()
extern void JsonWriter_UpdateCurrentState_m2B3AC465B975F2FAD70CA03329BE872DD7664432 ();
// 0x0000017C System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonWriter_WriteEnd_m5F44EE29B0E14DBA543DB4729E4FDACD5957236F ();
// 0x0000017D System.Void Newtonsoft.Json.JsonWriter::WriteIndent()
extern void JsonWriter_WriteIndent_m105FE18738B00F7D2776F77323518046402BDEFB ();
// 0x0000017E System.Void Newtonsoft.Json.JsonWriter::WriteValueDelimiter()
extern void JsonWriter_WriteValueDelimiter_m8175731233D75F530A8807290FC78B18352AF1BE ();
// 0x0000017F System.Void Newtonsoft.Json.JsonWriter::WriteIndentSpace()
extern void JsonWriter_WriteIndentSpace_m5CA7EB6F38155296AA94515A5F69D399FF3585DA ();
// 0x00000180 System.Void Newtonsoft.Json.JsonWriter::AutoComplete(Newtonsoft.Json.JsonToken)
extern void JsonWriter_AutoComplete_mB7BAB3C255951F307D6BE6DD7E1C6831A6D48D38 ();
// 0x00000181 System.Void Newtonsoft.Json.JsonWriter::WriteNull()
extern void JsonWriter_WriteNull_m0FD8BEBB2BBA170138AE9C6C705C92E558320A9B ();
// 0x00000182 System.Void Newtonsoft.Json.JsonWriter::WriteUndefined()
extern void JsonWriter_WriteUndefined_m178BD42543356D26B7E99DA8E5E2CCF3004D0811 ();
// 0x00000183 System.Void Newtonsoft.Json.JsonWriter::WriteRaw(System.String)
extern void JsonWriter_WriteRaw_m6EE163869CD409F26E60073B507D73A4CECDE56D ();
// 0x00000184 System.Void Newtonsoft.Json.JsonWriter::WriteRawValue(System.String)
extern void JsonWriter_WriteRawValue_m41AA54810805FC37A6EBBBFBBFB9E69C1EA19B58 ();
// 0x00000185 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.String)
extern void JsonWriter_WriteValue_mC84EDFC58A7A8FF306CE6F15E4E456E5F02902B7 ();
// 0x00000186 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int32)
extern void JsonWriter_WriteValue_m68A7AE8D9CF34AD300ED01B85544E99AB775E8AA ();
// 0x00000187 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt32)
extern void JsonWriter_WriteValue_m2BBB5FF1C848BBAC00D349083E6C5731329A38BD ();
// 0x00000188 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int64)
extern void JsonWriter_WriteValue_m77CE130F58FF4C9CEDDB3C2B45E6C7C1FBD02981 ();
// 0x00000189 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt64)
extern void JsonWriter_WriteValue_m5D26E47E8875950BEF46BC77A976CE122DD751B2 ();
// 0x0000018A System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single)
extern void JsonWriter_WriteValue_m5301A218957F4A65A6BFB31B474E74E3392ECF23 ();
// 0x0000018B System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Double)
extern void JsonWriter_WriteValue_m66486269BFF584456DE35AF6F393F84FEFF56B89 ();
// 0x0000018C System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Boolean)
extern void JsonWriter_WriteValue_m6DABF07C6A729ACC379116FDC57E47C24574E249 ();
// 0x0000018D System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int16)
extern void JsonWriter_WriteValue_mFEC109BAB96B61845126AE50F2F5D9E5E6B798DA ();
// 0x0000018E System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt16)
extern void JsonWriter_WriteValue_m242608E121FA6873CCEFF584F0B235540656D703 ();
// 0x0000018F System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Char)
extern void JsonWriter_WriteValue_mC6975D72EE6699CEA4488753530FE3742452ED92 ();
// 0x00000190 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte)
extern void JsonWriter_WriteValue_m7ADFA914BD4B4CEAA35E0F3ABD959C896E9DD956 ();
// 0x00000191 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.SByte)
extern void JsonWriter_WriteValue_m527B7E2AE12DB82E048B42F9D96520D510A84297 ();
// 0x00000192 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Decimal)
extern void JsonWriter_WriteValue_m6C8EBC9973CB6EF721F208300E0559F32F8920F6 ();
// 0x00000193 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTime)
extern void JsonWriter_WriteValue_m55EDEF66C19EE6D1955592EF37E213BC5F60C0E5 ();
// 0x00000194 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Guid)
extern void JsonWriter_WriteValue_m2D7EBC1A209BBF27A77D6E57147001854DC463E8 ();
// 0x00000195 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.TimeSpan)
extern void JsonWriter_WriteValue_m0B1F342B47D5D1383657AE011BDC11670F05C571 ();
// 0x00000196 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern void JsonWriter_WriteValue_mA695B39F24210D8113A323EA0269258A3C4EE1A6 ();
// 0x00000197 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern void JsonWriter_WriteValue_m6E8D9DB5EEA5D90659535BFFC4DC581B82A9E475 ();
// 0x00000198 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern void JsonWriter_WriteValue_mF9C293FFD62FACA327C71CAB3CD5A75E878AE72D ();
// 0x00000199 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern void JsonWriter_WriteValue_m27A04F669D85D0F97BAB36799E3722B67A3FC49B ();
// 0x0000019A System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonWriter_WriteValue_m14064AE26EA3E5B36866F4F861A196EB9C200E5F ();
// 0x0000019B System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonWriter_WriteValue_m77A7E7EAC9B13A0756CF25DC4178CA3010CC578B ();
// 0x0000019C System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern void JsonWriter_WriteValue_m3932EC259233109D6CC64B7008D8F3B02A998A23 ();
// 0x0000019D System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern void JsonWriter_WriteValue_m64994C650001FA82624201009AE882AE3D6FDD69 ();
// 0x0000019E System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern void JsonWriter_WriteValue_mD9E66E4CFE07F89CA257213A9746CEC3D0061398 ();
// 0x0000019F System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern void JsonWriter_WriteValue_m20826615A413917E65098F0BBAD30B66BC77396D ();
// 0x000001A0 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void JsonWriter_WriteValue_mE62572704399FC870EA15E0C36BB52FB2A950982 ();
// 0x000001A1 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern void JsonWriter_WriteValue_mC17E770BC70CF513CFCC343D4B1D0190E0029D7D ();
// 0x000001A2 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern void JsonWriter_WriteValue_m6A594DBB70FF961711FA237F2D60F169AA940F90 ();
// 0x000001A3 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern void JsonWriter_WriteValue_mA7D61778A627FEBF4538F33D6AB9B9D391C2B74C ();
// 0x000001A4 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern void JsonWriter_WriteValue_m12A6CD9BA564D23CF575B59DFB82E1EA49B78DED ();
// 0x000001A5 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern void JsonWriter_WriteValue_m3FF6BA49F6C9FFBC8BC1375E2095DC7DECCC5C74 ();
// 0x000001A6 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte[])
extern void JsonWriter_WriteValue_m5761F7F00B42F7B7DED177B4970CEB5606AF7FE8 ();
// 0x000001A7 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Uri)
extern void JsonWriter_WriteValue_mBB6DC1861474504F0ED964BA2781B47083EE5976 ();
// 0x000001A8 System.Void Newtonsoft.Json.JsonWriter::WriteComment(System.String)
extern void JsonWriter_WriteComment_mAC796E558310D38291EB0E0875AA542407C46584 ();
// 0x000001A9 System.Void Newtonsoft.Json.JsonWriter::System.IDisposable.Dispose()
extern void JsonWriter_System_IDisposable_Dispose_mA737C1CC092461603FEEA734B9FDF3E18A8B5551 ();
// 0x000001AA System.Void Newtonsoft.Json.JsonWriter::Dispose(System.Boolean)
extern void JsonWriter_Dispose_m8009396C5AA34C60B94A809BEE35C2351F068FE1 ();
// 0x000001AB System.Void Newtonsoft.Json.JsonWriter::WriteValue(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Object)
extern void JsonWriter_WriteValue_m253A963D4485D232597A4E3D0FDFD200B31CF3DA ();
// 0x000001AC System.Void Newtonsoft.Json.JsonWriter::ResolveConvertibleValue(System.IConvertible,Newtonsoft.Json.Utilities.PrimitiveTypeCode&,System.Object&)
extern void JsonWriter_ResolveConvertibleValue_m3515C7424B2C720BBEE4517F3478D4165B84941A ();
// 0x000001AD Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriter::CreateUnsupportedTypeException(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonWriter_CreateUnsupportedTypeException_mFA0E6848FECC96BAABFE8DBCD007B1F5C1BBA118 ();
// 0x000001AE System.Void Newtonsoft.Json.JsonWriter::SetWriteState(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonWriter_SetWriteState_m52C914A54478648DAF335637EE06A1EA07E07FC3 ();
// 0x000001AF System.Void Newtonsoft.Json.JsonWriter::InternalWriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteEnd_m87362A5341E4DD9CC51DE540DD0A311D7D9196F9 ();
// 0x000001B0 System.Void Newtonsoft.Json.JsonWriter::InternalWritePropertyName(System.String)
extern void JsonWriter_InternalWritePropertyName_mA3CB0892966C12792342EE02E8249DD605312FF0 ();
// 0x000001B1 System.Void Newtonsoft.Json.JsonWriter::InternalWriteRaw()
extern void JsonWriter_InternalWriteRaw_m15F01B472C3A562731C75565E1F52DCC74DDB66C ();
// 0x000001B2 System.Void Newtonsoft.Json.JsonWriter::InternalWriteStart(Newtonsoft.Json.JsonToken,Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteStart_mCD4A59771D7BA1D5C44ABBCD5A2C38632D7E33CA ();
// 0x000001B3 System.Void Newtonsoft.Json.JsonWriter::InternalWriteValue(Newtonsoft.Json.JsonToken)
extern void JsonWriter_InternalWriteValue_mB415D72FC95E93FEC7AB9E0CB9A14602BC857114 ();
// 0x000001B4 System.Void Newtonsoft.Json.JsonWriter::InternalWriteComment()
extern void JsonWriter_InternalWriteComment_m94AD4FBEB479C2BBD93DD411CA80D89E283C9366 ();
// 0x000001B5 System.Void Newtonsoft.Json.JsonWriterException::.ctor()
extern void JsonWriterException__ctor_mBF32C5A5668CE121D3893B42B9B7513BA354FE6D ();
// 0x000001B6 System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonWriterException__ctor_mB5B0B047EC836B13233851125916F5E45D74ADF7 ();
// 0x000001B7 System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String,System.String,System.Exception)
extern void JsonWriterException__ctor_m4C83004213435CE5CF252DADA8B58139DB0F21B7 ();
// 0x000001B8 Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(Newtonsoft.Json.JsonWriter,System.String,System.Exception)
extern void JsonWriterException_Create_mCD7BF9B20AFD76955151D4896FACC86FDA3D9DB5 ();
// 0x000001B9 Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(System.String,System.String,System.Exception)
extern void JsonWriterException_Create_mDA098F93B086B2E37D62069D573788ACFE8F4D78 ();
// 0x000001BA System.Void Newtonsoft.Json.Utilities.Base64Encoder::.ctor(System.IO.TextWriter)
extern void Base64Encoder__ctor_mC4CBAD945D781A4176190E1DFB843C8F883ED053 ();
// 0x000001BB System.Void Newtonsoft.Json.Utilities.Base64Encoder::ValidateEncode(System.Byte[],System.Int32,System.Int32)
extern void Base64Encoder_ValidateEncode_m549263E0E9A63ECE0AF45BA74CF995DB439019C2 ();
// 0x000001BC System.Void Newtonsoft.Json.Utilities.Base64Encoder::Encode(System.Byte[],System.Int32,System.Int32)
extern void Base64Encoder_Encode_mF1E5F00D5BF599A77CEED5F188D5DE656973C6C4 ();
// 0x000001BD System.Void Newtonsoft.Json.Utilities.Base64Encoder::StoreLeftOverBytes(System.Byte[],System.Int32,System.Int32&)
extern void Base64Encoder_StoreLeftOverBytes_m6406F00D029031E5B1FB7C76E6206C37950AA8D0 ();
// 0x000001BE System.Boolean Newtonsoft.Json.Utilities.Base64Encoder::FulfillFromLeftover(System.Byte[],System.Int32,System.Int32&)
extern void Base64Encoder_FulfillFromLeftover_m25942D9D22ABAA4D6E4D7CC35DC4B06BDD876986 ();
// 0x000001BF System.Void Newtonsoft.Json.Utilities.Base64Encoder::Flush()
extern void Base64Encoder_Flush_m6D30AA62E4FE7E4AE243955BD5885DEDCEB7C3C5 ();
// 0x000001C0 System.Void Newtonsoft.Json.Utilities.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern void Base64Encoder_WriteChars_m5925C49AB37B29D2A60DD3BDD1270AD91F2C69DA ();
// 0x000001C1 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
// 0x000001C2 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::Set(TFirst,TSecond)
// 0x000001C3 System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetByFirst(TFirst,TSecond&)
// 0x000001C4 System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetBySecond(TSecond,TFirst&)
// 0x000001C5 System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsNullOrEmpty(System.Collections.Generic.ICollection`1<T>)
// 0x000001C6 System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x000001C7 System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.IEnumerable)
// 0x000001C8 System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsDictionaryType(System.Type)
extern void CollectionUtils_IsDictionaryType_mB14E1B02BEA74BF90220EABEAD5C25B974A045A5 ();
// 0x000001C9 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_m6C553198F70C217853C08C9F8FFD2D8B21F9F81A ();
// 0x000001CA System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_m966F5145F4DF95EEFA2B1BD82BD79055E34BAF97 ();
// 0x000001CB System.Int32 Newtonsoft.Json.Utilities.CollectionUtils::IndexOf(System.Collections.Generic.IEnumerable`1<T>,Newtonsoft.Json.Serialization.Func`2<T,System.Boolean>)
// 0x000001CC System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::Contains(System.Collections.Generic.List`1<T>,T,System.Collections.IEqualityComparer)
// 0x000001CD System.Int32 Newtonsoft.Json.Utilities.CollectionUtils::IndexOfReference(System.Collections.Generic.List`1<T>,T)
// 0x000001CE System.Void Newtonsoft.Json.Utilities.CollectionUtils::FastReverse(System.Collections.Generic.List`1<T>)
// 0x000001CF System.Collections.Generic.IList`1<System.Int32> Newtonsoft.Json.Utilities.CollectionUtils::GetDimensions(System.Collections.IList,System.Int32)
extern void CollectionUtils_GetDimensions_mE2CFCFD696DA5FA323EDE9638B84B89B9F8DE56A ();
// 0x000001D0 System.Void Newtonsoft.Json.Utilities.CollectionUtils::CopyFromJaggedToMultidimensionalArray(System.Collections.IList,System.Array,System.Int32[])
extern void CollectionUtils_CopyFromJaggedToMultidimensionalArray_mFA6EA8B6CD9BC85F5885E36A05FF6F1B08AEA2CA ();
// 0x000001D1 System.Object Newtonsoft.Json.Utilities.CollectionUtils::JaggedArrayGetValue(System.Collections.IList,System.Int32[])
extern void CollectionUtils_JaggedArrayGetValue_m23A652C55B82F8534BC7AECD115EECF5C359A4D6 ();
// 0x000001D2 System.Array Newtonsoft.Json.Utilities.CollectionUtils::ToMultidimensionalArray(System.Collections.IList,System.Type,System.Int32)
extern void CollectionUtils_ToMultidimensionalArray_m2EEE96BB43FF18002956B09982FE97CD63E096C9 ();
// 0x000001D3 T[] Newtonsoft.Json.Utilities.CollectionUtils::ArrayEmpty()
// 0x000001D4 System.Object Newtonsoft.Json.Utilities.IWrappedCollection::get_UnderlyingCollection()
// 0x000001D5 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::.ctor(System.Collections.IList)
// 0x000001D6 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x000001D7 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::Add(T)
// 0x000001D8 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::Clear()
// 0x000001D9 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::Contains(T)
// 0x000001DA System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::CopyTo(T[],System.Int32)
// 0x000001DB System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::get_Count()
// 0x000001DC System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::get_IsReadOnly()
// 0x000001DD System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::Remove(T)
// 0x000001DE System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1::GetEnumerator()
// 0x000001DF System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000001E0 System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Add(System.Object)
// 0x000001E1 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Contains(System.Object)
// 0x000001E2 System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.IndexOf(System.Object)
// 0x000001E3 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x000001E4 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x000001E5 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.get_IsFixedSize()
// 0x000001E6 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Remove(System.Object)
// 0x000001E7 System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.get_Item(System.Int32)
// 0x000001E8 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x000001E9 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x000001EA System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.ICollection.get_SyncRoot()
// 0x000001EB System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::VerifyValueType(System.Object)
// 0x000001EC System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::IsCompatibleObject(System.Object)
// 0x000001ED System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::get_UnderlyingCollection()
// 0x000001EE System.Type Newtonsoft.Json.Utilities.TypeInformation::get_Type()
extern void TypeInformation_get_Type_m5622F49D76FA191ACDEEF95E93C36BEB91D810D8 ();
// 0x000001EF System.Void Newtonsoft.Json.Utilities.TypeInformation::set_Type(System.Type)
extern void TypeInformation_set_Type_mEE342CD463AB58FBEB92D67AD12C0301FD276CE4 ();
// 0x000001F0 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::get_TypeCode()
extern void TypeInformation_get_TypeCode_m2C2972ECA9A90A50E1B38E6E5BF8F35770472D14 ();
// 0x000001F1 System.Void Newtonsoft.Json.Utilities.TypeInformation::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void TypeInformation_set_TypeCode_m398D60C782A4AF8BF24799E6F1BD7022A43E2DAC ();
// 0x000001F2 System.Void Newtonsoft.Json.Utilities.TypeInformation::.ctor()
extern void TypeInformation__ctor_mED3C73AB8C3DE7CF6A9607ECB930BE28133FF398 ();
// 0x000001F3 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type)
extern void ConvertUtils_GetTypeCode_m98A7A7D6E4F516DF3BA90A41B608AC616B1DCA71 ();
// 0x000001F4 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type,System.Boolean&)
extern void ConvertUtils_GetTypeCode_m515C946C69926370D40753FA3939D80C18F91867 ();
// 0x000001F5 Newtonsoft.Json.Utilities.TypeInformation Newtonsoft.Json.Utilities.ConvertUtils::GetTypeInformation(System.IConvertible)
extern void ConvertUtils_GetTypeInformation_m7B3EB5E8BA53BE89F20632F071F0E694D4A1902E ();
// 0x000001F6 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsConvertible(System.Type)
extern void ConvertUtils_IsConvertible_m18BB3FD4936CF9B2321A76A6E55787B2D076A423 ();
// 0x000001F7 System.TimeSpan Newtonsoft.Json.Utilities.ConvertUtils::ParseTimeSpan(System.String)
extern void ConvertUtils_ParseTimeSpan_mB0F6A7CC0B0B810F0F8A61A4324DF80710CB1D78 ();
// 0x000001F8 Newtonsoft.Json.Serialization.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey)
extern void ConvertUtils_CreateCastConverter_mF16BB1A5FABB8905D3419B5025FEF6F7D86A2184 ();
// 0x000001F9 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvert_mDE33D0CBE790940604C049B4E66BC8C834E07CBD ();
// 0x000001FA Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult Newtonsoft.Json.Utilities.ConvertUtils::TryConvertInternal(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvertInternal_mF7308B4FB8645104422574B07624F291F9E3BF4C ();
// 0x000001FB System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern void ConvertUtils_ConvertOrCast_m234B06B247020C0884EC612D9057AC6FF8FB53B8 ();
// 0x000001FC System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern void ConvertUtils_EnsureTypeAssignable_m33695B8DF4E84BB86EBFE63EC46FDDC476DC4AAC ();
// 0x000001FD System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::VersionTryParse(System.String,System.Version&)
extern void ConvertUtils_VersionTryParse_m5725816D8E7EBE423513638150CE5621C5259F6C ();
// 0x000001FE System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern void ConvertUtils_IsInteger_m2473B63BF0694439E3B03E64907923404A99A8EA ();
// 0x000001FF Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int32TryParse(System.Char[],System.Int32,System.Int32,System.Int32&)
extern void ConvertUtils_Int32TryParse_mA09446ED6134554E02AF78D20458EAC8EE94D917 ();
// 0x00000200 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int64TryParse(System.Char[],System.Int32,System.Int32,System.Int64&)
extern void ConvertUtils_Int64TryParse_m6C13096BF01222333BEF765452134BF9FD8B7A33 ();
// 0x00000201 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::DecimalTryParse(System.Char[],System.Int32,System.Int32,System.Decimal&)
extern void ConvertUtils_DecimalTryParse_mE20F8E94703579075A72A89A5A9E721F287CA6EB ();
// 0x00000202 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertGuid(System.String,System.Guid&)
extern void ConvertUtils_TryConvertGuid_mAEDE54822286E8A7F9C7A369D14D884263BD226E ();
// 0x00000203 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryHexTextToInt(System.Char[],System.Int32,System.Int32,System.Int32&)
extern void ConvertUtils_TryHexTextToInt_m7CE9F2F025D138159458539C02BF162A39E93940 ();
// 0x00000204 System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern void ConvertUtils__cctor_m03D12791C3347C189765C85A239397104B02B401 ();
// 0x00000205 System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::get_InitialType()
extern void TypeConvertKey_get_InitialType_m1954D54FA029A8DCBA5B4815E6EF4AF294B28532_AdjustorThunk ();
// 0x00000206 System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::get_TargetType()
extern void TypeConvertKey_get_TargetType_m57A6E28390929AE245A00818E79DFFD9EF100E88_AdjustorThunk ();
// 0x00000207 System.Void Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::.ctor(System.Type,System.Type)
extern void TypeConvertKey__ctor_m9C57E9FDCA5CF0618FE165FDA3813E7BFAEF7FEF_AdjustorThunk ();
// 0x00000208 System.Int32 Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::GetHashCode()
extern void TypeConvertKey_GetHashCode_mA906BBD90727DDCFBF61C68DCF9CAE190DE2BB46_AdjustorThunk ();
// 0x00000209 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::Equals(System.Object)
extern void TypeConvertKey_Equals_mFD9449A0550C1C3C00B6739BDFC628EE9DF6C407_AdjustorThunk ();
// 0x0000020A System.Boolean Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::Equals(Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey)
extern void TypeConvertKey_Equals_mB56CC4BEC97528C33606A8CCFD8A3D9BDDD47AC7_AdjustorThunk ();
// 0x0000020B System.Void Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m628C6F21C0428600D9928E144BCBC172A7B51B60 ();
// 0x0000020C System.Object Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0::<CreateCastConverter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass9_0_U3CCreateCastConverterU3Eb__0_m11B2092E8AE226493A956BFFBB34C250ABA57AFB ();
// 0x0000020D System.Void Newtonsoft.Json.Utilities.DateTimeParser::.cctor()
extern void DateTimeParser__cctor_mE5BB176F9F0C742DEDD54367FC0C4D4570F49892 ();
// 0x0000020E System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse(System.Char[],System.Int32,System.Int32)
extern void DateTimeParser_Parse_m8D839DF69155233C111A3066B2DBE1521ABC0F34_AdjustorThunk ();
// 0x0000020F System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseDate(System.Int32)
extern void DateTimeParser_ParseDate_m21304E123E53270F5F2C11C6CCE066A08CEA3855_AdjustorThunk ();
// 0x00000210 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTimeAndZoneAndWhitespace(System.Int32)
extern void DateTimeParser_ParseTimeAndZoneAndWhitespace_m8B1E25B1ABBFB2AA3EDD4330DFEEF6B126B8D5D0_AdjustorThunk ();
// 0x00000211 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTime(System.Int32&)
extern void DateTimeParser_ParseTime_mD035CFFAD3EBC4A77286479AA8C083283341AA91_AdjustorThunk ();
// 0x00000212 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseZone(System.Int32)
extern void DateTimeParser_ParseZone_mCCFB01B219A9C4D3B33D39710AE8B0F6B89ED501_AdjustorThunk ();
// 0x00000213 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse4Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse4Digit_m1EFA45A08E5E2B4B98F6BBBF9BF82309095BF954_AdjustorThunk ();
// 0x00000214 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse2Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse2Digit_mC8B195410B2FE66FCA501BA1147247E768A8C5F9_AdjustorThunk ();
// 0x00000215 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseChar(System.Int32,System.Char)
extern void DateTimeParser_ParseChar_mA51C47054381CC8DE368DE6EBF0352798C241098_AdjustorThunk ();
// 0x00000216 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::.cctor()
extern void DateTimeUtils__cctor_m9F196B690D756228460E6844A37F2F2F4FE991E2 ();
// 0x00000217 System.TimeSpan Newtonsoft.Json.Utilities.DateTimeUtils::GetUtcOffset(System.DateTime)
extern void DateTimeUtils_GetUtcOffset_mE63D6699FC3BE3EA24A906E115195AE43AC03D69 ();
// 0x00000218 System.Xml.XmlDateTimeSerializationMode Newtonsoft.Json.Utilities.DateTimeUtils::ToSerializationMode(System.DateTimeKind)
extern void DateTimeUtils_ToSerializationMode_m44D4624BBBDBD7EA6DD360DD67C2B5F10E2AF35C ();
// 0x00000219 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::EnsureDateTime(System.DateTime,Newtonsoft.Json.DateTimeZoneHandling)
extern void DateTimeUtils_EnsureDateTime_m9C23BE3D67ACE4157896A6C3AA7C1E39FC21AA6D ();
// 0x0000021A System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToLocalTime(System.DateTime)
extern void DateTimeUtils_SwitchToLocalTime_mA5951E29C4E037357C23B6CB2B7E9E782DB1CA1A ();
// 0x0000021B System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToUtcTime(System.DateTime)
extern void DateTimeUtils_SwitchToUtcTime_mF44B732D9D928D1A327CA68630457FC725AD8D57 ();
// 0x0000021C System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ToUniversalTicks_m18B2413B9B715E19A4394535597F4C7F0708D5DC ();
// 0x0000021D System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m6FDE9E2EFE7574BD23D0DD76EE5CEC6D928DF6CA ();
// 0x0000021E System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern void DateTimeUtils_UniversialTicksToJavaScriptTicks_m02A546342FF16C2822D140CEFD572FDD267282F4 ();
// 0x0000021F System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern void DateTimeUtils_ConvertJavaScriptTicksToDateTime_m049B6E252BF83188A13A66FD326D6357889E8F9F ();
// 0x00000220 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeIso(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeIso_m372F05D11A8E8C72979AB91CCF1B95E3B3BB8F2D ();
// 0x00000221 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::CreateDateTime(Newtonsoft.Json.Utilities.DateTimeParser)
extern void DateTimeUtils_CreateDateTime_m3DEC2C577C76B744BDB7CC3AFE694F9BB45F7185 ();
// 0x00000222 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_mD789926381DB2424577C2844F9EBF3A2E93BF0D2 ();
// 0x00000223 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_mA79B4B042533677F961E826D391540982E50641A ();
// 0x00000224 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseMicrosoftDate(Newtonsoft.Json.Utilities.StringReference,System.Int64&,System.TimeSpan&,System.DateTimeKind&)
extern void DateTimeUtils_TryParseMicrosoftDate_m0D741CB59BF11EE289302C0065E56F9FA08CF28D ();
// 0x00000225 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeMicrosoft(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeMicrosoft_mEF817232A5326A0399F8CFD33C9A3D898D380F90 ();
// 0x00000226 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeExact(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeExact_m661F016F15BE6A6C71328CC54C80E1040D72EBB5 ();
// 0x00000227 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryReadOffset(Newtonsoft.Json.Utilities.StringReference,System.Int32,System.TimeSpan&)
extern void DateTimeUtils_TryReadOffset_m9B184DBC5236D3E7B12A319FCC7B16BD0F280671 ();
// 0x00000228 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.IO.TextWriter,System.DateTime,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern void DateTimeUtils_WriteDateTimeString_m08855D6349C306443F5B7F508286FAF294E0D429 ();
// 0x00000229 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.Char[],System.Int32,System.DateTime,System.Nullable`1<System.TimeSpan>,System.DateTimeKind,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeString_mD2B32C68D9565CEF7DF4627D491DBE99A830BB39 ();
// 0x0000022A System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDefaultIsoDate(System.Char[],System.Int32,System.DateTime)
extern void DateTimeUtils_WriteDefaultIsoDate_m1AD19F17470238D3E40E0F6724E253E72D3DC8B9 ();
// 0x0000022B System.Void Newtonsoft.Json.Utilities.DateTimeUtils::CopyIntToCharArray(System.Char[],System.Int32,System.Int32,System.Int32)
extern void DateTimeUtils_CopyIntToCharArray_mA3CF1D1A6F54115B8C84D2B8467E70C822B98EC3 ();
// 0x0000022C System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffset(System.Char[],System.Int32,System.TimeSpan,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeOffset_m93D3CCF79C993A4497A0FD6C580C88C674A08F60 ();
// 0x0000022D System.Void Newtonsoft.Json.Utilities.DateTimeUtils::GetDateValues(System.DateTime,System.Int32&,System.Int32&,System.Int32&)
extern void DateTimeUtils_GetDateValues_m09986AC1D8DA89BE0DD8B9194F604576C061C106 ();
// 0x0000022E System.Object Newtonsoft.Json.Utilities.IWrappedDictionary::get_UnderlyingDictionary()
// 0x0000022F System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::.ctor(System.Collections.IDictionary)
// 0x00000230 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000231 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Add(TKey,TValue)
// 0x00000232 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::ContainsKey(TKey)
// 0x00000233 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(TKey)
// 0x00000234 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::TryGetValue(TKey,TValue&)
// 0x00000235 System.Collections.Generic.ICollection`1<TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Values()
// 0x00000236 TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Item(TKey)
// 0x00000237 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::set_Item(TKey,TValue)
// 0x00000238 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000239 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Clear()
// 0x0000023A System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000023B System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x0000023C System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Count()
// 0x0000023D System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_IsReadOnly()
// 0x0000023E System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000023F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2::GetEnumerator()
// 0x00000240 System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000241 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.Add(System.Object,System.Object)
// 0x00000242 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.get_Item(System.Object)
// 0x00000243 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.set_Item(System.Object,System.Object)
// 0x00000244 System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.GetEnumerator()
// 0x00000245 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.Contains(System.Object)
// 0x00000246 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(System.Object)
// 0x00000247 System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.get_Values()
// 0x00000248 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000249 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.ICollection.get_SyncRoot()
// 0x0000024A System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_UnderlyingDictionary()
// 0x0000024B System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
// 0x0000024C System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Entry()
// 0x0000024D System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Key()
// 0x0000024E System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Value()
// 0x0000024F System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Current()
// 0x00000250 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::MoveNext()
// 0x00000251 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::Reset()
// 0x00000252 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::.cctor()
// 0x00000253 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::.ctor()
// 0x00000254 System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::<GetEnumerator>b__25_0(System.Collections.DictionaryEntry)
// 0x00000255 Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::get_Instance()
extern void DynamicReflectionDelegateFactory_get_Instance_m95291387B6B34CB4F73C663E9FB5AAD28013245F ();
// 0x00000256 System.Reflection.Emit.DynamicMethod Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateDynamicMethod(System.String,System.Type,System.Type[],System.Type)
extern void DynamicReflectionDelegateFactory_CreateDynamicMethod_m3EB6785FC635B11D15D51D2B0CBDA0E0522D08D6 ();
// 0x00000257 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern void DynamicReflectionDelegateFactory_CreateParameterizedConstructor_m8FDA0F533CD77324D53FA77FE74A5BE9E80BB8A9 ();
// 0x00000258 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x00000259 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateMethodCallIL(System.Reflection.MethodBase,System.Reflection.Emit.ILGenerator,System.Int32)
extern void DynamicReflectionDelegateFactory_GenerateCreateMethodCallIL_mE303F4377E4D81C243A1152D07F74B2FC8993AB7 ();
// 0x0000025A Newtonsoft.Json.Serialization.Func`1<T> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x0000025B System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateDefaultConstructorIL(System.Type,System.Reflection.Emit.ILGenerator,System.Type)
extern void DynamicReflectionDelegateFactory_GenerateCreateDefaultConstructorIL_m30F17C276BB79CB8CBEB78AA3858172D3777A0FC ();
// 0x0000025C Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x0000025D System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateGetPropertyIL(System.Reflection.PropertyInfo,System.Reflection.Emit.ILGenerator)
extern void DynamicReflectionDelegateFactory_GenerateCreateGetPropertyIL_m38A04DBD1306F7B08206A74FD6675079F3525463 ();
// 0x0000025E Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x0000025F System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateGetFieldIL(System.Reflection.FieldInfo,System.Reflection.Emit.ILGenerator)
extern void DynamicReflectionDelegateFactory_GenerateCreateGetFieldIL_m4DB053EAC0498977225576A4DA9070AB09C08B09 ();
// 0x00000260 Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x00000261 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateSetFieldIL(System.Reflection.FieldInfo,System.Reflection.Emit.ILGenerator)
extern void DynamicReflectionDelegateFactory_GenerateCreateSetFieldIL_m07C16C2C44B9E962424D28DC097811C9FC3C7C13 ();
// 0x00000262 Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x00000263 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateSetPropertyIL(System.Reflection.PropertyInfo,System.Reflection.Emit.ILGenerator)
extern void DynamicReflectionDelegateFactory_GenerateCreateSetPropertyIL_m8FB91D5AB6DFBB0097D3279211226E462DF11051 ();
// 0x00000264 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::.ctor()
extern void DynamicReflectionDelegateFactory__ctor_m6C9477E8AE551BBF7FBBE322C353710B3328B0B3 ();
// 0x00000265 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::.cctor()
extern void DynamicReflectionDelegateFactory__cctor_m21DDE69EBD62A3DD74B26CEBF7CB792B14451D66 ();
// 0x00000266 System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory_<>c__DisplayClass11_0`1::.ctor()
// 0x00000267 System.Object Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory_<>c__DisplayClass11_0`1::<CreateGet>b__0(T)
// 0x00000268 System.Void Newtonsoft.Json.Utilities.EnumInfo::.ctor(System.Boolean,System.UInt64[],System.String[],System.String[])
extern void EnumInfo__ctor_m14535EC095DAABAA892466DDCA6C2AA04B9E1979 ();
// 0x00000269 Newtonsoft.Json.Utilities.EnumInfo Newtonsoft.Json.Utilities.EnumUtils::InitializeValuesAndNames(System.Type)
extern void EnumUtils_InitializeValuesAndNames_m7BD33F27800EABDCA2AEFFC8DBA11A7FC0EFC4A9 ();
// 0x0000026A System.Boolean Newtonsoft.Json.Utilities.EnumUtils::TryToString(System.Type,System.Object,System.Boolean,System.String&)
extern void EnumUtils_TryToString_m551684055E6EA97A54FE67F9CC56345DA8944AB5 ();
// 0x0000026B System.String Newtonsoft.Json.Utilities.EnumUtils::InternalFlagsFormat(Newtonsoft.Json.Utilities.EnumInfo,System.UInt64,System.Boolean)
extern void EnumUtils_InternalFlagsFormat_mA3CBA6933178972F361D34F0DD265AAD3B97F2E4 ();
// 0x0000026C Newtonsoft.Json.Utilities.EnumInfo Newtonsoft.Json.Utilities.EnumUtils::GetEnumValuesAndNames(System.Type)
extern void EnumUtils_GetEnumValuesAndNames_m2F73859A109EC7DCCFFF45A08699BC83AE3A57EB ();
// 0x0000026D System.UInt64 Newtonsoft.Json.Utilities.EnumUtils::ToUInt64(System.Object)
extern void EnumUtils_ToUInt64_m8D8949D7F4CDFDB5BDCF39543719A3D998C66446 ();
// 0x0000026E System.Object Newtonsoft.Json.Utilities.EnumUtils::ParseEnum(System.Type,System.String,System.Boolean)
extern void EnumUtils_ParseEnum_m4F6BE2585140D0069B07A7242935068D28EFB6DC ();
// 0x0000026F System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.EnumUtils::MatchName(System.String,System.String[],System.String[],System.Int32,System.Int32,System.StringComparison)
extern void EnumUtils_MatchName_m12C1F723B5DE6ECE45C70FB1AC7C9DBC1864A0BF ();
// 0x00000270 System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.EnumUtils::FindIndexByName(System.String[],System.String,System.Int32,System.Int32,System.StringComparison)
extern void EnumUtils_FindIndexByName_m698B9D2299D907DEC9CB7B8DBBB7A0FB059B3B96 ();
// 0x00000271 System.Void Newtonsoft.Json.Utilities.EnumUtils::.cctor()
extern void EnumUtils__cctor_m199852B7357FAF08F3B070EF448852E948414C0E ();
// 0x00000272 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::PushInstance(System.Reflection.Emit.ILGenerator,System.Type)
extern void ILGeneratorExtensions_PushInstance_mADC3C005F4F69EB47E1431C9F4C49599757EFDCE ();
// 0x00000273 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::PushArrayInstance(System.Reflection.Emit.ILGenerator,System.Int32,System.Int32)
extern void ILGeneratorExtensions_PushArrayInstance_m08C6F9B9FF230A1C164828CA6A1DDBAE78AC213E ();
// 0x00000274 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::BoxIfNeeded(System.Reflection.Emit.ILGenerator,System.Type)
extern void ILGeneratorExtensions_BoxIfNeeded_mBB63555FC24AB1D5057579662F5F474AA34E17E0 ();
// 0x00000275 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::UnboxIfNeeded(System.Reflection.Emit.ILGenerator,System.Type)
extern void ILGeneratorExtensions_UnboxIfNeeded_mBB1374FD971D2FE8BE5B825F0E318E9B0F63B7BD ();
// 0x00000276 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::CallMethod(System.Reflection.Emit.ILGenerator,System.Reflection.MethodInfo)
extern void ILGeneratorExtensions_CallMethod_m4EB6FF3DBC8AEAF337A90B10796133E004B69B93 ();
// 0x00000277 System.Void Newtonsoft.Json.Utilities.ILGeneratorExtensions::Return(System.Reflection.Emit.ILGenerator)
extern void ILGeneratorExtensions_Return_mC6D1CA3AB4A8DB82ABF66EE30F8397D54ED73B5C ();
// 0x00000278 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::TryBuildImmutableForArrayContract(System.Type,System.Type,System.Type&,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>&)
extern void ImmutableCollectionsUtils_TryBuildImmutableForArrayContract_m81A3ACB22C9B84F4335306C352CE046B4FAD284E ();
// 0x00000279 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::TryBuildImmutableForDictionaryContract(System.Type,System.Type,System.Type,System.Type&,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>&)
extern void ImmutableCollectionsUtils_TryBuildImmutableForDictionaryContract_m27EBC590DDE00BE213C3AB4B6307D734E206D089 ();
// 0x0000027A System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::.cctor()
extern void ImmutableCollectionsUtils__cctor_mC46C3C5685050770CCCBA64EF3DDFFEC7683DEDF ();
// 0x0000027B System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::.ctor(System.String,System.String,System.String)
extern void ImmutableCollectionTypeInfo__ctor_mF1EBB7743E6A002B21CE59C303D861471AD2A195 ();
// 0x0000027C System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_ContractTypeName()
extern void ImmutableCollectionTypeInfo_get_ContractTypeName_m0D5E4B609C011E897FAE6F6C47B0502F56BD5635 ();
// 0x0000027D System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_ContractTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_ContractTypeName_mEB75E6E71DFB1BEA2C3F87F871FD96055035198E ();
// 0x0000027E System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_CreatedTypeName()
extern void ImmutableCollectionTypeInfo_get_CreatedTypeName_m2CAC914D610A537B788A82D974A397D4E7CE73AE ();
// 0x0000027F System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_CreatedTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_CreatedTypeName_m8EE937465C5EBDD89E53A27175997762DC409432 ();
// 0x00000280 System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_BuilderTypeName()
extern void ImmutableCollectionTypeInfo_get_BuilderTypeName_m4283937E6AC55266696DCFC0E57A382018E48B6D ();
// 0x00000281 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_BuilderTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_BuilderTypeName_mB3F1C174DE747A6522B54B314511E680EF9CCB0D ();
// 0x00000282 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m8692CCC41C38ECDB8B8AF27AA3BD97E5A80B5C5A ();
// 0x00000283 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0::<TryBuildImmutableForArrayContract>b__0(Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo)
extern void U3CU3Ec__DisplayClass24_0_U3CTryBuildImmutableForArrayContractU3Eb__0_m419CAD460910D17E5761A4AC3C894C1800C239C7 ();
// 0x00000284 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::.cctor()
extern void U3CU3Ec__cctor_m5EB0EA7E47B0766F4CBBA49E52BC1CDA337F3E25 ();
// 0x00000285 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::.ctor()
extern void U3CU3Ec__ctor_mE1A296AD7C06D6A5A41BEFEE471F7820BD23BE07 ();
// 0x00000286 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<TryBuildImmutableForArrayContract>b__24_1(System.Reflection.MethodInfo)
extern void U3CU3Ec_U3CTryBuildImmutableForArrayContractU3Eb__24_1_m56CA826DCEE7572D015F4023C39FB9A2ACF991C0 ();
// 0x00000287 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<TryBuildImmutableForDictionaryContract>b__25_1(System.Reflection.MethodInfo)
extern void U3CU3Ec_U3CTryBuildImmutableForDictionaryContractU3Eb__25_1_m957911CD1C97CE609DB7C23025177FBD036D8617 ();
// 0x00000288 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m79E4006A3FBB69367152145E78F8C8902A4E1643 ();
// 0x00000289 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0::<TryBuildImmutableForDictionaryContract>b__0(Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo)
extern void U3CU3Ec__DisplayClass25_0_U3CTryBuildImmutableForDictionaryContractU3Eb__0_mE023CD0E6B4C032086918D6D3C531B713BB5A440 ();
// 0x0000028A System.Char[] Newtonsoft.Json.Utilities.BufferUtils::RentBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void BufferUtils_RentBuffer_m30FA4797B8C91B658B4F018965C36DC83AA948FE ();
// 0x0000028B System.Void Newtonsoft.Json.Utilities.BufferUtils::ReturnBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[])
extern void BufferUtils_ReturnBuffer_mED00AD7AB01720B97653280A0AD83F0A1E97548C ();
// 0x0000028C System.Char[] Newtonsoft.Json.Utilities.BufferUtils::EnsureBufferSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32,System.Char[])
extern void BufferUtils_EnsureBufferSize_m9A5B7B4E4DF71AA8F2F651CC56A554B438E67EEA ();
// 0x0000028D System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::.cctor()
extern void JavaScriptUtils__cctor_mDC0BF179E7A0A6A924A44A34D391CE40A9881690 ();
// 0x0000028E System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::GetCharEscapeFlags(Newtonsoft.Json.StringEscapeHandling,System.Char)
extern void JavaScriptUtils_GetCharEscapeFlags_m2C28E314173C145E82B48373B5F93BD8B467A976 ();
// 0x0000028F System.Boolean Newtonsoft.Json.Utilities.JavaScriptUtils::ShouldEscapeJavaScriptString(System.String,System.Boolean[])
extern void JavaScriptUtils_ShouldEscapeJavaScriptString_m351E12F2D532ABEC9F83D85C446B8827AC54F463 ();
// 0x00000290 System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::WriteEscapedJavaScriptString(System.IO.TextWriter,System.String,System.Char,System.Boolean,System.Boolean[],Newtonsoft.Json.StringEscapeHandling,Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[]&)
extern void JavaScriptUtils_WriteEscapedJavaScriptString_m0849BCD2BFE93613932C0D22A9D0117D6BF12E00 ();
// 0x00000291 System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ToEscapedJavaScriptString(System.String,System.Char,System.Boolean,Newtonsoft.Json.StringEscapeHandling)
extern void JavaScriptUtils_ToEscapedJavaScriptString_m7A0B5E200B4C9BE6C8734233E75E85E5435FB5C8 ();
// 0x00000292 System.Int32 Newtonsoft.Json.Utilities.JavaScriptUtils::FirstCharToEscape(System.String,System.Boolean[],Newtonsoft.Json.StringEscapeHandling)
extern void JavaScriptUtils_FirstCharToEscape_m42DF3535F36DA933E8458C65C7B7A752A5EB4D3E ();
// 0x00000293 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsEndToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsEndToken_mC167BF200227D5CE769C7F458326FA49B8BEADDD ();
// 0x00000294 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsStartToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsStartToken_m59C812E9C9992DAC3C482EEF0C90BFA391CAC543 ();
// 0x00000295 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsPrimitiveToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsPrimitiveToken_m0EE19C28ABB2C9BFEB8494CBA079CC2EC2F6B101 ();
// 0x00000296 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::get_Instance()
extern void LateBoundReflectionDelegateFactory_get_Instance_mEC11C354F50176619C1C82AE633EAE27BE2AF916 ();
// 0x00000297 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern void LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m5199397D2C6AB1DB11944E9B6335AF9F18806B19 ();
// 0x00000298 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x00000299 Newtonsoft.Json.Serialization.Func`1<T> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x0000029A Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x0000029B Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x0000029C Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x0000029D Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x0000029E System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.ctor()
extern void LateBoundReflectionDelegateFactory__ctor_mB27586C750F98D3A9ADA7589F5CACE9C9217F526 ();
// 0x0000029F System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.cctor()
extern void LateBoundReflectionDelegateFactory__cctor_m78B66BE4139C12B9958AB966FAD1570451E13AA6 ();
// 0x000002A0 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m390EFB75E27606B1E85FAD9E95D3343D52A44EF1 ();
// 0x000002A1 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m9E2DECEC34D0CA6F0CE807CAE4AB07C35643917E ();
// 0x000002A2 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__1(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_m3CCE2D05306D21B9247C422FFE49882AB909CA99 ();
// 0x000002A3 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::.ctor()
// 0x000002A4 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::<CreateMethodCall>b__0(T,System.Object[])
// 0x000002A5 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::<CreateMethodCall>b__1(T,System.Object[])
// 0x000002A6 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::.ctor()
// 0x000002A7 T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__0()
// 0x000002A8 T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__1()
// 0x000002A9 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass6_0`1::.ctor()
// 0x000002AA System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass6_0`1::<CreateGet>b__0(T)
// 0x000002AB System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass7_0`1::.ctor()
// 0x000002AC System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass7_0`1::<CreateGet>b__0(T)
// 0x000002AD System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass8_0`1::.ctor()
// 0x000002AE System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass8_0`1::<CreateSet>b__0(T,System.Object)
// 0x000002AF System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass9_0`1::.ctor()
// 0x000002B0 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass9_0`1::<CreateSet>b__0(T,System.Object)
// 0x000002B1 System.Int32 Newtonsoft.Json.Utilities.MathUtils::IntLength(System.UInt64)
extern void MathUtils_IntLength_m67D092EFAFF46BD4A67DB420EC27C6030D586823 ();
// 0x000002B2 System.Char Newtonsoft.Json.Utilities.MathUtils::IntToHex(System.Int32)
extern void MathUtils_IntToHex_mAAC0332066D03A3B0A68C0571002A47D605DBCBE ();
// 0x000002B3 System.Boolean Newtonsoft.Json.Utilities.MathUtils::ApproxEquals(System.Double,System.Double)
extern void MathUtils_ApproxEquals_mD9D25E78C742A44C398C77DC44EF680962F00B9B ();
// 0x000002B4 System.Void Newtonsoft.Json.Utilities.MethodCall`2::.ctor(System.Object,System.IntPtr)
// 0x000002B5 TResult Newtonsoft.Json.Utilities.MethodCall`2::Invoke(T,System.Object[])
// 0x000002B6 System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
// 0x000002B7 TResult Newtonsoft.Json.Utilities.MethodCall`2::EndInvoke(System.IAsyncResult)
// 0x000002B8 System.Boolean Newtonsoft.Json.Utilities.MiscellaneousUtils::ValueEquals(System.Object,System.Object)
extern void MiscellaneousUtils_ValueEquals_m449C562930BCA7EAC9A575BBF8E070587A3DD0E6 ();
// 0x000002B9 System.ArgumentOutOfRangeException Newtonsoft.Json.Utilities.MiscellaneousUtils::CreateArgumentOutOfRangeException(System.String,System.Object,System.String)
extern void MiscellaneousUtils_CreateArgumentOutOfRangeException_mCF8F5AA0C6E8ADB6D3A6CD72E4D1965905BC6522 ();
// 0x000002BA System.Int32 Newtonsoft.Json.Utilities.MiscellaneousUtils::ByteArrayCompare(System.Byte[],System.Byte[])
extern void MiscellaneousUtils_ByteArrayCompare_m2BB9EE4CB13E82D9282FB0DF93E5A3A979912D12 ();
// 0x000002BB System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::GetPrefix(System.String)
extern void MiscellaneousUtils_GetPrefix_mC375B32C59807F6714D93F13537EB6D04E308C67 ();
// 0x000002BC System.Void Newtonsoft.Json.Utilities.MiscellaneousUtils::GetQualifiedNameParts(System.String,System.String&,System.String&)
extern void MiscellaneousUtils_GetQualifiedNameParts_mB81F6B7C02BA7F17CE91E176F97B071721350035 ();
// 0x000002BD System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::FormatValueForPrint(System.Object)
extern void MiscellaneousUtils_FormatValueForPrint_m6A35F05B228397ACB2FEF254079C5D834A4C913D ();
// 0x000002BE System.Text.RegularExpressions.RegexOptions Newtonsoft.Json.Utilities.MiscellaneousUtils::GetRegexOptions(System.String)
extern void MiscellaneousUtils_GetRegexOptions_mC4320586C2D4FCA5E5A2E6089BA32D3F75F099FF ();
// 0x000002BF System.Void Newtonsoft.Json.Utilities.PropertyNameTable::.cctor()
extern void PropertyNameTable__cctor_mAED8DBA88CCA0E72E1F25C9D6005C67963D5F5B0 ();
// 0x000002C0 System.Void Newtonsoft.Json.Utilities.PropertyNameTable::.ctor()
extern void PropertyNameTable__ctor_m551F4B7013046ADDB625BC5F6582EFD1D7A124F2 ();
// 0x000002C1 System.String Newtonsoft.Json.Utilities.PropertyNameTable::Get(System.Char[],System.Int32,System.Int32)
extern void PropertyNameTable_Get_mDAA768129FDE9D79091D1E3AA90DA7F0B75ED395 ();
// 0x000002C2 System.String Newtonsoft.Json.Utilities.PropertyNameTable::Add(System.String)
extern void PropertyNameTable_Add_m47407E2B92D5C18BD58876B749E7BC6DEA742B7B ();
// 0x000002C3 System.String Newtonsoft.Json.Utilities.PropertyNameTable::AddEntry(System.String,System.Int32)
extern void PropertyNameTable_AddEntry_m951FED0ED5304E4AC256B104183B79D8E381A469 ();
// 0x000002C4 System.Void Newtonsoft.Json.Utilities.PropertyNameTable::Grow()
extern void PropertyNameTable_Grow_mDC7B3FDD166249D6626C9153A3F018DBF77FC569 ();
// 0x000002C5 System.Boolean Newtonsoft.Json.Utilities.PropertyNameTable::TextEquals(System.String,System.Char[],System.Int32,System.Int32)
extern void PropertyNameTable_TextEquals_mA3CF7C8751FBC447EDF1F5ED6F1841FD5681CE9F ();
// 0x000002C6 System.Void Newtonsoft.Json.Utilities.PropertyNameTable_Entry::.ctor(System.String,System.Int32,Newtonsoft.Json.Utilities.PropertyNameTable_Entry)
extern void Entry__ctor_mA2149BC597D4FCBFC2E57A2293513B552BB63D99 ();
// 0x000002C7 Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.MemberInfo)
// 0x000002C8 Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.MemberInfo)
// 0x000002C9 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x000002CA Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
// 0x000002CB Newtonsoft.Json.Serialization.Func`1<T> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x000002CC Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x000002CD Newtonsoft.Json.Serialization.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x000002CE Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x000002CF Newtonsoft.Json.Serialization.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x000002D0 System.Void Newtonsoft.Json.Utilities.ReflectionDelegateFactory::.ctor()
extern void ReflectionDelegateFactory__ctor_mC9ADB12BECE662F977AFABB5B285150D54902E85 ();
// 0x000002D1 System.Type Newtonsoft.Json.Utilities.ReflectionMember::get_MemberType()
extern void ReflectionMember_get_MemberType_mDB7835DC3BA9236241EB77743EA997B7761487BD ();
// 0x000002D2 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_MemberType(System.Type)
extern void ReflectionMember_set_MemberType_mF057603A8EC1BF604016C92808ECC5C21221774E ();
// 0x000002D3 Newtonsoft.Json.Serialization.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::get_Getter()
extern void ReflectionMember_get_Getter_mFEB3405C8B2C642BF5E2328D6CA418C290F4D7A4 ();
// 0x000002D4 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Getter(Newtonsoft.Json.Serialization.Func`2<System.Object,System.Object>)
extern void ReflectionMember_set_Getter_m624FF829420D4F5BAC8363E30D03A65372E37C0D ();
// 0x000002D5 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Setter(Newtonsoft.Json.Serialization.Action`2<System.Object,System.Object>)
extern void ReflectionMember_set_Setter_m6577CDEDFF2B98E99D7CEF673C9127B052783ADB ();
// 0x000002D6 System.Void Newtonsoft.Json.Utilities.ReflectionMember::.ctor()
extern void ReflectionMember__ctor_mF7F23A185AAC6D80D004D8115EA45E9E31EC5EB9 ();
// 0x000002D7 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::get_Creator()
extern void ReflectionObject_get_Creator_m70A42D72CC73787F5947E94A194C042C156F4EBC ();
// 0x000002D8 System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::get_Members()
extern void ReflectionObject_get_Members_m7C6354CE99FF725E1C32D5048853D305650B911C ();
// 0x000002D9 System.Void Newtonsoft.Json.Utilities.ReflectionObject::.ctor(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void ReflectionObject__ctor_m53709028AE17BEA9E037A8D69E486604BF64D0EA ();
// 0x000002DA System.Object Newtonsoft.Json.Utilities.ReflectionObject::GetValue(System.Object,System.String)
extern void ReflectionObject_GetValue_m704071723EF49EA1AF319125303194A021027454 ();
// 0x000002DB System.Type Newtonsoft.Json.Utilities.ReflectionObject::GetType(System.String)
extern void ReflectionObject_GetType_m786A921833EADAD4F9E323109BD837D90F8C9D3A ();
// 0x000002DC Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.Reflection.MethodBase,System.String[])
extern void ReflectionObject_Create_mFCDEDAFB543CBD5AC486281E68B708A23084ACCB ();
// 0x000002DD System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mCBBAC22D9A5A1260F6086E1620BF505B6B0D4C2E ();
// 0x000002DE System.Object Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0::<Create>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass11_0_U3CCreateU3Eb__0_m429E22D5256F1D86AD46809AC02D13465A1CB248 ();
// 0x000002DF System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1::.ctor()
extern void U3CU3Ec__DisplayClass11_1__ctor_m8158889AE32830C7D291C257E99F76AB345E4D20 ();
// 0x000002E0 System.Object Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1::<Create>b__1(System.Object)
extern void U3CU3Ec__DisplayClass11_1_U3CCreateU3Eb__1_m81E9B80D1E6217CFEEB69CE5A3FCD87C8ED109A0 ();
// 0x000002E1 System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2::.ctor()
extern void U3CU3Ec__DisplayClass11_2__ctor_m35ECF94E7D27EFA46499F1214E5CA214CDD9EB98 ();
// 0x000002E2 System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2::<Create>b__2(System.Object,System.Object)
extern void U3CU3Ec__DisplayClass11_2_U3CCreateU3Eb__2_mEC684468313045ED83223C839BA09C0E50E55325 ();
// 0x000002E3 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::.cctor()
extern void ReflectionUtils__cctor_m0451BD7C156F6CF157F4C37BB418768A3A4BE093 ();
// 0x000002E4 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsVirtual(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsVirtual_m63B646B725C4183E021CE56970CA1D60D8B58A24 ();
// 0x000002E5 System.Reflection.MethodInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetBaseDefinition(System.Reflection.PropertyInfo)
extern void ReflectionUtils_GetBaseDefinition_mFA5FCF18125DAC20BE8CA9ADB3731FDF83A1E026 ();
// 0x000002E6 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsPublic(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsPublic_m32527AA45845F6A52280996D99555A5C4ECED23E ();
// 0x000002E7 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetObjectType(System.Object)
extern void ReflectionUtils_GetObjectType_mBD999995158E5ECC6A20BB1DD14AB15F9ED43E76 ();
// 0x000002E8 System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetTypeName(System.Type,Newtonsoft.Json.TypeNameAssemblyFormatHandling,Newtonsoft.Json.Serialization.ISerializationBinder)
extern void ReflectionUtils_GetTypeName_mC9E70D8E105DC9D994989D6AF55815010AF22BE4 ();
// 0x000002E9 System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetFullyQualifiedTypeName(System.Type,Newtonsoft.Json.Serialization.ISerializationBinder)
extern void ReflectionUtils_GetFullyQualifiedTypeName_m25D33D676F373D3E85FC8AB611A8E95C159FDB25 ();
// 0x000002EA System.String Newtonsoft.Json.Utilities.ReflectionUtils::RemoveAssemblyDetails(System.String)
extern void ReflectionUtils_RemoveAssemblyDetails_mE771D0C2FC98335B2318C6CBD2F1F51B51BE7242 ();
// 0x000002EB System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::HasDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_HasDefaultConstructor_mD2D4561252CF3D3E87592DD7CA6186DB3D81FA5A ();
// 0x000002EC System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type)
extern void ReflectionUtils_GetDefaultConstructor_mB63FE0AD6CA7980E2C393FD16C32349D13964149 ();
// 0x000002ED System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_GetDefaultConstructor_m62A502EE983ADB01FBE1B62707931E995FAD9044 ();
// 0x000002EE System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullable(System.Type)
extern void ReflectionUtils_IsNullable_mBD35C2D4F04415D3B28740CA753886A74F228E41 ();
// 0x000002EF System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullableType(System.Type)
extern void ReflectionUtils_IsNullableType_m190D5C00707C561FECB5C54EDA2970765016EA67 ();
// 0x000002F0 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::EnsureNotNullableType(System.Type)
extern void ReflectionUtils_EnsureNotNullableType_m35D18D864F83693924DA3358963AFEAF58F1967E ();
// 0x000002F1 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_IsGenericDefinition_mBA65CFC1AD852377383C7A37EA3707EA293D8B10 ();
// 0x000002F2 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_ImplementsGenericDefinition_m21865858B0606AE227C69EBF962CEEFA2C61D1C1 ();
// 0x000002F3 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_ImplementsGenericDefinition_m956BE177FA0D5ADB10F78820F885C47136971E2D ();
// 0x000002F4 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_InheritsGenericDefinition_m26FC0C9D6703ED7CE6014106CB9AA707955A79A7 ();
// 0x000002F5 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinition_mA48A6276C77A1D0326B4E3BE79A8EB237005FF79 ();
// 0x000002F6 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinitionInternal(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinitionInternal_m5DAAEB9A5C744B3E9B838EFEDB1C585843FD3089 ();
// 0x000002F7 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetCollectionItemType(System.Type)
extern void ReflectionUtils_GetCollectionItemType_mC5FD99ED17734012701AB1B3F145560825DE7E35 ();
// 0x000002F8 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryKeyValueTypes(System.Type,System.Type&,System.Type&)
extern void ReflectionUtils_GetDictionaryKeyValueTypes_m3FB4EB66B8D1BE1DFA6D28D325C9D9888AB5B75C ();
// 0x000002F9 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberUnderlyingType_mE824A7CF6CA344EE3D52516077CCC623CEB27452 ();
// 0x000002FA System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.MemberInfo)
extern void ReflectionUtils_IsIndexedProperty_m11926E8DE9A89EDAED070B03E43F2F10CAC634BD ();
// 0x000002FB System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsIndexedProperty_mCFB17E4BC2968A8222652D6AE3D36BAE1C86C477 ();
// 0x000002FC System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void ReflectionUtils_GetMemberValue_mA73290834FDFE9DB888D51B4A242D43474A3A38C ();
// 0x000002FD System.Void Newtonsoft.Json.Utilities.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void ReflectionUtils_SetMemberValue_m15C10E14BBBF301F9D8D24ABB5B6A34FD66069F6 ();
// 0x000002FE System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo,System.Boolean)
extern void ReflectionUtils_CanReadMemberValue_m8FDC113AC7DC7C9B2DF1D7593576FD9BE80574FA ();
// 0x000002FF System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo,System.Boolean,System.Boolean)
extern void ReflectionUtils_CanSetMemberValue_m3E52E8863D3DC4AAD68442AE4229F7A799A0D708 ();
// 0x00000300 System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFieldsAndProperties_mD00FD8BF55EE5F1DD3D62F549A21C3EFA677F2DB ();
// 0x00000301 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsOverridenGenericMember(System.Reflection.MemberInfo,System.Reflection.BindingFlags)
extern void ReflectionUtils_IsOverridenGenericMember_m406BE64FC04713D31BC52292D94CD76109B7FAF8 ();
// 0x00000302 T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object)
// 0x00000303 T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object,System.Boolean)
// 0x00000304 T[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Boolean)
// 0x00000305 System.Attribute[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Type,System.Boolean)
extern void ReflectionUtils_GetAttributes_mF3FAB9BAD2DE66EDFD352476774CFB2114FB7980 ();
// 0x00000306 Newtonsoft.Json.Utilities.TypeNameKey Newtonsoft.Json.Utilities.ReflectionUtils::SplitFullyQualifiedTypeName(System.String)
extern void ReflectionUtils_SplitFullyQualifiedTypeName_m7CA0C210F96D8888C1EDE468FA47011F0A4787AB ();
// 0x00000307 System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.ReflectionUtils::GetAssemblyDelimiterIndex(System.String)
extern void ReflectionUtils_GetAssemblyDelimiterIndex_m623A7BDB7B25AB1B4EE7BD7619EA358B7AA5D330 ();
// 0x00000308 System.Reflection.MemberInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberInfoFromType(System.Type,System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberInfoFromType_mC8D662479BDBF9778EA95A0E4686EFD981C0A03C ();
// 0x00000309 System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFields(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFields_mC7BBB66C8578C8BE30E3F8A73C6D17736DC97D80 ();
// 0x0000030A System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateFields(System.Collections.Generic.IList`1<System.Reflection.MemberInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateFields_mC930BB29828AA19293D2BF27BCA4532AAE0292E9 ();
// 0x0000030B System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetProperties_m8629EE5D0B04946663A3EE97948C7DFD3F7D7EEE ();
// 0x0000030C System.Reflection.BindingFlags Newtonsoft.Json.Utilities.ReflectionUtils::RemoveFlag(System.Reflection.BindingFlags,System.Reflection.BindingFlags)
extern void ReflectionUtils_RemoveFlag_m1100A917CDA3891ACF2AC949DB1AFDCB4B5E06FA ();
// 0x0000030D System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateProperties(System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateProperties_m0B08F9719D18F5843549B0440D67A53F992D0B08 ();
// 0x0000030E System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultValue(System.Type)
extern void ReflectionUtils_GetDefaultValue_m4BCD103E40969465A210E08FD789DC44A1B518B0 ();
// 0x0000030F System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c::.cctor()
extern void U3CU3Ec__cctor_m3709607BD37BA4073ADA364B10993D9E7B8C6205 ();
// 0x00000310 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c::.ctor()
extern void U3CU3Ec__ctor_mEB328C14C51205D9EA306DD6FCFB8180F4070E01 ();
// 0x00000311 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetDefaultConstructor>b__11_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetDefaultConstructorU3Eb__11_0_m96A5887B345267C96AB575E2E2633CF7F6F4CEE8 ();
// 0x00000312 System.String Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetFieldsAndProperties>b__30_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__30_0_mA0005323FF603950643C8B2B454412EF27AC5D8A ();
// 0x00000313 System.Type Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetMemberInfoFromType>b__38_0(System.Reflection.ParameterInfo)
extern void U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__38_0_m566B3369A66348CBEF765BE92D2BF784C596CFFC ();
// 0x00000314 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetChildPrivateFields>b__40_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__40_0_m8267AEBE4AA31118BFD6FC2892E117CC07EC7D02 ();
// 0x00000315 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m3C54273EE03E4E56B5227258EEA32DFB5E3F2501 ();
// 0x00000316 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass30_0::<GetFieldsAndProperties>b__1(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass30_0_U3CGetFieldsAndPropertiesU3Eb__1_m60E896A814DA21EDBD8720846A22FBFC48430F96 ();
// 0x00000317 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m3A5D5F46B82672984A690F68DFFC0C39AAEA5C8D ();
// 0x00000318 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0::<GetChildPrivateProperties>b__0(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass43_0_U3CGetChildPrivatePropertiesU3Eb__0_mB182F85C5416D038D2B587781729D8624AA0F509 ();
// 0x00000319 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0::<GetChildPrivateProperties>b__1(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass43_0_U3CGetChildPrivatePropertiesU3Eb__1_mCE3E8E1BA8E3B47F5DF4A15221D7E65D9FDDA216 ();
// 0x0000031A System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_1::.ctor()
extern void U3CU3Ec__DisplayClass43_1__ctor_m1E3B227ADF572B9AE8EDF695E714CD6E192425F8 ();
// 0x0000031B System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_1::<GetChildPrivateProperties>b__2(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass43_1_U3CGetChildPrivatePropertiesU3Eb__2_m556D6BF375B2CDB5531CD3BB9ED883EE6ED4DFF2 ();
// 0x0000031C System.Void Newtonsoft.Json.Utilities.TypeNameKey::.ctor(System.String,System.String)
extern void TypeNameKey__ctor_mF3E794056C4B398E504D5A5FE2FBC6AF5C14BB3F_AdjustorThunk ();
// 0x0000031D System.Int32 Newtonsoft.Json.Utilities.TypeNameKey::GetHashCode()
extern void TypeNameKey_GetHashCode_m0F67AAE26FB0AC8513608789BF7457FB938CFC2B_AdjustorThunk ();
// 0x0000031E System.Boolean Newtonsoft.Json.Utilities.TypeNameKey::Equals(System.Object)
extern void TypeNameKey_Equals_mFBA09733674F70F86DE687549E683E074738F16C_AdjustorThunk ();
// 0x0000031F System.Boolean Newtonsoft.Json.Utilities.TypeNameKey::Equals(Newtonsoft.Json.Utilities.TypeNameKey)
extern void TypeNameKey_Equals_m5E04FEBCFF1AF599AA9688FAFA09118BFB221617_AdjustorThunk ();
// 0x00000320 System.Int32 Newtonsoft.Json.Utilities.StringBuffer::get_Position()
extern void StringBuffer_get_Position_m488425AE9C848AE1B6FC1166591E37FC23345F1C_AdjustorThunk ();
// 0x00000321 System.Void Newtonsoft.Json.Utilities.StringBuffer::set_Position(System.Int32)
extern void StringBuffer_set_Position_m5D376FC4AC916F5E0D14B163503F3B37C0E59797_AdjustorThunk ();
// 0x00000322 System.Boolean Newtonsoft.Json.Utilities.StringBuffer::get_IsEmpty()
extern void StringBuffer_get_IsEmpty_m926D266B656A76A95454D825EA379C587BB6CF53_AdjustorThunk ();
// 0x00000323 System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer__ctor_m800C4774C1053B3C987BAAADDA2A2A4E3905EE33_AdjustorThunk ();
// 0x00000324 System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(System.Char[])
extern void StringBuffer__ctor_m32FA61DF2F4B540B6F98D4056DA0E56DFC7ADC88_AdjustorThunk ();
// 0x00000325 System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char)
extern void StringBuffer_Append_m43C8A20A6470CCB7E14D7C44778920D5456BCC23_AdjustorThunk ();
// 0x00000326 System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[],System.Int32,System.Int32)
extern void StringBuffer_Append_mA728EFC8C80B11E5AC542967748F5A703BA595A1_AdjustorThunk ();
// 0x00000327 System.Void Newtonsoft.Json.Utilities.StringBuffer::Clear(Newtonsoft.Json.IArrayPool`1<System.Char>)
extern void StringBuffer_Clear_m8E58F9B101AB268484B9C8C1732CE36BFA3C91CB_AdjustorThunk ();
// 0x00000328 System.Void Newtonsoft.Json.Utilities.StringBuffer::EnsureSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer_EnsureSize_m97AAE254F53E3EAC614D62199D2712033A69AEA8_AdjustorThunk ();
// 0x00000329 System.String Newtonsoft.Json.Utilities.StringBuffer::ToString()
extern void StringBuffer_ToString_mBBF4FA9D950121B175F11CA71F5471EF7A6BD2BD_AdjustorThunk ();
// 0x0000032A System.String Newtonsoft.Json.Utilities.StringBuffer::ToString(System.Int32,System.Int32)
extern void StringBuffer_ToString_m4C236F628D94A6881FFB5739293BBDEAA94D9841_AdjustorThunk ();
// 0x0000032B System.Char[] Newtonsoft.Json.Utilities.StringBuffer::get_InternalBuffer()
extern void StringBuffer_get_InternalBuffer_m2ECD780B7AFECDB39C784EA63B66DEFCD5F56D41_AdjustorThunk ();
// 0x0000032C System.Char Newtonsoft.Json.Utilities.StringReference::get_Item(System.Int32)
extern void StringReference_get_Item_mD51926C1F89AB5CDC5DD2BA3EE74B0AD8E699ED4_AdjustorThunk ();
// 0x0000032D System.Char[] Newtonsoft.Json.Utilities.StringReference::get_Chars()
extern void StringReference_get_Chars_m7668344466F173B57588F0D272F4D89B7EDFB81A_AdjustorThunk ();
// 0x0000032E System.Int32 Newtonsoft.Json.Utilities.StringReference::get_StartIndex()
extern void StringReference_get_StartIndex_m56C5C1EA9AE2EFBA4EC53F6A319D8D7F6F68EE27_AdjustorThunk ();
// 0x0000032F System.Int32 Newtonsoft.Json.Utilities.StringReference::get_Length()
extern void StringReference_get_Length_mBCE16FC3D03133692B8AAA70AE195DFB3DE6F408_AdjustorThunk ();
// 0x00000330 System.Void Newtonsoft.Json.Utilities.StringReference::.ctor(System.Char[],System.Int32,System.Int32)
extern void StringReference__ctor_m770FE9F149443A8EB199E60CAB6D6F710DD52A72_AdjustorThunk ();
// 0x00000331 System.String Newtonsoft.Json.Utilities.StringReference::ToString()
extern void StringReference_ToString_m78957CA1DAC727D32B0D96DFD96E35B3F6DD7ED9_AdjustorThunk ();
// 0x00000332 System.Int32 Newtonsoft.Json.Utilities.StringReferenceExtensions::IndexOf(Newtonsoft.Json.Utilities.StringReference,System.Char,System.Int32,System.Int32)
extern void StringReferenceExtensions_IndexOf_mA4BBEC4B1A7CB4447F0B14BF08DE69C1A9903935 ();
// 0x00000333 System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::StartsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_StartsWith_m9A5D5A869C4AAC862436DA728992BB33C07CC554 ();
// 0x00000334 System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::EndsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_EndsWith_m9CF06FE296692CF1AD35F90F2937561102468820 ();
// 0x00000335 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object)
extern void StringUtils_FormatWith_m0E88F3B87F5D71C8BF7FF7F8AE9AB1020A056D83 ();
// 0x00000336 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object)
extern void StringUtils_FormatWith_m925E08E842B2405C7C7E6F21F728463C53AF7EEC ();
// 0x00000337 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_m257EE6C19CB071121BDD6DBDDA6ECB2D42A85745 ();
// 0x00000338 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_mBF018F864F41F7AE9B4EE3DE3368836AD3DA24C2 ();
// 0x00000339 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object[])
extern void StringUtils_FormatWith_m2396D684ED00A0583EF4F3C87B2696558F59E9E8 ();
// 0x0000033A System.IO.StringWriter Newtonsoft.Json.Utilities.StringUtils::CreateStringWriter(System.Int32)
extern void StringUtils_CreateStringWriter_m0512DB7DD95DF6B3A5743AA8A39D6BBDD01F55AC ();
// 0x0000033B System.Void Newtonsoft.Json.Utilities.StringUtils::ToCharAsUnicode(System.Char,System.Char[])
extern void StringUtils_ToCharAsUnicode_mC3A49C9A3AEB61ADDFF81C1D316457E586200AF4 ();
// 0x0000033C TSource Newtonsoft.Json.Utilities.StringUtils::ForgivingCaseSensitiveFind(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.String>,System.String)
// 0x0000033D System.String Newtonsoft.Json.Utilities.StringUtils::ToCamelCase(System.String)
extern void StringUtils_ToCamelCase_mF386619B170EBA1D2EA3F8DE91EF2770F653C5EB ();
// 0x0000033E System.Char Newtonsoft.Json.Utilities.StringUtils::ToLower(System.Char)
extern void StringUtils_ToLower_m53CFAE39CB886E09B7E3BC65B1EDFB18B0017368 ();
// 0x0000033F System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsHighSurrogate(System.Char)
extern void StringUtils_IsHighSurrogate_mC21B3F6940910CA309D84D7FCA809434CCD2C63F ();
// 0x00000340 System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsLowSurrogate(System.Char)
extern void StringUtils_IsLowSurrogate_mF0599E63827C5C6D42C7731DAE4F6E4B02CE6A87 ();
// 0x00000341 System.Boolean Newtonsoft.Json.Utilities.StringUtils::StartsWith(System.String,System.Char)
extern void StringUtils_StartsWith_m9A6B4A2611084EA41845426D6DBE4329C0C4DBAE ();
// 0x00000342 System.Boolean Newtonsoft.Json.Utilities.StringUtils::EndsWith(System.String,System.Char)
extern void StringUtils_EndsWith_m688679CF80F7D6ED0D8BFE4722EC34F8983D4DDD ();
// 0x00000343 System.String Newtonsoft.Json.Utilities.StringUtils::Trim(System.String,System.Int32,System.Int32)
extern void StringUtils_Trim_m83F9669E0914674571A0EB0625247338F30B20DC ();
// 0x00000344 System.Void Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass13_0`1::.ctor()
// 0x00000345 System.Boolean Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass13_0`1::<ForgivingCaseSensitiveFind>b__0(TSource)
// 0x00000346 System.Boolean Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass13_0`1::<ForgivingCaseSensitiveFind>b__1(TSource)
// 0x00000347 System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2::.ctor(Newtonsoft.Json.Serialization.Func`2<TKey,TValue>)
// 0x00000348 TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2::Get(TKey)
// 0x00000349 TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2::AddValue(TKey)
// 0x0000034A System.Reflection.MemberTypes Newtonsoft.Json.Utilities.TypeExtensions::MemberType(System.Reflection.MemberInfo)
extern void TypeExtensions_MemberType_m9CF5B1EF1F8684D1F1064EAF324A8D6A86827AA3 ();
// 0x0000034B System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ContainsGenericParameters(System.Type)
extern void TypeExtensions_ContainsGenericParameters_mFC3F48331F28D3AA46A711B2CFB6990B2BDC7020 ();
// 0x0000034C System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsInterface(System.Type)
extern void TypeExtensions_IsInterface_m929BAA3EF8A9CAA2612740DCD69E86438DC6E527 ();
// 0x0000034D System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericType(System.Type)
extern void TypeExtensions_IsGenericType_mBC936719A611E7EA7A7F106522C5CF2BDABAE3C4 ();
// 0x0000034E System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericTypeDefinition(System.Type)
extern void TypeExtensions_IsGenericTypeDefinition_mF6B17FD568133CD842D9B30643DA140A84901277 ();
// 0x0000034F System.Type Newtonsoft.Json.Utilities.TypeExtensions::BaseType(System.Type)
extern void TypeExtensions_BaseType_m38FA9CEA11834CF5AC2D1216C9EE0FCB03FF8453 ();
// 0x00000350 System.Reflection.Assembly Newtonsoft.Json.Utilities.TypeExtensions::Assembly(System.Type)
extern void TypeExtensions_Assembly_m136B62BD0FDBDB6DB8130754D4CC94F38CE1AA40 ();
// 0x00000351 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsEnum(System.Type)
extern void TypeExtensions_IsEnum_mE860224904C6BCC92B32C8A36DAE461652DE9451 ();
// 0x00000352 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsClass(System.Type)
extern void TypeExtensions_IsClass_m9171AF673A6D294CD4FB8BFB20B5CEBF3590419C ();
// 0x00000353 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsSealed(System.Type)
extern void TypeExtensions_IsSealed_m4AE126033A7929B1B927166A9C232F238080A7C4 ();
// 0x00000354 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsAbstract(System.Type)
extern void TypeExtensions_IsAbstract_m91F4D2EF03EAFA37AE9D41247D6498395E2CC6F0 ();
// 0x00000355 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsValueType(System.Type)
extern void TypeExtensions_IsValueType_m7FD48105A70DFA4590FBEAEF999DAEB32A55268C ();
// 0x00000356 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsPrimitive(System.Type)
extern void TypeExtensions_IsPrimitive_m766C6FAA226E8761AFD257CDFAE1AE08E0FFC52E ();
// 0x00000357 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String,System.Boolean,System.Type&)
extern void TypeExtensions_AssignableToTypeName_m0DF91C0773BF94C001F16475C1D0F25A1F5BEAD5 ();
// 0x00000358 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String,System.Boolean)
extern void TypeExtensions_AssignableToTypeName_mFFBA9BAE800B1B4299152AE15DA342C3DD53779C ();
// 0x00000359 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ImplementInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementInterface_m06B052E52BCD41CD0BB01EBA71CE3A2D50EAB624 ();
// 0x0000035A System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNull(System.Object,System.String)
extern void ValidationUtils_ArgumentNotNull_m73F6A3CC2CD770D8EDF72C3391869AD2668A502F ();
// 0x0000035B System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Empty()
// 0x0000035C System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000035D System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::CastYield(System.Collections.IEnumerable)
// 0x0000035E System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x0000035F System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::WhereYield(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x00000360 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TResult>)
// 0x00000361 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectYield(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TResult>)
// 0x00000362 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`3<TSource,System.Int32,TResult>)
// 0x00000363 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectYield(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`3<TSource,System.Int32,TResult>)
// 0x00000364 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000365 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000366 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,Newtonsoft.Json.Serialization.Func`3<TSource,TCollection,TResult>)
// 0x00000367 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SelectManyYield(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,Newtonsoft.Json.Serialization.Func`3<TSource,TCollection,TResult>)
// 0x00000368 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::FirstImpl(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`1<TSource>)
// 0x00000369 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000036A TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000036B TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x0000036C TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::LastImpl(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`1<TSource>)
// 0x0000036D TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000036E TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x0000036F TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SingleImpl(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`1<TSource>)
// 0x00000370 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000371 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000372 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x00000373 System.Int32 Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000374 System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000375 System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ConcatYield(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000376 System.Collections.Generic.List`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000377 TSource[] Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000378 System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000379 System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::DistinctYield(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000037A Newtonsoft.Json.Utilities.LinqBridge.ILookup`2<TKey,TElement> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ToLookup(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,Newtonsoft.Json.Serialization.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000037B System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2<TKey,TSource>> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>)
// 0x0000037C System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2<TKey,TSource>> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000037D System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2<TKey,TElement>> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,Newtonsoft.Json.Serialization.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000037E System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000037F System.Collections.Generic.IEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000380 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x00000381 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000382 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,System.Boolean>)
// 0x00000383 Newtonsoft.Json.Utilities.LinqBridge.IOrderedEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>)
// 0x00000384 Newtonsoft.Json.Utilities.LinqBridge.IOrderedEnumerable`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,System.Collections.Generic.IComparer`1<TKey>)
// 0x00000385 System.Collections.Generic.Dictionary`2<TKey,TElement> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,Newtonsoft.Json.Serialization.Func`2<TSource,TElement>)
// 0x00000386 System.Collections.Generic.Dictionary`2<TKey,TElement> Newtonsoft.Json.Utilities.LinqBridge.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,Newtonsoft.Json.Serialization.Func`2<TSource,TKey>,Newtonsoft.Json.Serialization.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000387 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable::CheckNotNull(T,System.String)
// 0x00000388 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Futures`1::.cctor()
// 0x00000389 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Futures`1_<>c::.cctor()
// 0x0000038A System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Futures`1_<>c::.ctor()
// 0x0000038B T Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Futures`1_<>c::<.cctor>b__2_0()
// 0x0000038C T Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Futures`1_<>c::<.cctor>b__2_1()
// 0x0000038D System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Sequence`1::.cctor()
// 0x0000038E System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Grouping`2::.ctor(K)
// 0x0000038F K Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Grouping`2::get_Key()
// 0x00000390 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_Grouping`2::set_Key(K)
// 0x00000391 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::.ctor(System.Int32)
// 0x00000392 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.IDisposable.Dispose()
// 0x00000393 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::MoveNext()
// 0x00000394 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::<>m__Finally1()
// 0x00000395 TResult Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000396 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.Collections.IEnumerator.Reset()
// 0x00000397 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.Collections.IEnumerator.get_Current()
// 0x00000398 System.Collections.Generic.IEnumerator`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000399 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<CastYield>d__3`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000039A System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::.ctor(System.Int32)
// 0x0000039B System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.IDisposable.Dispose()
// 0x0000039C System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::MoveNext()
// 0x0000039D System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::<>m__Finally1()
// 0x0000039E TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000039F System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.Collections.IEnumerator.Reset()
// 0x000003A0 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.Collections.IEnumerator.get_Current()
// 0x000003A1 System.Collections.Generic.IEnumerator`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000003A2 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<WhereYield>d__11`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003A3 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::.ctor(System.Int32)
// 0x000003A4 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.IDisposable.Dispose()
// 0x000003A5 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::MoveNext()
// 0x000003A6 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::<>m__Finally1()
// 0x000003A7 TResult Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000003A8 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.Collections.IEnumerator.Reset()
// 0x000003A9 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.Collections.IEnumerator.get_Current()
// 0x000003AA System.Collections.Generic.IEnumerator`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000003AB System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__15`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000003AC System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::.ctor(System.Int32)
// 0x000003AD System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.IDisposable.Dispose()
// 0x000003AE System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::MoveNext()
// 0x000003AF System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::<>m__Finally1()
// 0x000003B0 TResult Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000003B1 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.Collections.IEnumerator.Reset()
// 0x000003B2 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x000003B3 System.Collections.Generic.IEnumerator`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000003B4 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectYield>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000003B5 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__DisplayClass18_0`2::.ctor()
// 0x000003B6 System.Collections.Generic.IEnumerable`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__DisplayClass18_0`2::<SelectMany>b__0(TSource,System.Int32)
// 0x000003B7 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__19`2::.cctor()
// 0x000003B8 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__19`2::.ctor()
// 0x000003B9 TResult Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__19`2::<SelectMany>b__19_0(TSource,TResult)
// 0x000003BA System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::.ctor(System.Int32)
// 0x000003BB System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.IDisposable.Dispose()
// 0x000003BC System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::MoveNext()
// 0x000003BD System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::<>m__Finally1()
// 0x000003BE System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::<>m__Finally2()
// 0x000003BF TResult Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000003C0 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.Collections.IEnumerator.Reset()
// 0x000003C1 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.Collections.IEnumerator.get_Current()
// 0x000003C2 System.Collections.Generic.IEnumerator`1<TResult> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000003C3 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<SelectManyYield>d__22`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000003C4 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::.ctor(System.Int32)
// 0x000003C5 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.IDisposable.Dispose()
// 0x000003C6 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::MoveNext()
// 0x000003C7 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::<>m__Finally1()
// 0x000003C8 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::<>m__Finally2()
// 0x000003C9 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000003CA System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.Collections.IEnumerator.Reset()
// 0x000003CB System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.Collections.IEnumerator.get_Current()
// 0x000003CC System.Collections.Generic.IEnumerator`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000003CD System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<ConcatYield>d__56`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003CE System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::.ctor(System.Int32)
// 0x000003CF System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.IDisposable.Dispose()
// 0x000003D0 System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::MoveNext()
// 0x000003D1 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::<>m__Finally1()
// 0x000003D2 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000003D3 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.Collections.IEnumerator.Reset()
// 0x000003D4 System.Object Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.Collections.IEnumerator.get_Current()
// 0x000003D5 System.Collections.Generic.IEnumerator`1<TSource> Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000003D6 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<DistinctYield>d__61`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003D7 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__67`2::.cctor()
// 0x000003D8 System.Void Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__67`2::.ctor()
// 0x000003D9 TSource Newtonsoft.Json.Utilities.LinqBridge.Enumerable_<>c__67`2::<GroupBy>b__67_0(TSource)
// 0x000003DA TKey Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2::get_Key()
// 0x000003DB System.Void Newtonsoft.Json.Utilities.LinqBridge.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000003DC System.Void Newtonsoft.Json.Utilities.LinqBridge.Lookup`2::Add(Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2<TKey,TElement>)
// 0x000003DD System.Collections.Generic.IEnumerable`1<TElement> Newtonsoft.Json.Utilities.LinqBridge.Lookup`2::Find(TKey)
// 0x000003DE System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.LinqBridge.IGrouping`2<TKey,TElement>> Newtonsoft.Json.Utilities.LinqBridge.Lookup`2::GetEnumerator()
// 0x000003DF System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000003E0 System.Void Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<T>,Newtonsoft.Json.Serialization.Func`2<T,K>,System.Collections.Generic.IComparer`1<K>,System.Boolean)
// 0x000003E1 System.Void Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.List`1<System.Comparison`1<T>>,Newtonsoft.Json.Serialization.Func`2<T,K>,System.Collections.Generic.IComparer`1<K>,System.Boolean)
// 0x000003E2 System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::GetEnumerator()
// 0x000003E3 Newtonsoft.Json.Utilities.LinqBridge.Tuple`2<T,System.Int32> Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::TagPosition(T,System.Int32)
// 0x000003E4 T Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::GetFirst(Newtonsoft.Json.Utilities.LinqBridge.Tuple`2<T,System.Int32>)
// 0x000003E5 System.Collections.IEnumerator Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000003E6 System.Int32 Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2::<GetEnumerator>b__5_0(Newtonsoft.Json.Utilities.LinqBridge.Tuple`2<T,System.Int32>,Newtonsoft.Json.Utilities.LinqBridge.Tuple`2<T,System.Int32>)
// 0x000003E7 System.Void Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2_<>c__DisplayClass3_0::.ctor()
// 0x000003E8 System.Int32 Newtonsoft.Json.Utilities.LinqBridge.OrderedEnumerable`2_<>c__DisplayClass3_0::<.ctor>b__0(T,T)
// 0x000003E9 TFirst Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::get_First()
// 0x000003EA TSecond Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::get_Second()
// 0x000003EB System.Void Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::.ctor(TFirst,TSecond)
// 0x000003EC System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::Equals(System.Object)
// 0x000003ED System.Boolean Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::Equals(Newtonsoft.Json.Utilities.LinqBridge.Tuple`2<TFirst,TSecond>)
// 0x000003EE System.Int32 Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::GetHashCode()
// 0x000003EF System.String Newtonsoft.Json.Utilities.LinqBridge.Tuple`2::ToString()
// 0x000003F0 T Newtonsoft.Json.Serialization.CachedAttributeGetter`1::GetAttribute(System.Object)
// 0x000003F1 System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1::.cctor()
// 0x000003F2 Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::get_Instance()
extern void DefaultContractResolver_get_Instance_mA143E809D478D15EFEE82356FAD69166AC1EA9C3 ();
// 0x000003F3 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_DynamicCodeGeneration()
extern void DefaultContractResolver_get_DynamicCodeGeneration_m64DB45593336BB75286B6A0B9F191E8805BF49E4 ();
// 0x000003F4 System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::get_DefaultMembersSearchFlags()
extern void DefaultContractResolver_get_DefaultMembersSearchFlags_m8272C93FDEA3C6C335D16095B59C7852E038B780 ();
// 0x000003F5 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_DefaultMembersSearchFlags(System.Reflection.BindingFlags)
extern void DefaultContractResolver_set_DefaultMembersSearchFlags_mC4E35C40BC5FA44637406B2B3902184C6505A56D ();
// 0x000003F6 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_SerializeCompilerGeneratedMembers()
extern void DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mDBC665D93F83376E6A95942B969E3D8C53722BA6 ();
// 0x000003F7 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableInterface()
extern void DefaultContractResolver_get_IgnoreSerializableInterface_m741D9DD41755124D727592C9144D0D7D8D5704D6 ();
// 0x000003F8 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableAttribute()
extern void DefaultContractResolver_get_IgnoreSerializableAttribute_mDAF98C7D55767841B3F87D44EC965B3E49267EC3 ();
// 0x000003F9 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_IgnoreSerializableAttribute(System.Boolean)
extern void DefaultContractResolver_set_IgnoreSerializableAttribute_mC9C2DE9BAB752E354362346D9F16CC65589DBE28 ();
// 0x000003FA System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreIsSpecifiedMembers()
extern void DefaultContractResolver_get_IgnoreIsSpecifiedMembers_m07874F9681042A12570903095C64D97C8CF28509 ();
// 0x000003FB System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreShouldSerializeMembers()
extern void DefaultContractResolver_get_IgnoreShouldSerializeMembers_m73EF95AAAEDE82855DD5FBB6597D0DD49050F0C9 ();
// 0x000003FC Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver::get_NamingStrategy()
extern void DefaultContractResolver_get_NamingStrategy_m8A47165A0D74FD594FA3FED797DFAB370560DF72 ();
// 0x000003FD System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor()
extern void DefaultContractResolver__ctor_m31FC48FF4FEF009E3BD720D3DD02D784D8F9B2A3 ();
// 0x000003FE Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContract(System.Type)
extern void DefaultContractResolver_ResolveContract_mB8264D595EDB2C764192921F3D0689781DD102CB ();
// 0x000003FF System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::GetSerializableMembers(System.Type)
extern void DefaultContractResolver_GetSerializableMembers_m27C22AABF3EAC1DE2CBDB37DDAA95125BAB51709 ();
// 0x00000400 Newtonsoft.Json.Serialization.JsonObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateObjectContract(System.Type)
extern void DefaultContractResolver_CreateObjectContract_m5E10D4C8726A6759A23D3DB22DB3622B263644E3 ();
// 0x00000401 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ThrowUnableToSerializeError(System.Object,System.Runtime.Serialization.StreamingContext)
extern void DefaultContractResolver_ThrowUnableToSerializeError_m070F7B0D04B9921F51A068FF3D7A8ECAB2CADFB2 ();
// 0x00000402 System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetExtensionDataMemberForType(System.Type)
extern void DefaultContractResolver_GetExtensionDataMemberForType_mFC604FD23D2A75B330B18386853EE608508CA004 ();
// 0x00000403 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetExtensionDataDelegates(Newtonsoft.Json.Serialization.JsonObjectContract,System.Reflection.MemberInfo)
extern void DefaultContractResolver_SetExtensionDataDelegates_mBFFABF4D52C094E95A1FF4A7EFC7BCDD1ACA16AD ();
// 0x00000404 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetAttributeConstructor(System.Type)
extern void DefaultContractResolver_GetAttributeConstructor_m4870197C0EC6A65115ABF09DD4C5F47C99CA0A10 ();
// 0x00000405 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetImmutableConstructor(System.Type,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void DefaultContractResolver_GetImmutableConstructor_m0583E2772CADA34F621742E0F9173F26C5A98CBA ();
// 0x00000406 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetParameterizedConstructor(System.Type)
extern void DefaultContractResolver_GetParameterizedConstructor_m8C57A8B55826ACB79D1EBE9BD94019FE24D7BF61 ();
// 0x00000407 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateConstructorParameters(System.Reflection.ConstructorInfo,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void DefaultContractResolver_CreateConstructorParameters_mBBA8A8BC677FB47D3361D42D77208A4A60A4E5A8 ();
// 0x00000408 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::MatchProperty(Newtonsoft.Json.Serialization.JsonPropertyCollection,System.String,System.Type)
extern void DefaultContractResolver_MatchProperty_mC68421A13E93DE762C2856A70378EAC4C3017DE5 ();
// 0x00000409 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePropertyFromConstructorParameter(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ParameterInfo)
extern void DefaultContractResolver_CreatePropertyFromConstructorParameter_m329031C1C0F78435CBB2DA096A3BED1EB8075C72 ();
// 0x0000040A Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContractConverter(System.Type)
extern void DefaultContractResolver_ResolveContractConverter_mFAE4E607B79292EB5EFD01114088EF847D5E8027 ();
// 0x0000040B Newtonsoft.Json.Serialization.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::GetDefaultCreator(System.Type)
extern void DefaultContractResolver_GetDefaultCreator_m635E067E5FB88835E96050E15E7EC3852A646029 ();
// 0x0000040C System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::InitializeContract(Newtonsoft.Json.Serialization.JsonContract)
extern void DefaultContractResolver_InitializeContract_m4AE3624F49C50E9A687F20C2D8E037D6B836980E ();
// 0x0000040D System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveCallbackMethods(Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void DefaultContractResolver_ResolveCallbackMethods_mBA6EBB3A6550257EC824048D3D9DBE4283D8520E ();
// 0x0000040E System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::GetCallbackMethodsForType(System.Type,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>&)
extern void DefaultContractResolver_GetCallbackMethodsForType_m818EF38CCC9B824D2CCFC295BDE628FA3B3891B9 ();
// 0x0000040F System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsConcurrentOrObservableCollection(System.Type)
extern void DefaultContractResolver_IsConcurrentOrObservableCollection_mD698E9FD9046E1A855B13819410137ADC4873208 ();
// 0x00000410 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipDeserialized(System.Type)
extern void DefaultContractResolver_ShouldSkipDeserialized_mF22D1A34DD512DD25788C9FA2CE1757215A12A5D ();
// 0x00000411 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipSerializing(System.Type)
extern void DefaultContractResolver_ShouldSkipSerializing_m303A0D3E0437DCA82B452FA6C26431D221B72DD3 ();
// 0x00000412 System.Collections.Generic.List`1<System.Type> Newtonsoft.Json.Serialization.DefaultContractResolver::GetClassHierarchyForType(System.Type)
extern void DefaultContractResolver_GetClassHierarchyForType_m9B10B8B2DE8482191C8AE888F0E90725100BF1A7 ();
// 0x00000413 Newtonsoft.Json.Serialization.JsonDictionaryContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDictionaryContract(System.Type)
extern void DefaultContractResolver_CreateDictionaryContract_m85FFB44B012DCAF1C459D292C666DCF7E762BDEC ();
// 0x00000414 Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateArrayContract(System.Type)
extern void DefaultContractResolver_CreateArrayContract_m06E2B8401E3C62EA2907B333E05B8ADDC6B99118 ();
// 0x00000415 Newtonsoft.Json.Serialization.JsonPrimitiveContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePrimitiveContract(System.Type)
extern void DefaultContractResolver_CreatePrimitiveContract_mDCC4E4E00910DB10DF950579FD29CDFC5618F91B ();
// 0x00000416 Newtonsoft.Json.Serialization.JsonLinqContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateLinqContract(System.Type)
extern void DefaultContractResolver_CreateLinqContract_mDD6928F5A4908F33DD9DD75BF534289D831F5980 ();
// 0x00000417 Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateISerializableContract(System.Type)
extern void DefaultContractResolver_CreateISerializableContract_m729294B8EEE558B60B21E85A1AFDCF7A5DA4FA4B ();
// 0x00000418 Newtonsoft.Json.Serialization.JsonStringContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateStringContract(System.Type)
extern void DefaultContractResolver_CreateStringContract_m8D0F0BBC6510DDB3E22DA7227EA76BB76CA605AD ();
// 0x00000419 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateContract(System.Type)
extern void DefaultContractResolver_CreateContract_mA880E83A963883906B9E60B5135D0D3210405844 ();
// 0x0000041A System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsJsonPrimitiveType(System.Type)
extern void DefaultContractResolver_IsJsonPrimitiveType_m5A161C5C4FE3A43C4D9EB376022488A8B85F4D25 ();
// 0x0000041B System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsIConvertible(System.Type)
extern void DefaultContractResolver_IsIConvertible_mF69C8E09B876524D58FC003B1A1F6D53B11C0D82 ();
// 0x0000041C System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::CanConvertToString(System.Type)
extern void DefaultContractResolver_CanConvertToString_mFB449D0C68891053F64466F9CA8C706111549FE9 ();
// 0x0000041D System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsValidCallback(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern void DefaultContractResolver_IsValidCallback_mA68CF0A2CD67A9F0D6D8E1DEE1FDF0815A4CEB8F ();
// 0x0000041E System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetClrTypeFullName(System.Type)
extern void DefaultContractResolver_GetClrTypeFullName_mBA6FD7E5FC03D837CD14F2C658AB31ACF4A6486E ();
// 0x0000041F System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperties(System.Type,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperties_mD88F8E8ED6BC6B1764A83BFDE5351744973251DB ();
// 0x00000420 Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.Serialization.DefaultContractResolver::GetNameTable()
extern void DefaultContractResolver_GetNameTable_m2495823B4CB9F53A8C68F672CC6642E3C95742B6 ();
// 0x00000421 Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.DefaultContractResolver::CreateMemberValueProvider(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateMemberValueProvider_m148D89F39386BB0B0A40230168F1431FE19F78CC ();
// 0x00000422 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperty_mE050154CA1E0803E1F2196F4DA89DF4E9D51EDD5 ();
// 0x00000423 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetPropertySettingsFromAttributes(Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String,System.Type,Newtonsoft.Json.MemberSerialization,System.Boolean&)
extern void DefaultContractResolver_SetPropertySettingsFromAttributes_mDE05CF52DA722B1EF9A2489B616E2DF7D52111E1 ();
// 0x00000424 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateShouldSerializeTest(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateShouldSerializeTest_m190F5A6C21F3216CA45F7979448B789B3869B2A4 ();
// 0x00000425 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetIsSpecifiedActions(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.MemberInfo,System.Boolean)
extern void DefaultContractResolver_SetIsSpecifiedActions_m3A91501EBC2DAD9DE353081DD57FE14A8EF08CA6 ();
// 0x00000426 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolvePropertyName(System.String)
extern void DefaultContractResolver_ResolvePropertyName_m2EB8945C3E292A626B1704DD69C84A1A81C19DC6 ();
// 0x00000427 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveExtensionDataName(System.String)
extern void DefaultContractResolver_ResolveExtensionDataName_m12706E81CF3C382BFC6ACC207AEF6FD198E916EE ();
// 0x00000428 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveDictionaryKey(System.String)
extern void DefaultContractResolver_ResolveDictionaryKey_m2C0ED8755665192B4EBAE33D34465AA8337F9170 ();
// 0x00000429 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetResolvedPropertyName(System.String)
extern void DefaultContractResolver_GetResolvedPropertyName_m7FD81548670A52AFE8A31675508FCE1817F008F6 ();
// 0x0000042A System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.cctor()
extern void DefaultContractResolver__cctor_m85C7D20B4F94BF1B5771BF6793DC5D147F8A9BB9 ();
// 0x0000042B System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::.cctor()
extern void U3CU3Ec__cctor_m85CAEDB57FF7676AAC95DFD260B6481281197167 ();
// 0x0000042C System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::.ctor()
extern void U3CU3Ec__ctor_m66B5C3727C9221926D751981D8976FBAC92AF194 ();
// 0x0000042D System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetSerializableMembers>b__39_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__39_0_m65957274B3F38B089CF9703826DD03F8C2E199B1 ();
// 0x0000042E System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetSerializableMembers>b__39_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__39_1_m05C8BE0493F96445A90DB2C1542137E97B6A3914 ();
// 0x0000042F System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetExtensionDataMemberForType>b__42_0(System.Type)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__42_0_m34CE4C615A5C3B39FA3B9239F08FD5990F617FDB ();
// 0x00000430 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetExtensionDataMemberForType>b__42_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__42_1_mB1349BC000C88A419927DA9D7972E0C1536D47B7 ();
// 0x00000431 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetAttributeConstructor>b__45_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetAttributeConstructorU3Eb__45_0_m6897AED9EF80B0A10A7FAF89A20267B9BA3B58C1 ();
// 0x00000432 System.Int32 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<CreateProperties>b__72_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreatePropertiesU3Eb__72_0_mE22DCE6869938AD48D6C2B4E7DF99A8436656B1D ();
// 0x00000433 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mCA177C229D955B328FF2309BE7B5F7952BBA28DB ();
// 0x00000434 System.String Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass40_0::<CreateObjectContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass40_0_U3CCreateObjectContractU3Eb__0_m1772D9B55FEF32F663C61A3617867C47944D9644 ();
// 0x00000435 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_mF2FDF84828A7B9CC2232990CCB0D9087C5B0DFE2 ();
// 0x00000436 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass43_1::.ctor()
extern void U3CU3Ec__DisplayClass43_1__ctor_mCD53D4C82A79996FE0540AD5C4A7B3376392C87B ();
// 0x00000437 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass43_1::<SetExtensionDataDelegates>b__0(System.Object,System.String,System.Object)
extern void U3CU3Ec__DisplayClass43_1_U3CSetExtensionDataDelegatesU3Eb__0_mB827890F42016E332D5830CB8C32C94B62324E3A ();
// 0x00000438 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass43_2::.ctor()
extern void U3CU3Ec__DisplayClass43_2__ctor_mD09804D2CABC9BBFA37B57F7AAEA31CEFF97D1DC ();
// 0x00000439 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass43_2::<SetExtensionDataDelegates>b__1(System.Object)
extern void U3CU3Ec__DisplayClass43_2_U3CSetExtensionDataDelegatesU3Eb__1_mD471676DB90927C3FCC3DEDBE9603AC0D2600146 ();
// 0x0000043A System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_mD75C93650AC41535F7A80DC65446F025E68D444F ();
// 0x0000043B System.String Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass60_0::<CreateDictionaryContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass60_0_U3CCreateDictionaryContractU3Eb__0_m7E65C583EA3A298D29437846CD399F2A065E96AF ();
// 0x0000043C System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_m03E3EFF049D6950FC322426F1E93A6985679ED05 ();
// 0x0000043D System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass77_0::<CreateShouldSerializeTest>b__0(System.Object)
extern void U3CU3Ec__DisplayClass77_0_U3CCreateShouldSerializeTestU3Eb__0_m7594BD1829776FFC8AFE47E10BDBA3089CE75410 ();
// 0x0000043E System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass78_0::.ctor()
extern void U3CU3Ec__DisplayClass78_0__ctor_mB5664B6EABBE53AF92E544828AD5BDBD1EBAE024 ();
// 0x0000043F System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass78_0::<SetIsSpecifiedActions>b__0(System.Object)
extern void U3CU3Ec__DisplayClass78_0_U3CSetIsSpecifiedActionsU3Eb__0_m7B81C63705B9F354C21B5B7B65975F96EF0CFEE9 ();
// 0x00000440 Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetMappings(System.Object)
extern void DefaultReferenceResolver_GetMappings_mE1960042FFA99D09F6E8D5A2CA2AB4EA1A87DDC0 ();
// 0x00000441 System.Object Newtonsoft.Json.Serialization.DefaultReferenceResolver::ResolveReference(System.Object,System.String)
extern void DefaultReferenceResolver_ResolveReference_mF589A336E9F5DD1230CAB2E7B1F1D4CF461D3007 ();
// 0x00000442 System.String Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetReference(System.Object,System.Object)
extern void DefaultReferenceResolver_GetReference_m333599673086EAF4D1A55DF8ECF0FE6BBB100406 ();
// 0x00000443 System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::AddReference(System.Object,System.String,System.Object)
extern void DefaultReferenceResolver_AddReference_m11D09BAFE21B5B8424C44D127DED8F2F9FC36960 ();
// 0x00000444 System.Boolean Newtonsoft.Json.Serialization.DefaultReferenceResolver::IsReferenced(System.Object,System.Object)
extern void DefaultReferenceResolver_IsReferenced_m6F43C4F7DBC6800C488310424E07C755FF1239CD ();
// 0x00000445 System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::.ctor()
extern void DefaultReferenceResolver__ctor_m8556D2A1340729EF8E9B576801A3D26A6CD80D08 ();
// 0x00000446 System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern void DefaultSerializationBinder__ctor_m05B9AC290947E7CA939B8373D93A5E59AA33E69B ();
// 0x00000447 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Utilities.TypeNameKey)
extern void DefaultSerializationBinder_GetTypeFromTypeNameKey_mA5BB5D6C117393A47807349D4023C82FC6F17035 ();
// 0x00000448 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetGenericTypeFromTypeName(System.String,System.Reflection.Assembly)
extern void DefaultSerializationBinder_GetGenericTypeFromTypeName_mA40F741CA7AFC6723D3D834AF5DFACFFDEF825A8 ();
// 0x00000449 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeByName(Newtonsoft.Json.Utilities.TypeNameKey)
extern void DefaultSerializationBinder_GetTypeByName_mBA729C2421AF7C2766962D1031DD572E8273B622 ();
// 0x0000044A System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern void DefaultSerializationBinder_BindToType_m13BE2767B59C18923CB1658EE4C06BA0412238F2 ();
// 0x0000044B System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToName(System.Type,System.String&,System.String&)
extern void DefaultSerializationBinder_BindToName_m0EDF7C3E4A11D04A6B323FB796C68ACFE8DCFD01 ();
// 0x0000044C System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern void DefaultSerializationBinder__cctor_m8E6C24AD52B4506CCB273E2A38AD6737287BDC16 ();
// 0x0000044D System.Void Newtonsoft.Json.Serialization.DynamicValueProvider::.ctor(System.Reflection.MemberInfo)
extern void DynamicValueProvider__ctor_mAD909816718BA1B7F74B1C3866095ACE9103EE3D ();
// 0x0000044E System.Void Newtonsoft.Json.Serialization.DynamicValueProvider::SetValue(System.Object,System.Object)
extern void DynamicValueProvider_SetValue_mAD79AEB7D25C6A94DE1851886295FD72D0906615 ();
// 0x0000044F System.Object Newtonsoft.Json.Serialization.DynamicValueProvider::GetValue(System.Object)
extern void DynamicValueProvider_GetValue_m87237D481A0C3773210A6064062C935ED9C85227 ();
// 0x00000450 System.Void Newtonsoft.Json.Serialization.ErrorContext::.ctor(System.Object,System.Object,System.String,System.Exception)
extern void ErrorContext__ctor_mA6A6D4CEF2207DE09B3C13065682DCD945B4932A ();
// 0x00000451 System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Traced()
extern void ErrorContext_get_Traced_m02E5538C159F2026BDF4F20361A156D173CBA44A ();
// 0x00000452 System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Traced(System.Boolean)
extern void ErrorContext_set_Traced_m396C003E7298E22C8705DA1F4C6B967DED616BAA ();
// 0x00000453 System.Exception Newtonsoft.Json.Serialization.ErrorContext::get_Error()
extern void ErrorContext_get_Error_m59B747073F35AE4F417B45D697F81B6720CC8E8E ();
// 0x00000454 System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Handled()
extern void ErrorContext_get_Handled_m4944848F97A77AF536C336EC0B1536DC2CA8884F ();
// 0x00000455 System.Void Newtonsoft.Json.Serialization.ErrorEventArgs::.ctor(System.Object,Newtonsoft.Json.Serialization.ErrorContext)
extern void ErrorEventArgs__ctor_m247007CD0A98AFF374E2ADE82074E883F0A48795 ();
// 0x00000456 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.IContractResolver::ResolveContract(System.Type)
// 0x00000457 System.Object Newtonsoft.Json.Serialization.IReferenceResolver::ResolveReference(System.Object,System.String)
// 0x00000458 System.String Newtonsoft.Json.Serialization.IReferenceResolver::GetReference(System.Object,System.Object)
// 0x00000459 System.Boolean Newtonsoft.Json.Serialization.IReferenceResolver::IsReferenced(System.Object,System.Object)
// 0x0000045A System.Void Newtonsoft.Json.Serialization.IReferenceResolver::AddReference(System.Object,System.String,System.Object)
// 0x0000045B System.Type Newtonsoft.Json.Serialization.ISerializationBinder::BindToType(System.String,System.String)
// 0x0000045C System.Void Newtonsoft.Json.Serialization.ISerializationBinder::BindToName(System.Type,System.String&,System.String&)
// 0x0000045D System.Diagnostics.TraceLevel Newtonsoft.Json.Serialization.ITraceWriter::get_LevelFilter()
// 0x0000045E System.Void Newtonsoft.Json.Serialization.ITraceWriter::Trace(System.Diagnostics.TraceLevel,System.String,System.Exception)
// 0x0000045F System.Void Newtonsoft.Json.Serialization.IValueProvider::SetValue(System.Object,System.Object)
// 0x00000460 System.Object Newtonsoft.Json.Serialization.IValueProvider::GetValue(System.Object)
// 0x00000461 System.Type Newtonsoft.Json.Serialization.JsonArrayContract::get_CollectionItemType()
extern void JsonArrayContract_get_CollectionItemType_m58C0FB4AF576AFBFE83CC36F4DEAC65E8A0D7350 ();
// 0x00000462 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsMultidimensionalArray()
extern void JsonArrayContract_get_IsMultidimensionalArray_mCCD0D5821D5B77E36E458CBDA3F28163A43FD454 ();
// 0x00000463 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsArray()
extern void JsonArrayContract_get_IsArray_mAB2C550CF36FF426BD4774317B9816CF6CB7C50E ();
// 0x00000464 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_ShouldCreateWrapper()
extern void JsonArrayContract_get_ShouldCreateWrapper_m9AEF6534DE709C5FC01A1C4A717CD59CFB2B817C ();
// 0x00000465 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_CanDeserialize()
extern void JsonArrayContract_get_CanDeserialize_m13172B489513B03324D63C2661527917B1E5714D ();
// 0x00000466 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_CanDeserialize(System.Boolean)
extern void JsonArrayContract_set_CanDeserialize_m7813695B8049D30380D70688BAC0A25A21BFB288 ();
// 0x00000467 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_ParameterizedCreator()
extern void JsonArrayContract_get_ParameterizedCreator_m8F45E862032D695379BF426C5270F32B54B3FDDB ();
// 0x00000468 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_OverrideCreator()
extern void JsonArrayContract_get_OverrideCreator_m2E46801CF67F1C660D975EB2539B9F00C0204A26 ();
// 0x00000469 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonArrayContract_set_OverrideCreator_m2BD77CC152EB6DF77F4946C3E7F00367007FE9C4 ();
// 0x0000046A System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreator()
extern void JsonArrayContract_get_HasParameterizedCreator_m139E86DB88D6B14B21341BD20B76E5F0D7634B47 ();
// 0x0000046B System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonArrayContract_set_HasParameterizedCreator_m765F6C00D80C9E0DBC2AD3AC75FFD1FB0B386969 ();
// 0x0000046C System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreatorInternal()
extern void JsonArrayContract_get_HasParameterizedCreatorInternal_m284C176991121E8630D5222A5059810D08A94342 ();
// 0x0000046D System.Void Newtonsoft.Json.Serialization.JsonArrayContract::.ctor(System.Type)
extern void JsonArrayContract__ctor_m4719BDEB9AA6721F184B4EAEE498183CDAB204DB ();
// 0x0000046E Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Serialization.JsonArrayContract::CreateWrapper(System.Object)
extern void JsonArrayContract_CreateWrapper_m9E518180FE80E0BC72A205247FE4EDFDE0BEC8A9 ();
// 0x0000046F System.Collections.IList Newtonsoft.Json.Serialization.JsonArrayContract::CreateTemporaryCollection()
extern void JsonArrayContract_CreateTemporaryCollection_m6F31E83A0BA577719EE14D397479179D982B2403 ();
// 0x00000470 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemContract()
extern void JsonContainerContract_get_ItemContract_mFD4146AE53DF6781FC435949D1F63774DD498F96 ();
// 0x00000471 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonContainerContract_set_ItemContract_m7FDF4D4278E98B76D633598045C6CF7B2CECFFBF ();
// 0x00000472 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_FinalItemContract()
extern void JsonContainerContract_get_FinalItemContract_mBFC8C36111B3B864B5A76596D9F0F7ED1C12BFBC ();
// 0x00000473 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemConverter()
extern void JsonContainerContract_get_ItemConverter_m6F134F5DCB3E238759E2DD0C5789AF513D4347C9 ();
// 0x00000474 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContainerContract_set_ItemConverter_m77C608FBF8F4DC9D018020D3F667E1B54B618779 ();
// 0x00000475 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemIsReference()
extern void JsonContainerContract_get_ItemIsReference_m89701BA68E992728520E9BDC50F9B5060A634B03 ();
// 0x00000476 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonContainerContract_set_ItemIsReference_mA8F291425545C17D7E8BB1F814061DC27A19D8E8 ();
// 0x00000477 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemReferenceLoopHandling()
extern void JsonContainerContract_get_ItemReferenceLoopHandling_mA5C57B9B6796727730296822ED90A1703071F4CA ();
// 0x00000478 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonContainerContract_set_ItemReferenceLoopHandling_mE6B800BA9FA5F760A182559D965C017814774B2E ();
// 0x00000479 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemTypeNameHandling()
extern void JsonContainerContract_get_ItemTypeNameHandling_m82D5A30719DD4C5E06C51209C77873BB68E7B1A7 ();
// 0x0000047A System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonContainerContract_set_ItemTypeNameHandling_mC8EA1B8FCEFFDD0D4027F47529DF713237279CA0 ();
// 0x0000047B System.Void Newtonsoft.Json.Serialization.JsonContainerContract::.ctor(System.Type)
extern void JsonContainerContract__ctor_mFB1531B7BD4786A499B4271C1D5A3493971D9A5A ();
// 0x0000047C System.Void Newtonsoft.Json.Serialization.SerializationCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationCallback__ctor_m8734530AE12301E9AEFDC1AB11CCEA8FB4EE9BA6 ();
// 0x0000047D System.Void Newtonsoft.Json.Serialization.SerializationCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext)
extern void SerializationCallback_Invoke_m524AD019CB047D6E89E1A0FA012C144CA54BE24F ();
// 0x0000047E System.IAsyncResult Newtonsoft.Json.Serialization.SerializationCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
extern void SerializationCallback_BeginInvoke_mFF84F02BF28275D11FDCDF81EBD8B0C28065C8BE ();
// 0x0000047F System.Void Newtonsoft.Json.Serialization.SerializationCallback::EndInvoke(System.IAsyncResult)
extern void SerializationCallback_EndInvoke_mDE4D476D0D7B1BA2317571896FD1B7973949412D ();
// 0x00000480 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationErrorCallback__ctor_mFF6F70CAFB27C7C8CEF59F712CD00FBA4035315D ();
// 0x00000481 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void SerializationErrorCallback_Invoke_m06FD2EAE36978C8200BC25387FDE84AEB82BE37E ();
// 0x00000482 System.IAsyncResult Newtonsoft.Json.Serialization.SerializationErrorCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext,System.AsyncCallback,System.Object)
extern void SerializationErrorCallback_BeginInvoke_m335013C48D92B7C85264995976F8D988157DE539 ();
// 0x00000483 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::EndInvoke(System.IAsyncResult)
extern void SerializationErrorCallback_EndInvoke_m669DBD23D1F3437A77FA967A4AD3DB42250B2331 ();
// 0x00000484 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataSetter__ctor_mC7CC6584099E799A5300F0BE5E05D85B56D15D2B ();
// 0x00000485 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::Invoke(System.Object,System.String,System.Object)
extern void ExtensionDataSetter_Invoke_m41C6856C3C3E0D185B4B2A837C9AFA3EDF78C2DA ();
// 0x00000486 System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataSetter::BeginInvoke(System.Object,System.String,System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataSetter_BeginInvoke_m5565380B6CE3ECCB245BA13D58DAAD0C1948230C ();
// 0x00000487 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataSetter_EndInvoke_m4C5B6F0D74E33FCF96FA022145E158C48C8D7FF3 ();
// 0x00000488 System.Void Newtonsoft.Json.Serialization.ExtensionDataGetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataGetter__ctor_mFA51DD454AAB5593A0646645E716B07AEF5A84B4 ();
// 0x00000489 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::Invoke(System.Object)
extern void ExtensionDataGetter_Invoke_m6B7D37D0D2744BD1BF0D2BB1ADAB9FC5F2DB7C57 ();
// 0x0000048A System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataGetter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataGetter_BeginInvoke_m4561867004FE47843B5CBEECFEE02E27DE3FCA56 ();
// 0x0000048B System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataGetter_EndInvoke_m467E719381C8348ED6209C314C93E18F39BBC4DA ();
// 0x0000048C System.Type Newtonsoft.Json.Serialization.JsonContract::get_UnderlyingType()
extern void JsonContract_get_UnderlyingType_m45980F01D64CE7B5AC2AEF9061FE5B3A93C08BA8 ();
// 0x0000048D System.Type Newtonsoft.Json.Serialization.JsonContract::get_CreatedType()
extern void JsonContract_get_CreatedType_mB48F0500CE45B05A1352B65F80A2EC8A8C606570 ();
// 0x0000048E System.Void Newtonsoft.Json.Serialization.JsonContract::set_CreatedType(System.Type)
extern void JsonContract_set_CreatedType_mE341A9647D6D20455BC261311DF612F70C20F8FF ();
// 0x0000048F System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::get_IsReference()
extern void JsonContract_get_IsReference_mEBAE449EDAD35B1B706CB30EC8CAFB2A769AC20A ();
// 0x00000490 System.Void Newtonsoft.Json.Serialization.JsonContract::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonContract_set_IsReference_m5A4CB847E6BF0AC7C2D0765E1248F86347D89709 ();
// 0x00000491 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_Converter()
extern void JsonContract_get_Converter_m34E7F7699FBC9748F40C37E28EF0553BD6215E01 ();
// 0x00000492 System.Void Newtonsoft.Json.Serialization.JsonContract::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_Converter_mBC0ADBEFC4479A9F6FB6F70DE4D9204DBCE131C0 ();
// 0x00000493 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_InternalConverter()
extern void JsonContract_get_InternalConverter_mB748D1B3C7C239EF77BCB381453CB66CE179EDA4 ();
// 0x00000494 System.Void Newtonsoft.Json.Serialization.JsonContract::set_InternalConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_InternalConverter_mD8358610598F4B5C8BD0548F204A08A52D4214E5 ();
// 0x00000495 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializedCallbacks()
extern void JsonContract_get_OnDeserializedCallbacks_mB5B4A64BA7F646CCF088440ED75690C31024C40F ();
// 0x00000496 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializingCallbacks()
extern void JsonContract_get_OnDeserializingCallbacks_m46D80EBA425C20AF2D9F2C8D47F3C8F769AC4110 ();
// 0x00000497 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializedCallbacks()
extern void JsonContract_get_OnSerializedCallbacks_m9F50D3434C11EDE6E5C82A774ED90F6010ECF51F ();
// 0x00000498 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializingCallbacks()
extern void JsonContract_get_OnSerializingCallbacks_m9FB0353BAE7BB6D3412C50FD78845BE7D0A3FD6F ();
// 0x00000499 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnErrorCallbacks()
extern void JsonContract_get_OnErrorCallbacks_m39667641B576F32A520243FB761B674A4A4C5088 ();
// 0x0000049A Newtonsoft.Json.Serialization.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreator()
extern void JsonContract_get_DefaultCreator_mA08BD7027873CF7540F5C6F3086B1D0C87E18173 ();
// 0x0000049B System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreator(Newtonsoft.Json.Serialization.Func`1<System.Object>)
extern void JsonContract_set_DefaultCreator_m64C75428A8F1E09FA0F23A8DD7E9EC476372C7C5 ();
// 0x0000049C System.Boolean Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreatorNonPublic()
extern void JsonContract_get_DefaultCreatorNonPublic_mE4942F277CD741CC7967C59F3DED9F2A7B5A4E4F ();
// 0x0000049D System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreatorNonPublic(System.Boolean)
extern void JsonContract_set_DefaultCreatorNonPublic_m7843F72D0B00583DCA81D665AF2505F545A3A8D7 ();
// 0x0000049E System.Void Newtonsoft.Json.Serialization.JsonContract::.ctor(System.Type)
extern void JsonContract__ctor_mF2F3FCE50B11B81AC11B60B88F42174138F08BE4 ();
// 0x0000049F System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerializing_mBEF6CE17EA951BE50A178B5A4858CB2724A052FE ();
// 0x000004A0 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerialized_m32A6BC2D9E689E8AA97798134F083351CCCD57C3 ();
// 0x000004A1 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserializing_mBB4C99168045ED89F9CF6BF348411E582F590856 ();
// 0x000004A2 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserialized_mEC2A5862C91C7983D6A7E76D5358265D7AABD81F ();
// 0x000004A3 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnError(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void JsonContract_InvokeOnError_mECEE2A74CAD094C8EDF252131975184E0200A444 ();
// 0x000004A4 Newtonsoft.Json.Serialization.SerializationCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationCallback_mDFF6B5A0D3573E128796738D4EB45FB8C3615DC4 ();
// 0x000004A5 Newtonsoft.Json.Serialization.SerializationErrorCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationErrorCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationErrorCallback_mBB90AB471B4CDA0D4DE06DA0003C3F8DE6109410 ();
// 0x000004A6 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_m573A9E78F159BB8B79A7D3FDDC0A5D2CAD48B434 ();
// 0x000004A7 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0::<CreateSerializationCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext)
extern void U3CU3Ec__DisplayClass57_0_U3CCreateSerializationCallbackU3Eb__0_m69BEC5AFFCA40C5FFB86FD60B74F6EA9737F8795 ();
// 0x000004A8 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_m758ABE63F4A644ED468F0D847E40465449642E4D ();
// 0x000004A9 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0::<CreateSerializationErrorCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void U3CU3Ec__DisplayClass58_0_U3CCreateSerializationErrorCallbackU3Eb__0_mB469C70295AF9D4E5072311485CBCE46DEC204A9 ();
// 0x000004AA Newtonsoft.Json.Serialization.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyResolver()
extern void JsonDictionaryContract_get_DictionaryKeyResolver_mA30855381E2A0E56CEFC26A939AD5A3C88ABBBF5 ();
// 0x000004AB System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyResolver(Newtonsoft.Json.Serialization.Func`2<System.String,System.String>)
extern void JsonDictionaryContract_set_DictionaryKeyResolver_m6C7E791F7659578704279021C7E8556B3C05EAFA ();
// 0x000004AC System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyType()
extern void JsonDictionaryContract_get_DictionaryKeyType_m3B2393F0B0E6C901208622866668197A544EAF69 ();
// 0x000004AD System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryValueType()
extern void JsonDictionaryContract_get_DictionaryValueType_m18B51FFC0B141D17C842B9AB539E40419024C8D6 ();
// 0x000004AE Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::get_KeyContract()
extern void JsonDictionaryContract_get_KeyContract_mDE6300EB3CB9550CB6128903D4ED63333D355C51 ();
// 0x000004AF System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_KeyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonDictionaryContract_set_KeyContract_m0309C6CC0F834B10CE3552A4C5C3DC45F5444E3D ();
// 0x000004B0 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ShouldCreateWrapper()
extern void JsonDictionaryContract_get_ShouldCreateWrapper_m3E9DD303CD5CBF792B03471175D82DEB4862F970 ();
// 0x000004B1 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ParameterizedCreator()
extern void JsonDictionaryContract_get_ParameterizedCreator_mA829B0C4882992B7E76A77064C528BBD4896309D ();
// 0x000004B2 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_OverrideCreator()
extern void JsonDictionaryContract_get_OverrideCreator_mA20249814DA22C45A7EDFD55DF0EEC44D38C6607 ();
// 0x000004B3 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonDictionaryContract_set_OverrideCreator_m08A7A0D7419745997A0E360EFC8BAF990672F368 ();
// 0x000004B4 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreator()
extern void JsonDictionaryContract_get_HasParameterizedCreator_mAABA49AF066CBC6190708DA93E0EEBEF63A16BFE ();
// 0x000004B5 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonDictionaryContract_set_HasParameterizedCreator_m4F901A8DD0E3BC5D544AD71796F9DB2921473E62 ();
// 0x000004B6 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreatorInternal()
extern void JsonDictionaryContract_get_HasParameterizedCreatorInternal_m8B412FEF7CCA1A943B7D7AEE68C6F27392E70C7D ();
// 0x000004B7 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::.ctor(System.Type)
extern void JsonDictionaryContract__ctor_m97B5D4ABF93C2CE8B29FC97C478A7D0FAB2B43A7 ();
// 0x000004B8 Newtonsoft.Json.Utilities.IWrappedDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateWrapper(System.Object)
extern void JsonDictionaryContract_CreateWrapper_m229C49B193EC20E3911ECBBD3A375D5E6B9FA4CB ();
// 0x000004B9 System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateTemporaryDictionary()
extern void JsonDictionaryContract_CreateTemporaryDictionary_mD17DE127136BC9FCAC9C2024F351673BE4D0F739 ();
// 0x000004BA System.Void Newtonsoft.Json.Serialization.JsonFormatterConverter::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonFormatterConverter__ctor_mD78CBC712E8FFDCC31A47A3F509F9DD4DDD51D31 ();
// 0x000004BB T Newtonsoft.Json.Serialization.JsonFormatterConverter::GetTokenValue(System.Object)
// 0x000004BC System.Object Newtonsoft.Json.Serialization.JsonFormatterConverter::Convert(System.Object,System.Type)
extern void JsonFormatterConverter_Convert_mF71A9A75B9415F1DA056D9E1071E3EC1F49BB5F4 ();
// 0x000004BD System.Boolean Newtonsoft.Json.Serialization.JsonFormatterConverter::ToBoolean(System.Object)
extern void JsonFormatterConverter_ToBoolean_m687C3A3E7CC29EC0369C3343B083BBD6429D7693 ();
// 0x000004BE System.Int32 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt32(System.Object)
extern void JsonFormatterConverter_ToInt32_m904107C732689EDE88B7B9B39832D9FB794221B9 ();
// 0x000004BF System.Int64 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt64(System.Object)
extern void JsonFormatterConverter_ToInt64_m564E7570EFA1EA5FD30A1ABF0560DC184714B8A3 ();
// 0x000004C0 System.Single Newtonsoft.Json.Serialization.JsonFormatterConverter::ToSingle(System.Object)
extern void JsonFormatterConverter_ToSingle_m6DD7701BD2A18E18DDBABCAAC6642DC667161811 ();
// 0x000004C1 System.String Newtonsoft.Json.Serialization.JsonFormatterConverter::ToString(System.Object)
extern void JsonFormatterConverter_ToString_m3FABE771D7486050C38B75CF10BAAEECE6FBE712 ();
// 0x000004C2 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::get_ISerializableCreator()
extern void JsonISerializableContract_get_ISerializableCreator_mA3D12FD5076FE7CE59CA1BDAC01858F0A62D9B9C ();
// 0x000004C3 System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::set_ISerializableCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonISerializableContract_set_ISerializableCreator_m8103074E088F950559CB4030C1C937029B627BAA ();
// 0x000004C4 System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::.ctor(System.Type)
extern void JsonISerializableContract__ctor_m1650283E0A6A37DDAB9B665CA2369780DF67A9E6 ();
// 0x000004C5 System.Void Newtonsoft.Json.Serialization.JsonLinqContract::.ctor(System.Type)
extern void JsonLinqContract__ctor_m884A3CE0683B618D903ECD483BA337A66B336492 ();
// 0x000004C6 Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::get_MemberSerialization()
extern void JsonObjectContract_get_MemberSerialization_m1C3932BCD60E68117DA329840A44A6FC814171BA ();
// 0x000004C7 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern void JsonObjectContract_set_MemberSerialization_mF41E83E04B7327E3360EAD1A5A4312CD04FF154D ();
// 0x000004C8 System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemRequired()
extern void JsonObjectContract_get_ItemRequired_m2C0BAB58CAD7982A4676A9F3AEE1782BFABC4093 ();
// 0x000004C9 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemRequired(System.Nullable`1<Newtonsoft.Json.Required>)
extern void JsonObjectContract_set_ItemRequired_mD6F9F146F10F9549F16E56A62A3F919414555D1E ();
// 0x000004CA System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemNullValueHandling()
extern void JsonObjectContract_get_ItemNullValueHandling_m3E6700775B255D9825799C221FDF5B1EC15A9657 ();
// 0x000004CB System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemNullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern void JsonObjectContract_set_ItemNullValueHandling_m213FF62B5E95DE1D3B15AABD7EE82BBB3A3F80DB ();
// 0x000004CC Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_Properties()
extern void JsonObjectContract_get_Properties_m065DF6051D2A3AF34541ACD898073356E21C5C08 ();
// 0x000004CD Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_CreatorParameters()
extern void JsonObjectContract_get_CreatorParameters_m516BA24E12A716D6147AB616C54ADAE4D4C59044 ();
// 0x000004CE Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_OverrideCreator()
extern void JsonObjectContract_get_OverrideCreator_mBE2DED7FCC2E5D92B251171246C432F134BE608F ();
// 0x000004CF System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonObjectContract_set_OverrideCreator_m2297BFE1313F2AE6C1F1C072C0CEB78C91DFE975 ();
// 0x000004D0 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_ParameterizedCreator()
extern void JsonObjectContract_get_ParameterizedCreator_mCCFAF03C5273A0072C0A08CCF8EC7EB8D7D7988D ();
// 0x000004D1 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ParameterizedCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonObjectContract_set_ParameterizedCreator_m3E7B7DD5BDF268CC566A71BF0A92019F7CBDFB2B ();
// 0x000004D2 Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataSetter()
extern void JsonObjectContract_get_ExtensionDataSetter_mC9692BE063000BC9B08E1CED0333A3495760AE58 ();
// 0x000004D3 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataSetter(Newtonsoft.Json.Serialization.ExtensionDataSetter)
extern void JsonObjectContract_set_ExtensionDataSetter_m7C0F5BA0C2A7C08242130A6104297AB0B3B555A9 ();
// 0x000004D4 Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataGetter()
extern void JsonObjectContract_get_ExtensionDataGetter_m2DCDBF7717530A39DEC71B3D73C69FD4D50A34EF ();
// 0x000004D5 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataGetter(Newtonsoft.Json.Serialization.ExtensionDataGetter)
extern void JsonObjectContract_set_ExtensionDataGetter_m334F0BC5B32791C76E6ACB987ECDC67DFFC2803A ();
// 0x000004D6 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataValueType(System.Type)
extern void JsonObjectContract_set_ExtensionDataValueType_mB5626C408F3FA6B6D433E000FC5A148872249B0F ();
// 0x000004D7 Newtonsoft.Json.Serialization.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataNameResolver()
extern void JsonObjectContract_get_ExtensionDataNameResolver_mC8587CFC411EE07462A508C5E8FCB7966CCC6498 ();
// 0x000004D8 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataNameResolver(Newtonsoft.Json.Serialization.Func`2<System.String,System.String>)
extern void JsonObjectContract_set_ExtensionDataNameResolver_mD1861025CF7E48684ACCD319246D66EB5C2FE1DD ();
// 0x000004D9 System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::get_HasRequiredOrDefaultValueProperties()
extern void JsonObjectContract_get_HasRequiredOrDefaultValueProperties_m78BB6E86F2A01E8156C4F03485BC6308B1F88A35 ();
// 0x000004DA System.Void Newtonsoft.Json.Serialization.JsonObjectContract::.ctor(System.Type)
extern void JsonObjectContract__ctor_mF16D02FE32088010F6952A761EFB133AD8C6EDE3 ();
// 0x000004DB System.Object Newtonsoft.Json.Serialization.JsonObjectContract::GetUninitializedObject()
extern void JsonObjectContract_GetUninitializedObject_m7A6556A66B12E5062184B13713E1D8BB619E2187 ();
// 0x000004DC Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::get_TypeCode()
extern void JsonPrimitiveContract_get_TypeCode_m8545211B4345052B6765F2859EA4183BB20AA923 ();
// 0x000004DD System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void JsonPrimitiveContract_set_TypeCode_mD22E1C529E174A27FA53C4AD8592DF253D20B909 ();
// 0x000004DE System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.ctor(System.Type)
extern void JsonPrimitiveContract__ctor_m65E54C8F30D7E7FBCF614F010C20A6DAE2F9DF6F ();
// 0x000004DF System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.cctor()
extern void JsonPrimitiveContract__cctor_m8CB6942B41C31680FE7C65107D1094CC2D57C9B4 ();
// 0x000004E0 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::get_PropertyContract()
extern void JsonProperty_get_PropertyContract_m6621EC7EE35D98EC28FB01C4868F94DE7865E7F6 ();
// 0x000004E1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonProperty_set_PropertyContract_m64F023649F7DF01BB542EA54A593E6DB6E07268F ();
// 0x000004E2 System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern void JsonProperty_get_PropertyName_m3A8236BD26DEF15AEDBE08598CBA3F9689D94F5B ();
// 0x000004E3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern void JsonProperty_set_PropertyName_m979E6473F69E230A72DA614C27B307461A6D983F ();
// 0x000004E4 System.Type Newtonsoft.Json.Serialization.JsonProperty::get_DeclaringType()
extern void JsonProperty_get_DeclaringType_m976E262EC6F092BEA2FBB98CABD70CDA5E8FF63E ();
// 0x000004E5 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DeclaringType(System.Type)
extern void JsonProperty_set_DeclaringType_m6FCAEE65C54088658CC4160077C90F6186F9C284 ();
// 0x000004E6 System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern void JsonProperty_get_Order_m4EB23EACDFADFDE45C4BE2CAF5E28CB4DA1A8516 ();
// 0x000004E7 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern void JsonProperty_set_Order_mE549A685FA9C4EE36AB1A0BF6EE11565768019E7 ();
// 0x000004E8 System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern void JsonProperty_get_UnderlyingName_mD6564BFA9D18DF7C45595E954A2399E4B7028F0E ();
// 0x000004E9 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern void JsonProperty_set_UnderlyingName_mD3EB3951C9BEF10B520E70EA38D8DDA01D34D97A ();
// 0x000004EA Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern void JsonProperty_get_ValueProvider_m972498900FA4F4E86D9DD1C466C58778A167B12F ();
// 0x000004EB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern void JsonProperty_set_ValueProvider_m77B954D939D32BAF89E9FCF6579925932C0E5309 ();
// 0x000004EC System.Void Newtonsoft.Json.Serialization.JsonProperty::set_AttributeProvider(Newtonsoft.Json.Serialization.IAttributeProvider)
extern void JsonProperty_set_AttributeProvider_m6CEFD85E6F1E383C4FDB97A94AD12A65209AA990 ();
// 0x000004ED System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern void JsonProperty_get_PropertyType_m2FE4193B8F352B14462B16330E71EB1ED4F8FF2F ();
// 0x000004EE System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern void JsonProperty_set_PropertyType_mD35E7552F19409C6933FEC710684A2C8BD3E952D ();
// 0x000004EF Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern void JsonProperty_get_Converter_m1A1C64049C0A44B256C0179697732F127B311EFC ();
// 0x000004F0 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_Converter_mB35200EA2441936770DB030003A87EE6B5B09B09 ();
// 0x000004F1 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern void JsonProperty_get_Ignored_mC3C447F0BC959E4D863772EE74E5AE9C50AB8FE4 ();
// 0x000004F2 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern void JsonProperty_set_Ignored_m3C88E6DB452A55460D1DDEB9C42568107F11EA39 ();
// 0x000004F3 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern void JsonProperty_get_Readable_mC6124AAEEF35A6B3BD15224443CA59822C36DA1F ();
// 0x000004F4 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern void JsonProperty_set_Readable_mC1C33DE05E4A92CEA5AD55D4B5A0EFA62C9D73E8 ();
// 0x000004F5 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern void JsonProperty_get_Writable_m9131922BBAD606235A7A6BF77676275DCD5F3AEB ();
// 0x000004F6 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern void JsonProperty_set_Writable_m2F5F9E7FAD763692EDDB36659026565919A2F1E2 ();
// 0x000004F7 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_HasMemberAttribute()
extern void JsonProperty_get_HasMemberAttribute_m445D4214B47A55A73728A1271443543912E79531 ();
// 0x000004F8 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_HasMemberAttribute(System.Boolean)
extern void JsonProperty_set_HasMemberAttribute_m08FE18D3015206DFAC5EC0E5EA0015B0CC755680 ();
// 0x000004F9 System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern void JsonProperty_get_DefaultValue_mF8DF9CA27EEBDD8D79CAF03FB23AE740E69B23AD ();
// 0x000004FA System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern void JsonProperty_set_DefaultValue_mC7EA67870E4B739B7045B4EE5CDB8751D7D4FA58 ();
// 0x000004FB System.Object Newtonsoft.Json.Serialization.JsonProperty::GetResolvedDefaultValue()
extern void JsonProperty_GetResolvedDefaultValue_mA8FC3C66D112B268094678C9CD14D8334B54FC91 ();
// 0x000004FC Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern void JsonProperty_get_Required_m3668F7485B68D37ED15F146FC9B317B99AF6FBC1 ();
// 0x000004FD System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern void JsonProperty_get_IsReference_m3988660B672B5D301D5B3B0EFCD63F3C70AB4852 ();
// 0x000004FE System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_IsReference_mDC54A3A02A4909671E3A2208AD3D41A140BB6546 ();
// 0x000004FF System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern void JsonProperty_get_NullValueHandling_m896C71D1CCD07AE9154C80275415FD4B2A999397 ();
// 0x00000500 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern void JsonProperty_set_NullValueHandling_m3184B0C18C078D416BE8541B97F0BD074EE338AA ();
// 0x00000501 System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern void JsonProperty_get_DefaultValueHandling_m58F05A0C25616AEFADFD215DD03075CA8670084F ();
// 0x00000502 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern void JsonProperty_set_DefaultValueHandling_mBE94482E6670757C5FC3593E79B708521F832679 ();
// 0x00000503 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern void JsonProperty_get_ReferenceLoopHandling_m116B5E02C523E9F0089407C29361364B05D7B6F2 ();
// 0x00000504 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ReferenceLoopHandling_m7BBED2FB39CA67867885E69A95CF9BC7F7F54913 ();
// 0x00000505 System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern void JsonProperty_get_ObjectCreationHandling_m385565294A68FB44827E1DE872A583AA2D433F93 ();
// 0x00000506 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern void JsonProperty_set_ObjectCreationHandling_mF32B2596EA272FB133BCE1DF6113459054352000 ();
// 0x00000507 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern void JsonProperty_get_TypeNameHandling_m559564A7FF806199DC77F97E3F42D04054DB2902 ();
// 0x00000508 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_TypeNameHandling_m053CFEA2942A28D6E83C942108E633950067647B ();
// 0x00000509 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern void JsonProperty_get_ShouldSerialize_mE29C87E635F339B721C627A058621662D3D8DEA8 ();
// 0x0000050A System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern void JsonProperty_set_ShouldSerialize_m8E658963D133DFDB4CAB5E409E1445BB96AED232 ();
// 0x0000050B System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldDeserialize()
extern void JsonProperty_get_ShouldDeserialize_m9A5EC533BF2CC7E94828CFA12AFF138D16555672 ();
// 0x0000050C System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern void JsonProperty_get_GetIsSpecified_mC435BFA967B9E6A08C3F0AB36FBE45BAF90097DA ();
// 0x0000050D System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern void JsonProperty_set_GetIsSpecified_mC7FB140145EAF32A04D1DD2F718AE2054184D63E ();
// 0x0000050E Newtonsoft.Json.Serialization.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern void JsonProperty_get_SetIsSpecified_m14F00C1E1D25DD1D6A6E5ABC8D754E308414A7BD ();
// 0x0000050F System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(Newtonsoft.Json.Serialization.Action`2<System.Object,System.Object>)
extern void JsonProperty_set_SetIsSpecified_m8691BB1961C25D41A9E6AA38954F4C2FEFE11477 ();
// 0x00000510 System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern void JsonProperty_ToString_mA846523AB86DD3C22C429B1CFDD58545CFE1D123 ();
// 0x00000511 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_ItemConverter()
extern void JsonProperty_get_ItemConverter_m66ACE06AB5396FEABB92C8E5B317374266073565 ();
// 0x00000512 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_ItemConverter_m0418546DC6476224EDBA1367F31B476ED5150D58 ();
// 0x00000513 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_ItemIsReference()
extern void JsonProperty_get_ItemIsReference_mE5E54C52206ADB476FECBDE1D0940EF9C332F459 ();
// 0x00000514 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_ItemIsReference_m46652718B5387EFD8E910DF76934A13DE4F8A924 ();
// 0x00000515 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemTypeNameHandling()
extern void JsonProperty_get_ItemTypeNameHandling_mDD88F25E528718F2F2564CECC7CB1C0834705278 ();
// 0x00000516 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_ItemTypeNameHandling_m6DFD39D4422A219A742A5CAD7D4C3536DF9B8EDB ();
// 0x00000517 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemReferenceLoopHandling()
extern void JsonProperty_get_ItemReferenceLoopHandling_m39C5B21061ADCFBF5455FD93EDAD16D5EF5D4D93 ();
// 0x00000518 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ItemReferenceLoopHandling_m97E915C27C2A039368E13837764D4BFC1DF5CBB9 ();
// 0x00000519 System.Void Newtonsoft.Json.Serialization.JsonProperty::WritePropertyName(Newtonsoft.Json.JsonWriter)
extern void JsonProperty_WritePropertyName_mBE43EB4FC925AF8F70F9E6E92F9EFB7695FE1D9F ();
// 0x0000051A System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern void JsonProperty__ctor_mAFDF0B1359B6D3699C12D897FEEE602636E0D0BA ();
// 0x0000051B System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::.ctor(System.Type)
extern void JsonPropertyCollection__ctor_m1488050B277F87CE5485028D1A4E213C2E72F3FB ();
// 0x0000051C System.String Newtonsoft.Json.Serialization.JsonPropertyCollection::GetKeyForItem(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_GetKeyForItem_m78731352A971AE62F3E5212371A64DACF604D678 ();
// 0x0000051D System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::AddProperty(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_AddProperty_mBAA7916DAA0B1BCDCD463DB9F36673EA2D052CB0 ();
// 0x0000051E Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetClosestMatchProperty(System.String)
extern void JsonPropertyCollection_GetClosestMatchProperty_mD923C318AD20E1401ABFCDDF87AF187E91C14959 ();
// 0x0000051F System.Boolean Newtonsoft.Json.Serialization.JsonPropertyCollection::TryGetValue(System.String,Newtonsoft.Json.Serialization.JsonProperty&)
extern void JsonPropertyCollection_TryGetValue_m7B1F63944D4C591ECE6AD454464345E0C68D5582 ();
// 0x00000520 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetProperty(System.String,System.StringComparison)
extern void JsonPropertyCollection_GetProperty_m31EE66DF721691202CDC4686A5714FBA65D93185 ();
// 0x00000521 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalBase__ctor_m11DAD1159345D932A16A31786B15B70E765D3988 ();
// 0x00000522 Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_DefaultReferenceMappings()
extern void JsonSerializerInternalBase_get_DefaultReferenceMappings_m7245BA4771135E8438907D478E2BB3F6122C134A ();
// 0x00000523 Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ResolvedNullValueHandling(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalBase_ResolvedNullValueHandling_m131555084FE0A86E92045F8F3050ECFCD1BEC129 ();
// 0x00000524 Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::GetErrorContext(System.Object,System.Object,System.String,System.Exception)
extern void JsonSerializerInternalBase_GetErrorContext_mC7EBEAAD364F1391E94B16B7912AD4EB1DBC5717 ();
// 0x00000525 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ClearErrorContext()
extern void JsonSerializerInternalBase_ClearErrorContext_m85C25D02DB5D6B78553B0A2DCB052B96BD47E071 ();
// 0x00000526 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::IsErrorHandled(System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,Newtonsoft.Json.IJsonLineInfo,System.String,System.Exception)
extern void JsonSerializerInternalBase_IsErrorHandled_m3D40E16A569A929358F8F7FC3E365EDFDD2762E7 ();
// 0x00000527 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.Equals(System.Object,System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m399DD19F29973AE6AF8440C30F694B0E749E1E66 ();
// 0x00000528 System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.GetHashCode(System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_mB0EF7249D7B30A3F4DC491B0C022B6F4062D20E8 ();
// 0x00000529 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::.ctor()
extern void ReferenceEqualsEqualityComparer__ctor_m353BA24394163AD8A8A21704B4E1D5A287459397 ();
// 0x0000052A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalReader__ctor_m4E1FD1B5EB69B283A7ADA370DE4A2B21A37289B5 ();
// 0x0000052B Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type)
extern void JsonSerializerInternalReader_GetContractSafe_mC4569AA3AF6C371AD6450FE80CD974186131856A ();
// 0x0000052C System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Deserialize(Newtonsoft.Json.JsonReader,System.Type,System.Boolean)
extern void JsonSerializerInternalReader_Deserialize_mC3606E927A9A9202156E5FB48E3FFF0B0C68853B ();
// 0x0000052D Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetInternalSerializer()
extern void JsonSerializerInternalReader_GetInternalSerializer_m114BA946FB6F4A608B4D71561368EF046D655522 ();
// 0x0000052E Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJToken(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_CreateJToken_m3EF5BD66A1659C46F1EA12B93C4D9F1A4B6065BB ();
// 0x0000052F Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJObject(Newtonsoft.Json.JsonReader)
extern void JsonSerializerInternalReader_CreateJObject_mC6EA6EFFBAD07A6522817C11CB84558E99B5D668 ();
// 0x00000530 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueInternal(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateValueInternal_mA73A7B9EACB8A206CC5EEF00F23C18FCAD7F8783 ();
// 0x00000531 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CoerceEmptyStringToNull(System.Type,Newtonsoft.Json.Serialization.JsonContract,System.String)
extern void JsonSerializerInternalReader_CoerceEmptyStringToNull_m37643F406591D05A627B3DD4AD5EE71B987C7AF2 ();
// 0x00000532 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetExpectedDescription(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_GetExpectedDescription_mEEE2D2E0911733D29B6BA2445F562AADCC1B4FB7 ();
// 0x00000533 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetConverter(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalReader_GetConverter_mC462340EABBA12092814C7DCEF0F34D085715EBF ();
// 0x00000534 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObject(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateObject_mEA9F0BF0ACFE679F68C4450050741AB81CB64A58 ();
// 0x00000535 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataPropertiesToken(Newtonsoft.Json.Linq.JTokenReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern void JsonSerializerInternalReader_ReadMetadataPropertiesToken_m6D6A75FD666D8394035677734D08A056B7BE8EEC ();
// 0x00000536 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataProperties(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern void JsonSerializerInternalReader_ReadMetadataProperties_m7EF1A3AEA96C3AAD5F7F68133FED73D271C08F2A ();
// 0x00000537 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolveTypeName(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_ResolveTypeName_m03EA9DBF50163C16B9E32F64C37204E557F09F50 ();
// 0x00000538 Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureArrayContract(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_EnsureArrayContract_m9EFDAF930FAE6F30D201F01B91DDFDB15B2D2251 ();
// 0x00000539 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateList(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern void JsonSerializerInternalReader_CreateList_mA78CFDD6B3BFE4CB4CED9DF5A7A62828182396C5 ();
// 0x0000053A System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasNoDefinedType(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_HasNoDefinedType_m349CF11482D5A5001BED1C0FA44B706138FF14FC ();
// 0x0000053B System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureType(Newtonsoft.Json.JsonReader,System.Object,System.Globalization.CultureInfo,Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void JsonSerializerInternalReader_EnsureType_mD0002044CC57504ECDDFE2639451A15D0FE484C4 ();
// 0x0000053C System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern void JsonSerializerInternalReader_SetPropertyValue_mDD7A849104951B8FA3B93CCAAAF845C8977EC160 ();
// 0x0000053D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CalculatePropertyDetails(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter&,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object,System.Boolean&,System.Object&,Newtonsoft.Json.Serialization.JsonContract&,System.Boolean&)
extern void JsonSerializerInternalReader_CalculatePropertyDetails_m0BA453FE9985672267CFFA62324BA6DCE5074491 ();
// 0x0000053E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::AddReference(Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_AddReference_m00411E619C5655867CA3D7988736C4923E606D61 ();
// 0x0000053F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalReader_HasFlag_mD4905E3AA68799D4D422D2C25DABF0EEEEC38181 ();
// 0x00000540 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldSetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonObjectContract,System.Object)
extern void JsonSerializerInternalReader_ShouldSetPropertyValue_m1114DDC0AA8ED9FC7EC33108A20A44D0253E00CE ();
// 0x00000541 System.Collections.IList Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewList(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewList_mC0E460958F1497328782CFA5BFD36F34A2D9CB82 ();
// 0x00000542 System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewDictionary(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewDictionary_m88D09814CB7F7129B3FA1B9004E7F5935376A415 ();
// 0x00000543 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserializing(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserializing_m79E6B557B6C186D26153251BE6B7701B36D0AB41 ();
// 0x00000544 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserialized(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserialized_m8F91168F37134B56AA777BBE6AE8213D030FA329 ();
// 0x00000545 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateDictionary(System.Collections.IDictionary,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateDictionary_m5C482E0BDDB328F0426383382C5D03A96F359A39 ();
// 0x00000546 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateMultidimensionalArray(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateMultidimensionalArray_mF56DDBEE60DA682D82D2E9CE07D9FB344A8CA01B ();
// 0x00000547 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ThrowUnexpectedEndException(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.String)
extern void JsonSerializerInternalReader_ThrowUnexpectedEndException_m918875D8BDD21A536497103BDB3668001536D14F ();
// 0x00000548 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateList(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateList_m60BEB72C49A08641972834EFABA8C2FC88EB2DAC ();
// 0x00000549 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializable(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_CreateISerializable_m8BD969D811DE13161AEDE570D08462932E7EBF46 ();
// 0x0000054A System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializableItem(Newtonsoft.Json.Linq.JToken,System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalReader_CreateISerializableItem_m26E5683BFCBB961FCF0BC24B8C3A308A086201B7 ();
// 0x0000054B System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObjectUsingCreatorWithParameters(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>,System.String)
extern void JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_m35DC1774B76BFE97BB14E893F1C00E1E31988913 ();
// 0x0000054C System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::DeserializeConvertable(Newtonsoft.Json.JsonConverter,Newtonsoft.Json.JsonReader,System.Type,System.Object)
extern void JsonSerializerInternalReader_DeserializeConvertable_mB8F7ADC0D7BEB196E1FBFFE6F48B6ACDEE16154A ();
// 0x0000054D System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolvePropertyAndCreatorValues(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_m480A31AECC3B5CA724C3ECFECD191BC387E726BB ();
// 0x0000054E System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,System.String,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewObject_m3E6B7AF730C7A523759B0A5DCEDFC99410532BCA ();
// 0x0000054F System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateObject(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateObject_m0DEE3DE2DC2B277D5C8D1EF5420016B7466624A7 ();
// 0x00000550 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldDeserialize(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_ShouldDeserialize_m6D555A56EADEFE976A27155BAB971AC12C33625A ();
// 0x00000551 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CheckPropertyName(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializerInternalReader_CheckPropertyName_mC1CDD471A620E12E3EA1215DBFF436C351738663 ();
// 0x00000552 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetExtensionData(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_SetExtensionData_m7FBB106DA821C7B0B512CD9A49BB4073C65E6090 ();
// 0x00000553 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadExtensionDataValue(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader)
extern void JsonSerializerInternalReader_ReadExtensionDataValue_mF9225B343CF7017D49D32D8602F57D8A3B689298 ();
// 0x00000554 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EndProcessProperty(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,System.Int32,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence,System.Boolean)
extern void JsonSerializerInternalReader_EndProcessProperty_mC1D50D24270D298E25F5C514F8848E32AC807950 ();
// 0x00000555 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyPresence(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence>)
extern void JsonSerializerInternalReader_SetPropertyPresence_mE1B8CD7F94AD2B9222049387B0DF58C8E065CCB2 ();
// 0x00000556 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HandleError(Newtonsoft.Json.JsonReader,System.Boolean,System.Int32)
extern void JsonSerializerInternalReader_HandleError_mE5A153A62CE71460457D620936E36E8477F089FA ();
// 0x00000557 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::.ctor()
extern void CreatorPropertyContext__ctor_m327E84FD81E1FA4A7F27B831C01BB57EB7F5F120 ();
// 0x00000558 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m3B1D885623A71042ED53E2D7BD72B83A13D2999A ();
// 0x00000559 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0::<CreateObjectUsingCreatorWithParameters>b__1(Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext)
extern void U3CU3Ec__DisplayClass36_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_m34A13F9F5D8A4EFB43AFB75610CC09C8AD0D9DA0 ();
// 0x0000055A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::.cctor()
extern void U3CU3Ec__cctor_m59C867C073F6904AFFA98A859E11053A220E19CF ();
// 0x0000055B System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::.ctor()
extern void U3CU3Ec__ctor_m499024B9C18B797C253AC1FA44205652DFE2BE99 ();
// 0x0000055C System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<CreateObjectUsingCreatorWithParameters>b__36_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_0_mC4C78099726532AE263EF01E99E16401F0CEA930 ();
// 0x0000055D System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<CreateObjectUsingCreatorWithParameters>b__36_2(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_2_mE4FF7ABC4C0CA29894E647D2E0FD09CE53D328A6 ();
// 0x0000055E Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<PopulateObject>b__40_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__40_0_mDA526FBA9F0365411314BAD23B45960144505B01 ();
// 0x0000055F Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<PopulateObject>b__40_1(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__40_1_mCD37A7F02A7E54123380DA97A7AE3844FE84C2A5 ();
// 0x00000560 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalWriter__ctor_mE222E23A936016CF22BC6D5DD90B513740C3EF43 ();
// 0x00000561 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerInternalWriter_Serialize_mB81AB622D9197CFA442D02EE1768BE2B938565CE ();
// 0x00000562 Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern void JsonSerializerInternalWriter_GetInternalSerializer_m29F89D7516AE6D6C5F3BE891407ED9321BF83293 ();
// 0x00000563 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern void JsonSerializerInternalWriter_GetContractSafe_mABC3E4FF52D0A52E36235E312F494D89E69634E6 ();
// 0x00000564 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializePrimitive_mD707F472B797F555808981F4A536D3355A8252E6 ();
// 0x00000565 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeValue_m262B3526913978315CB0FE94F99CFCA365758D4C ();
// 0x00000566 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ResolveIsReference(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ResolveIsReference_m791A5968F9DF07938A1F827A236DB1DB6E3596C7 ();
// 0x00000567 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteReference_mE52BB4845EB2E01D4D8D2A8884855E5A9DCC071A ();
// 0x00000568 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteProperty(System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteProperty_mB3A11BDAD0FE51047E3304BD0925D915606B8E35 ();
// 0x00000569 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_CheckForCircularReference_m7EBAE415EE04B240222A7D1E65E4078692DAC301 ();
// 0x0000056A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_WriteReference_m5AFF08F79D36642F420F6E1F4DE8058BB3F57D92 ();
// 0x0000056B System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_GetReference_m9DDB7A61792259E5EFFD226A2726B0A897291C26 ();
// 0x0000056C System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern void JsonSerializerInternalWriter_TryConvertToString_m5AC00FAAF624D945205B7F535FD981109C893B1D ();
// 0x0000056D System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern void JsonSerializerInternalWriter_SerializeString_m516B66A3A01AF7BE2075195355D03D1053286B4D ();
// 0x0000056E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerializing(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerializing_m65EEE3DC4955694C8AFA38C459EA8B385F54F384 ();
// 0x0000056F System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerialized(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerialized_m8D2366F112D14C0B2813A0FC04F7F731B1112C6F ();
// 0x00000570 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeObject_mED95180E0BED8F456B88A1FFFE21C98CA25126D8 ();
// 0x00000571 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CalculatePropertyValues(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract&,System.Object&)
extern void JsonSerializerInternalWriter_CalculatePropertyValues_m28BC2C83F1B4949D17084890E4C270A034A1B792 ();
// 0x00000572 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteObjectStart(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteObjectStart_m0DCF4C27043CF7250245957554142D60C55108B9 ();
// 0x00000573 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasCreatorParameter(Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_HasCreatorParameter_m54385E3A91443F2887ABA867C712040243ACCDFA ();
// 0x00000574 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReferenceIdProperty(Newtonsoft.Json.JsonWriter,System.Type,System.Object)
extern void JsonSerializerInternalWriter_WriteReferenceIdProperty_m24243802EC67246895BB1167464B3F175D37DAAE ();
// 0x00000575 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern void JsonSerializerInternalWriter_WriteTypeProperty_mF2D29E0480691A1A8AB1BA64FFC2456B91E3904A ();
// 0x00000576 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalWriter_HasFlag_mE1670D333F8969FA956E74392F744F2C300ACB8B ();
// 0x00000577 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerInternalWriter_HasFlag_m725159C2E2861437505881504BD10509B4BA2766 ();
// 0x00000578 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerInternalWriter_HasFlag_m512DCD11A895A0542E1F8C01D22BCB27F868555A ();
// 0x00000579 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeConvertable_mB8D2C9A2439E0B54862776483B116B24BDBA5A23 ();
// 0x0000057A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,System.Collections.IEnumerable,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeList_mD579696A244DDF8957E314997DBF2C3385BFF394 ();
// 0x0000057B System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m230F96926275B0CD5D8F2AE9946BA13C834C5CD8 ();
// 0x0000057C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.Int32,System.Int32[])
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m6A6E5FE5756EBE2745D8494BB461BB9FB2DE6AC1 ();
// 0x0000057D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteStartArray(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteStartArray_mEA8ECB91996080367A9560C73E9C047F8D10E0D2 ();
// 0x0000057E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeISerializable(Newtonsoft.Json.JsonWriter,System.Runtime.Serialization.ISerializable,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeISerializable_m1A14CE5ED29D2725223A96FDC45C9FC9B1B7071E ();
// 0x0000057F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteType_m2606D95F26A0BC8941734F8C13B5CF44317BEBF8 ();
// 0x00000580 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,System.Collections.IDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeDictionary_m32DF5FF0A240870A7B5A8E50566A1B4DB7A55760 ();
// 0x00000581 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Boolean&)
extern void JsonSerializerInternalWriter_GetPropertyName_mA086590DA00420DEBDB4C98632CC993FE0A6D99C ();
// 0x00000582 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern void JsonSerializerInternalWriter_HandleError_m7E1059B1E484F17FF2F3BFF50290B08D498F7565 ();
// 0x00000583 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_ShouldSerialize_m145618199F35BF6D8AE4E59CFAB6EDDD14EC3919 ();
// 0x00000584 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_IsSpecified_m1C145636865B720AE2B1A6013ED030EF668284A0 ();
// 0x00000585 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_add_Error_m2526D770DDE5BA48165929DB5DA51B56D337BE28 ();
// 0x00000586 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_remove_Error_mE4F5A003C74F0393A5B792EE628B301787B1F888 ();
// 0x00000587 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializerProxy_set_ReferenceResolver_m7AC23C2EA6FB9C72943FFE9ACCC26FB7723AA733 ();
// 0x00000588 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TraceWriter()
extern void JsonSerializerProxy_get_TraceWriter_m7937AD9FF02E97B62E8922FEB60B3713BCF4D251 ();
// 0x00000589 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializerProxy_set_TraceWriter_m87F509EA4098C7E0D6CA8D179ED8A9B9FA8DE82F ();
// 0x0000058A System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializerProxy_set_EqualityComparer_m60268277D8BF789EDC533BE9F076CD043BF18108 ();
// 0x0000058B Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Converters()
extern void JsonSerializerProxy_get_Converters_m74C33049C2C84290700FA7EA0D99494F103F73A3 ();
// 0x0000058C System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerProxy_set_DefaultValueHandling_m7A14E955B131E389616CE14BEAE254F1F487EA90 ();
// 0x0000058D Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ContractResolver()
extern void JsonSerializerProxy_get_ContractResolver_m9251A215AFC5A70E7D43D4CEBF6DE6FE0AB79496 ();
// 0x0000058E System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializerProxy_set_ContractResolver_m45046529796EC81E3335E2B9C9570D1216E1EA2D ();
// 0x0000058F System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializerProxy_set_MissingMemberHandling_m25AC6F2FE7355968AC9CE8FD737FFDA52CB5AFA8 ();
// 0x00000590 Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_NullValueHandling()
extern void JsonSerializerProxy_get_NullValueHandling_mB933D08033C9FFECD2052256EB0AB777FC9AC72B ();
// 0x00000591 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializerProxy_set_NullValueHandling_m3323408ABEF098B69A2BEA3086603085B1D6686F ();
// 0x00000592 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializerProxy_set_ObjectCreationHandling_mA2259B3F44E8339EA7ACAF90984CBD2F44542349 ();
// 0x00000593 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializerProxy_set_ReferenceLoopHandling_mD4B2FE502662C957735294003A578B4A48A90AF4 ();
// 0x00000594 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerProxy_set_PreserveReferencesHandling_m2FEC26DBA5637E14FDA714D5ACEDF47622C47DAE ();
// 0x00000595 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerProxy_set_TypeNameHandling_mBD42644DBC5548793779C6167E960219621645A0 ();
// 0x00000596 Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_MetadataPropertyHandling()
extern void JsonSerializerProxy_get_MetadataPropertyHandling_m0F652BE9639ED4E6C54CA209D167767862B35E38 ();
// 0x00000597 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializerProxy_set_MetadataPropertyHandling_mD40B0E2D7B76EC4C3BD2A11C0797110817322077 ();
// 0x00000598 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameAssemblyFormatHandling(Newtonsoft.Json.TypeNameAssemblyFormatHandling)
extern void JsonSerializerProxy_set_TypeNameAssemblyFormatHandling_m26CF563B1DE9BC16BEE2B6092450B1028F9980AD ();
// 0x00000599 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializerProxy_set_ConstructorHandling_m3ECA2C3224D032A86057F9ABA95A3A3EF0F2D63C ();
// 0x0000059A System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_SerializationBinder(Newtonsoft.Json.Serialization.ISerializationBinder)
extern void JsonSerializerProxy_set_SerializationBinder_m3C41A384D98CF40F9C2BB08C0866BB907676176D ();
// 0x0000059B System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Context()
extern void JsonSerializerProxy_get_Context_m67AC3276FC06E71B0FCBA8FB08A2E8CEF718A839 ();
// 0x0000059C System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializerProxy_set_Context_m758E2CF6587ADD2F6BF194AA9171B7BE411DB7C3 ();
// 0x0000059D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerProxy::get_CheckAdditionalContent()
extern void JsonSerializerProxy_get_CheckAdditionalContent_mFE0AC41789CD9135CC64464F0BAE3031723CAC48 ();
// 0x0000059E System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializerProxy_set_CheckAdditionalContent_m3070FDB34A19A3103E09E7A5337C407F59354143 ();
// 0x0000059F Newtonsoft.Json.Serialization.JsonSerializerInternalBase Newtonsoft.Json.Serialization.JsonSerializerProxy::GetInternalSerializer()
extern void JsonSerializerProxy_GetInternalSerializer_mC8A1487C24A158A0077AF88A3E74A0BD5D0A726B ();
// 0x000005A0 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern void JsonSerializerProxy__ctor_m7490D2667DD309D8F35387F83FC8A92028C3F4FC ();
// 0x000005A1 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern void JsonSerializerProxy__ctor_m6EABB469A96E516F4ABB182B220DA3AF82072E53 ();
// 0x000005A2 System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerProxy_DeserializeInternal_mEE4AB7F972B88D837AA86D2DCB5E8695BF86FEBE ();
// 0x000005A3 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerProxy_SerializeInternal_m442FC4EE02AFD20FBACC1778E65A2843A2F710BA ();
// 0x000005A4 System.Void Newtonsoft.Json.Serialization.JsonStringContract::.ctor(System.Type)
extern void JsonStringContract__ctor_mF771B7DCB9BCCC297D09E71B1A268D13D13E626F ();
// 0x000005A5 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetCachedAttribute(System.Object)
// 0x000005A6 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::CanTypeDescriptorConvertString(System.Type,System.ComponentModel.TypeConverter&)
extern void JsonTypeReflector_CanTypeDescriptorConvertString_m8A838B5AB1C499D72BF70EED50A6ED74DD53BC80 ();
// 0x000005A7 Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type,System.Boolean)
extern void JsonTypeReflector_GetObjectMemberSerialization_mA03AD683791A0B706854179BB79E7AF838A1468D ();
// 0x000005A8 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Object)
extern void JsonTypeReflector_GetJsonConverter_m0CDD012F7294FF61AA8E47CC9481457269E1DFF1 ();
// 0x000005A9 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::CreateJsonConverterInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateJsonConverterInstance_m1EEDE1F13AF058DB5421362A8523741071D56444 ();
// 0x000005AA Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::CreateNamingStrategyInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateNamingStrategyInstance_mCD1DE82A24637E05EE085EBF2D62ADC93BDB5BB5 ();
// 0x000005AB Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::GetContainerNamingStrategy(Newtonsoft.Json.JsonContainerAttribute)
extern void JsonTypeReflector_GetContainerNamingStrategy_mD1235F9B4B3468565F805CE15F39D048535B7C9D ();
// 0x000005AC Newtonsoft.Json.Serialization.Func`2<System.Object[],System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector::GetCreator(System.Type)
extern void JsonTypeReflector_GetCreator_m1AB3EA75468B4B616EF9878E369128F28E569512 ();
// 0x000005AD T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Type)
// 0x000005AE T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Reflection.MemberInfo)
// 0x000005AF System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::IsNonSerializable(System.Object)
extern void JsonTypeReflector_IsNonSerializable_m2A8C909D5FBEAE3CA180BD01FF9AD22AA70BF66C ();
// 0x000005B0 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::IsSerializable(System.Object)
extern void JsonTypeReflector_IsSerializable_mDFFA4CE2986EAAF4AEE97959978F8D910FF1ECA2 ();
// 0x000005B1 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Object)
// 0x000005B2 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_DynamicCodeGeneration()
extern void JsonTypeReflector_get_DynamicCodeGeneration_m200C1EB3550693B54A44CAC8A16478316E6A76D9 ();
// 0x000005B3 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_FullyTrusted()
extern void JsonTypeReflector_get_FullyTrusted_m88E91D0392DD735A3B9E655CA7AC129D1CA8A358 ();
// 0x000005B4 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern void JsonTypeReflector_get_ReflectionDelegateFactory_mB6DB479B33ADB2C949EF10E3E9203F33F24BF404 ();
// 0x000005B5 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern void JsonTypeReflector__cctor_mD9EB9C885D15BA8B136F35411D32F3023B1A8967 ();
// 0x000005B6 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mC1A7CB9FD6A90ECDEE4E457803C4BE177EE89098 ();
// 0x000005B7 System.Object Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass18_0::<GetCreator>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass18_0_U3CGetCreatorU3Eb__0_m0A13F549511BFE3683FA050BF9E2A877C41429E1 ();
// 0x000005B8 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::.cctor()
extern void U3CU3Ec__cctor_m7E7EBEC9A2CBAB5604164493C4DE8C50300575E0 ();
// 0x000005B9 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::.ctor()
extern void U3CU3Ec__ctor_m04EB66D6C0C9B2C9BFF1001073A2AEC10CFA96EC ();
// 0x000005BA System.Type Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<GetCreator>b__18_1(System.Object)
extern void U3CU3Ec_U3CGetCreatorU3Eb__18_1_mD8E99DE8C0BCFB11BE0158B8B71BE5B529C3738D ();
// 0x000005BB System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_ProcessDictionaryKeys()
extern void NamingStrategy_get_ProcessDictionaryKeys_m45543D7B09F3ECBDF7EDD29194F7C0B802DF1376 ();
// 0x000005BC System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_ProcessExtensionDataNames()
extern void NamingStrategy_get_ProcessExtensionDataNames_mC214568C21291DAFDBC899918E57FDEBF1A1FFBA ();
// 0x000005BD System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_OverrideSpecifiedNames()
extern void NamingStrategy_get_OverrideSpecifiedNames_m18521705823587C72CB6C682B2F1257593178399 ();
// 0x000005BE System.String Newtonsoft.Json.Serialization.NamingStrategy::GetPropertyName(System.String,System.Boolean)
extern void NamingStrategy_GetPropertyName_m2BF3AF050CCC85B967787EDD9773226474823EB9 ();
// 0x000005BF System.String Newtonsoft.Json.Serialization.NamingStrategy::GetExtensionDataName(System.String)
extern void NamingStrategy_GetExtensionDataName_m1F916B49FB6E46C39459E1DD83BB4326C104F716 ();
// 0x000005C0 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetDictionaryKey(System.String)
extern void NamingStrategy_GetDictionaryKey_m1BCA2FCB652EE8825BCFDE1A3646269572F0BE5D ();
// 0x000005C1 System.String Newtonsoft.Json.Serialization.NamingStrategy::ResolvePropertyName(System.String)
// 0x000005C2 System.Void Newtonsoft.Json.Serialization.ObjectConstructor`1::.ctor(System.Object,System.IntPtr)
// 0x000005C3 System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::Invoke(System.Object[])
// 0x000005C4 System.IAsyncResult Newtonsoft.Json.Serialization.ObjectConstructor`1::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
// 0x000005C5 System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::EndInvoke(System.IAsyncResult)
// 0x000005C6 System.Void Newtonsoft.Json.Serialization.ReflectionAttributeProvider::.ctor(System.Object)
extern void ReflectionAttributeProvider__ctor_mD0C00695E4500A380C460C6052D10DE22303488A ();
// 0x000005C7 System.Void Newtonsoft.Json.Serialization.ReflectionValueProvider::.ctor(System.Reflection.MemberInfo)
extern void ReflectionValueProvider__ctor_m46DCFF4183FB5866D0C02BE525C3C69BC8040F6A ();
// 0x000005C8 System.Void Newtonsoft.Json.Serialization.ReflectionValueProvider::SetValue(System.Object,System.Object)
extern void ReflectionValueProvider_SetValue_mB38C1CCC3E4CF1BFECA4E2662562774A054692AE ();
// 0x000005C9 System.Object Newtonsoft.Json.Serialization.ReflectionValueProvider::GetValue(System.Object)
extern void ReflectionValueProvider_GetValue_m6951935ED5FB87665B0B84B327D2A3AFD3C324CD ();
// 0x000005CA System.Void Newtonsoft.Json.Serialization.TraceJsonReader::.ctor(Newtonsoft.Json.JsonReader)
extern void TraceJsonReader__ctor_mBBA57634B7ECA935CC02F2F65220387F8EE4A8AF ();
// 0x000005CB System.String Newtonsoft.Json.Serialization.TraceJsonReader::GetDeserializedJsonMessage()
extern void TraceJsonReader_GetDeserializedJsonMessage_m551ADE42ACE633249F166F1F6EB163B16C2613D3 ();
// 0x000005CC System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Read()
extern void TraceJsonReader_Read_mEA0DB59F9231823AEC8495F0B0D2370C15422192 ();
// 0x000005CD System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsInt32()
extern void TraceJsonReader_ReadAsInt32_m65DDD08A6D11638DFBB69485FBC0CD2C59EED9D1 ();
// 0x000005CE System.String Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsString()
extern void TraceJsonReader_ReadAsString_mF7C60D00D3B7B3C7A83CDD13AE9814E14725DDEB ();
// 0x000005CF System.Byte[] Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBytes()
extern void TraceJsonReader_ReadAsBytes_mADFC6B5F53A5C999E40D7AB676053C72F54E8F85 ();
// 0x000005D0 System.Nullable`1<System.Decimal> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDecimal()
extern void TraceJsonReader_ReadAsDecimal_mD869B273A584ABC02F5685C9E2E3D9FC394C14EC ();
// 0x000005D1 System.Nullable`1<System.Double> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDouble()
extern void TraceJsonReader_ReadAsDouble_m26F8BFC5231BAD21F9117743DCCD0A344521B6A2 ();
// 0x000005D2 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBoolean()
extern void TraceJsonReader_ReadAsBoolean_mE4B8430C5E50B37E9126FFB75B0B4589302515F9 ();
// 0x000005D3 System.Nullable`1<System.DateTime> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTime()
extern void TraceJsonReader_ReadAsDateTime_m35BDA12F383DAB69E6C773497C78691B688AAF32 ();
// 0x000005D4 System.Void Newtonsoft.Json.Serialization.TraceJsonReader::WriteCurrentToken()
extern void TraceJsonReader_WriteCurrentToken_m8FC0FEC53513849812D512B3E20C54589F7F59AE ();
// 0x000005D5 System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::get_Depth()
extern void TraceJsonReader_get_Depth_m0F007CBB55DB9E12A8B1DE2BD3985D8A01A3675D ();
// 0x000005D6 System.String Newtonsoft.Json.Serialization.TraceJsonReader::get_Path()
extern void TraceJsonReader_get_Path_m6008B90D682AFAD28248C986510BF26FF904A84B ();
// 0x000005D7 Newtonsoft.Json.JsonToken Newtonsoft.Json.Serialization.TraceJsonReader::get_TokenType()
extern void TraceJsonReader_get_TokenType_m732D9298CF015F7164D4D80802851F886C893B16 ();
// 0x000005D8 System.Object Newtonsoft.Json.Serialization.TraceJsonReader::get_Value()
extern void TraceJsonReader_get_Value_mE9302AC2F565717DE44DEB44EF0D1F45CAA787A1 ();
// 0x000005D9 System.Type Newtonsoft.Json.Serialization.TraceJsonReader::get_ValueType()
extern void TraceJsonReader_get_ValueType_m2182D11387BAAD4089C09EC3AD433100D3B81BEE ();
// 0x000005DA System.Void Newtonsoft.Json.Serialization.TraceJsonReader::Close()
extern void TraceJsonReader_Close_m0395D8A4D5842A5E8BEA4E4BE5A1DE06E0847097 ();
// 0x000005DB System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mF86BFF9795B5084D67C42A7BABB1B6CF27F4F7A8 ();
// 0x000005DC System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_mFEBDF7B5BF2777604C4AB2208C04D55962B33BFF ();
// 0x000005DD System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_mEBA08346A784F3DE8D25DA11EE05BA420331AD12 ();
// 0x000005DE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::.ctor(Newtonsoft.Json.JsonWriter)
extern void TraceJsonWriter__ctor_m9E4A24698DF9460497374EBAD9A110861D5C05DA ();
// 0x000005DF System.String Newtonsoft.Json.Serialization.TraceJsonWriter::GetSerializedJsonMessage()
extern void TraceJsonWriter_GetSerializedJsonMessage_m0595305B3C90D0F465F41A3851438908AEF6D8DF ();
// 0x000005E0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Decimal)
extern void TraceJsonWriter_WriteValue_mF18E21DBDEBDDAF7D21BD0AF863B1FCAA4AFCDF5 ();
// 0x000005E1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern void TraceJsonWriter_WriteValue_m3B5110772FBE1B6C2667EF9694A0D720223AA21A ();
// 0x000005E2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Boolean)
extern void TraceJsonWriter_WriteValue_m36AA72FCF09CF1BC0414AE8C5EA0ED83F3098A34 ();
// 0x000005E3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern void TraceJsonWriter_WriteValue_mB6DA5077F14EFB38664A4B0D8156D81DAE7AFFA3 ();
// 0x000005E4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte)
extern void TraceJsonWriter_WriteValue_m64FC61CF870E4D36D13D92C14BDCC78153222429 ();
// 0x000005E5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void TraceJsonWriter_WriteValue_m1D40689B39D206C0940E00262DE2D1CF6A1F1FF2 ();
// 0x000005E6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Char)
extern void TraceJsonWriter_WriteValue_mD277C16BE412034B944359369C17435A7ED222D2 ();
// 0x000005E7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern void TraceJsonWriter_WriteValue_mD552653A65F280973E4373591ED447714B2D9260 ();
// 0x000005E8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte[])
extern void TraceJsonWriter_WriteValue_mFA078441220387746167303603C9E6EFFC55F018 ();
// 0x000005E9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.DateTime)
extern void TraceJsonWriter_WriteValue_mBAAC77989626D5F908F2EB1D8EBFC365FF9AF155 ();
// 0x000005EA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern void TraceJsonWriter_WriteValue_m12C73A12B00E3B63DC230B6B1AE456A3753CD9A6 ();
// 0x000005EB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Double)
extern void TraceJsonWriter_WriteValue_mAB528E62B91308388D52AD7D3A9E1298F2F74A51 ();
// 0x000005EC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern void TraceJsonWriter_WriteValue_m3E23A06BF40E3388ED0A50FF910BD6FB4C5AC7B2 ();
// 0x000005ED System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteUndefined()
extern void TraceJsonWriter_WriteUndefined_mB84BC489A4E6321779D729916AEAE073F1F4CFCB ();
// 0x000005EE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteNull()
extern void TraceJsonWriter_WriteNull_m1836FE638B2A4E29B49C29F6C4A0149A93976F72 ();
// 0x000005EF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Single)
extern void TraceJsonWriter_WriteValue_m36EB5E40F380018AFC179988D3CCC959A13C1DAC ();
// 0x000005F0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern void TraceJsonWriter_WriteValue_m1572DA725169C43F69CCE11F634B5B11747CB91A ();
// 0x000005F1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Guid)
extern void TraceJsonWriter_WriteValue_m37701EBA2490C89114362BC7D9FFCF0B9360F05E ();
// 0x000005F2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern void TraceJsonWriter_WriteValue_m462B6D6D4B9A3F5CCEAD781B425A6B01A5EE1F66 ();
// 0x000005F3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int32)
extern void TraceJsonWriter_WriteValue_mCC4CB98C5F2C053142E88D418F5E295EF4BCA1F2 ();
// 0x000005F4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern void TraceJsonWriter_WriteValue_mF2D432520258B523E937930DD4D0B7EA695D0EFE ();
// 0x000005F5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int64)
extern void TraceJsonWriter_WriteValue_m0D5B3D7DA82A844FC60BABA4BBF9F74D9971A12F ();
// 0x000005F6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern void TraceJsonWriter_WriteValue_m880B344F9DF140D3F9A8D2D549A3280CD9B0A4C1 ();
// 0x000005F7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.SByte)
extern void TraceJsonWriter_WriteValue_m9B0FF249C11259FBD52E1E88F9193328628FAB89 ();
// 0x000005F8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern void TraceJsonWriter_WriteValue_mB6E99E9A9D0C6B5A0F048DEA34D7C895443B8AB4 ();
// 0x000005F9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int16)
extern void TraceJsonWriter_WriteValue_m984365554098C2459F60909FD826001DB6A3D9B1 ();
// 0x000005FA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern void TraceJsonWriter_WriteValue_m261A772DBE1FA16AA08131A6FA449C59276FBBA7 ();
// 0x000005FB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.String)
extern void TraceJsonWriter_WriteValue_mBFBC1CEEFF3C4FB0B0C8B454DB710CC1E157FD3B ();
// 0x000005FC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.TimeSpan)
extern void TraceJsonWriter_WriteValue_m873CD18426F0EF63015123A4B71A30AA070E5862 ();
// 0x000005FD System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern void TraceJsonWriter_WriteValue_mD8CC01FDF6F2689464D0AD905B3749ABF5076D2F ();
// 0x000005FE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt32)
extern void TraceJsonWriter_WriteValue_mBDF6599890059A1F0FB533858E36DDDF1FA9ACBF ();
// 0x000005FF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern void TraceJsonWriter_WriteValue_mAE4CE378C154883E68D8AA7A901FC378EAAE08CA ();
// 0x00000600 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt64)
extern void TraceJsonWriter_WriteValue_mDD11F0BB6CC162EA040D58DDC6AC8F010005E85D ();
// 0x00000601 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern void TraceJsonWriter_WriteValue_mA0D773D3AB93D04ED7D4B8B8AAD5C8FE37B73A0F ();
// 0x00000602 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Uri)
extern void TraceJsonWriter_WriteValue_m055EDC8CF6BEC6E248CCC6F2FFC038B86AF3CF0C ();
// 0x00000603 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt16)
extern void TraceJsonWriter_WriteValue_m669CDDAB22B7EF7FA94C31442ACD84818B5702B8 ();
// 0x00000604 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern void TraceJsonWriter_WriteValue_m17399FCF1F94CDDF6AD0D9AAF32D1CDAB2E842B1 ();
// 0x00000605 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteComment(System.String)
extern void TraceJsonWriter_WriteComment_m006DEDF5164FC4930F967DEC99E1B36AC108C688 ();
// 0x00000606 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartArray()
extern void TraceJsonWriter_WriteStartArray_m3045FBB94AA0782E46EDF99DF4BC11DB963B2D18 ();
// 0x00000607 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndArray()
extern void TraceJsonWriter_WriteEndArray_mD29BCF35B02AF5DCDFF6BCCDA5E9847FB3DD1F0B ();
// 0x00000608 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartConstructor(System.String)
extern void TraceJsonWriter_WriteStartConstructor_m506509EE96C768CE0B042840C3D40733D765A4A3 ();
// 0x00000609 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndConstructor()
extern void TraceJsonWriter_WriteEndConstructor_m8E5D9ACF8C4F21E813E77BE146E0EF4408F834EC ();
// 0x0000060A System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String)
extern void TraceJsonWriter_WritePropertyName_m41BFB13D922627F43FCA0A45535628E4B4400ED8 ();
// 0x0000060B System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String,System.Boolean)
extern void TraceJsonWriter_WritePropertyName_mECD7CA85B626219F90662591D4AEEDD4A0064784 ();
// 0x0000060C System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartObject()
extern void TraceJsonWriter_WriteStartObject_m24965AF874C55AFCB5F64E88034A2BF14AB8D14A ();
// 0x0000060D System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndObject()
extern void TraceJsonWriter_WriteEndObject_mBE06596739B5167A8AEBFAC9C56ADFBB7011E98C ();
// 0x0000060E System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRawValue(System.String)
extern void TraceJsonWriter_WriteRawValue_m3676FFA1658D364C3E406CEFA8E635288086D924 ();
// 0x0000060F System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRaw(System.String)
extern void TraceJsonWriter_WriteRaw_mBFFD3E92F4A0A54B3A5389266768BE01CC53B138 ();
// 0x00000610 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::Close()
extern void TraceJsonWriter_Close_mAD2C82A79369D3B8C3A42425A14DCAAF7E7E54BA ();
// 0x00000611 System.Void Newtonsoft.Json.Serialization.Func`1::.ctor(System.Object,System.IntPtr)
// 0x00000612 TResult Newtonsoft.Json.Serialization.Func`1::Invoke()
// 0x00000613 System.IAsyncResult Newtonsoft.Json.Serialization.Func`1::BeginInvoke(System.AsyncCallback,System.Object)
// 0x00000614 TResult Newtonsoft.Json.Serialization.Func`1::EndInvoke(System.IAsyncResult)
// 0x00000615 System.Void Newtonsoft.Json.Serialization.Func`2::.ctor(System.Object,System.IntPtr)
// 0x00000616 TResult Newtonsoft.Json.Serialization.Func`2::Invoke(T)
// 0x00000617 System.IAsyncResult Newtonsoft.Json.Serialization.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000618 TResult Newtonsoft.Json.Serialization.Func`2::EndInvoke(System.IAsyncResult)
// 0x00000619 System.Void Newtonsoft.Json.Serialization.Func`3::.ctor(System.Object,System.IntPtr)
// 0x0000061A TResult Newtonsoft.Json.Serialization.Func`3::Invoke(T1,T2)
// 0x0000061B System.IAsyncResult Newtonsoft.Json.Serialization.Func`3::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
// 0x0000061C TResult Newtonsoft.Json.Serialization.Func`3::EndInvoke(System.IAsyncResult)
// 0x0000061D System.Void Newtonsoft.Json.Serialization.Action`2::.ctor(System.Object,System.IntPtr)
// 0x0000061E System.Void Newtonsoft.Json.Serialization.Action`2::Invoke(T1,T2)
// 0x0000061F System.IAsyncResult Newtonsoft.Json.Serialization.Action`2::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
// 0x00000620 System.Void Newtonsoft.Json.Serialization.Action`2::EndInvoke(System.IAsyncResult)
// 0x00000621 System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::get_ChildrenTokens()
extern void JArray_get_ChildrenTokens_mD0B3C4C99136242F8C553486112937F53A6F2E6F ();
// 0x00000622 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JArray::get_Type()
extern void JArray_get_Type_m20AA8D0E086E9909D435ABA8C446CF0D9D264DB4 ();
// 0x00000623 System.Void Newtonsoft.Json.Linq.JArray::.ctor()
extern void JArray__ctor_m6A528A9E3D179EBC0109F8794851133FC6E6DECD ();
// 0x00000624 System.Void Newtonsoft.Json.Linq.JArray::.ctor(Newtonsoft.Json.Linq.JArray)
extern void JArray__ctor_m8F404215EE0EEF80D73F87218971BB0223B9B9AD ();
// 0x00000625 System.Void Newtonsoft.Json.Linq.JArray::.ctor(System.Object)
extern void JArray__ctor_mD3F95410E37F7A033A4A351654124C118F819ACE ();
// 0x00000626 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::CloneToken()
extern void JArray_CloneToken_m9843ECF1C447319A4A11F8CD96282F30741F2AE9 ();
// 0x00000627 Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JArray_Load_m536322CA98FAEF1FB3D88FC7D1C31F9933BC0F8C ();
// 0x00000628 System.Void Newtonsoft.Json.Linq.JArray::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JArray_WriteTo_mD41E500B0E43AF234A80721BA0DB7B313BD76AD1 ();
// 0x00000629 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::get_Item(System.Int32)
extern void JArray_get_Item_m27159814DD196B5E04D0650A31B100FF7455AE64 ();
// 0x0000062A System.Void Newtonsoft.Json.Linq.JArray::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JArray_set_Item_m23419C1151763AF1E44DF256D693E4EA1C5F7596 ();
// 0x0000062B System.Int32 Newtonsoft.Json.Linq.JArray::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JArray_IndexOfItem_mAD248300C1DCCBBC96DB5B943A4B4AE421D830AD ();
// 0x0000062C System.Int32 Newtonsoft.Json.Linq.JArray::IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JArray_IndexOf_m30C6E4901ED006BD8C5AA6200878482E21590B2C ();
// 0x0000062D System.Void Newtonsoft.Json.Linq.JArray::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JArray_Insert_m78F358B56189EF992B8757E4A98B036B830D70A4 ();
// 0x0000062E System.Void Newtonsoft.Json.Linq.JArray::RemoveAt(System.Int32)
extern void JArray_RemoveAt_mAD06E3CF67F9DBB2FD994909949A29A7A3C53C62 ();
// 0x0000062F System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::GetEnumerator()
extern void JArray_GetEnumerator_mD72FCB9F9A6793BA0758A0467C14955C6135C736 ();
// 0x00000630 System.Void Newtonsoft.Json.Linq.JArray::Add(Newtonsoft.Json.Linq.JToken)
extern void JArray_Add_m160E2D7D3F0562969602C53102DDC455826BAA90 ();
// 0x00000631 System.Void Newtonsoft.Json.Linq.JArray::Clear()
extern void JArray_Clear_m28166D4D99498E888389E1EE7FC6A8C403A3D2E9 ();
// 0x00000632 System.Boolean Newtonsoft.Json.Linq.JArray::Contains(Newtonsoft.Json.Linq.JToken)
extern void JArray_Contains_m42E2037E2AA74D256817D8467414C0E53EB27CD9 ();
// 0x00000633 System.Void Newtonsoft.Json.Linq.JArray::CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JArray_CopyTo_m45C0D3692644A00637869E1C0A4F696693E5AC54 ();
// 0x00000634 System.Boolean Newtonsoft.Json.Linq.JArray::get_IsReadOnly()
extern void JArray_get_IsReadOnly_m9ECE40B3FD6F48332B36C7D5174FEFF238D06A31 ();
// 0x00000635 System.Boolean Newtonsoft.Json.Linq.JArray::Remove(Newtonsoft.Json.Linq.JToken)
extern void JArray_Remove_m21EBE81EB74C443538033275B24CFD592092D2BC ();
// 0x00000636 System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::get_ChildrenTokens()
extern void JConstructor_get_ChildrenTokens_mA7962EFF7CBDFF0A4CA22448C63DC28EC87386D9 ();
// 0x00000637 System.Int32 Newtonsoft.Json.Linq.JConstructor::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JConstructor_IndexOfItem_mB8CB3AB870A6FF8131DF2D9A4E5853DA2EADC2D2 ();
// 0x00000638 System.String Newtonsoft.Json.Linq.JConstructor::get_Name()
extern void JConstructor_get_Name_mC157359FD84A563E5ACF03E460AA45F7ACBBA74B ();
// 0x00000639 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JConstructor::get_Type()
extern void JConstructor_get_Type_m66D276263BCE8D1249F63012C881A43F5E3713AF ();
// 0x0000063A System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(Newtonsoft.Json.Linq.JConstructor)
extern void JConstructor__ctor_m34DD5E5F8905B948EC033F7C8DD8DBE86E4A3229 ();
// 0x0000063B System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String)
extern void JConstructor__ctor_mC83550526445CC7CC2E15EE9B33E44B62A32CEBE ();
// 0x0000063C Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::CloneToken()
extern void JConstructor_CloneToken_m44E0D07DF11AF7C53CD7C5B9420A7137209B2C02 ();
// 0x0000063D System.Void Newtonsoft.Json.Linq.JConstructor::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JConstructor_WriteTo_m4910418C3CBCDF4ABBA47438CA438D7FADEBE2EE ();
// 0x0000063E Newtonsoft.Json.Linq.JConstructor Newtonsoft.Json.Linq.JConstructor::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JConstructor_Load_m9C5E35B2B23503A9140BF2BA2842FF4BD07FDE16 ();
// 0x0000063F System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::get_ChildrenTokens()
// 0x00000640 System.Void Newtonsoft.Json.Linq.JContainer::.ctor()
extern void JContainer__ctor_m6F7FDE2FABAD6BBE91764B178AAE567313DB48BE ();
// 0x00000641 System.Void Newtonsoft.Json.Linq.JContainer::.ctor(Newtonsoft.Json.Linq.JContainer)
extern void JContainer__ctor_m3C6F58EFBAF2E0297A8A4057DC67E8C6E2FD5D51 ();
// 0x00000642 System.Void Newtonsoft.Json.Linq.JContainer::CheckReentrancy()
extern void JContainer_CheckReentrancy_mE03D475F6DC2B76129BBC2CA2A65F594C58F54C5 ();
// 0x00000643 System.Void Newtonsoft.Json.Linq.JContainer::OnListChanged(System.ComponentModel.ListChangedEventArgs)
extern void JContainer_OnListChanged_m2766A48A747C95EED2550B5D20A6E8FE891106C2 ();
// 0x00000644 System.Boolean Newtonsoft.Json.Linq.JContainer::get_HasValues()
extern void JContainer_get_HasValues_mFC7A19073E69D0A2B98A7ACF973BDD999DA1E386 ();
// 0x00000645 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_First()
extern void JContainer_get_First_m54F0FCB36A0B4F5F729897894FE2CB2FDFDC50DB ();
// 0x00000646 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_Last()
extern void JContainer_get_Last_mB71BE2A5FB83CA1EF54E947CDE32F446AD915232 ();
// 0x00000647 Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::Children()
extern void JContainer_Children_m6D4930BAFC45F524ABDB265D0F12B3FA655DE7B8 ();
// 0x00000648 System.Boolean Newtonsoft.Json.Linq.JContainer::IsMultiContent(System.Object)
extern void JContainer_IsMultiContent_m0DB18582345DF826B6B1C58B175A4531D9B80005 ();
// 0x00000649 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureParentToken(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JContainer_EnsureParentToken_mDDB2265F24D756FB4968CE75FDCF60D32D4514DC ();
// 0x0000064A System.Int32 Newtonsoft.Json.Linq.JContainer::IndexOfItem(Newtonsoft.Json.Linq.JToken)
// 0x0000064B System.Void Newtonsoft.Json.Linq.JContainer::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JContainer_InsertItem_mB70EF66A451AEDCCAB758A09029552DACC0D1380 ();
// 0x0000064C System.Void Newtonsoft.Json.Linq.JContainer::RemoveItemAt(System.Int32)
extern void JContainer_RemoveItemAt_m86F1E605C1D1F0AF026EBEB2397F9735425EA6C0 ();
// 0x0000064D System.Boolean Newtonsoft.Json.Linq.JContainer::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern void JContainer_RemoveItem_m9919857A2D11E99CC657DCE84BAA0FB8E815005D ();
// 0x0000064E Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::GetItem(System.Int32)
extern void JContainer_GetItem_m0A3BB5D9632475E01FEDEBA7714737F2563B3F08 ();
// 0x0000064F System.Void Newtonsoft.Json.Linq.JContainer::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_SetItem_m381414CF76B1A9D7AE0C35DF5114F8879EAFE27A ();
// 0x00000650 System.Void Newtonsoft.Json.Linq.JContainer::ClearItems()
extern void JContainer_ClearItems_mB29F66DDF001AB6723174F33674F97A1B0EC4A83 ();
// 0x00000651 System.Void Newtonsoft.Json.Linq.JContainer::ReplaceItem(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_ReplaceItem_mCFB836F29F3D0621931ABD43CD04B76FB1818D30 ();
// 0x00000652 System.Boolean Newtonsoft.Json.Linq.JContainer::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern void JContainer_ContainsItem_m2CCAA5EAE7C7B667DB638CD66F3FACD45825D21D ();
// 0x00000653 System.Void Newtonsoft.Json.Linq.JContainer::CopyItemsTo(System.Array,System.Int32)
extern void JContainer_CopyItemsTo_mEBA1493AE28A64C408549DCD006624A42838DF76 ();
// 0x00000654 System.Boolean Newtonsoft.Json.Linq.JContainer::IsTokenUnchanged(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_IsTokenUnchanged_m628A9E2AAF1477ABD5327F53B83E3772253167B7 ();
// 0x00000655 System.Void Newtonsoft.Json.Linq.JContainer::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_ValidateToken_mC667CFE9ED6D7A179EB649A08F9BE2282F8083CD ();
// 0x00000656 System.Void Newtonsoft.Json.Linq.JContainer::Add(System.Object)
extern void JContainer_Add_mD9550C1B022A20DA040C9CF62C0E5C49818963EB ();
// 0x00000657 System.Void Newtonsoft.Json.Linq.JContainer::AddAndSkipParentCheck(Newtonsoft.Json.Linq.JToken)
extern void JContainer_AddAndSkipParentCheck_m99F3BF83FD083E4CC7057A4028E5E993ECA30DAF ();
// 0x00000658 System.Void Newtonsoft.Json.Linq.JContainer::AddInternal(System.Int32,System.Object,System.Boolean)
extern void JContainer_AddInternal_mFEC5E9514CD1C24FE2BFA5E34B45BDA81F99783F ();
// 0x00000659 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::CreateFromContent(System.Object)
extern void JContainer_CreateFromContent_m910FF1BA1E07396CA41846F52AFFD823AB4B84E7 ();
// 0x0000065A System.Void Newtonsoft.Json.Linq.JContainer::RemoveAll()
extern void JContainer_RemoveAll_m5943CC7A4551BCA2F5B03BBE91904F9F7EB9F3E8 ();
// 0x0000065B System.Void Newtonsoft.Json.Linq.JContainer::ReadTokenFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JContainer_ReadTokenFrom_m3388EA17B357101E326F6A6EFE79F5457988CE19 ();
// 0x0000065C System.Void Newtonsoft.Json.Linq.JContainer::ReadContentFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JContainer_ReadContentFrom_mBD320D0ABB959749F381CFBDB2CE519927CDC6A5 ();
// 0x0000065D System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_IndexOf_m817F656A9DBC40C3CE2069DD52BC1AB652988BC1 ();
// 0x0000065E System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_Insert_mA91347984FFA777140863EE5F0D0D18C3F0550FB ();
// 0x0000065F System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.RemoveAt(System.Int32)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_RemoveAt_m168EC5FFED847D46A51311352FBE58BCBE0C8231 ();
// 0x00000660 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.get_Item(System.Int32)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m978B3246D0B5869B6F0C9DC74F4E1F0CCEE36081 ();
// 0x00000661 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_set_Item_m50013F935C583679C0205438B7D3A62C22788C34 ();
// 0x00000662 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Add(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Add_m7233EB87F651D9FEB9DF1991B711E293CA5D6CA0 ();
// 0x00000663 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Clear()
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Clear_mE088E128B96B65CC8DE6E487F820DE5AFD697DA3 ();
// 0x00000664 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Contains(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Contains_m13EDD25084953B846D3B2244A6E31903B492304C ();
// 0x00000665 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_mA535263ABEA90A2417262E8A1042D428864CCF1E ();
// 0x00000666 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.get_IsReadOnly()
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m4313128AB5B04DC1FB9E14F1DAA45E164CF9D9B1 ();
// 0x00000667 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Remove(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Remove_mC8901C3F7DBB488E34B2F129E91CC0DC9B110CF7 ();
// 0x00000668 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureValue(System.Object)
extern void JContainer_EnsureValue_m1B5EC4F7B78D93D5A43BDA42D91FFF1642EA2C27 ();
// 0x00000669 System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Add(System.Object)
extern void JContainer_System_Collections_IList_Add_m877310C5A0D5E4099E12CF6DD60E8EBF44AC88A9 ();
// 0x0000066A System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Clear()
extern void JContainer_System_Collections_IList_Clear_mCD55F64B9C65DC94AC087CC4DB61801909181082 ();
// 0x0000066B System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Contains(System.Object)
extern void JContainer_System_Collections_IList_Contains_m7AD0BF9E50173FB31E06E6345AFF83601CB406FA ();
// 0x0000066C System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.IndexOf(System.Object)
extern void JContainer_System_Collections_IList_IndexOf_mA5D4142B78C2A68B880A96C81134E32D7DAEC694 ();
// 0x0000066D System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Insert(System.Int32,System.Object)
extern void JContainer_System_Collections_IList_Insert_mE3521985A8FE5533C2968D2968D3D0C28F1960C2 ();
// 0x0000066E System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsFixedSize()
extern void JContainer_System_Collections_IList_get_IsFixedSize_m44891EF552BC45B49BDE37F2CED27286CF8C343B ();
// 0x0000066F System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsReadOnly()
extern void JContainer_System_Collections_IList_get_IsReadOnly_m94D57777497E41990E897B256B96D2E1F8FF21CB ();
// 0x00000670 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Remove(System.Object)
extern void JContainer_System_Collections_IList_Remove_m073EA89913199ADBFEFF2E75A08380739F372D74 ();
// 0x00000671 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.RemoveAt(System.Int32)
extern void JContainer_System_Collections_IList_RemoveAt_mD78B333695BAEC4F4342F0A4E4C7F5257D703FAA ();
// 0x00000672 System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_Item(System.Int32)
extern void JContainer_System_Collections_IList_get_Item_m1B417EA4D104B6ACF8483DE59D3A564C838EF4BE ();
// 0x00000673 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void JContainer_System_Collections_IList_set_Item_m747A07DA6FF39DEEDD3FD5BB7EC19B1366DF99C6 ();
// 0x00000674 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void JContainer_System_Collections_ICollection_CopyTo_m9BBA0D137DD5BD1566DFE10AFF8B71FA2F08BEF9 ();
// 0x00000675 System.Int32 Newtonsoft.Json.Linq.JContainer::get_Count()
extern void JContainer_get_Count_m99F43BC05DF8880EDA2BFC0DBA5B17E95966C77F ();
// 0x00000676 System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.get_SyncRoot()
extern void JContainer_System_Collections_ICollection_get_SyncRoot_m1631D700CF37EAA43B427868B4C9553FDBC263AC ();
// 0x00000677 System.Void Newtonsoft.Json.Linq.JEnumerable`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000678 System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1::GetEnumerator()
// 0x00000679 System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000067A System.Boolean Newtonsoft.Json.Linq.JEnumerable`1::Equals(Newtonsoft.Json.Linq.JEnumerable`1<T>)
// 0x0000067B System.Boolean Newtonsoft.Json.Linq.JEnumerable`1::Equals(System.Object)
// 0x0000067C System.Int32 Newtonsoft.Json.Linq.JEnumerable`1::GetHashCode()
// 0x0000067D System.Void Newtonsoft.Json.Linq.JEnumerable`1::.cctor()
// 0x0000067E System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::get_ChildrenTokens()
extern void JObject_get_ChildrenTokens_m63349F8B221F7904F66A3E8E06322ED77CEEB22F ();
// 0x0000067F System.Void Newtonsoft.Json.Linq.JObject::.ctor()
extern void JObject__ctor_mFA2D134BBD25FD55F332369EB3A15A06C04DC579 ();
// 0x00000680 System.Void Newtonsoft.Json.Linq.JObject::.ctor(Newtonsoft.Json.Linq.JObject)
extern void JObject__ctor_mF2575CF5A08CCE044A3C7F2655822DDDF2FC716F ();
// 0x00000681 System.Int32 Newtonsoft.Json.Linq.JObject::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JObject_IndexOfItem_m0730981FA1C644EBC26D76ADBFB768AB49468E27 ();
// 0x00000682 System.Void Newtonsoft.Json.Linq.JObject::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JObject_InsertItem_m3AE1D6E67D92A8BD3DEC1FFB0D61AF166A8A0767 ();
// 0x00000683 System.Void Newtonsoft.Json.Linq.JObject::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JObject_ValidateToken_m697C4DBD1BA5639552D80F7B72E2B1E08D945375 ();
// 0x00000684 System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanged(Newtonsoft.Json.Linq.JProperty)
extern void JObject_InternalPropertyChanged_m433BE9DBD9758003C2CDA0F86FF1C2360549B1C0 ();
// 0x00000685 System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanging(Newtonsoft.Json.Linq.JProperty)
extern void JObject_InternalPropertyChanging_m1A39468471C310AD0D2AA64B2B4C05487FFA6DE0 ();
// 0x00000686 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::CloneToken()
extern void JObject_CloneToken_mFC930B61437D70324826A2CF9BE482F211EAB4AA ();
// 0x00000687 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JObject::get_Type()
extern void JObject_get_Type_m69BE8D6E9064FCFF626A4B4FE4B5DEBF00903ECF ();
// 0x00000688 Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject::Property(System.String)
extern void JObject_Property_mE26936CF1739E55C76936912CED39FE0DC5AD5A8 ();
// 0x00000689 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.String)
extern void JObject_get_Item_m1CE23C597F843FB711CF431147B54C5D124BCA55 ();
// 0x0000068A System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.String,Newtonsoft.Json.Linq.JToken)
extern void JObject_set_Item_mFB29EFB2C36720C25D17B4A9A5D68B618236AE7D ();
// 0x0000068B Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JObject_Load_m38DE24D4285376E41CEBE2E9D4E522491A5AA8EB ();
// 0x0000068C Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object)
extern void JObject_FromObject_mD39912F544793B3AEF66D94FEB82BA9C86C5E157 ();
// 0x0000068D Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object,Newtonsoft.Json.JsonSerializer)
extern void JObject_FromObject_m1F0DC73281AD3BB70650C5294D6111CE18FF0DF8 ();
// 0x0000068E System.Void Newtonsoft.Json.Linq.JObject::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JObject_WriteTo_mCCA9AF897DA939A64A024422B86D48A04F2CDD23 ();
// 0x0000068F System.Void Newtonsoft.Json.Linq.JObject::Add(System.String,Newtonsoft.Json.Linq.JToken)
extern void JObject_Add_m7FF932071977406BA90D018306931A14BB36EB3B ();
// 0x00000690 System.Boolean Newtonsoft.Json.Linq.JObject::ContainsKey(System.String)
extern void JObject_ContainsKey_mC0142ACE0F24BF6CCB9A5A9905CC54578255463B ();
// 0x00000691 System.Boolean Newtonsoft.Json.Linq.JObject::Remove(System.String)
extern void JObject_Remove_m7D1577C8DFD7C7B50E4F1D37425BC1590738F20B ();
// 0x00000692 System.Boolean Newtonsoft.Json.Linq.JObject::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern void JObject_TryGetValue_m4D30DD97E819BDC4B81755A969858C3BB2678821 ();
// 0x00000693 System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Linq.JToken>.get_Values()
extern void JObject_System_Collections_Generic_IDictionaryU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m4931FE22963B7DCFDE0DCDF502A5EE88561C9BC9 ();
// 0x00000694 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Add(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_m9B5EAFF71EE780991B1D677374D14732ABFC9374 ();
// 0x00000695 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Clear()
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m2A030621B90A8272D997FCE7E0EA5AB42745DD1F ();
// 0x00000696 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Contains(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_mBBC9257F8EB4C13C443EECDE78AE41510A95996D ();
// 0x00000697 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[],System.Int32)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_mEA19B1E022E523C1748101FEE137AD8A6A3AA69B ();
// 0x00000698 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.get_IsReadOnly()
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_mC746613487BF8DBAA0B092D0B905DC4F78532C59 ();
// 0x00000699 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Remove(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_mBAE94220AF9D39B49E5179D75C69715D7430CB80 ();
// 0x0000069A System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JObject::GetEnumerator()
extern void JObject_GetEnumerator_mB261154B873224E72E2DD114557C8755175E9F1E ();
// 0x0000069B System.Void Newtonsoft.Json.Linq.JObject::OnPropertyChanged(System.String)
extern void JObject_OnPropertyChanged_m224474DEB0996D3D5005C04582627697C513821B ();
// 0x0000069C System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m0E8FD96DD08F13AFD57E8B06681E10C2F0A9DF0F ();
// 0x0000069D System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties(System.Attribute[])
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m6CF054F7971078F53171D8C1D9664A1AE8BD6562 ();
// 0x0000069E System.ComponentModel.AttributeCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetAttributes()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m73B3B45E4D5A4B08702094C25A81F678117EAB80 ();
// 0x0000069F System.ComponentModel.TypeConverter Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetConverter()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m856DB8DA2ABAD39C77D7F91EA117674A78F620F9 ();
// 0x000006A0 System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetPropertyOwner(System.ComponentModel.PropertyDescriptor)
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_mF5FDD23825622D701AE401C0F8A73BAF242398C7 ();
// 0x000006A1 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__56__ctor_m8741736BC1E68E98801C7507F7908B734CEB76F2 ();
// 0x000006A2 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__56_System_IDisposable_Dispose_m372F27E302BBEFE887C6D7BB1FB7F345CCAA930C ();
// 0x000006A3 System.Boolean Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::MoveNext()
extern void U3CGetEnumeratorU3Ed__56_MoveNext_m6FC4A5DEDE4CBA6D8483E11FBCC2DED58F69C8CA ();
// 0x000006A4 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__56_U3CU3Em__Finally1_m75AF7F284DBF610F0B4441CACDB7C2D5EFD4219C ();
// 0x000006A5 System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.get_Current()
extern void U3CGetEnumeratorU3Ed__56_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_m12BD511919C2201CDB3F941C903FA2D30EC9223C ();
// 0x000006A6 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__56_System_Collections_IEnumerator_Reset_m2BD7E5F15A004BEDAFA86B09D504A4443EA889F9 ();
// 0x000006A7 System.Object Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__56::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__56_System_Collections_IEnumerator_get_Current_m25460B63EF440644017919ACADFEE281482DB659 ();
// 0x000006A8 System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty::get_ChildrenTokens()
extern void JProperty_get_ChildrenTokens_m9D41F937B010D06641391C403044A564375282E1 ();
// 0x000006A9 System.String Newtonsoft.Json.Linq.JProperty::get_Name()
extern void JProperty_get_Name_m88FEA9CF6782258E27D9E3EFDD85D221EDC237B9 ();
// 0x000006AA Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::get_Value()
extern void JProperty_get_Value_m49A3D944A173652865089E9109D9CC4BF3DA2516 ();
// 0x000006AB System.Void Newtonsoft.Json.Linq.JProperty::set_Value(Newtonsoft.Json.Linq.JToken)
extern void JProperty_set_Value_m5FA3EA61479FB61A1B60F14854E250135E96F94B ();
// 0x000006AC System.Void Newtonsoft.Json.Linq.JProperty::.ctor(Newtonsoft.Json.Linq.JProperty)
extern void JProperty__ctor_m65DDA31471E1EA58804F53FC6642946D6E93C3FF ();
// 0x000006AD Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::GetItem(System.Int32)
extern void JProperty_GetItem_m5C7EA580E618ED77AE81BBF7E8D79E9FBE85C103 ();
// 0x000006AE System.Void Newtonsoft.Json.Linq.JProperty::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JProperty_SetItem_m5CA9F70CD487800734B0F5FEB12F3F566DAEEC1F ();
// 0x000006AF System.Boolean Newtonsoft.Json.Linq.JProperty::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_RemoveItem_m8E445D7450CBB784F1FCEC393D61EA129E8FEF10 ();
// 0x000006B0 System.Void Newtonsoft.Json.Linq.JProperty::RemoveItemAt(System.Int32)
extern void JProperty_RemoveItemAt_mA1D8AC89D89CC4F24BD7F0297065C7ED81DBA131 ();
// 0x000006B1 System.Int32 Newtonsoft.Json.Linq.JProperty::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_IndexOfItem_mFEC310782D3D416E8AADEC8EED40E725B3F75519 ();
// 0x000006B2 System.Void Newtonsoft.Json.Linq.JProperty::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JProperty_InsertItem_m996BA95E55914D99B386A70AC174165CEEE0222B ();
// 0x000006B3 System.Boolean Newtonsoft.Json.Linq.JProperty::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_ContainsItem_mE85B7D291C1B951066D13D4E049C196B4E409C90 ();
// 0x000006B4 System.Void Newtonsoft.Json.Linq.JProperty::ClearItems()
extern void JProperty_ClearItems_m1AA3F6B86DC60337C217C1BAB7EDDC4DD0405908 ();
// 0x000006B5 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::CloneToken()
extern void JProperty_CloneToken_m8498731C39EC65562D2C96541ED7ED4E133E8C3E ();
// 0x000006B6 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JProperty::get_Type()
extern void JProperty_get_Type_mF4CE521DD7F33429F362AAB9163ABB96D6351751 ();
// 0x000006B7 System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String)
extern void JProperty__ctor_m6B10ACA55A67631B016A1AB88D09E5AF14C15D81 ();
// 0x000006B8 System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String,System.Object)
extern void JProperty__ctor_mE9F9775D683A0D64A0C0A77D5D10D198565076D6 ();
// 0x000006B9 System.Void Newtonsoft.Json.Linq.JProperty::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JProperty_WriteTo_m9B30330518D7E8436731F61D3C433D41BD33D343 ();
// 0x000006BA Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JProperty::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JProperty_Load_m6DEEDF9D0CDE107413B292C452C40DDA0233C001 ();
// 0x000006BB System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty_JPropertyList::GetEnumerator()
extern void JPropertyList_GetEnumerator_m3BDE2EE7B2210D270E9645C52D4D18335A7A5018 ();
// 0x000006BC System.Collections.IEnumerator Newtonsoft.Json.Linq.JProperty_JPropertyList::System.Collections.IEnumerable.GetEnumerator()
extern void JPropertyList_System_Collections_IEnumerable_GetEnumerator_m0B1F9ACDB961C6919318636E18577612E7B4482F ();
// 0x000006BD System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Add(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Add_m780F96AFAAB8A0D863BFFD677DD5ED2A7BFE6E10 ();
// 0x000006BE System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Clear()
extern void JPropertyList_Clear_m02AF033AF9E29E0F003ECB887BE32924A587C54E ();
// 0x000006BF System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::Contains(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Contains_m58984DF894C61705D0FA8DAA468492085A726ABB ();
// 0x000006C0 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JPropertyList_CopyTo_m9E7B3D4A6EC591B6E61448643663972DD07730D8 ();
// 0x000006C1 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::Remove(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Remove_m23CE9DE25DD09C011C40057945CEBFDE181E992C ();
// 0x000006C2 System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList::get_Count()
extern void JPropertyList_get_Count_m43296B159804B4C2DA08B63AE3C139F17809AFF6 ();
// 0x000006C3 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::get_IsReadOnly()
extern void JPropertyList_get_IsReadOnly_mD59BF4C2896C5C721F0273C78B943A55760B2903 ();
// 0x000006C4 System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList::IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_IndexOf_mCBB240556976C0CC8BA37399544409EE24EDB804 ();
// 0x000006C5 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Insert_m57B0B9A631B92A6E5416A2DF185507DD60E4A4D1 ();
// 0x000006C6 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::RemoveAt(System.Int32)
extern void JPropertyList_RemoveAt_mF887CF79C66A509F1BEFEC666B63AB8B4CACEBCA ();
// 0x000006C7 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList::get_Item(System.Int32)
extern void JPropertyList_get_Item_mD671DBC78E470A29A369A026A884C7FC95D027C4 ();
// 0x000006C8 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_set_Item_m2B37E29C71A31BA0E700CC43FA23526A9065C95E ();
// 0x000006C9 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::.ctor()
extern void JPropertyList__ctor_mFE15BB09CD511BAB9D948537A2EE3FB0C9D79B2D ();
// 0x000006CA System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__1__ctor_mE6CF047F86955BCD821D9C1CEC1ED1E51370FF42 ();
// 0x000006CB System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__1_System_IDisposable_Dispose_m5CAC4DCF785BBC5CB50F8D340DD0C8421B19B366 ();
// 0x000006CC System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::MoveNext()
extern void U3CGetEnumeratorU3Ed__1_MoveNext_mD29CE72C4823ED6E23493120073DB4E8C6793748 ();
// 0x000006CD Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m075DF84A051D49C2A65D1F686850B06FA38D7641 ();
// 0x000006CE System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_Reset_mC4FABF2EE12A2400B8DC848E06219581308463B4 ();
// 0x000006CF System.Object Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_get_Current_mBEA8B5A85CBC2AA7F164ED4A0FBFF9790A78E5CA ();
// 0x000006D0 System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::.ctor(System.String)
extern void JPropertyDescriptor__ctor_mF8BC3F841E8D018FA749924B887363A1E7609017 ();
// 0x000006D1 System.Object Newtonsoft.Json.Linq.JPropertyDescriptor::GetValue(System.Object)
extern void JPropertyDescriptor_GetValue_m9544EE95991D48701D0B6DEDCA12B40D922B5B5C ();
// 0x000006D2 System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::SetValue(System.Object,System.Object)
extern void JPropertyDescriptor_SetValue_m72F04DFC725FF4E49C731BD9811C4BD84D84C933 ();
// 0x000006D3 System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::ShouldSerializeValue(System.Object)
extern void JPropertyDescriptor_ShouldSerializeValue_m898940041FF363D9E2CD5750D5483BF324C2F637 ();
// 0x000006D4 System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_ComponentType()
extern void JPropertyDescriptor_get_ComponentType_m21BB3A640DCE0C1F9B6AE9FA916B9EAC44EEC96A ();
// 0x000006D5 System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::get_IsReadOnly()
extern void JPropertyDescriptor_get_IsReadOnly_mD9BACFA60EFB8BBB633EFC420E24F8478C87B62C ();
// 0x000006D6 System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_PropertyType()
extern void JPropertyDescriptor_get_PropertyType_m529C70F78738711E0A640F454E15DD40E47A37FE ();
// 0x000006D7 System.Int32 Newtonsoft.Json.Linq.JPropertyDescriptor::get_NameHashCode()
extern void JPropertyDescriptor_get_NameHashCode_m39BD658C402C9133DDA2064B0BAA3CBD16093632 ();
// 0x000006D8 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.ctor()
extern void JPropertyKeyedCollection__ctor_mD085018AC0EC5F3C71DB6F2BB129D58DD0B9D607 ();
// 0x000006D9 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::AddKey(System.String,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_AddKey_m827936AFF9630BEC17B20C38ED9751A4A3C6EE67 ();
// 0x000006DA System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::ClearItems()
extern void JPropertyKeyedCollection_ClearItems_mA15718911E557296737FDF94756EDEF2F26760AC ();
// 0x000006DB System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::Contains(System.String)
extern void JPropertyKeyedCollection_Contains_m8A576E4FA8553FAA3CC58FFECD963D1559982B08 ();
// 0x000006DC System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::EnsureDictionary()
extern void JPropertyKeyedCollection_EnsureDictionary_m6EA26844C37CA7ACEB6DE76695DBA2F94D0763A6 ();
// 0x000006DD System.String Newtonsoft.Json.Linq.JPropertyKeyedCollection::GetKeyForItem(Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_GetKeyForItem_mB1F7563DDC81B9C8BB20E4B4B5D76162CEB0F41F ();
// 0x000006DE System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_InsertItem_m07A0B331DD04CDB30D8D2DBBB50B01743AE069C1 ();
// 0x000006DF System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveItem(System.Int32)
extern void JPropertyKeyedCollection_RemoveItem_mCB64265A09E588E936F9EB767176B0CA9D4A8D96 ();
// 0x000006E0 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveKey(System.String)
extern void JPropertyKeyedCollection_RemoveKey_m1BE4190718036D8E207EAB54C7E6D4498AB69577 ();
// 0x000006E1 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_SetItem_mFED8D3D8D4DDA7522F1C0112751FC1467715250E ();
// 0x000006E2 System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern void JPropertyKeyedCollection_TryGetValue_mFC7CDDC4347BEE67814105AFFCE640371D394356 ();
// 0x000006E3 System.Int32 Newtonsoft.Json.Linq.JPropertyKeyedCollection::IndexOfReference(Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_IndexOfReference_mA55206E55031655EE3AE93362BDA0135C44CD1C7 ();
// 0x000006E4 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.cctor()
extern void JPropertyKeyedCollection__cctor_mD0D61C94C1231CA4B1CB0BE849EBC203E68A2CDC ();
// 0x000006E5 System.Void Newtonsoft.Json.Linq.JRaw::.ctor(Newtonsoft.Json.Linq.JRaw)
extern void JRaw__ctor_mB6CE7D4DF90E64E4B2CC8B95353EBCAEF6792BC8 ();
// 0x000006E6 System.Void Newtonsoft.Json.Linq.JRaw::.ctor(System.Object)
extern void JRaw__ctor_mC683A7D742D041BA533874BE90971525BFC58D88 ();
// 0x000006E7 Newtonsoft.Json.Linq.JRaw Newtonsoft.Json.Linq.JRaw::Create(Newtonsoft.Json.JsonReader)
extern void JRaw_Create_m5D4A9AB9E920920A3676FADA04355E2BB9AE18E1 ();
// 0x000006E8 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JRaw::CloneToken()
extern void JRaw_CloneToken_m270E9A954ED7723EE1D2D01F76D93E06E13E396D ();
// 0x000006E9 Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_CommentHandling()
extern void JsonLoadSettings_get_CommentHandling_m0D1F1B3A7CAAC20364BB525655DD527A8427E639 ();
// 0x000006EA Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_LineInfoHandling()
extern void JsonLoadSettings_get_LineInfoHandling_m550443E4267BCEF2DE97FDC87375B255B4BC4898 ();
// 0x000006EB Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::get_Parent()
extern void JToken_get_Parent_m9D36FAEE8570A580C52A4489E0AC81AB3DF97DAF ();
// 0x000006EC System.Void Newtonsoft.Json.Linq.JToken::set_Parent(Newtonsoft.Json.Linq.JContainer)
extern void JToken_set_Parent_m25E46826CEDEC5CCD0373193B4C00DE5D9B8AB15 ();
// 0x000006ED Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Root()
extern void JToken_get_Root_mDBB03AB6D332BAA9C441E3E05FC595F3CF5D10D9 ();
// 0x000006EE Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::CloneToken()
// 0x000006EF Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JToken::get_Type()
// 0x000006F0 System.Boolean Newtonsoft.Json.Linq.JToken::get_HasValues()
// 0x000006F1 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Next()
extern void JToken_get_Next_mB3ABFBD8903CDB18AECD203C8486694C32C5AA1C ();
// 0x000006F2 System.Void Newtonsoft.Json.Linq.JToken::set_Next(Newtonsoft.Json.Linq.JToken)
extern void JToken_set_Next_mE1CB376E42ADAA5F0DE365177F63A5A6C83F5888 ();
// 0x000006F3 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Previous()
extern void JToken_get_Previous_m8B146F1A9F1A7C64E5528D987AAA429DF6DB48FC ();
// 0x000006F4 System.Void Newtonsoft.Json.Linq.JToken::set_Previous(Newtonsoft.Json.Linq.JToken)
extern void JToken_set_Previous_mEF6ED55A191CFA758A499C645906788865D4B40F ();
// 0x000006F5 System.String Newtonsoft.Json.Linq.JToken::get_Path()
extern void JToken_get_Path_mE558B72FCE14D8DCB3E5A43EEC5658AC7EFB5262 ();
// 0x000006F6 System.Void Newtonsoft.Json.Linq.JToken::.ctor()
extern void JToken__ctor_m6617FB9D1D309CE6CD7145B4D8AC50B93CAC88E3 ();
// 0x000006F7 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_First()
extern void JToken_get_First_mB304B6FB46ED3645725A6BF4D0EA2C327A5217AD ();
// 0x000006F8 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Last()
extern void JToken_get_Last_mBB5F28CCAB0DF9B1E1B3805BA9A574611D221884 ();
// 0x000006F9 Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Children()
extern void JToken_Children_mB119F9E9772A27F8C69F143D66EC03172151D419 ();
// 0x000006FA System.Void Newtonsoft.Json.Linq.JToken::Remove()
extern void JToken_Remove_m82702BEF06F90B07D859FD754E7D06255DE06888 ();
// 0x000006FB System.Void Newtonsoft.Json.Linq.JToken::Replace(Newtonsoft.Json.Linq.JToken)
extern void JToken_Replace_mA44F4327EF356AEB65A461989E46F065CE23A8ED ();
// 0x000006FC System.Void Newtonsoft.Json.Linq.JToken::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
// 0x000006FD System.String Newtonsoft.Json.Linq.JToken::ToString()
extern void JToken_ToString_mA2A9EC2D5D6A197006BBBCBBB5AACD99C4CA524D ();
// 0x000006FE System.String Newtonsoft.Json.Linq.JToken::ToString(Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern void JToken_ToString_m1F92CBAEC1C3359971DCA6EA3721EF9593C583EA ();
// 0x000006FF Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JToken::EnsureValue(Newtonsoft.Json.Linq.JToken)
extern void JToken_EnsureValue_m1A29C124C29713BCE639D4881635DD8978CEAC8E ();
// 0x00000700 System.String Newtonsoft.Json.Linq.JToken::GetType(Newtonsoft.Json.Linq.JToken)
extern void JToken_GetType_m282C001D941CFB607D4991FAFE720283E9A648D9 ();
// 0x00000701 System.Boolean Newtonsoft.Json.Linq.JToken::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JTokenType[],System.Boolean)
extern void JToken_ValidateToken_m70448ADFDB9DA07544828FC7B2C3353A0497DD11 ();
// 0x00000702 System.Boolean Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m4BBABF4C9C56A70ADA8AD1636F95DD4D4F494EBF ();
// 0x00000703 System.Nullable`1<System.Boolean> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m31B50113F690C48871285403438F7B080818DAF0 ();
// 0x00000704 System.Int64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mD122AE34E7356E371ED2E1A8C6B3D54B9B48AE16 ();
// 0x00000705 System.Nullable`1<System.DateTime> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m91BE83B3E0861CFB9557DECE258C05E2E0127624 ();
// 0x00000706 System.Nullable`1<System.Decimal> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m9C5A82B4C90793C63E27A931BEC8079FFE25520B ();
// 0x00000707 System.Nullable`1<System.Double> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m4BE5B58A0958EBF7256366D8296B6DA1EDF843EF ();
// 0x00000708 System.Nullable`1<System.Char> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mC0B29E3415EBF2088886D0DF43F1CD300428E710 ();
// 0x00000709 System.Int32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m37836E68C6B2F2C858D41CE2B223D5BF9DCB8A95 ();
// 0x0000070A System.Int16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m14BAD7E9A209B005A206AFB6C0A323CFCB7C1F70 ();
// 0x0000070B System.UInt16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mBFCD6094795E2F5B0E25C72EED7AA1E39E123E5A ();
// 0x0000070C System.Char Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mB6AEC1B995D857BC88DAE4D18633D4C1A59FF244 ();
// 0x0000070D System.Byte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m2BE76B72CEE194077BD493FB97B18ED73796BCFC ();
// 0x0000070E System.SByte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m68CD35D0E529644D3F9445B86270C01A4ED25A90 ();
// 0x0000070F System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mDC2F81A96F4E25A31149127EC4553EEBB6CEAE5A ();
// 0x00000710 System.Nullable`1<System.Int16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m4C80AE2A668E862BA26C64A82D8CDA73AC3926BC ();
// 0x00000711 System.Nullable`1<System.UInt16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m1D36670A16FCF5B2D120491806A86CA98F546337 ();
// 0x00000712 System.Nullable`1<System.Byte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m74182DF8F6B9DC3CCBFB214EF9BB49A55D3F3960 ();
// 0x00000713 System.Nullable`1<System.SByte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m71913A91611A2F3E8F8E9BAF2156DB72D2FFB416 ();
// 0x00000714 System.DateTime Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m49944554DC46B1A3D06744EEE9E1A469DE72F4BF ();
// 0x00000715 System.Nullable`1<System.Int64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m0E65643D032EE4B64024097B70742778ACD273B9 ();
// 0x00000716 System.Nullable`1<System.Single> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m1476837E7785C4531A81BC92373B597CEA94809B ();
// 0x00000717 System.Decimal Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m36B34442280075D1E346D08F92F54D9EFFA91FE0 ();
// 0x00000718 System.Nullable`1<System.UInt32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mC5A51D2B7BFAE090B3964AE80E5F3262C8AC96FA ();
// 0x00000719 System.Nullable`1<System.UInt64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m263B524CBD1A0BF448DA66F9AB45404703490265 ();
// 0x0000071A System.Double Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mCAD4BF3F8A1F8D9F539AF771B7729DB6F87BE3DA ();
// 0x0000071B System.Single Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m3221C59CA06D7231FC820752906F87C8704AABCA ();
// 0x0000071C System.String Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m055FB3BB521C482D722E6230E2EAAF6FA67CDD61 ();
// 0x0000071D System.UInt32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mF65CE06C658E2F5C618D89638560FD1C46805B10 ();
// 0x0000071E System.UInt64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m69BF66AD9E90A7B9B2A2FE51C12A7F1B7AD3F575 ();
// 0x0000071F System.Guid Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mBCBCF67BF5AB3ACC55BF4AC2D6FEC9A53B4B3C02 ();
// 0x00000720 System.Nullable`1<System.Guid> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mD301899BF3696A96E489097A084A6A7FC54839BE ();
// 0x00000721 System.TimeSpan Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mB312ABC861D909D196630522545CFC8C6A046749 ();
// 0x00000722 System.Nullable`1<System.TimeSpan> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mAEF05510845F680C02EA58B2EDDE52CCB8132174 ();
// 0x00000723 System.Uri Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mBF0AC1F8D5A3C4D105029909F90571014E7895A1 ();
// 0x00000724 System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken::System.Collections.IEnumerable.GetEnumerator()
extern void JToken_System_Collections_IEnumerable_GetEnumerator_mF30D971A6AD327ECA26A1BBF80195D7F5F4BE457 ();
// 0x00000725 System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern void JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m621DF86EEC5E33CB74751C9CAB075218CB5F3CC8 ();
// 0x00000726 Newtonsoft.Json.JsonReader Newtonsoft.Json.Linq.JToken::CreateReader()
extern void JToken_CreateReader_m10C04AA980CFFE57EA4470535BC159C02482A484 ();
// 0x00000727 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::FromObjectInternal(System.Object,Newtonsoft.Json.JsonSerializer)
extern void JToken_FromObjectInternal_m48B6348922E3A9EC4621AF403499669F5CF6834F ();
// 0x00000728 System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type)
extern void JToken_ToObject_mCC5082A54F55BB82E647D5E7DDA7394A0EF6C906 ();
// 0x00000729 System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type,Newtonsoft.Json.JsonSerializer)
extern void JToken_ToObject_mB6FF6C6C8AAC566BA8945F12308468810307981B ();
// 0x0000072A Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader)
extern void JToken_ReadFrom_mDC1F3EB1D1A8C2F73A8108BD3A7A73CF9373B867 ();
// 0x0000072B Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JToken_ReadFrom_m53AD4309BEAF4E238E7365DBC072B72A0159A1E1 ();
// 0x0000072C System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(Newtonsoft.Json.IJsonLineInfo,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JToken_SetLineInfo_m5ECC618ACC1BD50BC776327D19895F1507FCD01E ();
// 0x0000072D System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(System.Int32,System.Int32)
extern void JToken_SetLineInfo_m7A7A05CA2DEE523E5696483E4AADB98A7322A66D ();
// 0x0000072E System.Boolean Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m8DD08E1800E92F0E20B129E4A94E304088AEACAD ();
// 0x0000072F System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m7F4D09A3F0A1DA43DFAD1972DD80E0651A405CB9 ();
// 0x00000730 System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_mA5D67376DE59AF964E526528FF5FA7B3FC6DB4AE ();
// 0x00000731 System.Object Newtonsoft.Json.Linq.JToken::System.ICloneable.Clone()
extern void JToken_System_ICloneable_Clone_m82B5A98B979BB3FE1BCFACEBB1E977257D03E016 ();
// 0x00000732 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::DeepClone()
extern void JToken_DeepClone_m3D45D1CE0709D971773A03235F4FA5FD271B4380 ();
// 0x00000733 System.Void Newtonsoft.Json.Linq.JToken::AddAnnotation(System.Object)
extern void JToken_AddAnnotation_m25239875C749DEA9A93D75B0B784E68A5A3C9202 ();
// 0x00000734 T Newtonsoft.Json.Linq.JToken::Annotation()
// 0x00000735 System.Void Newtonsoft.Json.Linq.JToken::.cctor()
extern void JToken__cctor_mD2A5A64AC56266D9F970C3A433F571CCAC116DFF ();
// 0x00000736 System.Void Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoAnnotation__ctor_m86D35ED7EBB6C1C02B79695434C2D77DEAFFB001 ();
// 0x00000737 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::get_CurrentToken()
extern void JTokenReader_get_CurrentToken_m7F7C069ACA2894F6119385C6335D25D0E5C7720F ();
// 0x00000738 System.Void Newtonsoft.Json.Linq.JTokenReader::.ctor(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader__ctor_mF5021811DC9CB1A60B27ABF1AF1AE33CF08343B1 ();
// 0x00000739 System.Boolean Newtonsoft.Json.Linq.JTokenReader::Read()
extern void JTokenReader_Read_mEDD64BC892569E5BEF98900E73C069D2E8C32AEE ();
// 0x0000073A System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadOver(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader_ReadOver_m77159EDED761655EE024062B53E03A6E62078951 ();
// 0x0000073B System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadToEnd()
extern void JTokenReader_ReadToEnd_m2DDDD21DB0622142D517951F25D86D6D833CCB1C ();
// 0x0000073C System.Nullable`1<Newtonsoft.Json.JsonToken> Newtonsoft.Json.Linq.JTokenReader::GetEndToken(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_GetEndToken_mE2D7095B9770997E3D561E683FAEC15A3EF177E2 ();
// 0x0000073D System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadInto(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_ReadInto_mAB6C0E1E236E0753AF6CA7F64D8A086F97C2C4BC ();
// 0x0000073E System.Boolean Newtonsoft.Json.Linq.JTokenReader::SetEnd(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_SetEnd_mAB383DEA935830415D973B4ACD99751533E2E6E7 ();
// 0x0000073F System.Void Newtonsoft.Json.Linq.JTokenReader::SetToken(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader_SetToken_mF292AB5EEFA1ADD3B536227B3D98CE6AFD5DF750 ();
// 0x00000740 System.String Newtonsoft.Json.Linq.JTokenReader::SafeToString(System.Object)
extern void JTokenReader_SafeToString_m806AE1321BE07BFF1F419B3205D9B7525EE3C51F ();
// 0x00000741 System.Boolean Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mD8BC39B3CC8251DC7D57F03C2378717672F695FE ();
// 0x00000742 System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_mBB4CD5D75F64B74B92904A66F7CB43BE5DC04241 ();
// 0x00000743 System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m44849CF962C5C4C211A056F110A82D5B0DC1B7CB ();
// 0x00000744 System.String Newtonsoft.Json.Linq.JTokenReader::get_Path()
extern void JTokenReader_get_Path_m5587DC075EFCB65F9A82236250A4C3D6669CF71C ();
// 0x00000745 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::get_Token()
extern void JTokenWriter_get_Token_mC7CA1C3AEC7D8E9D7F89B4E468615BC2BC9A772F ();
// 0x00000746 System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor()
extern void JTokenWriter__ctor_mE708378D36BD7374178413098625C6A009626AD4 ();
// 0x00000747 System.Void Newtonsoft.Json.Linq.JTokenWriter::Close()
extern void JTokenWriter_Close_mDDD447FEAAB5543915E90079390CAA71FCAD8F11 ();
// 0x00000748 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartObject()
extern void JTokenWriter_WriteStartObject_m043EC4E2822323930EA7EF027092C435C401C6B7 ();
// 0x00000749 System.Void Newtonsoft.Json.Linq.JTokenWriter::AddParent(Newtonsoft.Json.Linq.JContainer)
extern void JTokenWriter_AddParent_mEA2C8D9170ECEC24B9EC5CF585CFA3E031D8F1D7 ();
// 0x0000074A System.Void Newtonsoft.Json.Linq.JTokenWriter::RemoveParent()
extern void JTokenWriter_RemoveParent_mA5DF944205B7295C6DC677864498FE86860EC5DE ();
// 0x0000074B System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartArray()
extern void JTokenWriter_WriteStartArray_m91B24F2ADDB19449F1BC1426B4B6EE372463D35F ();
// 0x0000074C System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartConstructor(System.String)
extern void JTokenWriter_WriteStartConstructor_m05C7F0A3D87CCC72026DAC7779C672FFACB99C1E ();
// 0x0000074D System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JTokenWriter_WriteEnd_mB89E18CA5BBC9C21D95003B693FF61A0D5CE8CDE ();
// 0x0000074E System.Void Newtonsoft.Json.Linq.JTokenWriter::WritePropertyName(System.String)
extern void JTokenWriter_WritePropertyName_m65859AA2B00205D2FFF1CBDBC9B8421A4F6FBC6F ();
// 0x0000074F System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(System.Object,Newtonsoft.Json.JsonToken)
extern void JTokenWriter_AddValue_mB25673EF93F183311280BAC9ED4AAA67556E674E ();
// 0x00000750 System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.JsonToken)
extern void JTokenWriter_AddValue_m517F1A7FC2B715C754D4E4C2BFDB242A21A12363 ();
// 0x00000751 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteNull()
extern void JTokenWriter_WriteNull_m1E67B093D7C63966CC409C3DEE37D7A20AB03BB5 ();
// 0x00000752 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteUndefined()
extern void JTokenWriter_WriteUndefined_m8B22F25C9BA63358DB23A12896538CE217BEA12B ();
// 0x00000753 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteRaw(System.String)
extern void JTokenWriter_WriteRaw_mF344420866E341558043DC20F08007E9B65F6005 ();
// 0x00000754 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteComment(System.String)
extern void JTokenWriter_WriteComment_m86822BF5FDD42F67F342DD6B0B30D5B3778B702C ();
// 0x00000755 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.String)
extern void JTokenWriter_WriteValue_m87566D68600CCF40D66BB79566C599501078C6B3 ();
// 0x00000756 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int32)
extern void JTokenWriter_WriteValue_mCBE08FD5727E6C0104BB5A2D0B79112DFC62015D ();
// 0x00000757 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt32)
extern void JTokenWriter_WriteValue_m59C7BAE67F8D310F42EA149AEB58B05BEF80F650 ();
// 0x00000758 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int64)
extern void JTokenWriter_WriteValue_m4FBFD455D4B9D2E37B8F4DDDBF4CAC317E118A93 ();
// 0x00000759 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt64)
extern void JTokenWriter_WriteValue_m9E17F66F8FC5F437D385A1163E86137973594F1D ();
// 0x0000075A System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Single)
extern void JTokenWriter_WriteValue_mB0BD1A934417381A8092A0784D11B7F2110421E8 ();
// 0x0000075B System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Double)
extern void JTokenWriter_WriteValue_mECC633614ED365707BFF0A817D5D5A4C9875A5F4 ();
// 0x0000075C System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Boolean)
extern void JTokenWriter_WriteValue_m1D65686F3200734354ED53467CDB541551ED201F ();
// 0x0000075D System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int16)
extern void JTokenWriter_WriteValue_mE076BAC613245C0315B549D0AD9CA7D0452384D1 ();
// 0x0000075E System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt16)
extern void JTokenWriter_WriteValue_mB945C35A6832983C1D073FC2540834585798CF70 ();
// 0x0000075F System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Char)
extern void JTokenWriter_WriteValue_mE949C9EE37632542B26B1F929A57FFF15B02A15F ();
// 0x00000760 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte)
extern void JTokenWriter_WriteValue_m8E79C0B7E01213DE6B9C7A6973A54451CF470C0D ();
// 0x00000761 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.SByte)
extern void JTokenWriter_WriteValue_mACD883C8DA509761CABF3A9566D41B7A1CC33C17 ();
// 0x00000762 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Decimal)
extern void JTokenWriter_WriteValue_m0769B4F650E63DEE57E0F02275AF14BA61357DBD ();
// 0x00000763 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTime)
extern void JTokenWriter_WriteValue_m2E00E57DA415557ACF7B7BAC3F0F3AEF7C5C9007 ();
// 0x00000764 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte[])
extern void JTokenWriter_WriteValue_m9B28464CBFBEF2E59194312BBAB87F6D8BFFCCF1 ();
// 0x00000765 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.TimeSpan)
extern void JTokenWriter_WriteValue_m8E943B4508E0A22822929246CE42C89653BFD77C ();
// 0x00000766 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Guid)
extern void JTokenWriter_WriteValue_m36A0F291604A5F966F1FB3B1BF8E812DA6B1F3FB ();
// 0x00000767 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Uri)
extern void JTokenWriter_WriteValue_m5EA172E431ADA548DBFD630E5FA1328C1604A004 ();
// 0x00000768 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean,System.Boolean,System.Boolean)
extern void JTokenWriter_WriteToken_m33571582CEA55F3D6568A397078C3950989B585C ();
// 0x00000769 System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object,Newtonsoft.Json.Linq.JTokenType)
extern void JValue__ctor_mAA1714A39AAFE0EEBFCB5FF3AEACC8268AC57FF3 ();
// 0x0000076A System.Void Newtonsoft.Json.Linq.JValue::.ctor(Newtonsoft.Json.Linq.JValue)
extern void JValue__ctor_m9544423AD14FC33210E2C5CBA810E73EB2310945 ();
// 0x0000076B System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object)
extern void JValue__ctor_mC938EEF912C5927E0F3E6921EEAEA35A9B3905A1 ();
// 0x0000076C System.Boolean Newtonsoft.Json.Linq.JValue::get_HasValues()
extern void JValue_get_HasValues_mDB37190AFDD95FF4DCBB11DD04A8132081AF84C9 ();
// 0x0000076D System.Int32 Newtonsoft.Json.Linq.JValue::Compare(Newtonsoft.Json.Linq.JTokenType,System.Object,System.Object)
extern void JValue_Compare_m5332270DC40488C0210555F6A886B3DDD6C0E671 ();
// 0x0000076E System.Int32 Newtonsoft.Json.Linq.JValue::CompareFloat(System.Object,System.Object)
extern void JValue_CompareFloat_mC2342A9C2EED12E11498088F3D3922F9494CBA34 ();
// 0x0000076F Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JValue::CloneToken()
extern void JValue_CloneToken_m04C61BB65B0589EDE3C6F0DD9B8D95569F2BC076 ();
// 0x00000770 Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateComment(System.String)
extern void JValue_CreateComment_m0173F97848C247164C2FBEC4AC5C01457360D857 ();
// 0x00000771 Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateNull()
extern void JValue_CreateNull_m8D0F45F7CF0B9B2DA5622EF413BDF9490FCDF49F ();
// 0x00000772 Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateUndefined()
extern void JValue_CreateUndefined_m0D7F8EA0F311AE96655A0E1F141FCA51D52F503A ();
// 0x00000773 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>,System.Object)
extern void JValue_GetValueType_mD61D369AE1B3B7238DAE959CCFEDCFD43D8EBC59 ();
// 0x00000774 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetStringValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>)
extern void JValue_GetStringValueType_m525F59416003AD7A696DF51CF06BF89C7A117AF9 ();
// 0x00000775 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::get_Type()
extern void JValue_get_Type_m6F26A2477D7CCB12AC3DBF2279F59D0DB6FB554B ();
// 0x00000776 System.Object Newtonsoft.Json.Linq.JValue::get_Value()
extern void JValue_get_Value_m0BCF7C89E8206431E18221C584A8F42BACC0F303 ();
// 0x00000777 System.Void Newtonsoft.Json.Linq.JValue::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JValue_WriteTo_m789007675E2310ECCD89F3316690DF91F399EEF9 ();
// 0x00000778 System.Boolean Newtonsoft.Json.Linq.JValue::ValuesEquals(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.Linq.JValue)
extern void JValue_ValuesEquals_mFB9B7F1F014496C897537E327F36CC1F85DAB38D ();
// 0x00000779 System.Boolean Newtonsoft.Json.Linq.JValue::Equals(Newtonsoft.Json.Linq.JValue)
extern void JValue_Equals_m31472D11FC02641A2C1C0BAFCE0379BD0CDE0574 ();
// 0x0000077A System.Boolean Newtonsoft.Json.Linq.JValue::Equals(System.Object)
extern void JValue_Equals_m1D2EF4D88545ECC0DA7D080DD3DC775D42A3144D ();
// 0x0000077B System.Int32 Newtonsoft.Json.Linq.JValue::GetHashCode()
extern void JValue_GetHashCode_m42ED70FA2F98B078CDE3BD981F1F48F40CF55D6E ();
// 0x0000077C System.String Newtonsoft.Json.Linq.JValue::ToString()
extern void JValue_ToString_mC90CFBFC6CB0782746643B1E86B58F79F532FF33 ();
// 0x0000077D System.String Newtonsoft.Json.Linq.JValue::ToString(System.IFormatProvider)
extern void JValue_ToString_m5F830DD4FC2F8DFDE0D4DEDFBB3C6A0DB47DAD9A ();
// 0x0000077E System.String Newtonsoft.Json.Linq.JValue::ToString(System.String,System.IFormatProvider)
extern void JValue_ToString_m11E52A801E3CD2482E823BBFEB234BD2E5BD1774 ();
// 0x0000077F System.Int32 Newtonsoft.Json.Linq.JValue::System.IComparable.CompareTo(System.Object)
extern void JValue_System_IComparable_CompareTo_mFD51718B3036C0849AFDB67C86E883530ABC9B1E ();
// 0x00000780 System.Int32 Newtonsoft.Json.Linq.JValue::CompareTo(Newtonsoft.Json.Linq.JValue)
extern void JValue_CompareTo_m2EF2F2E751D7669E598B930447F3065D2C074EB3 ();
// 0x00000781 System.TypeCode Newtonsoft.Json.Linq.JValue::System.IConvertible.GetTypeCode()
extern void JValue_System_IConvertible_GetTypeCode_m33F0B2BC4A8783389A0AF0E633C4B059F0D1674D ();
// 0x00000782 System.Boolean Newtonsoft.Json.Linq.JValue::System.IConvertible.ToBoolean(System.IFormatProvider)
extern void JValue_System_IConvertible_ToBoolean_m26D2B7F5C771CA926B9B96378FCB61613FAB7586 ();
// 0x00000783 System.Char Newtonsoft.Json.Linq.JValue::System.IConvertible.ToChar(System.IFormatProvider)
extern void JValue_System_IConvertible_ToChar_m351A5394085AA3DB43B337FB4A736AC75404D7ED ();
// 0x00000784 System.SByte Newtonsoft.Json.Linq.JValue::System.IConvertible.ToSByte(System.IFormatProvider)
extern void JValue_System_IConvertible_ToSByte_m3FC667C211F8E5AE58E8655B1B823E8D7FE7AFEF ();
// 0x00000785 System.Byte Newtonsoft.Json.Linq.JValue::System.IConvertible.ToByte(System.IFormatProvider)
extern void JValue_System_IConvertible_ToByte_mED376C6F520C603020D684EFFD454EE1D67D02D9 ();
// 0x00000786 System.Int16 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt16(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt16_m02A3BB93787861864706D4557BD87C6A985B9539 ();
// 0x00000787 System.UInt16 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt16(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt16_mC85CDD8AB0B381CF687AE522FF851862E2F60CCB ();
// 0x00000788 System.Int32 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt32(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt32_m6436DA2A444CA7ECF08DD3C97FC3D5C698479574 ();
// 0x00000789 System.UInt32 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt32(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt32_m2A365952FD605D5529B4EE04303BAC23DB236024 ();
// 0x0000078A System.Int64 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt64(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt64_mA013A28DEE59887372EA184BD75E77CA7CE6FAF0 ();
// 0x0000078B System.UInt64 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt64(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt64_m7AA8CB9A899E8CCCCE16C2FED5EE90AD7F919CD2 ();
// 0x0000078C System.Single Newtonsoft.Json.Linq.JValue::System.IConvertible.ToSingle(System.IFormatProvider)
extern void JValue_System_IConvertible_ToSingle_mEB51FE9A00AC598ADA2E9CB1F45456BCE18A8285 ();
// 0x0000078D System.Double Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDouble(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDouble_m873731133E7F8D965501FEB11273EE2109FA10C3 ();
// 0x0000078E System.Decimal Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDecimal(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDecimal_m1D354E5D5F56358215F341FB9BDC3600DE70D88C ();
// 0x0000078F System.DateTime Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDateTime(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDateTime_mD6FA83CAA5C96DF8360C601165ACFC4116879C6B ();
// 0x00000790 System.Object Newtonsoft.Json.Linq.JValue::System.IConvertible.ToType(System.Type,System.IFormatProvider)
extern void JValue_System_IConvertible_ToType_mC4E9F308FC745E5DFBDBD052AA618895B5976DFE ();
// 0x00000791 System.Void Newtonsoft.Json.Converters.BinaryConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_WriteJson_m899DB88862BDC17676CBF54E3D3BE25065373983 ();
// 0x00000792 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::GetByteArray(System.Object)
extern void BinaryConverter_GetByteArray_m7952735C5C4857A2EBF0A4EDBFE1D1F04EA38D73 ();
// 0x00000793 System.Object Newtonsoft.Json.Converters.BinaryConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_ReadJson_m1DCB8CBA1AA2CA80459361CC24B22CBBCDDF63DB ();
// 0x00000794 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::ReadByteArray(Newtonsoft.Json.JsonReader)
extern void BinaryConverter_ReadByteArray_m3E2BCE7FE0975482B74A7985560EB32E96E52EB1 ();
// 0x00000795 System.Boolean Newtonsoft.Json.Converters.BinaryConverter::CanConvert(System.Type)
extern void BinaryConverter_CanConvert_m24F66AE4B927162B48C3B6622103622404D52745 ();
// 0x00000796 System.Void Newtonsoft.Json.Converters.BinaryConverter::.ctor()
extern void BinaryConverter__ctor_m880F324192C6DF99C1B09B8135274F9DA3DC8569 ();
// 0x00000797 System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BsonObjectIdConverter_WriteJson_m8E50EDC6DBC50E814E9B11E3229B535897D08B2E ();
// 0x00000798 System.Object Newtonsoft.Json.Converters.BsonObjectIdConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BsonObjectIdConverter_ReadJson_m8C9673BD23276ABF49700F49BA007F974D7AB05E ();
// 0x00000799 System.Boolean Newtonsoft.Json.Converters.BsonObjectIdConverter::CanConvert(System.Type)
extern void BsonObjectIdConverter_CanConvert_m6B80161E4F3B26BE9B61067189EE2C750BE547A4 ();
// 0x0000079A System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::.ctor()
extern void BsonObjectIdConverter__ctor_mC16ECF13AD025A5BB98707D0109968287F25B3B1 ();
// 0x0000079B System.Void Newtonsoft.Json.Converters.DataSetConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataSetConverter_WriteJson_m88B7F775AC173099615EB9D57E98883821E4C954 ();
// 0x0000079C System.Object Newtonsoft.Json.Converters.DataSetConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataSetConverter_ReadJson_m4CE6E6DA7353EB2E45F307858BB6679F0E96AEF6 ();
// 0x0000079D System.Boolean Newtonsoft.Json.Converters.DataSetConverter::CanConvert(System.Type)
extern void DataSetConverter_CanConvert_m8DF1F7CAE7D162765C12516BDE2BBE0C9E95CB5D ();
// 0x0000079E System.Void Newtonsoft.Json.Converters.DataSetConverter::.ctor()
extern void DataSetConverter__ctor_mE871E420ED932761B56B8D362A7529A92A6C7123 ();
// 0x0000079F System.Void Newtonsoft.Json.Converters.DataTableConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_WriteJson_mE2FB424E7789B17ADB47805C12516457360A8BB3 ();
// 0x000007A0 System.Object Newtonsoft.Json.Converters.DataTableConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_ReadJson_m828E183BB040932E880EADDDAA27C61662110EA7 ();
// 0x000007A1 System.Void Newtonsoft.Json.Converters.DataTableConverter::CreateRow(Newtonsoft.Json.JsonReader,System.Data.DataTable,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_CreateRow_mD32680D74BB98B88DD2E0844F8D11C5E82F2C655 ();
// 0x000007A2 System.Type Newtonsoft.Json.Converters.DataTableConverter::GetColumnDataType(Newtonsoft.Json.JsonReader)
extern void DataTableConverter_GetColumnDataType_m90F040CC6C8C9851BA00ABEE428ABD23D90B33D0 ();
// 0x000007A3 System.Boolean Newtonsoft.Json.Converters.DataTableConverter::CanConvert(System.Type)
extern void DataTableConverter_CanConvert_m06429F25BDD1E1807F96499925E861963499A018 ();
// 0x000007A4 System.Void Newtonsoft.Json.Converters.DataTableConverter::.ctor()
extern void DataTableConverter__ctor_mB4FF21BFD7AC36D1CFE7304AB3CBBBD1E846DFD0 ();
// 0x000007A5 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.KeyValuePairConverter::InitializeReflectionObject(System.Type)
extern void KeyValuePairConverter_InitializeReflectionObject_mF6B897585DDC2C8CAF1B48BDC43CCC8023456646 ();
// 0x000007A6 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_WriteJson_m0CED8EB1BDF8F17BE4B8A1E660ED20D0C66224E3 ();
// 0x000007A7 System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_ReadJson_m527C301B095BCAA07EFD6F33C0274E2D046B9E96 ();
// 0x000007A8 System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::CanConvert(System.Type)
extern void KeyValuePairConverter_CanConvert_m09C997BF6C652E25AD2C7E557B96BE93DDF70FB8 ();
// 0x000007A9 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.ctor()
extern void KeyValuePairConverter__ctor_m40A70CC0B8C6E5FEC04284442C1738ED8B3DBCFB ();
// 0x000007AA System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.cctor()
extern void KeyValuePairConverter__cctor_mAEADE960BA0FB7938A15AF8998768D0465EF544B ();
// 0x000007AB System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_m28A08CC221C21E4322B70332F6333A849BCF6A1A ();
// 0x000007AC System.Boolean Newtonsoft.Json.Converters.RegexConverter::HasFlag(System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.RegexOptions)
extern void RegexConverter_HasFlag_m15B38A1E7546D024E518BAE8CEB7F3E85BF81733 ();
// 0x000007AD System.Void Newtonsoft.Json.Converters.RegexConverter::WriteBson(Newtonsoft.Json.Bson.BsonWriter,System.Text.RegularExpressions.Regex)
extern void RegexConverter_WriteBson_m1F347B3189F596B6B853257225CC027FE0978131 ();
// 0x000007AE System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Text.RegularExpressions.Regex,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_mBE8C5033A3356A10C15199029C5E7032387FCB8D ();
// 0x000007AF System.Object Newtonsoft.Json.Converters.RegexConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadJson_mEECB5D78296EE41FFBE81AABD50BB1D4ECCC4A2D ();
// 0x000007B0 System.Object Newtonsoft.Json.Converters.RegexConverter::ReadRegexString(Newtonsoft.Json.JsonReader)
extern void RegexConverter_ReadRegexString_m9CEC5177A492877F21D2D14ED010CDFA96606430 ();
// 0x000007B1 System.Text.RegularExpressions.Regex Newtonsoft.Json.Converters.RegexConverter::ReadRegexObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadRegexObject_mD16834F88889BD9192D580AB92CE2212AF72DE0B ();
// 0x000007B2 System.Boolean Newtonsoft.Json.Converters.RegexConverter::CanConvert(System.Type)
extern void RegexConverter_CanConvert_m4F245BB9F07B23E68DECF5D5112988779B2450DB ();
// 0x000007B3 System.Boolean Newtonsoft.Json.Converters.RegexConverter::IsRegex(System.Type)
extern void RegexConverter_IsRegex_m5551BD5E6D5CF0716FBEAC57D963268822C10E97 ();
// 0x000007B4 System.Void Newtonsoft.Json.Converters.RegexConverter::.ctor()
extern void RegexConverter__ctor_m11EC732064BF56396A4B2E766F8AF04BD8D22B13 ();
// 0x000007B5 System.Void Newtonsoft.Json.Converters.XmlDocumentWrapper::.ctor(System.Xml.XmlDocument)
extern void XmlDocumentWrapper__ctor_m39414686A021603CB51D7F7A01497FA265A7846E ();
// 0x000007B6 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateComment(System.String)
extern void XmlDocumentWrapper_CreateComment_mD27A46753AA66ACC362D270FC783EDADBD8216CC ();
// 0x000007B7 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateTextNode(System.String)
extern void XmlDocumentWrapper_CreateTextNode_mEAE85E9CD2540BDF661E4DC7F4C653FBF1695A8B ();
// 0x000007B8 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateCDataSection(System.String)
extern void XmlDocumentWrapper_CreateCDataSection_m75553E99729AAF106BA58F9831062B36018D3AE9 ();
// 0x000007B9 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateWhitespace(System.String)
extern void XmlDocumentWrapper_CreateWhitespace_m9EAECA84A85D87DD76E885A7E29C37D539FF6FEB ();
// 0x000007BA Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateSignificantWhitespace(System.String)
extern void XmlDocumentWrapper_CreateSignificantWhitespace_m81BF520F63EAE66D0E005985D8766BAA9AC64274 ();
// 0x000007BB Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateXmlDeclaration(System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateXmlDeclaration_mD9957E0A3599CC7DA587836EE36EF3252FC3DF41 ();
// 0x000007BC Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateXmlDocumentType(System.String,System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateXmlDocumentType_m8935B277505D34002BE90566B9B45A9280AD366C ();
// 0x000007BD Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateProcessingInstruction(System.String,System.String)
extern void XmlDocumentWrapper_CreateProcessingInstruction_m9960B4667DC37C38E76F1BAC0FD2C7C3F03EA96C ();
// 0x000007BE Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String)
extern void XmlDocumentWrapper_CreateElement_mE1410CE9D81A971AE7B4C0DB638D40FFC8C54F70 ();
// 0x000007BF Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String,System.String)
extern void XmlDocumentWrapper_CreateElement_m71D322ACBBB39B183E356BAA7B35C60939C74283 ();
// 0x000007C0 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String)
extern void XmlDocumentWrapper_CreateAttribute_mDFD7498EDFB62B93811E427296A6F949671B79BF ();
// 0x000007C1 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateAttribute_m2280FA87AB60EF17E5A8E4946CC6184EF45975C0 ();
// 0x000007C2 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::get_DocumentElement()
extern void XmlDocumentWrapper_get_DocumentElement_m9D380A0F6DDECAE020D35744E08E635C66598CEE ();
// 0x000007C3 System.Void Newtonsoft.Json.Converters.XmlElementWrapper::.ctor(System.Xml.XmlElement)
extern void XmlElementWrapper__ctor_m3BFEBF542E6C15FD1FC91C7CD707F048ED3F6843 ();
// 0x000007C4 System.Void Newtonsoft.Json.Converters.XmlElementWrapper::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlElementWrapper_SetAttributeNode_mCECB85254A7A4A92C20BC7317F29FABAB43346E0 ();
// 0x000007C5 System.Boolean Newtonsoft.Json.Converters.XmlElementWrapper::get_IsEmpty()
extern void XmlElementWrapper_get_IsEmpty_m21E7ED10C65320CA265D6E8F403C7D0918991C9E ();
// 0x000007C6 System.Void Newtonsoft.Json.Converters.XmlDeclarationWrapper::.ctor(System.Xml.XmlDeclaration)
extern void XmlDeclarationWrapper__ctor_m3E7E9BCB4EE576BB5DC0A85A32A5FBA5BCCAE533 ();
// 0x000007C7 System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Version()
extern void XmlDeclarationWrapper_get_Version_m905E92A648FC289AD71C9FA16F10DA7C317C9910 ();
// 0x000007C8 System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Encoding()
extern void XmlDeclarationWrapper_get_Encoding_mC18EE3C1E94D6D9D09CE7A91ACDE3DA4384DC3E3 ();
// 0x000007C9 System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Standalone()
extern void XmlDeclarationWrapper_get_Standalone_m7313C136E2FC0F60B512D42139DFF24CBFC69D57 ();
// 0x000007CA System.Void Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::.ctor(System.Xml.XmlDocumentType)
extern void XmlDocumentTypeWrapper__ctor_m0102D76E30C7DA96D44DABACEE4670CC9A959831 ();
// 0x000007CB System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_Name()
extern void XmlDocumentTypeWrapper_get_Name_m8DB8017FDD858946B565C6AE5F0F11671EF40B93 ();
// 0x000007CC System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_System()
extern void XmlDocumentTypeWrapper_get_System_mCA5FB6E2FEBB6BD9971D339EB774C8857313B296 ();
// 0x000007CD System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_Public()
extern void XmlDocumentTypeWrapper_get_Public_m31B703F9C653F64B414626FD6AF162243AB8ED77 ();
// 0x000007CE System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_InternalSubset()
extern void XmlDocumentTypeWrapper_get_InternalSubset_mEAFA9B362B062DA6B398C6BE17A409AF3F27388D ();
// 0x000007CF System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_LocalName()
extern void XmlDocumentTypeWrapper_get_LocalName_mD5CB85308E85555EA1F2EFC4AD96B55F7E725DAE ();
// 0x000007D0 System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::.ctor(System.Xml.XmlNode)
extern void XmlNodeWrapper__ctor_m19CAB1F86B0AA927AB9DF3E5E42C1CE41A3957AD ();
// 0x000007D1 System.Object Newtonsoft.Json.Converters.XmlNodeWrapper::get_WrappedNode()
extern void XmlNodeWrapper_get_WrappedNode_m052F46C5326C8E3030F90179FF742C033DFC7AFC ();
// 0x000007D2 System.Xml.XmlNodeType Newtonsoft.Json.Converters.XmlNodeWrapper::get_NodeType()
extern void XmlNodeWrapper_get_NodeType_mB47DF851140D7F28469377337D51BE05D065A871 ();
// 0x000007D3 System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_LocalName()
extern void XmlNodeWrapper_get_LocalName_m8EFCAA525828DD5B3B30D2D59B23D81F2DFA43D6 ();
// 0x000007D4 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_ChildNodes()
extern void XmlNodeWrapper_get_ChildNodes_m881A29B6E1779876FC175D1490C6ECAE24BC32C6 ();
// 0x000007D5 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::WrapNode(System.Xml.XmlNode)
extern void XmlNodeWrapper_WrapNode_mB7F16C56365196EF10E43D5E8CD2998D3764DA3D ();
// 0x000007D6 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_Attributes()
extern void XmlNodeWrapper_get_Attributes_m3D6D29BDB703EE2F67CE3854F6A777F4624F9F3A ();
// 0x000007D7 System.Boolean Newtonsoft.Json.Converters.XmlNodeWrapper::get_HasAttributes()
extern void XmlNodeWrapper_get_HasAttributes_m55E2612C26D3ABBB773C7006514781A18B7C1B52 ();
// 0x000007D8 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::get_ParentNode()
extern void XmlNodeWrapper_get_ParentNode_mCDAB2FDEA0B91F875953C045B2765C550FEA3FD0 ();
// 0x000007D9 System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Value()
extern void XmlNodeWrapper_get_Value_mABD94104C0453130E5ED458B993533A8AD5DE33E ();
// 0x000007DA System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::set_Value(System.String)
extern void XmlNodeWrapper_set_Value_m4431F0E09A6C097D3240542ED42E00E431558C53 ();
// 0x000007DB Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeWrapper_AppendChild_mC4477C47A87D17048CDF613DF54A652493BFB4F1 ();
// 0x000007DC System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_NamespaceUri()
extern void XmlNodeWrapper_get_NamespaceUri_mDD627CF0980E0A359F99000EEF0A4839F3ACF613 ();
// 0x000007DD Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateComment(System.String)
// 0x000007DE Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateTextNode(System.String)
// 0x000007DF Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateCDataSection(System.String)
// 0x000007E0 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateWhitespace(System.String)
// 0x000007E1 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateSignificantWhitespace(System.String)
// 0x000007E2 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateXmlDeclaration(System.String,System.String,System.String)
// 0x000007E3 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateXmlDocumentType(System.String,System.String,System.String,System.String)
// 0x000007E4 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateProcessingInstruction(System.String,System.String)
// 0x000007E5 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::CreateElement(System.String)
// 0x000007E6 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::CreateElement(System.String,System.String)
// 0x000007E7 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateAttribute(System.String,System.String)
// 0x000007E8 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateAttribute(System.String,System.String,System.String)
// 0x000007E9 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::get_DocumentElement()
// 0x000007EA System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Version()
// 0x000007EB System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Encoding()
// 0x000007EC System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Standalone()
// 0x000007ED System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_Name()
// 0x000007EE System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_System()
// 0x000007EF System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_Public()
// 0x000007F0 System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_InternalSubset()
// 0x000007F1 System.Void Newtonsoft.Json.Converters.IXmlElement::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
// 0x000007F2 System.Boolean Newtonsoft.Json.Converters.IXmlElement::get_IsEmpty()
// 0x000007F3 System.Xml.XmlNodeType Newtonsoft.Json.Converters.IXmlNode::get_NodeType()
// 0x000007F4 System.String Newtonsoft.Json.Converters.IXmlNode::get_LocalName()
// 0x000007F5 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.IXmlNode::get_ChildNodes()
// 0x000007F6 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.IXmlNode::get_Attributes()
// 0x000007F7 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlNode::get_ParentNode()
// 0x000007F8 System.String Newtonsoft.Json.Converters.IXmlNode::get_Value()
// 0x000007F9 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlNode::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
// 0x000007FA System.String Newtonsoft.Json.Converters.IXmlNode::get_NamespaceUri()
// 0x000007FB System.Object Newtonsoft.Json.Converters.IXmlNode::get_WrappedNode()
// 0x000007FC System.String Newtonsoft.Json.Converters.XmlNodeConverter::get_DeserializeRootElementName()
extern void XmlNodeConverter_get_DeserializeRootElementName_m9C1C4C122BFB0146C2B1FD55FAC7C67025443837 ();
// 0x000007FD System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_WriteArrayAttribute()
extern void XmlNodeConverter_get_WriteArrayAttribute_mA4DD1C7C9FA5E48FBD483D870283443CE7A9F7DC ();
// 0x000007FE System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_OmitRootObject()
extern void XmlNodeConverter_get_OmitRootObject_m1AEE7BF96A8AB19E9F7AEFA581F58B03E4A8A337 ();
// 0x000007FF System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void XmlNodeConverter_WriteJson_mFCB227A689CADE1C640299FFFBFAFB11054FE3B5 ();
// 0x00000800 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::WrapXml(System.Object)
extern void XmlNodeConverter_WrapXml_m83B84737527871E02E25F758F33B5D9796E98670 ();
// 0x00000801 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::PushParentNamespaces(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_PushParentNamespaces_m9B8127DAE33916994C214601CAB57E90B9825B5D ();
// 0x00000802 System.String Newtonsoft.Json.Converters.XmlNodeConverter::ResolveFullName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ResolveFullName_mED1FFE1404158C50F26B1D818315D5565C93E6FE ();
// 0x00000803 System.String Newtonsoft.Json.Converters.XmlNodeConverter::GetPropertyName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_GetPropertyName_mF6B8BFB5C2E2781A2F251B2C3B539F25B9C27CDE ();
// 0x00000804 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsArray(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_IsArray_mA2FEF5C9A084820E53DAE5B1D585CE237E6432AA ();
// 0x00000805 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeGroupedNodes(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern void XmlNodeConverter_SerializeGroupedNodes_mB7738BCB8B4A14286A41E7D209CD3302594E77DF ();
// 0x00000806 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteGroupedNodes(Newtonsoft.Json.JsonWriter,System.Xml.XmlNamespaceManager,System.Boolean,System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>,System.String)
extern void XmlNodeConverter_WriteGroupedNodes_m03FCE4A5B98373881CE5C982F2AD654C36FB28F9 ();
// 0x00000807 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteGroupedNodes(Newtonsoft.Json.JsonWriter,System.Xml.XmlNamespaceManager,System.Boolean,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern void XmlNodeConverter_WriteGroupedNodes_m366D624F56D8EEB22D2A16CE1ACF6EF01A7210D2 ();
// 0x00000808 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeNode(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern void XmlNodeConverter_SerializeNode_m0FFA375208C82CE9D7FEDE24DC92B8261E7EEB7B ();
// 0x00000809 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::AllSameName(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_AllSameName_mA00C3E5865E393BA410A2002648523118A69F7E5 ();
// 0x0000080A System.Object Newtonsoft.Json.Converters.XmlNodeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void XmlNodeConverter_ReadJson_m9FF6549303169FD36D81E177C32D5A63ED7766C1 ();
// 0x0000080B System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeValue(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,System.String,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_DeserializeValue_m13B9417E47F2F7C2CAB26A166C29C529043B6032 ();
// 0x0000080C System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadElement_m94F0CF5042D5B05D675A77A4D5F5E4DFEDF728BE ();
// 0x0000080D System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void XmlNodeConverter_CreateElement_m379912D29233C94BE4ABD11A7983F920B636F1FF ();
// 0x0000080E System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddAttribute(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.String,System.Xml.XmlNamespaceManager,System.String)
extern void XmlNodeConverter_AddAttribute_mC4025770145BFB755739CD04B56F6DA72DD67B31 ();
// 0x0000080F System.String Newtonsoft.Json.Converters.XmlNodeConverter::ConvertTokenToXmlValue(Newtonsoft.Json.JsonReader)
extern void XmlNodeConverter_ConvertTokenToXmlValue_mD36CAB0AD07C39FA70DC41A5FB2FFC761154B842 ();
// 0x00000810 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadArrayElements(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.String,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadArrayElements_m68FFB8FF35A9C192363CC8468ED3F0F4372FEDD0 ();
// 0x00000811 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddJsonArrayAttribute(Newtonsoft.Json.Converters.IXmlElement,Newtonsoft.Json.Converters.IXmlDocument)
extern void XmlNodeConverter_AddJsonArrayAttribute_m5310A8F0216B4DCD8721844FF8ECB8DE6ECCC2BA ();
// 0x00000812 System.Collections.Generic.Dictionary`2<System.String,System.String> Newtonsoft.Json.Converters.XmlNodeConverter::ReadAttributeElements(Newtonsoft.Json.JsonReader,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadAttributeElements_mAC9DFC790762D84D7EE60A34D25DCA076353A40E ();
// 0x00000813 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateInstruction(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern void XmlNodeConverter_CreateInstruction_m7794D562A58D42F80B98F36C2D175CDF5349C785 ();
// 0x00000814 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateDocumentType(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_CreateDocumentType_m10999FAE8FA184AA70D553BB7EA1F2C4396B66FA ();
// 0x00000815 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(System.String,Newtonsoft.Json.Converters.IXmlDocument,System.String,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_CreateElement_m8627B98FEBD5E8A71C3112EBC4A0EA3B2CE58F0D ();
// 0x00000816 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeNode(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_DeserializeNode_m850FF5DDFB166B51C67698F03B71DAB8349338BE ();
// 0x00000817 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsNamespaceAttribute(System.String,System.String&)
extern void XmlNodeConverter_IsNamespaceAttribute_mD1866EEC978652551C50516BF9808DE2670F1C1B ();
// 0x00000818 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ValueAttributes(System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>)
extern void XmlNodeConverter_ValueAttributes_m696FF576E296C506BAE1C311B5B8701459C4E306 ();
// 0x00000819 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::CanConvert(System.Type)
extern void XmlNodeConverter_CanConvert_m7A279C9706589F182A53A184F30E7467631D1D7E ();
// 0x0000081A System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsXmlNode(System.Type)
extern void XmlNodeConverter_IsXmlNode_m9736E155305CA69B8CF4E02F112A28B43EA1A7C7 ();
// 0x0000081B System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.ctor()
extern void XmlNodeConverter__ctor_m7481E930FDBEDE2CB9646BAFC87D082AB38F12D3 ();
// 0x0000081C System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.cctor()
extern void XmlNodeConverter__cctor_m0106E97F9B16E4DA96052DB683C6421199B22B33 ();
// 0x0000081D System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::get_Value()
extern void BsonObjectId_get_Value_m4BFD28522EBF22114BFD2BC2790F72812EDD81A4 ();
// 0x0000081E System.Void Newtonsoft.Json.Bson.BsonObjectId::.ctor(System.Byte[])
extern void BsonObjectId__ctor_mCDC1599B2EBDF5F25250C1A59E866348D64023E3 ();
// 0x0000081F Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonToken::get_Type()
// 0x00000820 System.Void Newtonsoft.Json.Bson.BsonToken::set_Parent(Newtonsoft.Json.Bson.BsonToken)
extern void BsonToken_set_Parent_m111C41C8B32998C9E219DCA585CF2081CBC63F03 ();
// 0x00000821 System.Void Newtonsoft.Json.Bson.BsonToken::.ctor()
extern void BsonToken__ctor_m9212A9356E9351D7D40FEDD19F9C34DFD8C22D44 ();
// 0x00000822 System.Void Newtonsoft.Json.Bson.BsonObject::Add(System.String,Newtonsoft.Json.Bson.BsonToken)
extern void BsonObject_Add_m126D7188A156B2AA91F766F7DE94F9385A1528C7 ();
// 0x00000823 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonObject::get_Type()
extern void BsonObject_get_Type_m900586CBC9A111F9C1B709E637BAB4898A7526C0 ();
// 0x00000824 System.Void Newtonsoft.Json.Bson.BsonArray::Add(Newtonsoft.Json.Bson.BsonToken)
extern void BsonArray_Add_mE1549675724A941DC68E40A94FD973EB03CDABB8 ();
// 0x00000825 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonArray::get_Type()
extern void BsonArray_get_Type_m8E9307C0CA17BE3613969AD7CF92A79CA300FDE6 ();
// 0x00000826 System.Void Newtonsoft.Json.Bson.BsonValue::.ctor(System.Object,Newtonsoft.Json.Bson.BsonType)
extern void BsonValue__ctor_mE4ACB2C97C4F6272E3FEDFB1C1D2D8F12DAB207F ();
// 0x00000827 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::get_Type()
extern void BsonValue_get_Type_mD8CB412F12AC87AB57E70F7BF3BE2196571F9BD4 ();
// 0x00000828 System.Void Newtonsoft.Json.Bson.BsonString::.ctor(System.Object,System.Boolean)
extern void BsonString__ctor_m87599A39F10501E522428B1D8E7A8660E99EAA42 ();
// 0x00000829 System.Void Newtonsoft.Json.Bson.BsonRegex::set_Pattern(Newtonsoft.Json.Bson.BsonString)
extern void BsonRegex_set_Pattern_m31699C93B3522B511689C31183A3FEAE70E5F867 ();
// 0x0000082A System.Void Newtonsoft.Json.Bson.BsonRegex::set_Options(Newtonsoft.Json.Bson.BsonString)
extern void BsonRegex_set_Options_m464E014FBE1475753BC87B5806FA40BAE9F16EB4 ();
// 0x0000082B System.Void Newtonsoft.Json.Bson.BsonRegex::.ctor(System.String,System.String)
extern void BsonRegex__ctor_m3C38477DF8852396E6CA2933B73B714899398743 ();
// 0x0000082C Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonRegex::get_Type()
extern void BsonRegex_get_Type_m09784AC86AB1654573979B70C25BD9D7B93A2F0F ();
// 0x0000082D System.Void Newtonsoft.Json.Bson.BsonProperty::set_Name(Newtonsoft.Json.Bson.BsonString)
extern void BsonProperty_set_Name_m724CFA189FDB017AEBED39A1311B0BE3B4F2477C ();
// 0x0000082E System.Void Newtonsoft.Json.Bson.BsonProperty::set_Value(Newtonsoft.Json.Bson.BsonToken)
extern void BsonProperty_set_Value_mB7D463A67EB8AE6FCCD014EA36875BBF0A875811 ();
// 0x0000082F System.Void Newtonsoft.Json.Bson.BsonProperty::.ctor()
extern void BsonProperty__ctor_m83CE510C551AE597B9023665E69B55955C13D0FF ();
// 0x00000830 System.Void Newtonsoft.Json.Bson.BsonWriter::AddValue(System.Object,Newtonsoft.Json.Bson.BsonType)
extern void BsonWriter_AddValue_mCFD64E6B76E21431092D4A437B62745D6A996B9A ();
// 0x00000831 System.Void Newtonsoft.Json.Bson.BsonWriter::AddToken(Newtonsoft.Json.Bson.BsonToken)
extern void BsonWriter_AddToken_mB087ECEE54BD81EF50302872CD17A95A580ACF17 ();
// 0x00000832 System.Void Newtonsoft.Json.Bson.BsonWriter::WriteObjectId(System.Byte[])
extern void BsonWriter_WriteObjectId_m38F4D84882019075F1DF0535A6C55D46A47EE806 ();
// 0x00000833 System.Void Newtonsoft.Json.Bson.BsonWriter::WriteRegex(System.String,System.String)
extern void BsonWriter_WriteRegex_m5A996A661CAECEC3025561B3E137FDFC94DD2745 ();
static Il2CppMethodPointer s_methodPointers[2099] = 
{
	EmbeddedAttribute__ctor_m27778AFEA29EBFB2F93D7DAF5231562AB231A0A7,
	IsReadOnlyAttribute__ctor_m17515D41590B32DBE35F8DB27EE7A707B8EC741F,
	ExtensionAttribute__ctor_mDFD2F429C7343734A87997E02A88CF0FAF105A43,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonContainerAttribute_get_ItemConverterType_mE247536B5BEC32B46087979E3F46FED189F595B8,
	JsonContainerAttribute_get_ItemConverterParameters_m2C959CD76DF456F824FE3667E8049CFDAFECBD53,
	JsonContainerAttribute_get_NamingStrategyType_m8062CFA021E0C0E2F6AD2B7C6E35B339A84D6BAE,
	JsonContainerAttribute_get_NamingStrategyParameters_m0073A60B3047867D3E8A42A23E347E1852BF6BC6,
	JsonContainerAttribute_get_NamingStrategyInstance_mBFBF66995F1D203594C8E4DCA4A86283D6F6D2A2,
	JsonContainerAttribute_set_NamingStrategyInstance_m8A24903D4D3C02368A34C5F80788B2961144AFF5,
	JsonConvert_get_DefaultSettings_m7A51B49A26CCAB79B2842D64AACA0A0F187B2A6B,
	JsonConvert_ToString_m85F40BA04EBCB76CAA8C7802BD2794AFCC5C2B75,
	JsonConvert_ToString_m3E819C2B1AEB5FC21BF0B3BB667470D50F1C6108,
	JsonConvert_ToString_m0EFDE4B65CDDFD6A6976B15F23C7C470581F803C,
	JsonConvert_EnsureFloatFormat_m812AD27101DA26F048B62B38D33838BA328CC278,
	JsonConvert_ToString_m943E2BF8B9545B98B0E595DA6917F9ECABC7A905,
	JsonConvert_EnsureDecimalPlace_mFFACDA0550E29FA22B4EA3C397BE0D489098845E,
	JsonConvert_EnsureDecimalPlace_m17FAC0CA5604012B71ACFF26841873C0413057A0,
	JsonConvert_ToString_m0E19BB1A182B366F36C84A4A0BAED73BA8434728,
	JsonConvert_ToString_m2A288CD49DF4E4523E5691E1528F00246EB572D2,
	JsonConvert_ToString_m05ED1765E01EF5867E0CA77B0C0D1B502A477B7D,
	JsonConvert_ToString_m8D0C39C163082C374EB859D21966C63394FF63F6,
	NULL,
	NULL,
	JsonConvert_DeserializeObject_m9602AE19A4B7F9174F8806B512BC6F8587012AAD,
	JsonConvert__cctor_m11E6C468A0C459067679F97598B8838BEFC4CBF2,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_CanRead_mC164472AB2A3D8CF999F6AB08763F3A5E956773B,
	JsonConverter_get_CanWrite_m47CB9438FEF36B3ABBCC9F5C21AE1BDB368ED4DC,
	JsonConverter__ctor_m6BE8348C559C7CF2F1B5DBEC1A5EC1D64ED07FEA,
	JsonConverterAttribute_get_ConverterType_m87171B070A7AD07E273B6B6528833B93C3C5C28F,
	JsonConverterAttribute_get_ConverterParameters_mA5C42F619005153B6F11E36D0B35FDA90CCCB382,
	JsonConverterCollection__ctor_mEDBE661FFC67E24DFBC0552B8A5F012C69AA8447,
	JsonException__ctor_m04617A66F6768F7EB0573832A44E6BD97EB236EB,
	JsonException__ctor_mB8EC1AB459DCA248D08C000DC4D7B398E0F6F1A2,
	JsonException__ctor_mF445915497264D23BCBB6BAB8E8559333A51C6D0,
	JsonException__ctor_m07EC4A4F6A1E1460BCF94B0B264D713FC3B9CDFF,
	JsonExtensionDataAttribute_get_WriteData_m55EA3188DFD617C8BC6E3C351BF7153F639838F0,
	JsonExtensionDataAttribute_get_ReadData_m2F77D189509DABD5490FF853B8E72E41D3319BF0,
	JsonObjectAttribute_get_MemberSerialization_m189BEA95020E8AF10964DC3F517C57D18C7108C0,
	JsonPosition__ctor_m26690EBA4C4BED257622AE08B7E3FA140DA88299_AdjustorThunk,
	JsonPosition_CalculateLength_m921A1A2BA11CC900A695B0B7CE87FEE74C3B208B_AdjustorThunk,
	JsonPosition_WriteTo_mF191367903AA33ED43CAE4E7423CE97C67C628DD_AdjustorThunk,
	JsonPosition_TypeHasIndex_mE4996BF80B64FE1672BE49DA7D4437B7E6C3BC47,
	JsonPosition_BuildPath_m26DAF8152ECFEFAEC0555F0BD7529DD8D670BF43,
	JsonPosition_FormatMessage_mA577A748D63EA8C15126615206685FE9333BF2A3,
	JsonPosition__cctor_m53C4B48E7ACF35D0FB697888F556D2F2E12E9A88,
	JsonPropertyAttribute_get_ItemConverterType_mFF23E0D4F0731DA4E2220CBD2454DE4865221D24,
	JsonPropertyAttribute_get_ItemConverterParameters_m9B11B09E89A231E9D8DB1B4B71CE0EE5785D6F35,
	JsonPropertyAttribute_get_NamingStrategyType_m5FF2DFA6BBC0869E3D6C66C9DCD2125AAE9E8BC9,
	JsonPropertyAttribute_get_NamingStrategyParameters_m0EFA60F0B795DB1623E73758F46A3A52EBE364F2,
	JsonPropertyAttribute_get_PropertyName_m4A8575B86821B1B2252871C4ED6BA04B3BC8B566,
	JsonReader_get_CurrentState_m7137D79D19A88A8A0BB692E5632145B150F421C1,
	JsonReader_get_CloseInput_m54E593028B735ECDA8A3EA6CB8B78AB64C4101A6,
	JsonReader_set_CloseInput_m529A2FC45821DD045671D3DF5DE2DFD46EBA8423,
	JsonReader_get_SupportMultipleContent_m1D2BBF3EC1E9A912385B5A804CFE42A13111FE40,
	JsonReader_set_SupportMultipleContent_mDEF71D6622D5F7C4CF05915E11DAC682A4ADBC74,
	JsonReader_get_DateTimeZoneHandling_mAE60084628AA490FE0B4FCA92362F2B75FDAD77A,
	JsonReader_set_DateTimeZoneHandling_mCE5F53C4170A7C09A7E10FA369DDA2E5A26E20D0,
	JsonReader_get_DateParseHandling_mD96560D6DDEF9DB0E5F31364375634F521B5EDD4,
	JsonReader_set_DateParseHandling_m3BDAD515C46CD0E0628DCE6F19F46CF72B21E0C8,
	JsonReader_get_FloatParseHandling_m90A0D2DDFD585F12AAC37E5B8AEBEE37BD89E1FB,
	JsonReader_set_FloatParseHandling_m9F542DCC5F1DACBDE7C8CB7A967E3B125F3400C3,
	JsonReader_get_DateFormatString_m366C5819D59C177FDEEE86F0B7882C88F92688C6,
	JsonReader_set_DateFormatString_m7DE5E6C831217CC8D1510DE35AF5A44B8CF0DA9F,
	JsonReader_get_MaxDepth_m1FF61F2318B1B585359077611E0AE14D5BD36745,
	JsonReader_set_MaxDepth_m704C66DBD74CB34326B665517FC615DD8EB09E8D,
	JsonReader_get_TokenType_m5C79518C15B34E8EA0477DB44A254BC08A965D7E,
	JsonReader_get_Value_m6F9E83E46D30C32A199DD6A66AFE64A481C87990,
	JsonReader_get_ValueType_mF1653CD43A265AA9BCF1B2A1CDE3CF121863C592,
	JsonReader_get_Depth_mD92B3C30A95CF265C184E5FAC2416A0F58350AEA,
	JsonReader_get_Path_m1CA607D98B8A79EF2B81EA5AA8BAC24A7261383F,
	JsonReader_get_Culture_m5AFC9DFD8CDC6C5DA061D77ED9F6203B750C8940,
	JsonReader_set_Culture_mAF8782A53436254EE5C2F208001B96786C490AAC,
	JsonReader_GetPosition_m1208A6618F915264EBD1BA8DE7C1B948F535E154,
	JsonReader__ctor_m59A90ADA802F2EFF746A7DD483F4CA9A3183CA98,
	JsonReader_Push_m625D9CE02FF4797FEC06F8C54352D346C9F6A34B,
	JsonReader_Pop_m616115E094F08CB9C30D549B82434E203B7B7A44,
	JsonReader_Peek_mE56F27ECB561C6D968A2C1E1269A354A9FC7AA47,
	NULL,
	JsonReader_ReadAsInt32_m30435B2A03B75919709F6C403D3712A624EB23BE,
	JsonReader_ReadInt32String_mB470677B0B7AA5E8E7AE1F5C679CC8737124A2B0,
	JsonReader_ReadAsString_m51465F67EF867AFADE155660F6F74FA61754A10F,
	JsonReader_ReadAsBytes_m1D32146809343093FBD44557E38A8631BA224D9E,
	JsonReader_ReadArrayIntoByteArray_m7CB88A67D6F3C25F04226EDBFA2CAA44AB519109,
	JsonReader_ReadArrayElementIntoByteArrayReportDone_m5C3E9A1EF463D84C841A4907BE5C97905FAC552C,
	JsonReader_ReadAsDouble_m83268BFC80A00441F323BA5061E3966B64E3F888,
	JsonReader_ReadDoubleString_m2D1A3F0C8BBD14167AA4FAC6778D4855C3600839,
	JsonReader_ReadAsBoolean_mA10EBF792B907950634E3DEB01BB3B8DC508563A,
	JsonReader_ReadBooleanString_m02ED9E94360662EB82270A89B3707128FE2F19E6,
	JsonReader_ReadAsDecimal_mE742BBAF80F128E6AA398A31AEC88D02B60C5200,
	JsonReader_ReadDecimalString_m1BBE5C2F7B7A5EA1E805AB9ACD0726D234BED8CE,
	JsonReader_ReadAsDateTime_m22FA3C0C7A0446FFC85F9E2C01458F59138B89C2,
	JsonReader_ReadDateTimeString_mB325EABD3F79C841702FCE7D71ECE3F436D72373,
	JsonReader_ReaderReadAndAssert_m8DD8A00BA33CC38E5A35E591EB6154D076FFF28D,
	JsonReader_CreateUnexpectedEndException_mE4921EC6BB31F24E9AEB31BAAC482A12691692D2,
	JsonReader_ReadIntoWrappedTypeObject_m318F9BCA007EE91D36504262CCC11DFCE4A88502,
	JsonReader_Skip_mDCD4C526C7585E9770A7BC743CA5D343E5F3F08B,
	JsonReader_SetToken_m5F5EF6634DCA35E6716067519E61BBDE796F70CC,
	JsonReader_SetToken_mCC04B1E8B024780C4F694583AF108E47E551029B,
	JsonReader_SetToken_m248478B3F3CDD09A4C130FB76AF0B39346ED4820,
	JsonReader_SetPostValueState_mA97D816A4790946B281DB4F80990673553493367,
	JsonReader_UpdateScopeWithFinishedValue_m8A3473E4CE3F41A3516BC5AF352D5FC68F269D4B,
	JsonReader_ValidateEnd_mA795A30C6B3DCE82DF05588A348B613BE456B415,
	JsonReader_SetStateBasedOnCurrent_m11B66BF4989AE85BB078446B73DF6ED5EC4E08EC,
	JsonReader_SetFinished_mB806E429B0BF925682207E3CB68B1072706BB8F3,
	JsonReader_GetTypeForCloseToken_m84355671714B89B9C097B65DA1C984FB730DA882,
	JsonReader_System_IDisposable_Dispose_mEB1D0200A57984B87AED7CF72E8F82B5062FC7B1,
	JsonReader_Dispose_mD2989BD191EABEDCC1148B25D4DE3ACB353205E4,
	JsonReader_Close_mD880192B68AD39E14C2BCC8664D29EFF24354508,
	JsonReader_ReadAndAssert_mA886FCD64978D1151939E4E92731D067084DA245,
	JsonReader_ReadForTypeAndAssert_m557EFF224CFBD0E3D488551FB09EDE35CEE227E1,
	JsonReader_ReadForType_mAA23254DBB10D0337A5C943D11DB959757C11150,
	JsonReader_ReadAndMoveToContent_m69F5B676251E94F06C5132A912A7AFB8EDB9FBD6,
	JsonReader_MoveToContent_m5428A5678ADAFF27A0E33F6926E155646A5D0A7F,
	JsonReader_GetContentToken_m6941DE6621388E2DFF0C28E307480453678EEEEB,
	JsonReaderException__ctor_m107001396704CF5FBC499BA1D194B6877C0E41FC,
	JsonReaderException__ctor_mD095417DE2CCEEF32741D8047DF5A4E53C6CD9BD,
	JsonReaderException__ctor_mF555EF160344C3E8705C34ABD274DC3AD8E6C0B1,
	JsonReaderException_Create_m542C24E20D0DC9BBAC727B7DC80370C07866BFB9,
	JsonReaderException_Create_mFCA48801BFC5C07D621439B7ADF501B97D028A88,
	JsonReaderException_Create_m758736D31A286C8F7DCC9F167DC15B65AC1E62D1,
	JsonSerializationException__ctor_m7C7629D1D0ACA3C627B2C0955D294D5CDBCC923F,
	JsonSerializationException__ctor_m1528BD4200A330AD611F936EAA8F85C6014F084E,
	JsonSerializationException__ctor_m2004EB22E7C185FB64905773BE3D65AE5A8CE914,
	JsonSerializationException__ctor_m0CD549EE0A76E02AA44B70253324F5E19213B96F,
	JsonSerializationException_Create_m4242A77595098B9E7687291CB3BFF96BD2249AC7,
	JsonSerializationException_Create_mC6637C74A9EA6C2527F5971694AAC40671C302B6,
	JsonSerializationException_Create_m0EBB0905A5B2993F6ACF9AEA8D7535ECD1D6981C,
	JsonSerializer_add_Error_m4C1E51CD3049D684A8DFC014EB07C645F3A73EA0,
	JsonSerializer_remove_Error_mA3D7CB2D7BB623F79D8D66B905BD9C6D2E4E791E,
	JsonSerializer_set_ReferenceResolver_m9C44638869949098A898108C468CB92ECC9E591D,
	JsonSerializer_set_SerializationBinder_m799247A598583D710FCE9250225839218B43069A,
	JsonSerializer_get_TraceWriter_m252481C00E908F419AD49ABC148583D81E2DCD7C,
	JsonSerializer_set_TraceWriter_m99646D2DC5C7E49CA2969BEF5A486CA04A2BE62D,
	JsonSerializer_set_EqualityComparer_m2529D197EF0628A78D26896FDCA79B32EDB4A9C0,
	JsonSerializer_set_TypeNameHandling_mA485C2838EE7A8A5F62F98B25E345A28B8EBF42C,
	JsonSerializer_set_TypeNameAssemblyFormatHandling_mFD586B0CBC504194A5676BC66B4DD9F64400BF58,
	JsonSerializer_set_PreserveReferencesHandling_m18165AAA840BFFD9DDFB1236BFBBA91B7059013F,
	JsonSerializer_set_ReferenceLoopHandling_m1F02FBB0C85842D5ECA0DCBCCDDE40DF3005CFE5,
	JsonSerializer_set_MissingMemberHandling_m9F0C807503161CF6E56EE00EB5BC752EA7A79780,
	JsonSerializer_get_NullValueHandling_mF5D54BAE0A41E747D189FE1D29B5324A148807D6,
	JsonSerializer_set_NullValueHandling_m756283732ECF267EE17F62B796EC8DFAC9E3D5EA,
	JsonSerializer_set_DefaultValueHandling_m4E4DDD012B764BA8508BBDC59CA3DF1950B93C72,
	JsonSerializer_set_ObjectCreationHandling_m7241DE5D3F047FF01E71558AD1C93F4B5B942DF6,
	JsonSerializer_set_ConstructorHandling_m84FE6147D6CB78C9E577AF9A20D72AABDDD6FA21,
	JsonSerializer_get_MetadataPropertyHandling_m9B7DA33D364F04F505137464A3D357F1A8BE6C36,
	JsonSerializer_set_MetadataPropertyHandling_mFADA3DF2590CE9903A3AF650F7551A0DD57D78DB,
	JsonSerializer_get_Converters_m42E233DF91EB26C264DF4F59695DA967C5D6FC40,
	JsonSerializer_get_ContractResolver_mC697373564D447BC54DCAB5CFD9E7E4BBC2C5C0C,
	JsonSerializer_set_ContractResolver_m133F62A635EF77449897DB0E807B67687B72592A,
	JsonSerializer_get_Context_mC39BC437E1C60E44F1653793240F69529A7D125A,
	JsonSerializer_set_Context_m4E23EB3BA8F7D8DB39E158A86ADC73B552F86B67,
	JsonSerializer_get_CheckAdditionalContent_m400A6A99C0B24E39E1399333CD2DFD3001F21201,
	JsonSerializer_set_CheckAdditionalContent_m26695B93ADF9337A6411567D4F920BDD94FA4159,
	JsonSerializer_IsCheckAdditionalContentSet_m17A7688B3A6BEB62518563B64EB90652920F3321,
	JsonSerializer__ctor_m4672AF4CB21A63D3C972037EC9F961D1242A0DAF,
	JsonSerializer_Create_m96314D601FD9AE91A028AF7A124ECE4BAEC043F7,
	JsonSerializer_Create_m88DFAF350F847F16FD4DF5AE26A7FBD3ACED81E9,
	JsonSerializer_CreateDefault_m8EDABDD16C11669D570E4770825D6A45B602AC3C,
	JsonSerializer_CreateDefault_m8E8A552229E1FC82C3CB5C2B6D6EB0EA9819F48A,
	JsonSerializer_ApplySerializerSettings_mB4CDAF7E955A016F12E649B90F24018A24AD729A,
	NULL,
	JsonSerializer_Deserialize_m878EA724D695FEF23CB17BD1CF936B5525AEFAB8,
	JsonSerializer_DeserializeInternal_mED9A9B045D22C3F6A562E3111311FEF41FE13F5C,
	JsonSerializer_SetupReader_mC5D4862F24009A92E7ACDD270AA27E27DCE5924E,
	JsonSerializer_ResetReader_m223B0B5AE2C4008BA0959DF8D2531C0508988345,
	JsonSerializer_Serialize_mBCD908364A9781189431063E2244C6550D958D61,
	JsonSerializer_Serialize_mE85324845C96E090A70AF7E694DFC6F23EB76C1F,
	JsonSerializer_Serialize_mCD7C1A951F04FE743A705EF75605BBAEDFFFB924,
	JsonSerializer_CreateTraceJsonReader_mE43FCABF651C601A28EE652C206DC03AADC629D2,
	JsonSerializer_SerializeInternal_mBC5C380C16B6A80C6370EE1FF281711B6DF27F1B,
	JsonSerializer_GetReferenceResolver_m3F61A527E858F13FC216EE1478E16B249D612E66,
	JsonSerializer_GetMatchingConverter_mFCF1E2F5CBF9DBEAE9FB855DA2D75E3360E4D80B,
	JsonSerializer_GetMatchingConverter_mBD8F2C669C9EC1DAEC036A33B0939D1A4EFF52F9,
	JsonSerializer_OnError_m62D77DC78C986C7A32ED7D90E75AF367301502ED,
	JsonSerializerSettings_get_ReferenceLoopHandling_mE90131C366549F5BBBAD1AEDE36DAA0BAD36B104,
	JsonSerializerSettings_get_MissingMemberHandling_m8A3F395640F090D05033DFCBB23DC79DCEB8506B,
	JsonSerializerSettings_get_ObjectCreationHandling_mB6921D82F2C6332399521EB1057352F20E6F96D0,
	JsonSerializerSettings_get_NullValueHandling_mC18A872C75FBF2C09D2D9CC56E89E1993BEC872F,
	JsonSerializerSettings_get_DefaultValueHandling_m343E75B4FAD9F44D98B92DC31E613353559C7DC9,
	JsonSerializerSettings_get_Converters_m14000B0C3DFBE0870A478DB0F678051E5A7AD033,
	JsonSerializerSettings_get_PreserveReferencesHandling_mD53D2909FE7D9E3731CA95D694152388A8CD0438,
	JsonSerializerSettings_get_TypeNameHandling_m2851204FA3BBCF7F62124FD872403A6DABCCC14C,
	JsonSerializerSettings_get_MetadataPropertyHandling_mCE8C028F265D3778FAF94AC0EAF5DCB1410BE9F9,
	JsonSerializerSettings_get_TypeNameAssemblyFormatHandling_mC8836F149AA061CE1F2909B73D68600D85A31ECA,
	JsonSerializerSettings_get_ConstructorHandling_m8B8571BD9474ACB01581705244C173C3CA69ED5E,
	JsonSerializerSettings_get_ContractResolver_m22DEFBEC3CF3363020E57D703E4DA1D69975F7DF,
	JsonSerializerSettings_get_EqualityComparer_m8045CC9A277D7E5226DB2B51C923F7BF44DE58B0,
	JsonSerializerSettings_get_ReferenceResolverProvider_mA50EA59821E514DE8568035E320E0CD130AC383A,
	JsonSerializerSettings_get_TraceWriter_m05A1F310CEC7BD2B071CE9E4E5BA3EFB8797669B,
	JsonSerializerSettings_get_SerializationBinder_m862F8E0DD9619EFAAD5A65761A50928DE3851D1A,
	JsonSerializerSettings_get_Error_m73767217D978A8BA0146618E99DA60A2C71B4EE2,
	JsonSerializerSettings_get_Context_m4FEFE07F9642722F31D58FBDF27A26CCB033A4D0,
	JsonSerializerSettings__cctor_mA71EC3E06741A2AB3A9F55053C34B10C8A711792,
	JsonTextReader__ctor_m4D88467341528476FF90C1568A3D97EB975CC64F,
	JsonTextReader_EnsureBufferNotEmpty_m54B4E0FFEBFA8327B5632F6D64E8B324A6CCF450,
	JsonTextReader_SetNewLine_m7203F86741F5805DA318D24EB612724C797C0AB5,
	JsonTextReader_OnNewLine_m7EFA0ACCA08A95509D53140AEE047139F1302C7F,
	JsonTextReader_ParseString_m43BC25BD4AF552BC2C36CEDB5827DE4490DAB979,
	JsonTextReader_ParseReadString_mB4A8084EB35794F7DE92621BB52173B369329151,
	JsonTextReader_BlockCopyChars_m9839762FBEEDF7B76D996744877DF2440CFA4069,
	JsonTextReader_ShiftBufferIfNeeded_m06CB7B34C1D1077566E07D58A63E457C267000D1,
	JsonTextReader_ReadData_m977DAED45CE58AB40DD8F72C097AF2A62071CEE2,
	JsonTextReader_PrepareBufferForReadData_m058FF10DB9EFDAE275E386AD8149316109E8778C,
	JsonTextReader_ReadData_m412C6F0F22279EF7B3DCF4FAC1653285C9AC5FFB,
	JsonTextReader_EnsureChars_mD8C8A52585F1D31237A1CB4428A824911818A436,
	JsonTextReader_ReadChars_m1141C4FEB25E8737EEB8C9F5D8630B2D88D7A394,
	JsonTextReader_Read_m080ED282D233E7ED81438BC6A5F65719CA5FDBF5,
	JsonTextReader_ReadAsInt32_m1B3CAE80968C62B56936F4AEDCBB680791710D23,
	JsonTextReader_ReadAsDateTime_m053B184CB8275C71869C338F57769278ED316B45,
	JsonTextReader_ReadAsString_m20AAE0EC8E2BF25F5890165F0E1FA83FF8DA0A92,
	JsonTextReader_ReadAsBytes_m90A5243866287CED1D8F8F80D48E2DE884C4B417,
	JsonTextReader_ReadStringValue_m75E13102F75E2FE147B8A182067BAFFEFA5F4C9B,
	JsonTextReader_FinishReadQuotedStringValue_m76C893F7E2FBD095BBCD560E208885D4014F427A,
	JsonTextReader_CreateUnexpectedCharacterException_m1C9768E827FD0384DA5F37F2E83796409909D572,
	JsonTextReader_ReadAsBoolean_m480742572E0AE3C94ACE3B0815D650B3C0FEE765,
	JsonTextReader_ProcessValueComma_mE0791D6EE713682643050D1B20E47D54E37F55FF,
	JsonTextReader_ReadNumberValue_mD0AAE37DC1C36D7D1DAB52DBA117FEAE345A0DD0,
	JsonTextReader_FinishReadQuotedNumber_m0BFF9FB4F493BDFEA21C95C6A3FF73004857B433,
	JsonTextReader_ReadAsDecimal_m39EAEF985CFCD5358EFB1E5E2376B2068813E798,
	JsonTextReader_ReadAsDouble_m42B3D6EDC5AE3208F89078349E6CFEE1126D3017,
	JsonTextReader_HandleNull_m3183FB4988AB7B5162A32464F0813C6D7DD05B1B,
	JsonTextReader_ReadFinished_m5644EFA44232CE01BD8E44A128EE65D4E1697891,
	JsonTextReader_ReadNullChar_m0B1EF00095EB4E85577747F312283ADCAF221ACE,
	JsonTextReader_EnsureBuffer_m59BEA692315A45A060C23405E989AF9F1CF2B1A8,
	JsonTextReader_ReadStringIntoBuffer_m02374811831B62F971B253AE56735ED16AB9EE2E,
	JsonTextReader_FinishReadStringIntoBuffer_mF8FE2C5A91A1B7FF1B48586B40733C1CFE46F624,
	JsonTextReader_WriteCharToBuffer_m2BA0C961F6D7DC874A612DC40766DBB27C62658B,
	JsonTextReader_ConvertUnicode_mC1B823CEF49BA1E687A0D9A776F9F706716D167F,
	JsonTextReader_ParseUnicode_mED7DCFA3C78AD4489D7A828BF0E3520ECC47DA1A,
	JsonTextReader_ReadNumberIntoBuffer_m301AC99974646240887D3A7EDC534A4BF54ABA8B,
	JsonTextReader_ReadNumberCharIntoBuffer_mCAA2EE0EB25CBB44EB5EF464EED5C73FEA233D7E,
	JsonTextReader_ClearRecentString_m11B91928C0C2029A03A84086D8549D029C3CA6C8,
	JsonTextReader_ParsePostValue_mC39E826C3F2FB2CE5DAE30156BD600FEC4C4F701,
	JsonTextReader_ParseObject_mB231A4928ABEEFCA075079A1D22C686EBC46F121,
	JsonTextReader_ParseProperty_mE55E07230DE0236AF32DE37CEA5D54E8240F2D4E,
	JsonTextReader_ValidIdentifierChar_m659AABF49B26DB1CF2F8597A92734EEFC0DDA1C2,
	JsonTextReader_ParseUnquotedProperty_m2895E5CD129B6D17B27522A877A8EFCC455313C9,
	JsonTextReader_ReadUnquotedPropertyReportIfDone_m25CC9BADD96170C2203B43C7F35599C9199914B0,
	JsonTextReader_ParseValue_m8E626BD3A71365BADA85811C1F248C7E82B8340D,
	JsonTextReader_ProcessLineFeed_m504D07288A3E04F5798F457C78072BAA551EBE07,
	JsonTextReader_ProcessCarriageReturn_mE5331F5AFA92FADF90BF8A1E7D6D490B1A886FBE,
	JsonTextReader_EatWhitespace_m4597FEBF59765A8B124EEA32825F593D74DBD36E,
	JsonTextReader_ParseConstructor_m8C37823F833B2B0093CD0522400D59D62C671B4F,
	JsonTextReader_ParseNumber_mD1CF3F5EDF2DE1608DB250ECAC2FCF41A6769703,
	JsonTextReader_ParseReadNumber_m5783A5B687A11F1E961C386A7713771FC475C00E,
	JsonTextReader_ThrowReaderError_mF93C319790CE54EB11457EDB965524B1BD3C3DD8,
	JsonTextReader_ParseComment_m7C6442B7246754DBC9A245F75507724D96264ADF,
	JsonTextReader_EndComment_m19EC51BE1DEF5944076FB728C646AB92E53D3B28,
	JsonTextReader_MatchValue_m075D6C448798134363261D015AFF537E294F2B8B,
	JsonTextReader_MatchValue_mD289A110DB4617E234A506899DEB563B500CCEFF,
	JsonTextReader_MatchValueWithTrailingSeparator_m9775B25A1A7D5B9F119413B2E66A458FDE2CE2F1,
	JsonTextReader_IsSeparator_mC404377FA0891D92378FD9FCA0005E7712837C04,
	JsonTextReader_ParseTrue_m75E55C52D9BF6A9FC040ADF6A7E50719045C8F57,
	JsonTextReader_ParseNull_m5CF3333EE6A2D8586952E9FB7756199B50F683D1,
	JsonTextReader_ParseUndefined_m7FBD87AB1DE479AE17B82A56C30CF7054DFF53AF,
	JsonTextReader_ParseFalse_mBE167CBD38442D77369563636898D7B9409522AA,
	JsonTextReader_ParseNumberNegativeInfinity_m0C8A6152F3D0597137063B4E620A6B6AADB9524F,
	JsonTextReader_ParseNumberNegativeInfinity_m4F8830D306A2E64E22369CFE0997A86B96A12FCB,
	JsonTextReader_ParseNumberPositiveInfinity_mFD3E75CE17E87E772FF9A67CEAD6BF7D0BB8F3CD,
	JsonTextReader_ParseNumberPositiveInfinity_mB11A5EE3FBE60BA4B2241850D03EF40BDC5668C9,
	JsonTextReader_ParseNumberNaN_mD56D70675DEDC4E82D6079F46A186FB8984AD29E,
	JsonTextReader_ParseNumberNaN_m02B73A3C6D19D102D2448795D274BEEEC9990E05,
	JsonTextReader_Close_mDD3B622618D8DD4911730B8C20759FA3F0A09AAD,
	JsonTextReader_HasLineInfo_m9EBB382ADC2BC19E83C8D8698170763CDFE447BF,
	JsonTextReader_get_LineNumber_mBF433F4F37657825DCFDFCC13CF514DD41E7A0EB,
	JsonTextReader_get_LinePosition_m2811031A40E62E7D3686347D4E29975D8888F860,
	JsonTextWriter_get_Base64Encoder_m57373C368E5458E437787B6A3705D68AE29E86AA,
	JsonTextWriter_get_QuoteChar_mD730D52F805F2A8B69B035DEF7FB4732E577C809,
	JsonTextWriter__ctor_m344EB779AA3C19BF0538BE547ABD227C3F5617B1,
	JsonTextWriter_Close_m428AE4CFCEE10215B1F278EB88F663559DA619B4,
	JsonTextWriter_CloseBufferAndWriter_m060795449BCECD4D638656BDFEDADDD5563B5AC1,
	JsonTextWriter_WriteStartObject_mCC70DDACF3B7DF872F72B9787D103A2A58E438BF,
	JsonTextWriter_WriteStartArray_m58CBDA8044E630C48FE274471DACB6518FF07B76,
	JsonTextWriter_WriteStartConstructor_m2A4B67F48DA26E616AE399EE55008D58F04DE6C8,
	JsonTextWriter_WriteEnd_m86043CD2D5FC2CAEBBA4992383428CF4A3243506,
	JsonTextWriter_WritePropertyName_mB45CF50C07961253C42A67C22209394440B43512,
	JsonTextWriter_WritePropertyName_m08D42DA59D2A7BD46B4AD2B0427AD8C6A7A13973,
	JsonTextWriter_OnStringEscapeHandlingChanged_mE0319659D85FE4584C6B98169D78AD1F8C131561,
	JsonTextWriter_UpdateCharEscapeFlags_m698893BD314E970F16482C5121F84A53FAFE93C3,
	JsonTextWriter_WriteIndent_m720E03FC8FEEDEFC73A4913F412C0D7B419C2F57,
	JsonTextWriter_SetIndentChars_mE772CA2DB92EBCBA4E70A97069EA528111F8DC34,
	JsonTextWriter_WriteValueDelimiter_mECB01176B06080C499E9A384D8822B88C0B980A0,
	JsonTextWriter_WriteIndentSpace_mC6C57B5BEEEBA13006C4319CFFF6B6E3DC25ADA6,
	JsonTextWriter_WriteValueInternal_m01D0B3050E7A6AA87EADEB8303436A780080BE2F,
	JsonTextWriter_WriteNull_m38E23776B07F0CEAA0B18E1A6549EFBD19A74039,
	JsonTextWriter_WriteUndefined_m8C51DAE7C8D431138869D55B1CB07FC9A3BC5607,
	JsonTextWriter_WriteRaw_m9D0661F55FBA967E80D81C909132EC02FD900A20,
	JsonTextWriter_WriteValue_m82176C2D11A1170B97B816F2593BC0DEA70A81A1,
	JsonTextWriter_WriteEscapedString_m30678B63E78451A849F0AEB24ED9FC2151B23AEC,
	JsonTextWriter_WriteValue_m4E04EB74C546C46C347DED9E6BF9AE3ED62F0104,
	JsonTextWriter_WriteValue_m63662223218A01B53F5C0975D2D67670B24A6A0E,
	JsonTextWriter_WriteValue_mDA1793E6E5373553630623375855D61B25DCEBD3,
	JsonTextWriter_WriteValue_m338EA13FEA3F463A7DE53AE70843D2E41239D52F,
	JsonTextWriter_WriteValue_mCDE58C488EE7E625A4D717742E3C022B2901C12E,
	JsonTextWriter_WriteValue_m481470E98AE70FCF7CD58F8BA0EE9BA2F2502E54,
	JsonTextWriter_WriteValue_mA141F35C7779FF0A1AA670D2724B4CED3D518713,
	JsonTextWriter_WriteValue_mFE1BF06AEBEC9F698B48E96C2AE423E7AF57623A,
	JsonTextWriter_WriteValue_m4F51794EE4EC3EF365DFDAFB5F26E69597DAB756,
	JsonTextWriter_WriteValue_m1B51029CB7DD9647EE829D8BD9037385FB83046F,
	JsonTextWriter_WriteValue_m81C8CFF4CF18D659589FB9B01DA4E6CE82D21C96,
	JsonTextWriter_WriteValue_mFF5E65F22BB05609D008EC79B36F8F31F4600F5B,
	JsonTextWriter_WriteValue_mF3554EDD8E41154FC681F1554EE63EC9391D3F61,
	JsonTextWriter_WriteValue_m65E22B01DC2DB73EF7F6AD2AB9815F52EEC5CC03,
	JsonTextWriter_WriteValue_m5B65B37BD44AC87786A8FC1A2BEB74F0CC46E6A2,
	JsonTextWriter_WriteValue_mC781EDF21381D8F56AD0E54B380A9BB1326C114A,
	JsonTextWriter_WriteValueToBuffer_mDAACC45FFA18C0753922C2A3501BBE842FE6FD25,
	JsonTextWriter_WriteValue_mEEBD3192B5F5B435CF8CC5D78CB3373002E35964,
	JsonTextWriter_WriteValue_mAE66E72A49374060D63EBE30B6F28114C5378410,
	JsonTextWriter_WriteValue_m7F58D32F28AF92F19CCD0DDB0EF5E6672400CAB7,
	JsonTextWriter_WriteValue_mF0F4E1389F887C149F68696E9CFDF56ED944BF67,
	JsonTextWriter_WriteComment_mB5C03EE5DD6EF551AD141607CAEE32F75B518934,
	JsonTextWriter_EnsureWriteBuffer_mC30FED151F0DE8E7465B4815F6340524A43AEE15,
	JsonTextWriter_WriteIntegerValue_m28AD8FEC60B9718FEDB0960FBDE81A3A4C92F77A,
	JsonTextWriter_WriteIntegerValue_mF4EDEEBCCCE1D247DD7030350F5AA9D21FAED539,
	JsonTextWriter_WriteNumberToBuffer_mE7207F1CFA6F3440ADB854B1B412D95F0FF04DB6,
	JsonTextWriter_WriteIntegerValue_mCB85257874EC4E329E1B3245B910CCB789451D1D,
	JsonTextWriter_WriteIntegerValue_m021DA68672908538141F1135D375B41E4514D81C,
	JsonTextWriter_WriteNumberToBuffer_m615D27EE31DE94794B4EC346AEC67C6D353B20A0,
	JsonWriter_BuildStateArray_m73173DCEE0FE8D86A4A6B8560BF87DE22C47FC1A,
	JsonWriter__cctor_mB16995CBB9819622E08CA8A6D5DA7BE7661C153B,
	JsonWriter_get_CloseOutput_mD0D99EC3BAB0F616E15B9FE03C2ABC947E5C4151,
	JsonWriter_set_CloseOutput_mA722DE122AFF55E4B8F2CD836D81CA859AF6BF49,
	JsonWriter_get_AutoCompleteOnClose_m6D283F8153209F11A7FB7FCD1425A3A7596C1A0F,
	JsonWriter_set_AutoCompleteOnClose_m613DA95AD0296DAA59BE38E8EFAF114491C2DFE5,
	JsonWriter_get_Top_m06FE406A5BEAD226E28F5C206CC802F72E771CD4,
	JsonWriter_get_WriteState_mA40CDDF21B3B472F7BF9486CEA68744F9418D007,
	JsonWriter_get_ContainerPath_mA9290A7DF7EFF28ABA0A159A4B00237259DB9546,
	JsonWriter_get_Path_m965DB09A52D16CC2D6E6C7AEC5235B68740EEB8F,
	JsonWriter_get_Formatting_m85DCC26F9351ECF7C2A4D72A943951C58F1FD55F,
	JsonWriter_set_Formatting_mA1623232BC3D41CEB26AB7AA7193DDA40EE85074,
	JsonWriter_get_DateFormatHandling_mABE02A23C0FCB901F53121FE1F74420223BB3E82,
	JsonWriter_set_DateFormatHandling_m1A9CC51681E4BA63EE773D9DB3257459DF1D843F,
	JsonWriter_get_DateTimeZoneHandling_m05CDCC55984513F2F487BB40CBFEB692BBA8EBCA,
	JsonWriter_set_DateTimeZoneHandling_m6785061AE10DBB8B1E03586004858C8B8203967F,
	JsonWriter_get_StringEscapeHandling_mE8AB4C70B2D840C2E04375184395850A09A47BF8,
	JsonWriter_set_StringEscapeHandling_mCD72BC5B15385CC32DF1F4CD28BF0552A56668E5,
	JsonWriter_OnStringEscapeHandlingChanged_mF207B9E57E6A5D4D730C0D487587D7E9D8F96E97,
	JsonWriter_get_FloatFormatHandling_m1249DAD3B85F6E0F7EE3AD5F2C853B032B779489,
	JsonWriter_set_FloatFormatHandling_mF337661FAA8A51D0502CF709EB1B243F24C3C0B5,
	JsonWriter_get_DateFormatString_m773103CB571412471A793CAFC440B3C385F6C673,
	JsonWriter_set_DateFormatString_m945500D83A209F1D550C3948DDAB6752DE4C15DB,
	JsonWriter_get_Culture_m73229B8C6B57D3440A1DAD8FFADEC0B6AD83AD6E,
	JsonWriter_set_Culture_m0DCE408EEE12A4422C24107BF6B95F563272293C,
	JsonWriter__ctor_m3299A3DDE82465D2B0CF736D685C642B9E9CD417,
	JsonWriter_UpdateScopeWithFinishedValue_mC11A0DD0AEC7842A9289957EB2B682093677D364,
	JsonWriter_Push_m83EACD5DE9A372717B9A4FA7EC8F8BE9EA33E57B,
	JsonWriter_Pop_m467BB7D43DBC26C95815EB1CB6A9CEFB64757F3E,
	JsonWriter_Peek_m1464615041C35045232D46F737F8F8C5DD02BABA,
	JsonWriter_Close_m6AEF1A5E237CBBA4F632CA0669FAEC2E8334968B,
	JsonWriter_WriteStartObject_m5132FC69631815FD70E05A7A710B9E9DB4D0CDA0,
	JsonWriter_WriteEndObject_mA156066911ABC6B4B7E1E4B8180726FA6A59855A,
	JsonWriter_WriteStartArray_m3265437EEF4FD8503C8F9EC2FBEFE66F7226DFA1,
	JsonWriter_WriteEndArray_m16BFF8EC6210580D4675C857E513584A45379E28,
	JsonWriter_WriteStartConstructor_m7AF6B811E053566BC90E91B05E2AE7846EA94174,
	JsonWriter_WriteEndConstructor_mB3474CF26705EB9FF31779043ABAD5F52DCC1E47,
	JsonWriter_WritePropertyName_m7157900D394C7C8A2FB41374846519B945EA6F9F,
	JsonWriter_WritePropertyName_mFE8E1614C66BCA8F196ADE8181CD9BCCB02880D4,
	JsonWriter_WriteEnd_m107687C7DCB87829E9B589FE0378C84AE3BE25C4,
	JsonWriter_WriteToken_mD30A47F97A0D909ED2CC9A7AF476AFA00AD986D5,
	JsonWriter_WriteToken_mA24011E9186AF80407F0A4AE02F12B74224F7BB9,
	JsonWriter_WriteToken_m6DAE89D82DE4C4680272134E3E126C6C16A5819C,
	JsonWriter_WriteToken_m0A8DA0C65F411D80FF7EDF7A0C3D42D2E25BE400,
	JsonWriter_CalculateWriteTokenInitialDepth_mB04EB72397DF45F3EC98D98E366C9F5033D8179C,
	JsonWriter_CalculateWriteTokenFinalDepth_mDD1C7CF54990FD094A2CF58C71BA34AD4D83F97A,
	JsonWriter_WriteConstructorDate_m042928008784ABD220DD6B8E29090428C087FD5A,
	JsonWriter_WriteEnd_m2B22EB9E3621F9EAB6564C5721134B43AF2E90F4,
	JsonWriter_AutoCompleteAll_m63DC7F3D3FAE18A5964DCE8F8ED77335F319AAA2,
	JsonWriter_GetCloseTokenForType_m8B135504923FDAEC378F298C42644A7E261DC0D7,
	JsonWriter_AutoCompleteClose_m95D49303152DA56B437CB194C59897D49085D485,
	JsonWriter_CalculateLevelsToComplete_m6F70D9434240904414722E1FCE1F9E8A9FF5C735,
	JsonWriter_UpdateCurrentState_m2B3AC465B975F2FAD70CA03329BE872DD7664432,
	JsonWriter_WriteEnd_m5F44EE29B0E14DBA543DB4729E4FDACD5957236F,
	JsonWriter_WriteIndent_m105FE18738B00F7D2776F77323518046402BDEFB,
	JsonWriter_WriteValueDelimiter_m8175731233D75F530A8807290FC78B18352AF1BE,
	JsonWriter_WriteIndentSpace_m5CA7EB6F38155296AA94515A5F69D399FF3585DA,
	JsonWriter_AutoComplete_mB7BAB3C255951F307D6BE6DD7E1C6831A6D48D38,
	JsonWriter_WriteNull_m0FD8BEBB2BBA170138AE9C6C705C92E558320A9B,
	JsonWriter_WriteUndefined_m178BD42543356D26B7E99DA8E5E2CCF3004D0811,
	JsonWriter_WriteRaw_m6EE163869CD409F26E60073B507D73A4CECDE56D,
	JsonWriter_WriteRawValue_m41AA54810805FC37A6EBBBFBBFB9E69C1EA19B58,
	JsonWriter_WriteValue_mC84EDFC58A7A8FF306CE6F15E4E456E5F02902B7,
	JsonWriter_WriteValue_m68A7AE8D9CF34AD300ED01B85544E99AB775E8AA,
	JsonWriter_WriteValue_m2BBB5FF1C848BBAC00D349083E6C5731329A38BD,
	JsonWriter_WriteValue_m77CE130F58FF4C9CEDDB3C2B45E6C7C1FBD02981,
	JsonWriter_WriteValue_m5D26E47E8875950BEF46BC77A976CE122DD751B2,
	JsonWriter_WriteValue_m5301A218957F4A65A6BFB31B474E74E3392ECF23,
	JsonWriter_WriteValue_m66486269BFF584456DE35AF6F393F84FEFF56B89,
	JsonWriter_WriteValue_m6DABF07C6A729ACC379116FDC57E47C24574E249,
	JsonWriter_WriteValue_mFEC109BAB96B61845126AE50F2F5D9E5E6B798DA,
	JsonWriter_WriteValue_m242608E121FA6873CCEFF584F0B235540656D703,
	JsonWriter_WriteValue_mC6975D72EE6699CEA4488753530FE3742452ED92,
	JsonWriter_WriteValue_m7ADFA914BD4B4CEAA35E0F3ABD959C896E9DD956,
	JsonWriter_WriteValue_m527B7E2AE12DB82E048B42F9D96520D510A84297,
	JsonWriter_WriteValue_m6C8EBC9973CB6EF721F208300E0559F32F8920F6,
	JsonWriter_WriteValue_m55EDEF66C19EE6D1955592EF37E213BC5F60C0E5,
	JsonWriter_WriteValue_m2D7EBC1A209BBF27A77D6E57147001854DC463E8,
	JsonWriter_WriteValue_m0B1F342B47D5D1383657AE011BDC11670F05C571,
	JsonWriter_WriteValue_mA695B39F24210D8113A323EA0269258A3C4EE1A6,
	JsonWriter_WriteValue_m6E8D9DB5EEA5D90659535BFFC4DC581B82A9E475,
	JsonWriter_WriteValue_mF9C293FFD62FACA327C71CAB3CD5A75E878AE72D,
	JsonWriter_WriteValue_m27A04F669D85D0F97BAB36799E3722B67A3FC49B,
	JsonWriter_WriteValue_m14064AE26EA3E5B36866F4F861A196EB9C200E5F,
	JsonWriter_WriteValue_m77A7E7EAC9B13A0756CF25DC4178CA3010CC578B,
	JsonWriter_WriteValue_m3932EC259233109D6CC64B7008D8F3B02A998A23,
	JsonWriter_WriteValue_m64994C650001FA82624201009AE882AE3D6FDD69,
	JsonWriter_WriteValue_mD9E66E4CFE07F89CA257213A9746CEC3D0061398,
	JsonWriter_WriteValue_m20826615A413917E65098F0BBAD30B66BC77396D,
	JsonWriter_WriteValue_mE62572704399FC870EA15E0C36BB52FB2A950982,
	JsonWriter_WriteValue_mC17E770BC70CF513CFCC343D4B1D0190E0029D7D,
	JsonWriter_WriteValue_m6A594DBB70FF961711FA237F2D60F169AA940F90,
	JsonWriter_WriteValue_mA7D61778A627FEBF4538F33D6AB9B9D391C2B74C,
	JsonWriter_WriteValue_m12A6CD9BA564D23CF575B59DFB82E1EA49B78DED,
	JsonWriter_WriteValue_m3FF6BA49F6C9FFBC8BC1375E2095DC7DECCC5C74,
	JsonWriter_WriteValue_m5761F7F00B42F7B7DED177B4970CEB5606AF7FE8,
	JsonWriter_WriteValue_mBB6DC1861474504F0ED964BA2781B47083EE5976,
	JsonWriter_WriteComment_mAC796E558310D38291EB0E0875AA542407C46584,
	JsonWriter_System_IDisposable_Dispose_mA737C1CC092461603FEEA734B9FDF3E18A8B5551,
	JsonWriter_Dispose_m8009396C5AA34C60B94A809BEE35C2351F068FE1,
	JsonWriter_WriteValue_m253A963D4485D232597A4E3D0FDFD200B31CF3DA,
	JsonWriter_ResolveConvertibleValue_m3515C7424B2C720BBEE4517F3478D4165B84941A,
	JsonWriter_CreateUnsupportedTypeException_mFA0E6848FECC96BAABFE8DBCD007B1F5C1BBA118,
	JsonWriter_SetWriteState_m52C914A54478648DAF335637EE06A1EA07E07FC3,
	JsonWriter_InternalWriteEnd_m87362A5341E4DD9CC51DE540DD0A311D7D9196F9,
	JsonWriter_InternalWritePropertyName_mA3CB0892966C12792342EE02E8249DD605312FF0,
	JsonWriter_InternalWriteRaw_m15F01B472C3A562731C75565E1F52DCC74DDB66C,
	JsonWriter_InternalWriteStart_mCD4A59771D7BA1D5C44ABBCD5A2C38632D7E33CA,
	JsonWriter_InternalWriteValue_mB415D72FC95E93FEC7AB9E0CB9A14602BC857114,
	JsonWriter_InternalWriteComment_m94AD4FBEB479C2BBD93DD411CA80D89E283C9366,
	JsonWriterException__ctor_mBF32C5A5668CE121D3893B42B9B7513BA354FE6D,
	JsonWriterException__ctor_mB5B0B047EC836B13233851125916F5E45D74ADF7,
	JsonWriterException__ctor_m4C83004213435CE5CF252DADA8B58139DB0F21B7,
	JsonWriterException_Create_mCD7BF9B20AFD76955151D4896FACC86FDA3D9DB5,
	JsonWriterException_Create_mDA098F93B086B2E37D62069D573788ACFE8F4D78,
	Base64Encoder__ctor_mC4CBAD945D781A4176190E1DFB843C8F883ED053,
	Base64Encoder_ValidateEncode_m549263E0E9A63ECE0AF45BA74CF995DB439019C2,
	Base64Encoder_Encode_mF1E5F00D5BF599A77CEED5F188D5DE656973C6C4,
	Base64Encoder_StoreLeftOverBytes_m6406F00D029031E5B1FB7C76E6206C37950AA8D0,
	Base64Encoder_FulfillFromLeftover_m25942D9D22ABAA4D6E4D7CC35DC4B06BDD876986,
	Base64Encoder_Flush_m6D30AA62E4FE7E4AE243955BD5885DEDCEB7C3C5,
	Base64Encoder_WriteChars_m5925C49AB37B29D2A60DD3BDD1270AD91F2C69DA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CollectionUtils_IsDictionaryType_mB14E1B02BEA74BF90220EABEAD5C25B974A045A5,
	CollectionUtils_ResolveEnumerableCollectionConstructor_m6C553198F70C217853C08C9F8FFD2D8B21F9F81A,
	CollectionUtils_ResolveEnumerableCollectionConstructor_m966F5145F4DF95EEFA2B1BD82BD79055E34BAF97,
	NULL,
	NULL,
	NULL,
	NULL,
	CollectionUtils_GetDimensions_mE2CFCFD696DA5FA323EDE9638B84B89B9F8DE56A,
	CollectionUtils_CopyFromJaggedToMultidimensionalArray_mFA6EA8B6CD9BC85F5885E36A05FF6F1B08AEA2CA,
	CollectionUtils_JaggedArrayGetValue_m23A652C55B82F8534BC7AECD115EECF5C359A4D6,
	CollectionUtils_ToMultidimensionalArray_m2EEE96BB43FF18002956B09982FE97CD63E096C9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeInformation_get_Type_m5622F49D76FA191ACDEEF95E93C36BEB91D810D8,
	TypeInformation_set_Type_mEE342CD463AB58FBEB92D67AD12C0301FD276CE4,
	TypeInformation_get_TypeCode_m2C2972ECA9A90A50E1B38E6E5BF8F35770472D14,
	TypeInformation_set_TypeCode_m398D60C782A4AF8BF24799E6F1BD7022A43E2DAC,
	TypeInformation__ctor_mED3C73AB8C3DE7CF6A9607ECB930BE28133FF398,
	ConvertUtils_GetTypeCode_m98A7A7D6E4F516DF3BA90A41B608AC616B1DCA71,
	ConvertUtils_GetTypeCode_m515C946C69926370D40753FA3939D80C18F91867,
	ConvertUtils_GetTypeInformation_m7B3EB5E8BA53BE89F20632F071F0E694D4A1902E,
	ConvertUtils_IsConvertible_m18BB3FD4936CF9B2321A76A6E55787B2D076A423,
	ConvertUtils_ParseTimeSpan_mB0F6A7CC0B0B810F0F8A61A4324DF80710CB1D78,
	ConvertUtils_CreateCastConverter_mF16BB1A5FABB8905D3419B5025FEF6F7D86A2184,
	ConvertUtils_TryConvert_mDE33D0CBE790940604C049B4E66BC8C834E07CBD,
	ConvertUtils_TryConvertInternal_mF7308B4FB8645104422574B07624F291F9E3BF4C,
	ConvertUtils_ConvertOrCast_m234B06B247020C0884EC612D9057AC6FF8FB53B8,
	ConvertUtils_EnsureTypeAssignable_m33695B8DF4E84BB86EBFE63EC46FDDC476DC4AAC,
	ConvertUtils_VersionTryParse_m5725816D8E7EBE423513638150CE5621C5259F6C,
	ConvertUtils_IsInteger_m2473B63BF0694439E3B03E64907923404A99A8EA,
	ConvertUtils_Int32TryParse_mA09446ED6134554E02AF78D20458EAC8EE94D917,
	ConvertUtils_Int64TryParse_m6C13096BF01222333BEF765452134BF9FD8B7A33,
	ConvertUtils_DecimalTryParse_mE20F8E94703579075A72A89A5A9E721F287CA6EB,
	ConvertUtils_TryConvertGuid_mAEDE54822286E8A7F9C7A369D14D884263BD226E,
	ConvertUtils_TryHexTextToInt_m7CE9F2F025D138159458539C02BF162A39E93940,
	ConvertUtils__cctor_m03D12791C3347C189765C85A239397104B02B401,
	TypeConvertKey_get_InitialType_m1954D54FA029A8DCBA5B4815E6EF4AF294B28532_AdjustorThunk,
	TypeConvertKey_get_TargetType_m57A6E28390929AE245A00818E79DFFD9EF100E88_AdjustorThunk,
	TypeConvertKey__ctor_m9C57E9FDCA5CF0618FE165FDA3813E7BFAEF7FEF_AdjustorThunk,
	TypeConvertKey_GetHashCode_mA906BBD90727DDCFBF61C68DCF9CAE190DE2BB46_AdjustorThunk,
	TypeConvertKey_Equals_mFD9449A0550C1C3C00B6739BDFC628EE9DF6C407_AdjustorThunk,
	TypeConvertKey_Equals_mB56CC4BEC97528C33606A8CCFD8A3D9BDDD47AC7_AdjustorThunk,
	U3CU3Ec__DisplayClass9_0__ctor_m628C6F21C0428600D9928E144BCBC172A7B51B60,
	U3CU3Ec__DisplayClass9_0_U3CCreateCastConverterU3Eb__0_m11B2092E8AE226493A956BFFBB34C250ABA57AFB,
	DateTimeParser__cctor_mE5BB176F9F0C742DEDD54367FC0C4D4570F49892,
	DateTimeParser_Parse_m8D839DF69155233C111A3066B2DBE1521ABC0F34_AdjustorThunk,
	DateTimeParser_ParseDate_m21304E123E53270F5F2C11C6CCE066A08CEA3855_AdjustorThunk,
	DateTimeParser_ParseTimeAndZoneAndWhitespace_m8B1E25B1ABBFB2AA3EDD4330DFEEF6B126B8D5D0_AdjustorThunk,
	DateTimeParser_ParseTime_mD035CFFAD3EBC4A77286479AA8C083283341AA91_AdjustorThunk,
	DateTimeParser_ParseZone_mCCFB01B219A9C4D3B33D39710AE8B0F6B89ED501_AdjustorThunk,
	DateTimeParser_Parse4Digit_m1EFA45A08E5E2B4B98F6BBBF9BF82309095BF954_AdjustorThunk,
	DateTimeParser_Parse2Digit_mC8B195410B2FE66FCA501BA1147247E768A8C5F9_AdjustorThunk,
	DateTimeParser_ParseChar_mA51C47054381CC8DE368DE6EBF0352798C241098_AdjustorThunk,
	DateTimeUtils__cctor_m9F196B690D756228460E6844A37F2F2F4FE991E2,
	DateTimeUtils_GetUtcOffset_mE63D6699FC3BE3EA24A906E115195AE43AC03D69,
	DateTimeUtils_ToSerializationMode_m44D4624BBBDBD7EA6DD360DD67C2B5F10E2AF35C,
	DateTimeUtils_EnsureDateTime_m9C23BE3D67ACE4157896A6C3AA7C1E39FC21AA6D,
	DateTimeUtils_SwitchToLocalTime_mA5951E29C4E037357C23B6CB2B7E9E782DB1CA1A,
	DateTimeUtils_SwitchToUtcTime_mF44B732D9D928D1A327CA68630457FC725AD8D57,
	DateTimeUtils_ToUniversalTicks_m18B2413B9B715E19A4394535597F4C7F0708D5DC,
	DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m6FDE9E2EFE7574BD23D0DD76EE5CEC6D928DF6CA,
	DateTimeUtils_UniversialTicksToJavaScriptTicks_m02A546342FF16C2822D140CEFD572FDD267282F4,
	DateTimeUtils_ConvertJavaScriptTicksToDateTime_m049B6E252BF83188A13A66FD326D6357889E8F9F,
	DateTimeUtils_TryParseDateTimeIso_m372F05D11A8E8C72979AB91CCF1B95E3B3BB8F2D,
	DateTimeUtils_CreateDateTime_m3DEC2C577C76B744BDB7CC3AFE694F9BB45F7185,
	DateTimeUtils_TryParseDateTime_mD789926381DB2424577C2844F9EBF3A2E93BF0D2,
	DateTimeUtils_TryParseDateTime_mA79B4B042533677F961E826D391540982E50641A,
	DateTimeUtils_TryParseMicrosoftDate_m0D741CB59BF11EE289302C0065E56F9FA08CF28D,
	DateTimeUtils_TryParseDateTimeMicrosoft_mEF817232A5326A0399F8CFD33C9A3D898D380F90,
	DateTimeUtils_TryParseDateTimeExact_m661F016F15BE6A6C71328CC54C80E1040D72EBB5,
	DateTimeUtils_TryReadOffset_m9B184DBC5236D3E7B12A319FCC7B16BD0F280671,
	DateTimeUtils_WriteDateTimeString_m08855D6349C306443F5B7F508286FAF294E0D429,
	DateTimeUtils_WriteDateTimeString_mD2B32C68D9565CEF7DF4627D491DBE99A830BB39,
	DateTimeUtils_WriteDefaultIsoDate_m1AD19F17470238D3E40E0F6724E253E72D3DC8B9,
	DateTimeUtils_CopyIntToCharArray_mA3CF1D1A6F54115B8C84D2B8467E70C822B98EC3,
	DateTimeUtils_WriteDateTimeOffset_m93D3CCF79C993A4497A0FD6C580C88C674A08F60,
	DateTimeUtils_GetDateValues_m09986AC1D8DA89BE0DD8B9194F604576C061C106,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DynamicReflectionDelegateFactory_get_Instance_m95291387B6B34CB4F73C663E9FB5AAD28013245F,
	DynamicReflectionDelegateFactory_CreateDynamicMethod_m3EB6785FC635B11D15D51D2B0CBDA0E0522D08D6,
	DynamicReflectionDelegateFactory_CreateParameterizedConstructor_m8FDA0F533CD77324D53FA77FE74A5BE9E80BB8A9,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateMethodCallIL_mE303F4377E4D81C243A1152D07F74B2FC8993AB7,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateDefaultConstructorIL_m30F17C276BB79CB8CBEB78AA3858172D3777A0FC,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateGetPropertyIL_m38A04DBD1306F7B08206A74FD6675079F3525463,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateGetFieldIL_m4DB053EAC0498977225576A4DA9070AB09C08B09,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateSetFieldIL_m07C16C2C44B9E962424D28DC097811C9FC3C7C13,
	NULL,
	DynamicReflectionDelegateFactory_GenerateCreateSetPropertyIL_m8FB91D5AB6DFBB0097D3279211226E462DF11051,
	DynamicReflectionDelegateFactory__ctor_m6C9477E8AE551BBF7FBBE322C353710B3328B0B3,
	DynamicReflectionDelegateFactory__cctor_m21DDE69EBD62A3DD74B26CEBF7CB792B14451D66,
	NULL,
	NULL,
	EnumInfo__ctor_m14535EC095DAABAA892466DDCA6C2AA04B9E1979,
	EnumUtils_InitializeValuesAndNames_m7BD33F27800EABDCA2AEFFC8DBA11A7FC0EFC4A9,
	EnumUtils_TryToString_m551684055E6EA97A54FE67F9CC56345DA8944AB5,
	EnumUtils_InternalFlagsFormat_mA3CBA6933178972F361D34F0DD265AAD3B97F2E4,
	EnumUtils_GetEnumValuesAndNames_m2F73859A109EC7DCCFFF45A08699BC83AE3A57EB,
	EnumUtils_ToUInt64_m8D8949D7F4CDFDB5BDCF39543719A3D998C66446,
	EnumUtils_ParseEnum_m4F6BE2585140D0069B07A7242935068D28EFB6DC,
	EnumUtils_MatchName_m12C1F723B5DE6ECE45C70FB1AC7C9DBC1864A0BF,
	EnumUtils_FindIndexByName_m698B9D2299D907DEC9CB7B8DBBB7A0FB059B3B96,
	EnumUtils__cctor_m199852B7357FAF08F3B070EF448852E948414C0E,
	ILGeneratorExtensions_PushInstance_mADC3C005F4F69EB47E1431C9F4C49599757EFDCE,
	ILGeneratorExtensions_PushArrayInstance_m08C6F9B9FF230A1C164828CA6A1DDBAE78AC213E,
	ILGeneratorExtensions_BoxIfNeeded_mBB63555FC24AB1D5057579662F5F474AA34E17E0,
	ILGeneratorExtensions_UnboxIfNeeded_mBB1374FD971D2FE8BE5B825F0E318E9B0F63B7BD,
	ILGeneratorExtensions_CallMethod_m4EB6FF3DBC8AEAF337A90B10796133E004B69B93,
	ILGeneratorExtensions_Return_mC6D1CA3AB4A8DB82ABF66EE30F8397D54ED73B5C,
	ImmutableCollectionsUtils_TryBuildImmutableForArrayContract_m81A3ACB22C9B84F4335306C352CE046B4FAD284E,
	ImmutableCollectionsUtils_TryBuildImmutableForDictionaryContract_m27EBC590DDE00BE213C3AB4B6307D734E206D089,
	ImmutableCollectionsUtils__cctor_mC46C3C5685050770CCCBA64EF3DDFFEC7683DEDF,
	ImmutableCollectionTypeInfo__ctor_mF1EBB7743E6A002B21CE59C303D861471AD2A195,
	ImmutableCollectionTypeInfo_get_ContractTypeName_m0D5E4B609C011E897FAE6F6C47B0502F56BD5635,
	ImmutableCollectionTypeInfo_set_ContractTypeName_mEB75E6E71DFB1BEA2C3F87F871FD96055035198E,
	ImmutableCollectionTypeInfo_get_CreatedTypeName_m2CAC914D610A537B788A82D974A397D4E7CE73AE,
	ImmutableCollectionTypeInfo_set_CreatedTypeName_m8EE937465C5EBDD89E53A27175997762DC409432,
	ImmutableCollectionTypeInfo_get_BuilderTypeName_m4283937E6AC55266696DCFC0E57A382018E48B6D,
	ImmutableCollectionTypeInfo_set_BuilderTypeName_mB3F1C174DE747A6522B54B314511E680EF9CCB0D,
	U3CU3Ec__DisplayClass24_0__ctor_m8692CCC41C38ECDB8B8AF27AA3BD97E5A80B5C5A,
	U3CU3Ec__DisplayClass24_0_U3CTryBuildImmutableForArrayContractU3Eb__0_m419CAD460910D17E5761A4AC3C894C1800C239C7,
	U3CU3Ec__cctor_m5EB0EA7E47B0766F4CBBA49E52BC1CDA337F3E25,
	U3CU3Ec__ctor_mE1A296AD7C06D6A5A41BEFEE471F7820BD23BE07,
	U3CU3Ec_U3CTryBuildImmutableForArrayContractU3Eb__24_1_m56CA826DCEE7572D015F4023C39FB9A2ACF991C0,
	U3CU3Ec_U3CTryBuildImmutableForDictionaryContractU3Eb__25_1_m957911CD1C97CE609DB7C23025177FBD036D8617,
	U3CU3Ec__DisplayClass25_0__ctor_m79E4006A3FBB69367152145E78F8C8902A4E1643,
	U3CU3Ec__DisplayClass25_0_U3CTryBuildImmutableForDictionaryContractU3Eb__0_mE023CD0E6B4C032086918D6D3C531B713BB5A440,
	BufferUtils_RentBuffer_m30FA4797B8C91B658B4F018965C36DC83AA948FE,
	BufferUtils_ReturnBuffer_mED00AD7AB01720B97653280A0AD83F0A1E97548C,
	BufferUtils_EnsureBufferSize_m9A5B7B4E4DF71AA8F2F651CC56A554B438E67EEA,
	JavaScriptUtils__cctor_mDC0BF179E7A0A6A924A44A34D391CE40A9881690,
	JavaScriptUtils_GetCharEscapeFlags_m2C28E314173C145E82B48373B5F93BD8B467A976,
	JavaScriptUtils_ShouldEscapeJavaScriptString_m351E12F2D532ABEC9F83D85C446B8827AC54F463,
	JavaScriptUtils_WriteEscapedJavaScriptString_m0849BCD2BFE93613932C0D22A9D0117D6BF12E00,
	JavaScriptUtils_ToEscapedJavaScriptString_m7A0B5E200B4C9BE6C8734233E75E85E5435FB5C8,
	JavaScriptUtils_FirstCharToEscape_m42DF3535F36DA933E8458C65C7B7A752A5EB4D3E,
	JsonTokenUtils_IsEndToken_mC167BF200227D5CE769C7F458326FA49B8BEADDD,
	JsonTokenUtils_IsStartToken_m59C812E9C9992DAC3C482EEF0C90BFA391CAC543,
	JsonTokenUtils_IsPrimitiveToken_m0EE19C28ABB2C9BFEB8494CBA079CC2EC2F6B101,
	LateBoundReflectionDelegateFactory_get_Instance_mEC11C354F50176619C1C82AE633EAE27BE2AF916,
	LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m5199397D2C6AB1DB11944E9B6335AF9F18806B19,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LateBoundReflectionDelegateFactory__ctor_mB27586C750F98D3A9ADA7589F5CACE9C9217F526,
	LateBoundReflectionDelegateFactory__cctor_m78B66BE4139C12B9958AB966FAD1570451E13AA6,
	U3CU3Ec__DisplayClass3_0__ctor_m390EFB75E27606B1E85FAD9E95D3343D52A44EF1,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m9E2DECEC34D0CA6F0CE807CAE4AB07C35643917E,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_m3CCE2D05306D21B9247C422FFE49882AB909CA99,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MathUtils_IntLength_m67D092EFAFF46BD4A67DB420EC27C6030D586823,
	MathUtils_IntToHex_mAAC0332066D03A3B0A68C0571002A47D605DBCBE,
	MathUtils_ApproxEquals_mD9D25E78C742A44C398C77DC44EF680962F00B9B,
	NULL,
	NULL,
	NULL,
	NULL,
	MiscellaneousUtils_ValueEquals_m449C562930BCA7EAC9A575BBF8E070587A3DD0E6,
	MiscellaneousUtils_CreateArgumentOutOfRangeException_mCF8F5AA0C6E8ADB6D3A6CD72E4D1965905BC6522,
	MiscellaneousUtils_ByteArrayCompare_m2BB9EE4CB13E82D9282FB0DF93E5A3A979912D12,
	MiscellaneousUtils_GetPrefix_mC375B32C59807F6714D93F13537EB6D04E308C67,
	MiscellaneousUtils_GetQualifiedNameParts_mB81F6B7C02BA7F17CE91E176F97B071721350035,
	MiscellaneousUtils_FormatValueForPrint_m6A35F05B228397ACB2FEF254079C5D834A4C913D,
	MiscellaneousUtils_GetRegexOptions_mC4320586C2D4FCA5E5A2E6089BA32D3F75F099FF,
	PropertyNameTable__cctor_mAED8DBA88CCA0E72E1F25C9D6005C67963D5F5B0,
	PropertyNameTable__ctor_m551F4B7013046ADDB625BC5F6582EFD1D7A124F2,
	PropertyNameTable_Get_mDAA768129FDE9D79091D1E3AA90DA7F0B75ED395,
	PropertyNameTable_Add_m47407E2B92D5C18BD58876B749E7BC6DEA742B7B,
	PropertyNameTable_AddEntry_m951FED0ED5304E4AC256B104183B79D8E381A469,
	PropertyNameTable_Grow_mDC7B3FDD166249D6626C9153A3F018DBF77FC569,
	PropertyNameTable_TextEquals_mA3CF7C8751FBC447EDF1F5ED6F1841FD5681CE9F,
	Entry__ctor_mA2149BC597D4FCBFC2E57A2293513B552BB63D99,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionDelegateFactory__ctor_mC9ADB12BECE662F977AFABB5B285150D54902E85,
	ReflectionMember_get_MemberType_mDB7835DC3BA9236241EB77743EA997B7761487BD,
	ReflectionMember_set_MemberType_mF057603A8EC1BF604016C92808ECC5C21221774E,
	ReflectionMember_get_Getter_mFEB3405C8B2C642BF5E2328D6CA418C290F4D7A4,
	ReflectionMember_set_Getter_m624FF829420D4F5BAC8363E30D03A65372E37C0D,
	ReflectionMember_set_Setter_m6577CDEDFF2B98E99D7CEF673C9127B052783ADB,
	ReflectionMember__ctor_mF7F23A185AAC6D80D004D8115EA45E9E31EC5EB9,
	ReflectionObject_get_Creator_m70A42D72CC73787F5947E94A194C042C156F4EBC,
	ReflectionObject_get_Members_m7C6354CE99FF725E1C32D5048853D305650B911C,
	ReflectionObject__ctor_m53709028AE17BEA9E037A8D69E486604BF64D0EA,
	ReflectionObject_GetValue_m704071723EF49EA1AF319125303194A021027454,
	ReflectionObject_GetType_m786A921833EADAD4F9E323109BD837D90F8C9D3A,
	ReflectionObject_Create_mFCDEDAFB543CBD5AC486281E68B708A23084ACCB,
	U3CU3Ec__DisplayClass11_0__ctor_mCBBAC22D9A5A1260F6086E1620BF505B6B0D4C2E,
	U3CU3Ec__DisplayClass11_0_U3CCreateU3Eb__0_m429E22D5256F1D86AD46809AC02D13465A1CB248,
	U3CU3Ec__DisplayClass11_1__ctor_m8158889AE32830C7D291C257E99F76AB345E4D20,
	U3CU3Ec__DisplayClass11_1_U3CCreateU3Eb__1_m81E9B80D1E6217CFEEB69CE5A3FCD87C8ED109A0,
	U3CU3Ec__DisplayClass11_2__ctor_m35ECF94E7D27EFA46499F1214E5CA214CDD9EB98,
	U3CU3Ec__DisplayClass11_2_U3CCreateU3Eb__2_mEC684468313045ED83223C839BA09C0E50E55325,
	ReflectionUtils__cctor_m0451BD7C156F6CF157F4C37BB418768A3A4BE093,
	ReflectionUtils_IsVirtual_m63B646B725C4183E021CE56970CA1D60D8B58A24,
	ReflectionUtils_GetBaseDefinition_mFA5FCF18125DAC20BE8CA9ADB3731FDF83A1E026,
	ReflectionUtils_IsPublic_m32527AA45845F6A52280996D99555A5C4ECED23E,
	ReflectionUtils_GetObjectType_mBD999995158E5ECC6A20BB1DD14AB15F9ED43E76,
	ReflectionUtils_GetTypeName_mC9E70D8E105DC9D994989D6AF55815010AF22BE4,
	ReflectionUtils_GetFullyQualifiedTypeName_m25D33D676F373D3E85FC8AB611A8E95C159FDB25,
	ReflectionUtils_RemoveAssemblyDetails_mE771D0C2FC98335B2318C6CBD2F1F51B51BE7242,
	ReflectionUtils_HasDefaultConstructor_mD2D4561252CF3D3E87592DD7CA6186DB3D81FA5A,
	ReflectionUtils_GetDefaultConstructor_mB63FE0AD6CA7980E2C393FD16C32349D13964149,
	ReflectionUtils_GetDefaultConstructor_m62A502EE983ADB01FBE1B62707931E995FAD9044,
	ReflectionUtils_IsNullable_mBD35C2D4F04415D3B28740CA753886A74F228E41,
	ReflectionUtils_IsNullableType_m190D5C00707C561FECB5C54EDA2970765016EA67,
	ReflectionUtils_EnsureNotNullableType_m35D18D864F83693924DA3358963AFEAF58F1967E,
	ReflectionUtils_IsGenericDefinition_mBA65CFC1AD852377383C7A37EA3707EA293D8B10,
	ReflectionUtils_ImplementsGenericDefinition_m21865858B0606AE227C69EBF962CEEFA2C61D1C1,
	ReflectionUtils_ImplementsGenericDefinition_m956BE177FA0D5ADB10F78820F885C47136971E2D,
	ReflectionUtils_InheritsGenericDefinition_m26FC0C9D6703ED7CE6014106CB9AA707955A79A7,
	ReflectionUtils_InheritsGenericDefinition_mA48A6276C77A1D0326B4E3BE79A8EB237005FF79,
	ReflectionUtils_InheritsGenericDefinitionInternal_m5DAAEB9A5C744B3E9B838EFEDB1C585843FD3089,
	ReflectionUtils_GetCollectionItemType_mC5FD99ED17734012701AB1B3F145560825DE7E35,
	ReflectionUtils_GetDictionaryKeyValueTypes_m3FB4EB66B8D1BE1DFA6D28D325C9D9888AB5B75C,
	ReflectionUtils_GetMemberUnderlyingType_mE824A7CF6CA344EE3D52516077CCC623CEB27452,
	ReflectionUtils_IsIndexedProperty_m11926E8DE9A89EDAED070B03E43F2F10CAC634BD,
	ReflectionUtils_IsIndexedProperty_mCFB17E4BC2968A8222652D6AE3D36BAE1C86C477,
	ReflectionUtils_GetMemberValue_mA73290834FDFE9DB888D51B4A242D43474A3A38C,
	ReflectionUtils_SetMemberValue_m15C10E14BBBF301F9D8D24ABB5B6A34FD66069F6,
	ReflectionUtils_CanReadMemberValue_m8FDC113AC7DC7C9B2DF1D7593576FD9BE80574FA,
	ReflectionUtils_CanSetMemberValue_m3E52E8863D3DC4AAD68442AE4229F7A799A0D708,
	ReflectionUtils_GetFieldsAndProperties_mD00FD8BF55EE5F1DD3D62F549A21C3EFA677F2DB,
	ReflectionUtils_IsOverridenGenericMember_m406BE64FC04713D31BC52292D94CD76109B7FAF8,
	NULL,
	NULL,
	NULL,
	ReflectionUtils_GetAttributes_mF3FAB9BAD2DE66EDFD352476774CFB2114FB7980,
	ReflectionUtils_SplitFullyQualifiedTypeName_m7CA0C210F96D8888C1EDE468FA47011F0A4787AB,
	ReflectionUtils_GetAssemblyDelimiterIndex_m623A7BDB7B25AB1B4EE7BD7619EA358B7AA5D330,
	ReflectionUtils_GetMemberInfoFromType_mC8D662479BDBF9778EA95A0E4686EFD981C0A03C,
	ReflectionUtils_GetFields_mC7BBB66C8578C8BE30E3F8A73C6D17736DC97D80,
	ReflectionUtils_GetChildPrivateFields_mC930BB29828AA19293D2BF27BCA4532AAE0292E9,
	ReflectionUtils_GetProperties_m8629EE5D0B04946663A3EE97948C7DFD3F7D7EEE,
	ReflectionUtils_RemoveFlag_m1100A917CDA3891ACF2AC949DB1AFDCB4B5E06FA,
	ReflectionUtils_GetChildPrivateProperties_m0B08F9719D18F5843549B0440D67A53F992D0B08,
	ReflectionUtils_GetDefaultValue_m4BCD103E40969465A210E08FD789DC44A1B518B0,
	U3CU3Ec__cctor_m3709607BD37BA4073ADA364B10993D9E7B8C6205,
	U3CU3Ec__ctor_mEB328C14C51205D9EA306DD6FCFB8180F4070E01,
	U3CU3Ec_U3CGetDefaultConstructorU3Eb__11_0_m96A5887B345267C96AB575E2E2633CF7F6F4CEE8,
	U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__30_0_mA0005323FF603950643C8B2B454412EF27AC5D8A,
	U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__38_0_m566B3369A66348CBEF765BE92D2BF784C596CFFC,
	U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__40_0_m8267AEBE4AA31118BFD6FC2892E117CC07EC7D02,
	U3CU3Ec__DisplayClass30_0__ctor_m3C54273EE03E4E56B5227258EEA32DFB5E3F2501,
	U3CU3Ec__DisplayClass30_0_U3CGetFieldsAndPropertiesU3Eb__1_m60E896A814DA21EDBD8720846A22FBFC48430F96,
	U3CU3Ec__DisplayClass43_0__ctor_m3A5D5F46B82672984A690F68DFFC0C39AAEA5C8D,
	U3CU3Ec__DisplayClass43_0_U3CGetChildPrivatePropertiesU3Eb__0_mB182F85C5416D038D2B587781729D8624AA0F509,
	U3CU3Ec__DisplayClass43_0_U3CGetChildPrivatePropertiesU3Eb__1_mCE3E8E1BA8E3B47F5DF4A15221D7E65D9FDDA216,
	U3CU3Ec__DisplayClass43_1__ctor_m1E3B227ADF572B9AE8EDF695E714CD6E192425F8,
	U3CU3Ec__DisplayClass43_1_U3CGetChildPrivatePropertiesU3Eb__2_m556D6BF375B2CDB5531CD3BB9ED883EE6ED4DFF2,
	TypeNameKey__ctor_mF3E794056C4B398E504D5A5FE2FBC6AF5C14BB3F_AdjustorThunk,
	TypeNameKey_GetHashCode_m0F67AAE26FB0AC8513608789BF7457FB938CFC2B_AdjustorThunk,
	TypeNameKey_Equals_mFBA09733674F70F86DE687549E683E074738F16C_AdjustorThunk,
	TypeNameKey_Equals_m5E04FEBCFF1AF599AA9688FAFA09118BFB221617_AdjustorThunk,
	StringBuffer_get_Position_m488425AE9C848AE1B6FC1166591E37FC23345F1C_AdjustorThunk,
	StringBuffer_set_Position_m5D376FC4AC916F5E0D14B163503F3B37C0E59797_AdjustorThunk,
	StringBuffer_get_IsEmpty_m926D266B656A76A95454D825EA379C587BB6CF53_AdjustorThunk,
	StringBuffer__ctor_m800C4774C1053B3C987BAAADDA2A2A4E3905EE33_AdjustorThunk,
	StringBuffer__ctor_m32FA61DF2F4B540B6F98D4056DA0E56DFC7ADC88_AdjustorThunk,
	StringBuffer_Append_m43C8A20A6470CCB7E14D7C44778920D5456BCC23_AdjustorThunk,
	StringBuffer_Append_mA728EFC8C80B11E5AC542967748F5A703BA595A1_AdjustorThunk,
	StringBuffer_Clear_m8E58F9B101AB268484B9C8C1732CE36BFA3C91CB_AdjustorThunk,
	StringBuffer_EnsureSize_m97AAE254F53E3EAC614D62199D2712033A69AEA8_AdjustorThunk,
	StringBuffer_ToString_mBBF4FA9D950121B175F11CA71F5471EF7A6BD2BD_AdjustorThunk,
	StringBuffer_ToString_m4C236F628D94A6881FFB5739293BBDEAA94D9841_AdjustorThunk,
	StringBuffer_get_InternalBuffer_m2ECD780B7AFECDB39C784EA63B66DEFCD5F56D41_AdjustorThunk,
	StringReference_get_Item_mD51926C1F89AB5CDC5DD2BA3EE74B0AD8E699ED4_AdjustorThunk,
	StringReference_get_Chars_m7668344466F173B57588F0D272F4D89B7EDFB81A_AdjustorThunk,
	StringReference_get_StartIndex_m56C5C1EA9AE2EFBA4EC53F6A319D8D7F6F68EE27_AdjustorThunk,
	StringReference_get_Length_mBCE16FC3D03133692B8AAA70AE195DFB3DE6F408_AdjustorThunk,
	StringReference__ctor_m770FE9F149443A8EB199E60CAB6D6F710DD52A72_AdjustorThunk,
	StringReference_ToString_m78957CA1DAC727D32B0D96DFD96E35B3F6DD7ED9_AdjustorThunk,
	StringReferenceExtensions_IndexOf_mA4BBEC4B1A7CB4447F0B14BF08DE69C1A9903935,
	StringReferenceExtensions_StartsWith_m9A5D5A869C4AAC862436DA728992BB33C07CC554,
	StringReferenceExtensions_EndsWith_m9CF06FE296692CF1AD35F90F2937561102468820,
	StringUtils_FormatWith_m0E88F3B87F5D71C8BF7FF7F8AE9AB1020A056D83,
	StringUtils_FormatWith_m925E08E842B2405C7C7E6F21F728463C53AF7EEC,
	StringUtils_FormatWith_m257EE6C19CB071121BDD6DBDDA6ECB2D42A85745,
	StringUtils_FormatWith_mBF018F864F41F7AE9B4EE3DE3368836AD3DA24C2,
	StringUtils_FormatWith_m2396D684ED00A0583EF4F3C87B2696558F59E9E8,
	StringUtils_CreateStringWriter_m0512DB7DD95DF6B3A5743AA8A39D6BBDD01F55AC,
	StringUtils_ToCharAsUnicode_mC3A49C9A3AEB61ADDFF81C1D316457E586200AF4,
	NULL,
	StringUtils_ToCamelCase_mF386619B170EBA1D2EA3F8DE91EF2770F653C5EB,
	StringUtils_ToLower_m53CFAE39CB886E09B7E3BC65B1EDFB18B0017368,
	StringUtils_IsHighSurrogate_mC21B3F6940910CA309D84D7FCA809434CCD2C63F,
	StringUtils_IsLowSurrogate_mF0599E63827C5C6D42C7731DAE4F6E4B02CE6A87,
	StringUtils_StartsWith_m9A6B4A2611084EA41845426D6DBE4329C0C4DBAE,
	StringUtils_EndsWith_m688679CF80F7D6ED0D8BFE4722EC34F8983D4DDD,
	StringUtils_Trim_m83F9669E0914674571A0EB0625247338F30B20DC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_MemberType_m9CF5B1EF1F8684D1F1064EAF324A8D6A86827AA3,
	TypeExtensions_ContainsGenericParameters_mFC3F48331F28D3AA46A711B2CFB6990B2BDC7020,
	TypeExtensions_IsInterface_m929BAA3EF8A9CAA2612740DCD69E86438DC6E527,
	TypeExtensions_IsGenericType_mBC936719A611E7EA7A7F106522C5CF2BDABAE3C4,
	TypeExtensions_IsGenericTypeDefinition_mF6B17FD568133CD842D9B30643DA140A84901277,
	TypeExtensions_BaseType_m38FA9CEA11834CF5AC2D1216C9EE0FCB03FF8453,
	TypeExtensions_Assembly_m136B62BD0FDBDB6DB8130754D4CC94F38CE1AA40,
	TypeExtensions_IsEnum_mE860224904C6BCC92B32C8A36DAE461652DE9451,
	TypeExtensions_IsClass_m9171AF673A6D294CD4FB8BFB20B5CEBF3590419C,
	TypeExtensions_IsSealed_m4AE126033A7929B1B927166A9C232F238080A7C4,
	TypeExtensions_IsAbstract_m91F4D2EF03EAFA37AE9D41247D6498395E2CC6F0,
	TypeExtensions_IsValueType_m7FD48105A70DFA4590FBEAEF999DAEB32A55268C,
	TypeExtensions_IsPrimitive_m766C6FAA226E8761AFD257CDFAE1AE08E0FFC52E,
	TypeExtensions_AssignableToTypeName_m0DF91C0773BF94C001F16475C1D0F25A1F5BEAD5,
	TypeExtensions_AssignableToTypeName_mFFBA9BAE800B1B4299152AE15DA342C3DD53779C,
	TypeExtensions_ImplementInterface_m06B052E52BCD41CD0BB01EBA71CE3A2D50EAB624,
	ValidationUtils_ArgumentNotNull_m73F6A3CC2CD770D8EDF72C3391869AD2668A502F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultContractResolver_get_Instance_mA143E809D478D15EFEE82356FAD69166AC1EA9C3,
	DefaultContractResolver_get_DynamicCodeGeneration_m64DB45593336BB75286B6A0B9F191E8805BF49E4,
	DefaultContractResolver_get_DefaultMembersSearchFlags_m8272C93FDEA3C6C335D16095B59C7852E038B780,
	DefaultContractResolver_set_DefaultMembersSearchFlags_mC4E35C40BC5FA44637406B2B3902184C6505A56D,
	DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mDBC665D93F83376E6A95942B969E3D8C53722BA6,
	DefaultContractResolver_get_IgnoreSerializableInterface_m741D9DD41755124D727592C9144D0D7D8D5704D6,
	DefaultContractResolver_get_IgnoreSerializableAttribute_mDAF98C7D55767841B3F87D44EC965B3E49267EC3,
	DefaultContractResolver_set_IgnoreSerializableAttribute_mC9C2DE9BAB752E354362346D9F16CC65589DBE28,
	DefaultContractResolver_get_IgnoreIsSpecifiedMembers_m07874F9681042A12570903095C64D97C8CF28509,
	DefaultContractResolver_get_IgnoreShouldSerializeMembers_m73EF95AAAEDE82855DD5FBB6597D0DD49050F0C9,
	DefaultContractResolver_get_NamingStrategy_m8A47165A0D74FD594FA3FED797DFAB370560DF72,
	DefaultContractResolver__ctor_m31FC48FF4FEF009E3BD720D3DD02D784D8F9B2A3,
	DefaultContractResolver_ResolveContract_mB8264D595EDB2C764192921F3D0689781DD102CB,
	DefaultContractResolver_GetSerializableMembers_m27C22AABF3EAC1DE2CBDB37DDAA95125BAB51709,
	DefaultContractResolver_CreateObjectContract_m5E10D4C8726A6759A23D3DB22DB3622B263644E3,
	DefaultContractResolver_ThrowUnableToSerializeError_m070F7B0D04B9921F51A068FF3D7A8ECAB2CADFB2,
	DefaultContractResolver_GetExtensionDataMemberForType_mFC604FD23D2A75B330B18386853EE608508CA004,
	DefaultContractResolver_SetExtensionDataDelegates_mBFFABF4D52C094E95A1FF4A7EFC7BCDD1ACA16AD,
	DefaultContractResolver_GetAttributeConstructor_m4870197C0EC6A65115ABF09DD4C5F47C99CA0A10,
	DefaultContractResolver_GetImmutableConstructor_m0583E2772CADA34F621742E0F9173F26C5A98CBA,
	DefaultContractResolver_GetParameterizedConstructor_m8C57A8B55826ACB79D1EBE9BD94019FE24D7BF61,
	DefaultContractResolver_CreateConstructorParameters_mBBA8A8BC677FB47D3361D42D77208A4A60A4E5A8,
	DefaultContractResolver_MatchProperty_mC68421A13E93DE762C2856A70378EAC4C3017DE5,
	DefaultContractResolver_CreatePropertyFromConstructorParameter_m329031C1C0F78435CBB2DA096A3BED1EB8075C72,
	DefaultContractResolver_ResolveContractConverter_mFAE4E607B79292EB5EFD01114088EF847D5E8027,
	DefaultContractResolver_GetDefaultCreator_m635E067E5FB88835E96050E15E7EC3852A646029,
	DefaultContractResolver_InitializeContract_m4AE3624F49C50E9A687F20C2D8E037D6B836980E,
	DefaultContractResolver_ResolveCallbackMethods_mBA6EBB3A6550257EC824048D3D9DBE4283D8520E,
	DefaultContractResolver_GetCallbackMethodsForType_m818EF38CCC9B824D2CCFC295BDE628FA3B3891B9,
	DefaultContractResolver_IsConcurrentOrObservableCollection_mD698E9FD9046E1A855B13819410137ADC4873208,
	DefaultContractResolver_ShouldSkipDeserialized_mF22D1A34DD512DD25788C9FA2CE1757215A12A5D,
	DefaultContractResolver_ShouldSkipSerializing_m303A0D3E0437DCA82B452FA6C26431D221B72DD3,
	DefaultContractResolver_GetClassHierarchyForType_m9B10B8B2DE8482191C8AE888F0E90725100BF1A7,
	DefaultContractResolver_CreateDictionaryContract_m85FFB44B012DCAF1C459D292C666DCF7E762BDEC,
	DefaultContractResolver_CreateArrayContract_m06E2B8401E3C62EA2907B333E05B8ADDC6B99118,
	DefaultContractResolver_CreatePrimitiveContract_mDCC4E4E00910DB10DF950579FD29CDFC5618F91B,
	DefaultContractResolver_CreateLinqContract_mDD6928F5A4908F33DD9DD75BF534289D831F5980,
	DefaultContractResolver_CreateISerializableContract_m729294B8EEE558B60B21E85A1AFDCF7A5DA4FA4B,
	DefaultContractResolver_CreateStringContract_m8D0F0BBC6510DDB3E22DA7227EA76BB76CA605AD,
	DefaultContractResolver_CreateContract_mA880E83A963883906B9E60B5135D0D3210405844,
	DefaultContractResolver_IsJsonPrimitiveType_m5A161C5C4FE3A43C4D9EB376022488A8B85F4D25,
	DefaultContractResolver_IsIConvertible_mF69C8E09B876524D58FC003B1A1F6D53B11C0D82,
	DefaultContractResolver_CanConvertToString_mFB449D0C68891053F64466F9CA8C706111549FE9,
	DefaultContractResolver_IsValidCallback_mA68CF0A2CD67A9F0D6D8E1DEE1FDF0815A4CEB8F,
	DefaultContractResolver_GetClrTypeFullName_mBA6FD7E5FC03D837CD14F2C658AB31ACF4A6486E,
	DefaultContractResolver_CreateProperties_mD88F8E8ED6BC6B1764A83BFDE5351744973251DB,
	DefaultContractResolver_GetNameTable_m2495823B4CB9F53A8C68F672CC6642E3C95742B6,
	DefaultContractResolver_CreateMemberValueProvider_m148D89F39386BB0B0A40230168F1431FE19F78CC,
	DefaultContractResolver_CreateProperty_mE050154CA1E0803E1F2196F4DA89DF4E9D51EDD5,
	DefaultContractResolver_SetPropertySettingsFromAttributes_mDE05CF52DA722B1EF9A2489B616E2DF7D52111E1,
	DefaultContractResolver_CreateShouldSerializeTest_m190F5A6C21F3216CA45F7979448B789B3869B2A4,
	DefaultContractResolver_SetIsSpecifiedActions_m3A91501EBC2DAD9DE353081DD57FE14A8EF08CA6,
	DefaultContractResolver_ResolvePropertyName_m2EB8945C3E292A626B1704DD69C84A1A81C19DC6,
	DefaultContractResolver_ResolveExtensionDataName_m12706E81CF3C382BFC6ACC207AEF6FD198E916EE,
	DefaultContractResolver_ResolveDictionaryKey_m2C0ED8755665192B4EBAE33D34465AA8337F9170,
	DefaultContractResolver_GetResolvedPropertyName_m7FD81548670A52AFE8A31675508FCE1817F008F6,
	DefaultContractResolver__cctor_m85C7D20B4F94BF1B5771BF6793DC5D147F8A9BB9,
	U3CU3Ec__cctor_m85CAEDB57FF7676AAC95DFD260B6481281197167,
	U3CU3Ec__ctor_m66B5C3727C9221926D751981D8976FBAC92AF194,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__39_0_m65957274B3F38B089CF9703826DD03F8C2E199B1,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__39_1_m05C8BE0493F96445A90DB2C1542137E97B6A3914,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__42_0_m34CE4C615A5C3B39FA3B9239F08FD5990F617FDB,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__42_1_mB1349BC000C88A419927DA9D7972E0C1536D47B7,
	U3CU3Ec_U3CGetAttributeConstructorU3Eb__45_0_m6897AED9EF80B0A10A7FAF89A20267B9BA3B58C1,
	U3CU3Ec_U3CCreatePropertiesU3Eb__72_0_mE22DCE6869938AD48D6C2B4E7DF99A8436656B1D,
	U3CU3Ec__DisplayClass40_0__ctor_mCA177C229D955B328FF2309BE7B5F7952BBA28DB,
	U3CU3Ec__DisplayClass40_0_U3CCreateObjectContractU3Eb__0_m1772D9B55FEF32F663C61A3617867C47944D9644,
	U3CU3Ec__DisplayClass43_0__ctor_mF2FDF84828A7B9CC2232990CCB0D9087C5B0DFE2,
	U3CU3Ec__DisplayClass43_1__ctor_mCD53D4C82A79996FE0540AD5C4A7B3376392C87B,
	U3CU3Ec__DisplayClass43_1_U3CSetExtensionDataDelegatesU3Eb__0_mB827890F42016E332D5830CB8C32C94B62324E3A,
	U3CU3Ec__DisplayClass43_2__ctor_mD09804D2CABC9BBFA37B57F7AAEA31CEFF97D1DC,
	U3CU3Ec__DisplayClass43_2_U3CSetExtensionDataDelegatesU3Eb__1_mD471676DB90927C3FCC3DEDBE9603AC0D2600146,
	U3CU3Ec__DisplayClass60_0__ctor_mD75C93650AC41535F7A80DC65446F025E68D444F,
	U3CU3Ec__DisplayClass60_0_U3CCreateDictionaryContractU3Eb__0_m7E65C583EA3A298D29437846CD399F2A065E96AF,
	U3CU3Ec__DisplayClass77_0__ctor_m03E3EFF049D6950FC322426F1E93A6985679ED05,
	U3CU3Ec__DisplayClass77_0_U3CCreateShouldSerializeTestU3Eb__0_m7594BD1829776FFC8AFE47E10BDBA3089CE75410,
	U3CU3Ec__DisplayClass78_0__ctor_mB5664B6EABBE53AF92E544828AD5BDBD1EBAE024,
	U3CU3Ec__DisplayClass78_0_U3CSetIsSpecifiedActionsU3Eb__0_m7B81C63705B9F354C21B5B7B65975F96EF0CFEE9,
	DefaultReferenceResolver_GetMappings_mE1960042FFA99D09F6E8D5A2CA2AB4EA1A87DDC0,
	DefaultReferenceResolver_ResolveReference_mF589A336E9F5DD1230CAB2E7B1F1D4CF461D3007,
	DefaultReferenceResolver_GetReference_m333599673086EAF4D1A55DF8ECF0FE6BBB100406,
	DefaultReferenceResolver_AddReference_m11D09BAFE21B5B8424C44D127DED8F2F9FC36960,
	DefaultReferenceResolver_IsReferenced_m6F43C4F7DBC6800C488310424E07C755FF1239CD,
	DefaultReferenceResolver__ctor_m8556D2A1340729EF8E9B576801A3D26A6CD80D08,
	DefaultSerializationBinder__ctor_m05B9AC290947E7CA939B8373D93A5E59AA33E69B,
	DefaultSerializationBinder_GetTypeFromTypeNameKey_mA5BB5D6C117393A47807349D4023C82FC6F17035,
	DefaultSerializationBinder_GetGenericTypeFromTypeName_mA40F741CA7AFC6723D3D834AF5DFACFFDEF825A8,
	DefaultSerializationBinder_GetTypeByName_mBA729C2421AF7C2766962D1031DD572E8273B622,
	DefaultSerializationBinder_BindToType_m13BE2767B59C18923CB1658EE4C06BA0412238F2,
	DefaultSerializationBinder_BindToName_m0EDF7C3E4A11D04A6B323FB796C68ACFE8DCFD01,
	DefaultSerializationBinder__cctor_m8E6C24AD52B4506CCB273E2A38AD6737287BDC16,
	DynamicValueProvider__ctor_mAD909816718BA1B7F74B1C3866095ACE9103EE3D,
	DynamicValueProvider_SetValue_mAD79AEB7D25C6A94DE1851886295FD72D0906615,
	DynamicValueProvider_GetValue_m87237D481A0C3773210A6064062C935ED9C85227,
	ErrorContext__ctor_mA6A6D4CEF2207DE09B3C13065682DCD945B4932A,
	ErrorContext_get_Traced_m02E5538C159F2026BDF4F20361A156D173CBA44A,
	ErrorContext_set_Traced_m396C003E7298E22C8705DA1F4C6B967DED616BAA,
	ErrorContext_get_Error_m59B747073F35AE4F417B45D697F81B6720CC8E8E,
	ErrorContext_get_Handled_m4944848F97A77AF536C336EC0B1536DC2CA8884F,
	ErrorEventArgs__ctor_m247007CD0A98AFF374E2ADE82074E883F0A48795,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonArrayContract_get_CollectionItemType_m58C0FB4AF576AFBFE83CC36F4DEAC65E8A0D7350,
	JsonArrayContract_get_IsMultidimensionalArray_mCCD0D5821D5B77E36E458CBDA3F28163A43FD454,
	JsonArrayContract_get_IsArray_mAB2C550CF36FF426BD4774317B9816CF6CB7C50E,
	JsonArrayContract_get_ShouldCreateWrapper_m9AEF6534DE709C5FC01A1C4A717CD59CFB2B817C,
	JsonArrayContract_get_CanDeserialize_m13172B489513B03324D63C2661527917B1E5714D,
	JsonArrayContract_set_CanDeserialize_m7813695B8049D30380D70688BAC0A25A21BFB288,
	JsonArrayContract_get_ParameterizedCreator_m8F45E862032D695379BF426C5270F32B54B3FDDB,
	JsonArrayContract_get_OverrideCreator_m2E46801CF67F1C660D975EB2539B9F00C0204A26,
	JsonArrayContract_set_OverrideCreator_m2BD77CC152EB6DF77F4946C3E7F00367007FE9C4,
	JsonArrayContract_get_HasParameterizedCreator_m139E86DB88D6B14B21341BD20B76E5F0D7634B47,
	JsonArrayContract_set_HasParameterizedCreator_m765F6C00D80C9E0DBC2AD3AC75FFD1FB0B386969,
	JsonArrayContract_get_HasParameterizedCreatorInternal_m284C176991121E8630D5222A5059810D08A94342,
	JsonArrayContract__ctor_m4719BDEB9AA6721F184B4EAEE498183CDAB204DB,
	JsonArrayContract_CreateWrapper_m9E518180FE80E0BC72A205247FE4EDFDE0BEC8A9,
	JsonArrayContract_CreateTemporaryCollection_m6F31E83A0BA577719EE14D397479179D982B2403,
	JsonContainerContract_get_ItemContract_mFD4146AE53DF6781FC435949D1F63774DD498F96,
	JsonContainerContract_set_ItemContract_m7FDF4D4278E98B76D633598045C6CF7B2CECFFBF,
	JsonContainerContract_get_FinalItemContract_mBFC8C36111B3B864B5A76596D9F0F7ED1C12BFBC,
	JsonContainerContract_get_ItemConverter_m6F134F5DCB3E238759E2DD0C5789AF513D4347C9,
	JsonContainerContract_set_ItemConverter_m77C608FBF8F4DC9D018020D3F667E1B54B618779,
	JsonContainerContract_get_ItemIsReference_m89701BA68E992728520E9BDC50F9B5060A634B03,
	JsonContainerContract_set_ItemIsReference_mA8F291425545C17D7E8BB1F814061DC27A19D8E8,
	JsonContainerContract_get_ItemReferenceLoopHandling_mA5C57B9B6796727730296822ED90A1703071F4CA,
	JsonContainerContract_set_ItemReferenceLoopHandling_mE6B800BA9FA5F760A182559D965C017814774B2E,
	JsonContainerContract_get_ItemTypeNameHandling_m82D5A30719DD4C5E06C51209C77873BB68E7B1A7,
	JsonContainerContract_set_ItemTypeNameHandling_mC8EA1B8FCEFFDD0D4027F47529DF713237279CA0,
	JsonContainerContract__ctor_mFB1531B7BD4786A499B4271C1D5A3493971D9A5A,
	SerializationCallback__ctor_m8734530AE12301E9AEFDC1AB11CCEA8FB4EE9BA6,
	SerializationCallback_Invoke_m524AD019CB047D6E89E1A0FA012C144CA54BE24F,
	SerializationCallback_BeginInvoke_mFF84F02BF28275D11FDCDF81EBD8B0C28065C8BE,
	SerializationCallback_EndInvoke_mDE4D476D0D7B1BA2317571896FD1B7973949412D,
	SerializationErrorCallback__ctor_mFF6F70CAFB27C7C8CEF59F712CD00FBA4035315D,
	SerializationErrorCallback_Invoke_m06FD2EAE36978C8200BC25387FDE84AEB82BE37E,
	SerializationErrorCallback_BeginInvoke_m335013C48D92B7C85264995976F8D988157DE539,
	SerializationErrorCallback_EndInvoke_m669DBD23D1F3437A77FA967A4AD3DB42250B2331,
	ExtensionDataSetter__ctor_mC7CC6584099E799A5300F0BE5E05D85B56D15D2B,
	ExtensionDataSetter_Invoke_m41C6856C3C3E0D185B4B2A837C9AFA3EDF78C2DA,
	ExtensionDataSetter_BeginInvoke_m5565380B6CE3ECCB245BA13D58DAAD0C1948230C,
	ExtensionDataSetter_EndInvoke_m4C5B6F0D74E33FCF96FA022145E158C48C8D7FF3,
	ExtensionDataGetter__ctor_mFA51DD454AAB5593A0646645E716B07AEF5A84B4,
	ExtensionDataGetter_Invoke_m6B7D37D0D2744BD1BF0D2BB1ADAB9FC5F2DB7C57,
	ExtensionDataGetter_BeginInvoke_m4561867004FE47843B5CBEECFEE02E27DE3FCA56,
	ExtensionDataGetter_EndInvoke_m467E719381C8348ED6209C314C93E18F39BBC4DA,
	JsonContract_get_UnderlyingType_m45980F01D64CE7B5AC2AEF9061FE5B3A93C08BA8,
	JsonContract_get_CreatedType_mB48F0500CE45B05A1352B65F80A2EC8A8C606570,
	JsonContract_set_CreatedType_mE341A9647D6D20455BC261311DF612F70C20F8FF,
	JsonContract_get_IsReference_mEBAE449EDAD35B1B706CB30EC8CAFB2A769AC20A,
	JsonContract_set_IsReference_m5A4CB847E6BF0AC7C2D0765E1248F86347D89709,
	JsonContract_get_Converter_m34E7F7699FBC9748F40C37E28EF0553BD6215E01,
	JsonContract_set_Converter_mBC0ADBEFC4479A9F6FB6F70DE4D9204DBCE131C0,
	JsonContract_get_InternalConverter_mB748D1B3C7C239EF77BCB381453CB66CE179EDA4,
	JsonContract_set_InternalConverter_mD8358610598F4B5C8BD0548F204A08A52D4214E5,
	JsonContract_get_OnDeserializedCallbacks_mB5B4A64BA7F646CCF088440ED75690C31024C40F,
	JsonContract_get_OnDeserializingCallbacks_m46D80EBA425C20AF2D9F2C8D47F3C8F769AC4110,
	JsonContract_get_OnSerializedCallbacks_m9F50D3434C11EDE6E5C82A774ED90F6010ECF51F,
	JsonContract_get_OnSerializingCallbacks_m9FB0353BAE7BB6D3412C50FD78845BE7D0A3FD6F,
	JsonContract_get_OnErrorCallbacks_m39667641B576F32A520243FB761B674A4A4C5088,
	JsonContract_get_DefaultCreator_mA08BD7027873CF7540F5C6F3086B1D0C87E18173,
	JsonContract_set_DefaultCreator_m64C75428A8F1E09FA0F23A8DD7E9EC476372C7C5,
	JsonContract_get_DefaultCreatorNonPublic_mE4942F277CD741CC7967C59F3DED9F2A7B5A4E4F,
	JsonContract_set_DefaultCreatorNonPublic_m7843F72D0B00583DCA81D665AF2505F545A3A8D7,
	JsonContract__ctor_mF2F3FCE50B11B81AC11B60B88F42174138F08BE4,
	JsonContract_InvokeOnSerializing_mBEF6CE17EA951BE50A178B5A4858CB2724A052FE,
	JsonContract_InvokeOnSerialized_m32A6BC2D9E689E8AA97798134F083351CCCD57C3,
	JsonContract_InvokeOnDeserializing_mBB4C99168045ED89F9CF6BF348411E582F590856,
	JsonContract_InvokeOnDeserialized_mEC2A5862C91C7983D6A7E76D5358265D7AABD81F,
	JsonContract_InvokeOnError_mECEE2A74CAD094C8EDF252131975184E0200A444,
	JsonContract_CreateSerializationCallback_mDFF6B5A0D3573E128796738D4EB45FB8C3615DC4,
	JsonContract_CreateSerializationErrorCallback_mBB90AB471B4CDA0D4DE06DA0003C3F8DE6109410,
	U3CU3Ec__DisplayClass57_0__ctor_m573A9E78F159BB8B79A7D3FDDC0A5D2CAD48B434,
	U3CU3Ec__DisplayClass57_0_U3CCreateSerializationCallbackU3Eb__0_m69BEC5AFFCA40C5FFB86FD60B74F6EA9737F8795,
	U3CU3Ec__DisplayClass58_0__ctor_m758ABE63F4A644ED468F0D847E40465449642E4D,
	U3CU3Ec__DisplayClass58_0_U3CCreateSerializationErrorCallbackU3Eb__0_mB469C70295AF9D4E5072311485CBCE46DEC204A9,
	JsonDictionaryContract_get_DictionaryKeyResolver_mA30855381E2A0E56CEFC26A939AD5A3C88ABBBF5,
	JsonDictionaryContract_set_DictionaryKeyResolver_m6C7E791F7659578704279021C7E8556B3C05EAFA,
	JsonDictionaryContract_get_DictionaryKeyType_m3B2393F0B0E6C901208622866668197A544EAF69,
	JsonDictionaryContract_get_DictionaryValueType_m18B51FFC0B141D17C842B9AB539E40419024C8D6,
	JsonDictionaryContract_get_KeyContract_mDE6300EB3CB9550CB6128903D4ED63333D355C51,
	JsonDictionaryContract_set_KeyContract_m0309C6CC0F834B10CE3552A4C5C3DC45F5444E3D,
	JsonDictionaryContract_get_ShouldCreateWrapper_m3E9DD303CD5CBF792B03471175D82DEB4862F970,
	JsonDictionaryContract_get_ParameterizedCreator_mA829B0C4882992B7E76A77064C528BBD4896309D,
	JsonDictionaryContract_get_OverrideCreator_mA20249814DA22C45A7EDFD55DF0EEC44D38C6607,
	JsonDictionaryContract_set_OverrideCreator_m08A7A0D7419745997A0E360EFC8BAF990672F368,
	JsonDictionaryContract_get_HasParameterizedCreator_mAABA49AF066CBC6190708DA93E0EEBEF63A16BFE,
	JsonDictionaryContract_set_HasParameterizedCreator_m4F901A8DD0E3BC5D544AD71796F9DB2921473E62,
	JsonDictionaryContract_get_HasParameterizedCreatorInternal_m8B412FEF7CCA1A943B7D7AEE68C6F27392E70C7D,
	JsonDictionaryContract__ctor_m97B5D4ABF93C2CE8B29FC97C478A7D0FAB2B43A7,
	JsonDictionaryContract_CreateWrapper_m229C49B193EC20E3911ECBBD3A375D5E6B9FA4CB,
	JsonDictionaryContract_CreateTemporaryDictionary_mD17DE127136BC9FCAC9C2024F351673BE4D0F739,
	JsonFormatterConverter__ctor_mD78CBC712E8FFDCC31A47A3F509F9DD4DDD51D31,
	NULL,
	JsonFormatterConverter_Convert_mF71A9A75B9415F1DA056D9E1071E3EC1F49BB5F4,
	JsonFormatterConverter_ToBoolean_m687C3A3E7CC29EC0369C3343B083BBD6429D7693,
	JsonFormatterConverter_ToInt32_m904107C732689EDE88B7B9B39832D9FB794221B9,
	JsonFormatterConverter_ToInt64_m564E7570EFA1EA5FD30A1ABF0560DC184714B8A3,
	JsonFormatterConverter_ToSingle_m6DD7701BD2A18E18DDBABCAAC6642DC667161811,
	JsonFormatterConverter_ToString_m3FABE771D7486050C38B75CF10BAAEECE6FBE712,
	JsonISerializableContract_get_ISerializableCreator_mA3D12FD5076FE7CE59CA1BDAC01858F0A62D9B9C,
	JsonISerializableContract_set_ISerializableCreator_m8103074E088F950559CB4030C1C937029B627BAA,
	JsonISerializableContract__ctor_m1650283E0A6A37DDAB9B665CA2369780DF67A9E6,
	JsonLinqContract__ctor_m884A3CE0683B618D903ECD483BA337A66B336492,
	JsonObjectContract_get_MemberSerialization_m1C3932BCD60E68117DA329840A44A6FC814171BA,
	JsonObjectContract_set_MemberSerialization_mF41E83E04B7327E3360EAD1A5A4312CD04FF154D,
	JsonObjectContract_get_ItemRequired_m2C0BAB58CAD7982A4676A9F3AEE1782BFABC4093,
	JsonObjectContract_set_ItemRequired_mD6F9F146F10F9549F16E56A62A3F919414555D1E,
	JsonObjectContract_get_ItemNullValueHandling_m3E6700775B255D9825799C221FDF5B1EC15A9657,
	JsonObjectContract_set_ItemNullValueHandling_m213FF62B5E95DE1D3B15AABD7EE82BBB3A3F80DB,
	JsonObjectContract_get_Properties_m065DF6051D2A3AF34541ACD898073356E21C5C08,
	JsonObjectContract_get_CreatorParameters_m516BA24E12A716D6147AB616C54ADAE4D4C59044,
	JsonObjectContract_get_OverrideCreator_mBE2DED7FCC2E5D92B251171246C432F134BE608F,
	JsonObjectContract_set_OverrideCreator_m2297BFE1313F2AE6C1F1C072C0CEB78C91DFE975,
	JsonObjectContract_get_ParameterizedCreator_mCCFAF03C5273A0072C0A08CCF8EC7EB8D7D7988D,
	JsonObjectContract_set_ParameterizedCreator_m3E7B7DD5BDF268CC566A71BF0A92019F7CBDFB2B,
	JsonObjectContract_get_ExtensionDataSetter_mC9692BE063000BC9B08E1CED0333A3495760AE58,
	JsonObjectContract_set_ExtensionDataSetter_m7C0F5BA0C2A7C08242130A6104297AB0B3B555A9,
	JsonObjectContract_get_ExtensionDataGetter_m2DCDBF7717530A39DEC71B3D73C69FD4D50A34EF,
	JsonObjectContract_set_ExtensionDataGetter_m334F0BC5B32791C76E6ACB987ECDC67DFFC2803A,
	JsonObjectContract_set_ExtensionDataValueType_mB5626C408F3FA6B6D433E000FC5A148872249B0F,
	JsonObjectContract_get_ExtensionDataNameResolver_mC8587CFC411EE07462A508C5E8FCB7966CCC6498,
	JsonObjectContract_set_ExtensionDataNameResolver_mD1861025CF7E48684ACCD319246D66EB5C2FE1DD,
	JsonObjectContract_get_HasRequiredOrDefaultValueProperties_m78BB6E86F2A01E8156C4F03485BC6308B1F88A35,
	JsonObjectContract__ctor_mF16D02FE32088010F6952A761EFB133AD8C6EDE3,
	JsonObjectContract_GetUninitializedObject_m7A6556A66B12E5062184B13713E1D8BB619E2187,
	JsonPrimitiveContract_get_TypeCode_m8545211B4345052B6765F2859EA4183BB20AA923,
	JsonPrimitiveContract_set_TypeCode_mD22E1C529E174A27FA53C4AD8592DF253D20B909,
	JsonPrimitiveContract__ctor_m65E54C8F30D7E7FBCF614F010C20A6DAE2F9DF6F,
	JsonPrimitiveContract__cctor_m8CB6942B41C31680FE7C65107D1094CC2D57C9B4,
	JsonProperty_get_PropertyContract_m6621EC7EE35D98EC28FB01C4868F94DE7865E7F6,
	JsonProperty_set_PropertyContract_m64F023649F7DF01BB542EA54A593E6DB6E07268F,
	JsonProperty_get_PropertyName_m3A8236BD26DEF15AEDBE08598CBA3F9689D94F5B,
	JsonProperty_set_PropertyName_m979E6473F69E230A72DA614C27B307461A6D983F,
	JsonProperty_get_DeclaringType_m976E262EC6F092BEA2FBB98CABD70CDA5E8FF63E,
	JsonProperty_set_DeclaringType_m6FCAEE65C54088658CC4160077C90F6186F9C284,
	JsonProperty_get_Order_m4EB23EACDFADFDE45C4BE2CAF5E28CB4DA1A8516,
	JsonProperty_set_Order_mE549A685FA9C4EE36AB1A0BF6EE11565768019E7,
	JsonProperty_get_UnderlyingName_mD6564BFA9D18DF7C45595E954A2399E4B7028F0E,
	JsonProperty_set_UnderlyingName_mD3EB3951C9BEF10B520E70EA38D8DDA01D34D97A,
	JsonProperty_get_ValueProvider_m972498900FA4F4E86D9DD1C466C58778A167B12F,
	JsonProperty_set_ValueProvider_m77B954D939D32BAF89E9FCF6579925932C0E5309,
	JsonProperty_set_AttributeProvider_m6CEFD85E6F1E383C4FDB97A94AD12A65209AA990,
	JsonProperty_get_PropertyType_m2FE4193B8F352B14462B16330E71EB1ED4F8FF2F,
	JsonProperty_set_PropertyType_mD35E7552F19409C6933FEC710684A2C8BD3E952D,
	JsonProperty_get_Converter_m1A1C64049C0A44B256C0179697732F127B311EFC,
	JsonProperty_set_Converter_mB35200EA2441936770DB030003A87EE6B5B09B09,
	JsonProperty_get_Ignored_mC3C447F0BC959E4D863772EE74E5AE9C50AB8FE4,
	JsonProperty_set_Ignored_m3C88E6DB452A55460D1DDEB9C42568107F11EA39,
	JsonProperty_get_Readable_mC6124AAEEF35A6B3BD15224443CA59822C36DA1F,
	JsonProperty_set_Readable_mC1C33DE05E4A92CEA5AD55D4B5A0EFA62C9D73E8,
	JsonProperty_get_Writable_m9131922BBAD606235A7A6BF77676275DCD5F3AEB,
	JsonProperty_set_Writable_m2F5F9E7FAD763692EDDB36659026565919A2F1E2,
	JsonProperty_get_HasMemberAttribute_m445D4214B47A55A73728A1271443543912E79531,
	JsonProperty_set_HasMemberAttribute_m08FE18D3015206DFAC5EC0E5EA0015B0CC755680,
	JsonProperty_get_DefaultValue_mF8DF9CA27EEBDD8D79CAF03FB23AE740E69B23AD,
	JsonProperty_set_DefaultValue_mC7EA67870E4B739B7045B4EE5CDB8751D7D4FA58,
	JsonProperty_GetResolvedDefaultValue_mA8FC3C66D112B268094678C9CD14D8334B54FC91,
	JsonProperty_get_Required_m3668F7485B68D37ED15F146FC9B317B99AF6FBC1,
	JsonProperty_get_IsReference_m3988660B672B5D301D5B3B0EFCD63F3C70AB4852,
	JsonProperty_set_IsReference_mDC54A3A02A4909671E3A2208AD3D41A140BB6546,
	JsonProperty_get_NullValueHandling_m896C71D1CCD07AE9154C80275415FD4B2A999397,
	JsonProperty_set_NullValueHandling_m3184B0C18C078D416BE8541B97F0BD074EE338AA,
	JsonProperty_get_DefaultValueHandling_m58F05A0C25616AEFADFD215DD03075CA8670084F,
	JsonProperty_set_DefaultValueHandling_mBE94482E6670757C5FC3593E79B708521F832679,
	JsonProperty_get_ReferenceLoopHandling_m116B5E02C523E9F0089407C29361364B05D7B6F2,
	JsonProperty_set_ReferenceLoopHandling_m7BBED2FB39CA67867885E69A95CF9BC7F7F54913,
	JsonProperty_get_ObjectCreationHandling_m385565294A68FB44827E1DE872A583AA2D433F93,
	JsonProperty_set_ObjectCreationHandling_mF32B2596EA272FB133BCE1DF6113459054352000,
	JsonProperty_get_TypeNameHandling_m559564A7FF806199DC77F97E3F42D04054DB2902,
	JsonProperty_set_TypeNameHandling_m053CFEA2942A28D6E83C942108E633950067647B,
	JsonProperty_get_ShouldSerialize_mE29C87E635F339B721C627A058621662D3D8DEA8,
	JsonProperty_set_ShouldSerialize_m8E658963D133DFDB4CAB5E409E1445BB96AED232,
	JsonProperty_get_ShouldDeserialize_m9A5EC533BF2CC7E94828CFA12AFF138D16555672,
	JsonProperty_get_GetIsSpecified_mC435BFA967B9E6A08C3F0AB36FBE45BAF90097DA,
	JsonProperty_set_GetIsSpecified_mC7FB140145EAF32A04D1DD2F718AE2054184D63E,
	JsonProperty_get_SetIsSpecified_m14F00C1E1D25DD1D6A6E5ABC8D754E308414A7BD,
	JsonProperty_set_SetIsSpecified_m8691BB1961C25D41A9E6AA38954F4C2FEFE11477,
	JsonProperty_ToString_mA846523AB86DD3C22C429B1CFDD58545CFE1D123,
	JsonProperty_get_ItemConverter_m66ACE06AB5396FEABB92C8E5B317374266073565,
	JsonProperty_set_ItemConverter_m0418546DC6476224EDBA1367F31B476ED5150D58,
	JsonProperty_get_ItemIsReference_mE5E54C52206ADB476FECBDE1D0940EF9C332F459,
	JsonProperty_set_ItemIsReference_m46652718B5387EFD8E910DF76934A13DE4F8A924,
	JsonProperty_get_ItemTypeNameHandling_mDD88F25E528718F2F2564CECC7CB1C0834705278,
	JsonProperty_set_ItemTypeNameHandling_m6DFD39D4422A219A742A5CAD7D4C3536DF9B8EDB,
	JsonProperty_get_ItemReferenceLoopHandling_m39C5B21061ADCFBF5455FD93EDAD16D5EF5D4D93,
	JsonProperty_set_ItemReferenceLoopHandling_m97E915C27C2A039368E13837764D4BFC1DF5CBB9,
	JsonProperty_WritePropertyName_mBE43EB4FC925AF8F70F9E6E92F9EFB7695FE1D9F,
	JsonProperty__ctor_mAFDF0B1359B6D3699C12D897FEEE602636E0D0BA,
	JsonPropertyCollection__ctor_m1488050B277F87CE5485028D1A4E213C2E72F3FB,
	JsonPropertyCollection_GetKeyForItem_m78731352A971AE62F3E5212371A64DACF604D678,
	JsonPropertyCollection_AddProperty_mBAA7916DAA0B1BCDCD463DB9F36673EA2D052CB0,
	JsonPropertyCollection_GetClosestMatchProperty_mD923C318AD20E1401ABFCDDF87AF187E91C14959,
	JsonPropertyCollection_TryGetValue_m7B1F63944D4C591ECE6AD454464345E0C68D5582,
	JsonPropertyCollection_GetProperty_m31EE66DF721691202CDC4686A5714FBA65D93185,
	JsonSerializerInternalBase__ctor_m11DAD1159345D932A16A31786B15B70E765D3988,
	JsonSerializerInternalBase_get_DefaultReferenceMappings_m7245BA4771135E8438907D478E2BB3F6122C134A,
	JsonSerializerInternalBase_ResolvedNullValueHandling_m131555084FE0A86E92045F8F3050ECFCD1BEC129,
	JsonSerializerInternalBase_GetErrorContext_mC7EBEAAD364F1391E94B16B7912AD4EB1DBC5717,
	JsonSerializerInternalBase_ClearErrorContext_m85C25D02DB5D6B78553B0A2DCB052B96BD47E071,
	JsonSerializerInternalBase_IsErrorHandled_m3D40E16A569A929358F8F7FC3E365EDFDD2762E7,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m399DD19F29973AE6AF8440C30F694B0E749E1E66,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_mB0EF7249D7B30A3F4DC491B0C022B6F4062D20E8,
	ReferenceEqualsEqualityComparer__ctor_m353BA24394163AD8A8A21704B4E1D5A287459397,
	JsonSerializerInternalReader__ctor_m4E1FD1B5EB69B283A7ADA370DE4A2B21A37289B5,
	JsonSerializerInternalReader_GetContractSafe_mC4569AA3AF6C371AD6450FE80CD974186131856A,
	JsonSerializerInternalReader_Deserialize_mC3606E927A9A9202156E5FB48E3FFF0B0C68853B,
	JsonSerializerInternalReader_GetInternalSerializer_m114BA946FB6F4A608B4D71561368EF046D655522,
	JsonSerializerInternalReader_CreateJToken_m3EF5BD66A1659C46F1EA12B93C4D9F1A4B6065BB,
	JsonSerializerInternalReader_CreateJObject_mC6EA6EFFBAD07A6522817C11CB84558E99B5D668,
	JsonSerializerInternalReader_CreateValueInternal_mA73A7B9EACB8A206CC5EEF00F23C18FCAD7F8783,
	JsonSerializerInternalReader_CoerceEmptyStringToNull_m37643F406591D05A627B3DD4AD5EE71B987C7AF2,
	JsonSerializerInternalReader_GetExpectedDescription_mEEE2D2E0911733D29B6BA2445F562AADCC1B4FB7,
	JsonSerializerInternalReader_GetConverter_mC462340EABBA12092814C7DCEF0F34D085715EBF,
	JsonSerializerInternalReader_CreateObject_mEA9F0BF0ACFE679F68C4450050741AB81CB64A58,
	JsonSerializerInternalReader_ReadMetadataPropertiesToken_m6D6A75FD666D8394035677734D08A056B7BE8EEC,
	JsonSerializerInternalReader_ReadMetadataProperties_m7EF1A3AEA96C3AAD5F7F68133FED73D271C08F2A,
	JsonSerializerInternalReader_ResolveTypeName_m03EA9DBF50163C16B9E32F64C37204E557F09F50,
	JsonSerializerInternalReader_EnsureArrayContract_m9EFDAF930FAE6F30D201F01B91DDFDB15B2D2251,
	JsonSerializerInternalReader_CreateList_mA78CFDD6B3BFE4CB4CED9DF5A7A62828182396C5,
	JsonSerializerInternalReader_HasNoDefinedType_m349CF11482D5A5001BED1C0FA44B706138FF14FC,
	JsonSerializerInternalReader_EnsureType_mD0002044CC57504ECDDFE2639451A15D0FE484C4,
	JsonSerializerInternalReader_SetPropertyValue_mDD7A849104951B8FA3B93CCAAAF845C8977EC160,
	JsonSerializerInternalReader_CalculatePropertyDetails_m0BA453FE9985672267CFFA62324BA6DCE5074491,
	JsonSerializerInternalReader_AddReference_m00411E619C5655867CA3D7988736C4923E606D61,
	JsonSerializerInternalReader_HasFlag_mD4905E3AA68799D4D422D2C25DABF0EEEEC38181,
	JsonSerializerInternalReader_ShouldSetPropertyValue_m1114DDC0AA8ED9FC7EC33108A20A44D0253E00CE,
	JsonSerializerInternalReader_CreateNewList_mC0E460958F1497328782CFA5BFD36F34A2D9CB82,
	JsonSerializerInternalReader_CreateNewDictionary_m88D09814CB7F7129B3FA1B9004E7F5935376A415,
	JsonSerializerInternalReader_OnDeserializing_m79E6B557B6C186D26153251BE6B7701B36D0AB41,
	JsonSerializerInternalReader_OnDeserialized_m8F91168F37134B56AA777BBE6AE8213D030FA329,
	JsonSerializerInternalReader_PopulateDictionary_m5C482E0BDDB328F0426383382C5D03A96F359A39,
	JsonSerializerInternalReader_PopulateMultidimensionalArray_mF56DDBEE60DA682D82D2E9CE07D9FB344A8CA01B,
	JsonSerializerInternalReader_ThrowUnexpectedEndException_m918875D8BDD21A536497103BDB3668001536D14F,
	JsonSerializerInternalReader_PopulateList_m60BEB72C49A08641972834EFABA8C2FC88EB2DAC,
	JsonSerializerInternalReader_CreateISerializable_m8BD969D811DE13161AEDE570D08462932E7EBF46,
	JsonSerializerInternalReader_CreateISerializableItem_m26E5683BFCBB961FCF0BC24B8C3A308A086201B7,
	JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_m35DC1774B76BFE97BB14E893F1C00E1E31988913,
	JsonSerializerInternalReader_DeserializeConvertable_mB8F7ADC0D7BEB196E1FBFFE6F48B6ACDEE16154A,
	JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_m480A31AECC3B5CA724C3ECFECD191BC387E726BB,
	JsonSerializerInternalReader_CreateNewObject_m3E6B7AF730C7A523759B0A5DCEDFC99410532BCA,
	JsonSerializerInternalReader_PopulateObject_m0DEE3DE2DC2B277D5C8D1EF5420016B7466624A7,
	JsonSerializerInternalReader_ShouldDeserialize_m6D555A56EADEFE976A27155BAB971AC12C33625A,
	JsonSerializerInternalReader_CheckPropertyName_mC1CDD471A620E12E3EA1215DBFF436C351738663,
	JsonSerializerInternalReader_SetExtensionData_m7FBB106DA821C7B0B512CD9A49BB4073C65E6090,
	JsonSerializerInternalReader_ReadExtensionDataValue_mF9225B343CF7017D49D32D8602F57D8A3B689298,
	JsonSerializerInternalReader_EndProcessProperty_mC1D50D24270D298E25F5C514F8848E32AC807950,
	JsonSerializerInternalReader_SetPropertyPresence_mE1B8CD7F94AD2B9222049387B0DF58C8E065CCB2,
	JsonSerializerInternalReader_HandleError_mE5A153A62CE71460457D620936E36E8477F089FA,
	CreatorPropertyContext__ctor_m327E84FD81E1FA4A7F27B831C01BB57EB7F5F120,
	U3CU3Ec__DisplayClass36_0__ctor_m3B1D885623A71042ED53E2D7BD72B83A13D2999A,
	U3CU3Ec__DisplayClass36_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_m34A13F9F5D8A4EFB43AFB75610CC09C8AD0D9DA0,
	U3CU3Ec__cctor_m59C867C073F6904AFFA98A859E11053A220E19CF,
	U3CU3Ec__ctor_m499024B9C18B797C253AC1FA44205652DFE2BE99,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_0_mC4C78099726532AE263EF01E99E16401F0CEA930,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_2_mE4FF7ABC4C0CA29894E647D2E0FD09CE53D328A6,
	U3CU3Ec_U3CPopulateObjectU3Eb__40_0_mDA526FBA9F0365411314BAD23B45960144505B01,
	U3CU3Ec_U3CPopulateObjectU3Eb__40_1_mCD37A7F02A7E54123380DA97A7AE3844FE84C2A5,
	JsonSerializerInternalWriter__ctor_mE222E23A936016CF22BC6D5DD90B513740C3EF43,
	JsonSerializerInternalWriter_Serialize_mB81AB622D9197CFA442D02EE1768BE2B938565CE,
	JsonSerializerInternalWriter_GetInternalSerializer_m29F89D7516AE6D6C5F3BE891407ED9321BF83293,
	JsonSerializerInternalWriter_GetContractSafe_mABC3E4FF52D0A52E36235E312F494D89E69634E6,
	JsonSerializerInternalWriter_SerializePrimitive_mD707F472B797F555808981F4A536D3355A8252E6,
	JsonSerializerInternalWriter_SerializeValue_m262B3526913978315CB0FE94F99CFCA365758D4C,
	JsonSerializerInternalWriter_ResolveIsReference_m791A5968F9DF07938A1F827A236DB1DB6E3596C7,
	JsonSerializerInternalWriter_ShouldWriteReference_mE52BB4845EB2E01D4D8D2A8884855E5A9DCC071A,
	JsonSerializerInternalWriter_ShouldWriteProperty_mB3A11BDAD0FE51047E3304BD0925D915606B8E35,
	JsonSerializerInternalWriter_CheckForCircularReference_m7EBAE415EE04B240222A7D1E65E4078692DAC301,
	JsonSerializerInternalWriter_WriteReference_m5AFF08F79D36642F420F6E1F4DE8058BB3F57D92,
	JsonSerializerInternalWriter_GetReference_m9DDB7A61792259E5EFFD226A2726B0A897291C26,
	JsonSerializerInternalWriter_TryConvertToString_m5AC00FAAF624D945205B7F535FD981109C893B1D,
	JsonSerializerInternalWriter_SerializeString_m516B66A3A01AF7BE2075195355D03D1053286B4D,
	JsonSerializerInternalWriter_OnSerializing_m65EEE3DC4955694C8AFA38C459EA8B385F54F384,
	JsonSerializerInternalWriter_OnSerialized_m8D2366F112D14C0B2813A0FC04F7F731B1112C6F,
	JsonSerializerInternalWriter_SerializeObject_mED95180E0BED8F456B88A1FFFE21C98CA25126D8,
	JsonSerializerInternalWriter_CalculatePropertyValues_m28BC2C83F1B4949D17084890E4C270A034A1B792,
	JsonSerializerInternalWriter_WriteObjectStart_m0DCF4C27043CF7250245957554142D60C55108B9,
	JsonSerializerInternalWriter_HasCreatorParameter_m54385E3A91443F2887ABA867C712040243ACCDFA,
	JsonSerializerInternalWriter_WriteReferenceIdProperty_m24243802EC67246895BB1167464B3F175D37DAAE,
	JsonSerializerInternalWriter_WriteTypeProperty_mF2D29E0480691A1A8AB1BA64FFC2456B91E3904A,
	JsonSerializerInternalWriter_HasFlag_mE1670D333F8969FA956E74392F744F2C300ACB8B,
	JsonSerializerInternalWriter_HasFlag_m725159C2E2861437505881504BD10509B4BA2766,
	JsonSerializerInternalWriter_HasFlag_m512DCD11A895A0542E1F8C01D22BCB27F868555A,
	JsonSerializerInternalWriter_SerializeConvertable_mB8D2C9A2439E0B54862776483B116B24BDBA5A23,
	JsonSerializerInternalWriter_SerializeList_mD579696A244DDF8957E314997DBF2C3385BFF394,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_m230F96926275B0CD5D8F2AE9946BA13C834C5CD8,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_m6A6E5FE5756EBE2745D8494BB461BB9FB2DE6AC1,
	JsonSerializerInternalWriter_WriteStartArray_mEA8ECB91996080367A9560C73E9C047F8D10E0D2,
	JsonSerializerInternalWriter_SerializeISerializable_m1A14CE5ED29D2725223A96FDC45C9FC9B1B7071E,
	JsonSerializerInternalWriter_ShouldWriteType_m2606D95F26A0BC8941734F8C13B5CF44317BEBF8,
	JsonSerializerInternalWriter_SerializeDictionary_m32DF5FF0A240870A7B5A8E50566A1B4DB7A55760,
	JsonSerializerInternalWriter_GetPropertyName_mA086590DA00420DEBDB4C98632CC993FE0A6D99C,
	JsonSerializerInternalWriter_HandleError_m7E1059B1E484F17FF2F3BFF50290B08D498F7565,
	JsonSerializerInternalWriter_ShouldSerialize_m145618199F35BF6D8AE4E59CFAB6EDDD14EC3919,
	JsonSerializerInternalWriter_IsSpecified_m1C145636865B720AE2B1A6013ED030EF668284A0,
	JsonSerializerProxy_add_Error_m2526D770DDE5BA48165929DB5DA51B56D337BE28,
	JsonSerializerProxy_remove_Error_mE4F5A003C74F0393A5B792EE628B301787B1F888,
	JsonSerializerProxy_set_ReferenceResolver_m7AC23C2EA6FB9C72943FFE9ACCC26FB7723AA733,
	JsonSerializerProxy_get_TraceWriter_m7937AD9FF02E97B62E8922FEB60B3713BCF4D251,
	JsonSerializerProxy_set_TraceWriter_m87F509EA4098C7E0D6CA8D179ED8A9B9FA8DE82F,
	JsonSerializerProxy_set_EqualityComparer_m60268277D8BF789EDC533BE9F076CD043BF18108,
	JsonSerializerProxy_get_Converters_m74C33049C2C84290700FA7EA0D99494F103F73A3,
	JsonSerializerProxy_set_DefaultValueHandling_m7A14E955B131E389616CE14BEAE254F1F487EA90,
	JsonSerializerProxy_get_ContractResolver_m9251A215AFC5A70E7D43D4CEBF6DE6FE0AB79496,
	JsonSerializerProxy_set_ContractResolver_m45046529796EC81E3335E2B9C9570D1216E1EA2D,
	JsonSerializerProxy_set_MissingMemberHandling_m25AC6F2FE7355968AC9CE8FD737FFDA52CB5AFA8,
	JsonSerializerProxy_get_NullValueHandling_mB933D08033C9FFECD2052256EB0AB777FC9AC72B,
	JsonSerializerProxy_set_NullValueHandling_m3323408ABEF098B69A2BEA3086603085B1D6686F,
	JsonSerializerProxy_set_ObjectCreationHandling_mA2259B3F44E8339EA7ACAF90984CBD2F44542349,
	JsonSerializerProxy_set_ReferenceLoopHandling_mD4B2FE502662C957735294003A578B4A48A90AF4,
	JsonSerializerProxy_set_PreserveReferencesHandling_m2FEC26DBA5637E14FDA714D5ACEDF47622C47DAE,
	JsonSerializerProxy_set_TypeNameHandling_mBD42644DBC5548793779C6167E960219621645A0,
	JsonSerializerProxy_get_MetadataPropertyHandling_m0F652BE9639ED4E6C54CA209D167767862B35E38,
	JsonSerializerProxy_set_MetadataPropertyHandling_mD40B0E2D7B76EC4C3BD2A11C0797110817322077,
	JsonSerializerProxy_set_TypeNameAssemblyFormatHandling_m26CF563B1DE9BC16BEE2B6092450B1028F9980AD,
	JsonSerializerProxy_set_ConstructorHandling_m3ECA2C3224D032A86057F9ABA95A3A3EF0F2D63C,
	JsonSerializerProxy_set_SerializationBinder_m3C41A384D98CF40F9C2BB08C0866BB907676176D,
	JsonSerializerProxy_get_Context_m67AC3276FC06E71B0FCBA8FB08A2E8CEF718A839,
	JsonSerializerProxy_set_Context_m758E2CF6587ADD2F6BF194AA9171B7BE411DB7C3,
	JsonSerializerProxy_get_CheckAdditionalContent_mFE0AC41789CD9135CC64464F0BAE3031723CAC48,
	JsonSerializerProxy_set_CheckAdditionalContent_m3070FDB34A19A3103E09E7A5337C407F59354143,
	JsonSerializerProxy_GetInternalSerializer_mC8A1487C24A158A0077AF88A3E74A0BD5D0A726B,
	JsonSerializerProxy__ctor_m7490D2667DD309D8F35387F83FC8A92028C3F4FC,
	JsonSerializerProxy__ctor_m6EABB469A96E516F4ABB182B220DA3AF82072E53,
	JsonSerializerProxy_DeserializeInternal_mEE4AB7F972B88D837AA86D2DCB5E8695BF86FEBE,
	JsonSerializerProxy_SerializeInternal_m442FC4EE02AFD20FBACC1778E65A2843A2F710BA,
	JsonStringContract__ctor_mF771B7DCB9BCCC297D09E71B1A268D13D13E626F,
	NULL,
	JsonTypeReflector_CanTypeDescriptorConvertString_m8A838B5AB1C499D72BF70EED50A6ED74DD53BC80,
	JsonTypeReflector_GetObjectMemberSerialization_mA03AD683791A0B706854179BB79E7AF838A1468D,
	JsonTypeReflector_GetJsonConverter_m0CDD012F7294FF61AA8E47CC9481457269E1DFF1,
	JsonTypeReflector_CreateJsonConverterInstance_m1EEDE1F13AF058DB5421362A8523741071D56444,
	JsonTypeReflector_CreateNamingStrategyInstance_mCD1DE82A24637E05EE085EBF2D62ADC93BDB5BB5,
	JsonTypeReflector_GetContainerNamingStrategy_mD1235F9B4B3468565F805CE15F39D048535B7C9D,
	JsonTypeReflector_GetCreator_m1AB3EA75468B4B616EF9878E369128F28E569512,
	NULL,
	NULL,
	JsonTypeReflector_IsNonSerializable_m2A8C909D5FBEAE3CA180BD01FF9AD22AA70BF66C,
	JsonTypeReflector_IsSerializable_mDFFA4CE2986EAAF4AEE97959978F8D910FF1ECA2,
	NULL,
	JsonTypeReflector_get_DynamicCodeGeneration_m200C1EB3550693B54A44CAC8A16478316E6A76D9,
	JsonTypeReflector_get_FullyTrusted_m88E91D0392DD735A3B9E655CA7AC129D1CA8A358,
	JsonTypeReflector_get_ReflectionDelegateFactory_mB6DB479B33ADB2C949EF10E3E9203F33F24BF404,
	JsonTypeReflector__cctor_mD9EB9C885D15BA8B136F35411D32F3023B1A8967,
	U3CU3Ec__DisplayClass18_0__ctor_mC1A7CB9FD6A90ECDEE4E457803C4BE177EE89098,
	U3CU3Ec__DisplayClass18_0_U3CGetCreatorU3Eb__0_m0A13F549511BFE3683FA050BF9E2A877C41429E1,
	U3CU3Ec__cctor_m7E7EBEC9A2CBAB5604164493C4DE8C50300575E0,
	U3CU3Ec__ctor_m04EB66D6C0C9B2C9BFF1001073A2AEC10CFA96EC,
	U3CU3Ec_U3CGetCreatorU3Eb__18_1_mD8E99DE8C0BCFB11BE0158B8B71BE5B529C3738D,
	NamingStrategy_get_ProcessDictionaryKeys_m45543D7B09F3ECBDF7EDD29194F7C0B802DF1376,
	NamingStrategy_get_ProcessExtensionDataNames_mC214568C21291DAFDBC899918E57FDEBF1A1FFBA,
	NamingStrategy_get_OverrideSpecifiedNames_m18521705823587C72CB6C682B2F1257593178399,
	NamingStrategy_GetPropertyName_m2BF3AF050CCC85B967787EDD9773226474823EB9,
	NamingStrategy_GetExtensionDataName_m1F916B49FB6E46C39459E1DD83BB4326C104F716,
	NamingStrategy_GetDictionaryKey_m1BCA2FCB652EE8825BCFDE1A3646269572F0BE5D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionAttributeProvider__ctor_mD0C00695E4500A380C460C6052D10DE22303488A,
	ReflectionValueProvider__ctor_m46DCFF4183FB5866D0C02BE525C3C69BC8040F6A,
	ReflectionValueProvider_SetValue_mB38C1CCC3E4CF1BFECA4E2662562774A054692AE,
	ReflectionValueProvider_GetValue_m6951935ED5FB87665B0B84B327D2A3AFD3C324CD,
	TraceJsonReader__ctor_mBBA57634B7ECA935CC02F2F65220387F8EE4A8AF,
	TraceJsonReader_GetDeserializedJsonMessage_m551ADE42ACE633249F166F1F6EB163B16C2613D3,
	TraceJsonReader_Read_mEA0DB59F9231823AEC8495F0B0D2370C15422192,
	TraceJsonReader_ReadAsInt32_m65DDD08A6D11638DFBB69485FBC0CD2C59EED9D1,
	TraceJsonReader_ReadAsString_mF7C60D00D3B7B3C7A83CDD13AE9814E14725DDEB,
	TraceJsonReader_ReadAsBytes_mADFC6B5F53A5C999E40D7AB676053C72F54E8F85,
	TraceJsonReader_ReadAsDecimal_mD869B273A584ABC02F5685C9E2E3D9FC394C14EC,
	TraceJsonReader_ReadAsDouble_m26F8BFC5231BAD21F9117743DCCD0A344521B6A2,
	TraceJsonReader_ReadAsBoolean_mE4B8430C5E50B37E9126FFB75B0B4589302515F9,
	TraceJsonReader_ReadAsDateTime_m35BDA12F383DAB69E6C773497C78691B688AAF32,
	TraceJsonReader_WriteCurrentToken_m8FC0FEC53513849812D512B3E20C54589F7F59AE,
	TraceJsonReader_get_Depth_m0F007CBB55DB9E12A8B1DE2BD3985D8A01A3675D,
	TraceJsonReader_get_Path_m6008B90D682AFAD28248C986510BF26FF904A84B,
	TraceJsonReader_get_TokenType_m732D9298CF015F7164D4D80802851F886C893B16,
	TraceJsonReader_get_Value_mE9302AC2F565717DE44DEB44EF0D1F45CAA787A1,
	TraceJsonReader_get_ValueType_m2182D11387BAAD4089C09EC3AD433100D3B81BEE,
	TraceJsonReader_Close_m0395D8A4D5842A5E8BEA4E4BE5A1DE06E0847097,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mF86BFF9795B5084D67C42A7BABB1B6CF27F4F7A8,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_mFEBDF7B5BF2777604C4AB2208C04D55962B33BFF,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_mEBA08346A784F3DE8D25DA11EE05BA420331AD12,
	TraceJsonWriter__ctor_m9E4A24698DF9460497374EBAD9A110861D5C05DA,
	TraceJsonWriter_GetSerializedJsonMessage_m0595305B3C90D0F465F41A3851438908AEF6D8DF,
	TraceJsonWriter_WriteValue_mF18E21DBDEBDDAF7D21BD0AF863B1FCAA4AFCDF5,
	TraceJsonWriter_WriteValue_m3B5110772FBE1B6C2667EF9694A0D720223AA21A,
	TraceJsonWriter_WriteValue_m36AA72FCF09CF1BC0414AE8C5EA0ED83F3098A34,
	TraceJsonWriter_WriteValue_mB6DA5077F14EFB38664A4B0D8156D81DAE7AFFA3,
	TraceJsonWriter_WriteValue_m64FC61CF870E4D36D13D92C14BDCC78153222429,
	TraceJsonWriter_WriteValue_m1D40689B39D206C0940E00262DE2D1CF6A1F1FF2,
	TraceJsonWriter_WriteValue_mD277C16BE412034B944359369C17435A7ED222D2,
	TraceJsonWriter_WriteValue_mD552653A65F280973E4373591ED447714B2D9260,
	TraceJsonWriter_WriteValue_mFA078441220387746167303603C9E6EFFC55F018,
	TraceJsonWriter_WriteValue_mBAAC77989626D5F908F2EB1D8EBFC365FF9AF155,
	TraceJsonWriter_WriteValue_m12C73A12B00E3B63DC230B6B1AE456A3753CD9A6,
	TraceJsonWriter_WriteValue_mAB528E62B91308388D52AD7D3A9E1298F2F74A51,
	TraceJsonWriter_WriteValue_m3E23A06BF40E3388ED0A50FF910BD6FB4C5AC7B2,
	TraceJsonWriter_WriteUndefined_mB84BC489A4E6321779D729916AEAE073F1F4CFCB,
	TraceJsonWriter_WriteNull_m1836FE638B2A4E29B49C29F6C4A0149A93976F72,
	TraceJsonWriter_WriteValue_m36EB5E40F380018AFC179988D3CCC959A13C1DAC,
	TraceJsonWriter_WriteValue_m1572DA725169C43F69CCE11F634B5B11747CB91A,
	TraceJsonWriter_WriteValue_m37701EBA2490C89114362BC7D9FFCF0B9360F05E,
	TraceJsonWriter_WriteValue_m462B6D6D4B9A3F5CCEAD781B425A6B01A5EE1F66,
	TraceJsonWriter_WriteValue_mCC4CB98C5F2C053142E88D418F5E295EF4BCA1F2,
	TraceJsonWriter_WriteValue_mF2D432520258B523E937930DD4D0B7EA695D0EFE,
	TraceJsonWriter_WriteValue_m0D5B3D7DA82A844FC60BABA4BBF9F74D9971A12F,
	TraceJsonWriter_WriteValue_m880B344F9DF140D3F9A8D2D549A3280CD9B0A4C1,
	TraceJsonWriter_WriteValue_m9B0FF249C11259FBD52E1E88F9193328628FAB89,
	TraceJsonWriter_WriteValue_mB6E99E9A9D0C6B5A0F048DEA34D7C895443B8AB4,
	TraceJsonWriter_WriteValue_m984365554098C2459F60909FD826001DB6A3D9B1,
	TraceJsonWriter_WriteValue_m261A772DBE1FA16AA08131A6FA449C59276FBBA7,
	TraceJsonWriter_WriteValue_mBFBC1CEEFF3C4FB0B0C8B454DB710CC1E157FD3B,
	TraceJsonWriter_WriteValue_m873CD18426F0EF63015123A4B71A30AA070E5862,
	TraceJsonWriter_WriteValue_mD8CC01FDF6F2689464D0AD905B3749ABF5076D2F,
	TraceJsonWriter_WriteValue_mBDF6599890059A1F0FB533858E36DDDF1FA9ACBF,
	TraceJsonWriter_WriteValue_mAE4CE378C154883E68D8AA7A901FC378EAAE08CA,
	TraceJsonWriter_WriteValue_mDD11F0BB6CC162EA040D58DDC6AC8F010005E85D,
	TraceJsonWriter_WriteValue_mA0D773D3AB93D04ED7D4B8B8AAD5C8FE37B73A0F,
	TraceJsonWriter_WriteValue_m055EDC8CF6BEC6E248CCC6F2FFC038B86AF3CF0C,
	TraceJsonWriter_WriteValue_m669CDDAB22B7EF7FA94C31442ACD84818B5702B8,
	TraceJsonWriter_WriteValue_m17399FCF1F94CDDF6AD0D9AAF32D1CDAB2E842B1,
	TraceJsonWriter_WriteComment_m006DEDF5164FC4930F967DEC99E1B36AC108C688,
	TraceJsonWriter_WriteStartArray_m3045FBB94AA0782E46EDF99DF4BC11DB963B2D18,
	TraceJsonWriter_WriteEndArray_mD29BCF35B02AF5DCDFF6BCCDA5E9847FB3DD1F0B,
	TraceJsonWriter_WriteStartConstructor_m506509EE96C768CE0B042840C3D40733D765A4A3,
	TraceJsonWriter_WriteEndConstructor_m8E5D9ACF8C4F21E813E77BE146E0EF4408F834EC,
	TraceJsonWriter_WritePropertyName_m41BFB13D922627F43FCA0A45535628E4B4400ED8,
	TraceJsonWriter_WritePropertyName_mECD7CA85B626219F90662591D4AEEDD4A0064784,
	TraceJsonWriter_WriteStartObject_m24965AF874C55AFCB5F64E88034A2BF14AB8D14A,
	TraceJsonWriter_WriteEndObject_mBE06596739B5167A8AEBFAC9C56ADFBB7011E98C,
	TraceJsonWriter_WriteRawValue_m3676FFA1658D364C3E406CEFA8E635288086D924,
	TraceJsonWriter_WriteRaw_mBFFD3E92F4A0A54B3A5389266768BE01CC53B138,
	TraceJsonWriter_Close_mAD2C82A79369D3B8C3A42425A14DCAAF7E7E54BA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JArray_get_ChildrenTokens_mD0B3C4C99136242F8C553486112937F53A6F2E6F,
	JArray_get_Type_m20AA8D0E086E9909D435ABA8C446CF0D9D264DB4,
	JArray__ctor_m6A528A9E3D179EBC0109F8794851133FC6E6DECD,
	JArray__ctor_m8F404215EE0EEF80D73F87218971BB0223B9B9AD,
	JArray__ctor_mD3F95410E37F7A033A4A351654124C118F819ACE,
	JArray_CloneToken_m9843ECF1C447319A4A11F8CD96282F30741F2AE9,
	JArray_Load_m536322CA98FAEF1FB3D88FC7D1C31F9933BC0F8C,
	JArray_WriteTo_mD41E500B0E43AF234A80721BA0DB7B313BD76AD1,
	JArray_get_Item_m27159814DD196B5E04D0650A31B100FF7455AE64,
	JArray_set_Item_m23419C1151763AF1E44DF256D693E4EA1C5F7596,
	JArray_IndexOfItem_mAD248300C1DCCBBC96DB5B943A4B4AE421D830AD,
	JArray_IndexOf_m30C6E4901ED006BD8C5AA6200878482E21590B2C,
	JArray_Insert_m78F358B56189EF992B8757E4A98B036B830D70A4,
	JArray_RemoveAt_mAD06E3CF67F9DBB2FD994909949A29A7A3C53C62,
	JArray_GetEnumerator_mD72FCB9F9A6793BA0758A0467C14955C6135C736,
	JArray_Add_m160E2D7D3F0562969602C53102DDC455826BAA90,
	JArray_Clear_m28166D4D99498E888389E1EE7FC6A8C403A3D2E9,
	JArray_Contains_m42E2037E2AA74D256817D8467414C0E53EB27CD9,
	JArray_CopyTo_m45C0D3692644A00637869E1C0A4F696693E5AC54,
	JArray_get_IsReadOnly_m9ECE40B3FD6F48332B36C7D5174FEFF238D06A31,
	JArray_Remove_m21EBE81EB74C443538033275B24CFD592092D2BC,
	JConstructor_get_ChildrenTokens_mA7962EFF7CBDFF0A4CA22448C63DC28EC87386D9,
	JConstructor_IndexOfItem_mB8CB3AB870A6FF8131DF2D9A4E5853DA2EADC2D2,
	JConstructor_get_Name_mC157359FD84A563E5ACF03E460AA45F7ACBBA74B,
	JConstructor_get_Type_m66D276263BCE8D1249F63012C881A43F5E3713AF,
	JConstructor__ctor_m34DD5E5F8905B948EC033F7C8DD8DBE86E4A3229,
	JConstructor__ctor_mC83550526445CC7CC2E15EE9B33E44B62A32CEBE,
	JConstructor_CloneToken_m44E0D07DF11AF7C53CD7C5B9420A7137209B2C02,
	JConstructor_WriteTo_m4910418C3CBCDF4ABBA47438CA438D7FADEBE2EE,
	JConstructor_Load_m9C5E35B2B23503A9140BF2BA2842FF4BD07FDE16,
	NULL,
	JContainer__ctor_m6F7FDE2FABAD6BBE91764B178AAE567313DB48BE,
	JContainer__ctor_m3C6F58EFBAF2E0297A8A4057DC67E8C6E2FD5D51,
	JContainer_CheckReentrancy_mE03D475F6DC2B76129BBC2CA2A65F594C58F54C5,
	JContainer_OnListChanged_m2766A48A747C95EED2550B5D20A6E8FE891106C2,
	JContainer_get_HasValues_mFC7A19073E69D0A2B98A7ACF973BDD999DA1E386,
	JContainer_get_First_m54F0FCB36A0B4F5F729897894FE2CB2FDFDC50DB,
	JContainer_get_Last_mB71BE2A5FB83CA1EF54E947CDE32F446AD915232,
	JContainer_Children_m6D4930BAFC45F524ABDB265D0F12B3FA655DE7B8,
	JContainer_IsMultiContent_m0DB18582345DF826B6B1C58B175A4531D9B80005,
	JContainer_EnsureParentToken_mDDB2265F24D756FB4968CE75FDCF60D32D4514DC,
	NULL,
	JContainer_InsertItem_mB70EF66A451AEDCCAB758A09029552DACC0D1380,
	JContainer_RemoveItemAt_m86F1E605C1D1F0AF026EBEB2397F9735425EA6C0,
	JContainer_RemoveItem_m9919857A2D11E99CC657DCE84BAA0FB8E815005D,
	JContainer_GetItem_m0A3BB5D9632475E01FEDEBA7714737F2563B3F08,
	JContainer_SetItem_m381414CF76B1A9D7AE0C35DF5114F8879EAFE27A,
	JContainer_ClearItems_mB29F66DDF001AB6723174F33674F97A1B0EC4A83,
	JContainer_ReplaceItem_mCFB836F29F3D0621931ABD43CD04B76FB1818D30,
	JContainer_ContainsItem_m2CCAA5EAE7C7B667DB638CD66F3FACD45825D21D,
	JContainer_CopyItemsTo_mEBA1493AE28A64C408549DCD006624A42838DF76,
	JContainer_IsTokenUnchanged_m628A9E2AAF1477ABD5327F53B83E3772253167B7,
	JContainer_ValidateToken_mC667CFE9ED6D7A179EB649A08F9BE2282F8083CD,
	JContainer_Add_mD9550C1B022A20DA040C9CF62C0E5C49818963EB,
	JContainer_AddAndSkipParentCheck_m99F3BF83FD083E4CC7057A4028E5E993ECA30DAF,
	JContainer_AddInternal_mFEC5E9514CD1C24FE2BFA5E34B45BDA81F99783F,
	JContainer_CreateFromContent_m910FF1BA1E07396CA41846F52AFFD823AB4B84E7,
	JContainer_RemoveAll_m5943CC7A4551BCA2F5B03BBE91904F9F7EB9F3E8,
	JContainer_ReadTokenFrom_m3388EA17B357101E326F6A6EFE79F5457988CE19,
	JContainer_ReadContentFrom_mBD320D0ABB959749F381CFBDB2CE519927CDC6A5,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_IndexOf_m817F656A9DBC40C3CE2069DD52BC1AB652988BC1,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_Insert_mA91347984FFA777140863EE5F0D0D18C3F0550FB,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_RemoveAt_m168EC5FFED847D46A51311352FBE58BCBE0C8231,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m978B3246D0B5869B6F0C9DC74F4E1F0CCEE36081,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_set_Item_m50013F935C583679C0205438B7D3A62C22788C34,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Add_m7233EB87F651D9FEB9DF1991B711E293CA5D6CA0,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Clear_mE088E128B96B65CC8DE6E487F820DE5AFD697DA3,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Contains_m13EDD25084953B846D3B2244A6E31903B492304C,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_mA535263ABEA90A2417262E8A1042D428864CCF1E,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m4313128AB5B04DC1FB9E14F1DAA45E164CF9D9B1,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Remove_mC8901C3F7DBB488E34B2F129E91CC0DC9B110CF7,
	JContainer_EnsureValue_m1B5EC4F7B78D93D5A43BDA42D91FFF1642EA2C27,
	JContainer_System_Collections_IList_Add_m877310C5A0D5E4099E12CF6DD60E8EBF44AC88A9,
	JContainer_System_Collections_IList_Clear_mCD55F64B9C65DC94AC087CC4DB61801909181082,
	JContainer_System_Collections_IList_Contains_m7AD0BF9E50173FB31E06E6345AFF83601CB406FA,
	JContainer_System_Collections_IList_IndexOf_mA5D4142B78C2A68B880A96C81134E32D7DAEC694,
	JContainer_System_Collections_IList_Insert_mE3521985A8FE5533C2968D2968D3D0C28F1960C2,
	JContainer_System_Collections_IList_get_IsFixedSize_m44891EF552BC45B49BDE37F2CED27286CF8C343B,
	JContainer_System_Collections_IList_get_IsReadOnly_m94D57777497E41990E897B256B96D2E1F8FF21CB,
	JContainer_System_Collections_IList_Remove_m073EA89913199ADBFEFF2E75A08380739F372D74,
	JContainer_System_Collections_IList_RemoveAt_mD78B333695BAEC4F4342F0A4E4C7F5257D703FAA,
	JContainer_System_Collections_IList_get_Item_m1B417EA4D104B6ACF8483DE59D3A564C838EF4BE,
	JContainer_System_Collections_IList_set_Item_m747A07DA6FF39DEEDD3FD5BB7EC19B1366DF99C6,
	JContainer_System_Collections_ICollection_CopyTo_m9BBA0D137DD5BD1566DFE10AFF8B71FA2F08BEF9,
	JContainer_get_Count_m99F43BC05DF8880EDA2BFC0DBA5B17E95966C77F,
	JContainer_System_Collections_ICollection_get_SyncRoot_m1631D700CF37EAA43B427868B4C9553FDBC263AC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JObject_get_ChildrenTokens_m63349F8B221F7904F66A3E8E06322ED77CEEB22F,
	JObject__ctor_mFA2D134BBD25FD55F332369EB3A15A06C04DC579,
	JObject__ctor_mF2575CF5A08CCE044A3C7F2655822DDDF2FC716F,
	JObject_IndexOfItem_m0730981FA1C644EBC26D76ADBFB768AB49468E27,
	JObject_InsertItem_m3AE1D6E67D92A8BD3DEC1FFB0D61AF166A8A0767,
	JObject_ValidateToken_m697C4DBD1BA5639552D80F7B72E2B1E08D945375,
	JObject_InternalPropertyChanged_m433BE9DBD9758003C2CDA0F86FF1C2360549B1C0,
	JObject_InternalPropertyChanging_m1A39468471C310AD0D2AA64B2B4C05487FFA6DE0,
	JObject_CloneToken_mFC930B61437D70324826A2CF9BE482F211EAB4AA,
	JObject_get_Type_m69BE8D6E9064FCFF626A4B4FE4B5DEBF00903ECF,
	JObject_Property_mE26936CF1739E55C76936912CED39FE0DC5AD5A8,
	JObject_get_Item_m1CE23C597F843FB711CF431147B54C5D124BCA55,
	JObject_set_Item_mFB29EFB2C36720C25D17B4A9A5D68B618236AE7D,
	JObject_Load_m38DE24D4285376E41CEBE2E9D4E522491A5AA8EB,
	JObject_FromObject_mD39912F544793B3AEF66D94FEB82BA9C86C5E157,
	JObject_FromObject_m1F0DC73281AD3BB70650C5294D6111CE18FF0DF8,
	JObject_WriteTo_mCCA9AF897DA939A64A024422B86D48A04F2CDD23,
	JObject_Add_m7FF932071977406BA90D018306931A14BB36EB3B,
	JObject_ContainsKey_mC0142ACE0F24BF6CCB9A5A9905CC54578255463B,
	JObject_Remove_m7D1577C8DFD7C7B50E4F1D37425BC1590738F20B,
	JObject_TryGetValue_m4D30DD97E819BDC4B81755A969858C3BB2678821,
	JObject_System_Collections_Generic_IDictionaryU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m4931FE22963B7DCFDE0DCDF502A5EE88561C9BC9,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_m9B5EAFF71EE780991B1D677374D14732ABFC9374,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m2A030621B90A8272D997FCE7E0EA5AB42745DD1F,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_mBBC9257F8EB4C13C443EECDE78AE41510A95996D,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_mEA19B1E022E523C1748101FEE137AD8A6A3AA69B,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_mC746613487BF8DBAA0B092D0B905DC4F78532C59,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_mBAE94220AF9D39B49E5179D75C69715D7430CB80,
	JObject_GetEnumerator_mB261154B873224E72E2DD114557C8755175E9F1E,
	JObject_OnPropertyChanged_m224474DEB0996D3D5005C04582627697C513821B,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m0E8FD96DD08F13AFD57E8B06681E10C2F0A9DF0F,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m6CF054F7971078F53171D8C1D9664A1AE8BD6562,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m73B3B45E4D5A4B08702094C25A81F678117EAB80,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m856DB8DA2ABAD39C77D7F91EA117674A78F620F9,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_mF5FDD23825622D701AE401C0F8A73BAF242398C7,
	U3CGetEnumeratorU3Ed__56__ctor_m8741736BC1E68E98801C7507F7908B734CEB76F2,
	U3CGetEnumeratorU3Ed__56_System_IDisposable_Dispose_m372F27E302BBEFE887C6D7BB1FB7F345CCAA930C,
	U3CGetEnumeratorU3Ed__56_MoveNext_m6FC4A5DEDE4CBA6D8483E11FBCC2DED58F69C8CA,
	U3CGetEnumeratorU3Ed__56_U3CU3Em__Finally1_m75AF7F284DBF610F0B4441CACDB7C2D5EFD4219C,
	U3CGetEnumeratorU3Ed__56_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_m12BD511919C2201CDB3F941C903FA2D30EC9223C,
	U3CGetEnumeratorU3Ed__56_System_Collections_IEnumerator_Reset_m2BD7E5F15A004BEDAFA86B09D504A4443EA889F9,
	U3CGetEnumeratorU3Ed__56_System_Collections_IEnumerator_get_Current_m25460B63EF440644017919ACADFEE281482DB659,
	JProperty_get_ChildrenTokens_m9D41F937B010D06641391C403044A564375282E1,
	JProperty_get_Name_m88FEA9CF6782258E27D9E3EFDD85D221EDC237B9,
	JProperty_get_Value_m49A3D944A173652865089E9109D9CC4BF3DA2516,
	JProperty_set_Value_m5FA3EA61479FB61A1B60F14854E250135E96F94B,
	JProperty__ctor_m65DDA31471E1EA58804F53FC6642946D6E93C3FF,
	JProperty_GetItem_m5C7EA580E618ED77AE81BBF7E8D79E9FBE85C103,
	JProperty_SetItem_m5CA9F70CD487800734B0F5FEB12F3F566DAEEC1F,
	JProperty_RemoveItem_m8E445D7450CBB784F1FCEC393D61EA129E8FEF10,
	JProperty_RemoveItemAt_mA1D8AC89D89CC4F24BD7F0297065C7ED81DBA131,
	JProperty_IndexOfItem_mFEC310782D3D416E8AADEC8EED40E725B3F75519,
	JProperty_InsertItem_m996BA95E55914D99B386A70AC174165CEEE0222B,
	JProperty_ContainsItem_mE85B7D291C1B951066D13D4E049C196B4E409C90,
	JProperty_ClearItems_m1AA3F6B86DC60337C217C1BAB7EDDC4DD0405908,
	JProperty_CloneToken_m8498731C39EC65562D2C96541ED7ED4E133E8C3E,
	JProperty_get_Type_mF4CE521DD7F33429F362AAB9163ABB96D6351751,
	JProperty__ctor_m6B10ACA55A67631B016A1AB88D09E5AF14C15D81,
	JProperty__ctor_mE9F9775D683A0D64A0C0A77D5D10D198565076D6,
	JProperty_WriteTo_m9B30330518D7E8436731F61D3C433D41BD33D343,
	JProperty_Load_m6DEEDF9D0CDE107413B292C452C40DDA0233C001,
	JPropertyList_GetEnumerator_m3BDE2EE7B2210D270E9645C52D4D18335A7A5018,
	JPropertyList_System_Collections_IEnumerable_GetEnumerator_m0B1F9ACDB961C6919318636E18577612E7B4482F,
	JPropertyList_Add_m780F96AFAAB8A0D863BFFD677DD5ED2A7BFE6E10,
	JPropertyList_Clear_m02AF033AF9E29E0F003ECB887BE32924A587C54E,
	JPropertyList_Contains_m58984DF894C61705D0FA8DAA468492085A726ABB,
	JPropertyList_CopyTo_m9E7B3D4A6EC591B6E61448643663972DD07730D8,
	JPropertyList_Remove_m23CE9DE25DD09C011C40057945CEBFDE181E992C,
	JPropertyList_get_Count_m43296B159804B4C2DA08B63AE3C139F17809AFF6,
	JPropertyList_get_IsReadOnly_mD59BF4C2896C5C721F0273C78B943A55760B2903,
	JPropertyList_IndexOf_mCBB240556976C0CC8BA37399544409EE24EDB804,
	JPropertyList_Insert_m57B0B9A631B92A6E5416A2DF185507DD60E4A4D1,
	JPropertyList_RemoveAt_mF887CF79C66A509F1BEFEC666B63AB8B4CACEBCA,
	JPropertyList_get_Item_mD671DBC78E470A29A369A026A884C7FC95D027C4,
	JPropertyList_set_Item_m2B37E29C71A31BA0E700CC43FA23526A9065C95E,
	JPropertyList__ctor_mFE15BB09CD511BAB9D948537A2EE3FB0C9D79B2D,
	U3CGetEnumeratorU3Ed__1__ctor_mE6CF047F86955BCD821D9C1CEC1ED1E51370FF42,
	U3CGetEnumeratorU3Ed__1_System_IDisposable_Dispose_m5CAC4DCF785BBC5CB50F8D340DD0C8421B19B366,
	U3CGetEnumeratorU3Ed__1_MoveNext_mD29CE72C4823ED6E23493120073DB4E8C6793748,
	U3CGetEnumeratorU3Ed__1_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m075DF84A051D49C2A65D1F686850B06FA38D7641,
	U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_Reset_mC4FABF2EE12A2400B8DC848E06219581308463B4,
	U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_get_Current_mBEA8B5A85CBC2AA7F164ED4A0FBFF9790A78E5CA,
	JPropertyDescriptor__ctor_mF8BC3F841E8D018FA749924B887363A1E7609017,
	JPropertyDescriptor_GetValue_m9544EE95991D48701D0B6DEDCA12B40D922B5B5C,
	JPropertyDescriptor_SetValue_m72F04DFC725FF4E49C731BD9811C4BD84D84C933,
	JPropertyDescriptor_ShouldSerializeValue_m898940041FF363D9E2CD5750D5483BF324C2F637,
	JPropertyDescriptor_get_ComponentType_m21BB3A640DCE0C1F9B6AE9FA916B9EAC44EEC96A,
	JPropertyDescriptor_get_IsReadOnly_mD9BACFA60EFB8BBB633EFC420E24F8478C87B62C,
	JPropertyDescriptor_get_PropertyType_m529C70F78738711E0A640F454E15DD40E47A37FE,
	JPropertyDescriptor_get_NameHashCode_m39BD658C402C9133DDA2064B0BAA3CBD16093632,
	JPropertyKeyedCollection__ctor_mD085018AC0EC5F3C71DB6F2BB129D58DD0B9D607,
	JPropertyKeyedCollection_AddKey_m827936AFF9630BEC17B20C38ED9751A4A3C6EE67,
	JPropertyKeyedCollection_ClearItems_mA15718911E557296737FDF94756EDEF2F26760AC,
	JPropertyKeyedCollection_Contains_m8A576E4FA8553FAA3CC58FFECD963D1559982B08,
	JPropertyKeyedCollection_EnsureDictionary_m6EA26844C37CA7ACEB6DE76695DBA2F94D0763A6,
	JPropertyKeyedCollection_GetKeyForItem_mB1F7563DDC81B9C8BB20E4B4B5D76162CEB0F41F,
	JPropertyKeyedCollection_InsertItem_m07A0B331DD04CDB30D8D2DBBB50B01743AE069C1,
	JPropertyKeyedCollection_RemoveItem_mCB64265A09E588E936F9EB767176B0CA9D4A8D96,
	JPropertyKeyedCollection_RemoveKey_m1BE4190718036D8E207EAB54C7E6D4498AB69577,
	JPropertyKeyedCollection_SetItem_mFED8D3D8D4DDA7522F1C0112751FC1467715250E,
	JPropertyKeyedCollection_TryGetValue_mFC7CDDC4347BEE67814105AFFCE640371D394356,
	JPropertyKeyedCollection_IndexOfReference_mA55206E55031655EE3AE93362BDA0135C44CD1C7,
	JPropertyKeyedCollection__cctor_mD0D61C94C1231CA4B1CB0BE849EBC203E68A2CDC,
	JRaw__ctor_mB6CE7D4DF90E64E4B2CC8B95353EBCAEF6792BC8,
	JRaw__ctor_mC683A7D742D041BA533874BE90971525BFC58D88,
	JRaw_Create_m5D4A9AB9E920920A3676FADA04355E2BB9AE18E1,
	JRaw_CloneToken_m270E9A954ED7723EE1D2D01F76D93E06E13E396D,
	JsonLoadSettings_get_CommentHandling_m0D1F1B3A7CAAC20364BB525655DD527A8427E639,
	JsonLoadSettings_get_LineInfoHandling_m550443E4267BCEF2DE97FDC87375B255B4BC4898,
	JToken_get_Parent_m9D36FAEE8570A580C52A4489E0AC81AB3DF97DAF,
	JToken_set_Parent_m25E46826CEDEC5CCD0373193B4C00DE5D9B8AB15,
	JToken_get_Root_mDBB03AB6D332BAA9C441E3E05FC595F3CF5D10D9,
	NULL,
	NULL,
	NULL,
	JToken_get_Next_mB3ABFBD8903CDB18AECD203C8486694C32C5AA1C,
	JToken_set_Next_mE1CB376E42ADAA5F0DE365177F63A5A6C83F5888,
	JToken_get_Previous_m8B146F1A9F1A7C64E5528D987AAA429DF6DB48FC,
	JToken_set_Previous_mEF6ED55A191CFA758A499C645906788865D4B40F,
	JToken_get_Path_mE558B72FCE14D8DCB3E5A43EEC5658AC7EFB5262,
	JToken__ctor_m6617FB9D1D309CE6CD7145B4D8AC50B93CAC88E3,
	JToken_get_First_mB304B6FB46ED3645725A6BF4D0EA2C327A5217AD,
	JToken_get_Last_mBB5F28CCAB0DF9B1E1B3805BA9A574611D221884,
	JToken_Children_mB119F9E9772A27F8C69F143D66EC03172151D419,
	JToken_Remove_m82702BEF06F90B07D859FD754E7D06255DE06888,
	JToken_Replace_mA44F4327EF356AEB65A461989E46F065CE23A8ED,
	NULL,
	JToken_ToString_mA2A9EC2D5D6A197006BBBCBBB5AACD99C4CA524D,
	JToken_ToString_m1F92CBAEC1C3359971DCA6EA3721EF9593C583EA,
	JToken_EnsureValue_m1A29C124C29713BCE639D4881635DD8978CEAC8E,
	JToken_GetType_m282C001D941CFB607D4991FAFE720283E9A648D9,
	JToken_ValidateToken_m70448ADFDB9DA07544828FC7B2C3353A0497DD11,
	JToken_op_Explicit_m4BBABF4C9C56A70ADA8AD1636F95DD4D4F494EBF,
	JToken_op_Explicit_m31B50113F690C48871285403438F7B080818DAF0,
	JToken_op_Explicit_mD122AE34E7356E371ED2E1A8C6B3D54B9B48AE16,
	JToken_op_Explicit_m91BE83B3E0861CFB9557DECE258C05E2E0127624,
	JToken_op_Explicit_m9C5A82B4C90793C63E27A931BEC8079FFE25520B,
	JToken_op_Explicit_m4BE5B58A0958EBF7256366D8296B6DA1EDF843EF,
	JToken_op_Explicit_mC0B29E3415EBF2088886D0DF43F1CD300428E710,
	JToken_op_Explicit_m37836E68C6B2F2C858D41CE2B223D5BF9DCB8A95,
	JToken_op_Explicit_m14BAD7E9A209B005A206AFB6C0A323CFCB7C1F70,
	JToken_op_Explicit_mBFCD6094795E2F5B0E25C72EED7AA1E39E123E5A,
	JToken_op_Explicit_mB6AEC1B995D857BC88DAE4D18633D4C1A59FF244,
	JToken_op_Explicit_m2BE76B72CEE194077BD493FB97B18ED73796BCFC,
	JToken_op_Explicit_m68CD35D0E529644D3F9445B86270C01A4ED25A90,
	JToken_op_Explicit_mDC2F81A96F4E25A31149127EC4553EEBB6CEAE5A,
	JToken_op_Explicit_m4C80AE2A668E862BA26C64A82D8CDA73AC3926BC,
	JToken_op_Explicit_m1D36670A16FCF5B2D120491806A86CA98F546337,
	JToken_op_Explicit_m74182DF8F6B9DC3CCBFB214EF9BB49A55D3F3960,
	JToken_op_Explicit_m71913A91611A2F3E8F8E9BAF2156DB72D2FFB416,
	JToken_op_Explicit_m49944554DC46B1A3D06744EEE9E1A469DE72F4BF,
	JToken_op_Explicit_m0E65643D032EE4B64024097B70742778ACD273B9,
	JToken_op_Explicit_m1476837E7785C4531A81BC92373B597CEA94809B,
	JToken_op_Explicit_m36B34442280075D1E346D08F92F54D9EFFA91FE0,
	JToken_op_Explicit_mC5A51D2B7BFAE090B3964AE80E5F3262C8AC96FA,
	JToken_op_Explicit_m263B524CBD1A0BF448DA66F9AB45404703490265,
	JToken_op_Explicit_mCAD4BF3F8A1F8D9F539AF771B7729DB6F87BE3DA,
	JToken_op_Explicit_m3221C59CA06D7231FC820752906F87C8704AABCA,
	JToken_op_Explicit_m055FB3BB521C482D722E6230E2EAAF6FA67CDD61,
	JToken_op_Explicit_mF65CE06C658E2F5C618D89638560FD1C46805B10,
	JToken_op_Explicit_m69BF66AD9E90A7B9B2A2FE51C12A7F1B7AD3F575,
	JToken_op_Explicit_mBCBCF67BF5AB3ACC55BF4AC2D6FEC9A53B4B3C02,
	JToken_op_Explicit_mD301899BF3696A96E489097A084A6A7FC54839BE,
	JToken_op_Explicit_mB312ABC861D909D196630522545CFC8C6A046749,
	JToken_op_Explicit_mAEF05510845F680C02EA58B2EDDE52CCB8132174,
	JToken_op_Explicit_mBF0AC1F8D5A3C4D105029909F90571014E7895A1,
	JToken_System_Collections_IEnumerable_GetEnumerator_mF30D971A6AD327ECA26A1BBF80195D7F5F4BE457,
	JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m621DF86EEC5E33CB74751C9CAB075218CB5F3CC8,
	JToken_CreateReader_m10C04AA980CFFE57EA4470535BC159C02482A484,
	JToken_FromObjectInternal_m48B6348922E3A9EC4621AF403499669F5CF6834F,
	JToken_ToObject_mCC5082A54F55BB82E647D5E7DDA7394A0EF6C906,
	JToken_ToObject_mB6FF6C6C8AAC566BA8945F12308468810307981B,
	JToken_ReadFrom_mDC1F3EB1D1A8C2F73A8108BD3A7A73CF9373B867,
	JToken_ReadFrom_m53AD4309BEAF4E238E7365DBC072B72A0159A1E1,
	JToken_SetLineInfo_m5ECC618ACC1BD50BC776327D19895F1507FCD01E,
	JToken_SetLineInfo_m7A7A05CA2DEE523E5696483E4AADB98A7322A66D,
	JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m8DD08E1800E92F0E20B129E4A94E304088AEACAD,
	JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m7F4D09A3F0A1DA43DFAD1972DD80E0651A405CB9,
	JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_mA5D67376DE59AF964E526528FF5FA7B3FC6DB4AE,
	JToken_System_ICloneable_Clone_m82B5A98B979BB3FE1BCFACEBB1E977257D03E016,
	JToken_DeepClone_m3D45D1CE0709D971773A03235F4FA5FD271B4380,
	JToken_AddAnnotation_m25239875C749DEA9A93D75B0B784E68A5A3C9202,
	NULL,
	JToken__cctor_mD2A5A64AC56266D9F970C3A433F571CCAC116DFF,
	LineInfoAnnotation__ctor_m86D35ED7EBB6C1C02B79695434C2D77DEAFFB001,
	JTokenReader_get_CurrentToken_m7F7C069ACA2894F6119385C6335D25D0E5C7720F,
	JTokenReader__ctor_mF5021811DC9CB1A60B27ABF1AF1AE33CF08343B1,
	JTokenReader_Read_mEDD64BC892569E5BEF98900E73C069D2E8C32AEE,
	JTokenReader_ReadOver_m77159EDED761655EE024062B53E03A6E62078951,
	JTokenReader_ReadToEnd_m2DDDD21DB0622142D517951F25D86D6D833CCB1C,
	JTokenReader_GetEndToken_mE2D7095B9770997E3D561E683FAEC15A3EF177E2,
	JTokenReader_ReadInto_mAB6C0E1E236E0753AF6CA7F64D8A086F97C2C4BC,
	JTokenReader_SetEnd_mAB383DEA935830415D973B4ACD99751533E2E6E7,
	JTokenReader_SetToken_mF292AB5EEFA1ADD3B536227B3D98CE6AFD5DF750,
	JTokenReader_SafeToString_m806AE1321BE07BFF1F419B3205D9B7525EE3C51F,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mD8BC39B3CC8251DC7D57F03C2378717672F695FE,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_mBB4CD5D75F64B74B92904A66F7CB43BE5DC04241,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m44849CF962C5C4C211A056F110A82D5B0DC1B7CB,
	JTokenReader_get_Path_m5587DC075EFCB65F9A82236250A4C3D6669CF71C,
	JTokenWriter_get_Token_mC7CA1C3AEC7D8E9D7F89B4E468615BC2BC9A772F,
	JTokenWriter__ctor_mE708378D36BD7374178413098625C6A009626AD4,
	JTokenWriter_Close_mDDD447FEAAB5543915E90079390CAA71FCAD8F11,
	JTokenWriter_WriteStartObject_m043EC4E2822323930EA7EF027092C435C401C6B7,
	JTokenWriter_AddParent_mEA2C8D9170ECEC24B9EC5CF585CFA3E031D8F1D7,
	JTokenWriter_RemoveParent_mA5DF944205B7295C6DC677864498FE86860EC5DE,
	JTokenWriter_WriteStartArray_m91B24F2ADDB19449F1BC1426B4B6EE372463D35F,
	JTokenWriter_WriteStartConstructor_m05C7F0A3D87CCC72026DAC7779C672FFACB99C1E,
	JTokenWriter_WriteEnd_mB89E18CA5BBC9C21D95003B693FF61A0D5CE8CDE,
	JTokenWriter_WritePropertyName_m65859AA2B00205D2FFF1CBDBC9B8421A4F6FBC6F,
	JTokenWriter_AddValue_mB25673EF93F183311280BAC9ED4AAA67556E674E,
	JTokenWriter_AddValue_m517F1A7FC2B715C754D4E4C2BFDB242A21A12363,
	JTokenWriter_WriteNull_m1E67B093D7C63966CC409C3DEE37D7A20AB03BB5,
	JTokenWriter_WriteUndefined_m8B22F25C9BA63358DB23A12896538CE217BEA12B,
	JTokenWriter_WriteRaw_mF344420866E341558043DC20F08007E9B65F6005,
	JTokenWriter_WriteComment_m86822BF5FDD42F67F342DD6B0B30D5B3778B702C,
	JTokenWriter_WriteValue_m87566D68600CCF40D66BB79566C599501078C6B3,
	JTokenWriter_WriteValue_mCBE08FD5727E6C0104BB5A2D0B79112DFC62015D,
	JTokenWriter_WriteValue_m59C7BAE67F8D310F42EA149AEB58B05BEF80F650,
	JTokenWriter_WriteValue_m4FBFD455D4B9D2E37B8F4DDDBF4CAC317E118A93,
	JTokenWriter_WriteValue_m9E17F66F8FC5F437D385A1163E86137973594F1D,
	JTokenWriter_WriteValue_mB0BD1A934417381A8092A0784D11B7F2110421E8,
	JTokenWriter_WriteValue_mECC633614ED365707BFF0A817D5D5A4C9875A5F4,
	JTokenWriter_WriteValue_m1D65686F3200734354ED53467CDB541551ED201F,
	JTokenWriter_WriteValue_mE076BAC613245C0315B549D0AD9CA7D0452384D1,
	JTokenWriter_WriteValue_mB945C35A6832983C1D073FC2540834585798CF70,
	JTokenWriter_WriteValue_mE949C9EE37632542B26B1F929A57FFF15B02A15F,
	JTokenWriter_WriteValue_m8E79C0B7E01213DE6B9C7A6973A54451CF470C0D,
	JTokenWriter_WriteValue_mACD883C8DA509761CABF3A9566D41B7A1CC33C17,
	JTokenWriter_WriteValue_m0769B4F650E63DEE57E0F02275AF14BA61357DBD,
	JTokenWriter_WriteValue_m2E00E57DA415557ACF7B7BAC3F0F3AEF7C5C9007,
	JTokenWriter_WriteValue_m9B28464CBFBEF2E59194312BBAB87F6D8BFFCCF1,
	JTokenWriter_WriteValue_m8E943B4508E0A22822929246CE42C89653BFD77C,
	JTokenWriter_WriteValue_m36A0F291604A5F966F1FB3B1BF8E812DA6B1F3FB,
	JTokenWriter_WriteValue_m5EA172E431ADA548DBFD630E5FA1328C1604A004,
	JTokenWriter_WriteToken_m33571582CEA55F3D6568A397078C3950989B585C,
	JValue__ctor_mAA1714A39AAFE0EEBFCB5FF3AEACC8268AC57FF3,
	JValue__ctor_m9544423AD14FC33210E2C5CBA810E73EB2310945,
	JValue__ctor_mC938EEF912C5927E0F3E6921EEAEA35A9B3905A1,
	JValue_get_HasValues_mDB37190AFDD95FF4DCBB11DD04A8132081AF84C9,
	JValue_Compare_m5332270DC40488C0210555F6A886B3DDD6C0E671,
	JValue_CompareFloat_mC2342A9C2EED12E11498088F3D3922F9494CBA34,
	JValue_CloneToken_m04C61BB65B0589EDE3C6F0DD9B8D95569F2BC076,
	JValue_CreateComment_m0173F97848C247164C2FBEC4AC5C01457360D857,
	JValue_CreateNull_m8D0F45F7CF0B9B2DA5622EF413BDF9490FCDF49F,
	JValue_CreateUndefined_m0D7F8EA0F311AE96655A0E1F141FCA51D52F503A,
	JValue_GetValueType_mD61D369AE1B3B7238DAE959CCFEDCFD43D8EBC59,
	JValue_GetStringValueType_m525F59416003AD7A696DF51CF06BF89C7A117AF9,
	JValue_get_Type_m6F26A2477D7CCB12AC3DBF2279F59D0DB6FB554B,
	JValue_get_Value_m0BCF7C89E8206431E18221C584A8F42BACC0F303,
	JValue_WriteTo_m789007675E2310ECCD89F3316690DF91F399EEF9,
	JValue_ValuesEquals_mFB9B7F1F014496C897537E327F36CC1F85DAB38D,
	JValue_Equals_m31472D11FC02641A2C1C0BAFCE0379BD0CDE0574,
	JValue_Equals_m1D2EF4D88545ECC0DA7D080DD3DC775D42A3144D,
	JValue_GetHashCode_m42ED70FA2F98B078CDE3BD981F1F48F40CF55D6E,
	JValue_ToString_mC90CFBFC6CB0782746643B1E86B58F79F532FF33,
	JValue_ToString_m5F830DD4FC2F8DFDE0D4DEDFBB3C6A0DB47DAD9A,
	JValue_ToString_m11E52A801E3CD2482E823BBFEB234BD2E5BD1774,
	JValue_System_IComparable_CompareTo_mFD51718B3036C0849AFDB67C86E883530ABC9B1E,
	JValue_CompareTo_m2EF2F2E751D7669E598B930447F3065D2C074EB3,
	JValue_System_IConvertible_GetTypeCode_m33F0B2BC4A8783389A0AF0E633C4B059F0D1674D,
	JValue_System_IConvertible_ToBoolean_m26D2B7F5C771CA926B9B96378FCB61613FAB7586,
	JValue_System_IConvertible_ToChar_m351A5394085AA3DB43B337FB4A736AC75404D7ED,
	JValue_System_IConvertible_ToSByte_m3FC667C211F8E5AE58E8655B1B823E8D7FE7AFEF,
	JValue_System_IConvertible_ToByte_mED376C6F520C603020D684EFFD454EE1D67D02D9,
	JValue_System_IConvertible_ToInt16_m02A3BB93787861864706D4557BD87C6A985B9539,
	JValue_System_IConvertible_ToUInt16_mC85CDD8AB0B381CF687AE522FF851862E2F60CCB,
	JValue_System_IConvertible_ToInt32_m6436DA2A444CA7ECF08DD3C97FC3D5C698479574,
	JValue_System_IConvertible_ToUInt32_m2A365952FD605D5529B4EE04303BAC23DB236024,
	JValue_System_IConvertible_ToInt64_mA013A28DEE59887372EA184BD75E77CA7CE6FAF0,
	JValue_System_IConvertible_ToUInt64_m7AA8CB9A899E8CCCCE16C2FED5EE90AD7F919CD2,
	JValue_System_IConvertible_ToSingle_mEB51FE9A00AC598ADA2E9CB1F45456BCE18A8285,
	JValue_System_IConvertible_ToDouble_m873731133E7F8D965501FEB11273EE2109FA10C3,
	JValue_System_IConvertible_ToDecimal_m1D354E5D5F56358215F341FB9BDC3600DE70D88C,
	JValue_System_IConvertible_ToDateTime_mD6FA83CAA5C96DF8360C601165ACFC4116879C6B,
	JValue_System_IConvertible_ToType_mC4E9F308FC745E5DFBDBD052AA618895B5976DFE,
	BinaryConverter_WriteJson_m899DB88862BDC17676CBF54E3D3BE25065373983,
	BinaryConverter_GetByteArray_m7952735C5C4857A2EBF0A4EDBFE1D1F04EA38D73,
	BinaryConverter_ReadJson_m1DCB8CBA1AA2CA80459361CC24B22CBBCDDF63DB,
	BinaryConverter_ReadByteArray_m3E2BCE7FE0975482B74A7985560EB32E96E52EB1,
	BinaryConverter_CanConvert_m24F66AE4B927162B48C3B6622103622404D52745,
	BinaryConverter__ctor_m880F324192C6DF99C1B09B8135274F9DA3DC8569,
	BsonObjectIdConverter_WriteJson_m8E50EDC6DBC50E814E9B11E3229B535897D08B2E,
	BsonObjectIdConverter_ReadJson_m8C9673BD23276ABF49700F49BA007F974D7AB05E,
	BsonObjectIdConverter_CanConvert_m6B80161E4F3B26BE9B61067189EE2C750BE547A4,
	BsonObjectIdConverter__ctor_mC16ECF13AD025A5BB98707D0109968287F25B3B1,
	DataSetConverter_WriteJson_m88B7F775AC173099615EB9D57E98883821E4C954,
	DataSetConverter_ReadJson_m4CE6E6DA7353EB2E45F307858BB6679F0E96AEF6,
	DataSetConverter_CanConvert_m8DF1F7CAE7D162765C12516BDE2BBE0C9E95CB5D,
	DataSetConverter__ctor_mE871E420ED932761B56B8D362A7529A92A6C7123,
	DataTableConverter_WriteJson_mE2FB424E7789B17ADB47805C12516457360A8BB3,
	DataTableConverter_ReadJson_m828E183BB040932E880EADDDAA27C61662110EA7,
	DataTableConverter_CreateRow_mD32680D74BB98B88DD2E0844F8D11C5E82F2C655,
	DataTableConverter_GetColumnDataType_m90F040CC6C8C9851BA00ABEE428ABD23D90B33D0,
	DataTableConverter_CanConvert_m06429F25BDD1E1807F96499925E861963499A018,
	DataTableConverter__ctor_mB4FF21BFD7AC36D1CFE7304AB3CBBBD1E846DFD0,
	KeyValuePairConverter_InitializeReflectionObject_mF6B897585DDC2C8CAF1B48BDC43CCC8023456646,
	KeyValuePairConverter_WriteJson_m0CED8EB1BDF8F17BE4B8A1E660ED20D0C66224E3,
	KeyValuePairConverter_ReadJson_m527C301B095BCAA07EFD6F33C0274E2D046B9E96,
	KeyValuePairConverter_CanConvert_m09C997BF6C652E25AD2C7E557B96BE93DDF70FB8,
	KeyValuePairConverter__ctor_m40A70CC0B8C6E5FEC04284442C1738ED8B3DBCFB,
	KeyValuePairConverter__cctor_mAEADE960BA0FB7938A15AF8998768D0465EF544B,
	RegexConverter_WriteJson_m28A08CC221C21E4322B70332F6333A849BCF6A1A,
	RegexConverter_HasFlag_m15B38A1E7546D024E518BAE8CEB7F3E85BF81733,
	RegexConverter_WriteBson_m1F347B3189F596B6B853257225CC027FE0978131,
	RegexConverter_WriteJson_mBE8C5033A3356A10C15199029C5E7032387FCB8D,
	RegexConverter_ReadJson_mEECB5D78296EE41FFBE81AABD50BB1D4ECCC4A2D,
	RegexConverter_ReadRegexString_m9CEC5177A492877F21D2D14ED010CDFA96606430,
	RegexConverter_ReadRegexObject_mD16834F88889BD9192D580AB92CE2212AF72DE0B,
	RegexConverter_CanConvert_m4F245BB9F07B23E68DECF5D5112988779B2450DB,
	RegexConverter_IsRegex_m5551BD5E6D5CF0716FBEAC57D963268822C10E97,
	RegexConverter__ctor_m11EC732064BF56396A4B2E766F8AF04BD8D22B13,
	XmlDocumentWrapper__ctor_m39414686A021603CB51D7F7A01497FA265A7846E,
	XmlDocumentWrapper_CreateComment_mD27A46753AA66ACC362D270FC783EDADBD8216CC,
	XmlDocumentWrapper_CreateTextNode_mEAE85E9CD2540BDF661E4DC7F4C653FBF1695A8B,
	XmlDocumentWrapper_CreateCDataSection_m75553E99729AAF106BA58F9831062B36018D3AE9,
	XmlDocumentWrapper_CreateWhitespace_m9EAECA84A85D87DD76E885A7E29C37D539FF6FEB,
	XmlDocumentWrapper_CreateSignificantWhitespace_m81BF520F63EAE66D0E005985D8766BAA9AC64274,
	XmlDocumentWrapper_CreateXmlDeclaration_mD9957E0A3599CC7DA587836EE36EF3252FC3DF41,
	XmlDocumentWrapper_CreateXmlDocumentType_m8935B277505D34002BE90566B9B45A9280AD366C,
	XmlDocumentWrapper_CreateProcessingInstruction_m9960B4667DC37C38E76F1BAC0FD2C7C3F03EA96C,
	XmlDocumentWrapper_CreateElement_mE1410CE9D81A971AE7B4C0DB638D40FFC8C54F70,
	XmlDocumentWrapper_CreateElement_m71D322ACBBB39B183E356BAA7B35C60939C74283,
	XmlDocumentWrapper_CreateAttribute_mDFD7498EDFB62B93811E427296A6F949671B79BF,
	XmlDocumentWrapper_CreateAttribute_m2280FA87AB60EF17E5A8E4946CC6184EF45975C0,
	XmlDocumentWrapper_get_DocumentElement_m9D380A0F6DDECAE020D35744E08E635C66598CEE,
	XmlElementWrapper__ctor_m3BFEBF542E6C15FD1FC91C7CD707F048ED3F6843,
	XmlElementWrapper_SetAttributeNode_mCECB85254A7A4A92C20BC7317F29FABAB43346E0,
	XmlElementWrapper_get_IsEmpty_m21E7ED10C65320CA265D6E8F403C7D0918991C9E,
	XmlDeclarationWrapper__ctor_m3E7E9BCB4EE576BB5DC0A85A32A5FBA5BCCAE533,
	XmlDeclarationWrapper_get_Version_m905E92A648FC289AD71C9FA16F10DA7C317C9910,
	XmlDeclarationWrapper_get_Encoding_mC18EE3C1E94D6D9D09CE7A91ACDE3DA4384DC3E3,
	XmlDeclarationWrapper_get_Standalone_m7313C136E2FC0F60B512D42139DFF24CBFC69D57,
	XmlDocumentTypeWrapper__ctor_m0102D76E30C7DA96D44DABACEE4670CC9A959831,
	XmlDocumentTypeWrapper_get_Name_m8DB8017FDD858946B565C6AE5F0F11671EF40B93,
	XmlDocumentTypeWrapper_get_System_mCA5FB6E2FEBB6BD9971D339EB774C8857313B296,
	XmlDocumentTypeWrapper_get_Public_m31B703F9C653F64B414626FD6AF162243AB8ED77,
	XmlDocumentTypeWrapper_get_InternalSubset_mEAFA9B362B062DA6B398C6BE17A409AF3F27388D,
	XmlDocumentTypeWrapper_get_LocalName_mD5CB85308E85555EA1F2EFC4AD96B55F7E725DAE,
	XmlNodeWrapper__ctor_m19CAB1F86B0AA927AB9DF3E5E42C1CE41A3957AD,
	XmlNodeWrapper_get_WrappedNode_m052F46C5326C8E3030F90179FF742C033DFC7AFC,
	XmlNodeWrapper_get_NodeType_mB47DF851140D7F28469377337D51BE05D065A871,
	XmlNodeWrapper_get_LocalName_m8EFCAA525828DD5B3B30D2D59B23D81F2DFA43D6,
	XmlNodeWrapper_get_ChildNodes_m881A29B6E1779876FC175D1490C6ECAE24BC32C6,
	XmlNodeWrapper_WrapNode_mB7F16C56365196EF10E43D5E8CD2998D3764DA3D,
	XmlNodeWrapper_get_Attributes_m3D6D29BDB703EE2F67CE3854F6A777F4624F9F3A,
	XmlNodeWrapper_get_HasAttributes_m55E2612C26D3ABBB773C7006514781A18B7C1B52,
	XmlNodeWrapper_get_ParentNode_mCDAB2FDEA0B91F875953C045B2765C550FEA3FD0,
	XmlNodeWrapper_get_Value_mABD94104C0453130E5ED458B993533A8AD5DE33E,
	XmlNodeWrapper_set_Value_m4431F0E09A6C097D3240542ED42E00E431558C53,
	XmlNodeWrapper_AppendChild_mC4477C47A87D17048CDF613DF54A652493BFB4F1,
	XmlNodeWrapper_get_NamespaceUri_mDD627CF0980E0A359F99000EEF0A4839F3ACF613,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XmlNodeConverter_get_DeserializeRootElementName_m9C1C4C122BFB0146C2B1FD55FAC7C67025443837,
	XmlNodeConverter_get_WriteArrayAttribute_mA4DD1C7C9FA5E48FBD483D870283443CE7A9F7DC,
	XmlNodeConverter_get_OmitRootObject_m1AEE7BF96A8AB19E9F7AEFA581F58B03E4A8A337,
	XmlNodeConverter_WriteJson_mFCB227A689CADE1C640299FFFBFAFB11054FE3B5,
	XmlNodeConverter_WrapXml_m83B84737527871E02E25F758F33B5D9796E98670,
	XmlNodeConverter_PushParentNamespaces_m9B8127DAE33916994C214601CAB57E90B9825B5D,
	XmlNodeConverter_ResolveFullName_mED1FFE1404158C50F26B1D818315D5565C93E6FE,
	XmlNodeConverter_GetPropertyName_mF6B8BFB5C2E2781A2F251B2C3B539F25B9C27CDE,
	XmlNodeConverter_IsArray_mA2FEF5C9A084820E53DAE5B1D585CE237E6432AA,
	XmlNodeConverter_SerializeGroupedNodes_mB7738BCB8B4A14286A41E7D209CD3302594E77DF,
	XmlNodeConverter_WriteGroupedNodes_m03FCE4A5B98373881CE5C982F2AD654C36FB28F9,
	XmlNodeConverter_WriteGroupedNodes_m366D624F56D8EEB22D2A16CE1ACF6EF01A7210D2,
	XmlNodeConverter_SerializeNode_m0FFA375208C82CE9D7FEDE24DC92B8261E7EEB7B,
	XmlNodeConverter_AllSameName_mA00C3E5865E393BA410A2002648523118A69F7E5,
	XmlNodeConverter_ReadJson_m9FF6549303169FD36D81E177C32D5A63ED7766C1,
	XmlNodeConverter_DeserializeValue_m13B9417E47F2F7C2CAB26A166C29C529043B6032,
	XmlNodeConverter_ReadElement_m94F0CF5042D5B05D675A77A4D5F5E4DFEDF728BE,
	XmlNodeConverter_CreateElement_m379912D29233C94BE4ABD11A7983F920B636F1FF,
	XmlNodeConverter_AddAttribute_mC4025770145BFB755739CD04B56F6DA72DD67B31,
	XmlNodeConverter_ConvertTokenToXmlValue_mD36CAB0AD07C39FA70DC41A5FB2FFC761154B842,
	XmlNodeConverter_ReadArrayElements_m68FFB8FF35A9C192363CC8468ED3F0F4372FEDD0,
	XmlNodeConverter_AddJsonArrayAttribute_m5310A8F0216B4DCD8721844FF8ECB8DE6ECCC2BA,
	XmlNodeConverter_ReadAttributeElements_mAC9DFC790762D84D7EE60A34D25DCA076353A40E,
	XmlNodeConverter_CreateInstruction_m7794D562A58D42F80B98F36C2D175CDF5349C785,
	XmlNodeConverter_CreateDocumentType_m10999FAE8FA184AA70D553BB7EA1F2C4396B66FA,
	XmlNodeConverter_CreateElement_m8627B98FEBD5E8A71C3112EBC4A0EA3B2CE58F0D,
	XmlNodeConverter_DeserializeNode_m850FF5DDFB166B51C67698F03B71DAB8349338BE,
	XmlNodeConverter_IsNamespaceAttribute_mD1866EEC978652551C50516BF9808DE2670F1C1B,
	XmlNodeConverter_ValueAttributes_m696FF576E296C506BAE1C311B5B8701459C4E306,
	XmlNodeConverter_CanConvert_m7A279C9706589F182A53A184F30E7467631D1D7E,
	XmlNodeConverter_IsXmlNode_m9736E155305CA69B8CF4E02F112A28B43EA1A7C7,
	XmlNodeConverter__ctor_m7481E930FDBEDE2CB9646BAFC87D082AB38F12D3,
	XmlNodeConverter__cctor_m0106E97F9B16E4DA96052DB683C6421199B22B33,
	BsonObjectId_get_Value_m4BFD28522EBF22114BFD2BC2790F72812EDD81A4,
	BsonObjectId__ctor_mCDC1599B2EBDF5F25250C1A59E866348D64023E3,
	NULL,
	BsonToken_set_Parent_m111C41C8B32998C9E219DCA585CF2081CBC63F03,
	BsonToken__ctor_m9212A9356E9351D7D40FEDD19F9C34DFD8C22D44,
	BsonObject_Add_m126D7188A156B2AA91F766F7DE94F9385A1528C7,
	BsonObject_get_Type_m900586CBC9A111F9C1B709E637BAB4898A7526C0,
	BsonArray_Add_mE1549675724A941DC68E40A94FD973EB03CDABB8,
	BsonArray_get_Type_m8E9307C0CA17BE3613969AD7CF92A79CA300FDE6,
	BsonValue__ctor_mE4ACB2C97C4F6272E3FEDFB1C1D2D8F12DAB207F,
	BsonValue_get_Type_mD8CB412F12AC87AB57E70F7BF3BE2196571F9BD4,
	BsonString__ctor_m87599A39F10501E522428B1D8E7A8660E99EAA42,
	BsonRegex_set_Pattern_m31699C93B3522B511689C31183A3FEAE70E5F867,
	BsonRegex_set_Options_m464E014FBE1475753BC87B5806FA40BAE9F16EB4,
	BsonRegex__ctor_m3C38477DF8852396E6CA2933B73B714899398743,
	BsonRegex_get_Type_m09784AC86AB1654573979B70C25BD9D7B93A2F0F,
	BsonProperty_set_Name_m724CFA189FDB017AEBED39A1311B0BE3B4F2477C,
	BsonProperty_set_Value_mB7D463A67EB8AE6FCCD014EA36875BBF0A875811,
	BsonProperty__ctor_m83CE510C551AE597B9023665E69B55955C13D0FF,
	BsonWriter_AddValue_mCFD64E6B76E21431092D4A437B62745D6A996B9A,
	BsonWriter_AddToken_mB087ECEE54BD81EF50302872CD17A95A580ACF17,
	BsonWriter_WriteObjectId_m38F4D84882019075F1DF0535A6C55D46A47EE806,
	BsonWriter_WriteRegex_m5A996A661CAECEC3025561B3E137FDFC94DD2745,
};
static const int32_t s_InvokerIndices[2099] = 
{
	23,
	23,
	23,
	-1,
	-1,
	89,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	26,
	4,
	217,
	218,
	2324,
	2325,
	2326,
	2327,
	0,
	356,
	0,
	376,
	2328,
	-1,
	-1,
	2,
	3,
	206,
	125,
	9,
	89,
	89,
	23,
	14,
	14,
	23,
	23,
	26,
	27,
	111,
	89,
	89,
	10,
	32,
	10,
	861,
	46,
	2329,
	2,
	3,
	14,
	14,
	14,
	14,
	14,
	10,
	89,
	31,
	89,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	793,
	2144,
	10,
	14,
	14,
	10,
	14,
	14,
	26,
	2330,
	23,
	32,
	10,
	10,
	89,
	793,
	2331,
	14,
	14,
	14,
	9,
	2332,
	2333,
	1360,
	2334,
	2335,
	2336,
	1362,
	2337,
	23,
	14,
	23,
	23,
	32,
	62,
	1406,
	31,
	23,
	32,
	23,
	23,
	37,
	23,
	31,
	23,
	23,
	465,
	487,
	89,
	89,
	10,
	23,
	111,
	1446,
	1,
	2,
	365,
	23,
	26,
	27,
	111,
	1,
	2,
	365,
	26,
	26,
	26,
	26,
	14,
	26,
	26,
	32,
	32,
	32,
	32,
	32,
	10,
	32,
	32,
	32,
	32,
	10,
	32,
	14,
	14,
	26,
	2338,
	346,
	89,
	31,
	89,
	23,
	4,
	0,
	4,
	0,
	137,
	-1,
	105,
	105,
	2339,
	2340,
	27,
	206,
	27,
	28,
	206,
	14,
	28,
	1,
	26,
	10,
	10,
	10,
	10,
	10,
	14,
	10,
	10,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	2338,
	3,
	26,
	23,
	31,
	32,
	508,
	508,
	205,
	23,
	226,
	841,
	2341,
	870,
	870,
	89,
	793,
	1362,
	14,
	14,
	34,
	34,
	675,
	1360,
	23,
	34,
	34,
	2335,
	2332,
	23,
	23,
	89,
	23,
	617,
	38,
	2342,
	2343,
	249,
	23,
	663,
	23,
	225,
	89,
	89,
	242,
	23,
	663,
	89,
	23,
	31,
	23,
	23,
	32,
	2344,
	105,
	31,
	1274,
	9,
	2317,
	9,
	242,
	23,
	23,
	23,
	23,
	34,
	93,
	34,
	93,
	34,
	93,
	23,
	89,
	10,
	10,
	14,
	249,
	26,
	23,
	23,
	23,
	23,
	26,
	32,
	26,
	465,
	23,
	23,
	23,
	10,
	23,
	23,
	130,
	23,
	23,
	26,
	26,
	465,
	32,
	32,
	209,
	209,
	343,
	2345,
	344,
	2346,
	31,
	617,
	617,
	617,
	31,
	31,
	968,
	331,
	313,
	26,
	1041,
	969,
	26,
	26,
	23,
	209,
	1418,
	2347,
	32,
	133,
	116,
	4,
	3,
	89,
	31,
	89,
	31,
	10,
	10,
	14,
	14,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	23,
	32,
	10,
	10,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	465,
	23,
	26,
	465,
	62,
	2316,
	112,
	112,
	26,
	32,
	23,
	37,
	32,
	37,
	23,
	32,
	23,
	23,
	23,
	32,
	23,
	23,
	26,
	26,
	26,
	32,
	32,
	209,
	209,
	343,
	344,
	31,
	617,
	617,
	617,
	31,
	31,
	968,
	331,
	1041,
	969,
	2144,
	2348,
	2349,
	2350,
	2345,
	2346,
	1361,
	2351,
	2352,
	2353,
	2354,
	2355,
	2356,
	1363,
	2357,
	2358,
	26,
	26,
	26,
	23,
	31,
	504,
	470,
	1,
	62,
	32,
	26,
	23,
	129,
	32,
	23,
	23,
	111,
	206,
	2,
	2,
	26,
	35,
	35,
	2359,
	2360,
	23,
	35,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	114,
	1,
	2,
	-1,
	-1,
	-1,
	-1,
	119,
	195,
	1,
	564,
	-1,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	10,
	32,
	23,
	94,
	649,
	0,
	114,
	535,
	2361,
	2362,
	2363,
	2,
	2,
	227,
	114,
	595,
	595,
	595,
	227,
	1328,
	3,
	14,
	14,
	27,
	10,
	9,
	2364,
	23,
	28,
	3,
	1194,
	30,
	30,
	862,
	30,
	1495,
	1495,
	1535,
	3,
	2365,
	21,
	317,
	1440,
	1440,
	2366,
	2366,
	271,
	316,
	2367,
	2368,
	2369,
	2370,
	2371,
	2367,
	2370,
	2367,
	2372,
	2373,
	2374,
	1373,
	2375,
	2376,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	365,
	28,
	-1,
	118,
	-1,
	206,
	-1,
	27,
	-1,
	27,
	-1,
	137,
	-1,
	137,
	23,
	3,
	-1,
	-1,
	1253,
	0,
	375,
	2377,
	0,
	160,
	215,
	2378,
	2379,
	3,
	137,
	191,
	137,
	137,
	137,
	163,
	590,
	2380,
	3,
	206,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	9,
	3,
	23,
	9,
	9,
	23,
	9,
	119,
	137,
	252,
	3,
	2381,
	135,
	2382,
	2383,
	189,
	46,
	46,
	46,
	4,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	3,
	23,
	28,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	144,
	222,
	2384,
	-1,
	-1,
	-1,
	-1,
	135,
	2,
	138,
	0,
	470,
	0,
	94,
	3,
	23,
	54,
	28,
	58,
	23,
	1427,
	107,
	-1,
	-1,
	-1,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	26,
	14,
	26,
	26,
	23,
	14,
	14,
	26,
	105,
	28,
	2,
	23,
	28,
	23,
	28,
	23,
	27,
	3,
	114,
	0,
	114,
	0,
	252,
	1,
	0,
	632,
	0,
	162,
	114,
	114,
	0,
	135,
	135,
	396,
	135,
	396,
	396,
	0,
	470,
	0,
	114,
	114,
	1,
	195,
	632,
	2385,
	119,
	134,
	-1,
	-1,
	-1,
	215,
	2386,
	2387,
	1,
	119,
	204,
	119,
	177,
	204,
	0,
	3,
	23,
	9,
	28,
	28,
	9,
	23,
	9,
	23,
	9,
	9,
	23,
	9,
	27,
	10,
	9,
	2388,
	10,
	32,
	89,
	130,
	26,
	950,
	36,
	26,
	130,
	14,
	199,
	14,
	426,
	14,
	10,
	10,
	35,
	14,
	2389,
	2390,
	2390,
	2,
	365,
	520,
	2391,
	2,
	43,
	2392,
	-1,
	0,
	246,
	48,
	48,
	2310,
	2310,
	202,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	94,
	114,
	114,
	114,
	114,
	0,
	0,
	114,
	114,
	114,
	114,
	114,
	114,
	375,
	216,
	135,
	137,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	89,
	10,
	32,
	89,
	89,
	89,
	31,
	89,
	89,
	14,
	23,
	28,
	28,
	28,
	2393,
	28,
	137,
	28,
	105,
	28,
	105,
	214,
	105,
	28,
	28,
	26,
	27,
	51,
	114,
	114,
	114,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	114,
	114,
	114,
	2394,
	0,
	58,
	14,
	28,
	58,
	2395,
	28,
	156,
	28,
	28,
	28,
	28,
	3,
	3,
	23,
	9,
	9,
	28,
	9,
	9,
	112,
	23,
	28,
	23,
	23,
	206,
	23,
	28,
	23,
	28,
	23,
	9,
	23,
	9,
	28,
	105,
	105,
	206,
	90,
	23,
	23,
	2396,
	105,
	2396,
	105,
	861,
	3,
	26,
	27,
	28,
	452,
	89,
	31,
	14,
	89,
	27,
	28,
	105,
	105,
	90,
	206,
	105,
	861,
	10,
	377,
	27,
	28,
	14,
	89,
	89,
	89,
	89,
	31,
	14,
	14,
	26,
	89,
	31,
	89,
	26,
	28,
	14,
	14,
	26,
	14,
	14,
	26,
	1360,
	1361,
	2397,
	2398,
	2399,
	2400,
	26,
	124,
	111,
	2401,
	26,
	124,
	2402,
	2403,
	26,
	124,
	206,
	1352,
	26,
	124,
	28,
	214,
	28,
	14,
	14,
	26,
	1360,
	1361,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	26,
	111,
	111,
	111,
	111,
	2402,
	0,
	0,
	23,
	111,
	23,
	2402,
	14,
	26,
	14,
	14,
	14,
	26,
	89,
	14,
	14,
	26,
	89,
	31,
	89,
	26,
	28,
	14,
	206,
	-1,
	105,
	9,
	112,
	229,
	230,
	28,
	14,
	26,
	26,
	26,
	10,
	32,
	2404,
	2405,
	2406,
	2407,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	89,
	26,
	14,
	10,
	32,
	26,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	793,
	2144,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	10,
	1360,
	1361,
	2406,
	2407,
	2408,
	2409,
	2397,
	2398,
	2410,
	2411,
	2399,
	2400,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	1360,
	1361,
	2399,
	2400,
	2397,
	2398,
	26,
	23,
	26,
	28,
	26,
	28,
	815,
	58,
	26,
	14,
	41,
	125,
	23,
	2412,
	90,
	112,
	23,
	26,
	28,
	152,
	14,
	105,
	28,
	1353,
	367,
	28,
	125,
	1353,
	2413,
	2413,
	2414,
	214,
	1354,
	9,
	1352,
	2412,
	2415,
	206,
	52,
	944,
	697,
	697,
	206,
	206,
	1352,
	1352,
	452,
	1352,
	125,
	125,
	1352,
	125,
	125,
	2416,
	1352,
	944,
	90,
	840,
	214,
	2417,
	206,
	1223,
	23,
	23,
	9,
	3,
	23,
	28,
	28,
	28,
	112,
	26,
	206,
	14,
	28,
	987,
	987,
	2418,
	2419,
	944,
	2412,
	27,
	105,
	396,
	206,
	206,
	206,
	987,
	2420,
	987,
	90,
	206,
	27,
	52,
	52,
	52,
	987,
	987,
	987,
	962,
	2412,
	987,
	2421,
	987,
	1478,
	130,
	944,
	944,
	26,
	26,
	26,
	14,
	26,
	26,
	14,
	32,
	14,
	26,
	32,
	10,
	32,
	32,
	32,
	32,
	32,
	10,
	32,
	32,
	32,
	26,
	2338,
	346,
	89,
	31,
	14,
	26,
	26,
	105,
	206,
	26,
	-1,
	227,
	2422,
	0,
	1,
	1,
	0,
	0,
	-1,
	-1,
	114,
	114,
	-1,
	49,
	49,
	4,
	3,
	23,
	28,
	3,
	23,
	28,
	89,
	89,
	89,
	157,
	28,
	28,
	28,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	27,
	28,
	26,
	14,
	89,
	793,
	14,
	14,
	2335,
	2332,
	1360,
	1362,
	23,
	10,
	14,
	10,
	14,
	14,
	23,
	89,
	10,
	10,
	26,
	14,
	968,
	2356,
	31,
	1361,
	31,
	2354,
	617,
	2353,
	26,
	331,
	1363,
	344,
	2346,
	23,
	23,
	343,
	2345,
	1041,
	2357,
	32,
	2144,
	209,
	2349,
	31,
	2355,
	617,
	2351,
	26,
	969,
	2358,
	32,
	2348,
	209,
	2350,
	26,
	617,
	2352,
	26,
	23,
	23,
	26,
	23,
	26,
	465,
	23,
	23,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	10,
	23,
	26,
	26,
	14,
	1,
	27,
	34,
	62,
	112,
	112,
	62,
	32,
	14,
	26,
	23,
	9,
	130,
	89,
	9,
	14,
	112,
	14,
	10,
	26,
	26,
	14,
	27,
	1,
	14,
	23,
	26,
	23,
	26,
	89,
	14,
	14,
	2423,
	9,
	157,
	112,
	1406,
	32,
	9,
	34,
	62,
	23,
	27,
	9,
	130,
	135,
	27,
	26,
	26,
	1406,
	0,
	23,
	27,
	27,
	112,
	62,
	32,
	34,
	62,
	26,
	23,
	9,
	130,
	89,
	9,
	28,
	112,
	23,
	9,
	112,
	62,
	89,
	89,
	26,
	32,
	34,
	62,
	130,
	10,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	23,
	26,
	112,
	1406,
	27,
	26,
	26,
	14,
	10,
	28,
	28,
	27,
	1,
	0,
	1,
	27,
	27,
	9,
	9,
	815,
	14,
	2424,
	23,
	2425,
	130,
	89,
	2425,
	14,
	26,
	14,
	28,
	14,
	14,
	28,
	32,
	23,
	89,
	23,
	2426,
	23,
	14,
	14,
	14,
	14,
	26,
	26,
	34,
	62,
	9,
	32,
	112,
	1406,
	9,
	23,
	14,
	10,
	26,
	27,
	27,
	1,
	14,
	14,
	26,
	23,
	9,
	130,
	9,
	10,
	89,
	112,
	62,
	32,
	34,
	62,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	28,
	27,
	9,
	14,
	89,
	14,
	10,
	23,
	27,
	23,
	9,
	23,
	28,
	62,
	32,
	26,
	62,
	815,
	112,
	3,
	26,
	26,
	0,
	14,
	10,
	10,
	14,
	26,
	14,
	14,
	10,
	89,
	14,
	26,
	14,
	26,
	14,
	23,
	14,
	14,
	2423,
	23,
	26,
	27,
	14,
	136,
	0,
	0,
	216,
	114,
	2427,
	160,
	2428,
	2429,
	2430,
	2431,
	94,
	244,
	244,
	244,
	114,
	114,
	2387,
	2432,
	2433,
	2434,
	2435,
	95,
	2436,
	2437,
	357,
	2438,
	2439,
	371,
	1438,
	0,
	94,
	160,
	428,
	2440,
	535,
	2441,
	0,
	14,
	14,
	14,
	1,
	28,
	105,
	0,
	1,
	27,
	129,
	89,
	10,
	10,
	14,
	14,
	26,
	-1,
	3,
	129,
	14,
	26,
	89,
	9,
	89,
	2442,
	9,
	9,
	26,
	28,
	89,
	10,
	10,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	32,
	26,
	130,
	130,
	23,
	23,
	26,
	26,
	26,
	32,
	32,
	209,
	209,
	343,
	344,
	31,
	617,
	617,
	617,
	31,
	31,
	968,
	331,
	26,
	969,
	1041,
	26,
	2316,
	130,
	26,
	26,
	89,
	2443,
	138,
	14,
	0,
	4,
	4,
	2444,
	2445,
	10,
	14,
	27,
	135,
	9,
	9,
	10,
	14,
	28,
	105,
	112,
	112,
	10,
	9,
	228,
	9,
	9,
	228,
	228,
	112,
	112,
	229,
	229,
	230,
	231,
	232,
	233,
	105,
	206,
	28,
	125,
	28,
	9,
	23,
	206,
	125,
	9,
	23,
	206,
	125,
	9,
	23,
	206,
	125,
	195,
	0,
	9,
	23,
	0,
	206,
	125,
	9,
	23,
	3,
	206,
	52,
	27,
	206,
	125,
	28,
	105,
	9,
	9,
	23,
	26,
	28,
	28,
	28,
	28,
	28,
	214,
	125,
	105,
	28,
	105,
	105,
	214,
	14,
	26,
	26,
	89,
	26,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	26,
	14,
	10,
	14,
	14,
	0,
	14,
	89,
	14,
	14,
	26,
	28,
	14,
	28,
	28,
	28,
	28,
	28,
	214,
	125,
	105,
	28,
	105,
	105,
	214,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	89,
	10,
	14,
	14,
	14,
	14,
	14,
	28,
	14,
	14,
	14,
	89,
	89,
	206,
	28,
	27,
	105,
	105,
	9,
	967,
	1186,
	1186,
	967,
	114,
	125,
	840,
	840,
	986,
	2446,
	0,
	840,
	27,
	105,
	452,
	206,
	125,
	452,
	815,
	9,
	9,
	9,
	23,
	3,
	14,
	26,
	89,
	26,
	23,
	27,
	89,
	26,
	89,
	465,
	89,
	465,
	26,
	26,
	27,
	89,
	26,
	26,
	23,
	465,
	26,
	26,
	27,
};
static const Il2CppTokenRangePair s_rgctxIndices[113] = 
{
	{ 0x02000039, { 5, 8 } },
	{ 0x0200003C, { 34, 14 } },
	{ 0x02000048, { 48, 20 } },
	{ 0x02000049, { 68, 7 } },
	{ 0x0200004A, { 75, 7 } },
	{ 0x0200005A, { 135, 1 } },
	{ 0x0200005B, { 136, 1 } },
	{ 0x0200005C, { 137, 1 } },
	{ 0x0200005D, { 138, 1 } },
	{ 0x0200005E, { 139, 1 } },
	{ 0x0200005F, { 140, 1 } },
	{ 0x02000075, { 161, 1 } },
	{ 0x02000076, { 162, 7 } },
	{ 0x0200007A, { 314, 6 } },
	{ 0x0200007B, { 320, 3 } },
	{ 0x0200007C, { 323, 2 } },
	{ 0x0200007D, { 325, 3 } },
	{ 0x0200007E, { 328, 6 } },
	{ 0x0200007F, { 334, 9 } },
	{ 0x02000080, { 343, 9 } },
	{ 0x02000081, { 352, 9 } },
	{ 0x02000082, { 361, 1 } },
	{ 0x02000083, { 362, 3 } },
	{ 0x02000084, { 365, 13 } },
	{ 0x02000085, { 378, 9 } },
	{ 0x02000086, { 387, 12 } },
	{ 0x02000087, { 399, 3 } },
	{ 0x0200008B, { 402, 9 } },
	{ 0x0200008C, { 411, 33 } },
	{ 0x0200008D, { 444, 2 } },
	{ 0x0200008E, { 446, 13 } },
	{ 0x0200008F, { 459, 7 } },
	{ 0x020000D3, { 477, 7 } },
	{ 0x0600001B, { 0, 1 } },
	{ 0x0600001C, { 1, 2 } },
	{ 0x060000A9, { 3, 2 } },
	{ 0x060001C5, { 13, 1 } },
	{ 0x060001C6, { 14, 3 } },
	{ 0x060001C7, { 17, 2 } },
	{ 0x060001CB, { 19, 3 } },
	{ 0x060001CC, { 22, 3 } },
	{ 0x060001CD, { 25, 3 } },
	{ 0x060001CE, { 28, 3 } },
	{ 0x060001D3, { 31, 3 } },
	{ 0x06000258, { 82, 2 } },
	{ 0x0600025A, { 84, 3 } },
	{ 0x0600025C, { 87, 3 } },
	{ 0x0600025E, { 90, 7 } },
	{ 0x06000260, { 97, 3 } },
	{ 0x06000262, { 100, 3 } },
	{ 0x06000298, { 103, 6 } },
	{ 0x06000299, { 109, 6 } },
	{ 0x0600029A, { 115, 5 } },
	{ 0x0600029B, { 120, 5 } },
	{ 0x0600029C, { 125, 5 } },
	{ 0x0600029D, { 130, 5 } },
	{ 0x060002C7, { 141, 2 } },
	{ 0x060002C8, { 143, 2 } },
	{ 0x06000302, { 145, 1 } },
	{ 0x06000303, { 146, 2 } },
	{ 0x06000304, { 148, 4 } },
	{ 0x0600033C, { 152, 9 } },
	{ 0x0600035B, { 169, 1 } },
	{ 0x0600035C, { 170, 4 } },
	{ 0x0600035D, { 174, 2 } },
	{ 0x0600035E, { 176, 3 } },
	{ 0x0600035F, { 179, 2 } },
	{ 0x06000360, { 181, 3 } },
	{ 0x06000361, { 184, 2 } },
	{ 0x06000362, { 186, 3 } },
	{ 0x06000363, { 189, 2 } },
	{ 0x06000364, { 191, 7 } },
	{ 0x06000365, { 198, 6 } },
	{ 0x06000366, { 204, 4 } },
	{ 0x06000367, { 208, 2 } },
	{ 0x06000368, { 210, 6 } },
	{ 0x06000369, { 216, 2 } },
	{ 0x0600036A, { 218, 2 } },
	{ 0x0600036B, { 220, 2 } },
	{ 0x0600036C, { 222, 6 } },
	{ 0x0600036D, { 228, 2 } },
	{ 0x0600036E, { 230, 2 } },
	{ 0x0600036F, { 232, 4 } },
	{ 0x06000370, { 236, 2 } },
	{ 0x06000371, { 238, 2 } },
	{ 0x06000372, { 240, 2 } },
	{ 0x06000373, { 242, 2 } },
	{ 0x06000374, { 244, 2 } },
	{ 0x06000375, { 246, 2 } },
	{ 0x06000376, { 248, 3 } },
	{ 0x06000377, { 251, 5 } },
	{ 0x06000378, { 256, 2 } },
	{ 0x06000379, { 258, 2 } },
	{ 0x0600037A, { 260, 14 } },
	{ 0x0600037B, { 274, 1 } },
	{ 0x0600037C, { 275, 5 } },
	{ 0x0600037D, { 280, 4 } },
	{ 0x0600037E, { 284, 1 } },
	{ 0x0600037F, { 285, 2 } },
	{ 0x06000380, { 287, 5 } },
	{ 0x06000381, { 292, 2 } },
	{ 0x06000382, { 294, 3 } },
	{ 0x06000383, { 297, 1 } },
	{ 0x06000384, { 298, 4 } },
	{ 0x06000385, { 302, 1 } },
	{ 0x06000386, { 303, 10 } },
	{ 0x06000387, { 313, 1 } },
	{ 0x060004BB, { 466, 2 } },
	{ 0x060005A5, { 468, 2 } },
	{ 0x060005AD, { 470, 2 } },
	{ 0x060005AE, { 472, 2 } },
	{ 0x060005B1, { 474, 3 } },
	{ 0x06000734, { 484, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[485] = 
{
	{ (Il2CppRGCTXDataType)3, 20171 },
	{ (Il2CppRGCTXDataType)1, 26618 },
	{ (Il2CppRGCTXDataType)2, 26618 },
	{ (Il2CppRGCTXDataType)1, 26667 },
	{ (Il2CppRGCTXDataType)2, 26667 },
	{ (Il2CppRGCTXDataType)2, 30250 },
	{ (Il2CppRGCTXDataType)3, 20172 },
	{ (Il2CppRGCTXDataType)2, 30251 },
	{ (Il2CppRGCTXDataType)3, 20173 },
	{ (Il2CppRGCTXDataType)2, 30252 },
	{ (Il2CppRGCTXDataType)2, 26781 },
	{ (Il2CppRGCTXDataType)2, 26779 },
	{ (Il2CppRGCTXDataType)2, 30253 },
	{ (Il2CppRGCTXDataType)2, 26788 },
	{ (Il2CppRGCTXDataType)2, 26792 },
	{ (Il2CppRGCTXDataType)2, 30254 },
	{ (Il2CppRGCTXDataType)2, 30255 },
	{ (Il2CppRGCTXDataType)3, 20174 },
	{ (Il2CppRGCTXDataType)3, 20175 },
	{ (Il2CppRGCTXDataType)2, 26795 },
	{ (Il2CppRGCTXDataType)2, 30256 },
	{ (Il2CppRGCTXDataType)3, 20176 },
	{ (Il2CppRGCTXDataType)2, 26799 },
	{ (Il2CppRGCTXDataType)3, 20177 },
	{ (Il2CppRGCTXDataType)3, 20178 },
	{ (Il2CppRGCTXDataType)2, 26801 },
	{ (Il2CppRGCTXDataType)3, 20179 },
	{ (Il2CppRGCTXDataType)3, 20180 },
	{ (Il2CppRGCTXDataType)3, 20181 },
	{ (Il2CppRGCTXDataType)3, 20182 },
	{ (Il2CppRGCTXDataType)3, 20183 },
	{ (Il2CppRGCTXDataType)3, 20184 },
	{ (Il2CppRGCTXDataType)2, 26804 },
	{ (Il2CppRGCTXDataType)2, 26804 },
	{ (Il2CppRGCTXDataType)2, 26809 },
	{ (Il2CppRGCTXDataType)2, 26810 },
	{ (Il2CppRGCTXDataType)3, 20185 },
	{ (Il2CppRGCTXDataType)2, 30044 },
	{ (Il2CppRGCTXDataType)3, 20186 },
	{ (Il2CppRGCTXDataType)2, 30257 },
	{ (Il2CppRGCTXDataType)3, 20187 },
	{ (Il2CppRGCTXDataType)3, 20188 },
	{ (Il2CppRGCTXDataType)3, 20189 },
	{ (Il2CppRGCTXDataType)3, 20190 },
	{ (Il2CppRGCTXDataType)3, 20191 },
	{ (Il2CppRGCTXDataType)2, 26811 },
	{ (Il2CppRGCTXDataType)3, 20192 },
	{ (Il2CppRGCTXDataType)1, 26810 },
	{ (Il2CppRGCTXDataType)2, 26852 },
	{ (Il2CppRGCTXDataType)2, 26853 },
	{ (Il2CppRGCTXDataType)2, 26851 },
	{ (Il2CppRGCTXDataType)3, 20193 },
	{ (Il2CppRGCTXDataType)3, 20194 },
	{ (Il2CppRGCTXDataType)2, 26856 },
	{ (Il2CppRGCTXDataType)2, 30045 },
	{ (Il2CppRGCTXDataType)3, 20195 },
	{ (Il2CppRGCTXDataType)3, 20196 },
	{ (Il2CppRGCTXDataType)3, 20197 },
	{ (Il2CppRGCTXDataType)2, 30258 },
	{ (Il2CppRGCTXDataType)3, 20198 },
	{ (Il2CppRGCTXDataType)2, 30259 },
	{ (Il2CppRGCTXDataType)3, 20199 },
	{ (Il2CppRGCTXDataType)3, 20200 },
	{ (Il2CppRGCTXDataType)2, 30046 },
	{ (Il2CppRGCTXDataType)3, 20201 },
	{ (Il2CppRGCTXDataType)2, 30260 },
	{ (Il2CppRGCTXDataType)3, 20202 },
	{ (Il2CppRGCTXDataType)2, 26857 },
	{ (Il2CppRGCTXDataType)3, 20203 },
	{ (Il2CppRGCTXDataType)3, 20204 },
	{ (Il2CppRGCTXDataType)2, 26863 },
	{ (Il2CppRGCTXDataType)3, 20205 },
	{ (Il2CppRGCTXDataType)2, 26865 },
	{ (Il2CppRGCTXDataType)3, 20206 },
	{ (Il2CppRGCTXDataType)2, 26866 },
	{ (Il2CppRGCTXDataType)2, 30263 },
	{ (Il2CppRGCTXDataType)3, 20207 },
	{ (Il2CppRGCTXDataType)2, 30263 },
	{ (Il2CppRGCTXDataType)2, 26871 },
	{ (Il2CppRGCTXDataType)2, 26872 },
	{ (Il2CppRGCTXDataType)2, 26870 },
	{ (Il2CppRGCTXDataType)3, 20208 },
	{ (Il2CppRGCTXDataType)1, 26883 },
	{ (Il2CppRGCTXDataType)2, 26883 },
	{ (Il2CppRGCTXDataType)1, 26886 },
	{ (Il2CppRGCTXDataType)1, 26885 },
	{ (Il2CppRGCTXDataType)2, 26885 },
	{ (Il2CppRGCTXDataType)1, 26888 },
	{ (Il2CppRGCTXDataType)1, 26887 },
	{ (Il2CppRGCTXDataType)2, 26887 },
	{ (Il2CppRGCTXDataType)2, 30264 },
	{ (Il2CppRGCTXDataType)3, 20209 },
	{ (Il2CppRGCTXDataType)3, 20210 },
	{ (Il2CppRGCTXDataType)2, 26889 },
	{ (Il2CppRGCTXDataType)3, 20211 },
	{ (Il2CppRGCTXDataType)1, 26890 },
	{ (Il2CppRGCTXDataType)1, 26889 },
	{ (Il2CppRGCTXDataType)1, 26892 },
	{ (Il2CppRGCTXDataType)1, 26891 },
	{ (Il2CppRGCTXDataType)2, 26891 },
	{ (Il2CppRGCTXDataType)1, 26894 },
	{ (Il2CppRGCTXDataType)1, 26893 },
	{ (Il2CppRGCTXDataType)2, 26893 },
	{ (Il2CppRGCTXDataType)2, 30265 },
	{ (Il2CppRGCTXDataType)3, 20212 },
	{ (Il2CppRGCTXDataType)3, 20213 },
	{ (Il2CppRGCTXDataType)2, 26929 },
	{ (Il2CppRGCTXDataType)3, 20214 },
	{ (Il2CppRGCTXDataType)3, 20215 },
	{ (Il2CppRGCTXDataType)2, 30266 },
	{ (Il2CppRGCTXDataType)3, 20216 },
	{ (Il2CppRGCTXDataType)3, 20217 },
	{ (Il2CppRGCTXDataType)2, 26931 },
	{ (Il2CppRGCTXDataType)3, 20218 },
	{ (Il2CppRGCTXDataType)3, 20219 },
	{ (Il2CppRGCTXDataType)2, 30267 },
	{ (Il2CppRGCTXDataType)3, 20220 },
	{ (Il2CppRGCTXDataType)3, 20221 },
	{ (Il2CppRGCTXDataType)2, 26933 },
	{ (Il2CppRGCTXDataType)3, 20222 },
	{ (Il2CppRGCTXDataType)2, 30268 },
	{ (Il2CppRGCTXDataType)3, 20223 },
	{ (Il2CppRGCTXDataType)3, 20224 },
	{ (Il2CppRGCTXDataType)2, 26935 },
	{ (Il2CppRGCTXDataType)3, 20225 },
	{ (Il2CppRGCTXDataType)2, 30269 },
	{ (Il2CppRGCTXDataType)3, 20226 },
	{ (Il2CppRGCTXDataType)3, 20227 },
	{ (Il2CppRGCTXDataType)2, 26937 },
	{ (Il2CppRGCTXDataType)3, 20228 },
	{ (Il2CppRGCTXDataType)2, 30270 },
	{ (Il2CppRGCTXDataType)3, 20229 },
	{ (Il2CppRGCTXDataType)3, 20230 },
	{ (Il2CppRGCTXDataType)2, 26939 },
	{ (Il2CppRGCTXDataType)3, 20231 },
	{ (Il2CppRGCTXDataType)2, 26948 },
	{ (Il2CppRGCTXDataType)2, 26951 },
	{ (Il2CppRGCTXDataType)2, 26954 },
	{ (Il2CppRGCTXDataType)2, 26958 },
	{ (Il2CppRGCTXDataType)2, 26962 },
	{ (Il2CppRGCTXDataType)2, 26965 },
	{ (Il2CppRGCTXDataType)3, 20232 },
	{ (Il2CppRGCTXDataType)3, 20233 },
	{ (Il2CppRGCTXDataType)3, 20234 },
	{ (Il2CppRGCTXDataType)3, 20235 },
	{ (Il2CppRGCTXDataType)3, 20236 },
	{ (Il2CppRGCTXDataType)3, 20237 },
	{ (Il2CppRGCTXDataType)3, 20238 },
	{ (Il2CppRGCTXDataType)1, 27014 },
	{ (Il2CppRGCTXDataType)2, 27013 },
	{ (Il2CppRGCTXDataType)3, 20239 },
	{ (Il2CppRGCTXDataType)3, 20240 },
	{ (Il2CppRGCTXDataType)2, 30271 },
	{ (Il2CppRGCTXDataType)3, 20241 },
	{ (Il2CppRGCTXDataType)3, 20242 },
	{ (Il2CppRGCTXDataType)2, 30272 },
	{ (Il2CppRGCTXDataType)3, 20243 },
	{ (Il2CppRGCTXDataType)3, 20244 },
	{ (Il2CppRGCTXDataType)3, 20245 },
	{ (Il2CppRGCTXDataType)3, 20246 },
	{ (Il2CppRGCTXDataType)3, 20247 },
	{ (Il2CppRGCTXDataType)3, 20248 },
	{ (Il2CppRGCTXDataType)2, 30273 },
	{ (Il2CppRGCTXDataType)3, 20249 },
	{ (Il2CppRGCTXDataType)3, 20250 },
	{ (Il2CppRGCTXDataType)3, 20251 },
	{ (Il2CppRGCTXDataType)3, 20252 },
	{ (Il2CppRGCTXDataType)3, 20253 },
	{ (Il2CppRGCTXDataType)3, 20254 },
	{ (Il2CppRGCTXDataType)2, 30274 },
	{ (Il2CppRGCTXDataType)2, 27060 },
	{ (Il2CppRGCTXDataType)2, 30275 },
	{ (Il2CppRGCTXDataType)1, 27061 },
	{ (Il2CppRGCTXDataType)3, 20255 },
	{ (Il2CppRGCTXDataType)2, 30276 },
	{ (Il2CppRGCTXDataType)3, 20256 },
	{ (Il2CppRGCTXDataType)3, 20257 },
	{ (Il2CppRGCTXDataType)3, 20258 },
	{ (Il2CppRGCTXDataType)3, 20259 },
	{ (Il2CppRGCTXDataType)2, 30277 },
	{ (Il2CppRGCTXDataType)3, 20260 },
	{ (Il2CppRGCTXDataType)3, 20261 },
	{ (Il2CppRGCTXDataType)3, 20262 },
	{ (Il2CppRGCTXDataType)3, 20263 },
	{ (Il2CppRGCTXDataType)2, 30278 },
	{ (Il2CppRGCTXDataType)3, 20264 },
	{ (Il2CppRGCTXDataType)3, 20265 },
	{ (Il2CppRGCTXDataType)3, 20266 },
	{ (Il2CppRGCTXDataType)3, 20267 },
	{ (Il2CppRGCTXDataType)2, 30279 },
	{ (Il2CppRGCTXDataType)3, 20268 },
	{ (Il2CppRGCTXDataType)2, 30280 },
	{ (Il2CppRGCTXDataType)3, 20269 },
	{ (Il2CppRGCTXDataType)3, 20270 },
	{ (Il2CppRGCTXDataType)3, 20271 },
	{ (Il2CppRGCTXDataType)2, 30281 },
	{ (Il2CppRGCTXDataType)3, 20272 },
	{ (Il2CppRGCTXDataType)3, 20273 },
	{ (Il2CppRGCTXDataType)3, 20274 },
	{ (Il2CppRGCTXDataType)2, 30282 },
	{ (Il2CppRGCTXDataType)3, 20275 },
	{ (Il2CppRGCTXDataType)2, 30283 },
	{ (Il2CppRGCTXDataType)3, 20276 },
	{ (Il2CppRGCTXDataType)3, 20277 },
	{ (Il2CppRGCTXDataType)3, 20278 },
	{ (Il2CppRGCTXDataType)3, 20279 },
	{ (Il2CppRGCTXDataType)3, 20280 },
	{ (Il2CppRGCTXDataType)3, 20281 },
	{ (Il2CppRGCTXDataType)2, 30284 },
	{ (Il2CppRGCTXDataType)3, 20282 },
	{ (Il2CppRGCTXDataType)3, 20283 },
	{ (Il2CppRGCTXDataType)2, 30285 },
	{ (Il2CppRGCTXDataType)2, 30286 },
	{ (Il2CppRGCTXDataType)3, 20284 },
	{ (Il2CppRGCTXDataType)2, 27116 },
	{ (Il2CppRGCTXDataType)2, 30287 },
	{ (Il2CppRGCTXDataType)2, 30288 },
	{ (Il2CppRGCTXDataType)3, 20285 },
	{ (Il2CppRGCTXDataType)2, 30289 },
	{ (Il2CppRGCTXDataType)3, 20286 },
	{ (Il2CppRGCTXDataType)3, 20287 },
	{ (Il2CppRGCTXDataType)3, 20288 },
	{ (Il2CppRGCTXDataType)3, 20289 },
	{ (Il2CppRGCTXDataType)2, 30290 },
	{ (Il2CppRGCTXDataType)2, 30291 },
	{ (Il2CppRGCTXDataType)3, 20290 },
	{ (Il2CppRGCTXDataType)2, 27126 },
	{ (Il2CppRGCTXDataType)2, 30292 },
	{ (Il2CppRGCTXDataType)2, 30293 },
	{ (Il2CppRGCTXDataType)3, 20291 },
	{ (Il2CppRGCTXDataType)3, 20292 },
	{ (Il2CppRGCTXDataType)3, 20293 },
	{ (Il2CppRGCTXDataType)3, 20294 },
	{ (Il2CppRGCTXDataType)2, 27134 },
	{ (Il2CppRGCTXDataType)2, 30294 },
	{ (Il2CppRGCTXDataType)3, 20295 },
	{ (Il2CppRGCTXDataType)2, 30295 },
	{ (Il2CppRGCTXDataType)3, 20296 },
	{ (Il2CppRGCTXDataType)2, 30296 },
	{ (Il2CppRGCTXDataType)3, 20297 },
	{ (Il2CppRGCTXDataType)3, 20298 },
	{ (Il2CppRGCTXDataType)3, 20299 },
	{ (Il2CppRGCTXDataType)3, 20300 },
	{ (Il2CppRGCTXDataType)2, 27144 },
	{ (Il2CppRGCTXDataType)3, 20301 },
	{ (Il2CppRGCTXDataType)3, 20302 },
	{ (Il2CppRGCTXDataType)2, 30297 },
	{ (Il2CppRGCTXDataType)3, 20303 },
	{ (Il2CppRGCTXDataType)3, 20304 },
	{ (Il2CppRGCTXDataType)2, 27152 },
	{ (Il2CppRGCTXDataType)3, 20305 },
	{ (Il2CppRGCTXDataType)2, 30298 },
	{ (Il2CppRGCTXDataType)2, 30299 },
	{ (Il2CppRGCTXDataType)2, 27155 },
	{ (Il2CppRGCTXDataType)3, 20306 },
	{ (Il2CppRGCTXDataType)3, 20307 },
	{ (Il2CppRGCTXDataType)3, 20308 },
	{ (Il2CppRGCTXDataType)3, 20309 },
	{ (Il2CppRGCTXDataType)2, 30300 },
	{ (Il2CppRGCTXDataType)3, 20310 },
	{ (Il2CppRGCTXDataType)3, 20311 },
	{ (Il2CppRGCTXDataType)3, 20312 },
	{ (Il2CppRGCTXDataType)3, 20313 },
	{ (Il2CppRGCTXDataType)2, 30301 },
	{ (Il2CppRGCTXDataType)3, 20314 },
	{ (Il2CppRGCTXDataType)2, 27162 },
	{ (Il2CppRGCTXDataType)2, 30302 },
	{ (Il2CppRGCTXDataType)3, 20315 },
	{ (Il2CppRGCTXDataType)3, 20316 },
	{ (Il2CppRGCTXDataType)2, 30303 },
	{ (Il2CppRGCTXDataType)3, 20317 },
	{ (Il2CppRGCTXDataType)3, 20318 },
	{ (Il2CppRGCTXDataType)3, 20319 },
	{ (Il2CppRGCTXDataType)3, 20320 },
	{ (Il2CppRGCTXDataType)3, 20321 },
	{ (Il2CppRGCTXDataType)2, 30304 },
	{ (Il2CppRGCTXDataType)3, 20322 },
	{ (Il2CppRGCTXDataType)2, 30305 },
	{ (Il2CppRGCTXDataType)3, 20323 },
	{ (Il2CppRGCTXDataType)3, 20324 },
	{ (Il2CppRGCTXDataType)3, 20325 },
	{ (Il2CppRGCTXDataType)3, 20326 },
	{ (Il2CppRGCTXDataType)3, 20327 },
	{ (Il2CppRGCTXDataType)3, 20328 },
	{ (Il2CppRGCTXDataType)3, 20329 },
	{ (Il2CppRGCTXDataType)3, 20330 },
	{ (Il2CppRGCTXDataType)3, 20331 },
	{ (Il2CppRGCTXDataType)3, 20332 },
	{ (Il2CppRGCTXDataType)3, 20333 },
	{ (Il2CppRGCTXDataType)2, 27197 },
	{ (Il2CppRGCTXDataType)2, 30306 },
	{ (Il2CppRGCTXDataType)3, 20334 },
	{ (Il2CppRGCTXDataType)3, 20335 },
	{ (Il2CppRGCTXDataType)2, 27200 },
	{ (Il2CppRGCTXDataType)2, 27202 },
	{ (Il2CppRGCTXDataType)2, 30307 },
	{ (Il2CppRGCTXDataType)3, 20336 },
	{ (Il2CppRGCTXDataType)3, 20337 },
	{ (Il2CppRGCTXDataType)3, 20338 },
	{ (Il2CppRGCTXDataType)3, 20339 },
	{ (Il2CppRGCTXDataType)2, 30308 },
	{ (Il2CppRGCTXDataType)3, 20340 },
	{ (Il2CppRGCTXDataType)3, 20341 },
	{ (Il2CppRGCTXDataType)3, 20342 },
	{ (Il2CppRGCTXDataType)3, 20343 },
	{ (Il2CppRGCTXDataType)3, 20344 },
	{ (Il2CppRGCTXDataType)2, 27230 },
	{ (Il2CppRGCTXDataType)3, 20345 },
	{ (Il2CppRGCTXDataType)2, 27223 },
	{ (Il2CppRGCTXDataType)2, 30309 },
	{ (Il2CppRGCTXDataType)3, 20346 },
	{ (Il2CppRGCTXDataType)3, 20347 },
	{ (Il2CppRGCTXDataType)3, 20348 },
	{ (Il2CppRGCTXDataType)2, 27231 },
	{ (Il2CppRGCTXDataType)2, 30310 },
	{ (Il2CppRGCTXDataType)3, 20349 },
	{ (Il2CppRGCTXDataType)2, 30311 },
	{ (Il2CppRGCTXDataType)3, 20350 },
	{ (Il2CppRGCTXDataType)2, 30312 },
	{ (Il2CppRGCTXDataType)3, 20351 },
	{ (Il2CppRGCTXDataType)2, 30313 },
	{ (Il2CppRGCTXDataType)3, 20352 },
	{ (Il2CppRGCTXDataType)2, 30313 },
	{ (Il2CppRGCTXDataType)2, 30314 },
	{ (Il2CppRGCTXDataType)2, 30315 },
	{ (Il2CppRGCTXDataType)3, 20353 },
	{ (Il2CppRGCTXDataType)2, 27246 },
	{ (Il2CppRGCTXDataType)3, 20354 },
	{ (Il2CppRGCTXDataType)3, 20355 },
	{ (Il2CppRGCTXDataType)2, 27254 },
	{ (Il2CppRGCTXDataType)3, 20356 },
	{ (Il2CppRGCTXDataType)2, 30316 },
	{ (Il2CppRGCTXDataType)3, 20357 },
	{ (Il2CppRGCTXDataType)3, 20358 },
	{ (Il2CppRGCTXDataType)3, 20359 },
	{ (Il2CppRGCTXDataType)2, 27270 },
	{ (Il2CppRGCTXDataType)2, 27263 },
	{ (Il2CppRGCTXDataType)3, 20360 },
	{ (Il2CppRGCTXDataType)3, 20361 },
	{ (Il2CppRGCTXDataType)2, 27262 },
	{ (Il2CppRGCTXDataType)2, 30317 },
	{ (Il2CppRGCTXDataType)3, 20362 },
	{ (Il2CppRGCTXDataType)3, 20363 },
	{ (Il2CppRGCTXDataType)3, 20364 },
	{ (Il2CppRGCTXDataType)2, 30318 },
	{ (Il2CppRGCTXDataType)2, 30319 },
	{ (Il2CppRGCTXDataType)3, 20365 },
	{ (Il2CppRGCTXDataType)3, 20366 },
	{ (Il2CppRGCTXDataType)2, 27274 },
	{ (Il2CppRGCTXDataType)2, 30320 },
	{ (Il2CppRGCTXDataType)3, 20367 },
	{ (Il2CppRGCTXDataType)3, 20368 },
	{ (Il2CppRGCTXDataType)3, 20369 },
	{ (Il2CppRGCTXDataType)2, 30321 },
	{ (Il2CppRGCTXDataType)2, 30322 },
	{ (Il2CppRGCTXDataType)3, 20370 },
	{ (Il2CppRGCTXDataType)3, 20371 },
	{ (Il2CppRGCTXDataType)2, 27287 },
	{ (Il2CppRGCTXDataType)2, 30323 },
	{ (Il2CppRGCTXDataType)3, 20372 },
	{ (Il2CppRGCTXDataType)3, 20373 },
	{ (Il2CppRGCTXDataType)3, 20374 },
	{ (Il2CppRGCTXDataType)2, 30324 },
	{ (Il2CppRGCTXDataType)3, 20375 },
	{ (Il2CppRGCTXDataType)2, 30324 },
	{ (Il2CppRGCTXDataType)3, 20376 },
	{ (Il2CppRGCTXDataType)3, 20377 },
	{ (Il2CppRGCTXDataType)2, 30325 },
	{ (Il2CppRGCTXDataType)2, 30326 },
	{ (Il2CppRGCTXDataType)3, 20378 },
	{ (Il2CppRGCTXDataType)2, 27319 },
	{ (Il2CppRGCTXDataType)2, 30327 },
	{ (Il2CppRGCTXDataType)3, 20379 },
	{ (Il2CppRGCTXDataType)3, 20380 },
	{ (Il2CppRGCTXDataType)2, 27312 },
	{ (Il2CppRGCTXDataType)2, 30328 },
	{ (Il2CppRGCTXDataType)3, 20381 },
	{ (Il2CppRGCTXDataType)3, 20382 },
	{ (Il2CppRGCTXDataType)3, 20383 },
	{ (Il2CppRGCTXDataType)3, 20384 },
	{ (Il2CppRGCTXDataType)2, 27337 },
	{ (Il2CppRGCTXDataType)2, 27332 },
	{ (Il2CppRGCTXDataType)3, 20385 },
	{ (Il2CppRGCTXDataType)2, 27331 },
	{ (Il2CppRGCTXDataType)2, 30329 },
	{ (Il2CppRGCTXDataType)3, 20386 },
	{ (Il2CppRGCTXDataType)3, 20387 },
	{ (Il2CppRGCTXDataType)3, 20388 },
	{ (Il2CppRGCTXDataType)2, 30330 },
	{ (Il2CppRGCTXDataType)3, 20389 },
	{ (Il2CppRGCTXDataType)2, 27350 },
	{ (Il2CppRGCTXDataType)2, 27342 },
	{ (Il2CppRGCTXDataType)2, 27341 },
	{ (Il2CppRGCTXDataType)3, 20390 },
	{ (Il2CppRGCTXDataType)3, 20391 },
	{ (Il2CppRGCTXDataType)3, 20392 },
	{ (Il2CppRGCTXDataType)2, 30331 },
	{ (Il2CppRGCTXDataType)3, 20393 },
	{ (Il2CppRGCTXDataType)3, 20394 },
	{ (Il2CppRGCTXDataType)2, 30332 },
	{ (Il2CppRGCTXDataType)3, 20395 },
	{ (Il2CppRGCTXDataType)2, 30332 },
	{ (Il2CppRGCTXDataType)2, 30333 },
	{ (Il2CppRGCTXDataType)3, 20396 },
	{ (Il2CppRGCTXDataType)2, 27377 },
	{ (Il2CppRGCTXDataType)3, 20397 },
	{ (Il2CppRGCTXDataType)3, 20398 },
	{ (Il2CppRGCTXDataType)3, 20399 },
	{ (Il2CppRGCTXDataType)3, 20400 },
	{ (Il2CppRGCTXDataType)2, 30334 },
	{ (Il2CppRGCTXDataType)3, 20401 },
	{ (Il2CppRGCTXDataType)3, 20402 },
	{ (Il2CppRGCTXDataType)2, 30335 },
	{ (Il2CppRGCTXDataType)3, 20403 },
	{ (Il2CppRGCTXDataType)3, 20404 },
	{ (Il2CppRGCTXDataType)2, 30336 },
	{ (Il2CppRGCTXDataType)2, 27391 },
	{ (Il2CppRGCTXDataType)3, 20405 },
	{ (Il2CppRGCTXDataType)3, 20406 },
	{ (Il2CppRGCTXDataType)2, 27392 },
	{ (Il2CppRGCTXDataType)3, 20407 },
	{ (Il2CppRGCTXDataType)3, 20408 },
	{ (Il2CppRGCTXDataType)3, 20409 },
	{ (Il2CppRGCTXDataType)2, 30337 },
	{ (Il2CppRGCTXDataType)3, 20410 },
	{ (Il2CppRGCTXDataType)3, 20411 },
	{ (Il2CppRGCTXDataType)3, 20412 },
	{ (Il2CppRGCTXDataType)3, 20413 },
	{ (Il2CppRGCTXDataType)2, 30338 },
	{ (Il2CppRGCTXDataType)3, 20414 },
	{ (Il2CppRGCTXDataType)3, 20415 },
	{ (Il2CppRGCTXDataType)3, 20416 },
	{ (Il2CppRGCTXDataType)2, 30339 },
	{ (Il2CppRGCTXDataType)3, 20417 },
	{ (Il2CppRGCTXDataType)3, 20418 },
	{ (Il2CppRGCTXDataType)2, 27386 },
	{ (Il2CppRGCTXDataType)2, 27394 },
	{ (Il2CppRGCTXDataType)3, 20419 },
	{ (Il2CppRGCTXDataType)3, 20420 },
	{ (Il2CppRGCTXDataType)3, 20421 },
	{ (Il2CppRGCTXDataType)3, 20422 },
	{ (Il2CppRGCTXDataType)3, 20423 },
	{ (Il2CppRGCTXDataType)3, 20424 },
	{ (Il2CppRGCTXDataType)3, 20425 },
	{ (Il2CppRGCTXDataType)3, 20426 },
	{ (Il2CppRGCTXDataType)2, 30340 },
	{ (Il2CppRGCTXDataType)2, 27409 },
	{ (Il2CppRGCTXDataType)3, 20427 },
	{ (Il2CppRGCTXDataType)2, 30341 },
	{ (Il2CppRGCTXDataType)3, 20428 },
	{ (Il2CppRGCTXDataType)3, 20429 },
	{ (Il2CppRGCTXDataType)3, 20430 },
	{ (Il2CppRGCTXDataType)2, 30342 },
	{ (Il2CppRGCTXDataType)3, 20431 },
	{ (Il2CppRGCTXDataType)3, 20432 },
	{ (Il2CppRGCTXDataType)3, 20433 },
	{ (Il2CppRGCTXDataType)3, 20434 },
	{ (Il2CppRGCTXDataType)2, 27407 },
	{ (Il2CppRGCTXDataType)2, 27408 },
	{ (Il2CppRGCTXDataType)2, 30343 },
	{ (Il2CppRGCTXDataType)3, 20435 },
	{ (Il2CppRGCTXDataType)3, 20436 },
	{ (Il2CppRGCTXDataType)2, 30344 },
	{ (Il2CppRGCTXDataType)3, 20437 },
	{ (Il2CppRGCTXDataType)2, 30345 },
	{ (Il2CppRGCTXDataType)3, 20438 },
	{ (Il2CppRGCTXDataType)1, 27515 },
	{ (Il2CppRGCTXDataType)2, 27515 },
	{ (Il2CppRGCTXDataType)3, 20439 },
	{ (Il2CppRGCTXDataType)2, 30346 },
	{ (Il2CppRGCTXDataType)3, 20440 },
	{ (Il2CppRGCTXDataType)2, 27575 },
	{ (Il2CppRGCTXDataType)3, 20441 },
	{ (Il2CppRGCTXDataType)2, 27576 },
	{ (Il2CppRGCTXDataType)3, 20442 },
	{ (Il2CppRGCTXDataType)3, 20443 },
	{ (Il2CppRGCTXDataType)3, 20444 },
	{ (Il2CppRGCTXDataType)2, 27639 },
	{ (Il2CppRGCTXDataType)2, 27639 },
	{ (Il2CppRGCTXDataType)2, 27636 },
	{ (Il2CppRGCTXDataType)3, 20445 },
	{ (Il2CppRGCTXDataType)3, 20446 },
	{ (Il2CppRGCTXDataType)3, 20447 },
	{ (Il2CppRGCTXDataType)3, 20448 },
	{ (Il2CppRGCTXDataType)2, 27677 },
};
extern const Il2CppCodeGenModule g_Newtonsoft_JsonCodeGenModule;
const Il2CppCodeGenModule g_Newtonsoft_JsonCodeGenModule = 
{
	"Newtonsoft.Json.dll",
	2099,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	113,
	s_rgctxIndices,
	485,
	s_rgctxValues,
	NULL,
};
