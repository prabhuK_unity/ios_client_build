﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Byn.Awrtc.ICall::add_CallEvent(Byn.Awrtc.CallEventHandler)
// 0x00000002 System.Void Byn.Awrtc.ICall::remove_CallEvent(Byn.Awrtc.CallEventHandler)
// 0x00000003 System.Void Byn.Awrtc.ICall::Configure(Byn.Awrtc.MediaConfig)
// 0x00000004 System.Void Byn.Awrtc.ICall::Listen(System.String)
// 0x00000005 System.Void Byn.Awrtc.ICall::Call(System.String)
// 0x00000006 System.Void Byn.Awrtc.ICall::Send(System.String)
// 0x00000007 System.Void Byn.Awrtc.ICall::Update()
// 0x00000008 System.Void Byn.Awrtc.ICall::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
// 0x00000009 System.Void Byn.Awrtc.ICall::set_LocalFrameEvents(System.Boolean)
// 0x0000000A System.Boolean Byn.Awrtc.ICall::HasAudioTrack(Byn.Awrtc.ConnectionId)
// 0x0000000B System.Boolean Byn.Awrtc.ICall::HasVideoTrack(Byn.Awrtc.ConnectionId)
// 0x0000000C System.Boolean Byn.Awrtc.ICall::IsMute()
// 0x0000000D System.Void Byn.Awrtc.ICall::SetMute(System.Boolean)
// 0x0000000E System.Void Byn.Awrtc.Base.AWebRtcCall::add_CallEvent(Byn.Awrtc.CallEventHandler)
extern void AWebRtcCall_add_CallEvent_m09270F23CE78B01FADF2B457CAA8E6D278ACEB6F ();
// 0x0000000F System.Void Byn.Awrtc.Base.AWebRtcCall::remove_CallEvent(Byn.Awrtc.CallEventHandler)
extern void AWebRtcCall_remove_CallEvent_mF8A454D1587D3A01AA7A516185D67C7650D572FC ();
// 0x00000010 System.Void Byn.Awrtc.Base.AWebRtcCall::set_LocalFrameEvents(System.Boolean)
extern void AWebRtcCall_set_LocalFrameEvents_m87C68176A272EC6304EE2D328FE10BFC8B64ACAE ();
// 0x00000011 System.Void Byn.Awrtc.Base.AWebRtcCall::.ctor(Byn.Awrtc.NetworkConfig)
extern void AWebRtcCall__ctor_mC4846A890AF3EC294C04F2BD4593F3721DF36A3C ();
// 0x00000012 System.Void Byn.Awrtc.Base.AWebRtcCall::Initialize(Byn.Awrtc.IMediaNetwork)
extern void AWebRtcCall_Initialize_m83E52F1519AF93FA065C52AFB177CFCE44A54847 ();
// 0x00000013 System.Void Byn.Awrtc.Base.AWebRtcCall::Configure(Byn.Awrtc.MediaConfig)
extern void AWebRtcCall_Configure_m8A0BB04F8590D2DCC55BA94B71E88ECC4526EFDB ();
// 0x00000014 System.Void Byn.Awrtc.Base.AWebRtcCall::Call(System.String)
extern void AWebRtcCall_Call_m02C9FCBC750B159F8C8DEF3EC869AEA673F45B11 ();
// 0x00000015 System.Void Byn.Awrtc.Base.AWebRtcCall::Listen(System.String)
extern void AWebRtcCall_Listen_m130E1DF8EA39AE89D2B8C21C04454CCF3A0539C9 ();
// 0x00000016 System.Void Byn.Awrtc.Base.AWebRtcCall::Send(System.String)
extern void AWebRtcCall_Send_m23E89B21AE68294EE90FFFF42EAA84A95C40B65C ();
// 0x00000017 System.Void Byn.Awrtc.Base.AWebRtcCall::Send(System.String,System.Boolean)
extern void AWebRtcCall_Send_mD6438A63F1C799D469A9822AD033768311D9EBA9 ();
// 0x00000018 System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsStringMsg(System.Byte[])
extern void AWebRtcCall_IsStringMsg_m7F45CD2616184B9739365F17F027461489F20882 ();
// 0x00000019 System.Byte[] Byn.Awrtc.Base.AWebRtcCall::PackStringMsg(System.String)
extern void AWebRtcCall_PackStringMsg_m80338D0EE5DE8E91F2D4510DC604A3652B4A5663 ();
// 0x0000001A System.String Byn.Awrtc.Base.AWebRtcCall::UnpackStringMsg(System.Byte[])
extern void AWebRtcCall_UnpackStringMsg_mF8138BBBD07AC8F11499A0CAEF32B842F658B804 ();
// 0x0000001B System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsDataMsg(System.Byte[])
extern void AWebRtcCall_IsDataMsg_m91B5AB76EFE79A24690D9B83E5FCBBCB57C1E860 ();
// 0x0000001C System.Byte[] Byn.Awrtc.Base.AWebRtcCall::UnpackDataMsg(System.Byte[])
extern void AWebRtcCall_UnpackDataMsg_m86BF873565BFBC1B6E93DDF3BDB91994DA5BAFC7 ();
// 0x0000001D System.Void Byn.Awrtc.Base.AWebRtcCall::Update()
extern void AWebRtcCall_Update_m549A46BEA47F858AB137C76F856410223F150902 ();
// 0x0000001E System.Void Byn.Awrtc.Base.AWebRtcCall::HandleMediaEvents()
extern void AWebRtcCall_HandleMediaEvents_m841524A2FE667C7D2A272C5DF04365D1B8108B6B ();
// 0x0000001F System.Void Byn.Awrtc.Base.AWebRtcCall::HandleNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void AWebRtcCall_HandleNetworkEvent_m6E1AC4BC273D0574A85039E7CEF3798E704D3ECF ();
// 0x00000020 System.Void Byn.Awrtc.Base.AWebRtcCall::PendingCall(System.String)
extern void AWebRtcCall_PendingCall_mEBF56D08C2A943272DB3DC82DEC2EFC06A628EE0 ();
// 0x00000021 System.Void Byn.Awrtc.Base.AWebRtcCall::ProcessCall(System.String)
extern void AWebRtcCall_ProcessCall_mB92C63DDC4A918A067FBC9B8AF077513BCE9D422 ();
// 0x00000022 System.Void Byn.Awrtc.Base.AWebRtcCall::PendingListen(System.String)
extern void AWebRtcCall_PendingListen_m9CDA2D2ACE32708C21229DD92B30A0F98A35EF40 ();
// 0x00000023 System.Void Byn.Awrtc.Base.AWebRtcCall::ProcessListen(System.String)
extern void AWebRtcCall_ProcessListen_m753AF5E46F70C7EC3F1E425CA13C514A77A3954B ();
// 0x00000024 System.Void Byn.Awrtc.Base.AWebRtcCall::DoPending()
extern void AWebRtcCall_DoPending_m91D500C5AFC6D6BF2B222DC22F842AA0BC5BBE50 ();
// 0x00000025 System.Void Byn.Awrtc.Base.AWebRtcCall::ClearPending()
extern void AWebRtcCall_ClearPending_m9C48C1D3A5968688756647EFBAF7A598FBF9A005 ();
// 0x00000026 System.Void Byn.Awrtc.Base.AWebRtcCall::CheckDisposed()
extern void AWebRtcCall_CheckDisposed_mD7EE6F33F9906A26C6CB83B0D103F50D9542F90B ();
// 0x00000027 System.Void Byn.Awrtc.Base.AWebRtcCall::EnsureConfiguration()
extern void AWebRtcCall_EnsureConfiguration_mAE816DED202FC4344ECF0EFE83E886024340C82C ();
// 0x00000028 System.Void Byn.Awrtc.Base.AWebRtcCall::TriggerCallEvent(Byn.Awrtc.CallEventArgs)
extern void AWebRtcCall_TriggerCallEvent_m1A63253FC3511CC77126B3ADE31C7A6F025F37BC ();
// 0x00000029 System.Void Byn.Awrtc.Base.AWebRtcCall::OnConfigurationComplete()
extern void AWebRtcCall_OnConfigurationComplete_mF6BED2C9B6EEF93AE305B540A32234F734B8016E ();
// 0x0000002A System.Void Byn.Awrtc.Base.AWebRtcCall::OnConfigurationFailed(System.String)
extern void AWebRtcCall_OnConfigurationFailed_mCF1E7B2727703A3A0232C3528F5E3C1B766F0E8D ();
// 0x0000002B System.Void Byn.Awrtc.Base.AWebRtcCall::Dispose(System.Boolean)
extern void AWebRtcCall_Dispose_m57B176D546CC11CD3C081E36DB47F0C9DDBAF853 ();
// 0x0000002C System.Void Byn.Awrtc.Base.AWebRtcCall::Dispose()
extern void AWebRtcCall_Dispose_m3A2F7F4EE8A114A06BACAF11D876254C06B22A94 ();
// 0x0000002D System.Void Byn.Awrtc.Base.AWebRtcCall::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_SetVolume_m0DC6CA614675820CC0E3CA770ABB01D6E7314E06 ();
// 0x0000002E System.Boolean Byn.Awrtc.Base.AWebRtcCall::HasAudioTrack(Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_HasAudioTrack_m6FF8B27547640444A150CBC4CD1563F8B485FEDF ();
// 0x0000002F System.Boolean Byn.Awrtc.Base.AWebRtcCall::HasVideoTrack(Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_HasVideoTrack_m612410BE7D65CADC97140DADD188D51EDC5C74A5 ();
// 0x00000030 System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsMute()
extern void AWebRtcCall_IsMute_m82735D1F5EBE8EA88E0B20B1C15BFD057E1799AC ();
// 0x00000031 System.Void Byn.Awrtc.Base.AWebRtcCall::SetMute(System.Boolean)
extern void AWebRtcCall_SetMute_mC804CF0F1CBC2F7599A84B9063541D50B0A57A15 ();
// 0x00000032 System.Void Byn.Awrtc.Base.AWebRtcCall::.cctor()
extern void AWebRtcCall__cctor_m2451B853A8F1A96A5CA5A77F1BCD706ECB94DF1D ();
// 0x00000033 System.Byte[] Byn.Awrtc.MessageDataBuffer::get_Buffer()
// 0x00000034 System.Int32 Byn.Awrtc.MessageDataBuffer::get_Offset()
// 0x00000035 System.Int32 Byn.Awrtc.MessageDataBuffer::get_ContentLength()
// 0x00000036 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionWriteRelative()
extern void ByteArrayBuffer_get_PositionWriteRelative_m5BA1493019330918106D5CB819DEAD7FCC62B987 ();
// 0x00000037 System.Void Byn.Awrtc.ByteArrayBuffer::set_PositionWriteRelative(System.Int32)
extern void ByteArrayBuffer_set_PositionWriteRelative_m934AA17DAF8473055C81A696C548D40EB8573C4D ();
// 0x00000038 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionWriteAbsolute()
extern void ByteArrayBuffer_get_PositionWriteAbsolute_m1E594C7A0C8E7051D4EB302510851064471D4D9D ();
// 0x00000039 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionReadRelative()
extern void ByteArrayBuffer_get_PositionReadRelative_m0E958B7F3E5C4BA2A813CC0B243B1441FDE7B044 ();
// 0x0000003A System.Int32 Byn.Awrtc.ByteArrayBuffer::get_Offset()
extern void ByteArrayBuffer_get_Offset_m8DD0FD9586C6A3D4B70A82EF5E3B5AF9AF0A59A3 ();
// 0x0000003B System.Byte[] Byn.Awrtc.ByteArrayBuffer::get_Buffer()
extern void ByteArrayBuffer_get_Buffer_mEA87A81D37582411E6A368A0E111DF7BF394433F ();
// 0x0000003C System.Int32 Byn.Awrtc.ByteArrayBuffer::get_ContentLength()
extern void ByteArrayBuffer_get_ContentLength_m7F0CA8BE74E0EE3AEDE7B8BBF797F8709E1307EF ();
// 0x0000003D System.Void Byn.Awrtc.ByteArrayBuffer::set_ContentLength(System.Int32)
extern void ByteArrayBuffer_set_ContentLength_m42A5853389EB22773CDC672D262EFAE690CC1762 ();
// 0x0000003E System.Void Byn.Awrtc.ByteArrayBuffer::.ctor(System.Int32)
extern void ByteArrayBuffer__ctor_mCE37F5382D4D067550B5BBB53DB62B4C98EE3B09 ();
// 0x0000003F System.Void Byn.Awrtc.ByteArrayBuffer::.ctor(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayBuffer__ctor_mE319DCE9AD7531B26B329588DB84CD61A4E8D562 ();
// 0x00000040 System.Void Byn.Awrtc.ByteArrayBuffer::Reset()
extern void ByteArrayBuffer_Reset_m375E1E454C447037210ACFEC6D1C5C769DD54448 ();
// 0x00000041 System.Void Byn.Awrtc.ByteArrayBuffer::Finalize()
extern void ByteArrayBuffer_Finalize_m330A155A6CCCBB66AE776C1A73E29870250A58FC ();
// 0x00000042 System.Void Byn.Awrtc.ByteArrayBuffer::CopyFrom(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayBuffer_CopyFrom_mEF2A576FF26FA7A4FD0A4EEB865770FC72DEDD94 ();
// 0x00000043 System.Void Byn.Awrtc.ByteArrayBuffer::.cctor()
extern void ByteArrayBuffer__cctor_m57B126CF5D1E4BAD8FE7382DD4F86F94A8616C7E ();
// 0x00000044 System.Int32 Byn.Awrtc.ByteArrayBuffer::GetPower(System.UInt32)
extern void ByteArrayBuffer_GetPower_m16CF6A82A1F8D26A5882179C21617A3960CA7F82 ();
// 0x00000045 System.UInt32 Byn.Awrtc.ByteArrayBuffer::NextPowerOfTwo(System.UInt32)
extern void ByteArrayBuffer_NextPowerOfTwo_m799FA4F6BC6E6ADE97AC651BE4BF457EF7A150DA ();
// 0x00000046 Byn.Awrtc.ByteArrayBuffer Byn.Awrtc.ByteArrayBuffer::Get(System.Int32,System.Boolean)
extern void ByteArrayBuffer_Get_m1CA04F719970DD2C4E61B152A14D1A8528641CC0 ();
// 0x00000047 System.Void Byn.Awrtc.ByteArrayBuffer::Dispose()
extern void ByteArrayBuffer_Dispose_mEEBA9EE7B80671DB5228061B31B43ED8F577CDF3 ();
// 0x00000048 System.Void Byn.Awrtc.CallEventHandler::.ctor(System.Object,System.IntPtr)
extern void CallEventHandler__ctor_mDAFA7082D690EF7E2C977F76235AAE7A9CE6AAC5 ();
// 0x00000049 System.Void Byn.Awrtc.CallEventHandler::Invoke(System.Object,Byn.Awrtc.CallEventArgs)
extern void CallEventHandler_Invoke_m4E24EBFA859C707089DF4C71DD8359DC0635B6B1 ();
// 0x0000004A System.IAsyncResult Byn.Awrtc.CallEventHandler::BeginInvoke(System.Object,Byn.Awrtc.CallEventArgs,System.AsyncCallback,System.Object)
extern void CallEventHandler_BeginInvoke_mC7E50FFAF70A8CD0AC92E8A9883BE4265EF016B7 ();
// 0x0000004B System.Void Byn.Awrtc.CallEventHandler::EndInvoke(System.IAsyncResult)
extern void CallEventHandler_EndInvoke_m8842C3AF89BBBAD0297F4300A9FBA6F0CC119481 ();
// 0x0000004C Byn.Awrtc.CallEventType Byn.Awrtc.CallEventArgs::get_Type()
extern void CallEventArgs_get_Type_m0BF7426058DBFB74B027F71703D096F557ADA077 ();
// 0x0000004D System.Void Byn.Awrtc.CallEventArgs::.ctor(Byn.Awrtc.CallEventType)
extern void CallEventArgs__ctor_m4ED27A352EC2D4A66D78EEE46EE96B30F119ACCD ();
// 0x0000004E Byn.Awrtc.ConnectionId Byn.Awrtc.CallAcceptedEventArgs::get_ConnectionId()
extern void CallAcceptedEventArgs_get_ConnectionId_mDC42510DDFF826CD4DC0AD45FF74242C24853268 ();
// 0x0000004F System.Void Byn.Awrtc.CallAcceptedEventArgs::.ctor(Byn.Awrtc.ConnectionId)
extern void CallAcceptedEventArgs__ctor_m313593CBB34D49EC5E4A3F90FB0EB0069E4C1C9A ();
// 0x00000050 Byn.Awrtc.ConnectionId Byn.Awrtc.CallEndedEventArgs::get_ConnectionId()
extern void CallEndedEventArgs_get_ConnectionId_mCA04B58382AE52B024639F14A5DB44B3DCED0B0B ();
// 0x00000051 System.Void Byn.Awrtc.CallEndedEventArgs::.ctor(Byn.Awrtc.ConnectionId)
extern void CallEndedEventArgs__ctor_m5C66FFEB0C34E59EB177FBDF82759A1F235ABE74 ();
// 0x00000052 Byn.Awrtc.ErrorInfo Byn.Awrtc.ErrorEventArgs::get_Info()
extern void ErrorEventArgs_get_Info_mE804CE356C5573E5CAC8D711190FB2C7891040A3 ();
// 0x00000053 System.Void Byn.Awrtc.ErrorEventArgs::.ctor(Byn.Awrtc.CallEventType,Byn.Awrtc.ErrorInfo)
extern void ErrorEventArgs__ctor_m8BEABC103A358B03A2FF9F9D546B443FF579F366 ();
// 0x00000054 System.String Byn.Awrtc.ErrorEventArgs::GuessError()
extern void ErrorEventArgs_GuessError_m6A6732680478E119C59FA45A2CB9129290265497 ();
// 0x00000055 System.String Byn.Awrtc.WaitForIncomingCallEventArgs::get_Address()
extern void WaitForIncomingCallEventArgs_get_Address_m15526D607B261340AD661D739058F58E71E2F170 ();
// 0x00000056 System.Void Byn.Awrtc.WaitForIncomingCallEventArgs::.ctor(System.String)
extern void WaitForIncomingCallEventArgs__ctor_m8A615252C226AD8065E43B233EA90820188AEE22 ();
// 0x00000057 System.String Byn.Awrtc.MessageEventArgs::get_Content()
extern void MessageEventArgs_get_Content_m5D299FF76DC2CD14D3C4E00748472395915BC91C ();
// 0x00000058 System.Void Byn.Awrtc.MessageEventArgs::.ctor(Byn.Awrtc.ConnectionId,System.String,System.Boolean)
extern void MessageEventArgs__ctor_m3F3C0FE07EDBD02D58DE442EA60D74FBA9FBC1B8 ();
// 0x00000059 System.Void Byn.Awrtc.DataMessageEventArgs::.ctor(Byn.Awrtc.ConnectionId,System.Byte[],System.Boolean)
extern void DataMessageEventArgs__ctor_m0696EC373C208888C4E333D1FB9B99D52224A03A ();
// 0x0000005A Byn.Awrtc.FramePixelFormat Byn.Awrtc.FrameUpdateEventArgs::get_Format()
extern void FrameUpdateEventArgs_get_Format_m5177E64E40BBD863EED1FA232F8723A9062995BA ();
// 0x0000005B Byn.Awrtc.ConnectionId Byn.Awrtc.FrameUpdateEventArgs::get_ConnectionId()
extern void FrameUpdateEventArgs_get_ConnectionId_m9C8635494A8367A71A6B9E744F699FC0D53D8FC2 ();
// 0x0000005C System.Boolean Byn.Awrtc.FrameUpdateEventArgs::get_IsRemote()
extern void FrameUpdateEventArgs_get_IsRemote_m2754AE629569A1DD3D909BDBE8BD9A620AEC69FA ();
// 0x0000005D Byn.Awrtc.IFrame Byn.Awrtc.FrameUpdateEventArgs::get_Frame()
extern void FrameUpdateEventArgs_get_Frame_m542290AC318831D475BA437E3CE91BCAEA48F50D ();
// 0x0000005E System.Void Byn.Awrtc.FrameUpdateEventArgs::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.IFrame)
extern void FrameUpdateEventArgs__ctor_mA0D47D896195E97BBE9FEB1BF9EE2CAABC8F53E4 ();
// 0x0000005F System.Void Byn.Awrtc.ConnectionId::.ctor(System.Int16)
extern void ConnectionId__ctor_m1B23DF8F185ADBFE90CF43E74F2A84983E6C5382_AdjustorThunk ();
// 0x00000060 System.Boolean Byn.Awrtc.ConnectionId::Equals(System.Object)
extern void ConnectionId_Equals_m3319070F68965CAA3729089E1A7700B71527ADB6_AdjustorThunk ();
// 0x00000061 System.Int32 Byn.Awrtc.ConnectionId::GetHashCode()
extern void ConnectionId_GetHashCode_mF80556358DA5A32ECEA07264C5F8138393804388_AdjustorThunk ();
// 0x00000062 System.Boolean Byn.Awrtc.ConnectionId::op_Equality(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_Equality_m59535C3744B394D8EC43CCE6A2C0521BBDDA6A15 ();
// 0x00000063 System.Boolean Byn.Awrtc.ConnectionId::op_Inequality(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_Inequality_m4D5E6C47FF0450F312BA489473E9FF71D339CBC0 ();
// 0x00000064 System.String Byn.Awrtc.ConnectionId::ToString()
extern void ConnectionId_ToString_m1F79176854ABAA347F6F91FA287BFC30B3BF4C27_AdjustorThunk ();
// 0x00000065 System.Void Byn.Awrtc.ConnectionId::.cctor()
extern void ConnectionId__cctor_m7FBEAF5033170D16F7B20CBC1B5465410DD4B651 ();
// 0x00000066 System.Boolean Byn.Awrtc.DefaultValues::get_AuthenticateAsClientBugWorkaround()
extern void DefaultValues_get_AuthenticateAsClientBugWorkaround_m9760747B06464A0A858D9D415F2B7F62569EA792 ();
// 0x00000067 System.Void Byn.Awrtc.ErrorInfo::.ctor(System.String)
extern void ErrorInfo__ctor_m48716C55A898B5D004BF029D7246647CD5B4B23D ();
// 0x00000068 System.String Byn.Awrtc.ErrorInfo::ToString()
extern void ErrorInfo_ToString_m9FF48FDBCBFBA7B39A1DD9DC0B8E96459F76281E ();
// 0x00000069 System.Void Byn.Awrtc.ErrorInfo::.cctor()
extern void ErrorInfo__cctor_m16E68031F48D4019AB86A46DE9F26588B1B72C75 ();
// 0x0000006A System.Boolean Byn.Awrtc.INetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
// 0x0000006B System.Void Byn.Awrtc.INetwork::Flush()
// 0x0000006C System.Boolean Byn.Awrtc.INetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
// 0x0000006D System.Void Byn.Awrtc.INetwork::Disconnect(Byn.Awrtc.ConnectionId)
// 0x0000006E System.Void Byn.Awrtc.INetwork::Shutdown()
// 0x0000006F System.Void Byn.Awrtc.INetwork::Update()
// 0x00000070 System.Void Byn.Awrtc.IBasicNetwork::StartServer(System.String)
// 0x00000071 System.Void Byn.Awrtc.IBasicNetwork::StopServer()
// 0x00000072 Byn.Awrtc.ConnectionId Byn.Awrtc.IBasicNetwork::Connect(System.String)
// 0x00000073 Byn.Awrtc.ICall Byn.Awrtc.IAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig)
// 0x00000074 Byn.Awrtc.IMediaNetwork Byn.Awrtc.IAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
// 0x00000075 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.IAwrtcFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
// 0x00000076 System.String[] Byn.Awrtc.IAwrtcFactory::GetVideoDevices()
// 0x00000077 System.Boolean Byn.Awrtc.IAwrtcFactory::CanSelectVideoDevice()
// 0x00000078 System.Collections.Generic.List`1<System.String> Byn.Awrtc.IceServer::get_Urls()
extern void IceServer_get_Urls_mD3E85B113CB05451E02FD54D7770B2A7E3909B8A ();
// 0x00000079 System.String Byn.Awrtc.IceServer::get_Username()
extern void IceServer_get_Username_m388B0012517F44C8895EBC05EF2A982D54C575FE ();
// 0x0000007A System.String Byn.Awrtc.IceServer::get_Credential()
extern void IceServer_get_Credential_mAD235B95FCC8AA0EF58E9E5C9084323EA6B29049 ();
// 0x0000007B System.Void Byn.Awrtc.IceServer::.ctor(System.String,System.String,System.String)
extern void IceServer__ctor_m9B4C7129BE2E61E4B73E186A7810035AA0ED8AF7 ();
// 0x0000007C System.String Byn.Awrtc.IceServer::ToString()
extern void IceServer_ToString_mD92D1EC60C32C0AF7E7546296C4D10DD59097DBF ();
// 0x0000007D System.Void Byn.Awrtc.IMediaNetwork::Configure(Byn.Awrtc.MediaConfig)
// 0x0000007E Byn.Awrtc.MediaConfigurationState Byn.Awrtc.IMediaNetwork::GetConfigurationState()
// 0x0000007F System.String Byn.Awrtc.IMediaNetwork::GetConfigurationError()
// 0x00000080 System.Void Byn.Awrtc.IMediaNetwork::ResetConfiguration()
// 0x00000081 Byn.Awrtc.IFrame Byn.Awrtc.IMediaNetwork::TryGetFrame(Byn.Awrtc.ConnectionId)
// 0x00000082 System.Void Byn.Awrtc.IMediaNetwork::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
// 0x00000083 System.Boolean Byn.Awrtc.IMediaNetwork::HasAudioTrack(Byn.Awrtc.ConnectionId)
// 0x00000084 System.Boolean Byn.Awrtc.IMediaNetwork::HasVideoTrack(Byn.Awrtc.ConnectionId)
// 0x00000085 System.Boolean Byn.Awrtc.IMediaNetwork::IsMute()
// 0x00000086 System.Void Byn.Awrtc.IMediaNetwork::SetMute(System.Boolean)
// 0x00000087 System.Boolean Byn.Awrtc.LocalNetwork::get_IsServer()
extern void LocalNetwork_get_IsServer_m4553266982BFDDB8D6B3A7C8E90CA0F97F7AB852 ();
// 0x00000088 System.Void Byn.Awrtc.LocalNetwork::.ctor()
extern void LocalNetwork__ctor_mEBFB0CF251BEF4406BF3F74DFF4B659EDFE231B5 ();
// 0x00000089 System.Boolean Byn.Awrtc.LocalNetwork::IsAddressInUse(System.String)
extern void LocalNetwork_IsAddressInUse_mFBB6B273F779EAB3093BF356EF57475856DE60A4 ();
// 0x0000008A System.Void Byn.Awrtc.LocalNetwork::StartServer(System.String)
extern void LocalNetwork_StartServer_m6FF3B6576EC1BA7BFD808D8BC358209FFC518420 ();
// 0x0000008B System.Void Byn.Awrtc.LocalNetwork::StopServer()
extern void LocalNetwork_StopServer_m4ED0D3C401EF3C16E1EB7B64672ACDF02A1D7C45 ();
// 0x0000008C Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::Connect(System.String)
extern void LocalNetwork_Connect_m59F1B0EE1678E54EFC67222D45DFC0F33F05AC57 ();
// 0x0000008D System.Void Byn.Awrtc.LocalNetwork::Shutdown()
extern void LocalNetwork_Shutdown_m0C1D588250811D685CEE21E92FC2B079179AF461 ();
// 0x0000008E System.Boolean Byn.Awrtc.LocalNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void LocalNetwork_SendData_m2A244BFD191B788AD8A96EBE7D285D514BB12F98 ();
// 0x0000008F System.Void Byn.Awrtc.LocalNetwork::Update()
extern void LocalNetwork_Update_m6752589296AB92E3F3D20821187D8BABA9814B26 ();
// 0x00000090 System.Boolean Byn.Awrtc.LocalNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void LocalNetwork_Dequeue_m6338D7028DEF3B984552DD7EC6686C9B794CC8AE ();
// 0x00000091 System.Void Byn.Awrtc.LocalNetwork::Flush()
extern void LocalNetwork_Flush_mBAE91457F1EF4B54B45082BC2BB5D9F9B4B317AD ();
// 0x00000092 System.Void Byn.Awrtc.LocalNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void LocalNetwork_Disconnect_m3F8609F3C16315A6962AED155F2EE0E6371477D7 ();
// 0x00000093 Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::FindConnectionId(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_FindConnectionId_m9A3BD72C12EC7D9809CDB08C37B15B043BB2014D ();
// 0x00000094 Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::NextConnectionId()
extern void LocalNetwork_NextConnectionId_m52D7F7F6AFFC8C040EEC8A25BA57E8EA556489E1 ();
// 0x00000095 System.Void Byn.Awrtc.LocalNetwork::ConnectClient(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_ConnectClient_m2DA894173389D07ABAEECDA48F14DCBE6DD5D92D ();
// 0x00000096 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ByteArrayBuffer)
extern void LocalNetwork_Enqueue_m3364B7754275AC65776D6AD3AFB86F0FB59122C6 ();
// 0x00000097 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void LocalNetwork_Enqueue_m6586F0D0E759C97E13F274594A787292C16FD85C ();
// 0x00000098 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId)
extern void LocalNetwork_Enqueue_mCDC62798381553BB3D9B5E660D18C6E328B1FB59 ();
// 0x00000099 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetworkEvent)
extern void LocalNetwork_Enqueue_m9D20B54FF730F80913DB2B65C28636DA3BCA14E1 ();
// 0x0000009A System.Void Byn.Awrtc.LocalNetwork::ReceiveData(Byn.Awrtc.LocalNetwork,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void LocalNetwork_ReceiveData_mB8112F374B300211274D8C38432A155CFEE9D31B ();
// 0x0000009B System.Void Byn.Awrtc.LocalNetwork::InternalDisconnect(Byn.Awrtc.ConnectionId)
extern void LocalNetwork_InternalDisconnect_mEF513BD39E3796C3C0744D5AE51EC9F2F8853C72 ();
// 0x0000009C System.Void Byn.Awrtc.LocalNetwork::InternalDisconnect(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_InternalDisconnect_mD5774B0D59C47658D08C882017A481640DD0A386 ();
// 0x0000009D System.Void Byn.Awrtc.LocalNetwork::CleanupWreakReferences()
extern void LocalNetwork_CleanupWreakReferences_m1337A34E6D7A17810C73FFB7A2BDDDCDC02C6AD6 ();
// 0x0000009E System.Void Byn.Awrtc.LocalNetwork::Dispose(System.Boolean)
extern void LocalNetwork_Dispose_m7BA54233357F7A76263C3659F9AB8F018265E162 ();
// 0x0000009F System.Void Byn.Awrtc.LocalNetwork::Dispose()
extern void LocalNetwork_Dispose_mF5044853F8334A60F7B9956A5A53315ECE2416D1 ();
// 0x000000A0 System.Void Byn.Awrtc.LocalNetwork::.cctor()
extern void LocalNetwork__cctor_m4D0C07B2709CDC5DAAD363D51554369A8107D377 ();
// 0x000000A1 System.Void Byn.Awrtc.LocalNetwork_WeakRef`1::.ctor(T)
// 0x000000A2 T Byn.Awrtc.LocalNetwork_WeakRef`1::Get()
// 0x000000A3 System.Boolean Byn.Awrtc.MediaConfig::get_Audio()
extern void MediaConfig_get_Audio_m9A5EA6849D1119E2C35B658ED37CA0504D445327 ();
// 0x000000A4 System.Void Byn.Awrtc.MediaConfig::set_Audio(System.Boolean)
extern void MediaConfig_set_Audio_m1D782AA0B593C78C31E6748EA46AA59112E1F8CE ();
// 0x000000A5 System.Boolean Byn.Awrtc.MediaConfig::get_Video()
extern void MediaConfig_get_Video_mC7B07DF1B36ED8B5B7B014073DE4A47FA0B13068 ();
// 0x000000A6 System.Void Byn.Awrtc.MediaConfig::set_Video(System.Boolean)
extern void MediaConfig_set_Video_m8465D09CCCE207B7E4B0D416364EEB8EA354AFE0 ();
// 0x000000A7 System.String Byn.Awrtc.MediaConfig::get_VideoDeviceName()
extern void MediaConfig_get_VideoDeviceName_mDC5973B52BF33E6B4FFA79E0214ABE0B01AFE108 ();
// 0x000000A8 System.Void Byn.Awrtc.MediaConfig::set_VideoDeviceName(System.String)
extern void MediaConfig_set_VideoDeviceName_mD78E973CE4A45C0CA249C5FD1ED456CBC83EF2D0 ();
// 0x000000A9 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinWidth()
extern void MediaConfig_get_MinWidth_m3206840C824A7767C4F8DB4EEC60EF7408201EDB ();
// 0x000000AA System.Void Byn.Awrtc.MediaConfig::set_MinWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MinWidth_mF3F552A1F743F7BE65082AF128B1A3EE54D46B5D ();
// 0x000000AB System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinHeight()
extern void MediaConfig_get_MinHeight_mA08FE71B377AF08C0561EBC75AAA8F1F6BEABD22 ();
// 0x000000AC System.Void Byn.Awrtc.MediaConfig::set_MinHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MinHeight_mE35C7CD18C15594CAE8178371F20FF72EE80721D ();
// 0x000000AD System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxWidth()
extern void MediaConfig_get_MaxWidth_m88A44DD29260CC3594F7AF0C9B000883F6332A29 ();
// 0x000000AE System.Void Byn.Awrtc.MediaConfig::set_MaxWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MaxWidth_m40ECFFAB3600027BF70B7FD100AA9F94735A1FC8 ();
// 0x000000AF System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxHeight()
extern void MediaConfig_get_MaxHeight_m7F772654AC31AA4CA86305634E8D0BA0695FC253 ();
// 0x000000B0 System.Void Byn.Awrtc.MediaConfig::set_MaxHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MaxHeight_m9528F762FBCA544FE81FEED89116947ACDAE9477 ();
// 0x000000B1 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealWidth()
extern void MediaConfig_get_IdealWidth_mCAF4D2637DB2C207E89E9E4B1B94D21C2FC5D9FE ();
// 0x000000B2 System.Void Byn.Awrtc.MediaConfig::set_IdealWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealWidth_m4660A65C8E78F0FED4ED3023CBE2D2CB2886A615 ();
// 0x000000B3 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealHeight()
extern void MediaConfig_get_IdealHeight_mC4FA1D5E861B781CE7BE0DF4AC6FF4B2EB0B01C1 ();
// 0x000000B4 System.Void Byn.Awrtc.MediaConfig::set_IdealHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealHeight_m88433F39BBDF1584EB240B5225EE19AEF0B1DEF1 ();
// 0x000000B5 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealFrameRate()
extern void MediaConfig_get_IdealFrameRate_m21DE0AEACAD550A06342C353FEC64140872BA2E1 ();
// 0x000000B6 System.Void Byn.Awrtc.MediaConfig::set_IdealFrameRate(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealFrameRate_m0AA6DA5B871C5DECD098CCCA084B1AFDACA709B2 ();
// 0x000000B7 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinFrameRate()
extern void MediaConfig_get_MinFrameRate_m70A65C63DDEA9BF11B803D23AB9C489CC6753FDD ();
// 0x000000B8 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxFrameRate()
extern void MediaConfig_get_MaxFrameRate_m8E4D79608AD51FB8318A829386D72F77638E0E32 ();
// 0x000000B9 Byn.Awrtc.FramePixelFormat Byn.Awrtc.MediaConfig::get_Format()
extern void MediaConfig_get_Format_mC7D69871CE9963DAFC16EA2C9D5F25488DDC2492 ();
// 0x000000BA System.Void Byn.Awrtc.MediaConfig::set_Format(Byn.Awrtc.FramePixelFormat)
extern void MediaConfig_set_Format_m323534BEA6DFD56ECE96A4C0E8409E0E1389D905 ();
// 0x000000BB System.Void Byn.Awrtc.MediaConfig::.ctor()
extern void MediaConfig__ctor_m9F57DF2DA5B07EAEA3FD116B734412547CF95C50 ();
// 0x000000BC System.Void Byn.Awrtc.MediaConfig::.ctor(Byn.Awrtc.MediaConfig)
extern void MediaConfig__ctor_m8DD3603B96D321F06BDC020E2341355B83584967 ();
// 0x000000BD Byn.Awrtc.MediaConfig Byn.Awrtc.MediaConfig::DeepClone()
extern void MediaConfig_DeepClone_m6700A00DAD34DACF1BA7266DD35A793AE6DCAC9F ();
// 0x000000BE System.String Byn.Awrtc.MediaConfig::ToString()
extern void MediaConfig_ToString_m63DACCF2CFA322BA1BE5597B28E7FD5B5D90CFA9 ();
// 0x000000BF System.String Byn.Awrtc.MessageDataBufferExt::AsStringUnicode(Byn.Awrtc.MessageDataBuffer)
extern void MessageDataBufferExt_AsStringUnicode_m2B5AC83632488AE41F7BA5C2272A089F61B8FCED ();
// 0x000000C0 System.Byte[] Byn.Awrtc.MessageDataBufferExt::Copy(Byn.Awrtc.MessageDataBuffer)
extern void MessageDataBufferExt_Copy_mA35B174D17F81FB2AB25399BDD5325CA8326E210 ();
// 0x000000C1 System.Collections.Generic.List`1<Byn.Awrtc.IceServer> Byn.Awrtc.NetworkConfig::get_IceServers()
extern void NetworkConfig_get_IceServers_m5E6A3C383226E2FCDB32569895032DA5F78937C2 ();
// 0x000000C2 System.String Byn.Awrtc.NetworkConfig::get_SignalingUrl()
extern void NetworkConfig_get_SignalingUrl_m23E0B3DF7D3A55FDCA43AB1FDC6CD17B3D6692E4 ();
// 0x000000C3 System.Void Byn.Awrtc.NetworkConfig::set_SignalingUrl(System.String)
extern void NetworkConfig_set_SignalingUrl_m8BE7C4E830F00645F4EBBCFAB689A4E8F15C8194 ();
// 0x000000C4 System.Boolean Byn.Awrtc.NetworkConfig::get_AllowRenegotiation()
extern void NetworkConfig_get_AllowRenegotiation_m053139CFE0705A02E35D62A572F3ED7A5807E94E ();
// 0x000000C5 System.Boolean Byn.Awrtc.NetworkConfig::get_IsConference()
extern void NetworkConfig_get_IsConference_mFACBDD257AE18F00F8BED9B4325AF4956FA857D4 ();
// 0x000000C6 System.Void Byn.Awrtc.NetworkConfig::set_IsConference(System.Boolean)
extern void NetworkConfig_set_IsConference_m4CBB6787E50567622F7584668B61BD237A318EE7 ();
// 0x000000C7 System.String Byn.Awrtc.NetworkConfig::ToString()
extern void NetworkConfig_ToString_m6C2BDF22F5AFE8B70F6444BB3F20E7167DC2D305 ();
// 0x000000C8 System.Void Byn.Awrtc.NetworkConfig::.ctor()
extern void NetworkConfig__ctor_m602D7088B4491C0C9438A2316195FAA7F1C0E5F4 ();
// 0x000000C9 Byn.Awrtc.NetEventType Byn.Awrtc.NetworkEvent::get_Type()
extern void NetworkEvent_get_Type_m90AD2D28598E4CB47CC71E363D104C8AC7A705C5_AdjustorThunk ();
// 0x000000CA Byn.Awrtc.ConnectionId Byn.Awrtc.NetworkEvent::get_ConnectionId()
extern void NetworkEvent_get_ConnectionId_m63ED1FC3B26ED68918F1395BA9D0EAF1306F2524_AdjustorThunk ();
// 0x000000CB Byn.Awrtc.MessageDataBuffer Byn.Awrtc.NetworkEvent::get_MessageData()
extern void NetworkEvent_get_MessageData_m4E9F1B0D89F50E350EF0262187C7DE74ADCEED26_AdjustorThunk ();
// 0x000000CC System.String Byn.Awrtc.NetworkEvent::get_Info()
extern void NetworkEvent_get_Info_mA98458FF9FA0B252D18705DD6B1A3B17D9503E16_AdjustorThunk ();
// 0x000000CD Byn.Awrtc.ErrorInfo Byn.Awrtc.NetworkEvent::get_ErrorInfo()
extern void NetworkEvent_get_ErrorInfo_m8AEF4E79F149ACDE34B56754A88ADA709AAD8D95_AdjustorThunk ();
// 0x000000CE System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId)
extern void NetworkEvent__ctor_m331245C2DC884EF629C1B1103876AD951A650585_AdjustorThunk ();
// 0x000000CF System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.MessageDataBuffer)
extern void NetworkEvent__ctor_mF53983C4264CD0231654304A49965EC5B9D94C9E_AdjustorThunk ();
// 0x000000D0 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,System.String)
extern void NetworkEvent__ctor_mA7EF6BADF8188FC05D529262AC7801F91609C857_AdjustorThunk ();
// 0x000000D1 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void NetworkEvent__ctor_mB80AF54D4891D78B87014B9855C68EE206381C89_AdjustorThunk ();
// 0x000000D2 System.String Byn.Awrtc.NetworkEvent::ToString()
extern void NetworkEvent_ToString_m4B700DB0C7D27B2855D19F7ABF80FB5112ED2105_AdjustorThunk ();
// 0x000000D3 System.Boolean Byn.Awrtc.NetworkEvent::IsMetaEvent(System.Byte[])
extern void NetworkEvent_IsMetaEvent_m6BEFDA1C4672772D4B0256672EC9F15661FCBFDB ();
// 0x000000D4 Byn.Awrtc.NetworkEvent Byn.Awrtc.NetworkEvent::FromByteArray(System.Byte[])
extern void NetworkEvent_FromByteArray_m31466305C7DB5DFC36469055EB90ECD536E165FC ();
// 0x000000D5 System.Byte[] Byn.Awrtc.NetworkEvent::ToByteArray(Byn.Awrtc.NetworkEvent)
extern void NetworkEvent_ToByteArray_m2C19FC5A5C98D7795FACBFC34CA65C40EDFB6785 ();
// 0x000000D6 System.Void Byn.Awrtc.NetworkEvent::AttachError(Byn.Awrtc.ErrorInfo)
extern void NetworkEvent_AttachError_m04E5757641BF4A66B8CDD83705378B4147F3EC15_AdjustorThunk ();
// 0x000000D7 System.Byte[] Byn.Awrtc.RawFrame::get_Buffer()
// 0x000000D8 System.Int32 Byn.Awrtc.RawFrame::get_Height()
// 0x000000D9 System.Int32 Byn.Awrtc.RawFrame::get_Width()
// 0x000000DA System.Int32 Byn.Awrtc.RawFrame::get_Rotation()
// 0x000000DB Byn.Awrtc.FramePixelFormat Byn.Awrtc.RawFrame::get_Format()
// 0x000000DC System.IntPtr Byn.Awrtc.IDirectMemoryFrame::GetIntPtr()
// 0x000000DD System.Int32 Byn.Awrtc.IDirectMemoryFrame::GetSize()
// 0x000000DE System.Byte[] Byn.Awrtc.BufferedFrame::get_Buffer()
extern void BufferedFrame_get_Buffer_mAE0ABEE3EEB522AA01F1F340AF5BA298C1A8FB6C ();
// 0x000000DF System.Int32 Byn.Awrtc.BufferedFrame::get_Width()
extern void BufferedFrame_get_Width_m72C73F4EA72CAEB75935EE8AC5D2DABED85CF715 ();
// 0x000000E0 System.Int32 Byn.Awrtc.BufferedFrame::get_Height()
extern void BufferedFrame_get_Height_m998CD32662B7B6C6E843E19464D1F47CDE9B8590 ();
// 0x000000E1 System.Int32 Byn.Awrtc.BufferedFrame::get_Rotation()
extern void BufferedFrame_get_Rotation_m7C3C0038C644484BB517B4C63A441AEF18C2C947 ();
// 0x000000E2 System.Void Byn.Awrtc.BufferedFrame::set_Rotation(System.Int32)
extern void BufferedFrame_set_Rotation_m0E114C8C8ABC472DF03C47233D39F4978D5C7E50 ();
// 0x000000E3 Byn.Awrtc.FramePixelFormat Byn.Awrtc.BufferedFrame::get_Format()
extern void BufferedFrame_get_Format_m816E89BAEDCF11F2EC5C8A5BA850DA4FD2B7C2D9 ();
// 0x000000E4 System.Void Byn.Awrtc.BufferedFrame::.ctor(System.Byte[],System.Int32,System.Int32,Byn.Awrtc.FramePixelFormat,System.Int32,System.Boolean)
extern void BufferedFrame__ctor_m887D4B7F3371309EBFCAF5CD7FFF85459F223071 ();
// 0x000000E5 System.Void Byn.Awrtc.BufferedFrame::Dispose()
extern void BufferedFrame_Dispose_m60511244D7ABFC342044E26E69CA6C9E239075C6 ();
// 0x000000E6 System.Void Byn.Awrtc.SLog::SetLogger(System.Action`2<System.Object,System.String[]>)
extern void SLog_SetLogger_m5D3E7F714E0AC197E10DB6FCDB06D1209D463F91 ();
// 0x000000E7 System.Void Byn.Awrtc.SLog::LogException(System.Object,System.String[])
extern void SLog_LogException_m36D2B19FEACBC5A8799BFC23E8B2DB1A183E88B0 ();
// 0x000000E8 System.Void Byn.Awrtc.SLog::LE(System.Object,System.String[])
extern void SLog_LE_m4B26380D8909353763D54ED1E2E1217292226234 ();
// 0x000000E9 System.Void Byn.Awrtc.SLog::LW(System.Object,System.String[])
extern void SLog_LW_m4C1FD9091E37D07F12DFBDCE665962B60B7F2DE5 ();
// 0x000000EA System.Void Byn.Awrtc.SLog::L(System.Object,System.String[])
extern void SLog_L_m2B84D1BE41D8BA16E73F6E952C5C17D7D2B686A1 ();
// 0x000000EB System.String[] Byn.Awrtc.SLog::MergeTags(System.String[],System.String[])
extern void SLog_MergeTags_mFF2513B04A32F652F6FDA529489D942C805803AA ();
// 0x000000EC System.Void Byn.Awrtc.SLog::LogArray(System.Object,System.String[])
extern void SLog_LogArray_mAD74446C19CFC07B879694644E4C675F3F30508E ();
// 0x000000ED System.Void Byn.Awrtc.SLog::.cctor()
extern void SLog__cctor_mC00C1300F50AA43CB5BFCFABC5C6FC5BD20DADC6 ();
static Il2CppMethodPointer s_methodPointers[237] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AWebRtcCall_add_CallEvent_m09270F23CE78B01FADF2B457CAA8E6D278ACEB6F,
	AWebRtcCall_remove_CallEvent_mF8A454D1587D3A01AA7A516185D67C7650D572FC,
	AWebRtcCall_set_LocalFrameEvents_m87C68176A272EC6304EE2D328FE10BFC8B64ACAE,
	AWebRtcCall__ctor_mC4846A890AF3EC294C04F2BD4593F3721DF36A3C,
	AWebRtcCall_Initialize_m83E52F1519AF93FA065C52AFB177CFCE44A54847,
	AWebRtcCall_Configure_m8A0BB04F8590D2DCC55BA94B71E88ECC4526EFDB,
	AWebRtcCall_Call_m02C9FCBC750B159F8C8DEF3EC869AEA673F45B11,
	AWebRtcCall_Listen_m130E1DF8EA39AE89D2B8C21C04454CCF3A0539C9,
	AWebRtcCall_Send_m23E89B21AE68294EE90FFFF42EAA84A95C40B65C,
	AWebRtcCall_Send_mD6438A63F1C799D469A9822AD033768311D9EBA9,
	AWebRtcCall_IsStringMsg_m7F45CD2616184B9739365F17F027461489F20882,
	AWebRtcCall_PackStringMsg_m80338D0EE5DE8E91F2D4510DC604A3652B4A5663,
	AWebRtcCall_UnpackStringMsg_mF8138BBBD07AC8F11499A0CAEF32B842F658B804,
	AWebRtcCall_IsDataMsg_m91B5AB76EFE79A24690D9B83E5FCBBCB57C1E860,
	AWebRtcCall_UnpackDataMsg_m86BF873565BFBC1B6E93DDF3BDB91994DA5BAFC7,
	AWebRtcCall_Update_m549A46BEA47F858AB137C76F856410223F150902,
	AWebRtcCall_HandleMediaEvents_m841524A2FE667C7D2A272C5DF04365D1B8108B6B,
	AWebRtcCall_HandleNetworkEvent_m6E1AC4BC273D0574A85039E7CEF3798E704D3ECF,
	AWebRtcCall_PendingCall_mEBF56D08C2A943272DB3DC82DEC2EFC06A628EE0,
	AWebRtcCall_ProcessCall_mB92C63DDC4A918A067FBC9B8AF077513BCE9D422,
	AWebRtcCall_PendingListen_m9CDA2D2ACE32708C21229DD92B30A0F98A35EF40,
	AWebRtcCall_ProcessListen_m753AF5E46F70C7EC3F1E425CA13C514A77A3954B,
	AWebRtcCall_DoPending_m91D500C5AFC6D6BF2B222DC22F842AA0BC5BBE50,
	AWebRtcCall_ClearPending_m9C48C1D3A5968688756647EFBAF7A598FBF9A005,
	AWebRtcCall_CheckDisposed_mD7EE6F33F9906A26C6CB83B0D103F50D9542F90B,
	AWebRtcCall_EnsureConfiguration_mAE816DED202FC4344ECF0EFE83E886024340C82C,
	AWebRtcCall_TriggerCallEvent_m1A63253FC3511CC77126B3ADE31C7A6F025F37BC,
	AWebRtcCall_OnConfigurationComplete_mF6BED2C9B6EEF93AE305B540A32234F734B8016E,
	AWebRtcCall_OnConfigurationFailed_mCF1E7B2727703A3A0232C3528F5E3C1B766F0E8D,
	AWebRtcCall_Dispose_m57B176D546CC11CD3C081E36DB47F0C9DDBAF853,
	AWebRtcCall_Dispose_m3A2F7F4EE8A114A06BACAF11D876254C06B22A94,
	AWebRtcCall_SetVolume_m0DC6CA614675820CC0E3CA770ABB01D6E7314E06,
	AWebRtcCall_HasAudioTrack_m6FF8B27547640444A150CBC4CD1563F8B485FEDF,
	AWebRtcCall_HasVideoTrack_m612410BE7D65CADC97140DADD188D51EDC5C74A5,
	AWebRtcCall_IsMute_m82735D1F5EBE8EA88E0B20B1C15BFD057E1799AC,
	AWebRtcCall_SetMute_mC804CF0F1CBC2F7599A84B9063541D50B0A57A15,
	AWebRtcCall__cctor_m2451B853A8F1A96A5CA5A77F1BCD706ECB94DF1D,
	NULL,
	NULL,
	NULL,
	ByteArrayBuffer_get_PositionWriteRelative_m5BA1493019330918106D5CB819DEAD7FCC62B987,
	ByteArrayBuffer_set_PositionWriteRelative_m934AA17DAF8473055C81A696C548D40EB8573C4D,
	ByteArrayBuffer_get_PositionWriteAbsolute_m1E594C7A0C8E7051D4EB302510851064471D4D9D,
	ByteArrayBuffer_get_PositionReadRelative_m0E958B7F3E5C4BA2A813CC0B243B1441FDE7B044,
	ByteArrayBuffer_get_Offset_m8DD0FD9586C6A3D4B70A82EF5E3B5AF9AF0A59A3,
	ByteArrayBuffer_get_Buffer_mEA87A81D37582411E6A368A0E111DF7BF394433F,
	ByteArrayBuffer_get_ContentLength_m7F0CA8BE74E0EE3AEDE7B8BBF797F8709E1307EF,
	ByteArrayBuffer_set_ContentLength_m42A5853389EB22773CDC672D262EFAE690CC1762,
	ByteArrayBuffer__ctor_mCE37F5382D4D067550B5BBB53DB62B4C98EE3B09,
	ByteArrayBuffer__ctor_mE319DCE9AD7531B26B329588DB84CD61A4E8D562,
	ByteArrayBuffer_Reset_m375E1E454C447037210ACFEC6D1C5C769DD54448,
	ByteArrayBuffer_Finalize_m330A155A6CCCBB66AE776C1A73E29870250A58FC,
	ByteArrayBuffer_CopyFrom_mEF2A576FF26FA7A4FD0A4EEB865770FC72DEDD94,
	ByteArrayBuffer__cctor_m57B126CF5D1E4BAD8FE7382DD4F86F94A8616C7E,
	ByteArrayBuffer_GetPower_m16CF6A82A1F8D26A5882179C21617A3960CA7F82,
	ByteArrayBuffer_NextPowerOfTwo_m799FA4F6BC6E6ADE97AC651BE4BF457EF7A150DA,
	ByteArrayBuffer_Get_m1CA04F719970DD2C4E61B152A14D1A8528641CC0,
	ByteArrayBuffer_Dispose_mEEBA9EE7B80671DB5228061B31B43ED8F577CDF3,
	CallEventHandler__ctor_mDAFA7082D690EF7E2C977F76235AAE7A9CE6AAC5,
	CallEventHandler_Invoke_m4E24EBFA859C707089DF4C71DD8359DC0635B6B1,
	CallEventHandler_BeginInvoke_mC7E50FFAF70A8CD0AC92E8A9883BE4265EF016B7,
	CallEventHandler_EndInvoke_m8842C3AF89BBBAD0297F4300A9FBA6F0CC119481,
	CallEventArgs_get_Type_m0BF7426058DBFB74B027F71703D096F557ADA077,
	CallEventArgs__ctor_m4ED27A352EC2D4A66D78EEE46EE96B30F119ACCD,
	CallAcceptedEventArgs_get_ConnectionId_mDC42510DDFF826CD4DC0AD45FF74242C24853268,
	CallAcceptedEventArgs__ctor_m313593CBB34D49EC5E4A3F90FB0EB0069E4C1C9A,
	CallEndedEventArgs_get_ConnectionId_mCA04B58382AE52B024639F14A5DB44B3DCED0B0B,
	CallEndedEventArgs__ctor_m5C66FFEB0C34E59EB177FBDF82759A1F235ABE74,
	ErrorEventArgs_get_Info_mE804CE356C5573E5CAC8D711190FB2C7891040A3,
	ErrorEventArgs__ctor_m8BEABC103A358B03A2FF9F9D546B443FF579F366,
	ErrorEventArgs_GuessError_m6A6732680478E119C59FA45A2CB9129290265497,
	WaitForIncomingCallEventArgs_get_Address_m15526D607B261340AD661D739058F58E71E2F170,
	WaitForIncomingCallEventArgs__ctor_m8A615252C226AD8065E43B233EA90820188AEE22,
	MessageEventArgs_get_Content_m5D299FF76DC2CD14D3C4E00748472395915BC91C,
	MessageEventArgs__ctor_m3F3C0FE07EDBD02D58DE442EA60D74FBA9FBC1B8,
	DataMessageEventArgs__ctor_m0696EC373C208888C4E333D1FB9B99D52224A03A,
	FrameUpdateEventArgs_get_Format_m5177E64E40BBD863EED1FA232F8723A9062995BA,
	FrameUpdateEventArgs_get_ConnectionId_m9C8635494A8367A71A6B9E744F699FC0D53D8FC2,
	FrameUpdateEventArgs_get_IsRemote_m2754AE629569A1DD3D909BDBE8BD9A620AEC69FA,
	FrameUpdateEventArgs_get_Frame_m542290AC318831D475BA437E3CE91BCAEA48F50D,
	FrameUpdateEventArgs__ctor_mA0D47D896195E97BBE9FEB1BF9EE2CAABC8F53E4,
	ConnectionId__ctor_m1B23DF8F185ADBFE90CF43E74F2A84983E6C5382_AdjustorThunk,
	ConnectionId_Equals_m3319070F68965CAA3729089E1A7700B71527ADB6_AdjustorThunk,
	ConnectionId_GetHashCode_mF80556358DA5A32ECEA07264C5F8138393804388_AdjustorThunk,
	ConnectionId_op_Equality_m59535C3744B394D8EC43CCE6A2C0521BBDDA6A15,
	ConnectionId_op_Inequality_m4D5E6C47FF0450F312BA489473E9FF71D339CBC0,
	ConnectionId_ToString_m1F79176854ABAA347F6F91FA287BFC30B3BF4C27_AdjustorThunk,
	ConnectionId__cctor_m7FBEAF5033170D16F7B20CBC1B5465410DD4B651,
	DefaultValues_get_AuthenticateAsClientBugWorkaround_m9760747B06464A0A858D9D415F2B7F62569EA792,
	ErrorInfo__ctor_m48716C55A898B5D004BF029D7246647CD5B4B23D,
	ErrorInfo_ToString_m9FF48FDBCBFBA7B39A1DD9DC0B8E96459F76281E,
	ErrorInfo__cctor_m16E68031F48D4019AB86A46DE9F26588B1B72C75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IceServer_get_Urls_mD3E85B113CB05451E02FD54D7770B2A7E3909B8A,
	IceServer_get_Username_m388B0012517F44C8895EBC05EF2A982D54C575FE,
	IceServer_get_Credential_mAD235B95FCC8AA0EF58E9E5C9084323EA6B29049,
	IceServer__ctor_m9B4C7129BE2E61E4B73E186A7810035AA0ED8AF7,
	IceServer_ToString_mD92D1EC60C32C0AF7E7546296C4D10DD59097DBF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalNetwork_get_IsServer_m4553266982BFDDB8D6B3A7C8E90CA0F97F7AB852,
	LocalNetwork__ctor_mEBFB0CF251BEF4406BF3F74DFF4B659EDFE231B5,
	LocalNetwork_IsAddressInUse_mFBB6B273F779EAB3093BF356EF57475856DE60A4,
	LocalNetwork_StartServer_m6FF3B6576EC1BA7BFD808D8BC358209FFC518420,
	LocalNetwork_StopServer_m4ED0D3C401EF3C16E1EB7B64672ACDF02A1D7C45,
	LocalNetwork_Connect_m59F1B0EE1678E54EFC67222D45DFC0F33F05AC57,
	LocalNetwork_Shutdown_m0C1D588250811D685CEE21E92FC2B079179AF461,
	LocalNetwork_SendData_m2A244BFD191B788AD8A96EBE7D285D514BB12F98,
	LocalNetwork_Update_m6752589296AB92E3F3D20821187D8BABA9814B26,
	LocalNetwork_Dequeue_m6338D7028DEF3B984552DD7EC6686C9B794CC8AE,
	LocalNetwork_Flush_mBAE91457F1EF4B54B45082BC2BB5D9F9B4B317AD,
	LocalNetwork_Disconnect_m3F8609F3C16315A6962AED155F2EE0E6371477D7,
	LocalNetwork_FindConnectionId_m9A3BD72C12EC7D9809CDB08C37B15B043BB2014D,
	LocalNetwork_NextConnectionId_m52D7F7F6AFFC8C040EEC8A25BA57E8EA556489E1,
	LocalNetwork_ConnectClient_m2DA894173389D07ABAEECDA48F14DCBE6DD5D92D,
	LocalNetwork_Enqueue_m3364B7754275AC65776D6AD3AFB86F0FB59122C6,
	LocalNetwork_Enqueue_m6586F0D0E759C97E13F274594A787292C16FD85C,
	LocalNetwork_Enqueue_mCDC62798381553BB3D9B5E660D18C6E328B1FB59,
	LocalNetwork_Enqueue_m9D20B54FF730F80913DB2B65C28636DA3BCA14E1,
	LocalNetwork_ReceiveData_mB8112F374B300211274D8C38432A155CFEE9D31B,
	LocalNetwork_InternalDisconnect_mEF513BD39E3796C3C0744D5AE51EC9F2F8853C72,
	LocalNetwork_InternalDisconnect_mD5774B0D59C47658D08C882017A481640DD0A386,
	LocalNetwork_CleanupWreakReferences_m1337A34E6D7A17810C73FFB7A2BDDDCDC02C6AD6,
	LocalNetwork_Dispose_m7BA54233357F7A76263C3659F9AB8F018265E162,
	LocalNetwork_Dispose_mF5044853F8334A60F7B9956A5A53315ECE2416D1,
	LocalNetwork__cctor_m4D0C07B2709CDC5DAAD363D51554369A8107D377,
	NULL,
	NULL,
	MediaConfig_get_Audio_m9A5EA6849D1119E2C35B658ED37CA0504D445327,
	MediaConfig_set_Audio_m1D782AA0B593C78C31E6748EA46AA59112E1F8CE,
	MediaConfig_get_Video_mC7B07DF1B36ED8B5B7B014073DE4A47FA0B13068,
	MediaConfig_set_Video_m8465D09CCCE207B7E4B0D416364EEB8EA354AFE0,
	MediaConfig_get_VideoDeviceName_mDC5973B52BF33E6B4FFA79E0214ABE0B01AFE108,
	MediaConfig_set_VideoDeviceName_mD78E973CE4A45C0CA249C5FD1ED456CBC83EF2D0,
	MediaConfig_get_MinWidth_m3206840C824A7767C4F8DB4EEC60EF7408201EDB,
	MediaConfig_set_MinWidth_mF3F552A1F743F7BE65082AF128B1A3EE54D46B5D,
	MediaConfig_get_MinHeight_mA08FE71B377AF08C0561EBC75AAA8F1F6BEABD22,
	MediaConfig_set_MinHeight_mE35C7CD18C15594CAE8178371F20FF72EE80721D,
	MediaConfig_get_MaxWidth_m88A44DD29260CC3594F7AF0C9B000883F6332A29,
	MediaConfig_set_MaxWidth_m40ECFFAB3600027BF70B7FD100AA9F94735A1FC8,
	MediaConfig_get_MaxHeight_m7F772654AC31AA4CA86305634E8D0BA0695FC253,
	MediaConfig_set_MaxHeight_m9528F762FBCA544FE81FEED89116947ACDAE9477,
	MediaConfig_get_IdealWidth_mCAF4D2637DB2C207E89E9E4B1B94D21C2FC5D9FE,
	MediaConfig_set_IdealWidth_m4660A65C8E78F0FED4ED3023CBE2D2CB2886A615,
	MediaConfig_get_IdealHeight_mC4FA1D5E861B781CE7BE0DF4AC6FF4B2EB0B01C1,
	MediaConfig_set_IdealHeight_m88433F39BBDF1584EB240B5225EE19AEF0B1DEF1,
	MediaConfig_get_IdealFrameRate_m21DE0AEACAD550A06342C353FEC64140872BA2E1,
	MediaConfig_set_IdealFrameRate_m0AA6DA5B871C5DECD098CCCA084B1AFDACA709B2,
	MediaConfig_get_MinFrameRate_m70A65C63DDEA9BF11B803D23AB9C489CC6753FDD,
	MediaConfig_get_MaxFrameRate_m8E4D79608AD51FB8318A829386D72F77638E0E32,
	MediaConfig_get_Format_mC7D69871CE9963DAFC16EA2C9D5F25488DDC2492,
	MediaConfig_set_Format_m323534BEA6DFD56ECE96A4C0E8409E0E1389D905,
	MediaConfig__ctor_m9F57DF2DA5B07EAEA3FD116B734412547CF95C50,
	MediaConfig__ctor_m8DD3603B96D321F06BDC020E2341355B83584967,
	MediaConfig_DeepClone_m6700A00DAD34DACF1BA7266DD35A793AE6DCAC9F,
	MediaConfig_ToString_m63DACCF2CFA322BA1BE5597B28E7FD5B5D90CFA9,
	MessageDataBufferExt_AsStringUnicode_m2B5AC83632488AE41F7BA5C2272A089F61B8FCED,
	MessageDataBufferExt_Copy_mA35B174D17F81FB2AB25399BDD5325CA8326E210,
	NetworkConfig_get_IceServers_m5E6A3C383226E2FCDB32569895032DA5F78937C2,
	NetworkConfig_get_SignalingUrl_m23E0B3DF7D3A55FDCA43AB1FDC6CD17B3D6692E4,
	NetworkConfig_set_SignalingUrl_m8BE7C4E830F00645F4EBBCFAB689A4E8F15C8194,
	NetworkConfig_get_AllowRenegotiation_m053139CFE0705A02E35D62A572F3ED7A5807E94E,
	NetworkConfig_get_IsConference_mFACBDD257AE18F00F8BED9B4325AF4956FA857D4,
	NetworkConfig_set_IsConference_m4CBB6787E50567622F7584668B61BD237A318EE7,
	NetworkConfig_ToString_m6C2BDF22F5AFE8B70F6444BB3F20E7167DC2D305,
	NetworkConfig__ctor_m602D7088B4491C0C9438A2316195FAA7F1C0E5F4,
	NetworkEvent_get_Type_m90AD2D28598E4CB47CC71E363D104C8AC7A705C5_AdjustorThunk,
	NetworkEvent_get_ConnectionId_m63ED1FC3B26ED68918F1395BA9D0EAF1306F2524_AdjustorThunk,
	NetworkEvent_get_MessageData_m4E9F1B0D89F50E350EF0262187C7DE74ADCEED26_AdjustorThunk,
	NetworkEvent_get_Info_mA98458FF9FA0B252D18705DD6B1A3B17D9503E16_AdjustorThunk,
	NetworkEvent_get_ErrorInfo_m8AEF4E79F149ACDE34B56754A88ADA709AAD8D95_AdjustorThunk,
	NetworkEvent__ctor_m331245C2DC884EF629C1B1103876AD951A650585_AdjustorThunk,
	NetworkEvent__ctor_mF53983C4264CD0231654304A49965EC5B9D94C9E_AdjustorThunk,
	NetworkEvent__ctor_mA7EF6BADF8188FC05D529262AC7801F91609C857_AdjustorThunk,
	NetworkEvent__ctor_mB80AF54D4891D78B87014B9855C68EE206381C89_AdjustorThunk,
	NetworkEvent_ToString_m4B700DB0C7D27B2855D19F7ABF80FB5112ED2105_AdjustorThunk,
	NetworkEvent_IsMetaEvent_m6BEFDA1C4672772D4B0256672EC9F15661FCBFDB,
	NetworkEvent_FromByteArray_m31466305C7DB5DFC36469055EB90ECD536E165FC,
	NetworkEvent_ToByteArray_m2C19FC5A5C98D7795FACBFC34CA65C40EDFB6785,
	NetworkEvent_AttachError_m04E5757641BF4A66B8CDD83705378B4147F3EC15_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BufferedFrame_get_Buffer_mAE0ABEE3EEB522AA01F1F340AF5BA298C1A8FB6C,
	BufferedFrame_get_Width_m72C73F4EA72CAEB75935EE8AC5D2DABED85CF715,
	BufferedFrame_get_Height_m998CD32662B7B6C6E843E19464D1F47CDE9B8590,
	BufferedFrame_get_Rotation_m7C3C0038C644484BB517B4C63A441AEF18C2C947,
	BufferedFrame_set_Rotation_m0E114C8C8ABC472DF03C47233D39F4978D5C7E50,
	BufferedFrame_get_Format_m816E89BAEDCF11F2EC5C8A5BA850DA4FD2B7C2D9,
	BufferedFrame__ctor_m887D4B7F3371309EBFCAF5CD7FFF85459F223071,
	BufferedFrame_Dispose_m60511244D7ABFC342044E26E69CA6C9E239075C6,
	SLog_SetLogger_m5D3E7F714E0AC197E10DB6FCDB06D1209D463F91,
	SLog_LogException_m36D2B19FEACBC5A8799BFC23E8B2DB1A183E88B0,
	SLog_LE_m4B26380D8909353763D54ED1E2E1217292226234,
	SLog_LW_m4C1FD9091E37D07F12DFBDCE665962B60B7F2DE5,
	SLog_L_m2B84D1BE41D8BA16E73F6E952C5C17D7D2B686A1,
	SLog_MergeTags_mFF2513B04A32F652F6FDA529489D942C805803AA,
	SLog_LogArray_mAD74446C19CFC07B879694644E4C675F3F30508E,
	SLog__cctor_mC00C1300F50AA43CB5BFCFABC5C6FC5BD20DADC6,
};
static const int32_t s_InvokerIndices[237] = 
{
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	2130,
	31,
	2131,
	2131,
	89,
	31,
	26,
	26,
	31,
	26,
	26,
	26,
	26,
	26,
	26,
	465,
	114,
	0,
	0,
	114,
	0,
	23,
	23,
	2132,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	31,
	23,
	2130,
	2131,
	2131,
	89,
	31,
	3,
	14,
	10,
	10,
	10,
	32,
	10,
	10,
	10,
	14,
	10,
	32,
	32,
	35,
	23,
	23,
	35,
	3,
	21,
	21,
	820,
	23,
	124,
	27,
	125,
	26,
	10,
	32,
	2133,
	2134,
	2133,
	2134,
	14,
	62,
	14,
	14,
	26,
	14,
	2135,
	2135,
	10,
	2133,
	89,
	14,
	2136,
	617,
	9,
	10,
	2137,
	2137,
	14,
	3,
	49,
	26,
	14,
	3,
	862,
	23,
	2138,
	2134,
	23,
	23,
	26,
	23,
	2139,
	28,
	28,
	105,
	14,
	89,
	14,
	14,
	14,
	206,
	14,
	26,
	89,
	14,
	23,
	2140,
	2130,
	2131,
	2131,
	89,
	31,
	89,
	23,
	9,
	26,
	23,
	2139,
	23,
	2138,
	23,
	862,
	23,
	2134,
	2139,
	2133,
	26,
	2141,
	2141,
	2142,
	2132,
	2143,
	2134,
	26,
	23,
	31,
	23,
	3,
	-1,
	-1,
	89,
	31,
	89,
	31,
	14,
	26,
	793,
	2144,
	793,
	2144,
	793,
	2144,
	793,
	2144,
	793,
	2144,
	793,
	2144,
	793,
	2144,
	793,
	793,
	10,
	32,
	23,
	26,
	14,
	14,
	0,
	0,
	14,
	14,
	26,
	89,
	89,
	31,
	14,
	23,
	89,
	2133,
	14,
	14,
	14,
	2142,
	2141,
	2141,
	2141,
	14,
	114,
	2145,
	2146,
	26,
	14,
	10,
	10,
	10,
	10,
	15,
	10,
	14,
	10,
	10,
	10,
	32,
	10,
	767,
	23,
	163,
	137,
	137,
	137,
	137,
	1,
	137,
	3,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0200001C, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 25372 },
};
extern const Il2CppCodeGenModule g_Byn_AwrtcCodeGenModule;
const Il2CppCodeGenModule g_Byn_AwrtcCodeGenModule = 
{
	"Byn.Awrtc.dll",
	237,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
