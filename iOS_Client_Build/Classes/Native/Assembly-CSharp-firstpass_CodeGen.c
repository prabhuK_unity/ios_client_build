﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CameraSwitch::OnEnable()
extern void CameraSwitch_OnEnable_m328899CF90E468B9BCA712951BE98D6BD12E22E6 ();
// 0x00000002 System.Void CameraSwitch::NextCamera()
extern void CameraSwitch_NextCamera_mA9928DD3F1556FBD87C28517BFF30F97061DD3DE ();
// 0x00000003 System.Void CameraSwitch::.ctor()
extern void CameraSwitch__ctor_mC59348604BF2EA0F8B95C411FB72D433805DA29D ();
// 0x00000004 System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LevelReset_OnPointerClick_m51DF1E315BB3E84074EC3BED6D371A390481BA54 ();
// 0x00000005 System.Void LevelReset::Update()
extern void LevelReset_Update_mD312970743CBF30D43B86B8FBD8532D9370D131D ();
// 0x00000006 System.Void LevelReset::.ctor()
extern void LevelReset__ctor_mF67827549AD897B8D088F6EB7A59D3EC1E201187 ();
// 0x00000007 System.Void AlphaButtonClickMask::Start()
extern void AlphaButtonClickMask_Start_mEFF14FD0C0E68631370255F5936C41963983C8F0 ();
// 0x00000008 System.Boolean AlphaButtonClickMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void AlphaButtonClickMask_IsRaycastLocationValid_m4E83446D8B35549DE9EEE9F6D4548A527A3C884D ();
// 0x00000009 System.Void AlphaButtonClickMask::.ctor()
extern void AlphaButtonClickMask__ctor_m5D0B36A0D207C1AD45B2434C1036141F3D9EA28D ();
// 0x0000000A System.Void EventSystemChecker::Awake()
extern void EventSystemChecker_Awake_mADD5BA9635C9ADBE91C44BC15DFFCB87798377EB ();
// 0x0000000B System.Void EventSystemChecker::.ctor()
extern void EventSystemChecker__ctor_m52A33F6E257A581FC40521C4CE4B484D2426DA66 ();
// 0x0000000C System.Void ForcedReset::Update()
extern void ForcedReset_Update_m35CBD926C48BD4ACCDD3CB14E0813D04F0E83FC0 ();
// 0x0000000D System.Void ForcedReset::.ctor()
extern void ForcedReset__ctor_mF32F8C351B95343959FD07B1FF0EC0FBC8CB8EE0 ();
// 0x0000000E System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern void ActivateTrigger_DoActivateTrigger_m91589CEC5598200BE6AC01C6D8ED7CD3C05ACF03 ();
// 0x0000000F System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void ActivateTrigger_OnTriggerEnter_mBB7C6EBA79177C6CBEB136C565F17135353319DB ();
// 0x00000010 System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern void ActivateTrigger__ctor_mE845995D67483BC443AE11D6EB0D4E133358EF15 ();
// 0x00000011 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern void AutoMobileShaderSwitch_OnEnable_m2CC4D934531213CC59B29149430BE5454F220327 ();
// 0x00000012 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern void AutoMobileShaderSwitch__ctor_m1D46DC3555ED91D57F71F19BD08DB1F72AA8928F ();
// 0x00000013 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern void AutoMoveAndRotate_Start_m8A0766F7DA35E226BEC81C38DC800B7C2395E654 ();
// 0x00000014 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern void AutoMoveAndRotate_Update_m23C3C317C277A79CF82C88044544258DD7201411 ();
// 0x00000015 System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern void AutoMoveAndRotate__ctor_m97BEA90D4085E63DA4D810FED6F495216024AC6B ();
// 0x00000016 System.Void UnityStandardAssets.Utility.CameraRefocus::.ctor(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern void CameraRefocus__ctor_mAEC82E5DC336BB11096300A567B554CF5235781C ();
// 0x00000017 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeCamera(UnityEngine.Camera)
extern void CameraRefocus_ChangeCamera_mD34A981CD7F9E0F4CB3DB5A3C929A969E0FFC538 ();
// 0x00000018 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeParent(UnityEngine.Transform)
extern void CameraRefocus_ChangeParent_m95B7671F5C921BACD887A784A798B9C373C107FD ();
// 0x00000019 System.Void UnityStandardAssets.Utility.CameraRefocus::GetFocusPoint()
extern void CameraRefocus_GetFocusPoint_m708CC6CD84E14B6285E4404388F0369B9A550C40 ();
// 0x0000001A System.Void UnityStandardAssets.Utility.CameraRefocus::SetFocusPoint()
extern void CameraRefocus_SetFocusPoint_m07D61D09C19C5F951C126B32B3341733C33C20BF ();
// 0x0000001B System.Void UnityStandardAssets.Utility.CurveControlledBob::Setup(UnityEngine.Camera,System.Single)
extern void CurveControlledBob_Setup_m2007E3FF9FB757CC191CEA557786000FDD658477 ();
// 0x0000001C UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::DoHeadBob(System.Single)
extern void CurveControlledBob_DoHeadBob_m00B658784E0C963606ADCAB3E3FA9C347F305C6F ();
// 0x0000001D System.Void UnityStandardAssets.Utility.CurveControlledBob::.ctor()
extern void CurveControlledBob__ctor_mC6A43DC3D1DD13D04AC060CCB621CCF90965BC90 ();
// 0x0000001E System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern void DragRigidbody_Update_m83FBCA60844264E0D9544B4BA94D8EE3590B1055 ();
// 0x0000001F System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern void DragRigidbody_DragObject_m2BF373EA880D49253F3C5BB726D8724B2288A80C ();
// 0x00000020 UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern void DragRigidbody_FindCamera_mAF1386C508DAEB88A9C5FD4D456EF4D4BE617D9F ();
// 0x00000021 System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern void DragRigidbody__ctor_m6E05682AB45C4BFE0DE11A644D612926F9E76E7D ();
// 0x00000022 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern void DynamicShadowSettings_Start_m77EC175AF947D4CD8BE908FEAFA761CB247E6D78 ();
// 0x00000023 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern void DynamicShadowSettings_Update_mCA0BBCA7CA61B468857533B4C7896AFABBF9175C ();
// 0x00000024 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern void DynamicShadowSettings__ctor_mDC8B0D663149C35F242EADFEFED8BA7D4DBCBC77 ();
// 0x00000025 System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern void FOVKick_Setup_m8469A5705E531E55305A58D3AA3265FA265D33F6 ();
// 0x00000026 System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern void FOVKick_CheckStatus_m9A63EE82724EA159E55AC55E840B4CE3701BE714 ();
// 0x00000027 System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern void FOVKick_ChangeCamera_m3218EF587B033155B743FE279EDCB6CF1CDF4C6F ();
// 0x00000028 System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern void FOVKick_FOVKickUp_m91F264E5432BCE86E9082C169D277E64C481D236 ();
// 0x00000029 System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern void FOVKick_FOVKickDown_m8BCEE7A586E2B8FAD73F8D8F5D2FB0808A1EEB75 ();
// 0x0000002A System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern void FOVKick__ctor_m3D6A4FBAE68243DB82771B3772735848FA090280 ();
// 0x0000002B System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern void FPSCounter_Start_mA01345F424F9731649187CC62E9E69FF0377EB65 ();
// 0x0000002C System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern void FPSCounter_Update_m8E1247E9BA50C20B0439DF5CA383621EB7B9FFE5 ();
// 0x0000002D System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern void FPSCounter__ctor_m3458B72F16625D58BEFC538E933635817205602D ();
// 0x0000002E System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern void FollowTarget_LateUpdate_m5ADBC5E3D58826659C01A2C4F8968A595F092435 ();
// 0x0000002F System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern void FollowTarget__ctor_m7ADC7081450A6DA01AE0B5CA1DB84BE961D5DD29 ();
// 0x00000030 System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern void LerpControlledBob_Offset_mCC7467CE1E66FB6246E52C5984D1C7BE2C0BF1B2 ();
// 0x00000031 System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern void LerpControlledBob_DoBobCycle_mB5C14AC5AEF7114F4A575E19B36BEDF0DE691923 ();
// 0x00000032 System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern void LerpControlledBob__ctor_m8E3865BC67563143CFF3C791D0BBFC25D94B18BE ();
// 0x00000033 System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern void ObjectResetter_Start_mF14852C85D595D52F35F221F45A822606EA013B7 ();
// 0x00000034 System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern void ObjectResetter_DelayedReset_mCFE31FD35B3AC724EFD1E76270A1FE3FD5014394 ();
// 0x00000035 System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern void ObjectResetter_ResetCoroutine_mD46D3A049C28C60DB794613257CC61C5102B0EFC ();
// 0x00000036 System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern void ObjectResetter__ctor_mD14550E617FC16A6C1338BE7D18C5B01141A6354 ();
// 0x00000037 System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern void ParticleSystemDestroyer_Start_m28FC4048F585ACE30B2FB2B6839F2801F8FFE584 ();
// 0x00000038 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern void ParticleSystemDestroyer_Stop_mC007423F42F57A16264A616C6970FF62E3A37617 ();
// 0x00000039 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern void ParticleSystemDestroyer__ctor_m3A484EB728A7ECF87E3BF235EF10F4EF49E11623 ();
// 0x0000003A System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern void PlatformSpecificContent_OnEnable_m3C87AD39C8B588F5FBA2C7D2808C717A238A5BFB ();
// 0x0000003B System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern void PlatformSpecificContent_CheckEnableContent_m6C285401B4F1F4BFC5E748C7753096628CDAB355 ();
// 0x0000003C System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern void PlatformSpecificContent_EnableContent_mF21760A340E974403AD8731E6731AD1BD6B6C73C ();
// 0x0000003D System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern void PlatformSpecificContent__ctor_m8464E7FB25A5C1EC2C1343995EC02402AA72804F ();
// 0x0000003E System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::OnEnable()
extern void SimpleActivatorMenu_OnEnable_m860A5963E1D18B5F5F870A5286F93FE49E55E7CF ();
// 0x0000003F System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::NextCamera()
extern void SimpleActivatorMenu_NextCamera_m0488F3D0A808FE4A11C85125ED15092E910CFE96 ();
// 0x00000040 System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::.ctor()
extern void SimpleActivatorMenu__ctor_m82227309D300C3ED3D119D6D063E19D5A56C7D18 ();
// 0x00000041 System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern void SimpleMouseRotator_Start_m8D06607C94F05C2C99A1D74A1D75AA716B1FE80E ();
// 0x00000042 System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern void SimpleMouseRotator_Update_m43E994D26B1A1396F1C87F119B313442201E7222 ();
// 0x00000043 System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern void SimpleMouseRotator__ctor_mC1B6E0FDF0226BE88DF307078BD8EE4E5DC546B8 ();
// 0x00000044 System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern void SmoothFollow_Start_mDCCB4CDC6353E65906B648B5AF23AB18D2E51C72 ();
// 0x00000045 System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern void SmoothFollow_LateUpdate_m7F7C460DDC3C80A3016F8050F50F54C6D55C5C15 ();
// 0x00000046 System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern void SmoothFollow__ctor_m3BA62097DC88A8513EFFEFCF2D5CD8D3DB7B548F ();
// 0x00000047 System.Void UnityStandardAssets.Utility.TimedObjectActivator::Awake()
extern void TimedObjectActivator_Awake_m79DD17D62A8055E1E336A30C26DB80C4D5407289 ();
// 0x00000048 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Activate(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_Activate_m12334B3127ED32333F52AD3DE31A5ABC8EA4D415 ();
// 0x00000049 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Deactivate(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_Deactivate_m6A966711DD7B1088A53D5CAC48760661834A3979 ();
// 0x0000004A System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::ReloadLevel(UnityStandardAssets.Utility.TimedObjectActivator_Entry)
extern void TimedObjectActivator_ReloadLevel_m80D86EA368E03010F6065856EF104F53D4FAB590 ();
// 0x0000004B System.Void UnityStandardAssets.Utility.TimedObjectActivator::.ctor()
extern void TimedObjectActivator__ctor_m26FCB0BA5A4631EC3F435E0182C9C4E702B1EAD6 ();
// 0x0000004C System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern void TimedObjectDestructor_Awake_m76424E72B7BE0F6424D18BEF93CAB84DBAAC34A6 ();
// 0x0000004D System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern void TimedObjectDestructor_DestroyNow_mE93B19E7C626189327CE5300D972C0BDBBE04851 ();
// 0x0000004E System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern void TimedObjectDestructor__ctor_mE45D4C3E5E22221059FB74DF5134F06171EFDE6D ();
// 0x0000004F System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern void WaypointCircuit_get_Length_mFA587B16273CB5FD90CC3BD7479A2810B4D318F8 ();
// 0x00000050 System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern void WaypointCircuit_set_Length_m39E0ED03A71FEC5DA4B5851069C677BB5A8A657B ();
// 0x00000051 UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern void WaypointCircuit_get_Waypoints_m95D546B796B966FA6ADB8AE7B553F25CCF548211 ();
// 0x00000052 System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern void WaypointCircuit_Awake_m3443AE18FBD735BE48A510AD2B277A86CEB1CC52 ();
// 0x00000053 UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern void WaypointCircuit_GetRoutePoint_m8D42746EC9609D8082B0E7CC8E19A2C4F27800B0 ();
// 0x00000054 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern void WaypointCircuit_GetRoutePosition_m0973D453CE3244E6E1066641A81AD3FF0EADBB6F ();
// 0x00000055 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void WaypointCircuit_CatmullRom_mD59EEE7A548ACAE81B8650873BCE7547CEA63077 ();
// 0x00000056 System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern void WaypointCircuit_CachePositionsAndDistances_m69B6E9F8A8CA46B05F025FD09309FC126FCF6F46 ();
// 0x00000057 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern void WaypointCircuit_OnDrawGizmos_m69BAAF20DC9C4E903B5BA88D0DE0D30D95456287 ();
// 0x00000058 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern void WaypointCircuit_OnDrawGizmosSelected_m07DB42CB844CC4517075DF5AFFDC5F2AAFDE8134 ();
// 0x00000059 System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern void WaypointCircuit_DrawGizmos_m614C51310D7116AECB903F6CA4A3047F819C9520 ();
// 0x0000005A System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern void WaypointCircuit__ctor_m349B1FD72D470FC31D2C7D18D0625C3DE68C9304 ();
// 0x0000005B UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern void WaypointProgressTracker_get_targetPoint_mF25566A72CA099496F6C921209690D340E7E1067 ();
// 0x0000005C System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_targetPoint_m9FF276072D75E5B62BDFDD69FE2BC764B332E636 ();
// 0x0000005D UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern void WaypointProgressTracker_get_speedPoint_mD1DA662D3473D8A387FA4B9217E024C9F86ECE32 ();
// 0x0000005E System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_speedPoint_m5CCCC560F087199E1A30537AF62CAAE4EFDA1062 ();
// 0x0000005F UnityStandardAssets.Utility.WaypointCircuit_RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern void WaypointProgressTracker_get_progressPoint_m2CBA2DD7271E5B4DCBEDE3D4A583DDCBAF7843CD ();
// 0x00000060 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit_RoutePoint)
extern void WaypointProgressTracker_set_progressPoint_mDB352CD4E635034EF5155F6661F200CCDE51B5F7 ();
// 0x00000061 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern void WaypointProgressTracker_Start_m3AE39D9788836A78979E71F8F6BC3109BD843341 ();
// 0x00000062 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern void WaypointProgressTracker_Reset_m71BA52616EDE5E48445AAC8163F57CD90A5B3602 ();
// 0x00000063 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern void WaypointProgressTracker_Update_mCF073672622D91EF9F70ED8E870A6E4400ABD694 ();
// 0x00000064 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern void WaypointProgressTracker_OnDrawGizmos_mAE2FA8689934F94640161CFA136411E825660EC9 ();
// 0x00000065 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern void WaypointProgressTracker__ctor_m6832B310DE7A3327A4AC027F4CD32D1D47D33722 ();
// 0x00000066 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern void ParticleSceneControls_Awake_m139756B596B52BFC8B59A49CE1D480F2F75C4CA5 ();
// 0x00000067 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern void ParticleSceneControls_OnDisable_mF56709D90F15CB0D24A732D4AA05B21B2337BE98 ();
// 0x00000068 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern void ParticleSceneControls_Previous_mAFB64030DC9B8850C6515E49A7DF5FEF47B87EF5 ();
// 0x00000069 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern void ParticleSceneControls_Next_m6B4A614F24EC9A11EB86A9DC0195DFA2558AFDB2 ();
// 0x0000006A System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern void ParticleSceneControls_Update_m859C7099E3ECFE587B85438B855CEBD33D94C9C2 ();
// 0x0000006B System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern void ParticleSceneControls_CheckForGuiCollision_mE600635956C49D0B018BE5BB365439812AD3341F ();
// 0x0000006C System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern void ParticleSceneControls_Select_m0700DE6D1A461460931B138FAD3F8F0B4A89E1DC ();
// 0x0000006D System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern void ParticleSceneControls__ctor_mFC35877F8E88194D78E79EFA0465B1C42F440AC7 ();
// 0x0000006E System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern void ParticleSceneControls__cctor_m3C39F6683C76F97D763656FF60A95F908776209D ();
// 0x0000006F System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern void PlaceTargetWithMouse_Update_mB6E4469ABC90B39EFC5ABD14C87476390371B5EA ();
// 0x00000070 System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern void PlaceTargetWithMouse__ctor_m4742E4E69EB09CE65BA92043B4F6634891A5511F ();
// 0x00000071 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern void SlowMoButton_Start_mEC813F697FF6A73D17788C1E1C2D4AEDF900D9B6 ();
// 0x00000072 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern void SlowMoButton_OnDestroy_m7935F8679D6AB5D66386D76D95A46EB0BCC93758 ();
// 0x00000073 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern void SlowMoButton_ChangeSpeed_m32B57883D33DBF82AD8C56EC7DE1517E905A7ED3 ();
// 0x00000074 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern void SlowMoButton__ctor_mA1B5306C33E6C4EBA9A440BEA3489D6BB602DBAF ();
// 0x00000075 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern void AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE ();
// 0x00000076 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern void AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A ();
// 0x00000077 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern void AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3 ();
// 0x00000078 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern void AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0 ();
// 0x00000079 System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern void ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22 ();
// 0x0000007A System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B ();
// 0x0000007B System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern void ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E ();
// 0x0000007C System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern void ExplosionPhysicsForce_Start_m22750184987B8A375DA674EF615F5ECD1DAD04C5 ();
// 0x0000007D System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern void ExplosionPhysicsForce__ctor_m3A4FFA852D5C736E4CE47A5A5A3E0CA1E954F660 ();
// 0x0000007E System.Void UnityStandardAssets.Effects.Explosive::Start()
extern void Explosive_Start_m500D588F6ABB1C03488B93AE78086A368943CDE2 ();
// 0x0000007F System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern void Explosive_OnCollisionEnter_mB88EB7BF1526BC0F9418D997273E1A1C875D00E1 ();
// 0x00000080 System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern void Explosive_Reset_m878D1F8DF175604AFB07827C7A4DA9DA20FFA9DA ();
// 0x00000081 System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern void Explosive__ctor_m21EF3DDCC8F02804ED974F48197F95DB5B4900F4 ();
// 0x00000082 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern void ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39 ();
// 0x00000083 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern void ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8 ();
// 0x00000084 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern void ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068 ();
// 0x00000085 System.Void UnityStandardAssets.Effects.FireLight::Start()
extern void FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D ();
// 0x00000086 System.Void UnityStandardAssets.Effects.FireLight::Update()
extern void FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7 ();
// 0x00000087 System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern void FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776 ();
// 0x00000088 System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern void FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE ();
// 0x00000089 System.Void UnityStandardAssets.Effects.Hose::Update()
extern void Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939 ();
// 0x0000008A System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern void Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094 ();
// 0x0000008B System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern void ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D ();
// 0x0000008C System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern void ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC ();
// 0x0000008D System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern void SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6 ();
// 0x0000008E System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern void SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C ();
// 0x0000008F System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern void WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792 ();
// 0x00000090 System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern void WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70 ();
// 0x00000091 System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern void WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3 ();
// 0x00000092 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern void AxisTouchButton_OnEnable_mCB6F2E22CE0ED2867462D2B6477319CA34EC6923 ();
// 0x00000093 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern void AxisTouchButton_FindPairedButton_m40E5A08627D81FC2C9B18410E0A315A00AFB8E5E ();
// 0x00000094 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern void AxisTouchButton_OnDisable_mA60CC8A5ACA0AF8EA245F67C774CC15489D26F0D ();
// 0x00000095 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerDown_mF8ED454A7629EC10D5D65B0806B1575B969CC151 ();
// 0x00000096 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerUp_m47ACA384B0AB0496024E2BA07670DE7F416442A6 ();
// 0x00000097 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern void AxisTouchButton__ctor_m92FC868F9C30069B6E82AEFB601E72D7958146EB ();
// 0x00000098 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern void ButtonHandler_OnEnable_mF4257EC750A191164B42AA7ED19E95221E0C9084 ();
// 0x00000099 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern void ButtonHandler_SetDownState_m22A563B85C0CFC7883586FE49D47EC8F4795740E ();
// 0x0000009A System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern void ButtonHandler_SetUpState_m20D0A07A6047D866EB95EB5D32D6D5C208CF779D ();
// 0x0000009B System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern void ButtonHandler_SetAxisPositiveState_m2914614CA82F6CE2BCDEF41198D6505C08692F60 ();
// 0x0000009C System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern void ButtonHandler_SetAxisNeutralState_m710F8A130253345840FC6A942453EBD386322691 ();
// 0x0000009D System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern void ButtonHandler_SetAxisNegativeState_m57D4FA1CCB8E9F4CCB3D5AB589D57328EBDA782C ();
// 0x0000009E System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern void ButtonHandler_Update_m84201A612F81A8C08E1E4F78635C47098A9C6BC0 ();
// 0x0000009F System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern void ButtonHandler__ctor_m85B4D66D1CA83E5A73BFC70CAFB4F2D00F559795 ();
// 0x000000A0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern void CrossPlatformInputManager__cctor_m62BA59D81597A25895815DDB40DD8944A6B804F2 ();
// 0x000000A1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_ActiveInputMethod)
extern void CrossPlatformInputManager_SwitchActiveInputMethod_m3BAB791534CAB7AA957901F370FE854C63AB1801 ();
// 0x000000A2 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern void CrossPlatformInputManager_AxisExists_m13A4ED5F88BAC335FDC42ADD4AAC9BB4CC5809F9 ();
// 0x000000A3 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern void CrossPlatformInputManager_ButtonExists_m6C0E094AF61CF2F0F6592EFA67763D208C867440 ();
// 0x000000A4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis)
extern void CrossPlatformInputManager_RegisterVirtualAxis_m84945297F5E2C4D218B59B76E9D90D3BD36198A4 ();
// 0x000000A5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton)
extern void CrossPlatformInputManager_RegisterVirtualButton_mA5218520E9EE798325C72DFD0C988DC313D36BCF ();
// 0x000000A6 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualAxis_m33DCEB8DAAF2703BFAB8F156A6633C0F4316C1A4 ();
// 0x000000A7 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualButton_m4B8F22F23F0891C1F5D4C07B729564D6A95CB82D ();
// 0x000000A8 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern void CrossPlatformInputManager_VirtualAxisReference_m5864A44C3FE72270B22D4C97FADEEB2AAA77869D ();
// 0x000000A9 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern void CrossPlatformInputManager_GetAxis_m4D45F9BE30A159DA4E72F4BF8294872297566E2D ();
// 0x000000AA System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern void CrossPlatformInputManager_GetAxisRaw_mE6D8754EAE5F6838CCF172FB03F4C251648EE987 ();
// 0x000000AB System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern void CrossPlatformInputManager_GetAxis_mC9F177F6F0D83131B599CF80C3F3A8D7AD4568A0 ();
// 0x000000AC System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern void CrossPlatformInputManager_GetButton_m728A64B9BC3F6471EB11B9CAF54BD4A10C710207 ();
// 0x000000AD System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern void CrossPlatformInputManager_GetButtonDown_mE1BCD85447E0EF510728E49314FBCCEEE1FC7E8D ();
// 0x000000AE System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern void CrossPlatformInputManager_GetButtonUp_mD115A6BD45062A08A42EBBC7F0C9EC0D4F764ADD ();
// 0x000000AF System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern void CrossPlatformInputManager_SetButtonDown_m4DBFE81592B86D460ACC34D5936C788CD5B50890 ();
// 0x000000B0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern void CrossPlatformInputManager_SetButtonUp_m6228A0BD77568A903DF6429EEACD2267028FA32A ();
// 0x000000B1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern void CrossPlatformInputManager_SetAxisPositive_mC5C7F88EEF5D6CB7B6B91BF6279FA53A94B4D527 ();
// 0x000000B2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern void CrossPlatformInputManager_SetAxisNegative_m41A74CBE51E8CB4870C79A8343E66B99B2CA7FDB ();
// 0x000000B3 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern void CrossPlatformInputManager_SetAxisZero_mBBD24590C97037F84384A559AAE37D2F8CA51730 ();
// 0x000000B4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern void CrossPlatformInputManager_SetAxis_m6BCE358D3D1A2E5E393AF281602B3E4745C0C5DA ();
// 0x000000B5 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern void CrossPlatformInputManager_get_mousePosition_mC886FC2F654E91F06407FDB891DF3201ED576DCD ();
// 0x000000B6 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionX_m1800042FCD90010EA2E2D51969D971324DD11964 ();
// 0x000000B7 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionY_mDD4A2DF42E6CD673054A91FFE3C7FA61812889A8 ();
// 0x000000B8 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionZ_m121058A0846AE6A974855607C8E3D46C221B376F ();
// 0x000000B9 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern void InputAxisScrollbar_Update_m4B6A6BBF4FAED786086BE4F9997E1D2D373BF2FE ();
// 0x000000BA System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern void InputAxisScrollbar_HandleInput_mF3A427E653ED917C3E91E0CBB1A3990F6110FB11 ();
// 0x000000BB System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern void InputAxisScrollbar__ctor_mB96FAA176CD2958CCDE6E5F9212DCF2082486243 ();
// 0x000000BC System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern void Joystick_OnEnable_m8728113F5BEE6D91514CB1A07550E8E7A3856CEE ();
// 0x000000BD System.Void UnityStandardAssets.CrossPlatformInput.Joystick::Start()
extern void Joystick_Start_m6164BF9BB7A0A8DD4524223639EA549E0491CFFD ();
// 0x000000BE System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern void Joystick_UpdateVirtualAxes_m5B79E0FBC765F85D9EE7FA9C7D74BDB35F326F3E ();
// 0x000000BF System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern void Joystick_CreateVirtualAxes_mCD13DFD2ADED0444F18C3856FD67A78539FD9C2C ();
// 0x000000C0 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m1DC1103944EB982931C5946BD8EBFB8E63073BB6 ();
// 0x000000C1 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m06850F5D6C95D16DEB57B3FC4E50CCBCCD0EF7FB ();
// 0x000000C2 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m1F10B670117FD67A734079ED71D4A3D36B783718 ();
// 0x000000C3 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern void Joystick_OnDisable_m5097E08289FECC9A5499DB0747575F075353CAFB ();
// 0x000000C4 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern void Joystick__ctor_mA2C408B1EB0671CB8B340DBF932CB4153BAC3ABF ();
// 0x000000C5 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern void MobileControlRig_OnEnable_mAF3C7A8C67CE239A1FD5E6A8B224F7A91DE8B2E8 ();
// 0x000000C6 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::Start()
extern void MobileControlRig_Start_m43792FB70FC02989DA9543801183A54005AD572B ();
// 0x000000C7 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern void MobileControlRig_CheckEnableControlRig_m59A8FCD09B2A6EA7702AE9EBB24E3BB9605B5CCD ();
// 0x000000C8 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern void MobileControlRig_EnableControlRig_m694051D1F28B05510357A3F96561EAF2732CAF8E ();
// 0x000000C9 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern void MobileControlRig__ctor_mCCFD8CBDA57F8D9B0E2805D4740637F5FFC4B120 ();
// 0x000000CA System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern void TiltInput_OnEnable_mEACF194C56E3620055240D9D46880E6F9C201E9C ();
// 0x000000CB System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern void TiltInput_Update_m14D22BD1D9D47DF03965F0F6BBC2FB0E322F2B2A ();
// 0x000000CC System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern void TiltInput_OnDisable_m169AFDCFFA0609747DA889DF88C86D0A5C9C42B0 ();
// 0x000000CD System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern void TiltInput__ctor_m909CBEC7014B584CB5EBF5A0B650E5D8845E4FB7 ();
// 0x000000CE System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern void TouchPad_OnEnable_mFED012C0FDD349798B1296799C0AE7A05C2017EE ();
// 0x000000CF System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Start()
extern void TouchPad_Start_m986951F12FF80D3A0D4B0DCB49D9FBA13B8A025C ();
// 0x000000D0 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern void TouchPad_CreateVirtualAxes_m214CE099E087A7A6FCDDF3B2740983436B62BA5E ();
// 0x000000D1 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern void TouchPad_UpdateVirtualAxes_m42D25C5EE9F890FECF580C97219455E73D09AF67 ();
// 0x000000D2 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerDown_m13FDEACD95785D853D85B68E4993AC520A2D771F ();
// 0x000000D3 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern void TouchPad_Update_m0DD077DCE945CC47C3DD4FFDB9FEC5D4BB3A762A ();
// 0x000000D4 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerUp_mE35C9A5F6CD1909E8819F4D6D7282C7D20B37B88 ();
// 0x000000D5 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern void TouchPad_OnDisable_mD0E67236EB0D365E3397D26723250C01614168B5 ();
// 0x000000D6 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern void TouchPad__ctor_m9FEC2CD43CD850304B41B1C0142CC47F44B01E25 ();
// 0x000000D7 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern void VirtualInput_get_virtualMousePosition_m897C50683722D1C3DF4FA9801524E7BF310B24BD ();
// 0x000000D8 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern void VirtualInput_set_virtualMousePosition_mDDF9F35B2C4AC37AB6CCF68772C57315612B1F75 ();
// 0x000000D9 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern void VirtualInput_AxisExists_mDB6E7D0AF32ECE3E3CB1C4DA089D4B030D61F3F8 ();
// 0x000000DA System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern void VirtualInput_ButtonExists_mBD9401EC2186C54F8EA7577FEEA500624F2E6083 ();
// 0x000000DB System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis)
extern void VirtualInput_RegisterVirtualAxis_m43BC4BC9355B708CC739E3F2D0761A49342BC60F ();
// 0x000000DC System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton)
extern void VirtualInput_RegisterVirtualButton_mF6874262B94F78D0C2C166F7E20CFA47DD39BF41 ();
// 0x000000DD System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern void VirtualInput_UnRegisterVirtualAxis_mD3511EE52A02EF720B086FF6EDCF9D4FA11A551F ();
// 0x000000DE System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern void VirtualInput_UnRegisterVirtualButton_mF05E241BD753B335E97CB8D1EDCFECE82A34F554 ();
// 0x000000DF UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern void VirtualInput_VirtualAxisReference_m5AE323533C7DF65D71B551B173A63680BB5850EA ();
// 0x000000E0 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern void VirtualInput_SetVirtualMousePositionX_m49716B45CE295686844FDD803083136B9BAC2124 ();
// 0x000000E1 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern void VirtualInput_SetVirtualMousePositionY_m80139449D4E09227D929E314419B1C72D57BD001 ();
// 0x000000E2 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern void VirtualInput_SetVirtualMousePositionZ_m9276A4D39BC31E00C1977B2621549B1C1F40E51D ();
// 0x000000E3 System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean)
// 0x000000E4 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String)
// 0x000000E5 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String)
// 0x000000E6 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String)
// 0x000000E7 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String)
// 0x000000E8 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String)
// 0x000000E9 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String)
// 0x000000EA System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String)
// 0x000000EB System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String)
// 0x000000EC System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single)
// 0x000000ED UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition()
// 0x000000EE System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern void VirtualInput__ctor_mD6A4228D372182ABC7372ED25F4987CE1EAA27CB ();
// 0x000000EF System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern void MobileInput_AddButton_m55B4ECB00F31F0904145B5DC71AE2B7289960F34 ();
// 0x000000F0 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern void MobileInput_AddAxes_mF5065897FC94197F4FD5BDD15A394E858218496E ();
// 0x000000F1 System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern void MobileInput_GetAxis_m24CDEC7DA08736467196B8F90F19B3110782421A ();
// 0x000000F2 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern void MobileInput_SetButtonDown_mF3C9EEAF5750B7CF53C0D6D04D035CA8F1D27547 ();
// 0x000000F3 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern void MobileInput_SetButtonUp_m0CEDEA05459505931FB2686C20AC0900A4941448 ();
// 0x000000F4 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern void MobileInput_SetAxisPositive_mD4522AE0A5CFA591D720C9FA1E42D38485F66C9A ();
// 0x000000F5 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern void MobileInput_SetAxisNegative_mDB7F89D127295F2D4CC4764EB04571D9A46774C4 ();
// 0x000000F6 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern void MobileInput_SetAxisZero_mCE681BFD720000CFA939C78B0EAEFCA3D5748BA8 ();
// 0x000000F7 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern void MobileInput_SetAxis_m40791B9F5D8B28086FEA1030918A6DDBD96D2704 ();
// 0x000000F8 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern void MobileInput_GetButtonDown_m8B7EC91AD10FF37A6910CC3AD684572F8CC4A403 ();
// 0x000000F9 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern void MobileInput_GetButtonUp_mFB0BF6CE172238F3AF6B28CB16A73B5A0D714ABB ();
// 0x000000FA System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern void MobileInput_GetButton_m1805C5AAFEA6C56E1F083C318C7D8A56414742DC ();
// 0x000000FB UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern void MobileInput_MousePosition_m655B9F793060E92EEAFC358ED5A612124F71B234 ();
// 0x000000FC System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern void MobileInput__ctor_m58D4C2380917920DD39E646CB4717F6EFBAA16F0 ();
// 0x000000FD System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern void StandaloneInput_GetAxis_m208A36BD2256D5439E8BF99DFEE7C4FBE5C321DB ();
// 0x000000FE System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern void StandaloneInput_GetButton_m2156BA026DDB9F6FA9F45BBC8FEC871A3090629C ();
// 0x000000FF System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern void StandaloneInput_GetButtonDown_m3F88DFF900E2AB8729E6F63694BE3C8E2C19BBB7 ();
// 0x00000100 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern void StandaloneInput_GetButtonUp_mDF55E35A4B50D58901CEEF8DAEECD050A636398C ();
// 0x00000101 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern void StandaloneInput_SetButtonDown_m5C1B0E5ED19F91DAEE8A23108865EBC57EB3F002 ();
// 0x00000102 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern void StandaloneInput_SetButtonUp_m23ECA36E7E2C9D79650FC93764E2FB47C52A6269 ();
// 0x00000103 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern void StandaloneInput_SetAxisPositive_m45ABA3A91481B6B07E37A24322C345DE4341472D ();
// 0x00000104 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern void StandaloneInput_SetAxisNegative_m8ABB3422B35FA3D5EFEC9A3BCCE7D813C91E1DDA ();
// 0x00000105 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern void StandaloneInput_SetAxisZero_mC0123C06F1DD19FF6DB9353DF0D711554B56B428 ();
// 0x00000106 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern void StandaloneInput_SetAxis_m620AB31F30F82FDB44EB995E494F618D8C42F765 ();
// 0x00000107 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern void StandaloneInput_MousePosition_m7D158ACA958E8C1101AFF4B3282E12B1A6EF7C82 ();
// 0x00000108 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern void StandaloneInput__ctor_m9D98FDF717857FB17019872D7A8446C224C493F3 ();
// 0x00000109 System.Void UnityStandardAssets.Vehicles.Ball.Ball::Start()
extern void Ball_Start_mD94D5E17C462F68ED882F7A71510571FE3BC99E2 ();
// 0x0000010A System.Void UnityStandardAssets.Vehicles.Ball.Ball::Move(UnityEngine.Vector3,System.Boolean)
extern void Ball_Move_m2876C25E0C756BF245A81AD607EFEF4635163D37 ();
// 0x0000010B System.Void UnityStandardAssets.Vehicles.Ball.Ball::.ctor()
extern void Ball__ctor_mCAF1E03B050453AC083D54A0395370CF4E322D4A ();
// 0x0000010C System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Awake()
extern void BallUserControl_Awake_m3053C00DA15A4F03C606E253DF2F1222233D2CD4 ();
// 0x0000010D System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Update()
extern void BallUserControl_Update_mCF16AA2CFDB461475BEFE7F4A82AD02E27B5A2C9 ();
// 0x0000010E System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::FixedUpdate()
extern void BallUserControl_FixedUpdate_mAFB462499A540A51F3A59B50EACEF013A1FD99C6 ();
// 0x0000010F System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::.ctor()
extern void BallUserControl__ctor_m933AEE908ED128E00AA48445422BAAACE54D27C8 ();
// 0x00000110 UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_agent()
extern void AICharacterControl_get_agent_mC145C43591E7F58C19D6812346E8B267562DCE2A ();
// 0x00000111 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_agent(UnityEngine.AI.NavMeshAgent)
extern void AICharacterControl_set_agent_m3AF797E33435DE57A5B68D4A370B38FF5066AC35 ();
// 0x00000112 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_character()
extern void AICharacterControl_get_character_m5F68CEC317221C119DD993F0BC66E83B46ECAF10 ();
// 0x00000113 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_character(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter)
extern void AICharacterControl_set_character_mB648916EAE530CF42ADB6A2133597CD2C5C351AE ();
// 0x00000114 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Start()
extern void AICharacterControl_Start_m94E0B0C90E7F8680D0BA14C0B7F73F1147F1ECBD ();
// 0x00000115 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Update()
extern void AICharacterControl_Update_m3739D8300A1B4ED83717268FACD69BA35E2F2D60 ();
// 0x00000116 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::SetTarget(UnityEngine.Transform)
extern void AICharacterControl_SetTarget_m85A54D9230685AEFC19FE47B0844545F3AC76ED7 ();
// 0x00000117 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::.ctor()
extern void AICharacterControl__ctor_m64E75FC80DC62E05D71EA1EDE411F5E9118F9895 ();
// 0x00000118 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Start()
extern void ThirdPersonCharacter_Start_m77BED10E07BCE3B9FCDFDAB0A604598E7EF52551 ();
// 0x00000119 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_Move_mC404A566646B48F6E4895C4B56B568B2FCD7B315 ();
// 0x0000011A System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ThirdPersonCharacter_ScaleCapsuleForCrouching_mEF225C57F19EEFB21F3FA5065F835ED295F9FA83 ();
// 0x0000011B System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::PreventStandingInLowHeadroom()
extern void ThirdPersonCharacter_PreventStandingInLowHeadroom_m11B4FCE921A339A567D1DE504C5781738FE130F4 ();
// 0x0000011C System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ThirdPersonCharacter_UpdateAnimator_mA55D3E6EEE3D094A0377F74680EDC955C915C2F3 ();
// 0x0000011D System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleAirborneMovement()
extern void ThirdPersonCharacter_HandleAirborneMovement_mA70C2C3E9776D1F93AC11B00DBB614A653D05713 ();
// 0x0000011E System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_HandleGroundedMovement_m9E47FDBABA1394392BC50B9D0E8F617417DCD8EA ();
// 0x0000011F System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ApplyExtraTurnRotation()
extern void ThirdPersonCharacter_ApplyExtraTurnRotation_m04D602E8910E9F91436DC3978A237DA976E0528A ();
// 0x00000120 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorMove()
extern void ThirdPersonCharacter_OnAnimatorMove_m1B720209EAC4123222D83A2604CDD9DEB35B4807 ();
// 0x00000121 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::CheckGroundStatus()
extern void ThirdPersonCharacter_CheckGroundStatus_m529274699AE7FB0AA68E6259A4009C5119346DD6 ();
// 0x00000122 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::.ctor()
extern void ThirdPersonCharacter__ctor_m2A343C6DA11522069E215C58B8C5F0F2916482B9 ();
// 0x00000123 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Start()
extern void ThirdPersonUserControl_Start_m6E6473ABBBE127E5FED209498ED24663D0612682 ();
// 0x00000124 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Update()
extern void ThirdPersonUserControl_Update_m673BE47FF260D53BF312CAB76F77C32D8CA22629 ();
// 0x00000125 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::FixedUpdate()
extern void ThirdPersonUserControl_FixedUpdate_mDF454F6DBD58CBB0429BBC36502150EF9D360453 ();
// 0x00000126 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::.ctor()
extern void ThirdPersonUserControl__ctor_m7BD727F15D1289F92B24B581C9DDB5A7489A967E ();
// 0x00000127 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Start()
extern void FirstPersonController_Start_m87414ABA8CE33FC0DE5E9856C79A357E501D7308 ();
// 0x00000128 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Update()
extern void FirstPersonController_Update_mF3FC7041AB276DC382BC8A4F61AEAFAFAEA04ED4 ();
// 0x00000129 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::FixedUpdate()
extern void FirstPersonController_FixedUpdate_mBA424DA0A9AA19D6C2E50F798C372AA7543FD918 ();
// 0x0000012A System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::ProgressStepCycle(System.Single)
extern void FirstPersonController_ProgressStepCycle_mF0FA5D7B32881E407247E42A6F07B1F49AB5717F ();
// 0x0000012B System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayFootStepAudio()
extern void FirstPersonController_PlayFootStepAudio_mE2350EC1F9F5D34A04464C32EEBE1E7E82134179 ();
// 0x0000012C System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::UpdateCameraPosition(System.Single)
extern void FirstPersonController_UpdateCameraPosition_m9287722E622F693557E810274D76CF402849E7B5 ();
// 0x0000012D System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::GetInput(System.Single&)
extern void FirstPersonController_GetInput_m20FF731BB9AE80DB53A15A87BB960E7552AF5730 ();
// 0x0000012E System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::RotateView()
extern void FirstPersonController_RotateView_m52AE6EADF85961F72D76454E11913AFE0AB0C77A ();
// 0x0000012F System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void FirstPersonController_OnControllerColliderHit_m359FEB09F912EF3311435D6CF9CAFB25CA6EBDDC ();
// 0x00000130 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::.ctor()
extern void FirstPersonController__ctor_m067AA344391066103FAE6175AFC5C1717B4022F6 ();
// 0x00000131 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Start()
extern void HeadBob_Start_m2956DB87FE33B62FC550D8E8581B1197E790BD8F ();
// 0x00000132 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Update()
extern void HeadBob_Update_mBF51EAFAB23AC5B5E176E02CCCA668AF6B20F4AC ();
// 0x00000133 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::.ctor()
extern void HeadBob__ctor_mD8C44B9DC99CA8E61E23578A15973F48D1B1D077 ();
// 0x00000134 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::Init(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_Init_m6F89547F704698EFB173D980CEC974CD6D11CE1E ();
// 0x00000135 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::LookRotation(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_LookRotation_m12F5371B9F69C6E79A959B1707E2079EF176FE47 ();
// 0x00000136 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::SetCursorLock(System.Boolean)
extern void MouseLook_SetCursorLock_m74E47ED1CDCF73947446BA6F3109C7305B44A27E ();
// 0x00000137 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::UpdateCursorLock()
extern void MouseLook_UpdateCursorLock_mC03FF4763BE1894EDD81D90A96791E644BDA4DF9 ();
// 0x00000138 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::InternalLockUpdate()
extern void MouseLook_InternalLockUpdate_m689C8345609827E54B2C45018C9FA2710641AE41 ();
// 0x00000139 UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLook_ClampRotationAroundXAxis_mB80B11F6AA879942BCC5B59AC99857BEC811CEBB ();
// 0x0000013A System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::.ctor()
extern void MouseLook__ctor_m4991C1F282EDF4C515B9C28106EC981F5D157CF2 ();
// 0x0000013B UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Velocity()
extern void RigidbodyFirstPersonController_get_Velocity_mA3844E469740CF2B014878B8BF192EF385675B28 ();
// 0x0000013C System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Grounded()
extern void RigidbodyFirstPersonController_get_Grounded_mB76301608244EA277030FED9FF07421B1DE37A55 ();
// 0x0000013D System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Jumping()
extern void RigidbodyFirstPersonController_get_Jumping_m7E2EACD44C0241B8423B97E1739DBE269FE204A6 ();
// 0x0000013E System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Running()
extern void RigidbodyFirstPersonController_get_Running_mE86F6DD182214508725455FC918CAF13BC1A462B ();
// 0x0000013F System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Start()
extern void RigidbodyFirstPersonController_Start_m2330F14619B7112B794576FAE87657F51E16F998 ();
// 0x00000140 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Update()
extern void RigidbodyFirstPersonController_Update_mC9D36ED411466D763F3B6726ED0264FE5815D040 ();
// 0x00000141 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::FixedUpdate()
extern void RigidbodyFirstPersonController_FixedUpdate_m81ED45B8FE887F4C512FC12EA0EA4A83AECC370C ();
// 0x00000142 System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::SlopeMultiplier()
extern void RigidbodyFirstPersonController_SlopeMultiplier_m5BC79F85397DC3492513C1AC8088A8E62DE47B37 ();
// 0x00000143 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::StickToGroundHelper()
extern void RigidbodyFirstPersonController_StickToGroundHelper_m1ECAEA0961503D6BBF65AA20D056F4E803C13CA8 ();
// 0x00000144 UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GetInput()
extern void RigidbodyFirstPersonController_GetInput_m4DDAC49710C5FA777D29BF16A154A8B35D6EA8D8 ();
// 0x00000145 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::RotateView()
extern void RigidbodyFirstPersonController_RotateView_mD3A6BE7C73F1839CE62FBECC3A03E53C68AF8711 ();
// 0x00000146 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GroundCheck()
extern void RigidbodyFirstPersonController_GroundCheck_m7843581359DD47F573ADD9059732FA20246C4FC0 ();
// 0x00000147 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::.ctor()
extern void RigidbodyFirstPersonController__ctor_mCCA1D3CC7960FE1BDB756F1CC3BC6D12DC5BC471 ();
// 0x00000148 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern void AbstractTargetFollower_Start_m5830AC9B925EF10A96832CB64E17703537ABA4D6 ();
// 0x00000149 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern void AbstractTargetFollower_FixedUpdate_m059E3F7EE069E830A1675B7E609B2C0992F5C9FE ();
// 0x0000014A System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern void AbstractTargetFollower_LateUpdate_mE6D4BAF43EA1C593A3439507476CAF6C360874F5 ();
// 0x0000014B System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern void AbstractTargetFollower_ManualUpdate_m5FDFBD137B58BE58593D6D751BAA4C4A5134C158 ();
// 0x0000014C System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// 0x0000014D System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern void AbstractTargetFollower_FindAndTargetPlayer_m0A72C9D084555F206759AF9BBC56C039170E497A ();
// 0x0000014E System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern void AbstractTargetFollower_SetTarget_m17CE0F35DAE4E4086CACD31BA14C05DA2589D3A9 ();
// 0x0000014F UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern void AbstractTargetFollower_get_Target_m24234F14398E0958CEA1A476F649E762D6E17814 ();
// 0x00000150 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern void AbstractTargetFollower__ctor_m7CA75F5DEB95588875BF359F17F9A45F1AC28E18 ();
// 0x00000151 System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern void AutoCam_FollowTarget_m49D07251C6021E9F838CC8747A0CF83EFCEE8552 ();
// 0x00000152 System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern void AutoCam__ctor_mD9E4154D81A29B6C8C68297374653FB9CFAB3AC2 ();
// 0x00000153 System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern void FreeLookCam_Awake_m9EAF9CF5A48AE92736DB798522FFFA95DF0C33A0 ();
// 0x00000154 System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern void FreeLookCam_Update_mD83A18376B2B5F9970C8DA78E60DB12C1B508AFC ();
// 0x00000155 System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern void FreeLookCam_OnDisable_mF4EE05B50BF94C52C65ABE1F26A383F3F614DE54 ();
// 0x00000156 System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern void FreeLookCam_FollowTarget_mADE713C6EA3D2A61B6FEFBAC2226C9519728478C ();
// 0x00000157 System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern void FreeLookCam_HandleRotationMovement_m835C5DFA2FD21F7C64057BDB0BAEA62B99A2D404 ();
// 0x00000158 System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern void FreeLookCam__ctor_mB38818588467BA1204288BB062BBAABDCC0225D9 ();
// 0x00000159 System.Void UnityStandardAssets.Cameras.HandHeldCam::FollowTarget(System.Single)
extern void HandHeldCam_FollowTarget_m9BF734BC3214DBB892E0D2D507E2F9452B147FDB ();
// 0x0000015A System.Void UnityStandardAssets.Cameras.HandHeldCam::.ctor()
extern void HandHeldCam__ctor_m57D82D85C30C87D03E7EAB05EBDF656962DA2AB5 ();
// 0x0000015B System.Void UnityStandardAssets.Cameras.LookatTarget::Start()
extern void LookatTarget_Start_m55D1DA54A4762FF2AF364421AED7C54396705CBA ();
// 0x0000015C System.Void UnityStandardAssets.Cameras.LookatTarget::FollowTarget(System.Single)
extern void LookatTarget_FollowTarget_m363133638041D45CAB46C88DD61768E4E39DF534 ();
// 0x0000015D System.Void UnityStandardAssets.Cameras.LookatTarget::.ctor()
extern void LookatTarget__ctor_m765D6EC2839C0E8BDF17F849E28A0D472A37D25D ();
// 0x0000015E System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern void PivotBasedCameraRig_Awake_m59237BF4FB7603169D6E12D95C372D7A889E1F27 ();
// 0x0000015F System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern void PivotBasedCameraRig__ctor_m3A67ED42B196F44E73ECF164084D9655F6BE700F ();
// 0x00000160 System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern void ProtectCameraFromWallClip_get_protecting_m1E2FFDFDBBEEA74AD539E8F35522047B7F14F9FB ();
// 0x00000161 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern void ProtectCameraFromWallClip_set_protecting_m23B7F1BBEE76FDAFF187820178B1CDAE2A326F55 ();
// 0x00000162 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern void ProtectCameraFromWallClip_Start_mFD2770D700A02F262AF9926E4C5828B0C8AC544C ();
// 0x00000163 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern void ProtectCameraFromWallClip_LateUpdate_mA71300721FF34772D041EC217AA182F5C9FFEDDC ();
// 0x00000164 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern void ProtectCameraFromWallClip__ctor_m9BE30002DD3BC07052D7E11A448D6A1330E822A9 ();
// 0x00000165 System.Void UnityStandardAssets.Cameras.TargetFieldOfView::Start()
extern void TargetFieldOfView_Start_mCBB06309FF6E440654164EDF0557C46AA5C673E7 ();
// 0x00000166 System.Void UnityStandardAssets.Cameras.TargetFieldOfView::FollowTarget(System.Single)
extern void TargetFieldOfView_FollowTarget_mA92F8333656949ACB3D8A4C383613C2B5791A5FB ();
// 0x00000167 System.Void UnityStandardAssets.Cameras.TargetFieldOfView::SetTarget(UnityEngine.Transform)
extern void TargetFieldOfView_SetTarget_mFBC0E9995E9CDF97FD74AAC1370400F5FCAB3373 ();
// 0x00000168 System.Single UnityStandardAssets.Cameras.TargetFieldOfView::MaxBoundsExtent(UnityEngine.Transform,System.Boolean)
extern void TargetFieldOfView_MaxBoundsExtent_mBAAA9C398FF2E1C3F6F80FF5BC51D40CD445E471 ();
// 0x00000169 System.Void UnityStandardAssets.Cameras.TargetFieldOfView::.ctor()
extern void TargetFieldOfView__ctor_m7F1799AEF5CD5A646C15D7C92B50668263FF525E ();
// 0x0000016A System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch_ReplacementDefinition::.ctor()
extern void ReplacementDefinition__ctor_m49E65F984C7161F3C2C5DD51172D8C9624AD31E5 ();
// 0x0000016B System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch_ReplacementList::.ctor()
extern void ReplacementList__ctor_mEC91B69367F2501099D36FD66A5715B772DAACCB ();
// 0x0000016C System.Void UnityStandardAssets.Utility.AutoMoveAndRotate_Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_mFE5D5C1F196B500E04A45EC3230CE4FBD138F894 ();
// 0x0000016D System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::.ctor(System.Int32)
extern void U3CDragObjectU3Ed__8__ctor_m5ED5AC5E5AB22F2FB9178DDCC2D99A933CB2C211 ();
// 0x0000016E System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.IDisposable.Dispose()
extern void U3CDragObjectU3Ed__8_System_IDisposable_Dispose_m011A5F292C63CAACDE3E483BE60B65308D1E1CE4 ();
// 0x0000016F System.Boolean UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::MoveNext()
extern void U3CDragObjectU3Ed__8_MoveNext_mDC1BCC4C79BDB25C44A9DC9735D48FA2E175E071 ();
// 0x00000170 System.Object UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m043FAB9E9E779B7ED3968BD00C707E31D47CA661 ();
// 0x00000171 System.Void UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_mC00266C3A2C2B2C5CAA743701D45DC345EC96706 ();
// 0x00000172 System.Object UnityStandardAssets.Utility.DragRigidbody_<DragObject>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m26F191AB7590668B6FE2A7D0F9D5ABC771B90EA0 ();
// 0x00000173 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::.ctor(System.Int32)
extern void U3CFOVKickUpU3Ed__9__ctor_m2E70E0F14634E8A3208961FEA2B1664230D5757C ();
// 0x00000174 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.IDisposable.Dispose()
extern void U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_mD4B00A512ECC65464CEF2F971595D3B28032A8A2 ();
// 0x00000175 System.Boolean UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::MoveNext()
extern void U3CFOVKickUpU3Ed__9_MoveNext_m512DBF15F770463827161931918513ECBB78759E ();
// 0x00000176 System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59D92574D53698089582F9A0BE55EB4A9B9B256 ();
// 0x00000177 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_m09B3619F760A7A100802FBC2C44718B3AABD19CE ();
// 0x00000178 System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickUp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mBA658BB1928272A6E7E098ECD397E77C191D04FC ();
// 0x00000179 System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::.ctor(System.Int32)
extern void U3CFOVKickDownU3Ed__10__ctor_mAC26AAD43FBF2CEF4164498556AE1B030837944F ();
// 0x0000017A System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.IDisposable.Dispose()
extern void U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_mA336F42BB92EDDD223120F28B0A13EF29F9FF9C7 ();
// 0x0000017B System.Boolean UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::MoveNext()
extern void U3CFOVKickDownU3Ed__10_MoveNext_mF35885E2D181C34AA25B41E9212A468D59DE456C ();
// 0x0000017C System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72EA85FAA1FD7B04B65BB1C719D6472A9B8EC0EC ();
// 0x0000017D System.Void UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_mD9005227142121AFF35782081F84B5AB7F8FD579 ();
// 0x0000017E System.Object UnityStandardAssets.Utility.FOVKick_<FOVKickDown>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFD66D6F9B957FC0522A35FE0D3662667B896A08C ();
// 0x0000017F System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::.ctor(System.Int32)
extern void U3CDoBobCycleU3Ed__4__ctor_m3845D1AEC7D920CC91A343EECDAFADD29AA364DF ();
// 0x00000180 System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.IDisposable.Dispose()
extern void U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m337964E05B4A29A899562BA187A6C43AA915FC03 ();
// 0x00000181 System.Boolean UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::MoveNext()
extern void U3CDoBobCycleU3Ed__4_MoveNext_m9452FC51F84697BCD114A5791B3137BA34CD2DE3 ();
// 0x00000182 System.Object UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A882F4C449D3A6AFE38E54266A62CCE9FEB71C ();
// 0x00000183 System.Void UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m2F0D716C86EF00BBFB2E9150ECA135004FEAC486 ();
// 0x00000184 System.Object UnityStandardAssets.Utility.LerpControlledBob_<DoBobCycle>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m7BBD42AB52B89D8F7451CC6A79BA3C660BA93392 ();
// 0x00000185 System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::.ctor(System.Int32)
extern void U3CResetCoroutineU3Ed__6__ctor_mE23E11148F2C3B9148F5CF189984AE4CD71E8F8C ();
// 0x00000186 System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m8E5DEBEC17DEB14B5B28C805C11BD627FDAEF4A3 ();
// 0x00000187 System.Boolean UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::MoveNext()
extern void U3CResetCoroutineU3Ed__6_MoveNext_m62B0E26E3294631050E345318DDC863C31EFAA26 ();
// 0x00000188 System.Object UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59F7FC20AC24226410D75D0B8D5DA40BEEC6C1A1 ();
// 0x00000189 System.Void UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mDAC82C73DF62065BCD4C1CB44848A55185A3987E ();
// 0x0000018A System.Object UnityStandardAssets.Utility.ObjectResetter_<ResetCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m432D1DC8AECAC892944242B8ADDF7CCD6FAD9E9F ();
// 0x0000018B System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mD0A1B215E5F066646BCAF66C33A5F48D0989E090 ();
// 0x0000018C System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m1FE667ACD1B0DC25165D56369A3CF8733C0B206A ();
// 0x0000018D System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m7AB511BE787031F2A04E72326E49E7B5F3B208C9 ();
// 0x0000018E System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F071490E4E71A9A719D10DAAF032F74B6B31363 ();
// 0x0000018F System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m32B0CA08CBECCFC5DEF3C43032A5658A24D3C7C0 ();
// 0x00000190 System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m3E718F7631CCED3184D3AB437310A802E3F5FAF0 ();
// 0x00000191 System.Void UnityStandardAssets.Utility.TimedObjectActivator_Entry::.ctor()
extern void Entry__ctor_m593D5A5177E927D55B4EDEA893C9A89DA0E3683C ();
// 0x00000192 System.Void UnityStandardAssets.Utility.TimedObjectActivator_Entries::.ctor()
extern void Entries__ctor_m9FF702A552EFD09D5146A438883B9D3267428151 ();
// 0x00000193 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::.ctor(System.Int32)
extern void U3CActivateU3Ed__5__ctor_m04B00CBAD8695D22C759B14B4228011F72884E9F ();
// 0x00000194 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.IDisposable.Dispose()
extern void U3CActivateU3Ed__5_System_IDisposable_Dispose_m6F5CC0A9E69B2ABB18B2F15DC8F1575F3899E175 ();
// 0x00000195 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::MoveNext()
extern void U3CActivateU3Ed__5_MoveNext_mF97B09D381486D8A6E40E66208EF442D65683980 ();
// 0x00000196 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8461E4FE3B48538BDB9C8FCA99CFB50C4D46C8 ();
// 0x00000197 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.IEnumerator.Reset()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m10D07A5F6216561973401AE2344F661E0C7E520C ();
// 0x00000198 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Activate>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_m013FDADA4980E415D472066957CB1D762B166B8B ();
// 0x00000199 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::.ctor(System.Int32)
extern void U3CDeactivateU3Ed__6__ctor_m43C0B814DE2C6DF5F445D122620F750158D95A9B ();
// 0x0000019A System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.IDisposable.Dispose()
extern void U3CDeactivateU3Ed__6_System_IDisposable_Dispose_mB5C406251AEA37C27D789602F621349E95B5077E ();
// 0x0000019B System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::MoveNext()
extern void U3CDeactivateU3Ed__6_MoveNext_m690EC1B5BB66A6A7DEA9B82A9EC24767FB669A06 ();
// 0x0000019C System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9DD75E5BEF9E464B4366FB4A5BBFA6C9624FD1 ();
// 0x0000019D System.Void UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C179259768EC874E6C752D62F84805BDC036AA3 ();
// 0x0000019E System.Object UnityStandardAssets.Utility.TimedObjectActivator_<Deactivate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1F491BC143C92AF323241F437B1854040FBB960B ();
// 0x0000019F System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::.ctor(System.Int32)
extern void U3CReloadLevelU3Ed__7__ctor_m89F904013A652554E256373377BF93992BA1205E ();
// 0x000001A0 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.IDisposable.Dispose()
extern void U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_m4EAEA10A25D3BCD8C4A41A7C305673C2702021A9 ();
// 0x000001A1 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::MoveNext()
extern void U3CReloadLevelU3Ed__7_MoveNext_m22DFAE90E48E5E9AE2E064903D8AA47C08EB9B48 ();
// 0x000001A2 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F1B7ED565E25B37FC48A22BDB4378F714F5B3A1 ();
// 0x000001A3 System.Void UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.IEnumerator.Reset()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_mF3AB1DBCA692E293D77D4D9294D0BC47CCB22CFB ();
// 0x000001A4 System.Object UnityStandardAssets.Utility.TimedObjectActivator_<ReloadLevel>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_m62C25C33793EE76CA6A5FA59EC19F7EE078C4BD1 ();
// 0x000001A5 System.Void UnityStandardAssets.Utility.WaypointCircuit_WaypointList::.ctor()
extern void WaypointList__ctor_m6F4EDE811589F476A95CC3ECCA3BE59C54AF753A ();
// 0x000001A6 System.Void UnityStandardAssets.Utility.WaypointCircuit_RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RoutePoint__ctor_m0CF428B188D139A371423658C3A41B96137CDA09_AdjustorThunk ();
// 0x000001A7 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls_DemoParticleSystem::.ctor()
extern void DemoParticleSystem__ctor_mE2CE12D47F99F9170148BFB896C67AE7A9A73C65 ();
// 0x000001A8 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls_DemoParticleSystemList::.ctor()
extern void DemoParticleSystemList__ctor_mDD1D4D7FC5AF577A1F1FC662575170B15ACA4650 ();
// 0x000001A9 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C ();
// 0x000001AA System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01 ();
// 0x000001AB System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953 ();
// 0x000001AC System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F ();
// 0x000001AD System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07 ();
// 0x000001AE System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2 ();
// 0x000001AF System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m28BA84EE932E5505BA7BD35AC5F8007BA8B96564 ();
// 0x000001B0 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m2411BD36258EF1A6EF59D488D875E1989C3BAE1C ();
// 0x000001B1 System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m69D4364C306F1FC8F21C33E29E6C125B2B9EEF2B ();
// 0x000001B2 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m600B8E8FC9018CA7E4700F80CF7114DBD2A89C0F ();
// 0x000001B3 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m8BE9B7F5466A03EDD437E3D16EB3E659B51ABC0E ();
// 0x000001B4 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce_<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m60734E8242F26A6B6ADF449A1668715F5E393E00 ();
// 0x000001B5 System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::.ctor(System.Int32)
extern void U3COnCollisionEnterU3Ed__8__ctor_mE0412A81997D86406428B1A95EEDF564D6ED49E0 ();
// 0x000001B6 System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.IDisposable.Dispose()
extern void U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_mE7DC56D7449DC66BB04E6EC60C7B143779E4AEB9 ();
// 0x000001B7 System.Boolean UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::MoveNext()
extern void U3COnCollisionEnterU3Ed__8_MoveNext_m53C4680C5CFB7DB025A0854EDFBA101B1C74C851 ();
// 0x000001B8 System.Object UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C70CC61071F2B35953F4283AA505C55E6940F69 ();
// 0x000001B9 System.Void UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m62403746C385587687DAFDAACBD369E71159685F ();
// 0x000001BA System.Object UnityStandardAssets.Effects.Explosive_<OnCollisionEnter>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m353A3041DBEED2FEBA3CCC26222620D9908BC733 ();
// 0x000001BB System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_name()
extern void VirtualAxis_get_name_mC3959CD36494EE1B06CAEA1675DD19E5FFCB9BD9 ();
// 0x000001BC System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::set_name(System.String)
extern void VirtualAxis_set_name_m2A44E0BF21BB426C9A14AB057D5EF41616B76096 ();
// 0x000001BD System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_matchWithInputManager()
extern void VirtualAxis_get_matchWithInputManager_m2F68784B6C454EB26934401303E28C159980F315 ();
// 0x000001BE System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::set_matchWithInputManager(System.Boolean)
extern void VirtualAxis_set_matchWithInputManager_m326813FB9C39A5D63C98D4AE931384D6B67AE944 ();
// 0x000001BF System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::.ctor(System.String)
extern void VirtualAxis__ctor_m9B094B00B2F0F1C6C474D3DA51419F4549540E53 ();
// 0x000001C0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::.ctor(System.String,System.Boolean)
extern void VirtualAxis__ctor_mAC45A3BC043EA253666CCDE2762DB39475FED915 ();
// 0x000001C1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::Remove()
extern void VirtualAxis_Remove_m0517C6C37E94CCC84337FD412982D1800E5CEFD6 ();
// 0x000001C2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::Update(System.Single)
extern void VirtualAxis_Update_m639BD6EC869B61C712D4519290523C61745FF6C3 ();
// 0x000001C3 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_GetValue()
extern void VirtualAxis_get_GetValue_mB0D352473A7E1F6A9402335FBD18625ADFCE0A69 ();
// 0x000001C4 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::get_GetValueRaw()
extern void VirtualAxis_get_GetValueRaw_mA75834F100AB39C130FEA7AE85677E4928E58397 ();
// 0x000001C5 System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_name()
extern void VirtualButton_get_name_m836058DAC831C5BB481A422120939EB4D14CE55B ();
// 0x000001C6 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::set_name(System.String)
extern void VirtualButton_set_name_mCC77CE771C89C23B47A2D9B027C7E754666A78A5 ();
// 0x000001C7 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_matchWithInputManager()
extern void VirtualButton_get_matchWithInputManager_mD6924A44FFCFF72519BDDEAD61E3072CC3C3FCF3 ();
// 0x000001C8 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::set_matchWithInputManager(System.Boolean)
extern void VirtualButton_set_matchWithInputManager_mD438AFD4E212727BED9ECD1F0CBFE6243112AE3D ();
// 0x000001C9 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::.ctor(System.String)
extern void VirtualButton__ctor_mECADC4A0B8ACF0954720A84061800EA0F00D9FDD ();
// 0x000001CA System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::.ctor(System.String,System.Boolean)
extern void VirtualButton__ctor_mBC57649412C90DFF3179B681B9D33BB88443FFD9 ();
// 0x000001CB System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Pressed()
extern void VirtualButton_Pressed_m596B075C829D1E8C500AF6694155488CF2250402 ();
// 0x000001CC System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Released()
extern void VirtualButton_Released_mC4B98C45864A5832601A90437E691119F28E25E6 ();
// 0x000001CD System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::Remove()
extern void VirtualButton_Remove_m0F66A404819C8B483DA3F02FDCEBDB005867D37D ();
// 0x000001CE System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButton()
extern void VirtualButton_get_GetButton_m228F811AD3C4911C45AFEA7960E35F4A84B7A32D ();
// 0x000001CF System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButtonDown()
extern void VirtualButton_get_GetButtonDown_mB6BBC9E21BB477279E5D74926CFA633E671AC430 ();
// 0x000001D0 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::get_GetButtonUp()
extern void VirtualButton_get_GetButtonUp_m79C31A03EE6AC926E932FA1A28989A73B0257E43 ();
// 0x000001D1 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping::.ctor()
extern void AxisMapping__ctor_m2B8C914999C51C9568C81B4C1E6750BCAF66BE1F ();
// 0x000001D2 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_MovementSettings::UpdateDesiredTargetSpeed(UnityEngine.Vector2)
extern void MovementSettings_UpdateDesiredTargetSpeed_m92BE3A22C9CF55BF12A0D441EBB51FF7A1779D69 ();
// 0x000001D3 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_MovementSettings::.ctor()
extern void MovementSettings__ctor_mF768FC3EE16A9A355B9FCD47EF76C8A4879FAF29 ();
// 0x000001D4 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController_AdvancedSettings::.ctor()
extern void AdvancedSettings__ctor_m845AB1B94472E573D2C5DD585876E67B97C0AD23 ();
// 0x000001D5 System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip_RayHitComparer::Compare(System.Object,System.Object)
extern void RayHitComparer_Compare_m6EFBFB91A4F97CC5D2111878BCD211DCEA51DAE3 ();
// 0x000001D6 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip_RayHitComparer::.ctor()
extern void RayHitComparer__ctor_m2A11DF0646D70F2DAFC0751CD63F55A40D397D11 ();
static Il2CppMethodPointer s_methodPointers[470] = 
{
	CameraSwitch_OnEnable_m328899CF90E468B9BCA712951BE98D6BD12E22E6,
	CameraSwitch_NextCamera_mA9928DD3F1556FBD87C28517BFF30F97061DD3DE,
	CameraSwitch__ctor_mC59348604BF2EA0F8B95C411FB72D433805DA29D,
	LevelReset_OnPointerClick_m51DF1E315BB3E84074EC3BED6D371A390481BA54,
	LevelReset_Update_mD312970743CBF30D43B86B8FBD8532D9370D131D,
	LevelReset__ctor_mF67827549AD897B8D088F6EB7A59D3EC1E201187,
	AlphaButtonClickMask_Start_mEFF14FD0C0E68631370255F5936C41963983C8F0,
	AlphaButtonClickMask_IsRaycastLocationValid_m4E83446D8B35549DE9EEE9F6D4548A527A3C884D,
	AlphaButtonClickMask__ctor_m5D0B36A0D207C1AD45B2434C1036141F3D9EA28D,
	EventSystemChecker_Awake_mADD5BA9635C9ADBE91C44BC15DFFCB87798377EB,
	EventSystemChecker__ctor_m52A33F6E257A581FC40521C4CE4B484D2426DA66,
	ForcedReset_Update_m35CBD926C48BD4ACCDD3CB14E0813D04F0E83FC0,
	ForcedReset__ctor_mF32F8C351B95343959FD07B1FF0EC0FBC8CB8EE0,
	ActivateTrigger_DoActivateTrigger_m91589CEC5598200BE6AC01C6D8ED7CD3C05ACF03,
	ActivateTrigger_OnTriggerEnter_mBB7C6EBA79177C6CBEB136C565F17135353319DB,
	ActivateTrigger__ctor_mE845995D67483BC443AE11D6EB0D4E133358EF15,
	AutoMobileShaderSwitch_OnEnable_m2CC4D934531213CC59B29149430BE5454F220327,
	AutoMobileShaderSwitch__ctor_m1D46DC3555ED91D57F71F19BD08DB1F72AA8928F,
	AutoMoveAndRotate_Start_m8A0766F7DA35E226BEC81C38DC800B7C2395E654,
	AutoMoveAndRotate_Update_m23C3C317C277A79CF82C88044544258DD7201411,
	AutoMoveAndRotate__ctor_m97BEA90D4085E63DA4D810FED6F495216024AC6B,
	CameraRefocus__ctor_mAEC82E5DC336BB11096300A567B554CF5235781C,
	CameraRefocus_ChangeCamera_mD34A981CD7F9E0F4CB3DB5A3C929A969E0FFC538,
	CameraRefocus_ChangeParent_m95B7671F5C921BACD887A784A798B9C373C107FD,
	CameraRefocus_GetFocusPoint_m708CC6CD84E14B6285E4404388F0369B9A550C40,
	CameraRefocus_SetFocusPoint_m07D61D09C19C5F951C126B32B3341733C33C20BF,
	CurveControlledBob_Setup_m2007E3FF9FB757CC191CEA557786000FDD658477,
	CurveControlledBob_DoHeadBob_m00B658784E0C963606ADCAB3E3FA9C347F305C6F,
	CurveControlledBob__ctor_mC6A43DC3D1DD13D04AC060CCB621CCF90965BC90,
	DragRigidbody_Update_m83FBCA60844264E0D9544B4BA94D8EE3590B1055,
	DragRigidbody_DragObject_m2BF373EA880D49253F3C5BB726D8724B2288A80C,
	DragRigidbody_FindCamera_mAF1386C508DAEB88A9C5FD4D456EF4D4BE617D9F,
	DragRigidbody__ctor_m6E05682AB45C4BFE0DE11A644D612926F9E76E7D,
	DynamicShadowSettings_Start_m77EC175AF947D4CD8BE908FEAFA761CB247E6D78,
	DynamicShadowSettings_Update_mCA0BBCA7CA61B468857533B4C7896AFABBF9175C,
	DynamicShadowSettings__ctor_mDC8B0D663149C35F242EADFEFED8BA7D4DBCBC77,
	FOVKick_Setup_m8469A5705E531E55305A58D3AA3265FA265D33F6,
	FOVKick_CheckStatus_m9A63EE82724EA159E55AC55E840B4CE3701BE714,
	FOVKick_ChangeCamera_m3218EF587B033155B743FE279EDCB6CF1CDF4C6F,
	FOVKick_FOVKickUp_m91F264E5432BCE86E9082C169D277E64C481D236,
	FOVKick_FOVKickDown_m8BCEE7A586E2B8FAD73F8D8F5D2FB0808A1EEB75,
	FOVKick__ctor_m3D6A4FBAE68243DB82771B3772735848FA090280,
	FPSCounter_Start_mA01345F424F9731649187CC62E9E69FF0377EB65,
	FPSCounter_Update_m8E1247E9BA50C20B0439DF5CA383621EB7B9FFE5,
	FPSCounter__ctor_m3458B72F16625D58BEFC538E933635817205602D,
	FollowTarget_LateUpdate_m5ADBC5E3D58826659C01A2C4F8968A595F092435,
	FollowTarget__ctor_m7ADC7081450A6DA01AE0B5CA1DB84BE961D5DD29,
	LerpControlledBob_Offset_mCC7467CE1E66FB6246E52C5984D1C7BE2C0BF1B2,
	LerpControlledBob_DoBobCycle_mB5C14AC5AEF7114F4A575E19B36BEDF0DE691923,
	LerpControlledBob__ctor_m8E3865BC67563143CFF3C791D0BBFC25D94B18BE,
	ObjectResetter_Start_mF14852C85D595D52F35F221F45A822606EA013B7,
	ObjectResetter_DelayedReset_mCFE31FD35B3AC724EFD1E76270A1FE3FD5014394,
	ObjectResetter_ResetCoroutine_mD46D3A049C28C60DB794613257CC61C5102B0EFC,
	ObjectResetter__ctor_mD14550E617FC16A6C1338BE7D18C5B01141A6354,
	ParticleSystemDestroyer_Start_m28FC4048F585ACE30B2FB2B6839F2801F8FFE584,
	ParticleSystemDestroyer_Stop_mC007423F42F57A16264A616C6970FF62E3A37617,
	ParticleSystemDestroyer__ctor_m3A484EB728A7ECF87E3BF235EF10F4EF49E11623,
	PlatformSpecificContent_OnEnable_m3C87AD39C8B588F5FBA2C7D2808C717A238A5BFB,
	PlatformSpecificContent_CheckEnableContent_m6C285401B4F1F4BFC5E748C7753096628CDAB355,
	PlatformSpecificContent_EnableContent_mF21760A340E974403AD8731E6731AD1BD6B6C73C,
	PlatformSpecificContent__ctor_m8464E7FB25A5C1EC2C1343995EC02402AA72804F,
	SimpleActivatorMenu_OnEnable_m860A5963E1D18B5F5F870A5286F93FE49E55E7CF,
	SimpleActivatorMenu_NextCamera_m0488F3D0A808FE4A11C85125ED15092E910CFE96,
	SimpleActivatorMenu__ctor_m82227309D300C3ED3D119D6D063E19D5A56C7D18,
	SimpleMouseRotator_Start_m8D06607C94F05C2C99A1D74A1D75AA716B1FE80E,
	SimpleMouseRotator_Update_m43E994D26B1A1396F1C87F119B313442201E7222,
	SimpleMouseRotator__ctor_mC1B6E0FDF0226BE88DF307078BD8EE4E5DC546B8,
	SmoothFollow_Start_mDCCB4CDC6353E65906B648B5AF23AB18D2E51C72,
	SmoothFollow_LateUpdate_m7F7C460DDC3C80A3016F8050F50F54C6D55C5C15,
	SmoothFollow__ctor_m3BA62097DC88A8513EFFEFCF2D5CD8D3DB7B548F,
	TimedObjectActivator_Awake_m79DD17D62A8055E1E336A30C26DB80C4D5407289,
	TimedObjectActivator_Activate_m12334B3127ED32333F52AD3DE31A5ABC8EA4D415,
	TimedObjectActivator_Deactivate_m6A966711DD7B1088A53D5CAC48760661834A3979,
	TimedObjectActivator_ReloadLevel_m80D86EA368E03010F6065856EF104F53D4FAB590,
	TimedObjectActivator__ctor_m26FCB0BA5A4631EC3F435E0182C9C4E702B1EAD6,
	TimedObjectDestructor_Awake_m76424E72B7BE0F6424D18BEF93CAB84DBAAC34A6,
	TimedObjectDestructor_DestroyNow_mE93B19E7C626189327CE5300D972C0BDBBE04851,
	TimedObjectDestructor__ctor_mE45D4C3E5E22221059FB74DF5134F06171EFDE6D,
	WaypointCircuit_get_Length_mFA587B16273CB5FD90CC3BD7479A2810B4D318F8,
	WaypointCircuit_set_Length_m39E0ED03A71FEC5DA4B5851069C677BB5A8A657B,
	WaypointCircuit_get_Waypoints_m95D546B796B966FA6ADB8AE7B553F25CCF548211,
	WaypointCircuit_Awake_m3443AE18FBD735BE48A510AD2B277A86CEB1CC52,
	WaypointCircuit_GetRoutePoint_m8D42746EC9609D8082B0E7CC8E19A2C4F27800B0,
	WaypointCircuit_GetRoutePosition_m0973D453CE3244E6E1066641A81AD3FF0EADBB6F,
	WaypointCircuit_CatmullRom_mD59EEE7A548ACAE81B8650873BCE7547CEA63077,
	WaypointCircuit_CachePositionsAndDistances_m69B6E9F8A8CA46B05F025FD09309FC126FCF6F46,
	WaypointCircuit_OnDrawGizmos_m69BAAF20DC9C4E903B5BA88D0DE0D30D95456287,
	WaypointCircuit_OnDrawGizmosSelected_m07DB42CB844CC4517075DF5AFFDC5F2AAFDE8134,
	WaypointCircuit_DrawGizmos_m614C51310D7116AECB903F6CA4A3047F819C9520,
	WaypointCircuit__ctor_m349B1FD72D470FC31D2C7D18D0625C3DE68C9304,
	WaypointProgressTracker_get_targetPoint_mF25566A72CA099496F6C921209690D340E7E1067,
	WaypointProgressTracker_set_targetPoint_m9FF276072D75E5B62BDFDD69FE2BC764B332E636,
	WaypointProgressTracker_get_speedPoint_mD1DA662D3473D8A387FA4B9217E024C9F86ECE32,
	WaypointProgressTracker_set_speedPoint_m5CCCC560F087199E1A30537AF62CAAE4EFDA1062,
	WaypointProgressTracker_get_progressPoint_m2CBA2DD7271E5B4DCBEDE3D4A583DDCBAF7843CD,
	WaypointProgressTracker_set_progressPoint_mDB352CD4E635034EF5155F6661F200CCDE51B5F7,
	WaypointProgressTracker_Start_m3AE39D9788836A78979E71F8F6BC3109BD843341,
	WaypointProgressTracker_Reset_m71BA52616EDE5E48445AAC8163F57CD90A5B3602,
	WaypointProgressTracker_Update_mCF073672622D91EF9F70ED8E870A6E4400ABD694,
	WaypointProgressTracker_OnDrawGizmos_mAE2FA8689934F94640161CFA136411E825660EC9,
	WaypointProgressTracker__ctor_m6832B310DE7A3327A4AC027F4CD32D1D47D33722,
	ParticleSceneControls_Awake_m139756B596B52BFC8B59A49CE1D480F2F75C4CA5,
	ParticleSceneControls_OnDisable_mF56709D90F15CB0D24A732D4AA05B21B2337BE98,
	ParticleSceneControls_Previous_mAFB64030DC9B8850C6515E49A7DF5FEF47B87EF5,
	ParticleSceneControls_Next_m6B4A614F24EC9A11EB86A9DC0195DFA2558AFDB2,
	ParticleSceneControls_Update_m859C7099E3ECFE587B85438B855CEBD33D94C9C2,
	ParticleSceneControls_CheckForGuiCollision_mE600635956C49D0B018BE5BB365439812AD3341F,
	ParticleSceneControls_Select_m0700DE6D1A461460931B138FAD3F8F0B4A89E1DC,
	ParticleSceneControls__ctor_mFC35877F8E88194D78E79EFA0465B1C42F440AC7,
	ParticleSceneControls__cctor_m3C39F6683C76F97D763656FF60A95F908776209D,
	PlaceTargetWithMouse_Update_mB6E4469ABC90B39EFC5ABD14C87476390371B5EA,
	PlaceTargetWithMouse__ctor_m4742E4E69EB09CE65BA92043B4F6634891A5511F,
	SlowMoButton_Start_mEC813F697FF6A73D17788C1E1C2D4AEDF900D9B6,
	SlowMoButton_OnDestroy_m7935F8679D6AB5D66386D76D95A46EB0BCC93758,
	SlowMoButton_ChangeSpeed_m32B57883D33DBF82AD8C56EC7DE1517E905A7ED3,
	SlowMoButton__ctor_mA1B5306C33E6C4EBA9A440BEA3489D6BB602DBAF,
	AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE,
	AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A,
	AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3,
	AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0,
	ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22,
	ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B,
	ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E,
	ExplosionPhysicsForce_Start_m22750184987B8A375DA674EF615F5ECD1DAD04C5,
	ExplosionPhysicsForce__ctor_m3A4FFA852D5C736E4CE47A5A5A3E0CA1E954F660,
	Explosive_Start_m500D588F6ABB1C03488B93AE78086A368943CDE2,
	Explosive_OnCollisionEnter_mB88EB7BF1526BC0F9418D997273E1A1C875D00E1,
	Explosive_Reset_m878D1F8DF175604AFB07827C7A4DA9DA20FFA9DA,
	Explosive__ctor_m21EF3DDCC8F02804ED974F48197F95DB5B4900F4,
	ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39,
	ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8,
	ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068,
	FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D,
	FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7,
	FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776,
	FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE,
	Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939,
	Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094,
	ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D,
	ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC,
	SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6,
	SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C,
	WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792,
	WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70,
	WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3,
	AxisTouchButton_OnEnable_mCB6F2E22CE0ED2867462D2B6477319CA34EC6923,
	AxisTouchButton_FindPairedButton_m40E5A08627D81FC2C9B18410E0A315A00AFB8E5E,
	AxisTouchButton_OnDisable_mA60CC8A5ACA0AF8EA245F67C774CC15489D26F0D,
	AxisTouchButton_OnPointerDown_mF8ED454A7629EC10D5D65B0806B1575B969CC151,
	AxisTouchButton_OnPointerUp_m47ACA384B0AB0496024E2BA07670DE7F416442A6,
	AxisTouchButton__ctor_m92FC868F9C30069B6E82AEFB601E72D7958146EB,
	ButtonHandler_OnEnable_mF4257EC750A191164B42AA7ED19E95221E0C9084,
	ButtonHandler_SetDownState_m22A563B85C0CFC7883586FE49D47EC8F4795740E,
	ButtonHandler_SetUpState_m20D0A07A6047D866EB95EB5D32D6D5C208CF779D,
	ButtonHandler_SetAxisPositiveState_m2914614CA82F6CE2BCDEF41198D6505C08692F60,
	ButtonHandler_SetAxisNeutralState_m710F8A130253345840FC6A942453EBD386322691,
	ButtonHandler_SetAxisNegativeState_m57D4FA1CCB8E9F4CCB3D5AB589D57328EBDA782C,
	ButtonHandler_Update_m84201A612F81A8C08E1E4F78635C47098A9C6BC0,
	ButtonHandler__ctor_m85B4D66D1CA83E5A73BFC70CAFB4F2D00F559795,
	CrossPlatformInputManager__cctor_m62BA59D81597A25895815DDB40DD8944A6B804F2,
	CrossPlatformInputManager_SwitchActiveInputMethod_m3BAB791534CAB7AA957901F370FE854C63AB1801,
	CrossPlatformInputManager_AxisExists_m13A4ED5F88BAC335FDC42ADD4AAC9BB4CC5809F9,
	CrossPlatformInputManager_ButtonExists_m6C0E094AF61CF2F0F6592EFA67763D208C867440,
	CrossPlatformInputManager_RegisterVirtualAxis_m84945297F5E2C4D218B59B76E9D90D3BD36198A4,
	CrossPlatformInputManager_RegisterVirtualButton_mA5218520E9EE798325C72DFD0C988DC313D36BCF,
	CrossPlatformInputManager_UnRegisterVirtualAxis_m33DCEB8DAAF2703BFAB8F156A6633C0F4316C1A4,
	CrossPlatformInputManager_UnRegisterVirtualButton_m4B8F22F23F0891C1F5D4C07B729564D6A95CB82D,
	CrossPlatformInputManager_VirtualAxisReference_m5864A44C3FE72270B22D4C97FADEEB2AAA77869D,
	CrossPlatformInputManager_GetAxis_m4D45F9BE30A159DA4E72F4BF8294872297566E2D,
	CrossPlatformInputManager_GetAxisRaw_mE6D8754EAE5F6838CCF172FB03F4C251648EE987,
	CrossPlatformInputManager_GetAxis_mC9F177F6F0D83131B599CF80C3F3A8D7AD4568A0,
	CrossPlatformInputManager_GetButton_m728A64B9BC3F6471EB11B9CAF54BD4A10C710207,
	CrossPlatformInputManager_GetButtonDown_mE1BCD85447E0EF510728E49314FBCCEEE1FC7E8D,
	CrossPlatformInputManager_GetButtonUp_mD115A6BD45062A08A42EBBC7F0C9EC0D4F764ADD,
	CrossPlatformInputManager_SetButtonDown_m4DBFE81592B86D460ACC34D5936C788CD5B50890,
	CrossPlatformInputManager_SetButtonUp_m6228A0BD77568A903DF6429EEACD2267028FA32A,
	CrossPlatformInputManager_SetAxisPositive_mC5C7F88EEF5D6CB7B6B91BF6279FA53A94B4D527,
	CrossPlatformInputManager_SetAxisNegative_m41A74CBE51E8CB4870C79A8343E66B99B2CA7FDB,
	CrossPlatformInputManager_SetAxisZero_mBBD24590C97037F84384A559AAE37D2F8CA51730,
	CrossPlatformInputManager_SetAxis_m6BCE358D3D1A2E5E393AF281602B3E4745C0C5DA,
	CrossPlatformInputManager_get_mousePosition_mC886FC2F654E91F06407FDB891DF3201ED576DCD,
	CrossPlatformInputManager_SetVirtualMousePositionX_m1800042FCD90010EA2E2D51969D971324DD11964,
	CrossPlatformInputManager_SetVirtualMousePositionY_mDD4A2DF42E6CD673054A91FFE3C7FA61812889A8,
	CrossPlatformInputManager_SetVirtualMousePositionZ_m121058A0846AE6A974855607C8E3D46C221B376F,
	InputAxisScrollbar_Update_m4B6A6BBF4FAED786086BE4F9997E1D2D373BF2FE,
	InputAxisScrollbar_HandleInput_mF3A427E653ED917C3E91E0CBB1A3990F6110FB11,
	InputAxisScrollbar__ctor_mB96FAA176CD2958CCDE6E5F9212DCF2082486243,
	Joystick_OnEnable_m8728113F5BEE6D91514CB1A07550E8E7A3856CEE,
	Joystick_Start_m6164BF9BB7A0A8DD4524223639EA549E0491CFFD,
	Joystick_UpdateVirtualAxes_m5B79E0FBC765F85D9EE7FA9C7D74BDB35F326F3E,
	Joystick_CreateVirtualAxes_mCD13DFD2ADED0444F18C3856FD67A78539FD9C2C,
	Joystick_OnDrag_m1DC1103944EB982931C5946BD8EBFB8E63073BB6,
	Joystick_OnPointerUp_m06850F5D6C95D16DEB57B3FC4E50CCBCCD0EF7FB,
	Joystick_OnPointerDown_m1F10B670117FD67A734079ED71D4A3D36B783718,
	Joystick_OnDisable_m5097E08289FECC9A5499DB0747575F075353CAFB,
	Joystick__ctor_mA2C408B1EB0671CB8B340DBF932CB4153BAC3ABF,
	MobileControlRig_OnEnable_mAF3C7A8C67CE239A1FD5E6A8B224F7A91DE8B2E8,
	MobileControlRig_Start_m43792FB70FC02989DA9543801183A54005AD572B,
	MobileControlRig_CheckEnableControlRig_m59A8FCD09B2A6EA7702AE9EBB24E3BB9605B5CCD,
	MobileControlRig_EnableControlRig_m694051D1F28B05510357A3F96561EAF2732CAF8E,
	MobileControlRig__ctor_mCCFD8CBDA57F8D9B0E2805D4740637F5FFC4B120,
	TiltInput_OnEnable_mEACF194C56E3620055240D9D46880E6F9C201E9C,
	TiltInput_Update_m14D22BD1D9D47DF03965F0F6BBC2FB0E322F2B2A,
	TiltInput_OnDisable_m169AFDCFFA0609747DA889DF88C86D0A5C9C42B0,
	TiltInput__ctor_m909CBEC7014B584CB5EBF5A0B650E5D8845E4FB7,
	TouchPad_OnEnable_mFED012C0FDD349798B1296799C0AE7A05C2017EE,
	TouchPad_Start_m986951F12FF80D3A0D4B0DCB49D9FBA13B8A025C,
	TouchPad_CreateVirtualAxes_m214CE099E087A7A6FCDDF3B2740983436B62BA5E,
	TouchPad_UpdateVirtualAxes_m42D25C5EE9F890FECF580C97219455E73D09AF67,
	TouchPad_OnPointerDown_m13FDEACD95785D853D85B68E4993AC520A2D771F,
	TouchPad_Update_m0DD077DCE945CC47C3DD4FFDB9FEC5D4BB3A762A,
	TouchPad_OnPointerUp_mE35C9A5F6CD1909E8819F4D6D7282C7D20B37B88,
	TouchPad_OnDisable_mD0E67236EB0D365E3397D26723250C01614168B5,
	TouchPad__ctor_m9FEC2CD43CD850304B41B1C0142CC47F44B01E25,
	VirtualInput_get_virtualMousePosition_m897C50683722D1C3DF4FA9801524E7BF310B24BD,
	VirtualInput_set_virtualMousePosition_mDDF9F35B2C4AC37AB6CCF68772C57315612B1F75,
	VirtualInput_AxisExists_mDB6E7D0AF32ECE3E3CB1C4DA089D4B030D61F3F8,
	VirtualInput_ButtonExists_mBD9401EC2186C54F8EA7577FEEA500624F2E6083,
	VirtualInput_RegisterVirtualAxis_m43BC4BC9355B708CC739E3F2D0761A49342BC60F,
	VirtualInput_RegisterVirtualButton_mF6874262B94F78D0C2C166F7E20CFA47DD39BF41,
	VirtualInput_UnRegisterVirtualAxis_mD3511EE52A02EF720B086FF6EDCF9D4FA11A551F,
	VirtualInput_UnRegisterVirtualButton_mF05E241BD753B335E97CB8D1EDCFECE82A34F554,
	VirtualInput_VirtualAxisReference_m5AE323533C7DF65D71B551B173A63680BB5850EA,
	VirtualInput_SetVirtualMousePositionX_m49716B45CE295686844FDD803083136B9BAC2124,
	VirtualInput_SetVirtualMousePositionY_m80139449D4E09227D929E314419B1C72D57BD001,
	VirtualInput_SetVirtualMousePositionZ_m9276A4D39BC31E00C1977B2621549B1C1F40E51D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VirtualInput__ctor_mD6A4228D372182ABC7372ED25F4987CE1EAA27CB,
	MobileInput_AddButton_m55B4ECB00F31F0904145B5DC71AE2B7289960F34,
	MobileInput_AddAxes_mF5065897FC94197F4FD5BDD15A394E858218496E,
	MobileInput_GetAxis_m24CDEC7DA08736467196B8F90F19B3110782421A,
	MobileInput_SetButtonDown_mF3C9EEAF5750B7CF53C0D6D04D035CA8F1D27547,
	MobileInput_SetButtonUp_m0CEDEA05459505931FB2686C20AC0900A4941448,
	MobileInput_SetAxisPositive_mD4522AE0A5CFA591D720C9FA1E42D38485F66C9A,
	MobileInput_SetAxisNegative_mDB7F89D127295F2D4CC4764EB04571D9A46774C4,
	MobileInput_SetAxisZero_mCE681BFD720000CFA939C78B0EAEFCA3D5748BA8,
	MobileInput_SetAxis_m40791B9F5D8B28086FEA1030918A6DDBD96D2704,
	MobileInput_GetButtonDown_m8B7EC91AD10FF37A6910CC3AD684572F8CC4A403,
	MobileInput_GetButtonUp_mFB0BF6CE172238F3AF6B28CB16A73B5A0D714ABB,
	MobileInput_GetButton_m1805C5AAFEA6C56E1F083C318C7D8A56414742DC,
	MobileInput_MousePosition_m655B9F793060E92EEAFC358ED5A612124F71B234,
	MobileInput__ctor_m58D4C2380917920DD39E646CB4717F6EFBAA16F0,
	StandaloneInput_GetAxis_m208A36BD2256D5439E8BF99DFEE7C4FBE5C321DB,
	StandaloneInput_GetButton_m2156BA026DDB9F6FA9F45BBC8FEC871A3090629C,
	StandaloneInput_GetButtonDown_m3F88DFF900E2AB8729E6F63694BE3C8E2C19BBB7,
	StandaloneInput_GetButtonUp_mDF55E35A4B50D58901CEEF8DAEECD050A636398C,
	StandaloneInput_SetButtonDown_m5C1B0E5ED19F91DAEE8A23108865EBC57EB3F002,
	StandaloneInput_SetButtonUp_m23ECA36E7E2C9D79650FC93764E2FB47C52A6269,
	StandaloneInput_SetAxisPositive_m45ABA3A91481B6B07E37A24322C345DE4341472D,
	StandaloneInput_SetAxisNegative_m8ABB3422B35FA3D5EFEC9A3BCCE7D813C91E1DDA,
	StandaloneInput_SetAxisZero_mC0123C06F1DD19FF6DB9353DF0D711554B56B428,
	StandaloneInput_SetAxis_m620AB31F30F82FDB44EB995E494F618D8C42F765,
	StandaloneInput_MousePosition_m7D158ACA958E8C1101AFF4B3282E12B1A6EF7C82,
	StandaloneInput__ctor_m9D98FDF717857FB17019872D7A8446C224C493F3,
	Ball_Start_mD94D5E17C462F68ED882F7A71510571FE3BC99E2,
	Ball_Move_m2876C25E0C756BF245A81AD607EFEF4635163D37,
	Ball__ctor_mCAF1E03B050453AC083D54A0395370CF4E322D4A,
	BallUserControl_Awake_m3053C00DA15A4F03C606E253DF2F1222233D2CD4,
	BallUserControl_Update_mCF16AA2CFDB461475BEFE7F4A82AD02E27B5A2C9,
	BallUserControl_FixedUpdate_mAFB462499A540A51F3A59B50EACEF013A1FD99C6,
	BallUserControl__ctor_m933AEE908ED128E00AA48445422BAAACE54D27C8,
	AICharacterControl_get_agent_mC145C43591E7F58C19D6812346E8B267562DCE2A,
	AICharacterControl_set_agent_m3AF797E33435DE57A5B68D4A370B38FF5066AC35,
	AICharacterControl_get_character_m5F68CEC317221C119DD993F0BC66E83B46ECAF10,
	AICharacterControl_set_character_mB648916EAE530CF42ADB6A2133597CD2C5C351AE,
	AICharacterControl_Start_m94E0B0C90E7F8680D0BA14C0B7F73F1147F1ECBD,
	AICharacterControl_Update_m3739D8300A1B4ED83717268FACD69BA35E2F2D60,
	AICharacterControl_SetTarget_m85A54D9230685AEFC19FE47B0844545F3AC76ED7,
	AICharacterControl__ctor_m64E75FC80DC62E05D71EA1EDE411F5E9118F9895,
	ThirdPersonCharacter_Start_m77BED10E07BCE3B9FCDFDAB0A604598E7EF52551,
	ThirdPersonCharacter_Move_mC404A566646B48F6E4895C4B56B568B2FCD7B315,
	ThirdPersonCharacter_ScaleCapsuleForCrouching_mEF225C57F19EEFB21F3FA5065F835ED295F9FA83,
	ThirdPersonCharacter_PreventStandingInLowHeadroom_m11B4FCE921A339A567D1DE504C5781738FE130F4,
	ThirdPersonCharacter_UpdateAnimator_mA55D3E6EEE3D094A0377F74680EDC955C915C2F3,
	ThirdPersonCharacter_HandleAirborneMovement_mA70C2C3E9776D1F93AC11B00DBB614A653D05713,
	ThirdPersonCharacter_HandleGroundedMovement_m9E47FDBABA1394392BC50B9D0E8F617417DCD8EA,
	ThirdPersonCharacter_ApplyExtraTurnRotation_m04D602E8910E9F91436DC3978A237DA976E0528A,
	ThirdPersonCharacter_OnAnimatorMove_m1B720209EAC4123222D83A2604CDD9DEB35B4807,
	ThirdPersonCharacter_CheckGroundStatus_m529274699AE7FB0AA68E6259A4009C5119346DD6,
	ThirdPersonCharacter__ctor_m2A343C6DA11522069E215C58B8C5F0F2916482B9,
	ThirdPersonUserControl_Start_m6E6473ABBBE127E5FED209498ED24663D0612682,
	ThirdPersonUserControl_Update_m673BE47FF260D53BF312CAB76F77C32D8CA22629,
	ThirdPersonUserControl_FixedUpdate_mDF454F6DBD58CBB0429BBC36502150EF9D360453,
	ThirdPersonUserControl__ctor_m7BD727F15D1289F92B24B581C9DDB5A7489A967E,
	FirstPersonController_Start_m87414ABA8CE33FC0DE5E9856C79A357E501D7308,
	FirstPersonController_Update_mF3FC7041AB276DC382BC8A4F61AEAFAFAEA04ED4,
	FirstPersonController_FixedUpdate_mBA424DA0A9AA19D6C2E50F798C372AA7543FD918,
	FirstPersonController_ProgressStepCycle_mF0FA5D7B32881E407247E42A6F07B1F49AB5717F,
	FirstPersonController_PlayFootStepAudio_mE2350EC1F9F5D34A04464C32EEBE1E7E82134179,
	FirstPersonController_UpdateCameraPosition_m9287722E622F693557E810274D76CF402849E7B5,
	FirstPersonController_GetInput_m20FF731BB9AE80DB53A15A87BB960E7552AF5730,
	FirstPersonController_RotateView_m52AE6EADF85961F72D76454E11913AFE0AB0C77A,
	FirstPersonController_OnControllerColliderHit_m359FEB09F912EF3311435D6CF9CAFB25CA6EBDDC,
	FirstPersonController__ctor_m067AA344391066103FAE6175AFC5C1717B4022F6,
	HeadBob_Start_m2956DB87FE33B62FC550D8E8581B1197E790BD8F,
	HeadBob_Update_mBF51EAFAB23AC5B5E176E02CCCA668AF6B20F4AC,
	HeadBob__ctor_mD8C44B9DC99CA8E61E23578A15973F48D1B1D077,
	MouseLook_Init_m6F89547F704698EFB173D980CEC974CD6D11CE1E,
	MouseLook_LookRotation_m12F5371B9F69C6E79A959B1707E2079EF176FE47,
	MouseLook_SetCursorLock_m74E47ED1CDCF73947446BA6F3109C7305B44A27E,
	MouseLook_UpdateCursorLock_mC03FF4763BE1894EDD81D90A96791E644BDA4DF9,
	MouseLook_InternalLockUpdate_m689C8345609827E54B2C45018C9FA2710641AE41,
	MouseLook_ClampRotationAroundXAxis_mB80B11F6AA879942BCC5B59AC99857BEC811CEBB,
	MouseLook__ctor_m4991C1F282EDF4C515B9C28106EC981F5D157CF2,
	RigidbodyFirstPersonController_get_Velocity_mA3844E469740CF2B014878B8BF192EF385675B28,
	RigidbodyFirstPersonController_get_Grounded_mB76301608244EA277030FED9FF07421B1DE37A55,
	RigidbodyFirstPersonController_get_Jumping_m7E2EACD44C0241B8423B97E1739DBE269FE204A6,
	RigidbodyFirstPersonController_get_Running_mE86F6DD182214508725455FC918CAF13BC1A462B,
	RigidbodyFirstPersonController_Start_m2330F14619B7112B794576FAE87657F51E16F998,
	RigidbodyFirstPersonController_Update_mC9D36ED411466D763F3B6726ED0264FE5815D040,
	RigidbodyFirstPersonController_FixedUpdate_m81ED45B8FE887F4C512FC12EA0EA4A83AECC370C,
	RigidbodyFirstPersonController_SlopeMultiplier_m5BC79F85397DC3492513C1AC8088A8E62DE47B37,
	RigidbodyFirstPersonController_StickToGroundHelper_m1ECAEA0961503D6BBF65AA20D056F4E803C13CA8,
	RigidbodyFirstPersonController_GetInput_m4DDAC49710C5FA777D29BF16A154A8B35D6EA8D8,
	RigidbodyFirstPersonController_RotateView_mD3A6BE7C73F1839CE62FBECC3A03E53C68AF8711,
	RigidbodyFirstPersonController_GroundCheck_m7843581359DD47F573ADD9059732FA20246C4FC0,
	RigidbodyFirstPersonController__ctor_mCCA1D3CC7960FE1BDB756F1CC3BC6D12DC5BC471,
	AbstractTargetFollower_Start_m5830AC9B925EF10A96832CB64E17703537ABA4D6,
	AbstractTargetFollower_FixedUpdate_m059E3F7EE069E830A1675B7E609B2C0992F5C9FE,
	AbstractTargetFollower_LateUpdate_mE6D4BAF43EA1C593A3439507476CAF6C360874F5,
	AbstractTargetFollower_ManualUpdate_m5FDFBD137B58BE58593D6D751BAA4C4A5134C158,
	NULL,
	AbstractTargetFollower_FindAndTargetPlayer_m0A72C9D084555F206759AF9BBC56C039170E497A,
	AbstractTargetFollower_SetTarget_m17CE0F35DAE4E4086CACD31BA14C05DA2589D3A9,
	AbstractTargetFollower_get_Target_m24234F14398E0958CEA1A476F649E762D6E17814,
	AbstractTargetFollower__ctor_m7CA75F5DEB95588875BF359F17F9A45F1AC28E18,
	AutoCam_FollowTarget_m49D07251C6021E9F838CC8747A0CF83EFCEE8552,
	AutoCam__ctor_mD9E4154D81A29B6C8C68297374653FB9CFAB3AC2,
	FreeLookCam_Awake_m9EAF9CF5A48AE92736DB798522FFFA95DF0C33A0,
	FreeLookCam_Update_mD83A18376B2B5F9970C8DA78E60DB12C1B508AFC,
	FreeLookCam_OnDisable_mF4EE05B50BF94C52C65ABE1F26A383F3F614DE54,
	FreeLookCam_FollowTarget_mADE713C6EA3D2A61B6FEFBAC2226C9519728478C,
	FreeLookCam_HandleRotationMovement_m835C5DFA2FD21F7C64057BDB0BAEA62B99A2D404,
	FreeLookCam__ctor_mB38818588467BA1204288BB062BBAABDCC0225D9,
	HandHeldCam_FollowTarget_m9BF734BC3214DBB892E0D2D507E2F9452B147FDB,
	HandHeldCam__ctor_m57D82D85C30C87D03E7EAB05EBDF656962DA2AB5,
	LookatTarget_Start_m55D1DA54A4762FF2AF364421AED7C54396705CBA,
	LookatTarget_FollowTarget_m363133638041D45CAB46C88DD61768E4E39DF534,
	LookatTarget__ctor_m765D6EC2839C0E8BDF17F849E28A0D472A37D25D,
	PivotBasedCameraRig_Awake_m59237BF4FB7603169D6E12D95C372D7A889E1F27,
	PivotBasedCameraRig__ctor_m3A67ED42B196F44E73ECF164084D9655F6BE700F,
	ProtectCameraFromWallClip_get_protecting_m1E2FFDFDBBEEA74AD539E8F35522047B7F14F9FB,
	ProtectCameraFromWallClip_set_protecting_m23B7F1BBEE76FDAFF187820178B1CDAE2A326F55,
	ProtectCameraFromWallClip_Start_mFD2770D700A02F262AF9926E4C5828B0C8AC544C,
	ProtectCameraFromWallClip_LateUpdate_mA71300721FF34772D041EC217AA182F5C9FFEDDC,
	ProtectCameraFromWallClip__ctor_m9BE30002DD3BC07052D7E11A448D6A1330E822A9,
	TargetFieldOfView_Start_mCBB06309FF6E440654164EDF0557C46AA5C673E7,
	TargetFieldOfView_FollowTarget_mA92F8333656949ACB3D8A4C383613C2B5791A5FB,
	TargetFieldOfView_SetTarget_mFBC0E9995E9CDF97FD74AAC1370400F5FCAB3373,
	TargetFieldOfView_MaxBoundsExtent_mBAAA9C398FF2E1C3F6F80FF5BC51D40CD445E471,
	TargetFieldOfView__ctor_m7F1799AEF5CD5A646C15D7C92B50668263FF525E,
	ReplacementDefinition__ctor_m49E65F984C7161F3C2C5DD51172D8C9624AD31E5,
	ReplacementList__ctor_mEC91B69367F2501099D36FD66A5715B772DAACCB,
	Vector3andSpace__ctor_mFE5D5C1F196B500E04A45EC3230CE4FBD138F894,
	U3CDragObjectU3Ed__8__ctor_m5ED5AC5E5AB22F2FB9178DDCC2D99A933CB2C211,
	U3CDragObjectU3Ed__8_System_IDisposable_Dispose_m011A5F292C63CAACDE3E483BE60B65308D1E1CE4,
	U3CDragObjectU3Ed__8_MoveNext_mDC1BCC4C79BDB25C44A9DC9735D48FA2E175E071,
	U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m043FAB9E9E779B7ED3968BD00C707E31D47CA661,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_mC00266C3A2C2B2C5CAA743701D45DC345EC96706,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m26F191AB7590668B6FE2A7D0F9D5ABC771B90EA0,
	U3CFOVKickUpU3Ed__9__ctor_m2E70E0F14634E8A3208961FEA2B1664230D5757C,
	U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_mD4B00A512ECC65464CEF2F971595D3B28032A8A2,
	U3CFOVKickUpU3Ed__9_MoveNext_m512DBF15F770463827161931918513ECBB78759E,
	U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59D92574D53698089582F9A0BE55EB4A9B9B256,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_m09B3619F760A7A100802FBC2C44718B3AABD19CE,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mBA658BB1928272A6E7E098ECD397E77C191D04FC,
	U3CFOVKickDownU3Ed__10__ctor_mAC26AAD43FBF2CEF4164498556AE1B030837944F,
	U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_mA336F42BB92EDDD223120F28B0A13EF29F9FF9C7,
	U3CFOVKickDownU3Ed__10_MoveNext_mF35885E2D181C34AA25B41E9212A468D59DE456C,
	U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72EA85FAA1FD7B04B65BB1C719D6472A9B8EC0EC,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_mD9005227142121AFF35782081F84B5AB7F8FD579,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFD66D6F9B957FC0522A35FE0D3662667B896A08C,
	U3CDoBobCycleU3Ed__4__ctor_m3845D1AEC7D920CC91A343EECDAFADD29AA364DF,
	U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m337964E05B4A29A899562BA187A6C43AA915FC03,
	U3CDoBobCycleU3Ed__4_MoveNext_m9452FC51F84697BCD114A5791B3137BA34CD2DE3,
	U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A882F4C449D3A6AFE38E54266A62CCE9FEB71C,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m2F0D716C86EF00BBFB2E9150ECA135004FEAC486,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m7BBD42AB52B89D8F7451CC6A79BA3C660BA93392,
	U3CResetCoroutineU3Ed__6__ctor_mE23E11148F2C3B9148F5CF189984AE4CD71E8F8C,
	U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m8E5DEBEC17DEB14B5B28C805C11BD627FDAEF4A3,
	U3CResetCoroutineU3Ed__6_MoveNext_m62B0E26E3294631050E345318DDC863C31EFAA26,
	U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59F7FC20AC24226410D75D0B8D5DA40BEEC6C1A1,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mDAC82C73DF62065BCD4C1CB44848A55185A3987E,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m432D1DC8AECAC892944242B8ADDF7CCD6FAD9E9F,
	U3CStartU3Ed__4__ctor_mD0A1B215E5F066646BCAF66C33A5F48D0989E090,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m1FE667ACD1B0DC25165D56369A3CF8733C0B206A,
	U3CStartU3Ed__4_MoveNext_m7AB511BE787031F2A04E72326E49E7B5F3B208C9,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F071490E4E71A9A719D10DAAF032F74B6B31363,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m32B0CA08CBECCFC5DEF3C43032A5658A24D3C7C0,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m3E718F7631CCED3184D3AB437310A802E3F5FAF0,
	Entry__ctor_m593D5A5177E927D55B4EDEA893C9A89DA0E3683C,
	Entries__ctor_m9FF702A552EFD09D5146A438883B9D3267428151,
	U3CActivateU3Ed__5__ctor_m04B00CBAD8695D22C759B14B4228011F72884E9F,
	U3CActivateU3Ed__5_System_IDisposable_Dispose_m6F5CC0A9E69B2ABB18B2F15DC8F1575F3899E175,
	U3CActivateU3Ed__5_MoveNext_mF97B09D381486D8A6E40E66208EF442D65683980,
	U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8461E4FE3B48538BDB9C8FCA99CFB50C4D46C8,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m10D07A5F6216561973401AE2344F661E0C7E520C,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_m013FDADA4980E415D472066957CB1D762B166B8B,
	U3CDeactivateU3Ed__6__ctor_m43C0B814DE2C6DF5F445D122620F750158D95A9B,
	U3CDeactivateU3Ed__6_System_IDisposable_Dispose_mB5C406251AEA37C27D789602F621349E95B5077E,
	U3CDeactivateU3Ed__6_MoveNext_m690EC1B5BB66A6A7DEA9B82A9EC24767FB669A06,
	U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A9DD75E5BEF9E464B4366FB4A5BBFA6C9624FD1,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m5C179259768EC874E6C752D62F84805BDC036AA3,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m1F491BC143C92AF323241F437B1854040FBB960B,
	U3CReloadLevelU3Ed__7__ctor_m89F904013A652554E256373377BF93992BA1205E,
	U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_m4EAEA10A25D3BCD8C4A41A7C305673C2702021A9,
	U3CReloadLevelU3Ed__7_MoveNext_m22DFAE90E48E5E9AE2E064903D8AA47C08EB9B48,
	U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F1B7ED565E25B37FC48A22BDB4378F714F5B3A1,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_mF3AB1DBCA692E293D77D4D9294D0BC47CCB22CFB,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_m62C25C33793EE76CA6A5FA59EC19F7EE078C4BD1,
	WaypointList__ctor_m6F4EDE811589F476A95CC3ECCA3BE59C54AF753A,
	RoutePoint__ctor_m0CF428B188D139A371423658C3A41B96137CDA09_AdjustorThunk,
	DemoParticleSystem__ctor_mE2CE12D47F99F9170148BFB896C67AE7A9A73C65,
	DemoParticleSystemList__ctor_mDD1D4D7FC5AF577A1F1FC662575170B15ACA4650,
	U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01,
	U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2,
	U3CStartU3Ed__1__ctor_m28BA84EE932E5505BA7BD35AC5F8007BA8B96564,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m2411BD36258EF1A6EF59D488D875E1989C3BAE1C,
	U3CStartU3Ed__1_MoveNext_m69D4364C306F1FC8F21C33E29E6C125B2B9EEF2B,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m600B8E8FC9018CA7E4700F80CF7114DBD2A89C0F,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m8BE9B7F5466A03EDD437E3D16EB3E659B51ABC0E,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m60734E8242F26A6B6ADF449A1668715F5E393E00,
	U3COnCollisionEnterU3Ed__8__ctor_mE0412A81997D86406428B1A95EEDF564D6ED49E0,
	U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_mE7DC56D7449DC66BB04E6EC60C7B143779E4AEB9,
	U3COnCollisionEnterU3Ed__8_MoveNext_m53C4680C5CFB7DB025A0854EDFBA101B1C74C851,
	U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C70CC61071F2B35953F4283AA505C55E6940F69,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m62403746C385587687DAFDAACBD369E71159685F,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m353A3041DBEED2FEBA3CCC26222620D9908BC733,
	VirtualAxis_get_name_mC3959CD36494EE1B06CAEA1675DD19E5FFCB9BD9,
	VirtualAxis_set_name_m2A44E0BF21BB426C9A14AB057D5EF41616B76096,
	VirtualAxis_get_matchWithInputManager_m2F68784B6C454EB26934401303E28C159980F315,
	VirtualAxis_set_matchWithInputManager_m326813FB9C39A5D63C98D4AE931384D6B67AE944,
	VirtualAxis__ctor_m9B094B00B2F0F1C6C474D3DA51419F4549540E53,
	VirtualAxis__ctor_mAC45A3BC043EA253666CCDE2762DB39475FED915,
	VirtualAxis_Remove_m0517C6C37E94CCC84337FD412982D1800E5CEFD6,
	VirtualAxis_Update_m639BD6EC869B61C712D4519290523C61745FF6C3,
	VirtualAxis_get_GetValue_mB0D352473A7E1F6A9402335FBD18625ADFCE0A69,
	VirtualAxis_get_GetValueRaw_mA75834F100AB39C130FEA7AE85677E4928E58397,
	VirtualButton_get_name_m836058DAC831C5BB481A422120939EB4D14CE55B,
	VirtualButton_set_name_mCC77CE771C89C23B47A2D9B027C7E754666A78A5,
	VirtualButton_get_matchWithInputManager_mD6924A44FFCFF72519BDDEAD61E3072CC3C3FCF3,
	VirtualButton_set_matchWithInputManager_mD438AFD4E212727BED9ECD1F0CBFE6243112AE3D,
	VirtualButton__ctor_mECADC4A0B8ACF0954720A84061800EA0F00D9FDD,
	VirtualButton__ctor_mBC57649412C90DFF3179B681B9D33BB88443FFD9,
	VirtualButton_Pressed_m596B075C829D1E8C500AF6694155488CF2250402,
	VirtualButton_Released_mC4B98C45864A5832601A90437E691119F28E25E6,
	VirtualButton_Remove_m0F66A404819C8B483DA3F02FDCEBDB005867D37D,
	VirtualButton_get_GetButton_m228F811AD3C4911C45AFEA7960E35F4A84B7A32D,
	VirtualButton_get_GetButtonDown_mB6BBC9E21BB477279E5D74926CFA633E671AC430,
	VirtualButton_get_GetButtonUp_m79C31A03EE6AC926E932FA1A28989A73B0257E43,
	AxisMapping__ctor_m2B8C914999C51C9568C81B4C1E6750BCAF66BE1F,
	MovementSettings_UpdateDesiredTargetSpeed_m92BE3A22C9CF55BF12A0D441EBB51FF7A1779D69,
	MovementSettings__ctor_mF768FC3EE16A9A355B9FCD47EF76C8A4879FAF29,
	AdvancedSettings__ctor_m845AB1B94472E573D2C5DD585876E67B97C0AD23,
	RayHitComparer_Compare_m6EFBFB91A4F97CC5D2111878BCD211DCEA51DAE3,
	RayHitComparer__ctor_m2A11DF0646D70F2DAFC0751CD63F55A40D397D11,
};
static const int32_t s_InvokerIndices[470] = 
{
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	2116,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	2667,
	26,
	26,
	23,
	23,
	951,
	1616,
	23,
	23,
	1523,
	14,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	14,
	23,
	23,
	343,
	1523,
	23,
	14,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	28,
	28,
	23,
	23,
	23,
	23,
	746,
	343,
	14,
	23,
	2668,
	1616,
	2669,
	23,
	23,
	23,
	31,
	23,
	2670,
	2671,
	2670,
	2671,
	2670,
	2671,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	32,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	2672,
	23,
	14,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	173,
	114,
	114,
	163,
	163,
	163,
	163,
	0,
	1438,
	1438,
	2673,
	114,
	114,
	114,
	163,
	163,
	163,
	163,
	163,
	1757,
	1709,
	1644,
	1644,
	1644,
	23,
	343,
	23,
	23,
	23,
	1612,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1612,
	26,
	23,
	26,
	23,
	23,
	1611,
	1612,
	9,
	9,
	26,
	26,
	26,
	26,
	28,
	343,
	343,
	343,
	2674,
	9,
	9,
	9,
	26,
	26,
	26,
	26,
	26,
	951,
	1611,
	23,
	26,
	26,
	2674,
	26,
	26,
	26,
	26,
	26,
	951,
	9,
	9,
	9,
	1611,
	23,
	2674,
	9,
	9,
	9,
	26,
	26,
	26,
	26,
	26,
	951,
	1611,
	23,
	23,
	2675,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	2676,
	31,
	23,
	1612,
	23,
	42,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	343,
	23,
	343,
	6,
	23,
	26,
	23,
	23,
	23,
	23,
	27,
	27,
	31,
	23,
	23,
	2677,
	23,
	1611,
	89,
	89,
	89,
	23,
	23,
	23,
	746,
	23,
	1620,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	343,
	23,
	26,
	14,
	23,
	343,
	23,
	23,
	23,
	23,
	343,
	23,
	23,
	343,
	23,
	23,
	343,
	23,
	23,
	23,
	89,
	31,
	23,
	23,
	23,
	23,
	343,
	26,
	2673,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	1609,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	26,
	89,
	31,
	26,
	465,
	23,
	343,
	746,
	746,
	14,
	26,
	89,
	31,
	26,
	465,
	23,
	23,
	23,
	89,
	89,
	89,
	23,
	1621,
	23,
	23,
	41,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	470,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
