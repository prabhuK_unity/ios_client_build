﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Collections.Generic.Dictionary`2<Byn.Awrtc.ConnectionId,Byn.Awrtc.Base.IDataPeer> Byn.Awrtc.Base.AWebRtcNetwork::get_IdToConnection()
extern void AWebRtcNetwork_get_IdToConnection_m4CA2E8D816FD12B632F91512B30FA65CCFBF3098 ();
// 0x00000002 System.Void Byn.Awrtc.Base.AWebRtcNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void AWebRtcNetwork__ctor_mD5C3898B208A4FBC1016A9A179BFBD610A4A1CA9 ();
// 0x00000003 Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Base.AWebRtcNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
// 0x00000004 System.Void Byn.Awrtc.Base.AWebRtcNetwork::StartServer(System.String)
extern void AWebRtcNetwork_StartServer_m1D5FBCD6FC01C00F21AE9B8E1C309C00E2B7603A ();
// 0x00000005 System.Void Byn.Awrtc.Base.AWebRtcNetwork::StopServer()
extern void AWebRtcNetwork_StopServer_mEAD0CA62887D3145333D84ED6390D3AC92D09C45 ();
// 0x00000006 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::Connect(System.String)
extern void AWebRtcNetwork_Connect_m2858D1011562CE953A370546F2B33B0E7ACC4A74 ();
// 0x00000007 System.Boolean Byn.Awrtc.Base.AWebRtcNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void AWebRtcNetwork_Dequeue_m090C2801F4CE22845D9CBE4BF81A87C97B62F22B ();
// 0x00000008 System.Void Byn.Awrtc.Base.AWebRtcNetwork::Flush()
extern void AWebRtcNetwork_Flush_mF08541735C7E3B2FBD6CFE1A44FD863331C211DA ();
// 0x00000009 System.Boolean Byn.Awrtc.Base.AWebRtcNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void AWebRtcNetwork_SendData_m4EA111835C244AE06183AB7EF90C6C7920E091C9 ();
// 0x0000000A System.Void Byn.Awrtc.Base.AWebRtcNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_Disconnect_m164354BD0291ECA083EC81A1BDA8A4A101A1D82F ();
// 0x0000000B System.Void Byn.Awrtc.Base.AWebRtcNetwork::Update()
extern void AWebRtcNetwork_Update_m43606A9785E192547C7ED702AF3A25530ABB1DBC ();
// 0x0000000C System.Void Byn.Awrtc.Base.AWebRtcNetwork::Shutdown()
extern void AWebRtcNetwork_Shutdown_m8CD81741B59CAA51C4BF26E868C365064A5F7C7C ();
// 0x0000000D System.Void Byn.Awrtc.Base.AWebRtcNetwork::CheckSignalingState()
extern void AWebRtcNetwork_CheckSignalingState_mC69D9CF1F03F1368DC6997017E53EA6AFA15A552 ();
// 0x0000000E System.Void Byn.Awrtc.Base.AWebRtcNetwork::UpdateSignalingNetwork()
extern void AWebRtcNetwork_UpdateSignalingNetwork_m4B164DC5BA3109A68C58529B9B711F493FAF3DFE ();
// 0x0000000F System.Void Byn.Awrtc.Base.AWebRtcNetwork::UpdatePeers()
extern void AWebRtcNetwork_UpdatePeers_m089CC02D5ED0372B62CF827413DC85D787AC6A34 ();
// 0x00000010 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::AddOutgoingConnection(System.String)
extern void AWebRtcNetwork_AddOutgoingConnection_m4854D9CB0213ED5BB80AE9B62BE173EFB3A1B33A ();
// 0x00000011 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::AddIncomingConnection(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_AddIncomingConnection_m0A50BDDB5E7887CBAA6227E782F91DD3C8AEDD8A ();
// 0x00000012 System.Void Byn.Awrtc.Base.AWebRtcNetwork::ConnectionEtablished(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_ConnectionEtablished_mBAC5E474A1017FA704C4349A4CE085475B886D49 ();
// 0x00000013 System.Void Byn.Awrtc.Base.AWebRtcNetwork::InitialSignalingFailed(Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void AWebRtcNetwork_InitialSignalingFailed_mBA5CA5FA7B09B70E162514876F43D2E65CA94CA2 ();
// 0x00000014 System.Void Byn.Awrtc.Base.AWebRtcNetwork::HandleDisconnect(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_HandleDisconnect_m322DA33D4504FF532B177E30C04FDF34931D7428 ();
// 0x00000015 Byn.Awrtc.MessageDataBuffer Byn.Awrtc.Base.AWebRtcNetwork::StringToBuffer(System.String)
extern void AWebRtcNetwork_StringToBuffer_m7A8B58C39DE5FC1D3278E5BB5F2EACE704FC6B02 ();
// 0x00000016 System.String Byn.Awrtc.Base.AWebRtcNetwork::BufferToString(Byn.Awrtc.MessageDataBuffer)
extern void AWebRtcNetwork_BufferToString_m6EDA9DA20D81F91A2113446F63115E8312569ED6 ();
// 0x00000017 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::NextConnectionId()
extern void AWebRtcNetwork_NextConnectionId_m32545ABD09F2DB9DBB2AB34CB353AD875AC788AA ();
// 0x00000018 System.Void Byn.Awrtc.Base.AWebRtcNetwork::Dispose(System.Boolean)
extern void AWebRtcNetwork_Dispose_m0909A5BA081EF6D68B0847E7EC2663ECDEC234C5 ();
// 0x00000019 System.Void Byn.Awrtc.Base.AWebRtcNetwork::Dispose()
extern void AWebRtcNetwork_Dispose_mDFA0F7A08F1B7B7A715DD675963BBC113D1103D9 ();
// 0x0000001A System.Void Byn.Awrtc.Base.AWebRtcNetwork::.cctor()
extern void AWebRtcNetwork__cctor_m827FDBD8A193417AED38255FA7E000E04A0BC3FD ();
// 0x0000001B System.Void Byn.Awrtc.Base.AMediaNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void AMediaNetwork__ctor_m6C1994F70AD9036F67811F87223F8B899C1A1E8D ();
// 0x0000001C Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Base.AMediaNetwork::CreateLocalStream(Byn.Awrtc.MediaConfig)
// 0x0000001D System.Void Byn.Awrtc.Base.AMediaNetwork::Configure(Byn.Awrtc.MediaConfig)
extern void AMediaNetwork_Configure_mE8EBD7B77F6BA75EB1D12511A5F8A16C979F0B6B ();
// 0x0000001E System.Void Byn.Awrtc.Base.AMediaNetwork::OnLocalStreamAdded()
extern void AMediaNetwork_OnLocalStreamAdded_m8C861AAA89CE1DF061F662888D6A35E6CC5ABFCE ();
// 0x0000001F System.Void Byn.Awrtc.Base.AMediaNetwork::AddLocalStreamTo(Byn.Awrtc.Base.IMediaPeer)
extern void AMediaNetwork_AddLocalStreamTo_m222A8CA7D8AD34F486F429F1329CC6C81E7F29E0 ();
// 0x00000020 System.Void Byn.Awrtc.Base.AMediaNetwork::OnLocalStreamRemoval()
extern void AMediaNetwork_OnLocalStreamRemoval_mABB3821E6219F7EDFE8AE459576C545F5CC472DD ();
// 0x00000021 Byn.Awrtc.MediaConfigurationState Byn.Awrtc.Base.AMediaNetwork::GetConfigurationState()
extern void AMediaNetwork_GetConfigurationState_mE1F320786F972D214F950EFCD79FE9F31E4DE551 ();
// 0x00000022 System.String Byn.Awrtc.Base.AMediaNetwork::GetConfigurationError()
extern void AMediaNetwork_GetConfigurationError_m2817623C30AA44B47EF3A6A4D7F3662AEB0B126E ();
// 0x00000023 System.Void Byn.Awrtc.Base.AMediaNetwork::ResetConfiguration()
extern void AMediaNetwork_ResetConfiguration_m55C59B4E6EDBCE4101F8A91F701AFB14C027AEB1 ();
// 0x00000024 Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Base.AMediaNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
extern void AMediaNetwork_CreatePeer_m481AF4E4DDB4CC6607D949EDD650EA93D2A1A203 ();
// 0x00000025 Byn.Awrtc.Base.IMediaPeer Byn.Awrtc.Base.AMediaNetwork::CreateMediaPeer(Byn.Awrtc.ConnectionId)
// 0x00000026 Byn.Awrtc.IFrame Byn.Awrtc.Base.AMediaNetwork::TryGetFrame(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_TryGetFrame_m27B10E65DA0196F6F2AD405CB4CBEEDC6951F68D ();
// 0x00000027 System.Void Byn.Awrtc.Base.AMediaNetwork::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_SetVolume_m96867A59F4AF6ABDB5B948D419D214EB3A7BA4C7 ();
// 0x00000028 System.Void Byn.Awrtc.Base.AMediaNetwork::Dispose(System.Boolean)
extern void AMediaNetwork_Dispose_mBF19A250E97D8326F4AF6A98D9932E3B4E956935 ();
// 0x00000029 System.Void Byn.Awrtc.Base.AMediaNetwork::DisposeLocalStream()
extern void AMediaNetwork_DisposeLocalStream_m936602A563B9F378296BE8650C358C02DA5116CF ();
// 0x0000002A System.Boolean Byn.Awrtc.Base.AMediaNetwork::HasAudioTrack(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_HasAudioTrack_m53CF7A28AAB8DBD29BC1E8F1FD91BE5AED532CEF ();
// 0x0000002B System.Boolean Byn.Awrtc.Base.AMediaNetwork::HasVideoTrack(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_HasVideoTrack_mA6E23276174323AE7F99F6DA38997DDB22F54F6B ();
// 0x0000002C System.Boolean Byn.Awrtc.Base.AMediaNetwork::IsMute()
// 0x0000002D System.Void Byn.Awrtc.Base.AMediaNetwork::SetMute(System.Boolean)
// 0x0000002E System.Int32 Byn.Awrtc.Base.AWebRtcPeer::NextLocalId()
extern void AWebRtcPeer_NextLocalId_m98529AD42CA2DF83F27757642C1E5609E5E0586F ();
// 0x0000002F System.Int32 Byn.Awrtc.Base.AWebRtcPeer::get_LocalId()
extern void AWebRtcPeer_get_LocalId_m817A245E8480A559EEEA0BC0FCB23637186E7144 ();
// 0x00000030 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcPeer::get_ConnectionId()
extern void AWebRtcPeer_get_ConnectionId_m039CD779581CC1C820E890F9BB51CEA20F62152B ();
// 0x00000031 Byn.Awrtc.Base.SignalingInfo Byn.Awrtc.Base.AWebRtcPeer::get_SignalingInfo()
extern void AWebRtcPeer_get_SignalingInfo_mEF9C8C2F2A6D90F5240927464E319DA45358FF7E ();
// 0x00000032 System.Void Byn.Awrtc.Base.AWebRtcPeer::set_SignalingInfo(Byn.Awrtc.Base.SignalingInfo)
extern void AWebRtcPeer_set_SignalingInfo_mDA4F41F2B58D6376CADDE45A3EC3E56AAE10E840 ();
// 0x00000033 Byn.Awrtc.Base.PeerConnectionState Byn.Awrtc.Base.AWebRtcPeer::get_ConnectionState()
extern void AWebRtcPeer_get_ConnectionState_mBDCCEF49726A33B993166F48BE7736F6F4BD64EB ();
// 0x00000034 Byn.Awrtc.Base.PeerSignalingState Byn.Awrtc.Base.AWebRtcPeer::get_SignalingState()
extern void AWebRtcPeer_get_SignalingState_mC4C2D07B8D25D3FBAEAF6E60E471ABA2A4007742 ();
// 0x00000035 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::get_WasRenegotiationRequested()
extern void AWebRtcPeer_get_WasRenegotiationRequested_m7EF7DF06C7CB1416030910DC52BBAA6FB10D0BE3 ();
// 0x00000036 System.Void Byn.Awrtc.Base.AWebRtcPeer::NegotiateSignaling()
extern void AWebRtcPeer_NegotiateSignaling_m5116502738174A4AAC75500306C2ADE33E9D94D5 ();
// 0x00000037 System.Void Byn.Awrtc.Base.AWebRtcPeer::EnqueueOutgoingSignaling(System.String)
extern void AWebRtcPeer_EnqueueOutgoingSignaling_mA98BE650140EE5E92AD830D29626E653CA3C0A30 ();
// 0x00000038 System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardOnDispose()
// 0x00000039 System.Void Byn.Awrtc.Base.AWebRtcPeer::UpdateConnectionState(Byn.Awrtc.Base.PeerConnectionState)
extern void AWebRtcPeer_UpdateConnectionState_m70E50D4DF2B8D289FA513FC43C015C1328A35C85 ();
// 0x0000003A System.Void Byn.Awrtc.Base.AWebRtcPeer::UpdateSignalingState(Byn.Awrtc.Base.PeerSignalingState)
extern void AWebRtcPeer_UpdateSignalingState_mF4E3704801AB38231B5E6BAEF6BF54F3C364BBD4 ();
// 0x0000003B System.Boolean Byn.Awrtc.Base.AWebRtcPeer::IsNumber(System.String)
extern void AWebRtcPeer_IsNumber_m42F4292193A4EFAE5D23E9B5FA3BAD1F057BD91D ();
// 0x0000003C System.Void Byn.Awrtc.Base.AWebRtcPeer::HandleSignalingNegotiation(System.String)
extern void AWebRtcPeer_HandleSignalingNegotiation_m051D2B3AE63C63CE23B1344D828C34A542D50E55 ();
// 0x0000003D System.Void Byn.Awrtc.Base.AWebRtcPeer::.ctor(Byn.Awrtc.ConnectionId)
extern void AWebRtcPeer__ctor_m3770632D7814AB297888BD614FD81ACF41F8E40A ();
// 0x0000003E System.Boolean Byn.Awrtc.Base.AWebRtcPeer::SetupPeer(Byn.Awrtc.IceServer[])
extern void AWebRtcPeer_SetupPeer_mEEB03690D4EA6622B3E03542E52589187F9FE17F ();
// 0x0000003F System.Boolean Byn.Awrtc.Base.AWebRtcPeer::ForwardSetupPeer(Byn.Awrtc.IceServer[])
// 0x00000040 System.Void Byn.Awrtc.Base.AWebRtcPeer::AddSignalingMessage(System.String)
extern void AWebRtcPeer_AddSignalingMessage_mC94B503708DAC762327774881900062C3FD3EAEE ();
// 0x00000041 System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardSignalingToPeer(System.String)
// 0x00000042 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::DequeueSignalingMessage(System.String&)
extern void AWebRtcPeer_DequeueSignalingMessage_m51BD0A84AEAD300EAC7A67784E6EA7C338B87A84 ();
// 0x00000043 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::DequeueEvent(Byn.Awrtc.NetworkEvent&)
extern void AWebRtcPeer_DequeueEvent_mC6A7476426FFFA92147DFDA89FC5DCD742B8338D ();
// 0x00000044 System.Void Byn.Awrtc.Base.AWebRtcPeer::Update()
// 0x00000045 System.Void Byn.Awrtc.Base.AWebRtcPeer::EnqueueOutgoingEvent(Byn.Awrtc.NetworkEvent)
extern void AWebRtcPeer_EnqueueOutgoingEvent_m33CBB9BA423CBD569516BFB5D6D69323B033EABD ();
// 0x00000046 System.Void Byn.Awrtc.Base.AWebRtcPeer::StartSignaling()
extern void AWebRtcPeer_StartSignaling_m622397FB1F92A6D9B36FCDA0E821CAC5C7C83DB8 ();
// 0x00000047 System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardStartSignaling()
// 0x00000048 System.Void Byn.Awrtc.Base.AWebRtcPeer::L(System.String)
extern void AWebRtcPeer_L_m4743CA7D2A506070046EC36370B22507C0CDAC3F ();
// 0x00000049 System.Void Byn.Awrtc.Base.AWebRtcPeer::LW(System.String)
extern void AWebRtcPeer_LW_m2FB5E27C57AF5AFBDAC6B91ABB7191CAC38A6F11 ();
// 0x0000004A System.Void Byn.Awrtc.Base.AWebRtcPeer::LE(System.String)
extern void AWebRtcPeer_LE_m1D17CB5217FEC61E746C8E564A4E1027E2FDCCA5 ();
// 0x0000004B System.Void Byn.Awrtc.Base.AWebRtcPeer::Dispose()
extern void AWebRtcPeer_Dispose_mDF40D658BCDCB168C30C4C89BFF93FC710AF5AF2 ();
// 0x0000004C System.Void Byn.Awrtc.Base.AWebRtcPeer::.cctor()
extern void AWebRtcPeer__cctor_mF95C87A4CEBCA312CDDFB2BB64EC8D04C182F582 ();
// 0x0000004D System.Void Byn.Awrtc.Base.WebsocketCloseStatus::.cctor()
extern void WebsocketCloseStatus__cctor_m8E2BB6F9E1FC5902E2C5F089AE980683EBE6D602 ();
// 0x0000004E System.Void Byn.Awrtc.Base.AWebsocketFactory::SetDefault(Byn.Awrtc.Base.AWebsocketFactory)
extern void AWebsocketFactory_SetDefault_m34C84E84A4811100B5EFE240D9353760A01FC08C ();
// 0x0000004F Byn.Awrtc.Base.AWebsocketFactory Byn.Awrtc.Base.AWebsocketFactory::get_DefaultFactory()
extern void AWebsocketFactory_get_DefaultFactory_m2496EB27AC9142431F689AE83A3CFEF37E9B4C9A ();
// 0x00000050 Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Base.AWebsocketFactory::CreateDefault()
extern void AWebsocketFactory_CreateDefault_m9454E4F1303E29B16E85C5C640E8D1EE98956BD0 ();
// 0x00000051 Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Base.AWebsocketFactory::Create()
// 0x00000052 System.Void Byn.Awrtc.Base.AWebsocketFactory::Dispose(System.Boolean)
extern void AWebsocketFactory_Dispose_m90CA34DC58585E563696597BDAC433C750C45E36 ();
// 0x00000053 System.Void Byn.Awrtc.Base.AWebsocketFactory::Dispose()
extern void AWebsocketFactory_Dispose_m860324B078C9CE7FC763406B95DB3A281DCFF0FD ();
// 0x00000054 System.Void Byn.Awrtc.Base.AWebsocketFactory::.ctor()
extern void AWebsocketFactory__ctor_m1B78EEEC033423135B0E60C57EAD6F97BB9925A3 ();
// 0x00000055 System.Boolean Byn.Awrtc.Base.IInternalMediaStream::Setup(System.String&)
// 0x00000056 System.Boolean Byn.Awrtc.Base.IInternalMediaStream::HasAudioTrack()
// 0x00000057 System.Boolean Byn.Awrtc.Base.IInternalMediaStream::HasVideoTrack()
// 0x00000058 System.Void Byn.Awrtc.Base.IInternalMediaStream::SetVolume(System.Double)
// 0x00000059 System.Boolean Byn.Awrtc.Base.IInternalMediaStream::IsMute()
// 0x0000005A System.Void Byn.Awrtc.Base.IInternalMediaStream::SetMute(System.Boolean)
// 0x0000005B Byn.Awrtc.IFrame Byn.Awrtc.Base.IInternalMediaStream::TryGetFrame()
// 0x0000005C Byn.Awrtc.Base.PeerConnectionState Byn.Awrtc.Base.IInternalPeer::get_ConnectionState()
// 0x0000005D Byn.Awrtc.Base.PeerSignalingState Byn.Awrtc.Base.IInternalPeer::get_SignalingState()
// 0x0000005E Byn.Awrtc.ConnectionId Byn.Awrtc.Base.IInternalPeer::get_ConnectionId()
// 0x0000005F Byn.Awrtc.Base.SignalingInfo Byn.Awrtc.Base.IInternalPeer::get_SignalingInfo()
// 0x00000060 System.Void Byn.Awrtc.Base.IInternalPeer::set_SignalingInfo(Byn.Awrtc.Base.SignalingInfo)
// 0x00000061 System.Boolean Byn.Awrtc.Base.IInternalPeer::SetupPeer(Byn.Awrtc.IceServer[])
// 0x00000062 System.Int32 Byn.Awrtc.Base.IInternalPeer::get_LocalId()
// 0x00000063 System.Boolean Byn.Awrtc.Base.IInternalPeer::get_WasRenegotiationRequested()
// 0x00000064 System.Void Byn.Awrtc.Base.IInternalPeer::NegotiateSignaling()
// 0x00000065 System.Void Byn.Awrtc.Base.IInternalPeer::AddSignalingMessage(System.String)
// 0x00000066 System.Boolean Byn.Awrtc.Base.IInternalPeer::DequeueSignalingMessage(System.String&)
// 0x00000067 System.Void Byn.Awrtc.Base.IInternalPeer::Dispose()
// 0x00000068 System.Void Byn.Awrtc.Base.IInternalPeer::StartSignaling()
// 0x00000069 System.Void Byn.Awrtc.Base.IInternalPeer::Update()
// 0x0000006A System.Boolean Byn.Awrtc.Base.IInternalPeer::DequeueEvent(Byn.Awrtc.NetworkEvent&)
// 0x0000006B System.Boolean Byn.Awrtc.Base.IDataPeer::SendData(System.Byte[],System.Int32,System.Int32,System.Boolean)
// 0x0000006C Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Base.IMediaPeer::get_RemoteStream()
// 0x0000006D System.Void Byn.Awrtc.Base.IMediaPeer::AddLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
// 0x0000006E System.Void Byn.Awrtc.Base.IMediaPeer::RemoveLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
// 0x0000006F System.Void Byn.Awrtc.Base.OnErrorCallback::.ctor(System.Object,System.IntPtr)
extern void OnErrorCallback__ctor_m96D63379478B9BC8A1B08503CE403B5379C696EE ();
// 0x00000070 System.Void Byn.Awrtc.Base.OnErrorCallback::Invoke(System.Object,System.String)
extern void OnErrorCallback_Invoke_mDD90ABE8218D6B575A4FF374DAE29966B6138F63 ();
// 0x00000071 System.IAsyncResult Byn.Awrtc.Base.OnErrorCallback::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern void OnErrorCallback_BeginInvoke_m3CC30ABDD6493927E96C1BA93F3B76A0441C0538 ();
// 0x00000072 System.Void Byn.Awrtc.Base.OnErrorCallback::EndInvoke(System.IAsyncResult)
extern void OnErrorCallback_EndInvoke_m6A84C7265E7C59F80E289DA2610201D3F882D797 ();
// 0x00000073 System.Void Byn.Awrtc.Base.OnMessageCallback::.ctor(System.Object,System.IntPtr)
extern void OnMessageCallback__ctor_m24FF3E6F3458C8D812BE5087FC8077749CE68D52 ();
// 0x00000074 System.Void Byn.Awrtc.Base.OnMessageCallback::Invoke(System.Object,System.Byte[])
extern void OnMessageCallback_Invoke_m44A0B96055F87CA6D81DFC8AFA2FEC986007F54B ();
// 0x00000075 System.IAsyncResult Byn.Awrtc.Base.OnMessageCallback::BeginInvoke(System.Object,System.Byte[],System.AsyncCallback,System.Object)
extern void OnMessageCallback_BeginInvoke_mCF0F75A6FB4E76FC677A2DB7AE1F2E4A9F06482B ();
// 0x00000076 System.Void Byn.Awrtc.Base.OnMessageCallback::EndInvoke(System.IAsyncResult)
extern void OnMessageCallback_EndInvoke_m20A8A8FF3067F5C4EB1E35EB7C2740A9B57451EB ();
// 0x00000077 System.Void Byn.Awrtc.Base.OnTextMessageCallback::.ctor(System.Object,System.IntPtr)
extern void OnTextMessageCallback__ctor_m2D4BAA74D9714BEB1CCF5CE9623D8C768DA25ED1 ();
// 0x00000078 System.Void Byn.Awrtc.Base.OnTextMessageCallback::Invoke(System.Object,System.String)
extern void OnTextMessageCallback_Invoke_m609908CD182EC5746E645B8B41F0471BC0F28943 ();
// 0x00000079 System.IAsyncResult Byn.Awrtc.Base.OnTextMessageCallback::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern void OnTextMessageCallback_BeginInvoke_m804B1FA73FF831582FBE9E2260505DC1F7D4B5DB ();
// 0x0000007A System.Void Byn.Awrtc.Base.OnTextMessageCallback::EndInvoke(System.IAsyncResult)
extern void OnTextMessageCallback_EndInvoke_m1AB629B9DE76697651FBEE6F3BA8C0EE7BA97E88 ();
// 0x0000007B System.Void Byn.Awrtc.Base.OnOpenCallback::.ctor(System.Object,System.IntPtr)
extern void OnOpenCallback__ctor_mD2350781A7FA5A5616222574DD01B0651018D546 ();
// 0x0000007C System.Void Byn.Awrtc.Base.OnOpenCallback::Invoke(System.Object)
extern void OnOpenCallback_Invoke_mDEFD94ECDF4CCA84DF0CC9CEEF433593C12F3101 ();
// 0x0000007D System.IAsyncResult Byn.Awrtc.Base.OnOpenCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void OnOpenCallback_BeginInvoke_m7DDFD0D1E2379672B3A79F1CB10658E2B7A671B8 ();
// 0x0000007E System.Void Byn.Awrtc.Base.OnOpenCallback::EndInvoke(System.IAsyncResult)
extern void OnOpenCallback_EndInvoke_m84CCAD373CFB796B321C03E0256C42B1211F30A4 ();
// 0x0000007F System.Void Byn.Awrtc.Base.OnCloseCallback::.ctor(System.Object,System.IntPtr)
extern void OnCloseCallback__ctor_mE017A8AA9D1B28111E0E29163D45C8FF68F20512 ();
// 0x00000080 System.Void Byn.Awrtc.Base.OnCloseCallback::Invoke(System.Object,System.Int32,System.String)
extern void OnCloseCallback_Invoke_m817040E0E3742F0FEE2D5BC045E49AD3CCDBF40A ();
// 0x00000081 System.IAsyncResult Byn.Awrtc.Base.OnCloseCallback::BeginInvoke(System.Object,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void OnCloseCallback_BeginInvoke_m2327A5ABFA5D7037B6B3F8D6BE3973F34BB08C38 ();
// 0x00000082 System.Void Byn.Awrtc.Base.OnCloseCallback::EndInvoke(System.IAsyncResult)
extern void OnCloseCallback_EndInvoke_mB1FFEB90AC86317D8E1281CCCA150C411BAF72A0 ();
// 0x00000083 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
// 0x00000084 System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
// 0x00000085 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnClose(Byn.Awrtc.Base.OnCloseCallback)
// 0x00000086 System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnClose(Byn.Awrtc.Base.OnCloseCallback)
// 0x00000087 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnError(Byn.Awrtc.Base.OnErrorCallback)
// 0x00000088 System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnError(Byn.Awrtc.Base.OnErrorCallback)
// 0x00000089 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
// 0x0000008A System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
// 0x0000008B System.Void Byn.Awrtc.Base.IWebsocketClient::Connect(System.String)
// 0x0000008C System.Void Byn.Awrtc.Base.IWebsocketClient::Send(System.Byte[])
// 0x0000008D System.Void Byn.Awrtc.Base.SignalingConfig::.ctor(Byn.Awrtc.IBasicNetwork)
extern void SignalingConfig__ctor_m87328F1C95932D3959DD20D917DAEE86705F2475 ();
// 0x0000008E Byn.Awrtc.IBasicNetwork Byn.Awrtc.Base.SignalingConfig::get_Network()
extern void SignalingConfig_get_Network_mFAF7513CB47560EEDCE0A3AC3604BDE8252A08C4 ();
// 0x0000008F System.Boolean Byn.Awrtc.Base.SignalingConfig::get_KeepSignalingAlive()
extern void SignalingConfig_get_KeepSignalingAlive_mE9838F905F507514430AF989E54D43AD9DBE70E0 ();
// 0x00000090 System.Void Byn.Awrtc.Base.SignalingConfig::set_KeepSignalingAlive(System.Boolean)
extern void SignalingConfig_set_KeepSignalingAlive_m08ED64BE301FAF17BE50EC8FF17577F77EF39D73 ();
// 0x00000091 System.Int32 Byn.Awrtc.Base.SignalingConfig::get_SignalingTimeout()
extern void SignalingConfig_get_SignalingTimeout_mBCC195A942B99DB69449B7490D99E9B6F0226338 ();
// 0x00000092 System.Void Byn.Awrtc.Base.SignalingConfig::Lock()
extern void SignalingConfig_Lock_mEDAD13BB9F475FA752B8C585E1D389B9E8044E7E ();
// 0x00000093 System.Boolean Byn.Awrtc.Base.SignalingInfo::get_IsSignalingConnected()
extern void SignalingInfo_get_IsSignalingConnected_mFAB6881A7305554550BB10C0FD5D6FCCB1C68A7F ();
// 0x00000094 System.Boolean Byn.Awrtc.Base.SignalingInfo::get_IsIncoming()
extern void SignalingInfo_get_IsIncoming_mFB0F9E83C41C2CE0E9732FF19E2ECB3B736E656C ();
// 0x00000095 System.Int32 Byn.Awrtc.Base.SignalingInfo::get_SignalingDuration()
extern void SignalingInfo_get_SignalingDuration_m393189F9D33AEE593682FB1E5684F6B20057B06D ();
// 0x00000096 System.Void Byn.Awrtc.Base.SignalingInfo::.ctor(Byn.Awrtc.ConnectionId,System.Boolean,System.DateTime)
extern void SignalingInfo__ctor_m7B570D22506DD398F7E1D3B304FB017828A14B7C ();
// 0x00000097 System.Void Byn.Awrtc.Base.SignalingInfo::SignalingDisconnected()
extern void SignalingInfo_SignalingDisconnected_mC2F0E38B469266203D686D7CE7AFF1D910EB6EA9 ();
// 0x00000098 System.Void Byn.Awrtc.Base.WebsocketNetwork::.ctor(System.String,Byn.Awrtc.Base.Configuration)
extern void WebsocketNetwork__ctor_m6FACE7667DB029AFBA2C430D12D96973FDB81EFC ();
// 0x00000099 System.Void Byn.Awrtc.Base.WebsocketNetwork::WebsocketConnect()
extern void WebsocketNetwork_WebsocketConnect_m35C0D308F5DD3F7485D98A80A5D49F88CF8516AB ();
// 0x0000009A System.Void Byn.Awrtc.Base.WebsocketNetwork::WebsocketCleanup()
extern void WebsocketNetwork_WebsocketCleanup_m63A51D0FFCF89AF803C7EA7B6B2268E2621DBB69 ();
// 0x0000009B System.Void Byn.Awrtc.Base.WebsocketNetwork::EnsureServerConnection()
extern void WebsocketNetwork_EnsureServerConnection_mA3553F7012E9305795E57BB6A9EF9EACFFAEBEA9 ();
// 0x0000009C System.Void Byn.Awrtc.Base.WebsocketNetwork::UpdateHeartbeat()
extern void WebsocketNetwork_UpdateHeartbeat_m89DA3DB5041791F3CE32C366FA8F9C5743D79F32 ();
// 0x0000009D System.Void Byn.Awrtc.Base.WebsocketNetwork::TriggerHeartbeatTimeout()
extern void WebsocketNetwork_TriggerHeartbeatTimeout_m037CF4766959D659152C0A0F4F3040DE58031493 ();
// 0x0000009E System.Void Byn.Awrtc.Base.WebsocketNetwork::CheckSleep()
extern void WebsocketNetwork_CheckSleep_m47582BA84B60F17610051E349B56397F541F06B9 ();
// 0x0000009F System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnOpen(System.Object)
extern void WebsocketNetwork_OnWebsocketOnOpen_mF9B225C134DCE6E4326B8EA3F1E439E9307927CC ();
// 0x000000A0 System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnClose(System.Object,System.Int32,System.String)
extern void WebsocketNetwork_OnWebsocketOnClose_m14A54E85C1BAC89BD7DCCEA28A814FEB45FBC3EC ();
// 0x000000A1 System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnError(System.Object,System.String)
extern void WebsocketNetwork_OnWebsocketOnError_m30A7B8A575648EAD27626DC3604B81E957FC23C2 ();
// 0x000000A2 System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnMessage(System.Object,System.Byte[])
extern void WebsocketNetwork_OnWebsocketOnMessage_m1525A69C3122B7AA418658529AA6DAE065B80FB8 ();
// 0x000000A3 System.Void Byn.Awrtc.Base.WebsocketNetwork::Cleanup(System.String)
extern void WebsocketNetwork_Cleanup_mE64AEFE47D2E9214D7BB87AA431275556EECEEE4 ();
// 0x000000A4 System.Void Byn.Awrtc.Base.WebsocketNetwork::EnqueueOutgoing(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_EnqueueOutgoing_m4F90E5C91A3B516B39C4C7BC81E1F20BDD3E417F ();
// 0x000000A5 System.Void Byn.Awrtc.Base.WebsocketNetwork::EnqueueIncoming(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_EnqueueIncoming_mB890BB2416C92287012429464E7830D38FAC1653 ();
// 0x000000A6 System.Void Byn.Awrtc.Base.WebsocketNetwork::TryRemoveConnecting(Byn.Awrtc.ConnectionId)
extern void WebsocketNetwork_TryRemoveConnecting_m853A0CFD2E2CE0E1113B0F8CF87B83F2FEC2CA39 ();
// 0x000000A7 System.Void Byn.Awrtc.Base.WebsocketNetwork::ParseMessage(System.Byte[])
extern void WebsocketNetwork_ParseMessage_m45241C9840C7BE90485EE230AE246C43357F350C ();
// 0x000000A8 System.Void Byn.Awrtc.Base.WebsocketNetwork::HandleIncomingEvent(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_HandleIncomingEvent_mF5366AFF6AEB374EA4E0DEA68CF88A895349B3A8 ();
// 0x000000A9 System.Void Byn.Awrtc.Base.WebsocketNetwork::HandleOutgoingEvents()
extern void WebsocketNetwork_HandleOutgoingEvents_mBBB4B350008161643350B8C5715073ABACA74820 ();
// 0x000000AA System.Void Byn.Awrtc.Base.WebsocketNetwork::SendHeartbeat()
extern void WebsocketNetwork_SendHeartbeat_m74A24E0CA289806CAE9CEEF076FA593AAA319C18 ();
// 0x000000AB System.Void Byn.Awrtc.Base.WebsocketNetwork::SendVersion()
extern void WebsocketNetwork_SendVersion_m0ED5F8B010CFC0729B164F520CDD3FD0F8782D2C ();
// 0x000000AC System.Void Byn.Awrtc.Base.WebsocketNetwork::SendNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_SendNetworkEvent_m4921195C67476F0E89D1FDD8AAC3714DD69038D6 ();
// 0x000000AD System.Void Byn.Awrtc.Base.WebsocketNetwork::InternalSend(System.Byte[])
extern void WebsocketNetwork_InternalSend_m2E6746AF02E50DFC0A3B80E8B9D150F9903D040F ();
// 0x000000AE Byn.Awrtc.ConnectionId Byn.Awrtc.Base.WebsocketNetwork::NextConnectionId()
extern void WebsocketNetwork_NextConnectionId_mA1A3AF06FAF002A0BB6C998F7B7DC09C39B230B6 ();
// 0x000000AF System.String Byn.Awrtc.Base.WebsocketNetwork::GetRandomKey()
extern void WebsocketNetwork_GetRandomKey_m43464673B138B1F6F761267B40C78B07A8B31B0A ();
// 0x000000B0 System.Boolean Byn.Awrtc.Base.WebsocketNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void WebsocketNetwork_Dequeue_m09AEC5A8D2617EBD7FA847496F69B6AFC557E5EF ();
// 0x000000B1 System.Void Byn.Awrtc.Base.WebsocketNetwork::Update()
extern void WebsocketNetwork_Update_m22D03EC7378A552278309E807A3395D462A57B14 ();
// 0x000000B2 System.Void Byn.Awrtc.Base.WebsocketNetwork::Flush()
extern void WebsocketNetwork_Flush_mC49F193DCB4AF43EC302364B551078E579A5C1AC ();
// 0x000000B3 System.Boolean Byn.Awrtc.Base.WebsocketNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void WebsocketNetwork_SendData_mF8DEB2357A16D6B3BD146D0AA7E6CB936B2CFED3 ();
// 0x000000B4 System.Void Byn.Awrtc.Base.WebsocketNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void WebsocketNetwork_Disconnect_m040AEA21666255B9A62AA00DAE32CEBAD8CF4E4D ();
// 0x000000B5 System.Void Byn.Awrtc.Base.WebsocketNetwork::Shutdown()
extern void WebsocketNetwork_Shutdown_mBC3F8A10639E8A18077F7355CF4DBAD5EF176570 ();
// 0x000000B6 System.Void Byn.Awrtc.Base.WebsocketNetwork::StartServer(System.String)
extern void WebsocketNetwork_StartServer_mFAFF4B9B8D71AA860336C4B7E37473DB7DF27B8F ();
// 0x000000B7 System.Void Byn.Awrtc.Base.WebsocketNetwork::StopServer()
extern void WebsocketNetwork_StopServer_m24DA49D309F697F530A810BEB9373E7F91F43B66 ();
// 0x000000B8 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.WebsocketNetwork::Connect(System.String)
extern void WebsocketNetwork_Connect_m478AA2496960CAE43BA2846F8C50162FA81FD94D ();
// 0x000000B9 System.Void Byn.Awrtc.Base.WebsocketNetwork::Dispose(System.Boolean)
extern void WebsocketNetwork_Dispose_mE88A90E267B6CB7B3729EA231DD685C73E5E83C9 ();
// 0x000000BA System.Void Byn.Awrtc.Base.WebsocketNetwork::Dispose()
extern void WebsocketNetwork_Dispose_m90C7458C5A067D5B8AFADEE090195E39F31F413B ();
// 0x000000BB System.Void Byn.Awrtc.Base.WebsocketNetwork::.cctor()
extern void WebsocketNetwork__cctor_mA443EC062CDD67EB72B3EF18DF062269189C75AC ();
// 0x000000BC System.Int32 Byn.Awrtc.Base.Configuration::get_Heartbeat()
extern void Configuration_get_Heartbeat_m3C5E972C16BFBA0B4CD0D6998FE428F403006622 ();
// 0x000000BD System.Void Byn.Awrtc.Base.Configuration::Lock()
extern void Configuration_Lock_mF117DD2D4E37602A9E6B7D1FC3B995C878D0EB9A ();
// 0x000000BE System.Void Byn.Awrtc.Base.Configuration::.ctor()
extern void Configuration__ctor_m101F4E032635050719AC9290598387FD4E033759 ();
// 0x000000BF System.Byte[] Byn.Awrtc.Native.DirectMemoryFrame::get_Buffer()
extern void DirectMemoryFrame_get_Buffer_mDCD2214669EC4787943F54752745D6E6E21169A7 ();
// 0x000000C0 Byn.Awrtc.FramePixelFormat Byn.Awrtc.Native.DirectMemoryFrame::get_Format()
extern void DirectMemoryFrame_get_Format_m7FB036D3AB885152D6B6FD15EBEDD0440CEA119F ();
// 0x000000C1 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Height()
extern void DirectMemoryFrame_get_Height_m1DC605521D4F38A07FD13EF9C2E0C1B2863CA8AB ();
// 0x000000C2 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Width()
extern void DirectMemoryFrame_get_Width_m08E76B7E7656BEC35A3BF8A3EABAE0F2AEFBF123 ();
// 0x000000C3 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Rotation()
extern void DirectMemoryFrame_get_Rotation_m20CA63BD694C6DF30AA3EF9C33A3486A845866FF ();
// 0x000000C4 System.Void Byn.Awrtc.Native.DirectMemoryFrame::.ctor(WebRtcCSharp.PollingMediaStreamRef)
extern void DirectMemoryFrame__ctor_mCD6C7EA1F1B0E266576F3B5A703F5AC1E141F86D ();
// 0x000000C5 System.IntPtr Byn.Awrtc.Native.DirectMemoryFrame::GetIntPtr()
extern void DirectMemoryFrame_GetIntPtr_mC84E802FAE1D9EEF822845987B61DDB9BA65E803 ();
// 0x000000C6 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::GetSize()
extern void DirectMemoryFrame_GetSize_mB598C1CAC9F1088AC34EE4E27AE8AEFECB4AEAE3 ();
// 0x000000C7 System.Void Byn.Awrtc.Native.DirectMemoryFrame::Dispose()
extern void DirectMemoryFrame_Dispose_m8F24773E5627665DCF18DA974E2D03F213758466 ();
// 0x000000C8 System.Void Byn.Awrtc.Native.InternalDataPeer::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.Native.NativeAwrtcFactory)
extern void InternalDataPeer__ctor_mDE20C07141E9D65ABD2014E4A36271F834A5FA1D ();
// 0x000000C9 System.Boolean Byn.Awrtc.Native.InternalDataPeer::ForwardSetupPeer(Byn.Awrtc.IceServer[])
extern void InternalDataPeer_ForwardSetupPeer_mE379F64AD9D83140F21D2C4D8F3C20753CAED58B ();
// 0x000000CA System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardSignalingToPeer(System.String)
extern void InternalDataPeer_ForwardSignalingToPeer_m18946E088BE76A35C66D8BE21CB99908858FF2AD ();
// 0x000000CB System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardStartSignaling()
extern void InternalDataPeer_ForwardStartSignaling_m123DC5F9842F6B20EE2D41E22F1CADF3DF3D31ED ();
// 0x000000CC System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardOnDispose()
extern void InternalDataPeer_ForwardOnDispose_mFABCD84D0ADA265AE28B710B882B8EDCB10D347A ();
// 0x000000CD System.Void Byn.Awrtc.Native.InternalDataPeer::Update()
extern void InternalDataPeer_Update_m3FAECE252B6B156F15A74EA09B78002380AFEFD9 ();
// 0x000000CE Byn.Awrtc.MessageDataBuffer Byn.Awrtc.Native.InternalDataPeer::ToMessageDataBuffer(WebRtcCSharp.DataBuffer)
extern void InternalDataPeer_ToMessageDataBuffer_mB26AD84834BF57F5FD83ED913C3450AD4C353DE2 ();
// 0x000000CF System.Boolean Byn.Awrtc.Native.InternalDataPeer::SendData(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void InternalDataPeer_SendData_m8A1F696EDF98DCBB72864FACFC35D28C13F71402 ();
// 0x000000D0 System.Void Byn.Awrtc.Native.InternalDataPeer::.cctor()
extern void InternalDataPeer__cctor_m6E35A0983AAFE505468BB8819E986C87F47EFE5D ();
// 0x000000D1 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Native.InternalMediaPeer::get_RemoteStream()
extern void InternalMediaPeer_get_RemoteStream_m0DD6A960F7A7F3C48B2457A3EB9FA31595FFA613 ();
// 0x000000D2 System.Void Byn.Awrtc.Native.InternalMediaPeer::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig)
extern void InternalMediaPeer__ctor_m2D5468384DFE91AE6DBFAE119D550C856B17A9A6 ();
// 0x000000D3 System.Void Byn.Awrtc.Native.InternalMediaPeer::AddLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
extern void InternalMediaPeer_AddLocalStream_m1A9DA2AA2F7EC94307870618E894648D47CF70A4 ();
// 0x000000D4 System.Void Byn.Awrtc.Native.InternalMediaPeer::RemoveLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
extern void InternalMediaPeer_RemoveLocalStream_m1FA730268ACAB98894BF86A8DE422C520435D044 ();
// 0x000000D5 Byn.Awrtc.IFrame Byn.Awrtc.Native.InternalMediaPeer::FrameToi420Buffer(WebRtcCSharp.PollingMediaStreamRef)
extern void InternalMediaPeer_FrameToi420Buffer_m2434136123E58B7514D866EC28FA570FC83E8158 ();
// 0x000000D6 System.Void Byn.Awrtc.Native.InternalMediaPeer::FrameToBuffer(WebRtcCSharp.PollingMediaStreamRef,Byn.Awrtc.BufferedFrame&)
extern void InternalMediaPeer_FrameToBuffer_mAB977D5CDD79492223A0C9B7E8BF67C81FF4AFD8 ();
// 0x000000D7 WebRtcCSharp.PollingMediaStreamRef Byn.Awrtc.Native.InternalMediaStream::GetInternalStream()
extern void InternalMediaStream_GetInternalStream_m77A38AF3338C96E9A7AA003DBF8E856EE249F4E8 ();
// 0x000000D8 System.Void Byn.Awrtc.Native.InternalMediaStream::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig)
extern void InternalMediaStream__ctor_m40672E050F624BA1E5A20EF23A5F6C4A162C1897 ();
// 0x000000D9 System.Void Byn.Awrtc.Native.InternalMediaStream::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig,WebRtcCSharp.PollingMediaStreamRef)
extern void InternalMediaStream__ctor_m8DC4E3CC7E01442F99110D51879D93799171905D ();
// 0x000000DA System.Boolean Byn.Awrtc.Native.InternalMediaStream::Setup(System.String&)
extern void InternalMediaStream_Setup_m73AE2F4CCBD13B0A8D1F55946E766C84EA615B03 ();
// 0x000000DB System.Void Byn.Awrtc.Native.InternalMediaStream::Dispose()
extern void InternalMediaStream_Dispose_mE2FD4EA58907064B6439AF5486FB8A8567539CD5 ();
// 0x000000DC System.Boolean Byn.Awrtc.Native.InternalMediaStream::IsMute()
extern void InternalMediaStream_IsMute_m7DF42B4096B4A4365098DDA49B3BD8E48C0D476F ();
// 0x000000DD System.Void Byn.Awrtc.Native.InternalMediaStream::SetMute(System.Boolean)
extern void InternalMediaStream_SetMute_mFC98A6852DA06BF90A38A60D957EB5C8E6549672 ();
// 0x000000DE System.Void Byn.Awrtc.Native.InternalMediaStream::SetVolume(System.Double)
extern void InternalMediaStream_SetVolume_mA291EF5D5EF938F92BBEBC9A2D989F28ACF5C52D ();
// 0x000000DF Byn.Awrtc.IFrame Byn.Awrtc.Native.InternalMediaStream::TryGetFrame()
extern void InternalMediaStream_TryGetFrame_m068DA5667A59F8F876B541EBFD7EEA1C0E2E02CB ();
// 0x000000E0 System.Boolean Byn.Awrtc.Native.InternalMediaStream::HasAudioTrack()
extern void InternalMediaStream_HasAudioTrack_m305F2F4577AFBB0CD7DF45B545C16C2F72426DF4 ();
// 0x000000E1 System.Boolean Byn.Awrtc.Native.InternalMediaStream::HasVideoTrack()
extern void InternalMediaStream_HasVideoTrack_m1D90BA32DC72457274ECDF3E2C9DD836CB92D9B4 ();
// 0x000000E2 Byn.Awrtc.Native.NativeAudioOptions Byn.Awrtc.Native.NativeMediaConfig::get_AudioOptions()
extern void NativeMediaConfig_get_AudioOptions_m9ADF3E35B3729F865568DC50468BC8DAF3F36EA7 ();
// 0x000000E3 System.Void Byn.Awrtc.Native.NativeMediaConfig::.ctor()
extern void NativeMediaConfig__ctor_mB9F397AEC11994F8C4124534CB755CEF50AC8EFB ();
// 0x000000E4 System.Void Byn.Awrtc.Native.NativeMediaConfig::.ctor(Byn.Awrtc.Native.NativeMediaConfig)
extern void NativeMediaConfig__ctor_m8DDBBB51904ECE5AF9D4FA5448FADB7D4E8D213B ();
// 0x000000E5 Byn.Awrtc.MediaConfig Byn.Awrtc.Native.NativeMediaConfig::DeepClone()
extern void NativeMediaConfig_DeepClone_m9669A2569E0F39BCFF39C68BB839DD5CD08D0F06 ();
// 0x000000E6 System.String Byn.Awrtc.Native.NativeMediaConfig::ToString()
extern void NativeMediaConfig_ToString_mAA346BD0B0E0C1813B5A1FD718549E62AFBB45FE ();
// 0x000000E7 System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_echo_cancellation()
extern void NativeAudioOptions_get_echo_cancellation_mABDC0DD719C6A715B06C99B3F7E23BD870E525EB ();
// 0x000000E8 System.Void Byn.Awrtc.Native.NativeAudioOptions::set_echo_cancellation(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_echo_cancellation_m5FC8D6E5F3C0FF69ECD2380941E6098D35974FAB ();
// 0x000000E9 System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_extended_filter_aec()
extern void NativeAudioOptions_get_extended_filter_aec_m169157ED12BC59E33700869867B1E4BD07EC0562 ();
// 0x000000EA System.Void Byn.Awrtc.Native.NativeAudioOptions::set_extended_filter_aec(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_extended_filter_aec_m3C06B76A99D2AA0BDF8C5CBF1088122844209769 ();
// 0x000000EB System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_delay_agnostic_aec()
extern void NativeAudioOptions_get_delay_agnostic_aec_m86E8B33D54896B83C6A3587FD2C83A6035B985D6 ();
// 0x000000EC System.Void Byn.Awrtc.Native.NativeAudioOptions::set_delay_agnostic_aec(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_delay_agnostic_aec_m827ED9C9D137BD6C7320B5BD1FBE57AD3CDBE803 ();
// 0x000000ED System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_noise_suppression()
extern void NativeAudioOptions_get_noise_suppression_m97F9CD2B2179CDE80AB1DFA2B9F3F944663BBE97 ();
// 0x000000EE System.Void Byn.Awrtc.Native.NativeAudioOptions::set_noise_suppression(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_noise_suppression_mF1F73DC679130BA60AD7A88047F69865BC507C0D ();
// 0x000000EF System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_auto_gain_control()
extern void NativeAudioOptions_get_auto_gain_control_mB0C781379B93B8F67DB3E74D477F87948EF3010E ();
// 0x000000F0 System.Void Byn.Awrtc.Native.NativeAudioOptions::set_auto_gain_control(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_auto_gain_control_m3661652A13E07AC5BEE7D100ACB712194E6DF9BA ();
// 0x000000F1 System.Void Byn.Awrtc.Native.NativeAudioOptions::.ctor()
extern void NativeAudioOptions__ctor_m45178698EF84BFD27A71AED113F7A9EC8DF9A4EA ();
// 0x000000F2 System.Void Byn.Awrtc.Native.NativeAudioOptions::.ctor(Byn.Awrtc.Native.NativeAudioOptions)
extern void NativeAudioOptions__ctor_m3150DE6627933BA8251D5C7E0C821A5EBDA74B0E ();
// 0x000000F3 Byn.Awrtc.Native.NativeAudioOptions Byn.Awrtc.Native.NativeAudioOptions::DeepClone()
extern void NativeAudioOptions_DeepClone_m2DF6FD92F4E7158DF9CD9D46E2503F349016181F ();
// 0x000000F4 System.String Byn.Awrtc.Native.NativeAudioOptions::ToString()
extern void NativeAudioOptions_ToString_mC92E8AD34EF2A04BCBF8F5AC0E8AC5665974872E ();
// 0x000000F5 System.Void Byn.Awrtc.Native.NativeMediaNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[],Byn.Awrtc.Native.NativeAwrtcFactory)
extern void NativeMediaNetwork__ctor_m7C57266E19BF5D59EB974E232A2B40AEB88F4087 ();
// 0x000000F6 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Native.NativeMediaNetwork::CreateLocalStream(Byn.Awrtc.MediaConfig)
extern void NativeMediaNetwork_CreateLocalStream_m1B83149312D5C2434FFBA43D46373987483AF1E8 ();
// 0x000000F7 Byn.Awrtc.Base.IMediaPeer Byn.Awrtc.Native.NativeMediaNetwork::CreateMediaPeer(Byn.Awrtc.ConnectionId)
extern void NativeMediaNetwork_CreateMediaPeer_m172EAF10500F82DF806438E911B7C8B5EF0A8E6A ();
// 0x000000F8 System.Void Byn.Awrtc.Native.NativeMediaNetwork::Dispose(System.Boolean)
extern void NativeMediaNetwork_Dispose_m646C58798AC5AAB10FB14CB69568461E645E1745 ();
// 0x000000F9 System.Boolean Byn.Awrtc.Native.NativeMediaNetwork::IsMute()
extern void NativeMediaNetwork_IsMute_mD056C7A90348DAC4C0E30460AA2BC60E8D68AB71 ();
// 0x000000FA System.Void Byn.Awrtc.Native.NativeMediaNetwork::SetMute(System.Boolean)
extern void NativeMediaNetwork_SetMute_m26FBE88D7678C2D2481C5632D53B295CD40817A4 ();
// 0x000000FB System.Void Byn.Awrtc.Native.NativeVideoInput::.ctor(WebRtcCSharp.VideoInputRef)
extern void NativeVideoInput__ctor_m50FD569D84A785ADA4C2454F65A041D80962EF16 ();
// 0x000000FC System.Void Byn.Awrtc.Native.NativeVideoInput::AddDevice(System.String,System.Int32,System.Int32,System.Int32)
extern void NativeVideoInput_AddDevice_m050949A8BFC7CED677CC337733ACFC34CAA4AAFB ();
// 0x000000FD System.Void Byn.Awrtc.Native.NativeVideoInput::RemoveDevice(System.String)
extern void NativeVideoInput_RemoveDevice_m1A0B99607F0432B53D242F5F3558720038FD7F61 ();
// 0x000000FE System.Boolean Byn.Awrtc.Native.NativeVideoInput::UpdateFrame(System.String,System.Byte[],System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void NativeVideoInput_UpdateFrame_mC8C2827F1975D3D90A5B844958FD6862683B46F4 ();
// 0x000000FF System.Void Byn.Awrtc.Native.NativeVideoInput::Dispose()
extern void NativeVideoInput_Dispose_m66B2A4851C640D60B5795E2FB3F310301556F9E6 ();
// 0x00000100 System.Void Byn.Awrtc.Native.NativeWebRtcCall::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.NetworkConfig,Byn.Awrtc.IBasicNetwork)
extern void NativeWebRtcCall__ctor_m9E4ACDA1FD5BC24737701EC98EC2C09066E32C7F ();
// 0x00000101 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeWebRtcCall::CreateNetwork(Byn.Awrtc.IBasicNetwork)
extern void NativeWebRtcCall_CreateNetwork_m9F2C65ED13F2F3272369419D8D9CC0D200EF4A6B ();
// 0x00000102 System.Void Byn.Awrtc.Native.NativeWebRtcCall::Dispose(System.Boolean)
extern void NativeWebRtcCall_Dispose_mBA63D61AC46D1268883C802C47E1AA3A670CFC66 ();
// 0x00000103 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::get_IsInitialized()
extern void NativeAwrtcFactory_get_IsInitialized_m3CC0662D2B6B3917130372632DE195EF8A3FE52D ();
// 0x00000104 WebRtcCSharp.RTCPeerConnectionFactoryRef Byn.Awrtc.Native.NativeAwrtcFactory::get_NativeFactory()
extern void NativeAwrtcFactory_get_NativeFactory_m32621B5A525A0807AB4A8F6734B0525C30626B68 ();
// 0x00000105 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::add_LastCallDisposed(System.Action)
extern void NativeAwrtcFactory_add_LastCallDisposed_m62016E58BCC04727CDE6725BC6FB300618202DD0 ();
// 0x00000106 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::remove_LastCallDisposed(System.Action)
extern void NativeAwrtcFactory_remove_LastCallDisposed_m9848398596ABA19A59E1B740BA6C9CE5E6B1E399 ();
// 0x00000107 Byn.Awrtc.Native.NativeVideoInput Byn.Awrtc.Native.NativeAwrtcFactory::get_VideoInput()
extern void NativeAwrtcFactory_get_VideoInput_mB98E0A12070ECBB6D176BC3AB1DBEA997708E7CE ();
// 0x00000108 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::.ctor()
extern void NativeAwrtcFactory__ctor_m3A148969A7720A61C81C88E2037889F5102D39F9 ();
// 0x00000109 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Initialize()
extern void NativeAwrtcFactory_Initialize_m1D7913F69D65EB9888422598DC9FAB1D4E6D5238 ();
// 0x0000010A System.Void Byn.Awrtc.Native.NativeAwrtcFactory::TryStaticInit()
extern void NativeAwrtcFactory_TryStaticInit_mF9D597FE417E2DD0CC2F3489892642DAA05499B6 ();
// 0x0000010B System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetNativeLogLevel(WebRtcCSharp.LoggingSeverity)
extern void NativeAwrtcFactory_SetNativeLogLevel_m5D1C6FC698A7DD88DBAD82A55E3DF21153CB0E58 ();
// 0x0000010C System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetNativeLogLevelInternal(WebRtcCSharp.LoggingSeverity)
extern void NativeAwrtcFactory_SetNativeLogLevelInternal_mC54650010881356461711CAF610BB2163C485E47 ();
// 0x0000010D System.String[] Byn.Awrtc.Native.NativeAwrtcFactory::GetVideoDevices()
extern void NativeAwrtcFactory_GetVideoDevices_m9A0DB0741E58920FF06FB2A08BFB4B698276A514 ();
// 0x0000010E Byn.Awrtc.ICall Byn.Awrtc.Native.NativeAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig)
extern void NativeAwrtcFactory_CreateCall_m1A35DF0EEE3E1AAACC934A7F091DD0164CF0656F ();
// 0x0000010F Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
extern void NativeAwrtcFactory_CreateMediaNetwork_mFA938D8AAAFF1ADE41D72161ADB9ABE600989BE6 ();
// 0x00000110 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateBasicNetwork_m2CA1DAB88E39AD66B784296B4524797BD14CDE11 ();
// 0x00000111 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateBasicNetwork(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateBasicNetwork_m5E2BFE150199AFBA5ED1DD935B714EF7FA950095 ();
// 0x00000112 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateMediaNetwork_mA83A079C6EFE3B88EBC496E3D0B3ABAB02A5B551 ();
// 0x00000113 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnCallDisposed(Byn.Awrtc.Native.NativeWebRtcCall)
extern void NativeAwrtcFactory_OnCallDisposed_m56FBCA699DFF09F0DB2D83153C5B5684329C9069 ();
// 0x00000114 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnNetworkDisposed(Byn.Awrtc.Native.NativeMediaNetwork)
extern void NativeAwrtcFactory_OnNetworkDisposed_m5B7A8326C718C9A6BA60A0E9DC7C68820EB3317F ();
// 0x00000115 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnNetworkDestroyed(Byn.Awrtc.Native.NativeWebRtcNetwork)
extern void NativeAwrtcFactory_OnNetworkDestroyed_m75D9849C6888F9412E3FAF0BCC63C2E0364C10E5 ();
// 0x00000116 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Dispose(System.Boolean)
extern void NativeAwrtcFactory_Dispose_m8B378F96171D4DEE829AFE90A94E030FF947B629 ();
// 0x00000117 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Dispose()
extern void NativeAwrtcFactory_Dispose_m6E748D0CD68FBEE32EF9BA4BD492E7ECBD79470B ();
// 0x00000118 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::CanSelectVideoDevice()
extern void NativeAwrtcFactory_CanSelectVideoDevice_m6C872A66BE907452B857D9BC9E6E2DF75F031E25 ();
// 0x00000119 System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetVersion()
extern void NativeAwrtcFactory_GetVersion_m1EB3E0E4014576D50BF8B07F0901B36BC771F69E ();
// 0x0000011A System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetWrapperVersion()
extern void NativeAwrtcFactory_GetWrapperVersion_m22D166BA0EC7A5648DDF61B00C5B48ABCAC7B359 ();
// 0x0000011B System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetWebRtcVersion()
extern void NativeAwrtcFactory_GetWebRtcVersion_mB11C0A58D32C5046D08D3769A489D43E0C86AA32 ();
// 0x0000011C System.Void Byn.Awrtc.Native.NativeAwrtcFactory::.cctor()
extern void NativeAwrtcFactory__cctor_m3F6BAC234C84C0EE931CBFBB4C5CAF12143F1C85 ();
// 0x0000011D System.Void Byn.Awrtc.Native.NativeWebRtcCallFactory::.ctor()
extern void NativeWebRtcCallFactory__ctor_m6D7593918FC6BE2338B812992BFEF9B11DED8108 ();
// 0x0000011E System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[],Byn.Awrtc.Native.NativeAwrtcFactory)
extern void NativeWebRtcNetwork__ctor_m20461B73CB61E429613F6C0C2C7D8649FE11C58E ();
// 0x0000011F System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::TestValidity()
extern void NativeWebRtcNetwork_TestValidity_mC687EC03A742353ED9884E7171734BBF6D6B1659 ();
// 0x00000120 Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Native.NativeWebRtcNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
extern void NativeWebRtcNetwork_CreatePeer_mA17AD1DE62D0493E98C00E92888388A27785122A ();
// 0x00000121 System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::Dispose(System.Boolean)
extern void NativeWebRtcNetwork_Dispose_m097E8FCCBB86E6E032365A0F53F62FB568ADD25B ();
// 0x00000122 WebRtcCSharp.MediaConstraints Byn.Awrtc.Native.WrapperConverter::ConfigToConstraints(Byn.Awrtc.MediaConfig)
extern void WrapperConverter_ConfigToConstraints_m11D26F400103503BB9E1F330E3FF838ACE9E0706 ();
// 0x00000123 WebRtcCSharp.AudioOptions Byn.Awrtc.Native.WrapperConverter::ConfigToAudioOptions(Byn.Awrtc.MediaConfig)
extern void WrapperConverter_ConfigToAudioOptions_m05C99ECE35962021169D7D53209A8D881BE84563 ();
// 0x00000124 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor()
extern void CloseEventArgs__ctor_m0DB3174A34E5C1FAB57CD041EC377BC173D5D5BA ();
// 0x00000125 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(System.UInt16)
extern void CloseEventArgs__ctor_m04775AFEDB2C2DD19B433F857DF3A8727F348C96 ();
// 0x00000126 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.CloseStatusCode)
extern void CloseEventArgs__ctor_m68FBB73EFFDFF5B823B0C1DE33C9E8211B661F27 ();
// 0x00000127 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.PayloadData)
extern void CloseEventArgs__ctor_m9F3B718EFD4BA9E807C252FC26F7FAB43F5064A3 ();
// 0x00000128 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(System.UInt16,System.String)
extern void CloseEventArgs__ctor_m3057BBFC52F3D3CDF81D7D3CEE37ED995B01BCB8 ();
// 0x00000129 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void CloseEventArgs__ctor_m7A1B11454F002664E72E63C34AEF55C502DF1C10 ();
// 0x0000012A WebSocketSharpUnityMod.PayloadData WebSocketSharpUnityMod.CloseEventArgs::get_PayloadData()
extern void CloseEventArgs_get_PayloadData_m3D7BD5895F230AB46FBBCB907BC8638C2740EC4B ();
// 0x0000012B System.UInt16 WebSocketSharpUnityMod.CloseEventArgs::get_Code()
extern void CloseEventArgs_get_Code_mAE13B1503238D6F5A6D5ED02025FB23A7F57FC67 ();
// 0x0000012C System.String WebSocketSharpUnityMod.CloseEventArgs::get_Reason()
extern void CloseEventArgs_get_Reason_m8472BBF6835EB5EA28AD63DE02600D91627717E0 ();
// 0x0000012D System.Void WebSocketSharpUnityMod.CloseEventArgs::set_WasClean(System.Boolean)
extern void CloseEventArgs_set_WasClean_m3703AF4482EB348810472EC86643EA854D27786F ();
// 0x0000012E System.Void WebSocketSharpUnityMod.ErrorEventArgs::.ctor(System.String,System.Exception)
extern void ErrorEventArgs__ctor_m45623A0336FF95D9947A20E98DE2F3DC06BB7291 ();
// 0x0000012F System.String WebSocketSharpUnityMod.ErrorEventArgs::get_Message()
extern void ErrorEventArgs_get_Message_m8205FBF5DAA3F6607801B0C2DD52CC7064CF5157 ();
// 0x00000130 System.IO.MemoryStream WebSocketSharpUnityMod.Ext::compress(System.IO.Stream)
extern void Ext_compress_m46A8A1F7903A87B6AC51715677CB7C31E78399B0 ();
// 0x00000131 System.Byte[] WebSocketSharpUnityMod.Ext::decompress(System.Byte[])
extern void Ext_decompress_m836A06295F53096E2D3BBF9299ED44FBB6504D4B ();
// 0x00000132 System.IO.MemoryStream WebSocketSharpUnityMod.Ext::decompress(System.IO.Stream)
extern void Ext_decompress_mEEF738F492D6CADD083A625E357693E7448A3798 ();
// 0x00000133 System.Byte[] WebSocketSharpUnityMod.Ext::decompressToArray(System.IO.Stream)
extern void Ext_decompressToArray_m20385B926621FF6B2E9012CFAB0A4401FDF47A3F ();
// 0x00000134 System.Byte[] WebSocketSharpUnityMod.Ext::Append(System.UInt16,System.String)
extern void Ext_Append_mE4FC83E9CF6A32840FB51731F6A9A0BE1E4184D2 ();
// 0x00000135 System.String WebSocketSharpUnityMod.Ext::CheckIfAvailable(WebSocketSharpUnityMod.WebSocketState,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void Ext_CheckIfAvailable_m85CEAAA4821DD3F6E014948825211CDD6462B9EF ();
// 0x00000136 System.String WebSocketSharpUnityMod.Ext::CheckIfValidProtocols(System.String[])
extern void Ext_CheckIfValidProtocols_mD1264E875FE0B80B571816D1FBB871F07383D2F1 ();
// 0x00000137 System.IO.Stream WebSocketSharpUnityMod.Ext::Compress(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Compress_m40FF2F28421D52C990C352EEDF009748FF49A8A9 ();
// 0x00000138 System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x00000139 System.Boolean WebSocketSharpUnityMod.Ext::ContainsTwice(System.String[])
extern void Ext_ContainsTwice_mB352A815690C291CD8AB2DBB04F10C10D3DA63D8 ();
// 0x0000013A System.Void WebSocketSharpUnityMod.Ext::CopyTo(System.IO.Stream,System.IO.Stream,System.Int32)
extern void Ext_CopyTo_mFF5CA5974DD34ABDEC680D30DF6ECA3859A508A8 ();
// 0x0000013B System.Byte[] WebSocketSharpUnityMod.Ext::Decompress(System.Byte[],WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Decompress_m945D982F69401509378A160912D42BD335B1B7CF ();
// 0x0000013C System.Byte[] WebSocketSharpUnityMod.Ext::DecompressToArray(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_DecompressToArray_m4998F5C564C1C070CFE0C572103DDC4823CA1944 ();
// 0x0000013D System.Boolean WebSocketSharpUnityMod.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern void Ext_EqualsWith_mBDB3646F21C970F9903B605A70D70A6A8C6B56C1 ();
// 0x0000013E System.String WebSocketSharpUnityMod.Ext::GetAbsolutePath(System.Uri)
extern void Ext_GetAbsolutePath_mFD5D6CE2A94E203F2A2AAED77302D3A9C52B516F ();
// 0x0000013F System.String WebSocketSharpUnityMod.Ext::GetMessage(WebSocketSharpUnityMod.CloseStatusCode)
extern void Ext_GetMessage_mC66E14F8CF9802520C0CDD8CC6AA9BCE62D882A1 ();
// 0x00000140 System.String WebSocketSharpUnityMod.Ext::GetValue(System.String,System.Char)
extern void Ext_GetValue_mF515B67DF6933DBEF752F6F740FCB7E890DAD0CA ();
// 0x00000141 System.String WebSocketSharpUnityMod.Ext::GetValue(System.String,System.Char,System.Boolean)
extern void Ext_GetValue_m68F028EB3B4DC479A7A25A1BAE259984B96CFD60 ();
// 0x00000142 System.Byte[] WebSocketSharpUnityMod.Ext::InternalToByteArray(System.UInt16,WebSocketSharpUnityMod.ByteOrder)
extern void Ext_InternalToByteArray_m4C6202B989B87F75B2BA383BE92C80BF1650E7E4 ();
// 0x00000143 System.Byte[] WebSocketSharpUnityMod.Ext::InternalToByteArray(System.UInt64,WebSocketSharpUnityMod.ByteOrder)
extern void Ext_InternalToByteArray_mCCC187A30EFFD446FFC951F28DC338C9F6BDA9B1 ();
// 0x00000144 System.Boolean WebSocketSharpUnityMod.Ext::IsCompressionExtension(System.String,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_IsCompressionExtension_m050E774AB8688AC225775DA266FAE3D119DE6232 ();
// 0x00000145 System.Boolean WebSocketSharpUnityMod.Ext::IsControl(System.Byte)
extern void Ext_IsControl_mE24689E063FCB9AE6C90DDF96BC8E7C790671235 ();
// 0x00000146 System.Boolean WebSocketSharpUnityMod.Ext::IsData(System.Byte)
extern void Ext_IsData_m862A3DB94C8906F21F781F48D6C14D18B70C2F21 ();
// 0x00000147 System.Boolean WebSocketSharpUnityMod.Ext::IsData(WebSocketSharpUnityMod.Opcode)
extern void Ext_IsData_m603EEE06067092536DCAE1A10B3F9AF1B2AD6DA3 ();
// 0x00000148 System.Boolean WebSocketSharpUnityMod.Ext::IsReserved(System.UInt16)
extern void Ext_IsReserved_m9A4709BF21BE7463B16413824A2AFCAC100B6BC9 ();
// 0x00000149 System.Boolean WebSocketSharpUnityMod.Ext::IsReserved(WebSocketSharpUnityMod.CloseStatusCode)
extern void Ext_IsReserved_mC52557FE31C4F20C39638821359798A2BF121210 ();
// 0x0000014A System.Boolean WebSocketSharpUnityMod.Ext::IsSupported(System.Byte)
extern void Ext_IsSupported_m18366BD24CDA79114821F940B82297AFAF3A0EF3 ();
// 0x0000014B System.Boolean WebSocketSharpUnityMod.Ext::IsText(System.String)
extern void Ext_IsText_m44B86C92EBD9E8A4F2D9D0D4039D087FA4CFD75A ();
// 0x0000014C System.Boolean WebSocketSharpUnityMod.Ext::IsToken(System.String)
extern void Ext_IsToken_m8F84DDE036F7FC9F2145B659D6D736B31F420A37 ();
// 0x0000014D System.Byte[] WebSocketSharpUnityMod.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern void Ext_ReadBytes_m0091FF142DA210BE0F4F685A93176C2C7E3FB61D ();
// 0x0000014E System.Byte[] WebSocketSharpUnityMod.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern void Ext_ReadBytes_mB24E009E51CD5465F56B5F76C1C9A2BE7251505F ();
// 0x0000014F System.Void WebSocketSharpUnityMod.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m05A3D2063082E659C3A22E3AC7155C51F86A9B92 ();
// 0x00000150 System.Void WebSocketSharpUnityMod.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m13270548F41EA3688F7DF3E0918CB39EA66EEF0D ();
// 0x00000151 T[] WebSocketSharpUnityMod.Ext::Reverse(T[])
// 0x00000152 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Ext::SplitHeaderValue(System.String,System.Char[])
extern void Ext_SplitHeaderValue_m54FF7CA6A00B830775C66EF211B157A44C484102 ();
// 0x00000153 System.Byte[] WebSocketSharpUnityMod.Ext::ToByteArray(System.IO.Stream)
extern void Ext_ToByteArray_m6F561B49C713C9019FB801C373500F097A74D378 ();
// 0x00000154 System.String WebSocketSharpUnityMod.Ext::ToExtensionString(WebSocketSharpUnityMod.CompressionMethod,System.String[])
extern void Ext_ToExtensionString_m5FCFE0375B24CB2F0B97101646BC08EB97DD3F43 ();
// 0x00000155 System.UInt16 WebSocketSharpUnityMod.Ext::ToUInt16(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToUInt16_mE2024718916264AAF08D3FEBAAD90D24240F8318 ();
// 0x00000156 System.UInt64 WebSocketSharpUnityMod.Ext::ToUInt64(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToUInt64_mA5CA03B4A866E7D8278B13EE7BBF67ACD0FB6DBE ();
// 0x00000157 System.Boolean WebSocketSharpUnityMod.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern void Ext_TryCreateWebSocketUri_m7CC4962F338B03DC2B4749CD1D9DE000F8962A6A ();
// 0x00000158 System.String WebSocketSharpUnityMod.Ext::Unquote(System.String)
extern void Ext_Unquote_m56398F497467975D7969618CC2EA45BEAA65083C ();
// 0x00000159 System.String WebSocketSharpUnityMod.Ext::UTF8Decode(System.Byte[])
extern void Ext_UTF8Decode_m2E51728157D0DEE3C6567C0403DD9D7E4E111C48 ();
// 0x0000015A System.Byte[] WebSocketSharpUnityMod.Ext::UTF8Encode(System.String)
extern void Ext_UTF8Encode_m05EC00CA0336F30755D31852734CDC3896710CB5 ();
// 0x0000015B System.Void WebSocketSharpUnityMod.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern void Ext_WriteBytes_mF2AD2DF42E26B1F9EFC8E1FB8AD7A78ABB6A261D ();
// 0x0000015C System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.String,System.Char[])
extern void Ext_Contains_m46573FC11089072D7754159C2DD6D7375A8E725B ();
// 0x0000015D System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String)
extern void Ext_Contains_mAFFB593126114852F00AA4748CE912D3CC0F9ECA ();
// 0x0000015E System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String)
extern void Ext_Contains_m5C519C754B6BEF365CE8114A6EA1B6890B9FE936 ();
// 0x0000015F System.Void WebSocketSharpUnityMod.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern void Ext_Emit_m6FBC2120132DF2BA07FF7636196184BAE297F529 ();
// 0x00000160 System.Void WebSocketSharpUnityMod.Ext::Emit(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
// 0x00000161 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern void Ext_GetCookies_m57EF613E2A739B37A4F5CAD7FB4F8D79FAE6464F ();
// 0x00000162 System.Boolean WebSocketSharpUnityMod.Ext::IsEnclosedIn(System.String,System.Char)
extern void Ext_IsEnclosedIn_m6A57F317469525F8C7EC7DA335350C988B1CB0A4 ();
// 0x00000163 System.Boolean WebSocketSharpUnityMod.Ext::IsHostOrder(WebSocketSharpUnityMod.ByteOrder)
extern void Ext_IsHostOrder_mBF88043D52F49AD9C3D4F19565C54A420A6923B6 ();
// 0x00000164 System.Boolean WebSocketSharpUnityMod.Ext::IsNullOrEmpty(System.String)
extern void Ext_IsNullOrEmpty_mC6C901754611BC6E3E3F57D91D2C9A26AA93D087 ();
// 0x00000165 System.Boolean WebSocketSharpUnityMod.Ext::IsPredefinedScheme(System.String)
extern void Ext_IsPredefinedScheme_mB040228C6B5C3D0FB6DB20424542266379842FF3 ();
// 0x00000166 System.Boolean WebSocketSharpUnityMod.Ext::MaybeUri(System.String)
extern void Ext_MaybeUri_mFDCDD897107619A18E640A0465925A89211D3F17 ();
// 0x00000167 T[] WebSocketSharpUnityMod.Ext::SubArray(T[],System.Int32,System.Int32)
// 0x00000168 T[] WebSocketSharpUnityMod.Ext::SubArray(T[],System.Int64,System.Int64)
// 0x00000169 System.Void WebSocketSharpUnityMod.Ext::Times(System.Int32,System.Action`1<System.Int32>)
extern void Ext_Times_mCCB50FF3F626576B11E3258B11D9537D81EE8172 ();
// 0x0000016A System.Byte[] WebSocketSharpUnityMod.Ext::ToHostOrder(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToHostOrder_m00C1849C29BAA1BC5D025CD661A471080605A5AF ();
// 0x0000016B System.String WebSocketSharpUnityMod.Ext::ToString(T[],System.String)
// 0x0000016C System.Uri WebSocketSharpUnityMod.Ext::ToUri(System.String)
extern void Ext_ToUri_mEC8E6899F4EC8D373BFB5FCBF57D620233D6F6C4 ();
// 0x0000016D System.String WebSocketSharpUnityMod.Ext::UrlDecode(System.String)
extern void Ext_UrlDecode_mB0A77A8BE3C05F6367151535A17AFA0B6365B00F ();
// 0x0000016E System.Boolean WebSocketSharpUnityMod.Ext::<CheckIfValidProtocols>b__0(System.String)
extern void Ext_U3CCheckIfValidProtocolsU3Eb__0_m63FE12A5576F745DEE14F9791F4CC802F7A3AD49 ();
// 0x0000016F System.Void WebSocketSharpUnityMod.Ext::.cctor()
extern void Ext__cctor_m60DDBA76C9084B3925FA22BB03BA229D75B137FD ();
// 0x00000170 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClass3::.ctor()
extern void U3CU3Ec__DisplayClass3__ctor_mA8838CAA571F52B2BFCC05E73ED615ACC1652A84 ();
// 0x00000171 System.Boolean WebSocketSharpUnityMod.Ext_<>c__DisplayClass3::<ContainsTwice>b__2(System.Int32)
extern void U3CU3Ec__DisplayClass3_U3CContainsTwiceU3Eb__2_m2CED0B687B4D76259A223852817DC12A9F7ECA64 ();
// 0x00000172 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClass9::.ctor()
extern void U3CU3Ec__DisplayClass9__ctor_m378B8B45EF36B6B5CC196A785F8A69BB2E892419 ();
// 0x00000173 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClass9::<ReadBytesAsync>b__8(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass9_U3CReadBytesAsyncU3Eb__8_m78C0576F9AB52530C475C4F399758629F78BC4E2 ();
// 0x00000174 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClassd::.ctor()
extern void U3CU3Ec__DisplayClassd__ctor_m903CC42078D26D05A4DBBE48DA96180E37BA4FC4 ();
// 0x00000175 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClassd::<ReadBytesAsync>b__b(System.Int64)
extern void U3CU3Ec__DisplayClassd_U3CReadBytesAsyncU3Eb__b_m4CD9EE991BE16BC6975DEE02A318DEFD13001EB3 ();
// 0x00000176 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClassd_<>c__DisplayClassf::.ctor()
extern void U3CU3Ec__DisplayClassf__ctor_m86005AA4FA5832144CA346111CB35465A2C932DC ();
// 0x00000177 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClassd_<>c__DisplayClassf::<ReadBytesAsync>b__c(System.IAsyncResult)
extern void U3CU3Ec__DisplayClassf_U3CReadBytesAsyncU3Eb__c_m9DC3FF236490F2D54EAC900B4E0255E5B4AF224C ();
// 0x00000178 System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__11_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m2A7023B8B86B4751828EA0964AC9C5EE7E90CE5E ();
// 0x00000179 System.Collections.IEnumerator WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.Collections.IEnumerable.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerable_GetEnumerator_m1C10E311685F332ED6124A564A8AB5CF39BB3E67 ();
// 0x0000017A System.Boolean WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::MoveNext()
extern void U3CSplitHeaderValueU3Ed__11_MoveNext_m06BC9C7CA0538FFA4C7212E2F996731027104D10 ();
// 0x0000017B System.String WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CSplitHeaderValueU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m0E2862496F8DE7FC010F07272E7515368D38D3E6 ();
// 0x0000017C System.Void WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerator_Reset_m276A7AE95E62CFB4336FB1EBFEB172128EE6FFF2 ();
// 0x0000017D System.Void WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.IDisposable.Dispose()
extern void U3CSplitHeaderValueU3Ed__11_System_IDisposable_Dispose_m923011C5F524B04DACEED4826FF8272A385198BB ();
// 0x0000017E System.Object WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerator_get_Current_m9AD24AE4E05AAC61AEE2BAEE83363A20BA9BC087 ();
// 0x0000017F System.Void WebSocketSharpUnityMod.Ext_<SplitHeaderValue>d__11::.ctor(System.Int32)
extern void U3CSplitHeaderValueU3Ed__11__ctor_mD4BF7E8EB9AFC4B3871B2D077B640712C498AF9D ();
// 0x00000180 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClass20`1::.ctor()
// 0x00000181 System.Void WebSocketSharpUnityMod.Ext_<>c__DisplayClass20`1::<ToString>b__1f(System.Int32)
// 0x00000182 System.Void WebSocketSharpUnityMod.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpBase__ctor_m4165B18971B005C70A3F1E444CD7C6B20A57F187 ();
// 0x00000183 System.String WebSocketSharpUnityMod.HttpBase::get_EntityBody()
extern void HttpBase_get_EntityBody_mD288C0CC22AD0A01F43CDE43D6CFDEF3FDE70C05 ();
// 0x00000184 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.HttpBase::get_Headers()
extern void HttpBase_get_Headers_m6E65386C443B2DCFDD1108F92D0CCB4AFAB922B9 ();
// 0x00000185 System.Version WebSocketSharpUnityMod.HttpBase::get_ProtocolVersion()
extern void HttpBase_get_ProtocolVersion_m2BC68E395703C75AF4CD502C63174650DE0268F5 ();
// 0x00000186 System.Byte[] WebSocketSharpUnityMod.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern void HttpBase_readEntityBody_m5615E327709AC55ECFD0C03B975E23D060C0E5B0 ();
// 0x00000187 System.String[] WebSocketSharpUnityMod.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern void HttpBase_readHeaders_mA59BC3BCD19A58E15A5DAFF4AB47D4E9E09C832F ();
// 0x00000188 T WebSocketSharpUnityMod.HttpBase::Read(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
// 0x00000189 System.Byte[] WebSocketSharpUnityMod.HttpBase::ToByteArray()
extern void HttpBase_ToByteArray_mB48F9B867184BC5E7771B99322131E4A4AA6A129 ();
// 0x0000018A System.Void WebSocketSharpUnityMod.HttpBase_<>c__DisplayClass1::.ctor()
extern void U3CU3Ec__DisplayClass1__ctor_mCE8F2D261E9B5118B0B126BB376AA4F0003DFCD0 ();
// 0x0000018B System.Void WebSocketSharpUnityMod.HttpBase_<>c__DisplayClass1::<readHeaders>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass1_U3CreadHeadersU3Eb__0_mE811BDBF8B39BF6E1B7B2A7B0CFCB08D79D02D1A ();
// 0x0000018C System.Void WebSocketSharpUnityMod.HttpBase_<>c__DisplayClass4`1::.ctor()
// 0x0000018D System.Void WebSocketSharpUnityMod.HttpBase_<>c__DisplayClass4`1::<Read>b__3(System.Object)
// 0x0000018E System.Void WebSocketSharpUnityMod.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpRequest__ctor_mC2D7D84D488E9B4809FEF640699B1C9480658E89 ();
// 0x0000018F System.Void WebSocketSharpUnityMod.HttpRequest::.ctor(System.String,System.String)
extern void HttpRequest__ctor_m306CD949512BA0499D9979EB9D6403D821A60756 ();
// 0x00000190 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::CreateConnectRequest(System.Uri)
extern void HttpRequest_CreateConnectRequest_m9326376FD476238ECC3B2176D80A41D80D4396D0 ();
// 0x00000191 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::CreateWebSocketRequest(System.Uri)
extern void HttpRequest_CreateWebSocketRequest_m5DF0326F894B38216EBC912351ECB5B2E703B2C2 ();
// 0x00000192 WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern void HttpRequest_GetResponse_mEAFF1853E31E707A30470CBA633B6C8F375BC04D ();
// 0x00000193 System.Void WebSocketSharpUnityMod.HttpRequest::SetCookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void HttpRequest_SetCookies_mC0F3E8E12D87DFE78C282452E316313E3FE82EF1 ();
// 0x00000194 System.String WebSocketSharpUnityMod.HttpRequest::ToString()
extern void HttpRequest_ToString_m59938FF81CC4D55F09E74019666B10B878450F11 ();
// 0x00000195 System.Void WebSocketSharpUnityMod.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpResponse__ctor_m061D0C9B8F6B283044F9B072AFDF33F00825A56E ();
// 0x00000196 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.HttpResponse::get_Cookies()
extern void HttpResponse_get_Cookies_m3A1BBD66F2D88B61C3EFBF398ACF807E9501F1CD ();
// 0x00000197 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_HasConnectionClose()
extern void HttpResponse_get_HasConnectionClose_mE7749F7EBE6120C4F850F97FB609C3A2A2F0FB89 ();
// 0x00000198 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsProxyAuthenticationRequired()
extern void HttpResponse_get_IsProxyAuthenticationRequired_m238B2E37AF27FE0B6B47973CA4799E35A3C0CC3D ();
// 0x00000199 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsRedirect()
extern void HttpResponse_get_IsRedirect_mC0C821FA49EB90632282ADBD44AA439799E76A20 ();
// 0x0000019A System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsUnauthorized()
extern void HttpResponse_get_IsUnauthorized_mA351DA558CD64DDB2B6410DC9F1B88190EE4C91E ();
// 0x0000019B System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsWebSocketResponse()
extern void HttpResponse_get_IsWebSocketResponse_m722D43F1AB433A282C0F73751EFD94FBACD1D55C ();
// 0x0000019C System.String WebSocketSharpUnityMod.HttpResponse::get_StatusCode()
extern void HttpResponse_get_StatusCode_mA99805825F473DA8A62F199D65F8D5FBE70CE608 ();
// 0x0000019D WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::Parse(System.String[])
extern void HttpResponse_Parse_m8424EBF321DF634A4F182559CA58CEACD0F52722 ();
// 0x0000019E System.String WebSocketSharpUnityMod.HttpResponse::ToString()
extern void HttpResponse_ToString_m05E9FBECEAF2C443B6289F1BCE8611FCE5A93946 ();
// 0x0000019F System.Void WebSocketSharpUnityMod.LogData::.ctor(WebSocketSharpUnityMod.LogLevel,System.Diagnostics.StackFrame,System.String)
extern void LogData__ctor_m33D5ABF45114912EE19D3DCB0BD01F92E0C8EC6C ();
// 0x000001A0 System.String WebSocketSharpUnityMod.LogData::ToString()
extern void LogData_ToString_m944E3C1C5FD5480EA21121714F50C88FD6591B00 ();
// 0x000001A1 System.Void WebSocketSharpUnityMod.Logger::.ctor()
extern void Logger__ctor_mC55DC74C3A52A81E6FB64C48D39011309708BBD3 ();
// 0x000001A2 System.Void WebSocketSharpUnityMod.Logger::.ctor(WebSocketSharpUnityMod.LogLevel,System.String,System.Action`2<WebSocketSharpUnityMod.LogData,System.String>)
extern void Logger__ctor_mD3DB232D7C3C7DCD3C0769D79329E552E5574582 ();
// 0x000001A3 System.Void WebSocketSharpUnityMod.Logger::defaultOutput(WebSocketSharpUnityMod.LogData,System.String)
extern void Logger_defaultOutput_m8893023F72ADD33533B906526075FAA69045600B ();
// 0x000001A4 System.Void WebSocketSharpUnityMod.Logger::output(System.String,WebSocketSharpUnityMod.LogLevel)
extern void Logger_output_m8AAE8BA1F7C05B9C9F75D7437BBF0F64BFA644BC ();
// 0x000001A5 System.Void WebSocketSharpUnityMod.Logger::writeToFile(System.String,System.String)
extern void Logger_writeToFile_mA1B20FBF3D7DD2EB1245817F4CA68071F431B746 ();
// 0x000001A6 System.Void WebSocketSharpUnityMod.Logger::Debug(System.String)
extern void Logger_Debug_m1087E5D8B286205A0A053A6CEC725DDF0DD3EE15 ();
// 0x000001A7 System.Void WebSocketSharpUnityMod.Logger::Error(System.String)
extern void Logger_Error_mD1DCB04433A59DA42460239357FACC09CC262BB2 ();
// 0x000001A8 System.Void WebSocketSharpUnityMod.Logger::Fatal(System.String)
extern void Logger_Fatal_m70C002827E95AF53E7C4D5F65B8375CB152843A2 ();
// 0x000001A9 System.Void WebSocketSharpUnityMod.Logger::Info(System.String)
extern void Logger_Info_mD9DBED65DDCD1CA90FF38EDCFB9B73C8ECA40EC5 ();
// 0x000001AA System.Void WebSocketSharpUnityMod.Logger::Trace(System.String)
extern void Logger_Trace_mCD517D0F8A833A9D05E4224437255867C0459A5E ();
// 0x000001AB System.Void WebSocketSharpUnityMod.Logger::Warn(System.String)
extern void Logger_Warn_m5B4B919007AE42F6C63CD61D1394F185104A37D8 ();
// 0x000001AC System.Void WebSocketSharpUnityMod.MessageEventArgs::.ctor(WebSocketSharpUnityMod.WebSocketFrame)
extern void MessageEventArgs__ctor_mE28253222CAB528040709CDB30AB7AE796588879 ();
// 0x000001AD System.Void WebSocketSharpUnityMod.MessageEventArgs::.ctor(WebSocketSharpUnityMod.Opcode,System.Byte[])
extern void MessageEventArgs__ctor_m1D2F4D4BCCBEC696BB93E389B40CDBD2DA2FF064 ();
// 0x000001AE System.String WebSocketSharpUnityMod.MessageEventArgs::get_Data()
extern void MessageEventArgs_get_Data_mEDF292CC4AEACD06EF648AA77790FAE9019FD8AA ();
// 0x000001AF System.Boolean WebSocketSharpUnityMod.MessageEventArgs::get_IsBinary()
extern void MessageEventArgs_get_IsBinary_m61BB7129529EFA9C784196E957F50617854D4B62 ();
// 0x000001B0 System.Byte[] WebSocketSharpUnityMod.MessageEventArgs::get_RawData()
extern void MessageEventArgs_get_RawData_mE764E56DAE51B38BA1C3CC3B54232D0795A52BD0 ();
// 0x000001B1 System.Void WebSocketSharpUnityMod.Net.AuthenticationBase::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationBase__ctor_mD956F45C397AEDDB077F78C45C5F571DAD103122 ();
// 0x000001B2 WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Net.AuthenticationBase::get_Scheme()
extern void AuthenticationBase_get_Scheme_m9D31D42B498CAFD09718DDC76DE5D3722F56D70B ();
// 0x000001B3 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::CreateNonceValue()
extern void AuthenticationBase_CreateNonceValue_mDFC38ED8BE431697C42FB5C36B100507039ED32D ();
// 0x000001B4 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.AuthenticationBase::ParseParameters(System.String)
extern void AuthenticationBase_ParseParameters_m253A9D2AEB932002A8AA134DF909CA6AEFBDDC45 ();
// 0x000001B5 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToBasicString()
// 0x000001B6 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToDigestString()
// 0x000001B7 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToString()
extern void AuthenticationBase_ToString_m6F7B02651C8E143DAA78A1285FE68517810D9FC1 ();
// 0x000001B8 System.Void WebSocketSharpUnityMod.Net.AuthenticationChallenge::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationChallenge__ctor_mFC188F896C0EC18E47893F2A96EC9E2CFA8D96C1 ();
// 0x000001B9 WebSocketSharpUnityMod.Net.AuthenticationChallenge WebSocketSharpUnityMod.Net.AuthenticationChallenge::Parse(System.String)
extern void AuthenticationChallenge_Parse_m2C887252447D8DF95747B902877E371C5A2DF311 ();
// 0x000001BA System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::ToBasicString()
extern void AuthenticationChallenge_ToBasicString_m4554A9FB78D449EF47ECC6BBB6690E0D1CB9D480 ();
// 0x000001BB System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::ToDigestString()
extern void AuthenticationChallenge_ToDigestString_mA39087A182EE0BB5694A3F47A40191711961CB69 ();
// 0x000001BC System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.NetworkCredential)
extern void AuthenticationResponse__ctor_m84953201122084E2C427A5DF6995D928F94F8FC2 ();
// 0x000001BD System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.AuthenticationChallenge,WebSocketSharpUnityMod.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m5DDEF851D0D8588BA351347A94F1D5B795911FEC ();
// 0x000001BE System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharpUnityMod.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m8EE647609498BED130417F608263222D25CD22C8 ();
// 0x000001BF System.UInt32 WebSocketSharpUnityMod.Net.AuthenticationResponse::get_NonceCount()
extern void AuthenticationResponse_get_NonceCount_mE7E89CE703D7AC001B26C1C692DEF7638574376B ();
// 0x000001C0 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_mE03E25AA3786EE079DC0E9B8F9106933BEEC8805 ();
// 0x000001C1 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m7261CD5E17CB199B47CCE25E9F860A37F94D88F1 ();
// 0x000001C2 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA2(System.String,System.String)
extern void AuthenticationResponse_createA2_m909965B4C2E988B261D07AB3C7500DD52BF11F7B ();
// 0x000001C3 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern void AuthenticationResponse_createA2_m02F88F10A1A7F4617E47C138E9DA3CE4AFB57500 ();
// 0x000001C4 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::hash(System.String)
extern void AuthenticationResponse_hash_mEC632AAE9AB26C003AA80B60CE9F17DBCA637B23 ();
// 0x000001C5 System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::initAsDigest()
extern void AuthenticationResponse_initAsDigest_m94FF3F2670DD777AEF409E659DB764B37545698A ();
// 0x000001C6 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse_CreateRequestDigest_m46D5C6725D1BF595E2F1A88BC29C0DF5B0C66C0F ();
// 0x000001C7 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::ToBasicString()
extern void AuthenticationResponse_ToBasicString_m8FCE8EDC5CE69DAA32D94B4C2BAC6A8F3DF2DC24 ();
// 0x000001C8 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::ToDigestString()
extern void AuthenticationResponse_ToDigestString_mF4FF6BC7A6E7DA952646CD5BE6117BA506BD4C2A ();
// 0x000001C9 System.Boolean WebSocketSharpUnityMod.Net.AuthenticationResponse::<initAsDigest>b__0(System.String)
extern void AuthenticationResponse_U3CinitAsDigestU3Eb__0_m88296FC69B0C92744F553AFA168FDC67642CDC71 ();
// 0x000001CA System.Void WebSocketSharpUnityMod.Net.SslConfiguration::.ctor(System.Security.Authentication.SslProtocols,System.Boolean)
extern void SslConfiguration__ctor_m677DBF069E824D78BC48074F029E814AD6C3BDFA ();
// 0x000001CB System.Net.Security.LocalCertificateSelectionCallback WebSocketSharpUnityMod.Net.SslConfiguration::get_CertificateSelectionCallback()
extern void SslConfiguration_get_CertificateSelectionCallback_m2F742178D426B6520E7C776C0C9903680D2F37AD ();
// 0x000001CC System.Net.Security.RemoteCertificateValidationCallback WebSocketSharpUnityMod.Net.SslConfiguration::get_CertificateValidationCallback()
extern void SslConfiguration_get_CertificateValidationCallback_mE285A26C4FB09E1F5A294326097A4366727F14CF ();
// 0x000001CD System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_CertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void SslConfiguration_set_CertificateValidationCallback_mC7FCFD8BA5B07A7CEEBF5B8B5ABA91D9F267D279 ();
// 0x000001CE System.Boolean WebSocketSharpUnityMod.Net.SslConfiguration::get_CheckCertificateRevocation()
extern void SslConfiguration_get_CheckCertificateRevocation_m97E42331DF2900ABCB0832033FFC6C64FA7408B5 ();
// 0x000001CF System.Security.Authentication.SslProtocols WebSocketSharpUnityMod.Net.SslConfiguration::get_EnabledSslProtocols()
extern void SslConfiguration_get_EnabledSslProtocols_m34619955FA535C1BB6C75EE24C01DB3B02509551 ();
// 0x000001D0 System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void SslConfiguration_set_EnabledSslProtocols_mEA8C3CC4DB64BEBC0586FDB2780F2830E335CD0D ();
// 0x000001D1 System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharpUnityMod.Net.SslConfiguration::<get_CertificateSelectionCallback>b__0(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void SslConfiguration_U3Cget_CertificateSelectionCallbackU3Eb__0_m136BFD4DD092E475E5886A936340AC192CE8557E ();
// 0x000001D2 System.Boolean WebSocketSharpUnityMod.Net.SslConfiguration::<get_CertificateValidationCallback>b__2(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void SslConfiguration_U3Cget_CertificateValidationCallbackU3Eb__2_m1FCCFD7D4F48DD2EA48211D095A6CC3906FCCF92 ();
// 0x000001D3 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::.ctor(System.String)
extern void ClientSslConfiguration__ctor_m2764B7C0CF7A76BB7D339688B634FA8D52816D66 ();
// 0x000001D4 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::.ctor(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern void ClientSslConfiguration__ctor_m0604BDC20F7A157A1F1C3535B6EABEDB003BDD95 ();
// 0x000001D5 System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ClientCertificates()
extern void ClientSslConfiguration_get_ClientCertificates_mDAA3039829365BDC9C022B7DAEC8AC97C9FCA9D2 ();
// 0x000001D6 System.Net.Security.LocalCertificateSelectionCallback WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern void ClientSslConfiguration_get_ClientCertificateSelectionCallback_m699F27E58C1F78DE84C6F05C35684428D3B42B2D ();
// 0x000001D7 System.Net.Security.RemoteCertificateValidationCallback WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern void ClientSslConfiguration_get_ServerCertificateValidationCallback_m8F941D52F6C700B3F0FE20D85B8B11ACE5D9579F ();
// 0x000001D8 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void ClientSslConfiguration_set_ServerCertificateValidationCallback_m4618F00BE2FB0F7A8583DB468BF3BE3AFB19BEE5 ();
// 0x000001D9 System.String WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_TargetHost()
extern void ClientSslConfiguration_get_TargetHost_mA73D80B44708971DB597B66D8457FB6D0243C7C5 ();
// 0x000001DA System.Void WebSocketSharpUnityMod.Net.Cookie::.cctor()
extern void Cookie__cctor_m999C0D224AFFC227294BE01059E673DE2DF81C8C ();
// 0x000001DB System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor()
extern void Cookie__ctor_m83234BC60730C52E958774CA8968F5E94D6BB077 ();
// 0x000001DC System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor(System.String,System.String)
extern void Cookie__ctor_mDD62F8FB714F81FEF19F06D8BE5A449008F90CA5 ();
// 0x000001DD System.Void WebSocketSharpUnityMod.Net.Cookie::set_ExactDomain(System.Boolean)
extern void Cookie_set_ExactDomain_mA3C12ACF0ECCBBE9F3497D7B77F2EB7427ADAC99 ();
// 0x000001DE System.Void WebSocketSharpUnityMod.Net.Cookie::set_Comment(System.String)
extern void Cookie_set_Comment_mB64D12F86BBB68713984956840D99226B76DCC28 ();
// 0x000001DF System.Void WebSocketSharpUnityMod.Net.Cookie::set_CommentUri(System.Uri)
extern void Cookie_set_CommentUri_mD38E976CC9FD1E70730330CCCCCA938CE11285E5 ();
// 0x000001E0 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Discard(System.Boolean)
extern void Cookie_set_Discard_mD745B6687FF10A6E7DDA84A7A6AA466E3286361B ();
// 0x000001E1 System.String WebSocketSharpUnityMod.Net.Cookie::get_Domain()
extern void Cookie_get_Domain_m631D71FC83AFDE4ABC8E51270B7A146FBEE0CDC2 ();
// 0x000001E2 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Domain(System.String)
extern void Cookie_set_Domain_mCE016A08EAC78AD6AC4ADC81B594AF81522DC641 ();
// 0x000001E3 System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_Expired()
extern void Cookie_get_Expired_m6D006652937D8C26E638AF6F15D0775E21444851 ();
// 0x000001E4 System.DateTime WebSocketSharpUnityMod.Net.Cookie::get_Expires()
extern void Cookie_get_Expires_m533B091712A7479701B77956FAEADAA706C3DB2C ();
// 0x000001E5 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Expires(System.DateTime)
extern void Cookie_set_Expires_mB643D5B7D6A84D66E93A61819A351DE345211DDC ();
// 0x000001E6 System.Void WebSocketSharpUnityMod.Net.Cookie::set_HttpOnly(System.Boolean)
extern void Cookie_set_HttpOnly_m6BE2A73DD6B04BB08FC8DEE0FE9B3AD53E627B70 ();
// 0x000001E7 System.String WebSocketSharpUnityMod.Net.Cookie::get_Name()
extern void Cookie_get_Name_mAF3643FB1D89CB20C8CBC51B0EC1CE7B29E6F189 ();
// 0x000001E8 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Name(System.String)
extern void Cookie_set_Name_mF85004448EF80E21D2F50E856B20A2B1C15251A7 ();
// 0x000001E9 System.String WebSocketSharpUnityMod.Net.Cookie::get_Path()
extern void Cookie_get_Path_m58C636E8D4BBF83E816A763009BF88A561F7C690 ();
// 0x000001EA System.Void WebSocketSharpUnityMod.Net.Cookie::set_Path(System.String)
extern void Cookie_set_Path_m40D898C50F52D4607FF3A07AB647091DA2C86121 ();
// 0x000001EB System.Void WebSocketSharpUnityMod.Net.Cookie::set_Port(System.String)
extern void Cookie_set_Port_mF0CA07A10F4FA4E00D10A2A3F1B1543501131420 ();
// 0x000001EC System.Void WebSocketSharpUnityMod.Net.Cookie::set_Secure(System.Boolean)
extern void Cookie_set_Secure_m7B0BEBCB1D3D2620E5CA6598970B5E29FC39539A ();
// 0x000001ED System.String WebSocketSharpUnityMod.Net.Cookie::get_Value()
extern void Cookie_get_Value_m1F214BEAE9D77056EFEBFCD0696DE9DE8C264B61 ();
// 0x000001EE System.Void WebSocketSharpUnityMod.Net.Cookie::set_Value(System.String)
extern void Cookie_set_Value_m6C446F6ED4EF9475C0F44387834C9C74B37FD64C ();
// 0x000001EF System.Int32 WebSocketSharpUnityMod.Net.Cookie::get_Version()
extern void Cookie_get_Version_mD2A022565561F7DFAC1C43653677A71A60AF0A75 ();
// 0x000001F0 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Version(System.Int32)
extern void Cookie_set_Version_mC32E8B840E4915A4E55B74E3F6D5232A454E6C14 ();
// 0x000001F1 System.Boolean WebSocketSharpUnityMod.Net.Cookie::canSetName(System.String,System.String&)
extern void Cookie_canSetName_mF369DBD22F2FD4B1D0DA286F05D33088A1ACFBBD ();
// 0x000001F2 System.Boolean WebSocketSharpUnityMod.Net.Cookie::canSetValue(System.String,System.String&)
extern void Cookie_canSetValue_mF18745F89C5CB35B1BCDA824E73424BFAB721428 ();
// 0x000001F3 System.Int32 WebSocketSharpUnityMod.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Cookie_hash_mD0397CE44AEBC0F4BA56FA1A24C6309D38E2D0A9 ();
// 0x000001F4 System.Boolean WebSocketSharpUnityMod.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&,System.String&)
extern void Cookie_tryCreatePorts_mB66AA04E4B11DB39D9A44E26F62E56651E6D7CE0 ();
// 0x000001F5 System.String WebSocketSharpUnityMod.Net.Cookie::ToRequestString(System.Uri)
extern void Cookie_ToRequestString_m883334792F4A37AE59026568F1C2A33BC59DD9CD ();
// 0x000001F6 System.Boolean WebSocketSharpUnityMod.Net.Cookie::Equals(System.Object)
extern void Cookie_Equals_m82C68C8C56B72C1287266CA30A9B860F6930978B ();
// 0x000001F7 System.Int32 WebSocketSharpUnityMod.Net.Cookie::GetHashCode()
extern void Cookie_GetHashCode_m04EFDAD814ED9360157C3AEA1DFE3D14600A1A9B ();
// 0x000001F8 System.String WebSocketSharpUnityMod.Net.Cookie::ToString()
extern void Cookie_ToString_mE726F447B6A8AFD4FEAEF201E4C5EC2DA8EACEC9 ();
// 0x000001F9 System.Void WebSocketSharpUnityMod.Net.CookieCollection::.ctor()
extern void CookieCollection__ctor_mDE48C9757936FF7382AB37BDC7DCE7206C68D6F3 ();
// 0x000001FA System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.Net.CookieCollection::get_Sorted()
extern void CookieCollection_get_Sorted_m1634FF7659A9F4F41EF55394DFB134342E38DA74 ();
// 0x000001FB System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::get_Count()
extern void CookieCollection_get_Count_m2A92DAA0F6E072F550FDEB662A779B09F5608938 ();
// 0x000001FC System.Object WebSocketSharpUnityMod.Net.CookieCollection::get_SyncRoot()
extern void CookieCollection_get_SyncRoot_m1C96A338B47152CB3D682DE55973D8059E8EDE03 ();
// 0x000001FD System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::compareCookieWithinSorted(WebSocketSharpUnityMod.Net.Cookie,WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_compareCookieWithinSorted_m26119F0A3367EDBA858F30BE0E6C417F891B2D55 ();
// 0x000001FE WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::parseRequest(System.String)
extern void CookieCollection_parseRequest_mDA7A3CF32E4AD9E29BC539FD8FF4981AFBF37064 ();
// 0x000001FF WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::parseResponse(System.String)
extern void CookieCollection_parseResponse_m99A018C372BFF1C86CCE6C9F3F93A875C03F80C3 ();
// 0x00000200 System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::searchCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_searchCookie_m9CEEF0238B9287F47E684368FFC50D1C697740C7 ();
// 0x00000201 System.String[] WebSocketSharpUnityMod.Net.CookieCollection::splitCookieHeaderValue(System.String)
extern void CookieCollection_splitCookieHeaderValue_m23911D1DF40A2F8E071ACB0C0E573347F5AAAE71 ();
// 0x00000202 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::Parse(System.String,System.Boolean)
extern void CookieCollection_Parse_m9255ACB4EBD1B62FFC8C72D09290113D1C4611BA ();
// 0x00000203 System.Void WebSocketSharpUnityMod.Net.CookieCollection::SetOrRemove(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_SetOrRemove_mCF9056741DAA5D3D63C31B4C38C757FEE6AE052A ();
// 0x00000204 System.Void WebSocketSharpUnityMod.Net.CookieCollection::SetOrRemove(WebSocketSharpUnityMod.Net.CookieCollection)
extern void CookieCollection_SetOrRemove_m131EF8F30D4CF4A94A20175107CAEADDEC4E8C16 ();
// 0x00000205 System.Void WebSocketSharpUnityMod.Net.CookieCollection::Add(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_Add_m74BBC0F9D626AF25B32F29A9D6D7F16ABD5DEB69 ();
// 0x00000206 System.Void WebSocketSharpUnityMod.Net.CookieCollection::CopyTo(System.Array,System.Int32)
extern void CookieCollection_CopyTo_m7F94B5B05098C560EBEF14AD2CE30FC3C56E6597 ();
// 0x00000207 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.CookieCollection::GetEnumerator()
extern void CookieCollection_GetEnumerator_mADDBE728E05C32AEE57FEDEE324A1AD909A4847C ();
// 0x00000208 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor(System.String)
extern void CookieException__ctor_mA14039B3EE1868B62197E88323CF514391BC8FF5 ();
// 0x00000209 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException__ctor_m8B69E844CED63708B131F13D3EF25278A561549F ();
// 0x0000020A System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor()
extern void CookieException__ctor_m2F06EDE99EC7566274279CCB821E8CDC3FE94CF6 ();
// 0x0000020B System.Void WebSocketSharpUnityMod.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_GetObjectData_m8C1477B1CF776F28BB5933A8673CE0010BBF27CF ();
// 0x0000020C System.Void WebSocketSharpUnityMod.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m9341D61524031A7DAC92656DF1FB570AC484A7B1 ();
// 0x0000020D System.Void WebSocketSharpUnityMod.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharpUnityMod.Net.HttpHeaderType)
extern void HttpHeaderInfo__ctor_m8564E1CE722340F433E694DED3E6BC499C991A76 ();
// 0x0000020E System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern void HttpHeaderInfo_get_IsMultiValueInRequest_m848318D020DE8715A8A1970D168B773EB47A0223 ();
// 0x0000020F System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern void HttpHeaderInfo_get_IsMultiValueInResponse_m045FE262927EB1F684F16DFCFF2BC606EE5BFC9A ();
// 0x00000210 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsRequest()
extern void HttpHeaderInfo_get_IsRequest_m3854E071DA6A521B4467BB8944CFA80A93BFEA34 ();
// 0x00000211 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsResponse()
extern void HttpHeaderInfo_get_IsResponse_m97E800597D6D407C264D9AE602DECD67975546FF ();
// 0x00000212 System.String WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_Name()
extern void HttpHeaderInfo_get_Name_mEBA3D7587363DF76748B8FDDA251A1580DD57768 ();
// 0x00000213 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern void HttpHeaderInfo_IsMultiValue_m3D992A8350078719889CC92925F1972BC3814FBE ();
// 0x00000214 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern void HttpHeaderInfo_IsRestricted_m331D09A4A4F430844DDAAADDB6EBE263B087AB3B ();
// 0x00000215 System.Int32 WebSocketSharpUnityMod.Net.HttpUtility::getChar(System.String,System.Int32,System.Int32)
extern void HttpUtility_getChar_m504042133EF7A9FA411CA5C06612CC686E8A6AC4 ();
// 0x00000216 System.Int32 WebSocketSharpUnityMod.Net.HttpUtility::getInt(System.Byte)
extern void HttpUtility_getInt_m1C0968EA5FBD1568A2639F766A8AD99A14619D3E ();
// 0x00000217 System.Void WebSocketSharpUnityMod.Net.HttpUtility::writeCharBytes(System.Char,System.Collections.IList,System.Text.Encoding)
extern void HttpUtility_writeCharBytes_m2EBD88C03BD8B3FDE9E72910384523721FA87F8D ();
// 0x00000218 System.Text.Encoding WebSocketSharpUnityMod.Net.HttpUtility::GetEncoding(System.String)
extern void HttpUtility_GetEncoding_m5477121594343AF9CCCE6785E45DC7BD30F3F826 ();
// 0x00000219 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.String)
extern void HttpUtility_UrlDecode_mD616AFE862553A8E5CFA347D0EA5FE69DC6D6B77 ();
// 0x0000021A System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_mDA6546F244A5AED5BE997D420BF4C186F3FEA137 ();
// 0x0000021B System.Void WebSocketSharpUnityMod.Net.HttpUtility::.cctor()
extern void HttpUtility__cctor_m4E2CBD70E5102E4709414D4A97A2DA913920DB52 ();
// 0x0000021C System.Void WebSocketSharpUnityMod.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_m529DB5AA68F7E32FCCC4EF99528F7B90669AFDA5 ();
// 0x0000021D System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_Domain()
extern void NetworkCredential_get_Domain_mDC7D1EE5FFBE50F846C761A8A7BCC4E7F7F5729D ();
// 0x0000021E System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_Password()
extern void NetworkCredential_get_Password_mE29BDB4E1FBBEF3CE516EB7FABE06593E243F3DE ();
// 0x0000021F System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_UserName()
extern void NetworkCredential_get_UserName_mA05A185777FCBCAD07FDAA8EE0D0C4823B436AD6 ();
// 0x00000220 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_mEB1743827BE37B35F6616D00CC3F38FF944FC077 ();
// 0x00000221 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_mEECFB012EADD76D7482A568A415D9D620C8563D7 ();
// 0x00000222 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_m63EA39687C9B76F646C32EE4F959C976EC32CF3A ();
// 0x00000223 System.String[] WebSocketSharpUnityMod.Net.WebHeaderCollection::get_AllKeys()
extern void WebHeaderCollection_get_AllKeys_m0DB5921E1A7578EE28E53816E5186F3440F2C81B ();
// 0x00000224 System.Int32 WebSocketSharpUnityMod.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_mF817BB1BAEC36FEA0CB2A3429B2A472E34CF2688 ();
// 0x00000225 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_add_mC34CD5514FF18816AF89BFFC833941B7A9120478 ();
// 0x00000226 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingName_mAB7B3EE68E491004040EC862DFB808564A0ED60B ();
// 0x00000227 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingNameAndRestricted_m91C0CD404E0113509F46C8A4B492533C58810303 ();
// 0x00000228 System.Int32 WebSocketSharpUnityMod.Net.WebHeaderCollection::checkColonSeparated(System.String)
extern void WebHeaderCollection_checkColonSeparated_m30369BA3FBB4716321E0F51D4F4644351ACC3DDB ();
// 0x00000229 WebSocketSharpUnityMod.Net.HttpHeaderType WebSocketSharpUnityMod.Net.WebHeaderCollection::checkHeaderType(System.String)
extern void WebHeaderCollection_checkHeaderType_m1D96F5E40570441455DD91BBB4188306BF310B70 ();
// 0x0000022A System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::checkName(System.String)
extern void WebHeaderCollection_checkName_mB5DA5657A0FF1787C2A1A87B9FB70C5F39341801 ();
// 0x0000022B System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::checkRestricted(System.String)
extern void WebHeaderCollection_checkRestricted_m54E7D4E4CBD5D55B094665C7C560C1CBB053455B ();
// 0x0000022C System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::checkState(System.Boolean)
extern void WebHeaderCollection_checkState_mEC96F37E68FEA213CEB012353F91E899FBFFA28B ();
// 0x0000022D System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::checkValue(System.String)
extern void WebHeaderCollection_checkValue_m4393AFF3438F9A0C62E7ED1CC7042792CF62B539 ();
// 0x0000022E System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m1276DF2C86EB3EF3E4DCBB92DC213C8591C0BB55 ();
// 0x0000022F System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m16846307C53AC8C6AFA22BE8A0A45BB75D96368D ();
// 0x00000230 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern void WebHeaderCollection_doWithoutCheckingName_m8DD5B4E102E31035088A327997FCF384A28903E7 ();
// 0x00000231 WebSocketSharpUnityMod.Net.HttpHeaderInfo WebSocketSharpUnityMod.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern void WebHeaderCollection_getHeaderInfo_mC060F193D9A1ACFA77131335C35EFC558B7C0396 ();
// 0x00000232 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_isRestricted_m4C7A8745A5CF2B43C765C33E291944A591D699DC ();
// 0x00000233 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::removeWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_removeWithoutCheckingName_m19D6E06C26000DDCECAD4F011C5E698B01874B61 ();
// 0x00000234 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_setWithoutCheckingName_mF2CCE21EFF6E4FC6E87D39736E30B495D18B8715 ();
// 0x00000235 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m6375B6DD1CC86EA914C621A774AE603D4F54CF8A ();
// 0x00000236 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m91BC047E018EA298D48361180C971088B6098FA9 ();
// 0x00000237 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsHeaderName(System.String)
extern void WebHeaderCollection_IsHeaderName_m61ED5B919E7F6775067275B9970873F05D841CA5 ();
// 0x00000238 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsHeaderValue(System.String)
extern void WebHeaderCollection_IsHeaderValue_mC80B653B1AACC1B361EAFC2AD98DB7D32F5EB0A2 ();
// 0x00000239 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsMultiValue(System.String,System.Boolean)
extern void WebHeaderCollection_IsMultiValue_m905902EE4DFB65920B05565DEC15DAFC5EC41D5C ();
// 0x0000023A System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_mDF017822442501154F28DF402CD9CA0534649FFB ();
// 0x0000023B System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_m6FD17B1987139AB296121AC8B962CC77628782E9 ();
// 0x0000023C System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_m1A6AEEAADD288D2761973CE16F367FD204FBE829 ();
// 0x0000023D System.Collections.IEnumerator WebSocketSharpUnityMod.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m67218F489DA78C5FCB7542A5D0046B80C6FE0BA6 ();
// 0x0000023E System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_m6CDCA1F712BD44CCAF6E9E3E4F15211EDE1C418B ();
// 0x0000023F System.String[] WebSocketSharpUnityMod.Net.WebHeaderCollection::GetValues(System.String)
extern void WebHeaderCollection_GetValues_mAC3638CF807EF70CAE0E573C663031569B4BA6AA ();
// 0x00000240 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_mBE2CECEF70450E184B46C4190988987026C9DCC6 ();
// 0x00000241 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_mD60C05CEDCD4AC967FBBD6421439AB0B08C4F502 ();
// 0x00000242 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Remove(System.String)
extern void WebHeaderCollection_Remove_m13A71F6CAC6880770C0F28DB7D949270BB5C590C ();
// 0x00000243 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Set(System.String,System.String)
extern void WebHeaderCollection_Set_mC935065D4FC845C4FB5D89BC46C342737F55A079 ();
// 0x00000244 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_mA147577A18E6B01F512A2E3AB1EEF5A0A036BA93 ();
// 0x00000245 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m0DBBE00BBBCED171F8D1149E6F471E0D3624F5B6 ();
// 0x00000246 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection_<>c__DisplayClass5::.ctor()
extern void U3CU3Ec__DisplayClass5__ctor_m480EA80C30844DF63662B21C53688D023657CC42 ();
// 0x00000247 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection_<>c__DisplayClass5::<GetObjectData>b__4(System.Int32)
extern void U3CU3Ec__DisplayClass5_U3CGetObjectDataU3Eb__4_mE5150550CCF198D1D966D8A102733FA8C306DC99 ();
// 0x00000248 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection_<>c__DisplayClass8::.ctor()
extern void U3CU3Ec__DisplayClass8__ctor_m20E812CA52FFA1CB0CF3668358AB900B04DE4462 ();
// 0x00000249 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection_<>c__DisplayClass8::<ToString>b__7(System.Int32)
extern void U3CU3Ec__DisplayClass8_U3CToStringU3Eb__7_m7FA4E165490E2CC91CE7D912D043B3D318CCEA76 ();
// 0x0000024A System.Void WebSocketSharpUnityMod.PayloadData::.cctor()
extern void PayloadData__cctor_m792B5B2B91A5CA57615911875A8FE9CC2A9868B4 ();
// 0x0000024B System.Void WebSocketSharpUnityMod.PayloadData::.ctor()
extern void PayloadData__ctor_mBE4F21A9BD6CF6D496009EDB152D6C75264E6C23 ();
// 0x0000024C System.Void WebSocketSharpUnityMod.PayloadData::.ctor(System.Byte[])
extern void PayloadData__ctor_m0DBD961CE282B53AE396003E0B52D2EDA60ACC88 ();
// 0x0000024D System.Void WebSocketSharpUnityMod.PayloadData::.ctor(System.Byte[],System.Int64)
extern void PayloadData__ctor_m3867534B15BBEDD817598E13C2B51A56B63278C0 ();
// 0x0000024E System.Boolean WebSocketSharpUnityMod.PayloadData::get_IncludesReservedCloseStatusCode()
extern void PayloadData_get_IncludesReservedCloseStatusCode_m71CDC65D98F79DD138C5AD395216A5ECF8C37470 ();
// 0x0000024F System.Byte[] WebSocketSharpUnityMod.PayloadData::get_ApplicationData()
extern void PayloadData_get_ApplicationData_m664C38EAB0B230F72CC71E1403A0093F11B956FC ();
// 0x00000250 System.UInt64 WebSocketSharpUnityMod.PayloadData::get_Length()
extern void PayloadData_get_Length_m5BFB160D8BA70352C6522D791291FBCBD8E3A2D4 ();
// 0x00000251 System.Void WebSocketSharpUnityMod.PayloadData::Mask(System.Byte[])
extern void PayloadData_Mask_m5C5F30222C620AE7052EF78CA3859260A2441C9E ();
// 0x00000252 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharpUnityMod.PayloadData::GetEnumerator()
extern void PayloadData_GetEnumerator_mA7F046102D35322DF5377BD40023DC0EDA37D779 ();
// 0x00000253 System.Byte[] WebSocketSharpUnityMod.PayloadData::ToArray()
extern void PayloadData_ToArray_mA0E5A038ED07F275F5FA42AABD9448225B912DAB ();
// 0x00000254 System.String WebSocketSharpUnityMod.PayloadData::ToString()
extern void PayloadData_ToString_mB753AF4739BE8F22A11A9CE3ACFB7184CB56636B ();
// 0x00000255 System.Collections.IEnumerator WebSocketSharpUnityMod.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern void PayloadData_System_Collections_IEnumerable_GetEnumerator_m69D7EA791E592823A452D80B4317ECA206C0957F ();
// 0x00000256 System.Boolean WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::MoveNext()
extern void U3CGetEnumeratorU3Ed__0_MoveNext_m26ED9E64C3057FD534B99D1B7B774B87957C51D6 ();
// 0x00000257 System.Byte WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m7357821B357EAA2AB10714F4F26A25DEF5E9BAFA ();
// 0x00000258 System.Void WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__0_System_Collections_IEnumerator_Reset_mBAA25918B87DB63A8A08F0F5B3B3297CC0CA0736 ();
// 0x00000259 System.Void WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__0_System_IDisposable_Dispose_m9B32B455D2121835AEF7EF97501E51747AFC0582 ();
// 0x0000025A System.Object WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__0_System_Collections_IEnumerator_get_Current_mFDB0F239B80463E2B0D8EB715BFEF2B6FA5D1426 ();
// 0x0000025B System.Void WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__0__ctor_m927209AA70A82C9174114B698E730157A49CD13C ();
// 0x0000025C System.Void WebSocketSharpUnityMod.PayloadData_<GetEnumerator>d__0::<>m__Finally2()
extern void U3CGetEnumeratorU3Ed__0_U3CU3Em__Finally2_mB40A1293F33B29CEBF1D6EEAA329417280FCBE6D ();
// 0x0000025D System.Void WebSocketSharpUnityMod.WebSocket::.cctor()
extern void WebSocket__cctor_m98035982F9181F2806340B0E27AF472E1A91316F ();
// 0x0000025E System.Void WebSocketSharpUnityMod.WebSocket::.ctor(System.String,System.String[])
extern void WebSocket__ctor_mCC4F266948EDBC68F15C2B3A92739C45AE771273 ();
// 0x0000025F System.Boolean WebSocketSharpUnityMod.WebSocket::get_HasMessage()
extern void WebSocket_get_HasMessage_m06775220F89C62EF8A24DFBDC15D458A6320D219 ();
// 0x00000260 WebSocketSharpUnityMod.Net.ClientSslConfiguration WebSocketSharpUnityMod.WebSocket::get_SslConfiguration()
extern void WebSocket_get_SslConfiguration_m8C80F3D37C0F59CB3AEC8A7026FA288998A58194 ();
// 0x00000261 System.Void WebSocketSharpUnityMod.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharpUnityMod.CloseEventArgs>)
extern void WebSocket_add_OnClose_mAE3B23969EEB9BC49D76B6C35A918B5F7548B8CB ();
// 0x00000262 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharpUnityMod.CloseEventArgs>)
extern void WebSocket_remove_OnClose_mFAA5F88DB8A2CA77EB4C0075D9564B174128BF02 ();
// 0x00000263 System.Void WebSocketSharpUnityMod.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharpUnityMod.ErrorEventArgs>)
extern void WebSocket_add_OnError_mF47BB16B56827F104368667279FB53DC3719C5FF ();
// 0x00000264 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharpUnityMod.ErrorEventArgs>)
extern void WebSocket_remove_OnError_m7C9EF6591B541C0CAA39E35D71AA6AB088333FD5 ();
// 0x00000265 System.Void WebSocketSharpUnityMod.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharpUnityMod.MessageEventArgs>)
extern void WebSocket_add_OnMessage_m8FFA9F0C345A87CC6650A1605BE1D81D00D50F7E ();
// 0x00000266 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharpUnityMod.MessageEventArgs>)
extern void WebSocket_remove_OnMessage_mD553C345B96BD518792DC2AF7F2E5A5E7B9B40F7 ();
// 0x00000267 System.Void WebSocketSharpUnityMod.WebSocket::add_OnOpen(System.EventHandler)
extern void WebSocket_add_OnOpen_m7ED907432F542DC99E2E7C802501F3AA3ECF1AA9 ();
// 0x00000268 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnOpen(System.EventHandler)
extern void WebSocket_remove_OnOpen_mBE42E90C6BA9F0D7524925856A131B7C173C207A ();
// 0x00000269 System.Boolean WebSocketSharpUnityMod.WebSocket::checkHandshakeResponse(WebSocketSharpUnityMod.HttpResponse,System.String&)
extern void WebSocket_checkHandshakeResponse_mD2D9D2EB276B10E9B2F30C3E45EC3CA41E620009 ();
// 0x0000026A System.Boolean WebSocketSharpUnityMod.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_m1B97F86F3BDCAE1BCFC04EA609B64557E57101B2 ();
// 0x0000026B System.Boolean WebSocketSharpUnityMod.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_m73D0114F90FF1980E7AA59D12928128E63E2809A ();
// 0x0000026C System.Boolean WebSocketSharpUnityMod.WebSocket::checkReceivedFrame(WebSocketSharpUnityMod.WebSocketFrame,System.String&)
extern void WebSocket_checkReceivedFrame_mA0B093E77C5BFA458E9CA38C7462F80C9A125125 ();
// 0x0000026D System.Void WebSocketSharpUnityMod.WebSocket::close(WebSocketSharpUnityMod.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_close_m6D83AF3C34828B58580EC13AD59FF80EC187E2B9 ();
// 0x0000026E System.Void WebSocketSharpUnityMod.WebSocket::closeAsync(WebSocketSharpUnityMod.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_closeAsync_mC8BC3589E578D14193277C1D0B67547570A1C8CB ();
// 0x0000026F System.Boolean WebSocketSharpUnityMod.WebSocket::closeHandshake(System.Byte[],System.Boolean,System.Boolean)
extern void WebSocket_closeHandshake_m4C0FCCC8F69D808BAC349BA02FFF52F8DD38E8E4 ();
// 0x00000270 System.Boolean WebSocketSharpUnityMod.WebSocket::connect()
extern void WebSocket_connect_m68EA5F51A09C8E38F498D80E9049BFBF22B2AED8 ();
// 0x00000271 System.String WebSocketSharpUnityMod.WebSocket::createExtensions()
extern void WebSocket_createExtensions_m23439C2AB665870741AB9FB4561E289C63B891B8 ();
// 0x00000272 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.WebSocket::createHandshakeRequest()
extern void WebSocket_createHandshakeRequest_m46397E9ACF28159508B71F43302DB2C525DB2871 ();
// 0x00000273 System.Boolean WebSocketSharpUnityMod.WebSocket::doHandshake()
extern void WebSocket_doHandshake_m3CC523AD884D63940611A1F33CD215E81E007FDC ();
// 0x00000274 System.Void WebSocketSharpUnityMod.WebSocket::enqueueToMessageEventQueue(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocket_enqueueToMessageEventQueue_m774EBC0EDD934EADF3BA29F08E43F0FB14182FEC ();
// 0x00000275 System.Void WebSocketSharpUnityMod.WebSocket::error(System.String,System.Exception)
extern void WebSocket_error_m9B719713DCD16DCCFF88048B1203500CA316A49D ();
// 0x00000276 System.Void WebSocketSharpUnityMod.WebSocket::fatal(System.String,System.Exception)
extern void WebSocket_fatal_m7E41521E5A7D204AFFC1E3EB85EE16314E1A50E2 ();
// 0x00000277 System.Void WebSocketSharpUnityMod.WebSocket::fatal(System.String,WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocket_fatal_mCCBA6922C448AC1346F5DDAB5F4DEFADD7B2EAA9 ();
// 0x00000278 System.Void WebSocketSharpUnityMod.WebSocket::init()
extern void WebSocket_init_m85902AF3CCC027479A360D7DF22935CEC37B47A1 ();
// 0x00000279 System.Void WebSocketSharpUnityMod.WebSocket::message()
extern void WebSocket_message_mEAA51906CD00DE2FE7866982E2A5FC6762D21379 ();
// 0x0000027A System.Void WebSocketSharpUnityMod.WebSocket::messagec(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocket_messagec_mB68B23CE3D2F7DE889194AD17C7AD6D7539BE792 ();
// 0x0000027B System.Void WebSocketSharpUnityMod.WebSocket::open()
extern void WebSocket_open_m722485B47DE06FA65ED6115CA30F965CBC97FC1D ();
// 0x0000027C System.Boolean WebSocketSharpUnityMod.WebSocket::processCloseFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processCloseFrame_mBE17F899B1503700332D66A34F0EB59103736AC7 ();
// 0x0000027D System.Void WebSocketSharpUnityMod.WebSocket::processCookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void WebSocket_processCookies_m8AED395626EFD82C6CFC4A8CC084316B43EFA7BD ();
// 0x0000027E System.Boolean WebSocketSharpUnityMod.WebSocket::processDataFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processDataFrame_m5F766D674A538C1797418865E836AE60022D755E ();
// 0x0000027F System.Boolean WebSocketSharpUnityMod.WebSocket::processFragmentFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processFragmentFrame_m072D1FC879595D3F8AF2EEBAE60D68E1DBEFBF1A ();
// 0x00000280 System.Boolean WebSocketSharpUnityMod.WebSocket::processPingFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processPingFrame_mDE0F69459EA75CEBB8C13C75BAC7AEF855288021 ();
// 0x00000281 System.Boolean WebSocketSharpUnityMod.WebSocket::processPongFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processPongFrame_m50C268C80C47C5D0C477AA218AF923C1E15CC482 ();
// 0x00000282 System.Boolean WebSocketSharpUnityMod.WebSocket::processReceivedFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processReceivedFrame_mEA96A0A49C0EC14D35A01A17A5D43D634B93DD33 ();
// 0x00000283 System.Void WebSocketSharpUnityMod.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsServerHeader_mBFC0A51B44EDDE7CA938C6752ABA9F2906B2E6A0 ();
// 0x00000284 System.Boolean WebSocketSharpUnityMod.WebSocket::processUnsupportedFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processUnsupportedFrame_m85E19972B477387FFB7184CA99E98539558CE726 ();
// 0x00000285 System.Void WebSocketSharpUnityMod.WebSocket::releaseClientResources()
extern void WebSocket_releaseClientResources_m9ED6DDDF32FD8AD32EE14FFA426953B33768673E ();
// 0x00000286 System.Void WebSocketSharpUnityMod.WebSocket::releaseCommonResources()
extern void WebSocket_releaseCommonResources_mB81B41E30DD9AF962841823AA1F2A788208F450D ();
// 0x00000287 System.Void WebSocketSharpUnityMod.WebSocket::releaseResources()
extern void WebSocket_releaseResources_m02FFFE97712A73FF2349392C1FF6FF606DE0766C ();
// 0x00000288 System.Void WebSocketSharpUnityMod.WebSocket::releaseServerResources()
extern void WebSocket_releaseServerResources_m6BAA49461E5A52F8CEE47EDE050457675477BA64 ();
// 0x00000289 System.Boolean WebSocketSharpUnityMod.WebSocket::send(System.Byte[])
extern void WebSocket_send_mB2483C53CEBD43EB0383FF42901DA9727BAC5F83 ();
// 0x0000028A System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Opcode,System.IO.Stream)
extern void WebSocket_send_m72AC66A2ADEEF5A408CA9756945181949DF9612F ();
// 0x0000028B System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Boolean)
extern void WebSocket_send_m8654F92C90A67C55B58208C1E81F00AD06382571 ();
// 0x0000028C System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,System.Byte[],System.Boolean)
extern void WebSocket_send_m8DB180561110C7801126CF22CAA06732201353A6 ();
// 0x0000028D System.Boolean WebSocketSharpUnityMod.WebSocket::sendBytes(System.Byte[])
extern void WebSocket_sendBytes_m62FC307662CC170D09A859B814E83D9DADFB0BA2 ();
// 0x0000028E WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::sendHandshakeRequest()
extern void WebSocket_sendHandshakeRequest_m9246D270FE8681FE165B0EB85AA74010A992E27E ();
// 0x0000028F WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::sendHttpRequest(WebSocketSharpUnityMod.HttpRequest,System.Int32)
extern void WebSocket_sendHttpRequest_mEAA3EF54C07AC4E613D33577721FBEFF2EFE1EE6 ();
// 0x00000290 System.Void WebSocketSharpUnityMod.WebSocket::sendProxyConnectRequest()
extern void WebSocket_sendProxyConnectRequest_m6EB1D033BED1C7893C4412ED8728E73C17A40E4B ();
// 0x00000291 System.Void WebSocketSharpUnityMod.WebSocket::setClientStream()
extern void WebSocket_setClientStream_mE95F7B8D381C33C5E078190F46B4325EDE870C18 ();
// 0x00000292 System.Void WebSocketSharpUnityMod.WebSocket::setClientStreamOriginal()
extern void WebSocket_setClientStreamOriginal_mEA48659AB41A3448B551FD50AF4AC41143FC250D ();
// 0x00000293 System.Boolean WebSocketSharpUnityMod.WebSocket::TrySetClientStream(System.Int32)
extern void WebSocket_TrySetClientStream_mACDD30B8B5012E84938D7828B80959936A5F12B8 ();
// 0x00000294 System.Void WebSocketSharpUnityMod.WebSocket::startReceiving()
extern void WebSocket_startReceiving_m72B8A5C33B9797BB4E58AE3F4F142D85AC5674CC ();
// 0x00000295 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern void WebSocket_validateSecWebSocketAcceptHeader_m9A0399F441ABCA7634E7443274C07CA11F91BB1E ();
// 0x00000296 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsServerHeader_mB551E2FD3978778081F30B859F4C4755CB58415A ();
// 0x00000297 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolServerHeader_mD3000F563F4B70C81EED45E8587C8B27C67F5892 ();
// 0x00000298 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionServerHeader_m8F7004DEA113C120D1A1E0000600A4F1299FBED6 ();
// 0x00000299 System.String WebSocketSharpUnityMod.WebSocket::CheckSendParameter(System.Byte[])
extern void WebSocket_CheckSendParameter_mBAC975DA8D81B62FEEC22F44EE97E611E3F6BCAA ();
// 0x0000029A System.String WebSocketSharpUnityMod.WebSocket::CreateBase64Key()
extern void WebSocket_CreateBase64Key_m8D3796EA7666A09290C8A592C4A2E93C0BD4C482 ();
// 0x0000029B System.String WebSocketSharpUnityMod.WebSocket::CreateResponseKey(System.String)
extern void WebSocket_CreateResponseKey_m3FAE41707C77B48180ABACC8465447E8F82036C1 ();
// 0x0000029C System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync()
extern void WebSocket_CloseAsync_mE024BB9EEA317959740890F8A3889F9C7CEF4378 ();
// 0x0000029D System.Void WebSocketSharpUnityMod.WebSocket::ConnectAsync()
extern void WebSocket_ConnectAsync_m8E7B6AB5EBCC59E5D764FC7A50FAFE03C20AA230 ();
// 0x0000029E System.Void WebSocketSharpUnityMod.WebSocket::Send(System.Byte[])
extern void WebSocket_Send_mC8B41404AD501120AE09BBAC9CFCB0E8154BC823 ();
// 0x0000029F System.Void WebSocketSharpUnityMod.WebSocket::System.IDisposable.Dispose()
extern void WebSocket_System_IDisposable_Dispose_m9C2A05A1AC9D688645BE69E76B93708CAF68616C ();
// 0x000002A0 System.Void WebSocketSharpUnityMod.WebSocket::<open>b__f(System.IAsyncResult)
extern void WebSocket_U3CopenU3Eb__f_m86A7758F9CD398D52378ED57075D54AC187A4981 ();
// 0x000002A1 System.Void WebSocketSharpUnityMod.WebSocket::<TrySetClientStream>b__14(System.IAsyncResult)
extern void WebSocket_U3CTrySetClientStreamU3Eb__14_mE77B5DAD1366CE8F84B79CEF72CAFBB59FA9C542 ();
// 0x000002A2 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClassa::.ctor()
extern void U3CU3Ec__DisplayClassa__ctor_m296FB04282590699EC05F8223BCEDB82A7404113 ();
// 0x000002A3 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClassa::<closeAsync>b__9(System.IAsyncResult)
extern void U3CU3Ec__DisplayClassa_U3CcloseAsyncU3Eb__9_m9470F054C642BDB5265C5AB85E257CCDCCDC4D69 ();
// 0x000002A4 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass19::.ctor()
extern void U3CU3Ec__DisplayClass19__ctor_m87782B82D2CF169A714EE490B6CC97C1FAE82671 ();
// 0x000002A5 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass19::<startReceiving>b__16()
extern void U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__16_m27EDF8EA9A21C97D5512CF96C08772D33E4172FF ();
// 0x000002A6 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass19::<startReceiving>b__17(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__17_m0372F7D546B868ADA53B1BB7D705F7E2BC6A2997 ();
// 0x000002A7 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass19::<startReceiving>b__18(System.Exception)
extern void U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__18_m73F73F6F378C8B9101AB10DA66C4E7452937F93D ();
// 0x000002A8 System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass1c::.ctor()
extern void U3CU3Ec__DisplayClass1c__ctor_m3B17892AA015D0C013C98E096E770CB97BF48CEA ();
// 0x000002A9 System.Boolean WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass1c::<validateSecWebSocketExtensionsServerHeader>b__1b(System.String)
extern void U3CU3Ec__DisplayClass1c_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__1b_m888259490CB257E79BFD0D3F3FF5D676F8C19811 ();
// 0x000002AA System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass1f::.ctor()
extern void U3CU3Ec__DisplayClass1f__ctor_m51A799EB2EC3A6C40A97DABDD623C27D7FE661F4 ();
// 0x000002AB System.Boolean WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass1f::<validateSecWebSocketProtocolServerHeader>b__1e(System.String)
extern void U3CU3Ec__DisplayClass1f_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__1e_mDB32D547B230628E18E80BB6B9E1188339A5D3EC ();
// 0x000002AC System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass25::.ctor()
extern void U3CU3Ec__DisplayClass25__ctor_mB4717B4A9E7EE789C822A143B41A785EFED78BD7 ();
// 0x000002AD System.Void WebSocketSharpUnityMod.WebSocket_<>c__DisplayClass25::<ConnectAsync>b__24(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass25_U3CConnectAsyncU3Eb__24_mD6A1C1BA857DCF93C5615EAC543C3D3FD972DE8F ();
// 0x000002AE System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(System.String)
extern void WebSocketException__ctor_m17B4F2757F024B5E70799FB417481500525B1A62 ();
// 0x000002AF System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocketException__ctor_m30FB71D410A6F11923B99796009820254C1E93F4 ();
// 0x000002B0 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(System.String,System.Exception)
extern void WebSocketException__ctor_mDDAB2306941E75DBE02BEED24274225DE56C0DF2 ();
// 0x000002B1 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.Exception)
extern void WebSocketException__ctor_mFF81C814D67A1A760E9D3B9844049A8EB34321D4 ();
// 0x000002B2 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocketException__ctor_m8978DFB89AF2E6EE4BBC344CE1EEB099B0DF62FF ();
// 0x000002B3 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String,System.Exception)
extern void WebSocketException__ctor_m47C1E4AFFC6141B67632086DD4626CF3E74F7730 ();
// 0x000002B4 WebSocketSharpUnityMod.CloseStatusCode WebSocketSharpUnityMod.WebSocketException::get_Code()
extern void WebSocketException_get_Code_m9190D1BDBFB43EB512E9EBC6F9D0910281F5BFA9 ();
// 0x000002B5 System.Void WebSocketSharpUnityMod.WebSocketFrame::.cctor()
extern void WebSocketFrame__cctor_m9E48B3E758D026B48841B6CBEA6E4FD71F11B1B0 ();
// 0x000002B6 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor()
extern void WebSocketFrame__ctor_m037E607498FE0A61A9230E7033B65EADF820F338 ();
// 0x000002B7 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Opcode,WebSocketSharpUnityMod.PayloadData,System.Boolean)
extern void WebSocketFrame__ctor_m342593B1B594BFB620BAF41016123BE07A4CAD08 ();
// 0x000002B8 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_m0013F7C13B3670F677819AA8C7808E73474DD484 ();
// 0x000002B9 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,WebSocketSharpUnityMod.PayloadData,System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_mFAC565C97A447307CA0F8996B2DC1FEB44DF3A97 ();
// 0x000002BA System.Int32 WebSocketSharpUnityMod.WebSocketFrame::get_ExtendedPayloadLengthCount()
extern void WebSocketFrame_get_ExtendedPayloadLengthCount_mB8A7A85E3CF4176E95DC51E38A69AD0AE73456C7 ();
// 0x000002BB System.UInt64 WebSocketSharpUnityMod.WebSocketFrame::get_FullPayloadLength()
extern void WebSocketFrame_get_FullPayloadLength_mEED816EF2B8D6804E1C97B00272CD8415B8E3576 ();
// 0x000002BC System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsClose()
extern void WebSocketFrame_get_IsClose_m2C710CA83E4C48F54BEB16F2C7DBC13DF33D3EBB ();
// 0x000002BD System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsCompressed()
extern void WebSocketFrame_get_IsCompressed_m654232F9B6C4ED563511BB73037676ED34DCDEA3 ();
// 0x000002BE System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsContinuation()
extern void WebSocketFrame_get_IsContinuation_mA06255503060AF3EE98751FAA591CDD795632684 ();
// 0x000002BF System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsData()
extern void WebSocketFrame_get_IsData_m6A7238DE98BCCF63A2E8812A8D9B8D558714526B ();
// 0x000002C0 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsFinal()
extern void WebSocketFrame_get_IsFinal_mE3115CE026B4FB87E10D395DD5FA1357C38A6F25 ();
// 0x000002C1 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsFragment()
extern void WebSocketFrame_get_IsFragment_m78EFF154862697C167C57D54245D7747915DFE67 ();
// 0x000002C2 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsMasked()
extern void WebSocketFrame_get_IsMasked_mFCD6A5B5AB576E9BB75C07DE7C6794B5F914F6FA ();
// 0x000002C3 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsPing()
extern void WebSocketFrame_get_IsPing_mC6AF4928175DF2A561C494E8F85866086D0C013D ();
// 0x000002C4 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsPong()
extern void WebSocketFrame_get_IsPong_mED1724210A02A7959B38F6E5B07A9A86A6E3E902 ();
// 0x000002C5 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsText()
extern void WebSocketFrame_get_IsText_m12ABC6D4A74426424D87997A57AC63C0B1D176FE ();
// 0x000002C6 System.UInt64 WebSocketSharpUnityMod.WebSocketFrame::get_Length()
extern void WebSocketFrame_get_Length_mC732C1FAAB740E2673FB56DDFEDD49E22FA54538 ();
// 0x000002C7 WebSocketSharpUnityMod.Opcode WebSocketSharpUnityMod.WebSocketFrame::get_Opcode()
extern void WebSocketFrame_get_Opcode_m62DDBCCECDB609D9DB270D1655ED18108CE695FC ();
// 0x000002C8 WebSocketSharpUnityMod.PayloadData WebSocketSharpUnityMod.WebSocketFrame::get_PayloadData()
extern void WebSocketFrame_get_PayloadData_m9763EC127D7C389FCE1ECDEE67E777B6AD0D6635 ();
// 0x000002C9 WebSocketSharpUnityMod.Rsv WebSocketSharpUnityMod.WebSocketFrame::get_Rsv2()
extern void WebSocketFrame_get_Rsv2_m9C572E43E3C4384BC89D2E34C20E76769E571173 ();
// 0x000002CA WebSocketSharpUnityMod.Rsv WebSocketSharpUnityMod.WebSocketFrame::get_Rsv3()
extern void WebSocketFrame_get_Rsv3_m47DA852603CBDA6EE65EC04AA0C4A1E8D26304B7 ();
// 0x000002CB System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::createMaskingKey()
extern void WebSocketFrame_createMaskingKey_m9EA9323DC90B53F3A675BF89B6ECA6B8AFA8C78F ();
// 0x000002CC System.String WebSocketSharpUnityMod.WebSocketFrame::dump(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_dump_m441FDCB5CFF26764E7BEE26E9CD4D9BE28A7172A ();
// 0x000002CD System.String WebSocketSharpUnityMod.WebSocketFrame::print(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_print_mCED4B356FEA339F87D2E2EEDF6E9FBA08D3BFEF9 ();
// 0x000002CE WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::processHeader(System.Byte[])
extern void WebSocketFrame_processHeader_mAFCECF1F986C2E3A1A960C42004A5B89385B3CA9 ();
// 0x000002CF System.Void WebSocketSharpUnityMod.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readExtendedPayloadLengthAsync_mDB59DE752266FF8012F24B321660C8F11BB4E8C3 ();
// 0x000002D0 System.Void WebSocketSharpUnityMod.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readHeaderAsync_m43A9CF70493FF6D68A5899099372723D30B86EA6 ();
// 0x000002D1 System.Void WebSocketSharpUnityMod.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readMaskingKeyAsync_mC318CC8738621ABC92991830165F5AB33DB29AAF ();
// 0x000002D2 System.Void WebSocketSharpUnityMod.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readPayloadDataAsync_mAB298585C85ECBFC396A74914C24A67288F25F78 ();
// 0x000002D3 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::CreateCloseFrame(WebSocketSharpUnityMod.PayloadData,System.Boolean)
extern void WebSocketFrame_CreateCloseFrame_m681CD2814AC45C21786570159BF45E6756F1AA97 ();
// 0x000002D4 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::CreatePingFrame(System.Boolean)
extern void WebSocketFrame_CreatePingFrame_mF6E79DF65ECF3B352727BB42521F956D95CB3F57 ();
// 0x000002D5 System.Void WebSocketSharpUnityMod.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_ReadFrameAsync_m361390F8E16FE535199DA22AC6FDE08D8C3B5C85 ();
// 0x000002D6 System.Void WebSocketSharpUnityMod.WebSocketFrame::Unmask()
extern void WebSocketFrame_Unmask_m0C113F0BA69EE89CA1F5FB84920E3F020980E5D6 ();
// 0x000002D7 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharpUnityMod.WebSocketFrame::GetEnumerator()
extern void WebSocketFrame_GetEnumerator_mD31370ED87158275EF87932B4A5EF291109BFFA8 ();
// 0x000002D8 System.String WebSocketSharpUnityMod.WebSocketFrame::PrintToString(System.Boolean)
extern void WebSocketFrame_PrintToString_m4BB9563342AE3313D11CAEB5DDF9BC0A88BC0A5E ();
// 0x000002D9 System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::ToArray()
extern void WebSocketFrame_ToArray_m03558D4CF594154CCE80A45FE5204094F39D565D ();
// 0x000002DA System.String WebSocketSharpUnityMod.WebSocketFrame::ToString()
extern void WebSocketFrame_ToString_m1DF85247AA8EF4B8A7565BDB7F0B1B7A151BFF98 ();
// 0x000002DB System.Collections.IEnumerator WebSocketSharpUnityMod.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern void WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mD04B5573706066E38DB70205B4D976E7BCD7E879 ();
// 0x000002DC System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass2::.ctor()
extern void U3CU3Ec__DisplayClass2__ctor_m9B2559761F7A54E94C559C44F25F35BBAE24C440 ();
// 0x000002DD System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass2::<dump>b__0()
extern void U3CU3Ec__DisplayClass2_U3CdumpU3Eb__0_mFC63C6CE0FDBB36F50458BCB4FC2BC6039E21624 ();
// 0x000002DE System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass2_<>c__DisplayClass4::.ctor()
extern void U3CU3Ec__DisplayClass4__ctor_mCFC2E9FFCFB90BEB1D8BAF9DC2BEA174FC9B808F ();
// 0x000002DF System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass2_<>c__DisplayClass4::<dump>b__1(System.String,System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass4_U3CdumpU3Eb__1_mF18215167F7DEA4E3525DF66BC87B5472FFA2466 ();
// 0x000002E0 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass7::.ctor()
extern void U3CU3Ec__DisplayClass7__ctor_mE7232DF8359DA41CF4D73ED5EC72C49A878FB355 ();
// 0x000002E1 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass7::<readExtendedPayloadLengthAsync>b__6(System.Byte[])
extern void U3CU3Ec__DisplayClass7_U3CreadExtendedPayloadLengthAsyncU3Eb__6_m6F0734CDF6CE2E98FE5C3B0D19EE049A54F2AB88 ();
// 0x000002E2 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClassa::.ctor()
extern void U3CU3Ec__DisplayClassa__ctor_m395090876EB7B1CF96A8A0C8534E8CEAFC0BA2DD ();
// 0x000002E3 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClassa::<readHeaderAsync>b__9(System.Byte[])
extern void U3CU3Ec__DisplayClassa_U3CreadHeaderAsyncU3Eb__9_m1269BB3DC98BD6C7AAA0204FF53D51D4271AC769 ();
// 0x000002E4 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClassd::.ctor()
extern void U3CU3Ec__DisplayClassd__ctor_mF342F4F999DB455F7838A6390A377B87B19F7C65 ();
// 0x000002E5 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClassd::<readMaskingKeyAsync>b__c(System.Byte[])
extern void U3CU3Ec__DisplayClassd_U3CreadMaskingKeyAsyncU3Eb__c_m34866E43A32C4A5E8B9FE821D46AF3F230B0C3DB ();
// 0x000002E6 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass10::.ctor()
extern void U3CU3Ec__DisplayClass10__ctor_m125417832C3E0DE015992070289B00B0D0BA9D72 ();
// 0x000002E7 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass10::<readPayloadDataAsync>b__f(System.Byte[])
extern void U3CU3Ec__DisplayClass10_U3CreadPayloadDataAsyncU3Eb__f_mEEBECB7CA00B0D4AE0D3E1B7BDD8A390E2068274 ();
// 0x000002E8 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass16::.ctor()
extern void U3CU3Ec__DisplayClass16__ctor_mEF5CC0C41268BA10D263D5CA017A76DB60F35656 ();
// 0x000002E9 System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass16::<ReadFrameAsync>b__12(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__12_m70A10814E1E23EF5F08FA9D7293DF63F5288DCFA ();
// 0x000002EA System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass16::<ReadFrameAsync>b__13(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__13_m8C0D376E0FE9A550D61F84896FA31A5960C67443 ();
// 0x000002EB System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass16::<ReadFrameAsync>b__14(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__14_mE3240193FAA86BE217E8FE76B2D109783FDB15CB ();
// 0x000002EC System.Void WebSocketSharpUnityMod.WebSocketFrame_<>c__DisplayClass16::<ReadFrameAsync>b__15(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__15_mA86FBA95D4ECC30F0176A7FC02A710CF57E29DF5 ();
// 0x000002ED System.Boolean WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::MoveNext()
extern void U3CGetEnumeratorU3Ed__18_MoveNext_m891487F0EE1FB7B22944D693E89D6F1B4410FD7C ();
// 0x000002EE System.Byte WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m7F68BEAF106255123ED843C0B82C84E588A5195A ();
// 0x000002EF System.Void WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__18_System_Collections_IEnumerator_Reset_mAB9EE2C0F26E02E60703525047A12BF192D8FFED ();
// 0x000002F0 System.Void WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__18_System_IDisposable_Dispose_m4B0C0E67D480B6E730BA807479A148481C329892 ();
// 0x000002F1 System.Object WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__18_System_Collections_IEnumerator_get_Current_m1844F841F06656F0F3C2458112BC51F92FCFF8A4 ();
// 0x000002F2 System.Void WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__18__ctor_m1CE2D3F84C434505A39EEC11162B74C263D43836 ();
// 0x000002F3 System.Void WebSocketSharpUnityMod.WebSocketFrame_<GetEnumerator>d__18::<>m__Finally1a()
extern void U3CGetEnumeratorU3Ed__18_U3CU3Em__Finally1a_m3AB33D211D172E5CDA521EC32E1904A0D73ED724 ();
// 0x000002F4 System.Net.Security.RemoteCertificateValidationCallback Byn.Awrtc.Native.WebsocketSharpClient::get_CertCallback()
extern void WebsocketSharpClient_get_CertCallback_mE0A2333EB99C94C5CB8A27F191CA76241FB2A3BA ();
// 0x000002F5 System.Void Byn.Awrtc.Native.WebsocketSharpClient::set_CertCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void WebsocketSharpClient_set_CertCallback_mEB332F20C6B6DA14F09ED60E690B715950458B50 ();
// 0x000002F6 System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
extern void WebsocketSharpClient_add_OnOpen_m0122B05D29E9609FB917D3E5F467E6E25D7E77F3 ();
// 0x000002F7 System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
extern void WebsocketSharpClient_remove_OnOpen_m1EAB76D084E7983E800C675FA1FCF8E3CA058B9A ();
// 0x000002F8 System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnClose(Byn.Awrtc.Base.OnCloseCallback)
extern void WebsocketSharpClient_add_OnClose_m5CB8EB56754F086164C1AA27CAD1779A7F37D74A ();
// 0x000002F9 System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnClose(Byn.Awrtc.Base.OnCloseCallback)
extern void WebsocketSharpClient_remove_OnClose_m736A1BA53AF719DDE591598BAD4E2703D035E99B ();
// 0x000002FA System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnError(Byn.Awrtc.Base.OnErrorCallback)
extern void WebsocketSharpClient_add_OnError_m36794324B28AC385A21C475A7D71FC01EC29417B ();
// 0x000002FB System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnError(Byn.Awrtc.Base.OnErrorCallback)
extern void WebsocketSharpClient_remove_OnError_m5BD76B1F05E7BD0BA99033C060D085DC25EB41AB ();
// 0x000002FC System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
extern void WebsocketSharpClient_add_OnMessage_m970739596577BE7E462E3A6CE586463754C4DDF8 ();
// 0x000002FD System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
extern void WebsocketSharpClient_remove_OnMessage_mA5F9A97C9C1B8CC7693813E57E881B8E7ECD3D9A ();
// 0x000002FE System.Void Byn.Awrtc.Native.WebsocketSharpClient::.ctor()
extern void WebsocketSharpClient__ctor_m06EA31072566A1D950E81CBCB209A6475240BDAB ();
// 0x000002FF System.Void Byn.Awrtc.Native.WebsocketSharpClient::Connect(System.String)
extern void WebsocketSharpClient_Connect_m78FCD8ECD2740625513085F0DE0803615C617A91 ();
// 0x00000300 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Send(System.Byte[])
extern void WebsocketSharpClient_Send_mE2D350B88C26389E30EE1A6ED4525E37DD2BD973 ();
// 0x00000301 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnOpen(System.Object,System.EventArgs)
extern void WebsocketSharpClient_OnWebsocketOnOpen_m1B0394B373221DC4D7DA18F2F7FE96182E77EAD3 ();
// 0x00000302 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnClose(System.Object,WebSocketSharpUnityMod.CloseEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnClose_mC62449E00D04761CA5B601FF4F7D070DD024E1E5 ();
// 0x00000303 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnMessage(System.Object,WebSocketSharpUnityMod.MessageEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnMessage_mCA8EF11B63B203707F832B8A114A0DB199097273 ();
// 0x00000304 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnError(System.Object,WebSocketSharpUnityMod.ErrorEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnError_mFCBF8035F97B49EA9DA502D3441DDEEE5252A21E ();
// 0x00000305 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Dispose(System.Boolean)
extern void WebsocketSharpClient_Dispose_m1C3CC621A326E262495B49B1FD09AB2AB75E8A42 ();
// 0x00000306 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Dispose()
extern void WebsocketSharpClient_Dispose_m9BC82124AA02233013A4C9675DFA041620CA0F1F ();
// 0x00000307 System.Net.Security.RemoteCertificateValidationCallback Byn.Awrtc.Native.WebsocketSharpFactory::get_CertCallbackOverride()
extern void WebsocketSharpFactory_get_CertCallbackOverride_m8C873CA91DB6F376DFCCFEBA69B2D7A05E7ABED3 ();
// 0x00000308 System.Void Byn.Awrtc.Native.WebsocketSharpFactory::set_CertCallbackOverride(System.Net.Security.RemoteCertificateValidationCallback)
extern void WebsocketSharpFactory_set_CertCallbackOverride_mF9DD44B7D61CE4611ABEC897115D9D8253ED41B2 ();
// 0x00000309 System.Void Byn.Awrtc.Native.WebsocketSharpFactory::.ctor()
extern void WebsocketSharpFactory__ctor_mDB3E46038A9096A07CCF04853542177F1F0511DA ();
// 0x0000030A System.Boolean Byn.Awrtc.Native.WebsocketSharpFactory::DefaultCertCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void WebsocketSharpFactory_DefaultCertCallback_m8E65079A8024D991F90548405C7CEA895CBB0961 ();
// 0x0000030B Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Native.WebsocketSharpFactory::Create()
extern void WebsocketSharpFactory_Create_m58BA0DD3C46E53D9D59FC0DA824776849EA106C8 ();
static Il2CppMethodPointer s_methodPointers[779] = 
{
	AWebRtcNetwork_get_IdToConnection_m4CA2E8D816FD12B632F91512B30FA65CCFBF3098,
	AWebRtcNetwork__ctor_mD5C3898B208A4FBC1016A9A179BFBD610A4A1CA9,
	NULL,
	AWebRtcNetwork_StartServer_m1D5FBCD6FC01C00F21AE9B8E1C309C00E2B7603A,
	AWebRtcNetwork_StopServer_mEAD0CA62887D3145333D84ED6390D3AC92D09C45,
	AWebRtcNetwork_Connect_m2858D1011562CE953A370546F2B33B0E7ACC4A74,
	AWebRtcNetwork_Dequeue_m090C2801F4CE22845D9CBE4BF81A87C97B62F22B,
	AWebRtcNetwork_Flush_mF08541735C7E3B2FBD6CFE1A44FD863331C211DA,
	AWebRtcNetwork_SendData_m4EA111835C244AE06183AB7EF90C6C7920E091C9,
	AWebRtcNetwork_Disconnect_m164354BD0291ECA083EC81A1BDA8A4A101A1D82F,
	AWebRtcNetwork_Update_m43606A9785E192547C7ED702AF3A25530ABB1DBC,
	AWebRtcNetwork_Shutdown_m8CD81741B59CAA51C4BF26E868C365064A5F7C7C,
	AWebRtcNetwork_CheckSignalingState_mC69D9CF1F03F1368DC6997017E53EA6AFA15A552,
	AWebRtcNetwork_UpdateSignalingNetwork_m4B164DC5BA3109A68C58529B9B711F493FAF3DFE,
	AWebRtcNetwork_UpdatePeers_m089CC02D5ED0372B62CF827413DC85D787AC6A34,
	AWebRtcNetwork_AddOutgoingConnection_m4854D9CB0213ED5BB80AE9B62BE173EFB3A1B33A,
	AWebRtcNetwork_AddIncomingConnection_m0A50BDDB5E7887CBAA6227E782F91DD3C8AEDD8A,
	AWebRtcNetwork_ConnectionEtablished_mBAC5E474A1017FA704C4349A4CE085475B886D49,
	AWebRtcNetwork_InitialSignalingFailed_mBA5CA5FA7B09B70E162514876F43D2E65CA94CA2,
	AWebRtcNetwork_HandleDisconnect_m322DA33D4504FF532B177E30C04FDF34931D7428,
	AWebRtcNetwork_StringToBuffer_m7A8B58C39DE5FC1D3278E5BB5F2EACE704FC6B02,
	AWebRtcNetwork_BufferToString_m6EDA9DA20D81F91A2113446F63115E8312569ED6,
	AWebRtcNetwork_NextConnectionId_m32545ABD09F2DB9DBB2AB34CB353AD875AC788AA,
	AWebRtcNetwork_Dispose_m0909A5BA081EF6D68B0847E7EC2663ECDEC234C5,
	AWebRtcNetwork_Dispose_mDFA0F7A08F1B7B7A715DD675963BBC113D1103D9,
	AWebRtcNetwork__cctor_m827FDBD8A193417AED38255FA7E000E04A0BC3FD,
	AMediaNetwork__ctor_m6C1994F70AD9036F67811F87223F8B899C1A1E8D,
	NULL,
	AMediaNetwork_Configure_mE8EBD7B77F6BA75EB1D12511A5F8A16C979F0B6B,
	AMediaNetwork_OnLocalStreamAdded_m8C861AAA89CE1DF061F662888D6A35E6CC5ABFCE,
	AMediaNetwork_AddLocalStreamTo_m222A8CA7D8AD34F486F429F1329CC6C81E7F29E0,
	AMediaNetwork_OnLocalStreamRemoval_mABB3821E6219F7EDFE8AE459576C545F5CC472DD,
	AMediaNetwork_GetConfigurationState_mE1F320786F972D214F950EFCD79FE9F31E4DE551,
	AMediaNetwork_GetConfigurationError_m2817623C30AA44B47EF3A6A4D7F3662AEB0B126E,
	AMediaNetwork_ResetConfiguration_m55C59B4E6EDBCE4101F8A91F701AFB14C027AEB1,
	AMediaNetwork_CreatePeer_m481AF4E4DDB4CC6607D949EDD650EA93D2A1A203,
	NULL,
	AMediaNetwork_TryGetFrame_m27B10E65DA0196F6F2AD405CB4CBEEDC6951F68D,
	AMediaNetwork_SetVolume_m96867A59F4AF6ABDB5B948D419D214EB3A7BA4C7,
	AMediaNetwork_Dispose_mBF19A250E97D8326F4AF6A98D9932E3B4E956935,
	AMediaNetwork_DisposeLocalStream_m936602A563B9F378296BE8650C358C02DA5116CF,
	AMediaNetwork_HasAudioTrack_m53CF7A28AAB8DBD29BC1E8F1FD91BE5AED532CEF,
	AMediaNetwork_HasVideoTrack_mA6E23276174323AE7F99F6DA38997DDB22F54F6B,
	NULL,
	NULL,
	AWebRtcPeer_NextLocalId_m98529AD42CA2DF83F27757642C1E5609E5E0586F,
	AWebRtcPeer_get_LocalId_m817A245E8480A559EEEA0BC0FCB23637186E7144,
	AWebRtcPeer_get_ConnectionId_m039CD779581CC1C820E890F9BB51CEA20F62152B,
	AWebRtcPeer_get_SignalingInfo_mEF9C8C2F2A6D90F5240927464E319DA45358FF7E,
	AWebRtcPeer_set_SignalingInfo_mDA4F41F2B58D6376CADDE45A3EC3E56AAE10E840,
	AWebRtcPeer_get_ConnectionState_mBDCCEF49726A33B993166F48BE7736F6F4BD64EB,
	AWebRtcPeer_get_SignalingState_mC4C2D07B8D25D3FBAEAF6E60E471ABA2A4007742,
	AWebRtcPeer_get_WasRenegotiationRequested_m7EF7DF06C7CB1416030910DC52BBAA6FB10D0BE3,
	AWebRtcPeer_NegotiateSignaling_m5116502738174A4AAC75500306C2ADE33E9D94D5,
	AWebRtcPeer_EnqueueOutgoingSignaling_mA98BE650140EE5E92AD830D29626E653CA3C0A30,
	NULL,
	AWebRtcPeer_UpdateConnectionState_m70E50D4DF2B8D289FA513FC43C015C1328A35C85,
	AWebRtcPeer_UpdateSignalingState_mF4E3704801AB38231B5E6BAEF6BF54F3C364BBD4,
	AWebRtcPeer_IsNumber_m42F4292193A4EFAE5D23E9B5FA3BAD1F057BD91D,
	AWebRtcPeer_HandleSignalingNegotiation_m051D2B3AE63C63CE23B1344D828C34A542D50E55,
	AWebRtcPeer__ctor_m3770632D7814AB297888BD614FD81ACF41F8E40A,
	AWebRtcPeer_SetupPeer_mEEB03690D4EA6622B3E03542E52589187F9FE17F,
	NULL,
	AWebRtcPeer_AddSignalingMessage_mC94B503708DAC762327774881900062C3FD3EAEE,
	NULL,
	AWebRtcPeer_DequeueSignalingMessage_m51BD0A84AEAD300EAC7A67784E6EA7C338B87A84,
	AWebRtcPeer_DequeueEvent_mC6A7476426FFFA92147DFDA89FC5DCD742B8338D,
	NULL,
	AWebRtcPeer_EnqueueOutgoingEvent_m33CBB9BA423CBD569516BFB5D6D69323B033EABD,
	AWebRtcPeer_StartSignaling_m622397FB1F92A6D9B36FCDA0E821CAC5C7C83DB8,
	NULL,
	AWebRtcPeer_L_m4743CA7D2A506070046EC36370B22507C0CDAC3F,
	AWebRtcPeer_LW_m2FB5E27C57AF5AFBDAC6B91ABB7191CAC38A6F11,
	AWebRtcPeer_LE_m1D17CB5217FEC61E746C8E564A4E1027E2FDCCA5,
	AWebRtcPeer_Dispose_mDF40D658BCDCB168C30C4C89BFF93FC710AF5AF2,
	AWebRtcPeer__cctor_mF95C87A4CEBCA312CDDFB2BB64EC8D04C182F582,
	WebsocketCloseStatus__cctor_m8E2BB6F9E1FC5902E2C5F089AE980683EBE6D602,
	AWebsocketFactory_SetDefault_m34C84E84A4811100B5EFE240D9353760A01FC08C,
	AWebsocketFactory_get_DefaultFactory_m2496EB27AC9142431F689AE83A3CFEF37E9B4C9A,
	AWebsocketFactory_CreateDefault_m9454E4F1303E29B16E85C5C640E8D1EE98956BD0,
	NULL,
	AWebsocketFactory_Dispose_m90CA34DC58585E563696597BDAC433C750C45E36,
	AWebsocketFactory_Dispose_m860324B078C9CE7FC763406B95DB3A281DCFF0FD,
	AWebsocketFactory__ctor_m1B78EEEC033423135B0E60C57EAD6F97BB9925A3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OnErrorCallback__ctor_m96D63379478B9BC8A1B08503CE403B5379C696EE,
	OnErrorCallback_Invoke_mDD90ABE8218D6B575A4FF374DAE29966B6138F63,
	OnErrorCallback_BeginInvoke_m3CC30ABDD6493927E96C1BA93F3B76A0441C0538,
	OnErrorCallback_EndInvoke_m6A84C7265E7C59F80E289DA2610201D3F882D797,
	OnMessageCallback__ctor_m24FF3E6F3458C8D812BE5087FC8077749CE68D52,
	OnMessageCallback_Invoke_m44A0B96055F87CA6D81DFC8AFA2FEC986007F54B,
	OnMessageCallback_BeginInvoke_mCF0F75A6FB4E76FC677A2DB7AE1F2E4A9F06482B,
	OnMessageCallback_EndInvoke_m20A8A8FF3067F5C4EB1E35EB7C2740A9B57451EB,
	OnTextMessageCallback__ctor_m2D4BAA74D9714BEB1CCF5CE9623D8C768DA25ED1,
	OnTextMessageCallback_Invoke_m609908CD182EC5746E645B8B41F0471BC0F28943,
	OnTextMessageCallback_BeginInvoke_m804B1FA73FF831582FBE9E2260505DC1F7D4B5DB,
	OnTextMessageCallback_EndInvoke_m1AB629B9DE76697651FBEE6F3BA8C0EE7BA97E88,
	OnOpenCallback__ctor_mD2350781A7FA5A5616222574DD01B0651018D546,
	OnOpenCallback_Invoke_mDEFD94ECDF4CCA84DF0CC9CEEF433593C12F3101,
	OnOpenCallback_BeginInvoke_m7DDFD0D1E2379672B3A79F1CB10658E2B7A671B8,
	OnOpenCallback_EndInvoke_m84CCAD373CFB796B321C03E0256C42B1211F30A4,
	OnCloseCallback__ctor_mE017A8AA9D1B28111E0E29163D45C8FF68F20512,
	OnCloseCallback_Invoke_m817040E0E3742F0FEE2D5BC045E49AD3CCDBF40A,
	OnCloseCallback_BeginInvoke_m2327A5ABFA5D7037B6B3F8D6BE3973F34BB08C38,
	OnCloseCallback_EndInvoke_mB1FFEB90AC86317D8E1281CCCA150C411BAF72A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SignalingConfig__ctor_m87328F1C95932D3959DD20D917DAEE86705F2475,
	SignalingConfig_get_Network_mFAF7513CB47560EEDCE0A3AC3604BDE8252A08C4,
	SignalingConfig_get_KeepSignalingAlive_mE9838F905F507514430AF989E54D43AD9DBE70E0,
	SignalingConfig_set_KeepSignalingAlive_m08ED64BE301FAF17BE50EC8FF17577F77EF39D73,
	SignalingConfig_get_SignalingTimeout_mBCC195A942B99DB69449B7490D99E9B6F0226338,
	SignalingConfig_Lock_mEDAD13BB9F475FA752B8C585E1D389B9E8044E7E,
	SignalingInfo_get_IsSignalingConnected_mFAB6881A7305554550BB10C0FD5D6FCCB1C68A7F,
	SignalingInfo_get_IsIncoming_mFB0F9E83C41C2CE0E9732FF19E2ECB3B736E656C,
	SignalingInfo_get_SignalingDuration_m393189F9D33AEE593682FB1E5684F6B20057B06D,
	SignalingInfo__ctor_m7B570D22506DD398F7E1D3B304FB017828A14B7C,
	SignalingInfo_SignalingDisconnected_mC2F0E38B469266203D686D7CE7AFF1D910EB6EA9,
	WebsocketNetwork__ctor_m6FACE7667DB029AFBA2C430D12D96973FDB81EFC,
	WebsocketNetwork_WebsocketConnect_m35C0D308F5DD3F7485D98A80A5D49F88CF8516AB,
	WebsocketNetwork_WebsocketCleanup_m63A51D0FFCF89AF803C7EA7B6B2268E2621DBB69,
	WebsocketNetwork_EnsureServerConnection_mA3553F7012E9305795E57BB6A9EF9EACFFAEBEA9,
	WebsocketNetwork_UpdateHeartbeat_m89DA3DB5041791F3CE32C366FA8F9C5743D79F32,
	WebsocketNetwork_TriggerHeartbeatTimeout_m037CF4766959D659152C0A0F4F3040DE58031493,
	WebsocketNetwork_CheckSleep_m47582BA84B60F17610051E349B56397F541F06B9,
	WebsocketNetwork_OnWebsocketOnOpen_mF9B225C134DCE6E4326B8EA3F1E439E9307927CC,
	WebsocketNetwork_OnWebsocketOnClose_m14A54E85C1BAC89BD7DCCEA28A814FEB45FBC3EC,
	WebsocketNetwork_OnWebsocketOnError_m30A7B8A575648EAD27626DC3604B81E957FC23C2,
	WebsocketNetwork_OnWebsocketOnMessage_m1525A69C3122B7AA418658529AA6DAE065B80FB8,
	WebsocketNetwork_Cleanup_mE64AEFE47D2E9214D7BB87AA431275556EECEEE4,
	WebsocketNetwork_EnqueueOutgoing_m4F90E5C91A3B516B39C4C7BC81E1F20BDD3E417F,
	WebsocketNetwork_EnqueueIncoming_mB890BB2416C92287012429464E7830D38FAC1653,
	WebsocketNetwork_TryRemoveConnecting_m853A0CFD2E2CE0E1113B0F8CF87B83F2FEC2CA39,
	WebsocketNetwork_ParseMessage_m45241C9840C7BE90485EE230AE246C43357F350C,
	WebsocketNetwork_HandleIncomingEvent_mF5366AFF6AEB374EA4E0DEA68CF88A895349B3A8,
	WebsocketNetwork_HandleOutgoingEvents_mBBB4B350008161643350B8C5715073ABACA74820,
	WebsocketNetwork_SendHeartbeat_m74A24E0CA289806CAE9CEEF076FA593AAA319C18,
	WebsocketNetwork_SendVersion_m0ED5F8B010CFC0729B164F520CDD3FD0F8782D2C,
	WebsocketNetwork_SendNetworkEvent_m4921195C67476F0E89D1FDD8AAC3714DD69038D6,
	WebsocketNetwork_InternalSend_m2E6746AF02E50DFC0A3B80E8B9D150F9903D040F,
	WebsocketNetwork_NextConnectionId_mA1A3AF06FAF002A0BB6C998F7B7DC09C39B230B6,
	WebsocketNetwork_GetRandomKey_m43464673B138B1F6F761267B40C78B07A8B31B0A,
	WebsocketNetwork_Dequeue_m09AEC5A8D2617EBD7FA847496F69B6AFC557E5EF,
	WebsocketNetwork_Update_m22D03EC7378A552278309E807A3395D462A57B14,
	WebsocketNetwork_Flush_mC49F193DCB4AF43EC302364B551078E579A5C1AC,
	WebsocketNetwork_SendData_mF8DEB2357A16D6B3BD146D0AA7E6CB936B2CFED3,
	WebsocketNetwork_Disconnect_m040AEA21666255B9A62AA00DAE32CEBAD8CF4E4D,
	WebsocketNetwork_Shutdown_mBC3F8A10639E8A18077F7355CF4DBAD5EF176570,
	WebsocketNetwork_StartServer_mFAFF4B9B8D71AA860336C4B7E37473DB7DF27B8F,
	WebsocketNetwork_StopServer_m24DA49D309F697F530A810BEB9373E7F91F43B66,
	WebsocketNetwork_Connect_m478AA2496960CAE43BA2846F8C50162FA81FD94D,
	WebsocketNetwork_Dispose_mE88A90E267B6CB7B3729EA231DD685C73E5E83C9,
	WebsocketNetwork_Dispose_m90C7458C5A067D5B8AFADEE090195E39F31F413B,
	WebsocketNetwork__cctor_mA443EC062CDD67EB72B3EF18DF062269189C75AC,
	Configuration_get_Heartbeat_m3C5E972C16BFBA0B4CD0D6998FE428F403006622,
	Configuration_Lock_mF117DD2D4E37602A9E6B7D1FC3B995C878D0EB9A,
	Configuration__ctor_m101F4E032635050719AC9290598387FD4E033759,
	DirectMemoryFrame_get_Buffer_mDCD2214669EC4787943F54752745D6E6E21169A7,
	DirectMemoryFrame_get_Format_m7FB036D3AB885152D6B6FD15EBEDD0440CEA119F,
	DirectMemoryFrame_get_Height_m1DC605521D4F38A07FD13EF9C2E0C1B2863CA8AB,
	DirectMemoryFrame_get_Width_m08E76B7E7656BEC35A3BF8A3EABAE0F2AEFBF123,
	DirectMemoryFrame_get_Rotation_m20CA63BD694C6DF30AA3EF9C33A3486A845866FF,
	DirectMemoryFrame__ctor_mCD6C7EA1F1B0E266576F3B5A703F5AC1E141F86D,
	DirectMemoryFrame_GetIntPtr_mC84E802FAE1D9EEF822845987B61DDB9BA65E803,
	DirectMemoryFrame_GetSize_mB598C1CAC9F1088AC34EE4E27AE8AEFECB4AEAE3,
	DirectMemoryFrame_Dispose_m8F24773E5627665DCF18DA974E2D03F213758466,
	InternalDataPeer__ctor_mDE20C07141E9D65ABD2014E4A36271F834A5FA1D,
	InternalDataPeer_ForwardSetupPeer_mE379F64AD9D83140F21D2C4D8F3C20753CAED58B,
	InternalDataPeer_ForwardSignalingToPeer_m18946E088BE76A35C66D8BE21CB99908858FF2AD,
	InternalDataPeer_ForwardStartSignaling_m123DC5F9842F6B20EE2D41E22F1CADF3DF3D31ED,
	InternalDataPeer_ForwardOnDispose_mFABCD84D0ADA265AE28B710B882B8EDCB10D347A,
	InternalDataPeer_Update_m3FAECE252B6B156F15A74EA09B78002380AFEFD9,
	InternalDataPeer_ToMessageDataBuffer_mB26AD84834BF57F5FD83ED913C3450AD4C353DE2,
	InternalDataPeer_SendData_m8A1F696EDF98DCBB72864FACFC35D28C13F71402,
	InternalDataPeer__cctor_m6E35A0983AAFE505468BB8819E986C87F47EFE5D,
	InternalMediaPeer_get_RemoteStream_m0DD6A960F7A7F3C48B2457A3EB9FA31595FFA613,
	InternalMediaPeer__ctor_m2D5468384DFE91AE6DBFAE119D550C856B17A9A6,
	InternalMediaPeer_AddLocalStream_m1A9DA2AA2F7EC94307870618E894648D47CF70A4,
	InternalMediaPeer_RemoveLocalStream_m1FA730268ACAB98894BF86A8DE422C520435D044,
	InternalMediaPeer_FrameToi420Buffer_m2434136123E58B7514D866EC28FA570FC83E8158,
	InternalMediaPeer_FrameToBuffer_mAB977D5CDD79492223A0C9B7E8BF67C81FF4AFD8,
	InternalMediaStream_GetInternalStream_m77A38AF3338C96E9A7AA003DBF8E856EE249F4E8,
	InternalMediaStream__ctor_m40672E050F624BA1E5A20EF23A5F6C4A162C1897,
	InternalMediaStream__ctor_m8DC4E3CC7E01442F99110D51879D93799171905D,
	InternalMediaStream_Setup_m73AE2F4CCBD13B0A8D1F55946E766C84EA615B03,
	InternalMediaStream_Dispose_mE2FD4EA58907064B6439AF5486FB8A8567539CD5,
	InternalMediaStream_IsMute_m7DF42B4096B4A4365098DDA49B3BD8E48C0D476F,
	InternalMediaStream_SetMute_mFC98A6852DA06BF90A38A60D957EB5C8E6549672,
	InternalMediaStream_SetVolume_mA291EF5D5EF938F92BBEBC9A2D989F28ACF5C52D,
	InternalMediaStream_TryGetFrame_m068DA5667A59F8F876B541EBFD7EEA1C0E2E02CB,
	InternalMediaStream_HasAudioTrack_m305F2F4577AFBB0CD7DF45B545C16C2F72426DF4,
	InternalMediaStream_HasVideoTrack_m1D90BA32DC72457274ECDF3E2C9DD836CB92D9B4,
	NativeMediaConfig_get_AudioOptions_m9ADF3E35B3729F865568DC50468BC8DAF3F36EA7,
	NativeMediaConfig__ctor_mB9F397AEC11994F8C4124534CB755CEF50AC8EFB,
	NativeMediaConfig__ctor_m8DDBBB51904ECE5AF9D4FA5448FADB7D4E8D213B,
	NativeMediaConfig_DeepClone_m9669A2569E0F39BCFF39C68BB839DD5CD08D0F06,
	NativeMediaConfig_ToString_mAA346BD0B0E0C1813B5A1FD718549E62AFBB45FE,
	NativeAudioOptions_get_echo_cancellation_mABDC0DD719C6A715B06C99B3F7E23BD870E525EB,
	NativeAudioOptions_set_echo_cancellation_m5FC8D6E5F3C0FF69ECD2380941E6098D35974FAB,
	NativeAudioOptions_get_extended_filter_aec_m169157ED12BC59E33700869867B1E4BD07EC0562,
	NativeAudioOptions_set_extended_filter_aec_m3C06B76A99D2AA0BDF8C5CBF1088122844209769,
	NativeAudioOptions_get_delay_agnostic_aec_m86E8B33D54896B83C6A3587FD2C83A6035B985D6,
	NativeAudioOptions_set_delay_agnostic_aec_m827ED9C9D137BD6C7320B5BD1FBE57AD3CDBE803,
	NativeAudioOptions_get_noise_suppression_m97F9CD2B2179CDE80AB1DFA2B9F3F944663BBE97,
	NativeAudioOptions_set_noise_suppression_mF1F73DC679130BA60AD7A88047F69865BC507C0D,
	NativeAudioOptions_get_auto_gain_control_mB0C781379B93B8F67DB3E74D477F87948EF3010E,
	NativeAudioOptions_set_auto_gain_control_m3661652A13E07AC5BEE7D100ACB712194E6DF9BA,
	NativeAudioOptions__ctor_m45178698EF84BFD27A71AED113F7A9EC8DF9A4EA,
	NativeAudioOptions__ctor_m3150DE6627933BA8251D5C7E0C821A5EBDA74B0E,
	NativeAudioOptions_DeepClone_m2DF6FD92F4E7158DF9CD9D46E2503F349016181F,
	NativeAudioOptions_ToString_mC92E8AD34EF2A04BCBF8F5AC0E8AC5665974872E,
	NativeMediaNetwork__ctor_m7C57266E19BF5D59EB974E232A2B40AEB88F4087,
	NativeMediaNetwork_CreateLocalStream_m1B83149312D5C2434FFBA43D46373987483AF1E8,
	NativeMediaNetwork_CreateMediaPeer_m172EAF10500F82DF806438E911B7C8B5EF0A8E6A,
	NativeMediaNetwork_Dispose_m646C58798AC5AAB10FB14CB69568461E645E1745,
	NativeMediaNetwork_IsMute_mD056C7A90348DAC4C0E30460AA2BC60E8D68AB71,
	NativeMediaNetwork_SetMute_m26FBE88D7678C2D2481C5632D53B295CD40817A4,
	NativeVideoInput__ctor_m50FD569D84A785ADA4C2454F65A041D80962EF16,
	NativeVideoInput_AddDevice_m050949A8BFC7CED677CC337733ACFC34CAA4AAFB,
	NativeVideoInput_RemoveDevice_m1A0B99607F0432B53D242F5F3558720038FD7F61,
	NativeVideoInput_UpdateFrame_mC8C2827F1975D3D90A5B844958FD6862683B46F4,
	NativeVideoInput_Dispose_m66B2A4851C640D60B5795E2FB3F310301556F9E6,
	NativeWebRtcCall__ctor_m9E4ACDA1FD5BC24737701EC98EC2C09066E32C7F,
	NativeWebRtcCall_CreateNetwork_m9F2C65ED13F2F3272369419D8D9CC0D200EF4A6B,
	NativeWebRtcCall_Dispose_mBA63D61AC46D1268883C802C47E1AA3A670CFC66,
	NativeAwrtcFactory_get_IsInitialized_m3CC0662D2B6B3917130372632DE195EF8A3FE52D,
	NativeAwrtcFactory_get_NativeFactory_m32621B5A525A0807AB4A8F6734B0525C30626B68,
	NativeAwrtcFactory_add_LastCallDisposed_m62016E58BCC04727CDE6725BC6FB300618202DD0,
	NativeAwrtcFactory_remove_LastCallDisposed_m9848398596ABA19A59E1B740BA6C9CE5E6B1E399,
	NativeAwrtcFactory_get_VideoInput_mB98E0A12070ECBB6D176BC3AB1DBEA997708E7CE,
	NativeAwrtcFactory__ctor_m3A148969A7720A61C81C88E2037889F5102D39F9,
	NativeAwrtcFactory_Initialize_m1D7913F69D65EB9888422598DC9FAB1D4E6D5238,
	NativeAwrtcFactory_TryStaticInit_mF9D597FE417E2DD0CC2F3489892642DAA05499B6,
	NativeAwrtcFactory_SetNativeLogLevel_m5D1C6FC698A7DD88DBAD82A55E3DF21153CB0E58,
	NativeAwrtcFactory_SetNativeLogLevelInternal_mC54650010881356461711CAF610BB2163C485E47,
	NativeAwrtcFactory_GetVideoDevices_m9A0DB0741E58920FF06FB2A08BFB4B698276A514,
	NativeAwrtcFactory_CreateCall_m1A35DF0EEE3E1AAACC934A7F091DD0164CF0656F,
	NativeAwrtcFactory_CreateMediaNetwork_mFA938D8AAAFF1ADE41D72161ADB9ABE600989BE6,
	NativeAwrtcFactory_CreateBasicNetwork_m2CA1DAB88E39AD66B784296B4524797BD14CDE11,
	NativeAwrtcFactory_CreateBasicNetwork_m5E2BFE150199AFBA5ED1DD935B714EF7FA950095,
	NativeAwrtcFactory_CreateMediaNetwork_mA83A079C6EFE3B88EBC496E3D0B3ABAB02A5B551,
	NativeAwrtcFactory_OnCallDisposed_m56FBCA699DFF09F0DB2D83153C5B5684329C9069,
	NativeAwrtcFactory_OnNetworkDisposed_m5B7A8326C718C9A6BA60A0E9DC7C68820EB3317F,
	NativeAwrtcFactory_OnNetworkDestroyed_m75D9849C6888F9412E3FAF0BCC63C2E0364C10E5,
	NativeAwrtcFactory_Dispose_m8B378F96171D4DEE829AFE90A94E030FF947B629,
	NativeAwrtcFactory_Dispose_m6E748D0CD68FBEE32EF9BA4BD492E7ECBD79470B,
	NativeAwrtcFactory_CanSelectVideoDevice_m6C872A66BE907452B857D9BC9E6E2DF75F031E25,
	NativeAwrtcFactory_GetVersion_m1EB3E0E4014576D50BF8B07F0901B36BC771F69E,
	NativeAwrtcFactory_GetWrapperVersion_m22D166BA0EC7A5648DDF61B00C5B48ABCAC7B359,
	NativeAwrtcFactory_GetWebRtcVersion_mB11C0A58D32C5046D08D3769A489D43E0C86AA32,
	NativeAwrtcFactory__cctor_m3F6BAC234C84C0EE931CBFBB4C5CAF12143F1C85,
	NativeWebRtcCallFactory__ctor_m6D7593918FC6BE2338B812992BFEF9B11DED8108,
	NativeWebRtcNetwork__ctor_m20461B73CB61E429613F6C0C2C7D8649FE11C58E,
	NativeWebRtcNetwork_TestValidity_mC687EC03A742353ED9884E7171734BBF6D6B1659,
	NativeWebRtcNetwork_CreatePeer_mA17AD1DE62D0493E98C00E92888388A27785122A,
	NativeWebRtcNetwork_Dispose_m097E8FCCBB86E6E032365A0F53F62FB568ADD25B,
	WrapperConverter_ConfigToConstraints_m11D26F400103503BB9E1F330E3FF838ACE9E0706,
	WrapperConverter_ConfigToAudioOptions_m05C99ECE35962021169D7D53209A8D881BE84563,
	CloseEventArgs__ctor_m0DB3174A34E5C1FAB57CD041EC377BC173D5D5BA,
	CloseEventArgs__ctor_m04775AFEDB2C2DD19B433F857DF3A8727F348C96,
	CloseEventArgs__ctor_m68FBB73EFFDFF5B823B0C1DE33C9E8211B661F27,
	CloseEventArgs__ctor_m9F3B718EFD4BA9E807C252FC26F7FAB43F5064A3,
	CloseEventArgs__ctor_m3057BBFC52F3D3CDF81D7D3CEE37ED995B01BCB8,
	CloseEventArgs__ctor_m7A1B11454F002664E72E63C34AEF55C502DF1C10,
	CloseEventArgs_get_PayloadData_m3D7BD5895F230AB46FBBCB907BC8638C2740EC4B,
	CloseEventArgs_get_Code_mAE13B1503238D6F5A6D5ED02025FB23A7F57FC67,
	CloseEventArgs_get_Reason_m8472BBF6835EB5EA28AD63DE02600D91627717E0,
	CloseEventArgs_set_WasClean_m3703AF4482EB348810472EC86643EA854D27786F,
	ErrorEventArgs__ctor_m45623A0336FF95D9947A20E98DE2F3DC06BB7291,
	ErrorEventArgs_get_Message_m8205FBF5DAA3F6607801B0C2DD52CC7064CF5157,
	Ext_compress_m46A8A1F7903A87B6AC51715677CB7C31E78399B0,
	Ext_decompress_m836A06295F53096E2D3BBF9299ED44FBB6504D4B,
	Ext_decompress_mEEF738F492D6CADD083A625E357693E7448A3798,
	Ext_decompressToArray_m20385B926621FF6B2E9012CFAB0A4401FDF47A3F,
	Ext_Append_mE4FC83E9CF6A32840FB51731F6A9A0BE1E4184D2,
	Ext_CheckIfAvailable_m85CEAAA4821DD3F6E014948825211CDD6462B9EF,
	Ext_CheckIfValidProtocols_mD1264E875FE0B80B571816D1FBB871F07383D2F1,
	Ext_Compress_m40FF2F28421D52C990C352EEDF009748FF49A8A9,
	NULL,
	Ext_ContainsTwice_mB352A815690C291CD8AB2DBB04F10C10D3DA63D8,
	Ext_CopyTo_mFF5CA5974DD34ABDEC680D30DF6ECA3859A508A8,
	Ext_Decompress_m945D982F69401509378A160912D42BD335B1B7CF,
	Ext_DecompressToArray_m4998F5C564C1C070CFE0C572103DDC4823CA1944,
	Ext_EqualsWith_mBDB3646F21C970F9903B605A70D70A6A8C6B56C1,
	Ext_GetAbsolutePath_mFD5D6CE2A94E203F2A2AAED77302D3A9C52B516F,
	Ext_GetMessage_mC66E14F8CF9802520C0CDD8CC6AA9BCE62D882A1,
	Ext_GetValue_mF515B67DF6933DBEF752F6F740FCB7E890DAD0CA,
	Ext_GetValue_m68F028EB3B4DC479A7A25A1BAE259984B96CFD60,
	Ext_InternalToByteArray_m4C6202B989B87F75B2BA383BE92C80BF1650E7E4,
	Ext_InternalToByteArray_mCCC187A30EFFD446FFC951F28DC338C9F6BDA9B1,
	Ext_IsCompressionExtension_m050E774AB8688AC225775DA266FAE3D119DE6232,
	Ext_IsControl_mE24689E063FCB9AE6C90DDF96BC8E7C790671235,
	Ext_IsData_m862A3DB94C8906F21F781F48D6C14D18B70C2F21,
	Ext_IsData_m603EEE06067092536DCAE1A10B3F9AF1B2AD6DA3,
	Ext_IsReserved_m9A4709BF21BE7463B16413824A2AFCAC100B6BC9,
	Ext_IsReserved_mC52557FE31C4F20C39638821359798A2BF121210,
	Ext_IsSupported_m18366BD24CDA79114821F940B82297AFAF3A0EF3,
	Ext_IsText_m44B86C92EBD9E8A4F2D9D0D4039D087FA4CFD75A,
	Ext_IsToken_m8F84DDE036F7FC9F2145B659D6D736B31F420A37,
	Ext_ReadBytes_m0091FF142DA210BE0F4F685A93176C2C7E3FB61D,
	Ext_ReadBytes_mB24E009E51CD5465F56B5F76C1C9A2BE7251505F,
	Ext_ReadBytesAsync_m05A3D2063082E659C3A22E3AC7155C51F86A9B92,
	Ext_ReadBytesAsync_m13270548F41EA3688F7DF3E0918CB39EA66EEF0D,
	NULL,
	Ext_SplitHeaderValue_m54FF7CA6A00B830775C66EF211B157A44C484102,
	Ext_ToByteArray_m6F561B49C713C9019FB801C373500F097A74D378,
	Ext_ToExtensionString_m5FCFE0375B24CB2F0B97101646BC08EB97DD3F43,
	Ext_ToUInt16_mE2024718916264AAF08D3FEBAAD90D24240F8318,
	Ext_ToUInt64_mA5CA03B4A866E7D8278B13EE7BBF67ACD0FB6DBE,
	Ext_TryCreateWebSocketUri_m7CC4962F338B03DC2B4749CD1D9DE000F8962A6A,
	Ext_Unquote_m56398F497467975D7969618CC2EA45BEAA65083C,
	Ext_UTF8Decode_m2E51728157D0DEE3C6567C0403DD9D7E4E111C48,
	Ext_UTF8Encode_m05EC00CA0336F30755D31852734CDC3896710CB5,
	Ext_WriteBytes_mF2AD2DF42E26B1F9EFC8E1FB8AD7A78ABB6A261D,
	Ext_Contains_m46573FC11089072D7754159C2DD6D7375A8E725B,
	Ext_Contains_mAFFB593126114852F00AA4748CE912D3CC0F9ECA,
	Ext_Contains_m5C519C754B6BEF365CE8114A6EA1B6890B9FE936,
	Ext_Emit_m6FBC2120132DF2BA07FF7636196184BAE297F529,
	NULL,
	Ext_GetCookies_m57EF613E2A739B37A4F5CAD7FB4F8D79FAE6464F,
	Ext_IsEnclosedIn_m6A57F317469525F8C7EC7DA335350C988B1CB0A4,
	Ext_IsHostOrder_mBF88043D52F49AD9C3D4F19565C54A420A6923B6,
	Ext_IsNullOrEmpty_mC6C901754611BC6E3E3F57D91D2C9A26AA93D087,
	Ext_IsPredefinedScheme_mB040228C6B5C3D0FB6DB20424542266379842FF3,
	Ext_MaybeUri_mFDCDD897107619A18E640A0465925A89211D3F17,
	NULL,
	NULL,
	Ext_Times_mCCB50FF3F626576B11E3258B11D9537D81EE8172,
	Ext_ToHostOrder_m00C1849C29BAA1BC5D025CD661A471080605A5AF,
	NULL,
	Ext_ToUri_mEC8E6899F4EC8D373BFB5FCBF57D620233D6F6C4,
	Ext_UrlDecode_mB0A77A8BE3C05F6367151535A17AFA0B6365B00F,
	Ext_U3CCheckIfValidProtocolsU3Eb__0_m63FE12A5576F745DEE14F9791F4CC802F7A3AD49,
	Ext__cctor_m60DDBA76C9084B3925FA22BB03BA229D75B137FD,
	U3CU3Ec__DisplayClass3__ctor_mA8838CAA571F52B2BFCC05E73ED615ACC1652A84,
	U3CU3Ec__DisplayClass3_U3CContainsTwiceU3Eb__2_m2CED0B687B4D76259A223852817DC12A9F7ECA64,
	U3CU3Ec__DisplayClass9__ctor_m378B8B45EF36B6B5CC196A785F8A69BB2E892419,
	U3CU3Ec__DisplayClass9_U3CReadBytesAsyncU3Eb__8_m78C0576F9AB52530C475C4F399758629F78BC4E2,
	U3CU3Ec__DisplayClassd__ctor_m903CC42078D26D05A4DBBE48DA96180E37BA4FC4,
	U3CU3Ec__DisplayClassd_U3CReadBytesAsyncU3Eb__b_m4CD9EE991BE16BC6975DEE02A318DEFD13001EB3,
	U3CU3Ec__DisplayClassf__ctor_m86005AA4FA5832144CA346111CB35465A2C932DC,
	U3CU3Ec__DisplayClassf_U3CReadBytesAsyncU3Eb__c_m9DC3FF236490F2D54EAC900B4E0255E5B4AF224C,
	U3CSplitHeaderValueU3Ed__11_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m2A7023B8B86B4751828EA0964AC9C5EE7E90CE5E,
	U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerable_GetEnumerator_m1C10E311685F332ED6124A564A8AB5CF39BB3E67,
	U3CSplitHeaderValueU3Ed__11_MoveNext_m06BC9C7CA0538FFA4C7212E2F996731027104D10,
	U3CSplitHeaderValueU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m0E2862496F8DE7FC010F07272E7515368D38D3E6,
	U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerator_Reset_m276A7AE95E62CFB4336FB1EBFEB172128EE6FFF2,
	U3CSplitHeaderValueU3Ed__11_System_IDisposable_Dispose_m923011C5F524B04DACEED4826FF8272A385198BB,
	U3CSplitHeaderValueU3Ed__11_System_Collections_IEnumerator_get_Current_m9AD24AE4E05AAC61AEE2BAEE83363A20BA9BC087,
	U3CSplitHeaderValueU3Ed__11__ctor_mD4BF7E8EB9AFC4B3871B2D077B640712C498AF9D,
	NULL,
	NULL,
	HttpBase__ctor_m4165B18971B005C70A3F1E444CD7C6B20A57F187,
	HttpBase_get_EntityBody_mD288C0CC22AD0A01F43CDE43D6CFDEF3FDE70C05,
	HttpBase_get_Headers_m6E65386C443B2DCFDD1108F92D0CCB4AFAB922B9,
	HttpBase_get_ProtocolVersion_m2BC68E395703C75AF4CD502C63174650DE0268F5,
	HttpBase_readEntityBody_m5615E327709AC55ECFD0C03B975E23D060C0E5B0,
	HttpBase_readHeaders_mA59BC3BCD19A58E15A5DAFF4AB47D4E9E09C832F,
	NULL,
	HttpBase_ToByteArray_mB48F9B867184BC5E7771B99322131E4A4AA6A129,
	U3CU3Ec__DisplayClass1__ctor_mCE8F2D261E9B5118B0B126BB376AA4F0003DFCD0,
	U3CU3Ec__DisplayClass1_U3CreadHeadersU3Eb__0_mE811BDBF8B39BF6E1B7B2A7B0CFCB08D79D02D1A,
	NULL,
	NULL,
	HttpRequest__ctor_mC2D7D84D488E9B4809FEF640699B1C9480658E89,
	HttpRequest__ctor_m306CD949512BA0499D9979EB9D6403D821A60756,
	HttpRequest_CreateConnectRequest_m9326376FD476238ECC3B2176D80A41D80D4396D0,
	HttpRequest_CreateWebSocketRequest_m5DF0326F894B38216EBC912351ECB5B2E703B2C2,
	HttpRequest_GetResponse_mEAFF1853E31E707A30470CBA633B6C8F375BC04D,
	HttpRequest_SetCookies_mC0F3E8E12D87DFE78C282452E316313E3FE82EF1,
	HttpRequest_ToString_m59938FF81CC4D55F09E74019666B10B878450F11,
	HttpResponse__ctor_m061D0C9B8F6B283044F9B072AFDF33F00825A56E,
	HttpResponse_get_Cookies_m3A1BBD66F2D88B61C3EFBF398ACF807E9501F1CD,
	HttpResponse_get_HasConnectionClose_mE7749F7EBE6120C4F850F97FB609C3A2A2F0FB89,
	HttpResponse_get_IsProxyAuthenticationRequired_m238B2E37AF27FE0B6B47973CA4799E35A3C0CC3D,
	HttpResponse_get_IsRedirect_mC0C821FA49EB90632282ADBD44AA439799E76A20,
	HttpResponse_get_IsUnauthorized_mA351DA558CD64DDB2B6410DC9F1B88190EE4C91E,
	HttpResponse_get_IsWebSocketResponse_m722D43F1AB433A282C0F73751EFD94FBACD1D55C,
	HttpResponse_get_StatusCode_mA99805825F473DA8A62F199D65F8D5FBE70CE608,
	HttpResponse_Parse_m8424EBF321DF634A4F182559CA58CEACD0F52722,
	HttpResponse_ToString_m05E9FBECEAF2C443B6289F1BCE8611FCE5A93946,
	LogData__ctor_m33D5ABF45114912EE19D3DCB0BD01F92E0C8EC6C,
	LogData_ToString_m944E3C1C5FD5480EA21121714F50C88FD6591B00,
	Logger__ctor_mC55DC74C3A52A81E6FB64C48D39011309708BBD3,
	Logger__ctor_mD3DB232D7C3C7DCD3C0769D79329E552E5574582,
	Logger_defaultOutput_m8893023F72ADD33533B906526075FAA69045600B,
	Logger_output_m8AAE8BA1F7C05B9C9F75D7437BBF0F64BFA644BC,
	Logger_writeToFile_mA1B20FBF3D7DD2EB1245817F4CA68071F431B746,
	Logger_Debug_m1087E5D8B286205A0A053A6CEC725DDF0DD3EE15,
	Logger_Error_mD1DCB04433A59DA42460239357FACC09CC262BB2,
	Logger_Fatal_m70C002827E95AF53E7C4D5F65B8375CB152843A2,
	Logger_Info_mD9DBED65DDCD1CA90FF38EDCFB9B73C8ECA40EC5,
	Logger_Trace_mCD517D0F8A833A9D05E4224437255867C0459A5E,
	Logger_Warn_m5B4B919007AE42F6C63CD61D1394F185104A37D8,
	MessageEventArgs__ctor_mE28253222CAB528040709CDB30AB7AE796588879,
	MessageEventArgs__ctor_m1D2F4D4BCCBEC696BB93E389B40CDBD2DA2FF064,
	MessageEventArgs_get_Data_mEDF292CC4AEACD06EF648AA77790FAE9019FD8AA,
	MessageEventArgs_get_IsBinary_m61BB7129529EFA9C784196E957F50617854D4B62,
	MessageEventArgs_get_RawData_mE764E56DAE51B38BA1C3CC3B54232D0795A52BD0,
	AuthenticationBase__ctor_mD956F45C397AEDDB077F78C45C5F571DAD103122,
	AuthenticationBase_get_Scheme_m9D31D42B498CAFD09718DDC76DE5D3722F56D70B,
	AuthenticationBase_CreateNonceValue_mDFC38ED8BE431697C42FB5C36B100507039ED32D,
	AuthenticationBase_ParseParameters_m253A9D2AEB932002A8AA134DF909CA6AEFBDDC45,
	NULL,
	NULL,
	AuthenticationBase_ToString_m6F7B02651C8E143DAA78A1285FE68517810D9FC1,
	AuthenticationChallenge__ctor_mFC188F896C0EC18E47893F2A96EC9E2CFA8D96C1,
	AuthenticationChallenge_Parse_m2C887252447D8DF95747B902877E371C5A2DF311,
	AuthenticationChallenge_ToBasicString_m4554A9FB78D449EF47ECC6BBB6690E0D1CB9D480,
	AuthenticationChallenge_ToDigestString_mA39087A182EE0BB5694A3F47A40191711961CB69,
	AuthenticationResponse__ctor_m84953201122084E2C427A5DF6995D928F94F8FC2,
	AuthenticationResponse__ctor_m5DDEF851D0D8588BA351347A94F1D5B795911FEC,
	AuthenticationResponse__ctor_m8EE647609498BED130417F608263222D25CD22C8,
	AuthenticationResponse_get_NonceCount_mE7E89CE703D7AC001B26C1C692DEF7638574376B,
	AuthenticationResponse_createA1_mE03E25AA3786EE079DC0E9B8F9106933BEEC8805,
	AuthenticationResponse_createA1_m7261CD5E17CB199B47CCE25E9F860A37F94D88F1,
	AuthenticationResponse_createA2_m909965B4C2E988B261D07AB3C7500DD52BF11F7B,
	AuthenticationResponse_createA2_m02F88F10A1A7F4617E47C138E9DA3CE4AFB57500,
	AuthenticationResponse_hash_mEC632AAE9AB26C003AA80B60CE9F17DBCA637B23,
	AuthenticationResponse_initAsDigest_m94FF3F2670DD777AEF409E659DB764B37545698A,
	AuthenticationResponse_CreateRequestDigest_m46D5C6725D1BF595E2F1A88BC29C0DF5B0C66C0F,
	AuthenticationResponse_ToBasicString_m8FCE8EDC5CE69DAA32D94B4C2BAC6A8F3DF2DC24,
	AuthenticationResponse_ToDigestString_mF4FF6BC7A6E7DA952646CD5BE6117BA506BD4C2A,
	AuthenticationResponse_U3CinitAsDigestU3Eb__0_m88296FC69B0C92744F553AFA168FDC67642CDC71,
	SslConfiguration__ctor_m677DBF069E824D78BC48074F029E814AD6C3BDFA,
	SslConfiguration_get_CertificateSelectionCallback_m2F742178D426B6520E7C776C0C9903680D2F37AD,
	SslConfiguration_get_CertificateValidationCallback_mE285A26C4FB09E1F5A294326097A4366727F14CF,
	SslConfiguration_set_CertificateValidationCallback_mC7FCFD8BA5B07A7CEEBF5B8B5ABA91D9F267D279,
	SslConfiguration_get_CheckCertificateRevocation_m97E42331DF2900ABCB0832033FFC6C64FA7408B5,
	SslConfiguration_get_EnabledSslProtocols_m34619955FA535C1BB6C75EE24C01DB3B02509551,
	SslConfiguration_set_EnabledSslProtocols_mEA8C3CC4DB64BEBC0586FDB2780F2830E335CD0D,
	SslConfiguration_U3Cget_CertificateSelectionCallbackU3Eb__0_m136BFD4DD092E475E5886A936340AC192CE8557E,
	SslConfiguration_U3Cget_CertificateValidationCallbackU3Eb__2_m1FCCFD7D4F48DD2EA48211D095A6CC3906FCCF92,
	ClientSslConfiguration__ctor_m2764B7C0CF7A76BB7D339688B634FA8D52816D66,
	ClientSslConfiguration__ctor_m0604BDC20F7A157A1F1C3535B6EABEDB003BDD95,
	ClientSslConfiguration_get_ClientCertificates_mDAA3039829365BDC9C022B7DAEC8AC97C9FCA9D2,
	ClientSslConfiguration_get_ClientCertificateSelectionCallback_m699F27E58C1F78DE84C6F05C35684428D3B42B2D,
	ClientSslConfiguration_get_ServerCertificateValidationCallback_m8F941D52F6C700B3F0FE20D85B8B11ACE5D9579F,
	ClientSslConfiguration_set_ServerCertificateValidationCallback_m4618F00BE2FB0F7A8583DB468BF3BE3AFB19BEE5,
	ClientSslConfiguration_get_TargetHost_mA73D80B44708971DB597B66D8457FB6D0243C7C5,
	Cookie__cctor_m999C0D224AFFC227294BE01059E673DE2DF81C8C,
	Cookie__ctor_m83234BC60730C52E958774CA8968F5E94D6BB077,
	Cookie__ctor_mDD62F8FB714F81FEF19F06D8BE5A449008F90CA5,
	Cookie_set_ExactDomain_mA3C12ACF0ECCBBE9F3497D7B77F2EB7427ADAC99,
	Cookie_set_Comment_mB64D12F86BBB68713984956840D99226B76DCC28,
	Cookie_set_CommentUri_mD38E976CC9FD1E70730330CCCCCA938CE11285E5,
	Cookie_set_Discard_mD745B6687FF10A6E7DDA84A7A6AA466E3286361B,
	Cookie_get_Domain_m631D71FC83AFDE4ABC8E51270B7A146FBEE0CDC2,
	Cookie_set_Domain_mCE016A08EAC78AD6AC4ADC81B594AF81522DC641,
	Cookie_get_Expired_m6D006652937D8C26E638AF6F15D0775E21444851,
	Cookie_get_Expires_m533B091712A7479701B77956FAEADAA706C3DB2C,
	Cookie_set_Expires_mB643D5B7D6A84D66E93A61819A351DE345211DDC,
	Cookie_set_HttpOnly_m6BE2A73DD6B04BB08FC8DEE0FE9B3AD53E627B70,
	Cookie_get_Name_mAF3643FB1D89CB20C8CBC51B0EC1CE7B29E6F189,
	Cookie_set_Name_mF85004448EF80E21D2F50E856B20A2B1C15251A7,
	Cookie_get_Path_m58C636E8D4BBF83E816A763009BF88A561F7C690,
	Cookie_set_Path_m40D898C50F52D4607FF3A07AB647091DA2C86121,
	Cookie_set_Port_mF0CA07A10F4FA4E00D10A2A3F1B1543501131420,
	Cookie_set_Secure_m7B0BEBCB1D3D2620E5CA6598970B5E29FC39539A,
	Cookie_get_Value_m1F214BEAE9D77056EFEBFCD0696DE9DE8C264B61,
	Cookie_set_Value_m6C446F6ED4EF9475C0F44387834C9C74B37FD64C,
	Cookie_get_Version_mD2A022565561F7DFAC1C43653677A71A60AF0A75,
	Cookie_set_Version_mC32E8B840E4915A4E55B74E3F6D5232A454E6C14,
	Cookie_canSetName_mF369DBD22F2FD4B1D0DA286F05D33088A1ACFBBD,
	Cookie_canSetValue_mF18745F89C5CB35B1BCDA824E73424BFAB721428,
	Cookie_hash_mD0397CE44AEBC0F4BA56FA1A24C6309D38E2D0A9,
	Cookie_tryCreatePorts_mB66AA04E4B11DB39D9A44E26F62E56651E6D7CE0,
	Cookie_ToRequestString_m883334792F4A37AE59026568F1C2A33BC59DD9CD,
	Cookie_Equals_m82C68C8C56B72C1287266CA30A9B860F6930978B,
	Cookie_GetHashCode_m04EFDAD814ED9360157C3AEA1DFE3D14600A1A9B,
	Cookie_ToString_mE726F447B6A8AFD4FEAEF201E4C5EC2DA8EACEC9,
	CookieCollection__ctor_mDE48C9757936FF7382AB37BDC7DCE7206C68D6F3,
	CookieCollection_get_Sorted_m1634FF7659A9F4F41EF55394DFB134342E38DA74,
	CookieCollection_get_Count_m2A92DAA0F6E072F550FDEB662A779B09F5608938,
	CookieCollection_get_SyncRoot_m1C96A338B47152CB3D682DE55973D8059E8EDE03,
	CookieCollection_compareCookieWithinSorted_m26119F0A3367EDBA858F30BE0E6C417F891B2D55,
	CookieCollection_parseRequest_mDA7A3CF32E4AD9E29BC539FD8FF4981AFBF37064,
	CookieCollection_parseResponse_m99A018C372BFF1C86CCE6C9F3F93A875C03F80C3,
	CookieCollection_searchCookie_m9CEEF0238B9287F47E684368FFC50D1C697740C7,
	CookieCollection_splitCookieHeaderValue_m23911D1DF40A2F8E071ACB0C0E573347F5AAAE71,
	CookieCollection_Parse_m9255ACB4EBD1B62FFC8C72D09290113D1C4611BA,
	CookieCollection_SetOrRemove_mCF9056741DAA5D3D63C31B4C38C757FEE6AE052A,
	CookieCollection_SetOrRemove_m131EF8F30D4CF4A94A20175107CAEADDEC4E8C16,
	CookieCollection_Add_m74BBC0F9D626AF25B32F29A9D6D7F16ABD5DEB69,
	CookieCollection_CopyTo_m7F94B5B05098C560EBEF14AD2CE30FC3C56E6597,
	CookieCollection_GetEnumerator_mADDBE728E05C32AEE57FEDEE324A1AD909A4847C,
	CookieException__ctor_mA14039B3EE1868B62197E88323CF514391BC8FF5,
	CookieException__ctor_m8B69E844CED63708B131F13D3EF25278A561549F,
	CookieException__ctor_m2F06EDE99EC7566274279CCB821E8CDC3FE94CF6,
	CookieException_GetObjectData_m8C1477B1CF776F28BB5933A8673CE0010BBF27CF,
	CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m9341D61524031A7DAC92656DF1FB570AC484A7B1,
	HttpHeaderInfo__ctor_m8564E1CE722340F433E694DED3E6BC499C991A76,
	HttpHeaderInfo_get_IsMultiValueInRequest_m848318D020DE8715A8A1970D168B773EB47A0223,
	HttpHeaderInfo_get_IsMultiValueInResponse_m045FE262927EB1F684F16DFCFF2BC606EE5BFC9A,
	HttpHeaderInfo_get_IsRequest_m3854E071DA6A521B4467BB8944CFA80A93BFEA34,
	HttpHeaderInfo_get_IsResponse_m97E800597D6D407C264D9AE602DECD67975546FF,
	HttpHeaderInfo_get_Name_mEBA3D7587363DF76748B8FDDA251A1580DD57768,
	HttpHeaderInfo_IsMultiValue_m3D992A8350078719889CC92925F1972BC3814FBE,
	HttpHeaderInfo_IsRestricted_m331D09A4A4F430844DDAAADDB6EBE263B087AB3B,
	HttpUtility_getChar_m504042133EF7A9FA411CA5C06612CC686E8A6AC4,
	HttpUtility_getInt_m1C0968EA5FBD1568A2639F766A8AD99A14619D3E,
	HttpUtility_writeCharBytes_m2EBD88C03BD8B3FDE9E72910384523721FA87F8D,
	HttpUtility_GetEncoding_m5477121594343AF9CCCE6785E45DC7BD30F3F826,
	HttpUtility_UrlDecode_mD616AFE862553A8E5CFA347D0EA5FE69DC6D6B77,
	HttpUtility_UrlDecode_mDA6546F244A5AED5BE997D420BF4C186F3FEA137,
	HttpUtility__cctor_m4E2CBD70E5102E4709414D4A97A2DA913920DB52,
	HttpVersion__cctor_m529DB5AA68F7E32FCCC4EF99528F7B90669AFDA5,
	NetworkCredential_get_Domain_mDC7D1EE5FFBE50F846C761A8A7BCC4E7F7F5729D,
	NetworkCredential_get_Password_mE29BDB4E1FBBEF3CE516EB7FABE06593E243F3DE,
	NetworkCredential_get_UserName_mA05A185777FCBCAD07FDAA8EE0D0C4823B436AD6,
	WebHeaderCollection__cctor_mEB1743827BE37B35F6616D00CC3F38FF944FC077,
	WebHeaderCollection__ctor_mEECFB012EADD76D7482A568A415D9D620C8563D7,
	WebHeaderCollection__ctor_m63EA39687C9B76F646C32EE4F959C976EC32CF3A,
	WebHeaderCollection_get_AllKeys_m0DB5921E1A7578EE28E53816E5186F3440F2C81B,
	WebHeaderCollection_get_Count_mF817BB1BAEC36FEA0CB2A3429B2A472E34CF2688,
	WebHeaderCollection_add_mC34CD5514FF18816AF89BFFC833941B7A9120478,
	WebHeaderCollection_addWithoutCheckingName_mAB7B3EE68E491004040EC862DFB808564A0ED60B,
	WebHeaderCollection_addWithoutCheckingNameAndRestricted_m91C0CD404E0113509F46C8A4B492533C58810303,
	WebHeaderCollection_checkColonSeparated_m30369BA3FBB4716321E0F51D4F4644351ACC3DDB,
	WebHeaderCollection_checkHeaderType_m1D96F5E40570441455DD91BBB4188306BF310B70,
	WebHeaderCollection_checkName_mB5DA5657A0FF1787C2A1A87B9FB70C5F39341801,
	WebHeaderCollection_checkRestricted_m54E7D4E4CBD5D55B094665C7C560C1CBB053455B,
	WebHeaderCollection_checkState_mEC96F37E68FEA213CEB012353F91E899FBFFA28B,
	WebHeaderCollection_checkValue_m4393AFF3438F9A0C62E7ED1CC7042792CF62B539,
	WebHeaderCollection_doWithCheckingState_m1276DF2C86EB3EF3E4DCBB92DC213C8591C0BB55,
	WebHeaderCollection_doWithCheckingState_m16846307C53AC8C6AFA22BE8A0A45BB75D96368D,
	WebHeaderCollection_doWithoutCheckingName_m8DD5B4E102E31035088A327997FCF384A28903E7,
	WebHeaderCollection_getHeaderInfo_mC060F193D9A1ACFA77131335C35EFC558B7C0396,
	WebHeaderCollection_isRestricted_m4C7A8745A5CF2B43C765C33E291944A591D699DC,
	WebHeaderCollection_removeWithoutCheckingName_m19D6E06C26000DDCECAD4F011C5E698B01874B61,
	WebHeaderCollection_setWithoutCheckingName_mF2CCE21EFF6E4FC6E87D39736E30B495D18B8715,
	WebHeaderCollection_InternalSet_m6375B6DD1CC86EA914C621A774AE603D4F54CF8A,
	WebHeaderCollection_InternalSet_m91BC047E018EA298D48361180C971088B6098FA9,
	WebHeaderCollection_IsHeaderName_m61ED5B919E7F6775067275B9970873F05D841CA5,
	WebHeaderCollection_IsHeaderValue_mC80B653B1AACC1B361EAFC2AD98DB7D32F5EB0A2,
	WebHeaderCollection_IsMultiValue_m905902EE4DFB65920B05565DEC15DAFC5EC41D5C,
	WebHeaderCollection_Add_mDF017822442501154F28DF402CD9CA0534649FFB,
	WebHeaderCollection_Get_m6FD17B1987139AB296121AC8B962CC77628782E9,
	WebHeaderCollection_Get_m1A6AEEAADD288D2761973CE16F367FD204FBE829,
	WebHeaderCollection_GetEnumerator_m67218F489DA78C5FCB7542A5D0046B80C6FE0BA6,
	WebHeaderCollection_GetKey_m6CDCA1F712BD44CCAF6E9E3E4F15211EDE1C418B,
	WebHeaderCollection_GetValues_mAC3638CF807EF70CAE0E573C663031569B4BA6AA,
	WebHeaderCollection_GetObjectData_mBE2CECEF70450E184B46C4190988987026C9DCC6,
	WebHeaderCollection_OnDeserialization_mD60C05CEDCD4AC967FBBD6421439AB0B08C4F502,
	WebHeaderCollection_Remove_m13A71F6CAC6880770C0F28DB7D949270BB5C590C,
	WebHeaderCollection_Set_mC935065D4FC845C4FB5D89BC46C342737F55A079,
	WebHeaderCollection_ToString_mA147577A18E6B01F512A2E3AB1EEF5A0A036BA93,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m0DBBE00BBBCED171F8D1149E6F471E0D3624F5B6,
	U3CU3Ec__DisplayClass5__ctor_m480EA80C30844DF63662B21C53688D023657CC42,
	U3CU3Ec__DisplayClass5_U3CGetObjectDataU3Eb__4_mE5150550CCF198D1D966D8A102733FA8C306DC99,
	U3CU3Ec__DisplayClass8__ctor_m20E812CA52FFA1CB0CF3668358AB900B04DE4462,
	U3CU3Ec__DisplayClass8_U3CToStringU3Eb__7_m7FA4E165490E2CC91CE7D912D043B3D318CCEA76,
	PayloadData__cctor_m792B5B2B91A5CA57615911875A8FE9CC2A9868B4,
	PayloadData__ctor_mBE4F21A9BD6CF6D496009EDB152D6C75264E6C23,
	PayloadData__ctor_m0DBD961CE282B53AE396003E0B52D2EDA60ACC88,
	PayloadData__ctor_m3867534B15BBEDD817598E13C2B51A56B63278C0,
	PayloadData_get_IncludesReservedCloseStatusCode_m71CDC65D98F79DD138C5AD395216A5ECF8C37470,
	PayloadData_get_ApplicationData_m664C38EAB0B230F72CC71E1403A0093F11B956FC,
	PayloadData_get_Length_m5BFB160D8BA70352C6522D791291FBCBD8E3A2D4,
	PayloadData_Mask_m5C5F30222C620AE7052EF78CA3859260A2441C9E,
	PayloadData_GetEnumerator_mA7F046102D35322DF5377BD40023DC0EDA37D779,
	PayloadData_ToArray_mA0E5A038ED07F275F5FA42AABD9448225B912DAB,
	PayloadData_ToString_mB753AF4739BE8F22A11A9CE3ACFB7184CB56636B,
	PayloadData_System_Collections_IEnumerable_GetEnumerator_m69D7EA791E592823A452D80B4317ECA206C0957F,
	U3CGetEnumeratorU3Ed__0_MoveNext_m26ED9E64C3057FD534B99D1B7B774B87957C51D6,
	U3CGetEnumeratorU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m7357821B357EAA2AB10714F4F26A25DEF5E9BAFA,
	U3CGetEnumeratorU3Ed__0_System_Collections_IEnumerator_Reset_mBAA25918B87DB63A8A08F0F5B3B3297CC0CA0736,
	U3CGetEnumeratorU3Ed__0_System_IDisposable_Dispose_m9B32B455D2121835AEF7EF97501E51747AFC0582,
	U3CGetEnumeratorU3Ed__0_System_Collections_IEnumerator_get_Current_mFDB0F239B80463E2B0D8EB715BFEF2B6FA5D1426,
	U3CGetEnumeratorU3Ed__0__ctor_m927209AA70A82C9174114B698E730157A49CD13C,
	U3CGetEnumeratorU3Ed__0_U3CU3Em__Finally2_mB40A1293F33B29CEBF1D6EEAA329417280FCBE6D,
	WebSocket__cctor_m98035982F9181F2806340B0E27AF472E1A91316F,
	WebSocket__ctor_mCC4F266948EDBC68F15C2B3A92739C45AE771273,
	WebSocket_get_HasMessage_m06775220F89C62EF8A24DFBDC15D458A6320D219,
	WebSocket_get_SslConfiguration_m8C80F3D37C0F59CB3AEC8A7026FA288998A58194,
	WebSocket_add_OnClose_mAE3B23969EEB9BC49D76B6C35A918B5F7548B8CB,
	WebSocket_remove_OnClose_mFAA5F88DB8A2CA77EB4C0075D9564B174128BF02,
	WebSocket_add_OnError_mF47BB16B56827F104368667279FB53DC3719C5FF,
	WebSocket_remove_OnError_m7C9EF6591B541C0CAA39E35D71AA6AB088333FD5,
	WebSocket_add_OnMessage_m8FFA9F0C345A87CC6650A1605BE1D81D00D50F7E,
	WebSocket_remove_OnMessage_mD553C345B96BD518792DC2AF7F2E5A5E7B9B40F7,
	WebSocket_add_OnOpen_m7ED907432F542DC99E2E7C802501F3AA3ECF1AA9,
	WebSocket_remove_OnOpen_mBE42E90C6BA9F0D7524925856A131B7C173C207A,
	WebSocket_checkHandshakeResponse_mD2D9D2EB276B10E9B2F30C3E45EC3CA41E620009,
	WebSocket_checkIfAvailable_m1B97F86F3BDCAE1BCFC04EA609B64557E57101B2,
	WebSocket_checkIfAvailable_m73D0114F90FF1980E7AA59D12928128E63E2809A,
	WebSocket_checkReceivedFrame_mA0B093E77C5BFA458E9CA38C7462F80C9A125125,
	WebSocket_close_m6D83AF3C34828B58580EC13AD59FF80EC187E2B9,
	WebSocket_closeAsync_mC8BC3589E578D14193277C1D0B67547570A1C8CB,
	WebSocket_closeHandshake_m4C0FCCC8F69D808BAC349BA02FFF52F8DD38E8E4,
	WebSocket_connect_m68EA5F51A09C8E38F498D80E9049BFBF22B2AED8,
	WebSocket_createExtensions_m23439C2AB665870741AB9FB4561E289C63B891B8,
	WebSocket_createHandshakeRequest_m46397E9ACF28159508B71F43302DB2C525DB2871,
	WebSocket_doHandshake_m3CC523AD884D63940611A1F33CD215E81E007FDC,
	WebSocket_enqueueToMessageEventQueue_m774EBC0EDD934EADF3BA29F08E43F0FB14182FEC,
	WebSocket_error_m9B719713DCD16DCCFF88048B1203500CA316A49D,
	WebSocket_fatal_m7E41521E5A7D204AFFC1E3EB85EE16314E1A50E2,
	WebSocket_fatal_mCCBA6922C448AC1346F5DDAB5F4DEFADD7B2EAA9,
	WebSocket_init_m85902AF3CCC027479A360D7DF22935CEC37B47A1,
	WebSocket_message_mEAA51906CD00DE2FE7866982E2A5FC6762D21379,
	WebSocket_messagec_mB68B23CE3D2F7DE889194AD17C7AD6D7539BE792,
	WebSocket_open_m722485B47DE06FA65ED6115CA30F965CBC97FC1D,
	WebSocket_processCloseFrame_mBE17F899B1503700332D66A34F0EB59103736AC7,
	WebSocket_processCookies_m8AED395626EFD82C6CFC4A8CC084316B43EFA7BD,
	WebSocket_processDataFrame_m5F766D674A538C1797418865E836AE60022D755E,
	WebSocket_processFragmentFrame_m072D1FC879595D3F8AF2EEBAE60D68E1DBEFBF1A,
	WebSocket_processPingFrame_mDE0F69459EA75CEBB8C13C75BAC7AEF855288021,
	WebSocket_processPongFrame_m50C268C80C47C5D0C477AA218AF923C1E15CC482,
	WebSocket_processReceivedFrame_mEA96A0A49C0EC14D35A01A17A5D43D634B93DD33,
	WebSocket_processSecWebSocketExtensionsServerHeader_mBFC0A51B44EDDE7CA938C6752ABA9F2906B2E6A0,
	WebSocket_processUnsupportedFrame_m85E19972B477387FFB7184CA99E98539558CE726,
	WebSocket_releaseClientResources_m9ED6DDDF32FD8AD32EE14FFA426953B33768673E,
	WebSocket_releaseCommonResources_mB81B41E30DD9AF962841823AA1F2A788208F450D,
	WebSocket_releaseResources_m02FFFE97712A73FF2349392C1FF6FF606DE0766C,
	WebSocket_releaseServerResources_m6BAA49461E5A52F8CEE47EDE050457675477BA64,
	WebSocket_send_mB2483C53CEBD43EB0383FF42901DA9727BAC5F83,
	WebSocket_send_m72AC66A2ADEEF5A408CA9756945181949DF9612F,
	WebSocket_send_m8654F92C90A67C55B58208C1E81F00AD06382571,
	WebSocket_send_m8DB180561110C7801126CF22CAA06732201353A6,
	WebSocket_sendBytes_m62FC307662CC170D09A859B814E83D9DADFB0BA2,
	WebSocket_sendHandshakeRequest_m9246D270FE8681FE165B0EB85AA74010A992E27E,
	WebSocket_sendHttpRequest_mEAA3EF54C07AC4E613D33577721FBEFF2EFE1EE6,
	WebSocket_sendProxyConnectRequest_m6EB1D033BED1C7893C4412ED8728E73C17A40E4B,
	WebSocket_setClientStream_mE95F7B8D381C33C5E078190F46B4325EDE870C18,
	WebSocket_setClientStreamOriginal_mEA48659AB41A3448B551FD50AF4AC41143FC250D,
	WebSocket_TrySetClientStream_mACDD30B8B5012E84938D7828B80959936A5F12B8,
	WebSocket_startReceiving_m72B8A5C33B9797BB4E58AE3F4F142D85AC5674CC,
	WebSocket_validateSecWebSocketAcceptHeader_m9A0399F441ABCA7634E7443274C07CA11F91BB1E,
	WebSocket_validateSecWebSocketExtensionsServerHeader_mB551E2FD3978778081F30B859F4C4755CB58415A,
	WebSocket_validateSecWebSocketProtocolServerHeader_mD3000F563F4B70C81EED45E8587C8B27C67F5892,
	WebSocket_validateSecWebSocketVersionServerHeader_m8F7004DEA113C120D1A1E0000600A4F1299FBED6,
	WebSocket_CheckSendParameter_mBAC975DA8D81B62FEEC22F44EE97E611E3F6BCAA,
	WebSocket_CreateBase64Key_m8D3796EA7666A09290C8A592C4A2E93C0BD4C482,
	WebSocket_CreateResponseKey_m3FAE41707C77B48180ABACC8465447E8F82036C1,
	WebSocket_CloseAsync_mE024BB9EEA317959740890F8A3889F9C7CEF4378,
	WebSocket_ConnectAsync_m8E7B6AB5EBCC59E5D764FC7A50FAFE03C20AA230,
	WebSocket_Send_mC8B41404AD501120AE09BBAC9CFCB0E8154BC823,
	WebSocket_System_IDisposable_Dispose_m9C2A05A1AC9D688645BE69E76B93708CAF68616C,
	WebSocket_U3CopenU3Eb__f_m86A7758F9CD398D52378ED57075D54AC187A4981,
	WebSocket_U3CTrySetClientStreamU3Eb__14_mE77B5DAD1366CE8F84B79CEF72CAFBB59FA9C542,
	U3CU3Ec__DisplayClassa__ctor_m296FB04282590699EC05F8223BCEDB82A7404113,
	U3CU3Ec__DisplayClassa_U3CcloseAsyncU3Eb__9_m9470F054C642BDB5265C5AB85E257CCDCCDC4D69,
	U3CU3Ec__DisplayClass19__ctor_m87782B82D2CF169A714EE490B6CC97C1FAE82671,
	U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__16_m27EDF8EA9A21C97D5512CF96C08772D33E4172FF,
	U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__17_m0372F7D546B868ADA53B1BB7D705F7E2BC6A2997,
	U3CU3Ec__DisplayClass19_U3CstartReceivingU3Eb__18_m73F73F6F378C8B9101AB10DA66C4E7452937F93D,
	U3CU3Ec__DisplayClass1c__ctor_m3B17892AA015D0C013C98E096E770CB97BF48CEA,
	U3CU3Ec__DisplayClass1c_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__1b_m888259490CB257E79BFD0D3F3FF5D676F8C19811,
	U3CU3Ec__DisplayClass1f__ctor_m51A799EB2EC3A6C40A97DABDD623C27D7FE661F4,
	U3CU3Ec__DisplayClass1f_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__1e_mDB32D547B230628E18E80BB6B9E1188339A5D3EC,
	U3CU3Ec__DisplayClass25__ctor_mB4717B4A9E7EE789C822A143B41A785EFED78BD7,
	U3CU3Ec__DisplayClass25_U3CConnectAsyncU3Eb__24_mD6A1C1BA857DCF93C5615EAC543C3D3FD972DE8F,
	WebSocketException__ctor_m17B4F2757F024B5E70799FB417481500525B1A62,
	WebSocketException__ctor_m30FB71D410A6F11923B99796009820254C1E93F4,
	WebSocketException__ctor_mDDAB2306941E75DBE02BEED24274225DE56C0DF2,
	WebSocketException__ctor_mFF81C814D67A1A760E9D3B9844049A8EB34321D4,
	WebSocketException__ctor_m8978DFB89AF2E6EE4BBC344CE1EEB099B0DF62FF,
	WebSocketException__ctor_m47C1E4AFFC6141B67632086DD4626CF3E74F7730,
	WebSocketException_get_Code_m9190D1BDBFB43EB512E9EBC6F9D0910281F5BFA9,
	WebSocketFrame__cctor_m9E48B3E758D026B48841B6CBEA6E4FD71F11B1B0,
	WebSocketFrame__ctor_m037E607498FE0A61A9230E7033B65EADF820F338,
	WebSocketFrame__ctor_m342593B1B594BFB620BAF41016123BE07A4CAD08,
	WebSocketFrame__ctor_m0013F7C13B3670F677819AA8C7808E73474DD484,
	WebSocketFrame__ctor_mFAC565C97A447307CA0F8996B2DC1FEB44DF3A97,
	WebSocketFrame_get_ExtendedPayloadLengthCount_mB8A7A85E3CF4176E95DC51E38A69AD0AE73456C7,
	WebSocketFrame_get_FullPayloadLength_mEED816EF2B8D6804E1C97B00272CD8415B8E3576,
	WebSocketFrame_get_IsClose_m2C710CA83E4C48F54BEB16F2C7DBC13DF33D3EBB,
	WebSocketFrame_get_IsCompressed_m654232F9B6C4ED563511BB73037676ED34DCDEA3,
	WebSocketFrame_get_IsContinuation_mA06255503060AF3EE98751FAA591CDD795632684,
	WebSocketFrame_get_IsData_m6A7238DE98BCCF63A2E8812A8D9B8D558714526B,
	WebSocketFrame_get_IsFinal_mE3115CE026B4FB87E10D395DD5FA1357C38A6F25,
	WebSocketFrame_get_IsFragment_m78EFF154862697C167C57D54245D7747915DFE67,
	WebSocketFrame_get_IsMasked_mFCD6A5B5AB576E9BB75C07DE7C6794B5F914F6FA,
	WebSocketFrame_get_IsPing_mC6AF4928175DF2A561C494E8F85866086D0C013D,
	WebSocketFrame_get_IsPong_mED1724210A02A7959B38F6E5B07A9A86A6E3E902,
	WebSocketFrame_get_IsText_m12ABC6D4A74426424D87997A57AC63C0B1D176FE,
	WebSocketFrame_get_Length_mC732C1FAAB740E2673FB56DDFEDD49E22FA54538,
	WebSocketFrame_get_Opcode_m62DDBCCECDB609D9DB270D1655ED18108CE695FC,
	WebSocketFrame_get_PayloadData_m9763EC127D7C389FCE1ECDEE67E777B6AD0D6635,
	WebSocketFrame_get_Rsv2_m9C572E43E3C4384BC89D2E34C20E76769E571173,
	WebSocketFrame_get_Rsv3_m47DA852603CBDA6EE65EC04AA0C4A1E8D26304B7,
	WebSocketFrame_createMaskingKey_m9EA9323DC90B53F3A675BF89B6ECA6B8AFA8C78F,
	WebSocketFrame_dump_m441FDCB5CFF26764E7BEE26E9CD4D9BE28A7172A,
	WebSocketFrame_print_mCED4B356FEA339F87D2E2EEDF6E9FBA08D3BFEF9,
	WebSocketFrame_processHeader_mAFCECF1F986C2E3A1A960C42004A5B89385B3CA9,
	WebSocketFrame_readExtendedPayloadLengthAsync_mDB59DE752266FF8012F24B321660C8F11BB4E8C3,
	WebSocketFrame_readHeaderAsync_m43A9CF70493FF6D68A5899099372723D30B86EA6,
	WebSocketFrame_readMaskingKeyAsync_mC318CC8738621ABC92991830165F5AB33DB29AAF,
	WebSocketFrame_readPayloadDataAsync_mAB298585C85ECBFC396A74914C24A67288F25F78,
	WebSocketFrame_CreateCloseFrame_m681CD2814AC45C21786570159BF45E6756F1AA97,
	WebSocketFrame_CreatePingFrame_mF6E79DF65ECF3B352727BB42521F956D95CB3F57,
	WebSocketFrame_ReadFrameAsync_m361390F8E16FE535199DA22AC6FDE08D8C3B5C85,
	WebSocketFrame_Unmask_m0C113F0BA69EE89CA1F5FB84920E3F020980E5D6,
	WebSocketFrame_GetEnumerator_mD31370ED87158275EF87932B4A5EF291109BFFA8,
	WebSocketFrame_PrintToString_m4BB9563342AE3313D11CAEB5DDF9BC0A88BC0A5E,
	WebSocketFrame_ToArray_m03558D4CF594154CCE80A45FE5204094F39D565D,
	WebSocketFrame_ToString_m1DF85247AA8EF4B8A7565BDB7F0B1B7A151BFF98,
	WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mD04B5573706066E38DB70205B4D976E7BCD7E879,
	U3CU3Ec__DisplayClass2__ctor_m9B2559761F7A54E94C559C44F25F35BBAE24C440,
	U3CU3Ec__DisplayClass2_U3CdumpU3Eb__0_mFC63C6CE0FDBB36F50458BCB4FC2BC6039E21624,
	U3CU3Ec__DisplayClass4__ctor_mCFC2E9FFCFB90BEB1D8BAF9DC2BEA174FC9B808F,
	U3CU3Ec__DisplayClass4_U3CdumpU3Eb__1_mF18215167F7DEA4E3525DF66BC87B5472FFA2466,
	U3CU3Ec__DisplayClass7__ctor_mE7232DF8359DA41CF4D73ED5EC72C49A878FB355,
	U3CU3Ec__DisplayClass7_U3CreadExtendedPayloadLengthAsyncU3Eb__6_m6F0734CDF6CE2E98FE5C3B0D19EE049A54F2AB88,
	U3CU3Ec__DisplayClassa__ctor_m395090876EB7B1CF96A8A0C8534E8CEAFC0BA2DD,
	U3CU3Ec__DisplayClassa_U3CreadHeaderAsyncU3Eb__9_m1269BB3DC98BD6C7AAA0204FF53D51D4271AC769,
	U3CU3Ec__DisplayClassd__ctor_mF342F4F999DB455F7838A6390A377B87B19F7C65,
	U3CU3Ec__DisplayClassd_U3CreadMaskingKeyAsyncU3Eb__c_m34866E43A32C4A5E8B9FE821D46AF3F230B0C3DB,
	U3CU3Ec__DisplayClass10__ctor_m125417832C3E0DE015992070289B00B0D0BA9D72,
	U3CU3Ec__DisplayClass10_U3CreadPayloadDataAsyncU3Eb__f_mEEBECB7CA00B0D4AE0D3E1B7BDD8A390E2068274,
	U3CU3Ec__DisplayClass16__ctor_mEF5CC0C41268BA10D263D5CA017A76DB60F35656,
	U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__12_m70A10814E1E23EF5F08FA9D7293DF63F5288DCFA,
	U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__13_m8C0D376E0FE9A550D61F84896FA31A5960C67443,
	U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__14_mE3240193FAA86BE217E8FE76B2D109783FDB15CB,
	U3CU3Ec__DisplayClass16_U3CReadFrameAsyncU3Eb__15_mA86FBA95D4ECC30F0176A7FC02A710CF57E29DF5,
	U3CGetEnumeratorU3Ed__18_MoveNext_m891487F0EE1FB7B22944D693E89D6F1B4410FD7C,
	U3CGetEnumeratorU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m7F68BEAF106255123ED843C0B82C84E588A5195A,
	U3CGetEnumeratorU3Ed__18_System_Collections_IEnumerator_Reset_mAB9EE2C0F26E02E60703525047A12BF192D8FFED,
	U3CGetEnumeratorU3Ed__18_System_IDisposable_Dispose_m4B0C0E67D480B6E730BA807479A148481C329892,
	U3CGetEnumeratorU3Ed__18_System_Collections_IEnumerator_get_Current_m1844F841F06656F0F3C2458112BC51F92FCFF8A4,
	U3CGetEnumeratorU3Ed__18__ctor_m1CE2D3F84C434505A39EEC11162B74C263D43836,
	U3CGetEnumeratorU3Ed__18_U3CU3Em__Finally1a_m3AB33D211D172E5CDA521EC32E1904A0D73ED724,
	WebsocketSharpClient_get_CertCallback_mE0A2333EB99C94C5CB8A27F191CA76241FB2A3BA,
	WebsocketSharpClient_set_CertCallback_mEB332F20C6B6DA14F09ED60E690B715950458B50,
	WebsocketSharpClient_add_OnOpen_m0122B05D29E9609FB917D3E5F467E6E25D7E77F3,
	WebsocketSharpClient_remove_OnOpen_m1EAB76D084E7983E800C675FA1FCF8E3CA058B9A,
	WebsocketSharpClient_add_OnClose_m5CB8EB56754F086164C1AA27CAD1779A7F37D74A,
	WebsocketSharpClient_remove_OnClose_m736A1BA53AF719DDE591598BAD4E2703D035E99B,
	WebsocketSharpClient_add_OnError_m36794324B28AC385A21C475A7D71FC01EC29417B,
	WebsocketSharpClient_remove_OnError_m5BD76B1F05E7BD0BA99033C060D085DC25EB41AB,
	WebsocketSharpClient_add_OnMessage_m970739596577BE7E462E3A6CE586463754C4DDF8,
	WebsocketSharpClient_remove_OnMessage_mA5F9A97C9C1B8CC7693813E57E881B8E7ECD3D9A,
	WebsocketSharpClient__ctor_m06EA31072566A1D950E81CBCB209A6475240BDAB,
	WebsocketSharpClient_Connect_m78FCD8ECD2740625513085F0DE0803615C617A91,
	WebsocketSharpClient_Send_mE2D350B88C26389E30EE1A6ED4525E37DD2BD973,
	WebsocketSharpClient_OnWebsocketOnOpen_m1B0394B373221DC4D7DA18F2F7FE96182E77EAD3,
	WebsocketSharpClient_OnWebsocketOnClose_mC62449E00D04761CA5B601FF4F7D070DD024E1E5,
	WebsocketSharpClient_OnWebsocketOnMessage_mCA8EF11B63B203707F832B8A114A0DB199097273,
	WebsocketSharpClient_OnWebsocketOnError_mFCBF8035F97B49EA9DA502D3441DDEEE5252A21E,
	WebsocketSharpClient_Dispose_m1C3CC621A326E262495B49B1FD09AB2AB75E8A42,
	WebsocketSharpClient_Dispose_m9BC82124AA02233013A4C9675DFA041620CA0F1F,
	WebsocketSharpFactory_get_CertCallbackOverride_m8C873CA91DB6F376DFCCFEBA69B2D7A05E7ABED3,
	WebsocketSharpFactory_set_CertCallbackOverride_mF9DD44B7D61CE4611ABEC897115D9D8253ED41B2,
	WebsocketSharpFactory__ctor_mDB3E46038A9096A07CCF04853542177F1F0511DA,
	WebsocketSharpFactory_DefaultCertCallback_m8E65079A8024D991F90548405C7CEA895CBB0961,
	WebsocketSharpFactory_Create_m58BA0DD3C46E53D9D59FC0DA824776849EA106C8,
};
static const int32_t s_InvokerIndices[779] = 
{
	14,
	27,
	2297,
	26,
	23,
	2139,
	862,
	23,
	2138,
	2134,
	23,
	23,
	23,
	23,
	23,
	2139,
	2298,
	2134,
	2136,
	2134,
	28,
	28,
	2133,
	31,
	23,
	3,
	27,
	28,
	26,
	23,
	26,
	23,
	89,
	14,
	23,
	2297,
	2140,
	2140,
	2130,
	31,
	23,
	2131,
	2131,
	89,
	31,
	106,
	10,
	2133,
	14,
	26,
	10,
	10,
	89,
	23,
	26,
	23,
	32,
	32,
	9,
	26,
	2134,
	9,
	9,
	26,
	26,
	862,
	862,
	23,
	2132,
	23,
	23,
	26,
	26,
	26,
	23,
	3,
	3,
	163,
	4,
	4,
	14,
	31,
	23,
	23,
	862,
	89,
	89,
	344,
	89,
	31,
	14,
	10,
	10,
	2133,
	14,
	26,
	9,
	10,
	89,
	23,
	26,
	862,
	23,
	23,
	23,
	862,
	2299,
	14,
	26,
	26,
	124,
	27,
	125,
	26,
	124,
	27,
	125,
	26,
	124,
	27,
	125,
	26,
	124,
	26,
	214,
	26,
	124,
	107,
	581,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	89,
	31,
	10,
	23,
	89,
	89,
	10,
	2300,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	107,
	27,
	27,
	26,
	2132,
	2132,
	2134,
	26,
	2132,
	23,
	23,
	23,
	2132,
	26,
	2133,
	4,
	862,
	23,
	23,
	2138,
	2134,
	23,
	26,
	23,
	2139,
	31,
	23,
	3,
	10,
	23,
	23,
	14,
	10,
	10,
	10,
	10,
	26,
	15,
	10,
	23,
	2136,
	9,
	26,
	23,
	23,
	23,
	28,
	2299,
	3,
	14,
	2301,
	26,
	26,
	0,
	401,
	14,
	27,
	206,
	862,
	23,
	89,
	31,
	344,
	14,
	89,
	89,
	14,
	23,
	26,
	14,
	14,
	1360,
	1361,
	1360,
	1361,
	1360,
	1361,
	1360,
	1361,
	1360,
	1361,
	23,
	26,
	14,
	14,
	206,
	28,
	2140,
	31,
	89,
	31,
	26,
	201,
	26,
	2302,
	23,
	206,
	28,
	31,
	89,
	14,
	26,
	26,
	14,
	23,
	23,
	3,
	173,
	173,
	14,
	28,
	28,
	105,
	105,
	105,
	26,
	26,
	26,
	31,
	23,
	89,
	4,
	4,
	4,
	3,
	23,
	206,
	23,
	2297,
	31,
	0,
	0,
	23,
	617,
	617,
	26,
	1102,
	1102,
	14,
	249,
	14,
	31,
	27,
	14,
	0,
	0,
	0,
	0,
	295,
	2303,
	0,
	162,
	-1,
	114,
	204,
	162,
	162,
	2304,
	0,
	218,
	376,
	2305,
	1444,
	2306,
	632,
	5,
	5,
	5,
	48,
	48,
	5,
	114,
	114,
	119,
	2307,
	587,
	2308,
	-1,
	1,
	0,
	2309,
	220,
	221,
	373,
	0,
	0,
	0,
	204,
	135,
	135,
	367,
	195,
	-1,
	162,
	2310,
	46,
	114,
	114,
	114,
	-1,
	-1,
	589,
	119,
	-1,
	0,
	0,
	114,
	3,
	23,
	30,
	23,
	26,
	23,
	209,
	23,
	26,
	14,
	14,
	89,
	14,
	23,
	23,
	14,
	32,
	-1,
	-1,
	27,
	14,
	14,
	14,
	1,
	119,
	-1,
	14,
	23,
	32,
	-1,
	-1,
	452,
	27,
	0,
	0,
	58,
	26,
	14,
	452,
	14,
	89,
	89,
	89,
	89,
	89,
	14,
	0,
	14,
	377,
	14,
	23,
	377,
	137,
	130,
	137,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	88,
	14,
	89,
	14,
	62,
	10,
	4,
	0,
	14,
	14,
	14,
	62,
	0,
	14,
	14,
	26,
	118,
	923,
	10,
	2,
	520,
	1,
	2,
	0,
	23,
	0,
	14,
	14,
	114,
	133,
	14,
	14,
	26,
	89,
	10,
	32,
	520,
	2311,
	26,
	759,
	14,
	14,
	14,
	26,
	14,
	3,
	23,
	27,
	31,
	26,
	26,
	31,
	14,
	26,
	89,
	110,
	331,
	31,
	14,
	26,
	14,
	26,
	26,
	31,
	14,
	26,
	10,
	32,
	227,
	227,
	2312,
	373,
	28,
	9,
	10,
	14,
	23,
	14,
	10,
	14,
	138,
	0,
	0,
	112,
	0,
	162,
	26,
	26,
	26,
	130,
	14,
	26,
	111,
	23,
	111,
	111,
	130,
	89,
	89,
	89,
	89,
	14,
	225,
	225,
	594,
	263,
	2313,
	0,
	0,
	1,
	3,
	3,
	14,
	14,
	14,
	3,
	111,
	23,
	14,
	10,
	156,
	27,
	27,
	94,
	94,
	0,
	26,
	31,
	0,
	967,
	1458,
	206,
	0,
	632,
	27,
	27,
	465,
	156,
	114,
	114,
	632,
	27,
	34,
	28,
	14,
	34,
	28,
	111,
	26,
	26,
	27,
	14,
	111,
	23,
	32,
	23,
	32,
	3,
	23,
	26,
	180,
	89,
	14,
	181,
	26,
	14,
	14,
	14,
	14,
	89,
	89,
	23,
	23,
	14,
	32,
	23,
	3,
	27,
	89,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	815,
	2314,
	2315,
	815,
	2316,
	2316,
	1847,
	89,
	14,
	14,
	89,
	26,
	27,
	27,
	950,
	23,
	23,
	26,
	23,
	9,
	26,
	9,
	9,
	9,
	9,
	9,
	26,
	9,
	23,
	23,
	23,
	23,
	9,
	2317,
	2318,
	2319,
	9,
	14,
	58,
	23,
	23,
	23,
	30,
	23,
	9,
	9,
	9,
	9,
	0,
	4,
	0,
	23,
	23,
	26,
	23,
	26,
	163,
	23,
	26,
	23,
	23,
	26,
	26,
	23,
	9,
	23,
	9,
	23,
	26,
	26,
	617,
	27,
	1102,
	1102,
	2320,
	249,
	3,
	23,
	2321,
	2322,
	2322,
	10,
	181,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	181,
	89,
	14,
	89,
	89,
	4,
	0,
	0,
	0,
	1473,
	195,
	1473,
	1473,
	162,
	217,
	2323,
	23,
	14,
	123,
	14,
	14,
	14,
	23,
	14,
	23,
	452,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	89,
	89,
	23,
	23,
	14,
	32,
	23,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	27,
	27,
	27,
	27,
	31,
	23,
	4,
	163,
	23,
	1183,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000035, { 11, 1 } },
	{ 0x06000138, { 0, 3 } },
	{ 0x06000151, { 3, 1 } },
	{ 0x06000160, { 4, 1 } },
	{ 0x06000167, { 5, 1 } },
	{ 0x06000168, { 6, 1 } },
	{ 0x0600016B, { 7, 4 } },
	{ 0x06000188, { 12, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[17] = 
{
	{ (Il2CppRGCTXDataType)2, 26378 },
	{ (Il2CppRGCTXDataType)2, 30247 },
	{ (Il2CppRGCTXDataType)3, 20164 },
	{ (Il2CppRGCTXDataType)2, 26381 },
	{ (Il2CppRGCTXDataType)3, 20165 },
	{ (Il2CppRGCTXDataType)2, 26386 },
	{ (Il2CppRGCTXDataType)2, 26388 },
	{ (Il2CppRGCTXDataType)2, 30248 },
	{ (Il2CppRGCTXDataType)3, 20166 },
	{ (Il2CppRGCTXDataType)3, 20167 },
	{ (Il2CppRGCTXDataType)2, 26391 },
	{ (Il2CppRGCTXDataType)2, 26414 },
	{ (Il2CppRGCTXDataType)2, 30249 },
	{ (Il2CppRGCTXDataType)3, 20168 },
	{ (Il2CppRGCTXDataType)3, 20169 },
	{ (Il2CppRGCTXDataType)3, 20170 },
	{ (Il2CppRGCTXDataType)2, 26421 },
};
extern const Il2CppCodeGenModule g_Byn_Awrtc_NativeCodeGenModule;
const Il2CppCodeGenModule g_Byn_Awrtc_NativeCodeGenModule = 
{
	"Byn.Awrtc.Native.dll",
	779,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	17,
	s_rgctxValues,
	NULL,
};
