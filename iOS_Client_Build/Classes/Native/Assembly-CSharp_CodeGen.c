﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DrawingController::DisableWriting()
extern void DrawingController_DisableWriting_m2131923E352FD568A2C10D8FD776A8403EC240F0 ();
// 0x00000002 System.Void DrawingController::EnableWriting()
extern void DrawingController_EnableWriting_m37344EED1A62573BF41ABBA91CDBDB78A1B01495 ();
// 0x00000003 System.Void DrawingController::Start()
extern void DrawingController_Start_mA7971C3A7FCE358ED961D73F99D30AD8BD580B54 ();
// 0x00000004 System.Void DrawingController::Update()
extern void DrawingController_Update_m5FF76A3D117703BA89A797498A2F39563061AEEC ();
// 0x00000005 UnityEngine.Vector2[] DrawingController::InterpolatePositions(UnityEngine.Vector2,UnityEngine.Vector2)
extern void DrawingController_InterpolatePositions_mE2321F262804BB3EEC9E883082C8C9A33C92211D ();
// 0x00000006 System.Void DrawingController::drawCircle(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32)
extern void DrawingController_drawCircle_m6F7DC7E0073B39CFBF00CB111BEB951AAE99EA22 ();
// 0x00000007 System.Void DrawingController::Brush(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color)
extern void DrawingController_Brush_m2CB8865174DEFDE7A0DC6025F955B7D99D42DC5C ();
// 0x00000008 System.Void DrawingController::SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color)
extern void DrawingController_SetPixel_m58A927469AD7D70224B94E2EB0F9B5EB0D8FE9F7 ();
// 0x00000009 System.Void DrawingController::SetBrushColor(UnityEngine.Color)
extern void DrawingController_SetBrushColor_m9C12F7CBD17FBF60E3B462A81C1CDFADFE0C7E96 ();
// 0x0000000A System.Void DrawingController::SetEraser()
extern void DrawingController_SetEraser_m08A06C73D7A3440C16C160C819037E1B4D24DA74 ();
// 0x0000000B System.Void DrawingController::ClearPanel()
extern void DrawingController_ClearPanel_mCF52907FCCB1DFB007FAD9B36AF827B2CFE47293 ();
// 0x0000000C System.Void DrawingController::OnDestroy()
extern void DrawingController_OnDestroy_m19D720854FD2068164251FBCA6675A08244AB369 ();
// 0x0000000D System.Void DrawingController::.ctor()
extern void DrawingController__ctor_m0C87419280108FC18D123546FDD17A6A55E1F83D ();
// 0x0000000E System.Void TextBoard::ExportValue()
extern void TextBoard_ExportValue_m38D033770AEA4D207B36100EA7F2797DA325302D ();
// 0x0000000F System.Void TextBoard::importvalue()
extern void TextBoard_importvalue_m1865965F44E51908604202CB2EC23DBA4E7EA4EE ();
// 0x00000010 System.Collections.IEnumerator TextBoard::DelayFunction()
extern void TextBoard_DelayFunction_m148DC4EBE0DDCD72E7B75C8BE35A123A58E3681A ();
// 0x00000011 System.Void TextBoard::OnKeyDown(UnityEngine.KeyCode)
extern void TextBoard_OnKeyDown_m8D2E3CC35B4CA5F4B96BEC1C6C755A7ACDD20EA1 ();
// 0x00000012 System.Void TextBoard::OnKeyUP(UnityEngine.KeyCode)
extern void TextBoard_OnKeyUP_m7263444BADA996D7B3C0085956FE5E062C9E39E9 ();
// 0x00000013 System.Void TextBoard::OnKey(UnityEngine.KeyCode)
extern void TextBoard_OnKey_m362F0BF901A717091BD08A028411EF9501A6BF70 ();
// 0x00000014 System.Void TextBoard::.ctor()
extern void TextBoard__ctor_m4F719D7DDBCC1161287AE658CC3BDAAD424B9D6F ();
// 0x00000015 System.Void ExtensionObject::SetID()
extern void ExtensionObject_SetID_m2616110E4B0837BA66CEC58EE6351D759D295754 ();
// 0x00000016 System.Void ExtensionObject::CheckForRelatives()
extern void ExtensionObject_CheckForRelatives_mAE49FDD221E89D7EFB9EAE99FCA4DBC188B12D78 ();
// 0x00000017 System.Void ExtensionObject::.ctor()
extern void ExtensionObject__ctor_mD38062EB15F56503651F714C072D486BBCD1D4D5 ();
// 0x00000018 System.Void ExtensionObject::.cctor()
extern void ExtensionObject__cctor_mCE3EA9996F6C72423022DB8F369AE8C21C5D2036 ();
// 0x00000019 UnityEngine.Networking.UnityWebRequest ProxyAPI::get_unityWebRequest()
extern void ProxyAPI_get_unityWebRequest_m6292BD6503A519B95D34D6D52BDD2C3C89AA9444 ();
// 0x0000001A System.Void ProxyAPI::set_unityWebRequest(UnityEngine.Networking.UnityWebRequest)
extern void ProxyAPI_set_unityWebRequest_m3AD98316E1378965EF1ECBBDCEE22B7E24FFE6FA ();
// 0x0000001B System.Collections.IEnumerator ProxyAPI::PostReq(System.String,System.Byte[])
extern void ProxyAPI_PostReq_mD1E30D8348F7C1998A735B42B8D4015497D83A1E ();
// 0x0000001C System.String ProxyAPI::PostRequest(System.String,System.Byte[])
extern void ProxyAPI_PostRequest_m6F06C0DFD82AD3E87681FF7C9062CBEC99513C83 ();
// 0x0000001D System.String ProxyAPI::GetRequest(System.String)
extern void ProxyAPI_GetRequest_m4E2C093EA858872413FDFEC041E89D4923B7958E ();
// 0x0000001E System.Byte[] ProxyAPI::GetRequestData(System.String)
extern void ProxyAPI_GetRequestData_m43ACA4865541A9A437FD0BC4E080BE872339FDB0 ();
// 0x0000001F System.Collections.IEnumerator ProxyAPI::GetReq(System.String)
extern void ProxyAPI_GetReq_mE5FC0B227D6C7FFC6716910932F1176DE25C190F ();
// 0x00000020 System.String ProxyAPI::UploadFile(System.String,System.String)
extern void ProxyAPI_UploadFile_mB6ADD6E88AE12C89058E8AEF0FD7DDFD18BC8774 ();
// 0x00000021 System.Collections.IEnumerator ProxyAPI::UploadMultiple(System.String,System.String)
extern void ProxyAPI_UploadMultiple_mB5C332AEA9ABDBC33F43D6A8F09C25B2A79FEDE6 ();
// 0x00000022 System.Collections.IEnumerator ProxyAPI::GetFileUrl(UnityEngine.WWWForm)
extern void ProxyAPI_GetFileUrl_mAC8B4A47E01D331DCD62C26EB2B1A60233DAEF35 ();
// 0x00000023 System.String ProxyAPI::DeleteRequest(System.String)
extern void ProxyAPI_DeleteRequest_m4D0F818C77E433307D8C28B98309D37094A74062 ();
// 0x00000024 System.Collections.IEnumerator ProxyAPI::_DeleteReq(System.String)
extern void ProxyAPI__DeleteReq_m8181639B9ECE78CC248FEADDD05FF79C9293E25E ();
// 0x00000025 System.Void ProxyAPI::.ctor()
extern void ProxyAPI__ctor_m3F6BE70D765716DC081884C885CBEC604D9B3A73 ();
// 0x00000026 System.Void RoomManagementAPI::.ctor()
extern void RoomManagementAPI__ctor_m2304E4B5422EE39795C668075F0057738225DCE5 ();
// 0x00000027 System.Void RoomManagementAPI::.cctor()
extern void RoomManagementAPI__cctor_mC2729D6769F550701F37D70173322D5951166838 ();
// 0x00000028 System.String RoomManager::get_RoomName()
extern void RoomManager_get_RoomName_m63350857141E1E8D966E8731D590D95F1195400D ();
// 0x00000029 System.Void RoomManager::set_RoomName(System.String)
extern void RoomManager_set_RoomName_m31EBC51565D7C80FEEA83C3E502A8F629BF1DBE0 ();
// 0x0000002A System.Boolean RoomManager::get_IsRender()
extern void RoomManager_get_IsRender_mE23C30B9C87D34C25983105DF70CA238B7C6776D ();
// 0x0000002B System.Void RoomManager::set_IsRender(System.Boolean)
extern void RoomManager_set_IsRender_mCAA6A5E8B80E46E5E77043DA00ABB3E50BA58DF6 ();
// 0x0000002C System.Void RoomManager::Start()
extern void RoomManager_Start_m3BEB349960C5482D5D17C1747EE1DB18ADDE7662 ();
// 0x0000002D System.Void RoomManager::RoomRender(System.String)
extern void RoomManager_RoomRender_m47430319B45D2A695FD8BD8E4E484E83F854C646 ();
// 0x0000002E System.Void RoomManager::GetEnvironments()
extern void RoomManager_GetEnvironments_mCEBC114649D4C78297F4B6466E2F9E2968CDFC26 ();
// 0x0000002F System.Void RoomManager::GameObjectRoomEventHandler()
extern void RoomManager_GameObjectRoomEventHandler_mD5DCFD0275D020628E5FED53A6903DC206E7D106 ();
// 0x00000030 System.Boolean RoomManager::RenderRoomObJ(System.String)
extern void RoomManager_RenderRoomObJ_m2D54561FB9B86CE82314F29EC973012DA6774D40 ();
// 0x00000031 System.Void RoomManager::RenderRoom(System.String)
extern void RoomManager_RenderRoom_m438F0146EAFD8EF5716C50E06CF74C0696A58600 ();
// 0x00000032 System.Void RoomManager::RenderRoom(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_RenderRoom_m1DECFB5A63802CC947C9D20369DCDD3B3AB861E7 ();
// 0x00000033 System.Void RoomManager::GameObjectEventHandler()
extern void RoomManager_GameObjectEventHandler_mA3D1B701303969CB8D152CBEE6B41275AEFA38B4 ();
// 0x00000034 System.Void RoomManager::LoadEvent(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_LoadEvent_m6EFC4998669AC39C2944C4DAD10E7BF711BC2667 ();
// 0x00000035 System.Void RoomManager::RoomRenderClear()
extern void RoomManager_RoomRenderClear_mE993717F563866ED0668BE8CF086175650A4087A ();
// 0x00000036 System.Void RoomManager::RenderTitle(System.Boolean)
extern void RoomManager_RenderTitle_m2E68E79436DCF4A32EB3406280385F6789077AF5 ();
// 0x00000037 System.Void RoomManager::RoomsRenderView()
extern void RoomManager_RoomsRenderView_mA02B651AEEE2C124484E11982DC05705623801AA ();
// 0x00000038 System.Void RoomManager::OpenRoomEvent(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_OpenRoomEvent_m2BE66163E2C2C39D0F5207FA375C2CF89935EF06 ();
// 0x00000039 System.Void RoomManager::openRoom()
extern void RoomManager_openRoom_m9D11789EAEE5ACDAF3C9929DE0F10C337ABDBB99 ();
// 0x0000003A System.Void RoomManager::DeleteRoomData(System.String)
extern void RoomManager_DeleteRoomData_m285B2039D5F3FFE20565B90E974E7A4AC33ABE47 ();
// 0x0000003B System.Void RoomManager::DeleteRoomList(UnityEngine.GameObject)
extern void RoomManager_DeleteRoomList_m0930D24D89EEE2FA42576E2466362D54A309BB9F ();
// 0x0000003C System.Void RoomManager::DeleteListViewRoom()
extern void RoomManager_DeleteListViewRoom_m8E00A94E0F50777D4FCF7EE33F989154E7FD0A4A ();
// 0x0000003D System.Void RoomManager::.ctor()
extern void RoomManager__ctor_m647CD75713362D4AE755130C4C3148F47AF980C3 ();
// 0x0000003E System.Void RoomManager::<GameObjectRoomEventHandler>b__22_0(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_U3CGameObjectRoomEventHandlerU3Eb__22_0_mD8E786072A27B8D5E759F361EED6C12336BF7165 ();
// 0x0000003F System.Void RoomManager::<GameObjectEventHandler>b__26_0(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_U3CGameObjectEventHandlerU3Eb__26_0_m92786B7DC786E507F62DF402BE365D31BF97CD34 ();
// 0x00000040 System.Void RoomManager::<RoomsRenderView>b__30_3(UnityEngine.EventSystems.BaseEventData)
extern void RoomManager_U3CRoomsRenderViewU3Eb__30_3_m43A3A89BA7ECEEA5CA0D668997EF564E19128120 ();
// 0x00000041 System.Void RoomStateManager::Start()
extern void RoomStateManager_Start_m8A8C7EDD83AB6EBA70242713F71A2C9C88B8C61F ();
// 0x00000042 System.Void RoomStateManager::Update()
extern void RoomStateManager_Update_mE204472A689A436AED50F92D8F49240FB5BA8435 ();
// 0x00000043 System.Void RoomStateManager::RenderRoomObject()
extern void RoomStateManager_RenderRoomObject_m68E340185666F4889B7887AED67E2096BFF7627B ();
// 0x00000044 System.Void RoomStateManager::RoomSaveData()
extern void RoomStateManager_RoomSaveData_mD6E7E4984D7BC261CB41B017B0DED4D946DDC240 ();
// 0x00000045 System.Void RoomStateManager::RoomRenderData()
extern void RoomStateManager_RoomRenderData_m3F10B7FC6B009209C16ECFE059790EF7512D8B5D ();
// 0x00000046 System.Void RoomStateManager::PerviousScene()
extern void RoomStateManager_PerviousScene_mC4BE3BFFA3B5CCA406FBE182A1FF3009D509057B ();
// 0x00000047 System.Void RoomStateManager::.ctor()
extern void RoomStateManager__ctor_mA73581202F7338531C151967CD9DD8B963405B67 ();
// 0x00000048 System.Void SceneObject::.ctor()
extern void SceneObject__ctor_mA213B91EE80D8FABD13EEAC98A1D3D1B5036B1BB ();
// 0x00000049 System.Double PositionObject::get_x()
extern void PositionObject_get_x_m91A18DC5F9632B883BA76D6DAB51FB12D015428A ();
// 0x0000004A System.Void PositionObject::set_x(System.Double)
extern void PositionObject_set_x_mD12F10E300C76DE5F6D826B171E5511D56071CF8 ();
// 0x0000004B System.Double PositionObject::get_y()
extern void PositionObject_get_y_m8ACF2EEC5A0FF1216547E96DA9E5F1BE8A30D523 ();
// 0x0000004C System.Void PositionObject::set_y(System.Double)
extern void PositionObject_set_y_m21927C781AC12FC0F10A1ACEB0DDC3CA32F234A9 ();
// 0x0000004D System.Double PositionObject::get_z()
extern void PositionObject_get_z_m30B004EA666A4D3FE013881BEFB9B6F43475D99D ();
// 0x0000004E System.Void PositionObject::set_z(System.Double)
extern void PositionObject_set_z_m317D3FC1745CA6B590262B5FC11AE7C1A087F594 ();
// 0x0000004F System.Void PositionObject::.ctor()
extern void PositionObject__ctor_m99427B687D53B6FB73E25DDECA9AD3636198DD65 ();
// 0x00000050 System.Single LocalScaleObject::get_x()
extern void LocalScaleObject_get_x_m792638A1830FDF7137FC2ED35749B17D602E2496 ();
// 0x00000051 System.Void LocalScaleObject::set_x(System.Single)
extern void LocalScaleObject_set_x_m5C4BE0FCDE8167567E12E7A2EBD6DACA0B7CE40A ();
// 0x00000052 System.Single LocalScaleObject::get_y()
extern void LocalScaleObject_get_y_m80731F62762689E6B61A328112642F8632827AAF ();
// 0x00000053 System.Void LocalScaleObject::set_y(System.Single)
extern void LocalScaleObject_set_y_m0B7D1723B724489DAD4734612260A98FCBC75389 ();
// 0x00000054 System.Single LocalScaleObject::get_z()
extern void LocalScaleObject_get_z_mE0EB41F8CE326D8C93D1061AE670522A605DC904 ();
// 0x00000055 System.Void LocalScaleObject::set_z(System.Single)
extern void LocalScaleObject_set_z_mE04D7EE77B0C3C84A12F0638191C28B348BE667E ();
// 0x00000056 System.Void LocalScaleObject::.ctor()
extern void LocalScaleObject__ctor_m7C0F776DD4884128D93FBAD33C7F06067D573FE5 ();
// 0x00000057 System.Single RotationObject::get_x()
extern void RotationObject_get_x_m6192CCBC031508889E8C809A22D9F290187BED96 ();
// 0x00000058 System.Void RotationObject::set_x(System.Single)
extern void RotationObject_set_x_mCE2C08178F31E28A4237697AEE0ECCB07A6B5192 ();
// 0x00000059 System.Single RotationObject::get_y()
extern void RotationObject_get_y_m172B00CA6B967A95EB12AB0AD57776EA532D6AAE ();
// 0x0000005A System.Void RotationObject::set_y(System.Single)
extern void RotationObject_set_y_mE6E11E731EA2853480DDF876BE608027DC9B9ED7 ();
// 0x0000005B System.Single RotationObject::get_z()
extern void RotationObject_get_z_m65671FEE94919C3C993E803AA4D362DC594F0E9C ();
// 0x0000005C System.Void RotationObject::set_z(System.Single)
extern void RotationObject_set_z_mA8E8328B7DE001791054DE67D98A7E31A84FD2E5 ();
// 0x0000005D System.Void RotationObject::.ctor()
extern void RotationObject__ctor_m1DB1A07D6D10F7F03BB5DEC7648E5189D183BC85 ();
// 0x0000005E System.String CollabarationSceneObject::get_collaboration_id()
extern void CollabarationSceneObject_get_collaboration_id_m812F7E461A88035730B89E76E9ED55A869FF0678 ();
// 0x0000005F System.Void CollabarationSceneObject::set_collaboration_id(System.String)
extern void CollabarationSceneObject_set_collaboration_id_m5B76B401CA844AC0E842C69F924535E0DD62992B ();
// 0x00000060 System.String CollabarationSceneObject::get_objectName()
extern void CollabarationSceneObject_get_objectName_m743AC3E854DBE78B7A12D2255821B60D0B164F3E ();
// 0x00000061 System.Void CollabarationSceneObject::set_objectName(System.String)
extern void CollabarationSceneObject_set_objectName_mB14949A09B5F39C4458EBEAAC4A82B83ECE10691 ();
// 0x00000062 System.String CollabarationSceneObject::get_fileType()
extern void CollabarationSceneObject_get_fileType_mEAFCB5CD80AFA7ECCFAA06D3E2D27CDF9F3047F0 ();
// 0x00000063 System.Void CollabarationSceneObject::set_fileType(System.String)
extern void CollabarationSceneObject_set_fileType_m25FF26B2B448DE6C3FBEB0322D578281ECCEEF39 ();
// 0x00000064 System.Boolean CollabarationSceneObject::get_isAssestBundle()
extern void CollabarationSceneObject_get_isAssestBundle_m9153546A3BBCC756C303A31367D2829AFE3F3564 ();
// 0x00000065 System.Void CollabarationSceneObject::set_isAssestBundle(System.Boolean)
extern void CollabarationSceneObject_set_isAssestBundle_m640788F71687B04B6257236FE7974EC0D0BE63C0 ();
// 0x00000066 System.Boolean CollabarationSceneObject::get_isScriptAttached()
extern void CollabarationSceneObject_get_isScriptAttached_m85465000FD3F9BCBB1D1366DCDFB3BE6B80EF569 ();
// 0x00000067 System.Void CollabarationSceneObject::set_isScriptAttached(System.Boolean)
extern void CollabarationSceneObject_set_isScriptAttached_m32473B0026E1C2455CF89E9DCDBB1757DD5ADA09 ();
// 0x00000068 System.DateTime CollabarationSceneObject::get_created_at()
extern void CollabarationSceneObject_get_created_at_m9BAD9B29BDAD0F63700D01D38AA8145231D95C09 ();
// 0x00000069 System.Void CollabarationSceneObject::set_created_at(System.DateTime)
extern void CollabarationSceneObject_set_created_at_m994F225745DCEF865BA449D2D3EFC43D94C651AB ();
// 0x0000006A System.String CollabarationSceneObject::get_created_by()
extern void CollabarationSceneObject_get_created_by_m534260D8E7FE7EEC5D975D4B2D3535A93878C150 ();
// 0x0000006B System.Void CollabarationSceneObject::set_created_by(System.String)
extern void CollabarationSceneObject_set_created_by_m54838A4F8D25AF909996ADBFE9E121A0FE6A8B2C ();
// 0x0000006C System.String CollabarationSceneObject::get_id()
extern void CollabarationSceneObject_get_id_m557C3DC818087D122780F1B47E8173A921527625 ();
// 0x0000006D System.Void CollabarationSceneObject::set_id(System.String)
extern void CollabarationSceneObject_set_id_m161DFCA73280200B4539CC24780531647906AA8F ();
// 0x0000006E System.Void CollabarationSceneObject::.ctor()
extern void CollabarationSceneObject__ctor_m1C471E092CE08F87C78D2C198DE1B2826A54C299 ();
// 0x0000006F System.String ApplicationContext::get_Token()
extern void ApplicationContext_get_Token_m8A51A88F0D695497A30D51E008ECE05A7E9C2EAD ();
// 0x00000070 System.String ApplicationContext::get_DropboxlocalPath()
extern void ApplicationContext_get_DropboxlocalPath_m05B6A98612CF1E68D30500AEA824D5DF95F5D304 ();
// 0x00000071 System.String ApplicationContext::get_APIDropBoxDowanldURI()
extern void ApplicationContext_get_APIDropBoxDowanldURI_m901569CFFF2151C6FCEBF2FDD5319CCF334BB0C4 ();
// 0x00000072 System.String ApplicationContext::get_APIDropBoxGetFileURI()
extern void ApplicationContext_get_APIDropBoxGetFileURI_m31E0133EA30771A34724DC90DF7D9452704324D7 ();
// 0x00000073 System.String ApplicationContext::get_persDataPath()
extern void ApplicationContext_get_persDataPath_m2943E4EB79B3139E541A1CD245461CFA1FDDBABF ();
// 0x00000074 System.String ApplicationContext::get_PersDataScript()
extern void ApplicationContext_get_PersDataScript_mCECC86EF6BAFF9699B0C433202ABBBA570115DD3 ();
// 0x00000075 System.String ApplicationContext::get_PersistentAssestBundle()
extern void ApplicationContext_get_PersistentAssestBundle_m6B473E0CA101797834B57D039BCF2BE90EA66BFC ();
// 0x00000076 System.String ApplicationContext::get_PersistentObject()
extern void ApplicationContext_get_PersistentObject_mF933AB0ED26D02A0EBD2C674AFBED8297891F192 ();
// 0x00000077 System.String ApplicationContext::get_PersistentRoomDataDetails()
extern void ApplicationContext_get_PersistentRoomDataDetails_mC52BC2E8375865E9D66C4F24AEAD6848627DD4B2 ();
// 0x00000078 UserInfoModel ApplicationContext::get_UserInfoModel()
extern void ApplicationContext_get_UserInfoModel_mD24B1FF89ABC497A861E1ED0A8DE1119EED87F9A ();
// 0x00000079 System.Void ApplicationContext::set_UserInfoModel(UserInfoModel)
extern void ApplicationContext_set_UserInfoModel_mE8F360ED673D0F1E4388088D58D8E4F5DED64011 ();
// 0x0000007A System.Boolean ApplicationContext::get_ISGizmoSelectParenGameobject()
extern void ApplicationContext_get_ISGizmoSelectParenGameobject_m51180CA878CAEC0796C1F0826DFA9EE4F8568443 ();
// 0x0000007B System.Void ApplicationContext::set_ISGizmoSelectParenGameobject(System.Boolean)
extern void ApplicationContext_set_ISGizmoSelectParenGameobject_m7073A1226CEDCAB8407A4FC7D84F8BE4C5019C15 ();
// 0x0000007C System.Collections.Generic.List`1<ViewerSettingModel> ApplicationContext::get_ViewerSettingModels()
extern void ApplicationContext_get_ViewerSettingModels_m10CDCC00DFCB249837E713210A951E1C6FF35F75 ();
// 0x0000007D System.Void ApplicationContext::set_ViewerSettingModels(System.Collections.Generic.List`1<ViewerSettingModel>)
extern void ApplicationContext_set_ViewerSettingModels_mC0DA470497231CEDA89B4C8529ED208444D90970 ();
// 0x0000007E System.Boolean ApplicationContext::get_IsPropertyPanel()
extern void ApplicationContext_get_IsPropertyPanel_m0EE84EE7953178514C92FD8E893CE6329F94CEBC ();
// 0x0000007F System.Void ApplicationContext::set_IsPropertyPanel(System.Boolean)
extern void ApplicationContext_set_IsPropertyPanel_mB48B947C4F6423220B592AC638D3E8AD3AFE0589 ();
// 0x00000080 System.Boolean ApplicationContext::get_IsNavigationPanel()
extern void ApplicationContext_get_IsNavigationPanel_m2898DB85F55F85C4221964339D0854C05788D38C ();
// 0x00000081 System.Void ApplicationContext::set_IsNavigationPanel(System.Boolean)
extern void ApplicationContext_set_IsNavigationPanel_mE69689A1D37B3596FC729E91EEF11B4E706D27C1 ();
// 0x00000082 System.String ApplicationContext::get_PersDataPath()
extern void ApplicationContext_get_PersDataPath_m20858D66D4828C1ED0E3CEBC7F6336E89D61DDE0 ();
// 0x00000083 System.String ApplicationContext::get_PersAssestPathLog()
extern void ApplicationContext_get_PersAssestPathLog_m7635B1BD359760E1C5CE1420816ACC453B8AA339 ();
// 0x00000084 UserInfoModel ApplicationContext::UpdateModel(UserProfile,UserOrganization,System.String,System.String)
extern void ApplicationContext_UpdateModel_m1A388AFD80A6D9F0CEB7C6F4B8B922847D0B5F65 ();
// 0x00000085 System.Collections.Generic.List`1<StorageModel> ApplicationContext::get_StorageModel()
extern void ApplicationContext_get_StorageModel_m5D205CADDCCF49B0B338DA4F8075E7A98A8BFF0A ();
// 0x00000086 System.Void ApplicationContext::set_StorageModel(System.Collections.Generic.List`1<StorageModel>)
extern void ApplicationContext_set_StorageModel_mF2C2082AE4EFE61165AD1D00F6535C7C8BC084A1 ();
// 0x00000087 System.Collections.Generic.List`1<EnvironmentModel> ApplicationContext::get_EnvironmentModels()
extern void ApplicationContext_get_EnvironmentModels_mA618F9758C178A1E6067E0CEAE18D2E7B32EEBE6 ();
// 0x00000088 System.Void ApplicationContext::set_EnvironmentModels(System.Collections.Generic.List`1<EnvironmentModel>)
extern void ApplicationContext_set_EnvironmentModels_m490E6890FB6D7BCB08BFA02CBC73C0CF15FC9E31 ();
// 0x00000089 System.Void ChangeSkybox::Start()
extern void ChangeSkybox_Start_mA0E4579790C2535D81F5129C43D6991280139837 ();
// 0x0000008A System.Void ChangeSkybox::Update()
extern void ChangeSkybox_Update_m21BEF5117F52A32890930167ED1594C26AA8E03F ();
// 0x0000008B System.Void ChangeSkybox::three()
extern void ChangeSkybox_three_m03492A68B665CB909A1301EA6E34952CDFDE9E55 ();
// 0x0000008C System.Void ChangeSkybox::Default()
extern void ChangeSkybox_Default_mC919F5821CAF770C81C07A1078A1FE3233B390DC ();
// 0x0000008D System.Void ChangeSkybox::Openroom()
extern void ChangeSkybox_Openroom_m662CE63CEBF9A09888EB7D2C4B59CB78E479E435 ();
// 0x0000008E System.Void ChangeSkybox::Warehouse_Room()
extern void ChangeSkybox_Warehouse_Room_mC3CA3E568888A139AABE3C149FF0E4C34DA1FB47 ();
// 0x0000008F System.Void ChangeSkybox::.ctor()
extern void ChangeSkybox__ctor_m18FC0E3AA2DEE526CAC2E488389903FAE53E02E4 ();
// 0x00000090 System.Void PanelActive::Hide_Showpanel()
extern void PanelActive_Hide_Showpanel_mC92D0CF8E6B6C0F699903D9E9709C8D6100353EB ();
// 0x00000091 System.Void PanelActive::.ctor()
extern void PanelActive__ctor_m516D8E1BCB77CFE1645A748B225CFCF904E1C78D ();
// 0x00000092 System.Void MenuEvent::Start()
extern void MenuEvent_Start_m8AFF7E3C4A6CC870401883B5461D35646B116796 ();
// 0x00000093 System.Void MenuEvent::ModeBasedDisplay()
extern void MenuEvent_ModeBasedDisplay_m0F160028988C910FF72DB8AD75973B62F08E9E26 ();
// 0x00000094 System.Void MenuEvent::GetArg()
extern void MenuEvent_GetArg_m7FFC2EC1CA65AF8E2C577FD2E3CDB8101ADBCA86 ();
// 0x00000095 System.Void MenuEvent::PDFBoard()
extern void MenuEvent_PDFBoard_m79BA87A3089E425941D850EBBEBA8855EF010F8D ();
// 0x00000096 System.Void MenuEvent::WordBoard()
extern void MenuEvent_WordBoard_m0A8E9814D76AE0486EAD2D8427246688C43BAC68 ();
// 0x00000097 System.Void MenuEvent::Textboard()
extern void MenuEvent_Textboard_m574EC5231DFC5C3FAE63E14FDBBC22582F702A48 ();
// 0x00000098 System.Void MenuEvent::StreamBoard()
extern void MenuEvent_StreamBoard_m990BFE67EB3C128CF97B604FCE5BCEE942851762 ();
// 0x00000099 System.Void MenuEvent::VideoBoard()
extern void MenuEvent_VideoBoard_mE95B42C295CB417E6501637BAE0A65C817429A6C ();
// 0x0000009A System.Void MenuEvent::AudioBoard()
extern void MenuEvent_AudioBoard_m1D841C39F4E6EECF6B7BFE8D50EFF4A69C6F3B51 ();
// 0x0000009B System.Void MenuEvent::WhiteBoard()
extern void MenuEvent_WhiteBoard_mC0B8DCCC05AC6F981AED7935D9DF0E5CD3AF563D ();
// 0x0000009C System.Void MenuEvent::WebBrowserBoard()
extern void MenuEvent_WebBrowserBoard_m341C8BF3F8CE9AA7243E00565B36ABA01CFF80F8 ();
// 0x0000009D System.Void MenuEvent::LiveStreamBoard()
extern void MenuEvent_LiveStreamBoard_m2EED7B9ADCD5CAA17719004561A7AD365C6F3B45 ();
// 0x0000009E System.Void MenuEvent::RealTimeStreamingProtocol()
extern void MenuEvent_RealTimeStreamingProtocol_mE28507537B7AA00DFB427280F1B6551A86925512 ();
// 0x0000009F System.Void MenuEvent::rader()
extern void MenuEvent_rader_m184EBBF8F7CDFDB2AA4B2E1917DED0723CD5D292 ();
// 0x000000A0 System.Void MenuEvent::Bar()
extern void MenuEvent_Bar_m84115A03E637F4A74FFDD46E3095F681B63251F0 ();
// 0x000000A1 System.Void MenuEvent::Torus()
extern void MenuEvent_Torus_m4B8E17F5C42B6A30DF358C33A9E299D773D2A9A8 ();
// 0x000000A2 System.Void MenuEvent::Pie()
extern void MenuEvent_Pie_m73623D994B02CD36DDA5EDB17972810DB72454F1 ();
// 0x000000A3 System.Void MenuEvent::Graph()
extern void MenuEvent_Graph_m4FFDAD0045E92907543FA64003DD476F6295D632 ();
// 0x000000A4 System.Void MenuEvent::Bubble()
extern void MenuEvent_Bubble_m4BAF3E72FE88A95C8128B5ADDA71059D5CFE3817 ();
// 0x000000A5 System.Void MenuEvent::ThreeDRadar()
extern void MenuEvent_ThreeDRadar_mF09CD8BCB45121FBD195C4A371498B022B7CB1BF ();
// 0x000000A6 System.Void MenuEvent::ThreeDBar()
extern void MenuEvent_ThreeDBar_m24EF3B11DD627E7CCE43A3B728A500A883D7F2AA ();
// 0x000000A7 System.Void MenuEvent::ThreeDTorus()
extern void MenuEvent_ThreeDTorus_m709A45474E42011F4192303905D6244AE5A339F2 ();
// 0x000000A8 System.Void MenuEvent::ThreeDPie()
extern void MenuEvent_ThreeDPie_m7A86E7A265462986CBEDA89686AF25F0EC67B94D ();
// 0x000000A9 System.Void MenuEvent::ThreeDGraph()
extern void MenuEvent_ThreeDGraph_m2FD206C5D28EA96962A2AD911037053F4839B6F7 ();
// 0x000000AA System.Void MenuEvent::ThreeDBubble()
extern void MenuEvent_ThreeDBubble_m6ABB1897A59FEAA8B6D7EE231B1D2B3B95C648B8 ();
// 0x000000AB System.Void MenuEvent::.ctor()
extern void MenuEvent__ctor_m6243BA20AEC4CD6C09C136C93F29FDEDB8A7B5E7 ();
// 0x000000AC System.Void MenuEvent::.cctor()
extern void MenuEvent__cctor_m7245187AE33FAF8A7ED3D51BDC91BDF9408E1B6D ();
// 0x000000AD System.Void MenuEvent::<Start>b__70_0()
extern void MenuEvent_U3CStartU3Eb__70_0_mFE2B98CAE2B9BBAE7AA076657C002C07715986F0 ();
// 0x000000AE System.Void MenuEvent::<Start>b__70_1()
extern void MenuEvent_U3CStartU3Eb__70_1_mD655DDC3E8F8F4CD95EFF1D8C41856D471166648 ();
// 0x000000AF System.Void MenuEvent::<Start>b__70_2()
extern void MenuEvent_U3CStartU3Eb__70_2_m9F997FFF6741515EA512AF2673B3D6F4F55EAB5F ();
// 0x000000B0 System.Void MenuEvent::<Start>b__70_3()
extern void MenuEvent_U3CStartU3Eb__70_3_m88EC6D0DB41CC79BB3B7011A99E7BC39EF06F821 ();
// 0x000000B1 System.Void MenuEvent::<Start>b__70_4()
extern void MenuEvent_U3CStartU3Eb__70_4_m2F3F540B469725844101DA2026B628B5B09E0A77 ();
// 0x000000B2 System.Void MenuEvent::<Start>b__70_5()
extern void MenuEvent_U3CStartU3Eb__70_5_mEBD546E15871282556EA49085F7EF4BE72492837 ();
// 0x000000B3 System.Void MenuEvent::<Start>b__70_6()
extern void MenuEvent_U3CStartU3Eb__70_6_mB67D00F6C359A67D59F38E65E02BF6E1BB8CF14D ();
// 0x000000B4 System.Void MenuEvent::<Start>b__70_7()
extern void MenuEvent_U3CStartU3Eb__70_7_m712294EC31BB94AAE54E6A742A73F2ED84709498 ();
// 0x000000B5 System.Void MenuEvent::<Start>b__70_8()
extern void MenuEvent_U3CStartU3Eb__70_8_m9CBC81DCA92B3B4497343281B6EAC17D829BC5CB ();
// 0x000000B6 System.Void MenuEvent::<Start>b__70_9()
extern void MenuEvent_U3CStartU3Eb__70_9_m49B008F6B22AEC5CCF559009D977B6F89F8134EB ();
// 0x000000B7 System.Void MenuEvent::<Start>b__70_10()
extern void MenuEvent_U3CStartU3Eb__70_10_mA7F9D40B3E87F5B912AB74804C237D8C3275B0D0 ();
// 0x000000B8 System.Void MenuEvent::<Start>b__70_11()
extern void MenuEvent_U3CStartU3Eb__70_11_mC6CBA197447EF5DA1C928238261A6022218213C7 ();
// 0x000000B9 System.Void MenuEvent::<Start>b__70_12()
extern void MenuEvent_U3CStartU3Eb__70_12_m634429FC960D2DF1D52AA5480A5862A8CFB430F4 ();
// 0x000000BA System.Void MenuEvent::<Start>b__70_13()
extern void MenuEvent_U3CStartU3Eb__70_13_m7699DD436676C2B936E59DC662D69E8B23C237EA ();
// 0x000000BB System.Void MenuEvent::<Start>b__70_14()
extern void MenuEvent_U3CStartU3Eb__70_14_mA90793F8E92C6E75E53CB9D3D3FF5CB5D388929F ();
// 0x000000BC System.Void MenuEvent::<Start>b__70_15()
extern void MenuEvent_U3CStartU3Eb__70_15_m6E770A93865FBA2CD186F4565582324A873DDC3F ();
// 0x000000BD System.Void MenuEvent::<Start>b__70_16()
extern void MenuEvent_U3CStartU3Eb__70_16_m0161EB1317B32EA4740F7F89571FAFB60EEE9F5C ();
// 0x000000BE System.Void MenuEvent::<Start>b__70_17()
extern void MenuEvent_U3CStartU3Eb__70_17_m833D6FC81410AA6E09DCEAB21DF2F6EEFBA0366B ();
// 0x000000BF System.Void MenuEvent::<Start>b__70_18()
extern void MenuEvent_U3CStartU3Eb__70_18_mC6880A6640A34A76953067EAD1542805B260268B ();
// 0x000000C0 System.Void MenuEvent::<Start>b__70_19()
extern void MenuEvent_U3CStartU3Eb__70_19_m06E82AC33FCD21947E8406A66F3CD8174404F30A ();
// 0x000000C1 System.Void MenuEvent::<Start>b__70_20()
extern void MenuEvent_U3CStartU3Eb__70_20_m26B16B899776309A250DB0CF79F29826323CA353 ();
// 0x000000C2 System.Void MenuEvent::<Start>b__70_21()
extern void MenuEvent_U3CStartU3Eb__70_21_m05899A6711696D9017C0CC93081EB2CF29AB0AF3 ();
// 0x000000C3 System.Void MenuEvent::<Start>b__70_22()
extern void MenuEvent_U3CStartU3Eb__70_22_mF6810A2F5E1C11564B8968C1A34247E3137694C4 ();
// 0x000000C4 System.Void MenuEvent::<Start>b__70_23()
extern void MenuEvent_U3CStartU3Eb__70_23_mF5C4354A334CF40983EA552746117E6A2608D799 ();
// 0x000000C5 System.Void MenuEvent::<Start>b__70_24()
extern void MenuEvent_U3CStartU3Eb__70_24_mD23001AE41528AF168E5A439AA2CCD3C7CF7CA92 ();
// 0x000000C6 System.Void MenuEvent::<Start>b__70_25()
extern void MenuEvent_U3CStartU3Eb__70_25_mB1F0EA65A7B7E7842D20720F18DE7BDFC9B934E7 ();
// 0x000000C7 System.Void MenuEvent::<Start>b__70_26()
extern void MenuEvent_U3CStartU3Eb__70_26_m0610FB107D38CDD7E38B62C90FFCCDC89827D781 ();
// 0x000000C8 System.String AssestBundelModel::get_GameObject()
extern void AssestBundelModel_get_GameObject_mA0586A05195C2BCC55D8D297206D14ECC18FBA65 ();
// 0x000000C9 System.Void AssestBundelModel::set_GameObject(System.String)
extern void AssestBundelModel_set_GameObject_m5BD1340C646AC0362F8220C599BF238DAD4182C7 ();
// 0x000000CA System.String AssestBundelModel::get_AssmClassName()
extern void AssestBundelModel_get_AssmClassName_mF6BF78ED3C1C3EC9D76BA03E9C53FF9CABE72AC7 ();
// 0x000000CB System.Void AssestBundelModel::set_AssmClassName(System.String)
extern void AssestBundelModel_set_AssmClassName_m7E74D7EF3D145A5E8BB1AC2EB754B6A9149E6C99 ();
// 0x000000CC System.Boolean AssestBundelModel::get_Paremeters()
extern void AssestBundelModel_get_Paremeters_mC574C5814A8B36C0BC4A4BA426EB142F805C528C ();
// 0x000000CD System.Void AssestBundelModel::set_Paremeters(System.Boolean)
extern void AssestBundelModel_set_Paremeters_m4096E7888F8294482EA9DC9ACCBA006CE2175235 ();
// 0x000000CE System.Collections.Generic.List`1<GameParam> AssestBundelModel::get_GameParam()
extern void AssestBundelModel_get_GameParam_m4837D1DEB27266135FECFD3F31225D5E9EA5D5DA ();
// 0x000000CF System.Void AssestBundelModel::set_GameParam(System.Collections.Generic.List`1<GameParam>)
extern void AssestBundelModel_set_GameParam_m52F9B095BA9606EFB20BECBAE20563269DF2F251 ();
// 0x000000D0 System.Void AssestBundelModel::.ctor()
extern void AssestBundelModel__ctor_m14BD98C34415AC83BB6C1CAEDCCD214A03DEFA43 ();
// 0x000000D1 System.Boolean GameParam::get_Inactive()
extern void GameParam_get_Inactive_mCF1982D03738084791B649D134FEAC28A0C64670 ();
// 0x000000D2 System.Void GameParam::set_Inactive(System.Boolean)
extern void GameParam_set_Inactive_m48C162103C3581F317E4BECCBF4B5E3A9EAAC648 ();
// 0x000000D3 System.String GameParam::get_ChildGameObject()
extern void GameParam_get_ChildGameObject_m7B9D79CFA45A5E1E5876D6ACBE3CC1ED8698BF15 ();
// 0x000000D4 System.Void GameParam::set_ChildGameObject(System.String)
extern void GameParam_set_ChildGameObject_mFEBA659802E5C533AA67DDBBF0A8BEFA6BBC3F46 ();
// 0x000000D5 System.String GameParam::get_ParentObject()
extern void GameParam_get_ParentObject_mA0007BB2A579F957FE43207933E1C8E471B6740B ();
// 0x000000D6 System.Void GameParam::set_ParentObject(System.String)
extern void GameParam_set_ParentObject_m3BB06A11DD32C0985A2F4DB92DB89F9C0F41DA70 ();
// 0x000000D7 System.String GameParam::get_FiledObject()
extern void GameParam_get_FiledObject_m5EA622FCE6C9CEA56C4EF5B81B31F68591C50436 ();
// 0x000000D8 System.Void GameParam::set_FiledObject(System.String)
extern void GameParam_set_FiledObject_mABACE95C867E90DB028A6180B20465E5E04C0A50 ();
// 0x000000D9 System.Void GameParam::.ctor()
extern void GameParam__ctor_m34A1F322F9219F37F0B3A909559F61AE84A11228 ();
// 0x000000DA System.String BarChartContent::get_CategoryName()
extern void BarChartContent_get_CategoryName_m613A1D630D02B4D67808243318857D6F53FECBBC ();
// 0x000000DB System.Void BarChartContent::set_CategoryName(System.String)
extern void BarChartContent_set_CategoryName_mB5999C16C259BC623092141D25B4E5A7221CD266 ();
// 0x000000DC System.String BarChartContent::get_CategoryValue()
extern void BarChartContent_get_CategoryValue_m63FF3914A648FFA36B8EAC49731D48629ACF82CC ();
// 0x000000DD System.Void BarChartContent::set_CategoryValue(System.String)
extern void BarChartContent_set_CategoryValue_mE4DB659D2B3089A6275DAF1BADABFF99A53D6997 ();
// 0x000000DE System.Void BarChartContent::.ctor()
extern void BarChartContent__ctor_mB91A5B552645B1419CA247528DB5D66F1D549005 ();
// 0x000000DF System.String BarChartGroup::get_GroupName()
extern void BarChartGroup_get_GroupName_mEB70A208C2795E30EEDBD3E672DA7ADD1BB90A40 ();
// 0x000000E0 System.Void BarChartGroup::set_GroupName(System.String)
extern void BarChartGroup_set_GroupName_m582421E5B6A2A987EF138A52CFF95E65C35651B8 ();
// 0x000000E1 System.Collections.Generic.List`1<Header> BarChartGroup::get_Header()
extern void BarChartGroup_get_Header_mA32B0D256E8CD6FCEF8FDD5C58CC3085F86385D3 ();
// 0x000000E2 System.Void BarChartGroup::set_Header(System.Collections.Generic.List`1<Header>)
extern void BarChartGroup_set_Header_m7E3B25AC30263C20EF83BBFFD1C5F3EF4D1E26C7 ();
// 0x000000E3 System.Collections.Generic.List`1<BarChartContent> BarChartGroup::get_Body()
extern void BarChartGroup_get_Body_m2A4B6BE1F5A321EE2B2E13AB4816DBACEC597CAA ();
// 0x000000E4 System.Void BarChartGroup::set_Body(System.Collections.Generic.List`1<BarChartContent>)
extern void BarChartGroup_set_Body_mFDECD26AD4F3079298DB154AACC8A6E9B8643D97 ();
// 0x000000E5 System.Void BarChartGroup::.ctor()
extern void BarChartGroup__ctor_m0F9C20905C048E1FC199384334B35D968E4D5290 ();
// 0x000000E6 System.Collections.Generic.List`1<BarChartGroup> BarChartDataModel::get_BarChartGroup()
extern void BarChartDataModel_get_BarChartGroup_m0D5C2B8953DFB0F66DA91DC6420EC470E275EC97 ();
// 0x000000E7 System.Void BarChartDataModel::set_BarChartGroup(System.Collections.Generic.List`1<BarChartGroup>)
extern void BarChartDataModel_set_BarChartGroup_m3FF612CA997F9361293827D4F5A3ECA4397CB12A ();
// 0x000000E8 System.Void BarChartDataModel::.ctor()
extern void BarChartDataModel__ctor_m80D876EC59DE3A6D875AE75C4BB458AB79C06F61 ();
// 0x000000E9 System.String RoomObjectStorageModel::get_storageProvider()
extern void RoomObjectStorageModel_get_storageProvider_m1CA5818CD9ACE01AAF2BAD0935FBA3E6DEEE2FBA ();
// 0x000000EA System.Void RoomObjectStorageModel::set_storageProvider(System.String)
extern void RoomObjectStorageModel_set_storageProvider_m46C460A0FB7A1CF3E0558B1675A109891619E118 ();
// 0x000000EB System.String RoomObjectStorageModel::get_storage_id()
extern void RoomObjectStorageModel_get_storage_id_m1003A84CCE8029661DE4CB91FFA33D4D71C041B3 ();
// 0x000000EC System.Void RoomObjectStorageModel::set_storage_id(System.String)
extern void RoomObjectStorageModel_set_storage_id_m3CD1ECF35688E5277B641D19D458218602F50F32 ();
// 0x000000ED System.String RoomObjectStorageModel::get_viewerstorage_id()
extern void RoomObjectStorageModel_get_viewerstorage_id_m9B0F72A1AE63CA0C149B81891B529888A74E2991 ();
// 0x000000EE System.Void RoomObjectStorageModel::set_viewerstorage_id(System.String)
extern void RoomObjectStorageModel_set_viewerstorage_id_mBA49385709CB9B9945F625712C712E4DCDD58D6B ();
// 0x000000EF System.String RoomObjectStorageModel::get_filename()
extern void RoomObjectStorageModel_get_filename_mD687DB3AAC9D112E51470FF693E01B3E3EA1B311 ();
// 0x000000F0 System.Void RoomObjectStorageModel::set_filename(System.String)
extern void RoomObjectStorageModel_set_filename_m586C6B4B824CF52B49D8E2F1005A852139D0AB75 ();
// 0x000000F1 System.String RoomObjectStorageModel::get_fileid()
extern void RoomObjectStorageModel_get_fileid_mA420FB67B4B74C4B27C0D3C9C2133AA94FFFC964 ();
// 0x000000F2 System.Void RoomObjectStorageModel::set_fileid(System.String)
extern void RoomObjectStorageModel_set_fileid_mEF68FC116D4DEBC9D2D98B75B52152FD49560BF8 ();
// 0x000000F3 System.String RoomObjectStorageModel::get_fileurl()
extern void RoomObjectStorageModel_get_fileurl_mD8DCCBCE210DD9FC6C5EE59A16C69407A018A02A ();
// 0x000000F4 System.Void RoomObjectStorageModel::set_fileurl(System.String)
extern void RoomObjectStorageModel_set_fileurl_mA0ED9539805FCECD38D5ADE5A722A29A6447172A ();
// 0x000000F5 System.String RoomObjectStorageModel::get_filesize()
extern void RoomObjectStorageModel_get_filesize_m86C7789299680CFF589D069F75B7E6EF9AA9F181 ();
// 0x000000F6 System.Void RoomObjectStorageModel::set_filesize(System.String)
extern void RoomObjectStorageModel_set_filesize_m95B5F0E59F3B6032E6C850482CAD68D9295B1586 ();
// 0x000000F7 System.String RoomObjectStorageModel::get_objectType()
extern void RoomObjectStorageModel_get_objectType_m3D60615CE7585C67D22BFFFE73EF2CE21518ED03 ();
// 0x000000F8 System.Void RoomObjectStorageModel::set_objectType(System.String)
extern void RoomObjectStorageModel_set_objectType_m3EC90A5ECFAAB51A9952F22680A52619331A4E0B ();
// 0x000000F9 System.DateTime RoomObjectStorageModel::get_created_at()
extern void RoomObjectStorageModel_get_created_at_mA037D465BB8F2CCBEEE54EADE5BE6ADF9F3C5AEE ();
// 0x000000FA System.Void RoomObjectStorageModel::set_created_at(System.DateTime)
extern void RoomObjectStorageModel_set_created_at_mB0889EBD92C7797386538ADB2FD0D77352CE6871 ();
// 0x000000FB System.String RoomObjectStorageModel::get_created_by()
extern void RoomObjectStorageModel_get_created_by_mAD693807C3E64520102CF5BBFFDF8E0940324BFB ();
// 0x000000FC System.Void RoomObjectStorageModel::set_created_by(System.String)
extern void RoomObjectStorageModel_set_created_by_m43F68C34E835B7DD14FE3B472248BAFC35830257 ();
// 0x000000FD System.String RoomObjectStorageModel::get_id()
extern void RoomObjectStorageModel_get_id_m46CF807F195DC0342B2F139A6CE6E899FE64D58F ();
// 0x000000FE System.Void RoomObjectStorageModel::set_id(System.String)
extern void RoomObjectStorageModel_set_id_mE2019C45E5C1E6C9F64A0177346CB062344AC871 ();
// 0x000000FF System.String RoomObjectStorageModel::get_org_id()
extern void RoomObjectStorageModel_get_org_id_m484471F1366BBADA2BF07C120CA300F136AE0292 ();
// 0x00000100 System.Void RoomObjectStorageModel::set_org_id(System.String)
extern void RoomObjectStorageModel_set_org_id_m7E27F0BA528EC392160B1D5A89C16A6CDF4496AE ();
// 0x00000101 System.Void RoomObjectStorageModel::.ctor()
extern void RoomObjectStorageModel__ctor_m0B937CABF1EEC21B780BF0D769EBDC1BB52953C0 ();
// 0x00000102 System.String ViewerSettingModel::get_storage_id()
extern void ViewerSettingModel_get_storage_id_m5FF12F90C2B4B422EB40A9A9196256E541861ABF ();
// 0x00000103 System.Void ViewerSettingModel::set_storage_id(System.String)
extern void ViewerSettingModel_set_storage_id_mB97D2B69F4C0516ABB1D9DCC1BE376AA74D3E4CC ();
// 0x00000104 System.String ViewerSettingModel::get_org_id()
extern void ViewerSettingModel_get_org_id_m7E0604510778185605A2688FE2D4A830B9155630 ();
// 0x00000105 System.Void ViewerSettingModel::set_org_id(System.String)
extern void ViewerSettingModel_set_org_id_mBCAF5D5D365ACCC5FF3F30F1FABC1EEF898076FD ();
// 0x00000106 System.String ViewerSettingModel::get_storageProvider()
extern void ViewerSettingModel_get_storageProvider_mBCD5F7D1D2F4F46695C71420AEC30A1C33570656 ();
// 0x00000107 System.Void ViewerSettingModel::set_storageProvider(System.String)
extern void ViewerSettingModel_set_storageProvider_m60859E4979426C7929A82548D0CD819A9B9D933F ();
// 0x00000108 System.String ViewerSettingModel::get_parentFolder()
extern void ViewerSettingModel_get_parentFolder_m29738338B5A05DB36746546225794E7EFF8CAE11 ();
// 0x00000109 System.Void ViewerSettingModel::set_parentFolder(System.String)
extern void ViewerSettingModel_set_parentFolder_mAA7EE034EF5E4128DC2A830DC3B98BB7A4B1FA58 ();
// 0x0000010A System.String ViewerSettingModel::get_accessToken()
extern void ViewerSettingModel_get_accessToken_m60EE5898BDC146F7C5627436F19E9625650D6CAB ();
// 0x0000010B System.Void ViewerSettingModel::set_accessToken(System.String)
extern void ViewerSettingModel_set_accessToken_m91BF7DC0260CB0730EF96FE3EE47771C9C301DFC ();
// 0x0000010C System.String ViewerSettingModel::get_folderId()
extern void ViewerSettingModel_get_folderId_m45730235A55C5ABCF7DAE5B6227229F4FDC7DDFA ();
// 0x0000010D System.Void ViewerSettingModel::set_folderId(System.String)
extern void ViewerSettingModel_set_folderId_mA616A0D06AF8BAC9CF99BD39422A73C422E4C00A ();
// 0x0000010E System.String ViewerSettingModel::get_created_by()
extern void ViewerSettingModel_get_created_by_mD373B28BADF441C5C853B9859D761714F0939777 ();
// 0x0000010F System.Void ViewerSettingModel::set_created_by(System.String)
extern void ViewerSettingModel_set_created_by_m98D269C55BECDD53FDB4F4BF9ADF0FD3BA7748DC ();
// 0x00000110 System.DateTime ViewerSettingModel::get_created_at()
extern void ViewerSettingModel_get_created_at_m8C5E5869D0457F51DAF3D59B2B5507B287AA0A0C ();
// 0x00000111 System.Void ViewerSettingModel::set_created_at(System.DateTime)
extern void ViewerSettingModel_set_created_at_mC51A4BD76DDD3D977E284F9845CBD69B4125F883 ();
// 0x00000112 System.String ViewerSettingModel::get_id()
extern void ViewerSettingModel_get_id_m6AFDCC3C2DE3356B7513412FE9ED1A091A7DC37F ();
// 0x00000113 System.Void ViewerSettingModel::set_id(System.String)
extern void ViewerSettingModel_set_id_m74E3E6386224FDCECB61A080FE4CE1DDC46C7F87 ();
// 0x00000114 System.Void ViewerSettingModel::.ctor()
extern void ViewerSettingModel__ctor_m4F670AF27F003B0C4402BA87122DBE31F86DD115 ();
// 0x00000115 System.String DownloadMetaModel::get_name()
extern void DownloadMetaModel_get_name_mABE90A7DA5D43B995DAAEBA288E25EE35C6EDBF2 ();
// 0x00000116 System.Void DownloadMetaModel::set_name(System.String)
extern void DownloadMetaModel_set_name_mAC0B435E2D933D80639C71D229F035F37421ADC0 ();
// 0x00000117 System.String DownloadMetaModel::get_path_lower()
extern void DownloadMetaModel_get_path_lower_m176B951A51057FA35D787ABB29F92C9F2F914A91 ();
// 0x00000118 System.Void DownloadMetaModel::set_path_lower(System.String)
extern void DownloadMetaModel_set_path_lower_m1E334C8C557B19D2E71D1174179DA82F5419E701 ();
// 0x00000119 System.String DownloadMetaModel::get_path_display()
extern void DownloadMetaModel_get_path_display_m4E6953A3C5BA372E993E8044F75FD047A768636E ();
// 0x0000011A System.Void DownloadMetaModel::set_path_display(System.String)
extern void DownloadMetaModel_set_path_display_m046532BCCBBFB99834FBC36D3C95F65D3B60B34A ();
// 0x0000011B System.String DownloadMetaModel::get_id()
extern void DownloadMetaModel_get_id_mBAC4DCC0DC075C32DD32AC431DC5E277D6F7E05A ();
// 0x0000011C System.Void DownloadMetaModel::set_id(System.String)
extern void DownloadMetaModel_set_id_m3A9D8D825AD2FB671DF3AF14399804ED9258DBA2 ();
// 0x0000011D System.DateTime DownloadMetaModel::get_client_modified()
extern void DownloadMetaModel_get_client_modified_m31F1D5149328B73930FF687C4075AAC0FF578C5B ();
// 0x0000011E System.Void DownloadMetaModel::set_client_modified(System.DateTime)
extern void DownloadMetaModel_set_client_modified_mF849850AD0DD621372412B183C90B3449E12239D ();
// 0x0000011F System.DateTime DownloadMetaModel::get_server_modified()
extern void DownloadMetaModel_get_server_modified_m02DEA2AA17E2A60847A5319BB6620E11BD3B878D ();
// 0x00000120 System.Void DownloadMetaModel::set_server_modified(System.DateTime)
extern void DownloadMetaModel_set_server_modified_m61E59D680F492769AFF9A073E858B23FEC14AD46 ();
// 0x00000121 System.String DownloadMetaModel::get_rev()
extern void DownloadMetaModel_get_rev_m5C32E9F54AAE0211BDE95194F451E89984EA5A3B ();
// 0x00000122 System.Void DownloadMetaModel::set_rev(System.String)
extern void DownloadMetaModel_set_rev_mD9511068D9F92AF13AA4D3A347BD89A47E6D564F ();
// 0x00000123 System.Int32 DownloadMetaModel::get_size()
extern void DownloadMetaModel_get_size_m34AD60D28499449BE541C501F7EF78D38BC6AC98 ();
// 0x00000124 System.Void DownloadMetaModel::set_size(System.Int32)
extern void DownloadMetaModel_set_size_mA60574CA8A9067234368E5EB624C6D424A96B0F5 ();
// 0x00000125 System.Boolean DownloadMetaModel::get_is_downloadable()
extern void DownloadMetaModel_get_is_downloadable_mFA5A538D2F613D0B1A000E0CACCC4D000051F008 ();
// 0x00000126 System.Void DownloadMetaModel::set_is_downloadable(System.Boolean)
extern void DownloadMetaModel_set_is_downloadable_m8885F0E20CF3AEEF6A8A3D5A225EC0DBACD06A2D ();
// 0x00000127 System.String DownloadMetaModel::get_content_hash()
extern void DownloadMetaModel_get_content_hash_m50C372C335F364EE9D79E6EEFAAF196178AAD210 ();
// 0x00000128 System.Void DownloadMetaModel::set_content_hash(System.String)
extern void DownloadMetaModel_set_content_hash_m4C01DB8907A44447836D14791C3399D51BCA5136 ();
// 0x00000129 System.Void DownloadMetaModel::.ctor()
extern void DownloadMetaModel__ctor_m4C093A4395CB69C8F36C4AC6A89D0C95DDCEC94D ();
// 0x0000012A DownloadMetaModel DownloadModel::get_metadata()
extern void DownloadModel_get_metadata_mBEDFEA2C4DDCEE8F79AC1166B632959C1339940A ();
// 0x0000012B System.Void DownloadModel::set_metadata(DownloadMetaModel)
extern void DownloadModel_set_metadata_m8D49A26E682EB5E8588760B3BEF6EA0B50356E2B ();
// 0x0000012C System.String DownloadModel::get_link()
extern void DownloadModel_get_link_m1B0640698BF0A6215EBED923FC78F3A63BE4FEE0 ();
// 0x0000012D System.Void DownloadModel::set_link(System.String)
extern void DownloadModel_set_link_m2F31EDC77E92499DF20C4C860C447488C3F0F735 ();
// 0x0000012E System.Void DownloadModel::.ctor()
extern void DownloadModel__ctor_m119FAD86C6584F3D904B18C6BD9A60EEBEF31498 ();
// 0x0000012F System.Void DropBoxItems::.ctor()
extern void DropBoxItems__ctor_m6F17FE7E405A3D5DF4FE540BCB279CF14C1C814F ();
// 0x00000130 System.Void entries::.ctor()
extern void entries__ctor_mB56E141F3276D5D1091ED4900C7348E54E662BC8 ();
// 0x00000131 System.String EnvironmentModel::get_name()
extern void EnvironmentModel_get_name_m3046CDDBE1ABB242361DB77F4E6A4CD6F5FD33D4 ();
// 0x00000132 System.Void EnvironmentModel::set_name(System.String)
extern void EnvironmentModel_set_name_m1123B41F3809DC0F9D5F7674728F1D3DA536431C ();
// 0x00000133 System.String EnvironmentModel::get_description()
extern void EnvironmentModel_get_description_m3E02CD14864985F7956F3F082531622EDDEB3D48 ();
// 0x00000134 System.Void EnvironmentModel::set_description(System.String)
extern void EnvironmentModel_set_description_mC6B9FF729B922FF44705FB7D11010D4BA2A35C6B ();
// 0x00000135 System.String EnvironmentModel::get_environmentType()
extern void EnvironmentModel_get_environmentType_mE4C28338828410A6EBDD1236B5FE1F1FB9F51A11 ();
// 0x00000136 System.Void EnvironmentModel::set_environmentType(System.String)
extern void EnvironmentModel_set_environmentType_m7427326FF33DD148607385155D3DA2133D028F38 ();
// 0x00000137 System.String EnvironmentModel::get_objectName()
extern void EnvironmentModel_get_objectName_mBF28FE63217BB63F5CBF8C4A4C6C2D353CC9E536 ();
// 0x00000138 System.Void EnvironmentModel::set_objectName(System.String)
extern void EnvironmentModel_set_objectName_mAF8BBE0ABB0F22B3684F3491644E70A470EC54C2 ();
// 0x00000139 System.String EnvironmentModel::get_viewerobject_id()
extern void EnvironmentModel_get_viewerobject_id_m4EE43F7DB83A8203A8F766EFDFCC806EF1D33F02 ();
// 0x0000013A System.Void EnvironmentModel::set_viewerobject_id(System.String)
extern void EnvironmentModel_set_viewerobject_id_m06908F3DD945AEE5423A0DC8455B60D7D7460C92 ();
// 0x0000013B System.DateTime EnvironmentModel::get_created_at()
extern void EnvironmentModel_get_created_at_m7C45D6A75F1D40B5B3152D6620C0BA2E31837B25 ();
// 0x0000013C System.Void EnvironmentModel::set_created_at(System.DateTime)
extern void EnvironmentModel_set_created_at_m275CE19B9209F4111CA45D786E7B26598CC9DF40 ();
// 0x0000013D System.String EnvironmentModel::get_created_by()
extern void EnvironmentModel_get_created_by_mB4C2CB6F2EB01A8772B88DD0242AF761CB2A8B11 ();
// 0x0000013E System.Void EnvironmentModel::set_created_by(System.String)
extern void EnvironmentModel_set_created_by_m9D23B0550072C5F4657B208EC21B458154622121 ();
// 0x0000013F System.String EnvironmentModel::get_id()
extern void EnvironmentModel_get_id_m9280FF39A52776916B2DB67C7E3705716A6428A4 ();
// 0x00000140 System.Void EnvironmentModel::set_id(System.String)
extern void EnvironmentModel_set_id_m23F84B2FE75A2D427AD0FC4C873367FB450E03AC ();
// 0x00000141 System.Void EnvironmentModel::.ctor()
extern void EnvironmentModel__ctor_m080F96F2809FD2C197BCE2C0C8F34930A4CED291 ();
// 0x00000142 System.String LobbyList::get_roomName()
extern void LobbyList_get_roomName_mE506E6A89C62857EFF0642D0B6DB754F76183D3A ();
// 0x00000143 System.Void LobbyList::set_roomName(System.String)
extern void LobbyList_set_roomName_m648CE486D8535E542E2136BEDDB67A79864E26F6 ();
// 0x00000144 System.String LobbyList::get_hostName()
extern void LobbyList_get_hostName_mC747CD3DC35F1386F98610300017FD1F9618E6A3 ();
// 0x00000145 System.Void LobbyList::set_hostName(System.String)
extern void LobbyList_set_hostName_m27B52358B08EC8F61A42EE5F3D069CE7B8D723E5 ();
// 0x00000146 System.String LobbyList::get_communicationType()
extern void LobbyList_get_communicationType_m5A9F5DFC2AF3B2E41759F8F2B9C9ACB5FB8FB3D4 ();
// 0x00000147 System.Void LobbyList::set_communicationType(System.String)
extern void LobbyList_set_communicationType_m6D3EE250C3A32857EC9B8B5404290304B27AA4F6 ();
// 0x00000148 System.String LobbyList::get_audioOptions()
extern void LobbyList_get_audioOptions_mEA18EAE908B67EEBD80BCEFEAC19FC7D9DBF7365 ();
// 0x00000149 System.Void LobbyList::set_audioOptions(System.String)
extern void LobbyList_set_audioOptions_m3E325A0A829DA8A128D8431F650AADFFF305DE27 ();
// 0x0000014A System.Boolean LobbyList::get_requirdMeetingPassword()
extern void LobbyList_get_requirdMeetingPassword_m0901280C5D35C4B8CD0F4E45D4F10FEDE32218BE ();
// 0x0000014B System.Void LobbyList::set_requirdMeetingPassword(System.Boolean)
extern void LobbyList_set_requirdMeetingPassword_m7829C8036B4F79F28B9455A0072E7B5492DD3194 ();
// 0x0000014C System.Boolean LobbyList::get_enableJoinBeforeHost()
extern void LobbyList_get_enableJoinBeforeHost_m358108D04BE04E5A438C70223A4FAEB4259B1A60 ();
// 0x0000014D System.Void LobbyList::set_enableJoinBeforeHost(System.Boolean)
extern void LobbyList_set_enableJoinBeforeHost_m44819E871DC787562EEE3D9F20CB859842C6D7EC ();
// 0x0000014E System.Boolean LobbyList::get_hostVideo()
extern void LobbyList_get_hostVideo_m88F260B640ABC750AB57AC12A67E7B253DD521AA ();
// 0x0000014F System.Void LobbyList::set_hostVideo(System.Boolean)
extern void LobbyList_set_hostVideo_mC71D0AF405A2EBF5C38949468D66E85A5EED1965 ();
// 0x00000150 System.Boolean LobbyList::get_participantsVideo()
extern void LobbyList_get_participantsVideo_m342D72DF89618B2C494B1CE5487233C16B0C2AF1 ();
// 0x00000151 System.Void LobbyList::set_participantsVideo(System.Boolean)
extern void LobbyList_set_participantsVideo_mFFF791BD193C4010F329A4BE997A79F31F77E1B7 ();
// 0x00000152 System.Boolean LobbyList::get_muteParticipants()
extern void LobbyList_get_muteParticipants_m59258C45228F0B2B17A8C686213B01CC9A4C714A ();
// 0x00000153 System.Void LobbyList::set_muteParticipants(System.Boolean)
extern void LobbyList_set_muteParticipants_m7D4BBF862B0DC4290335AFF5BAE5B1FED30F4A85 ();
// 0x00000154 System.Boolean LobbyList::get_usePersonalMeetingId()
extern void LobbyList_get_usePersonalMeetingId_mA9454A5A33C881EA45AB73A50E4677305DAEA1C4 ();
// 0x00000155 System.Void LobbyList::set_usePersonalMeetingId(System.Boolean)
extern void LobbyList_set_usePersonalMeetingId_m9D19CA5B1C8ABC2363338F0FA6240182190109BA ();
// 0x00000156 System.String LobbyList::get_recordType()
extern void LobbyList_get_recordType_mA50145971694FDACA5DF7B58396744557C6C3CED ();
// 0x00000157 System.Void LobbyList::set_recordType(System.String)
extern void LobbyList_set_recordType_mDF9006465345A88911D306818AF6B3E0B802E7C3 ();
// 0x00000158 System.Boolean LobbyList::get_defaultRecordEnable()
extern void LobbyList_get_defaultRecordEnable_m8EAEA5F964AB6C0D4DF80ACFD33471B3D24562EC ();
// 0x00000159 System.Void LobbyList::set_defaultRecordEnable(System.Boolean)
extern void LobbyList_set_defaultRecordEnable_m7BEE274CA616FFC56932EEE5E78F879E98E4F72B ();
// 0x0000015A System.String LobbyList::get_recordFormat()
extern void LobbyList_get_recordFormat_m6CE8434D3D9F179C23F41BFFDE6ED4F46BE76B36 ();
// 0x0000015B System.Void LobbyList::set_recordFormat(System.String)
extern void LobbyList_set_recordFormat_m8E817FA03F75C3B4F73417C24D4376D6041FCD99 ();
// 0x0000015C System.String LobbyList::get_mediaStorage()
extern void LobbyList_get_mediaStorage_mA022B1979A4EB47F702E9E35FDDDD2E05872A257 ();
// 0x0000015D System.Void LobbyList::set_mediaStorage(System.String)
extern void LobbyList_set_mediaStorage_mA7687AAEEE07614211373109CE49336350693BA9 ();
// 0x0000015E System.String LobbyList::get_mediaFile()
extern void LobbyList_get_mediaFile_mE53FE7503F2927813C3402167E4FACEEF951DADB ();
// 0x0000015F System.Void LobbyList::set_mediaFile(System.String)
extern void LobbyList_set_mediaFile_mC822526F1A8D629AD014BB2F7769EB2633CDBEF5 ();
// 0x00000160 System.String LobbyList::get_calenderType()
extern void LobbyList_get_calenderType_mCB9F9C57DF71FDE721302C3302AABE361CD08E45 ();
// 0x00000161 System.Void LobbyList::set_calenderType(System.String)
extern void LobbyList_set_calenderType_m4D0DD97B95E1EBA778C8CC4F3AF4BC0BD625509A ();
// 0x00000162 System.String LobbyList::get_id()
extern void LobbyList_get_id_m7DA2F99DFA46232D96440FCBB06DB7F8078EBD00 ();
// 0x00000163 System.Void LobbyList::set_id(System.String)
extern void LobbyList_set_id_m7A83DCFE90A2DFE82D5BEC66E7DECCDC46F19AC2 ();
// 0x00000164 System.String LobbyList::get_events_id()
extern void LobbyList_get_events_id_m14D313B42084645D3ED403249D53D6068E3A2D34 ();
// 0x00000165 System.Void LobbyList::set_events_id(System.String)
extern void LobbyList_set_events_id_mCD2A60A404DC92AA083FF8486FB861485522F729 ();
// 0x00000166 System.Void LobbyList::.ctor()
extern void LobbyList__ctor_m59D8AB49DCAF636B95D1EA100A3F9F5910EE05B0 ();
// 0x00000167 System.Void ModeTypeModel::.ctor()
extern void ModeTypeModel__ctor_mC0B6155E0A637C05B0691B9713DABDA0CF7FF249 ();
// 0x00000168 System.Single ObjectQuaternion::get_X()
extern void ObjectQuaternion_get_X_mC44A3D7B4C5E54DC463A6A7494E6F6871F1BDE11 ();
// 0x00000169 System.Void ObjectQuaternion::set_X(System.Single)
extern void ObjectQuaternion_set_X_m1861DCDFBC6912F7D3405717501D7EFD90F09014 ();
// 0x0000016A System.Single ObjectQuaternion::get_y()
extern void ObjectQuaternion_get_y_m874955519CD23CD1745874F57119A0EE7AD6F36A ();
// 0x0000016B System.Void ObjectQuaternion::set_y(System.Single)
extern void ObjectQuaternion_set_y_m9ADF2283319F849DC30C568B1F873EE4209EC006 ();
// 0x0000016C System.Single ObjectQuaternion::get_z()
extern void ObjectQuaternion_get_z_m7A1F58E7BDE89B779044AD1BD6B00D6BD7C3E8CE ();
// 0x0000016D System.Void ObjectQuaternion::set_z(System.Single)
extern void ObjectQuaternion_set_z_mCC0D7ABEEBEAF1D04D92007763ADC7F0E59D9B62 ();
// 0x0000016E System.Single ObjectQuaternion::get_w()
extern void ObjectQuaternion_get_w_m73B5527B214B6B51D4C86561CA4D110CB5DEDD26 ();
// 0x0000016F System.Void ObjectQuaternion::set_w(System.Single)
extern void ObjectQuaternion_set_w_m6F9E139C0BE05C5F28B126A4A76D88736F137977 ();
// 0x00000170 System.Void ObjectQuaternion::.ctor()
extern void ObjectQuaternion__ctor_m747D17964616B8EB612FBE4BDFF266499D9B660C ();
// 0x00000171 System.Single ObjectVector::get_X()
extern void ObjectVector_get_X_mAF666378C689AA12377B9B5119DD1650B5EAD25D ();
// 0x00000172 System.Void ObjectVector::set_X(System.Single)
extern void ObjectVector_set_X_m748480B292546D367B8B4D8D59549317319CC88A ();
// 0x00000173 System.Single ObjectVector::get_y()
extern void ObjectVector_get_y_mFCB8209233BA05D005BDD2BBE3E5FED6E134A7A3 ();
// 0x00000174 System.Void ObjectVector::set_y(System.Single)
extern void ObjectVector_set_y_mB62FA04A7E044E5F9E1D6C845CAFC85990AFA7DB ();
// 0x00000175 System.Single ObjectVector::get_z()
extern void ObjectVector_get_z_m1DA8E680EEBF938716E9E3C24CA6618C0D414F25 ();
// 0x00000176 System.Void ObjectVector::set_z(System.Single)
extern void ObjectVector_set_z_m3B6B842285FF1CC1BB53ADC0ADF646479B6BD4A9 ();
// 0x00000177 System.Void ObjectVector::.ctor()
extern void ObjectVector__ctor_m7E727D8CDE06661398A636444B6F0D7D6B8FD085 ();
// 0x00000178 System.String ObjectmovementModel::get_username()
extern void ObjectmovementModel_get_username_m90BF9566A2B4016C1607B7FDCF8127083B60336F ();
// 0x00000179 System.Void ObjectmovementModel::set_username(System.String)
extern void ObjectmovementModel_set_username_mAB05BDF49C899571B5CF76AD08020C4F44100BD8 ();
// 0x0000017A System.String ObjectmovementModel::get_objectname()
extern void ObjectmovementModel_get_objectname_mD05D51988E48C30E7524E2F7BD8EED3AA991B89E ();
// 0x0000017B System.Void ObjectmovementModel::set_objectname(System.String)
extern void ObjectmovementModel_set_objectname_m21ED9D72CCFEDF3746AE18891B3C28D47404AFB9 ();
// 0x0000017C Movement ObjectmovementModel::get_roomobject()
extern void ObjectmovementModel_get_roomobject_m67D0998261C14B093DE618074F1940065B1AE638 ();
// 0x0000017D System.Void ObjectmovementModel::set_roomobject(Movement)
extern void ObjectmovementModel_set_roomobject_m335F93E3B5C0EE91B5AADFD2B356C2B25AA37AE2 ();
// 0x0000017E System.Void ObjectmovementModel::.ctor()
extern void ObjectmovementModel__ctor_m0E066813F45C73CA5282D74385C759C6947FEF68 ();
// 0x0000017F System.Single MovementPosition::get_x()
extern void MovementPosition_get_x_mF5FE14AA62FA98636BE38C069A2CCEC9B7D0F8F4 ();
// 0x00000180 System.Void MovementPosition::set_x(System.Single)
extern void MovementPosition_set_x_m8124EC724C7F87CA77093462C6FEB0616794FF53 ();
// 0x00000181 System.Single MovementPosition::get_y()
extern void MovementPosition_get_y_m13F47B746AF92E5037AA824BA05E73F389224B22 ();
// 0x00000182 System.Void MovementPosition::set_y(System.Single)
extern void MovementPosition_set_y_m02F49CFC04AAC62E5991F2F606365AA04A3B7BDC ();
// 0x00000183 System.Single MovementPosition::get_z()
extern void MovementPosition_get_z_mC9C202ECCFCCCA0BB76704B7D5AE85D0035038DF ();
// 0x00000184 System.Void MovementPosition::set_z(System.Single)
extern void MovementPosition_set_z_mF9CE734B7092DF3E461A896C594E51D83624B7C0 ();
// 0x00000185 System.Void MovementPosition::.ctor()
extern void MovementPosition__ctor_m001CAFC1C91A0A93A532A2614B8E3AF03A8BB572 ();
// 0x00000186 System.Single MovementRotation::get_x()
extern void MovementRotation_get_x_m0FC0AE79B48B7FF7A7D2E7AF45AC3A87D65724F4 ();
// 0x00000187 System.Void MovementRotation::set_x(System.Single)
extern void MovementRotation_set_x_mE4DE245FD060905851A7BF45713381F4C93E0A26 ();
// 0x00000188 System.Single MovementRotation::get_y()
extern void MovementRotation_get_y_mA307CADDFAD771D26CF516F76601E4F26229FB4D ();
// 0x00000189 System.Void MovementRotation::set_y(System.Single)
extern void MovementRotation_set_y_m892CF0D76D9DBD8A826D7EC2D1EE76CFB2EFB214 ();
// 0x0000018A System.Single MovementRotation::get_z()
extern void MovementRotation_get_z_m5DAF9485954901A457E102768480E6F5657B21FB ();
// 0x0000018B System.Void MovementRotation::set_z(System.Single)
extern void MovementRotation_set_z_mD7EEC34996CD571EE39B7A1D2C2AA8B4FF1D0D21 ();
// 0x0000018C System.Single MovementRotation::get_w()
extern void MovementRotation_get_w_m0CF906EE637BA5F4053F8B898F759B8597935AF3 ();
// 0x0000018D System.Void MovementRotation::set_w(System.Single)
extern void MovementRotation_set_w_m081BD98AE50F740BEA5DADC7CB07F74D374011C3 ();
// 0x0000018E System.Void MovementRotation::.ctor()
extern void MovementRotation__ctor_m1BA68F6EE6A3F7104486F3B66F00C608EF2F3CD9 ();
// 0x0000018F System.Single MovementScale::get_x()
extern void MovementScale_get_x_mDA1C3122082DBC1C676CB4C92E2D3C6B51947C75 ();
// 0x00000190 System.Void MovementScale::set_x(System.Single)
extern void MovementScale_set_x_m8C34A2A284FDCC501486463E249DDE57D0266BFA ();
// 0x00000191 System.Single MovementScale::get_Y()
extern void MovementScale_get_Y_mFE72795FD1E3572EC27103CD2BE58C6399E0397A ();
// 0x00000192 System.Void MovementScale::set_Y(System.Single)
extern void MovementScale_set_Y_mDBB3DC293CFC1833F74613CF6059FE507D59E898 ();
// 0x00000193 System.Single MovementScale::get_Z()
extern void MovementScale_get_Z_m68ED4D53AB22AF9B6E91FD6DB9F0934F559F898D ();
// 0x00000194 System.Void MovementScale::set_Z(System.Single)
extern void MovementScale_set_Z_m76896E3D731C14BD773F46FF672C225E94BA7BC7 ();
// 0x00000195 System.Void MovementScale::.ctor()
extern void MovementScale__ctor_mE272D142066DB226C7E62F35A36FE064D3FEE920 ();
// 0x00000196 MovementPosition Movement::get_position()
extern void Movement_get_position_mF70D81C7EC4FA8B09DE07A19B8D8F7D2A326C9E8 ();
// 0x00000197 System.Void Movement::set_position(MovementPosition)
extern void Movement_set_position_m71866B328DEC07A94A52CAF2D172E0825AFA8817 ();
// 0x00000198 MovementRotation Movement::get_rotation()
extern void Movement_get_rotation_mCC61BB00842AFE1E8D2FA8C3262B6EE69DCF98D5 ();
// 0x00000199 System.Void Movement::set_rotation(MovementRotation)
extern void Movement_set_rotation_m237048B5091C5857A299B4F5DF62BB602929323A ();
// 0x0000019A MovementScale Movement::get_scale()
extern void Movement_get_scale_m71E7D3F001FC63082B3B49C6ECBDAF56322BA48E ();
// 0x0000019B System.Void Movement::set_scale(MovementScale)
extern void Movement_set_scale_m42883E72EA44AF241F3C193E5D7FA066B5858121 ();
// 0x0000019C System.Void Movement::.ctor()
extern void Movement__ctor_m1611A5910E25AEB0A8B91660D64BABA1A91987B4 ();
// 0x0000019D System.String Header::get_Category()
extern void Header_get_Category_mA7B7CB3C31D54769A33702C4289DC7CF8FE5AAD0 ();
// 0x0000019E System.Void Header::set_Category(System.String)
extern void Header_set_Category_mDA6E4AAEBBA27D92B35AA2697826787CFB2FAE1B ();
// 0x0000019F System.String Header::get_Material()
extern void Header_get_Material_mC797E87E6352546C3F130299EB5D4B9C7203FBE9 ();
// 0x000001A0 System.Void Header::set_Material(System.String)
extern void Header_set_Material_m977382A36D00EE01C2F4D46DFCB727C0069D71A6 ();
// 0x000001A1 System.Void Header::.ctor()
extern void Header__ctor_m659D4907FE9D76091688814A1B294AD48A47A9C5 ();
// 0x000001A2 System.String Body::get_CategoryName()
extern void Body_get_CategoryName_m50894D61BB4288EAC4F7AEA4BBF5F29F7F74A875 ();
// 0x000001A3 System.Void Body::set_CategoryName(System.String)
extern void Body_set_CategoryName_m590C27CD7EEEC94F8681B76D6873869F4348FA45 ();
// 0x000001A4 System.Boolean Body::get_ISSetValue()
extern void Body_get_ISSetValue_m50A091534416017BD66A23B9012D1808434473C7 ();
// 0x000001A5 System.Void Body::set_ISSetValue(System.Boolean)
extern void Body_set_ISSetValue_m84E9538F37C0814F8D1A01D56D5D95C285004022 ();
// 0x000001A6 System.String Body::get_CategoryValue()
extern void Body_get_CategoryValue_m585DF2D76D05AA07DB9AF96173A360AE28F510DF ();
// 0x000001A7 System.Void Body::set_CategoryValue(System.String)
extern void Body_set_CategoryValue_mE28D6DD5D521B57D474BB8341A8EB7E8B6D28902 ();
// 0x000001A8 System.String Body::get_SlideValue()
extern void Body_get_SlideValue_mABE0EA0419BD2EA38BBF29B19E90470DC1621DC0 ();
// 0x000001A9 System.Void Body::set_SlideValue(System.String)
extern void Body_set_SlideValue_m1177BFDFA527AFD194550ECB447618C721383148 ();
// 0x000001AA System.Void Body::.ctor()
extern void Body__ctor_m934AE05AD5882F055254FF2C1D7F37EEEE08AE21 ();
// 0x000001AB System.Collections.Generic.List`1<Header> PieChartDataModel::get_Header()
extern void PieChartDataModel_get_Header_mECA0DDE9BC1DD8E99268CED42BDB3916CC977AD3 ();
// 0x000001AC System.Void PieChartDataModel::set_Header(System.Collections.Generic.List`1<Header>)
extern void PieChartDataModel_set_Header_m11489E6F9372B971F6DAD893F24BED60772F0050 ();
// 0x000001AD System.Collections.Generic.List`1<Body> PieChartDataModel::get_Body()
extern void PieChartDataModel_get_Body_m9DE204578C43723309580D6A173A8C70A672197B ();
// 0x000001AE System.Void PieChartDataModel::set_Body(System.Collections.Generic.List`1<Body>)
extern void PieChartDataModel_set_Body_mCFCEADBE91F9D52C341B084510E0987E57DAA1B3 ();
// 0x000001AF System.Void PieChartDataModel::.ctor()
extern void PieChartDataModel__ctor_m0DD0895771938C95A345C4AD660ACC52A7874E77 ();
// 0x000001B0 System.String RadarChartcontent::get_CategoryName()
extern void RadarChartcontent_get_CategoryName_m871B16F410AF4E817CAD808ACC2675037E1245EB ();
// 0x000001B1 System.Void RadarChartcontent::set_CategoryName(System.String)
extern void RadarChartcontent_set_CategoryName_m24443F157AA59BE458ED9788616F42171658A4FA ();
// 0x000001B2 System.String RadarChartcontent::get_CategoryValue()
extern void RadarChartcontent_get_CategoryValue_m793E6B1243FCA7192C38C9AA9D383F261D216798 ();
// 0x000001B3 System.Void RadarChartcontent::set_CategoryValue(System.String)
extern void RadarChartcontent_set_CategoryValue_m0DFEC2560D7B62F3BD893694408ED51ABEB638A0 ();
// 0x000001B4 System.Void RadarChartcontent::.ctor()
extern void RadarChartcontent__ctor_m235BECAFAE4E5100AF166E3C5358F7D2A998B933 ();
// 0x000001B5 System.String RadarChartgroup::get_GroupName()
extern void RadarChartgroup_get_GroupName_mE8555850804E1D3BE6324CC3D7D68F70EE00BC5E ();
// 0x000001B6 System.Void RadarChartgroup::set_GroupName(System.String)
extern void RadarChartgroup_set_GroupName_mC9FCC2A264984AF29EDA06773BA149317D2C249C ();
// 0x000001B7 System.Collections.Generic.List`1<Header> RadarChartgroup::get_Header()
extern void RadarChartgroup_get_Header_m9838FF5779AC1843098B7D453E0DE92A9AF1568A ();
// 0x000001B8 System.Void RadarChartgroup::set_Header(System.Collections.Generic.List`1<Header>)
extern void RadarChartgroup_set_Header_mDD2B0415C29C2B0BE180818C9C92C316552CB9CB ();
// 0x000001B9 System.Collections.Generic.List`1<RadarChartcontent> RadarChartgroup::get_Body()
extern void RadarChartgroup_get_Body_m4BF341168EF0BA68F2364844A011A64572B1B87F ();
// 0x000001BA System.Void RadarChartgroup::set_Body(System.Collections.Generic.List`1<RadarChartcontent>)
extern void RadarChartgroup_set_Body_m351B682AEF07CF18CE59A0367CAF153799B87605 ();
// 0x000001BB System.Void RadarChartgroup::.ctor()
extern void RadarChartgroup__ctor_m09D2F69960A9B49BE654789F73E4193E26EC0F42 ();
// 0x000001BC System.Collections.Generic.List`1<RadarChartgroup> RadarChartDataModel::get_RadarChartgroup()
extern void RadarChartDataModel_get_RadarChartgroup_mF6F607B96E100FAD47DE649842D11BB80A26752C ();
// 0x000001BD System.Void RadarChartDataModel::set_RadarChartgroup(System.Collections.Generic.List`1<RadarChartgroup>)
extern void RadarChartDataModel_set_RadarChartgroup_m3AC32F7F20B72C65C7C95DAC4CF71389D1619CAA ();
// 0x000001BE System.Void RadarChartDataModel::.ctor()
extern void RadarChartDataModel__ctor_mBBABF4FD5139489B7F3EEE308ADE0E999BFCDDAC ();
// 0x000001BF System.Int64 ResponceModel::get_code()
extern void ResponceModel_get_code_mA340672AFCA50005BAF01F2770CBF338B244E2A9 ();
// 0x000001C0 System.Void ResponceModel::set_code(System.Int64)
extern void ResponceModel_set_code_mE139A6FE7AAF6008620073306627E70C36ACD167 ();
// 0x000001C1 System.String ResponceModel::get_message()
extern void ResponceModel_get_message_m1721E866CF63E99E055F29F6F518B513DB1B718C ();
// 0x000001C2 System.Void ResponceModel::set_message(System.String)
extern void ResponceModel_set_message_m1DFC3FC9473E121BBB2361DABDDE98D66921F166 ();
// 0x000001C3 System.Void ResponceModel::.ctor()
extern void ResponceModel__ctor_m70C11A79EF043C7B19E61F14683C41BC8464F745 ();
// 0x000001C4 System.Void RoomDetailModel::.ctor()
extern void RoomDetailModel__ctor_mD8071FC5BB9048C44C22BDDF93D80599DD812A03 ();
// 0x000001C5 System.Void Participant::.ctor()
extern void Participant__ctor_m72EF25AB2746C58D19F9C1F971809558CC5CEEAC ();
// 0x000001C6 System.String RoomDetailsModel::get_RoomName()
extern void RoomDetailsModel_get_RoomName_m8EA0C4E074284AD07DB847064C4FB2F2415999A3 ();
// 0x000001C7 System.Void RoomDetailsModel::set_RoomName(System.String)
extern void RoomDetailsModel_set_RoomName_m4D801AB5C4788582BE04F6DD6C8A8BE2D9CE76A9 ();
// 0x000001C8 System.String RoomDetailsModel::get_Description()
extern void RoomDetailsModel_get_Description_m2ED4CADADF3D765671F49BD059E477403092DEAF ();
// 0x000001C9 System.Void RoomDetailsModel::set_Description(System.String)
extern void RoomDetailsModel_set_Description_m7421CE06B44910A103FF9F349C53B42F383EE28F ();
// 0x000001CA System.String RoomDetailsModel::get_RoomId()
extern void RoomDetailsModel_get_RoomId_m065BAF7B49E8ABEAC55E2A9FFE31DDF86CE77357 ();
// 0x000001CB System.Void RoomDetailsModel::set_RoomId(System.String)
extern void RoomDetailsModel_set_RoomId_m69201C04BEE6EA8F774ECC82C59495E4AF0FA2BF ();
// 0x000001CC System.String RoomDetailsModel::get_RoomEnvironment()
extern void RoomDetailsModel_get_RoomEnvironment_m318148B04AEDCC82EF81C247557CF64829A1AF19 ();
// 0x000001CD System.Void RoomDetailsModel::set_RoomEnvironment(System.String)
extern void RoomDetailsModel_set_RoomEnvironment_mED83D8586DE952559071FA4E694431C1A6E2CB48 ();
// 0x000001CE System.DateTime RoomDetailsModel::get_CreatedAt()
extern void RoomDetailsModel_get_CreatedAt_m73F63E09B333546082EA6149BDB13E5C32A0293F ();
// 0x000001CF System.Void RoomDetailsModel::set_CreatedAt(System.DateTime)
extern void RoomDetailsModel_set_CreatedAt_mF025BE612402B547BA25098841999335517F480D ();
// 0x000001D0 System.Collections.Generic.List`1<RoomObject> RoomDetailsModel::get_RoomObject()
extern void RoomDetailsModel_get_RoomObject_m43C17EA42BC03291D075C51089265AD606B6B79D ();
// 0x000001D1 System.Void RoomDetailsModel::set_RoomObject(System.Collections.Generic.List`1<RoomObject>)
extern void RoomDetailsModel_set_RoomObject_mA622A0AA454A0FCEB73FF12A95B213921AFD946A ();
// 0x000001D2 ModeType RoomDetailsModel::get_ModeType()
extern void RoomDetailsModel_get_ModeType_m6315351859A18CA39E501DE4595F9866F03D7A83 ();
// 0x000001D3 System.Void RoomDetailsModel::set_ModeType(ModeType)
extern void RoomDetailsModel_set_ModeType_m59161590D4F8EEEDB55D7CEEEA4C6E6AF70F98C3 ();
// 0x000001D4 System.Void RoomDetailsModel::.ctor()
extern void RoomDetailsModel__ctor_m9FC7424450C915BC2B09BAFEF7165C4595102CD3 ();
// 0x000001D5 System.String RoomObjectScript::get_RoomID()
extern void RoomObjectScript_get_RoomID_mCAF19D5698FAF5531FAAA8DBAE2A34B47842FCA5 ();
// 0x000001D6 System.Void RoomObjectScript::set_RoomID(System.String)
extern void RoomObjectScript_set_RoomID_m919BCE127548C6D250D07F8B0AD4410E71223125 ();
// 0x000001D7 System.String RoomObjectScript::get_ObjectId()
extern void RoomObjectScript_get_ObjectId_m1E981427AAE3B4A15AF710E8B632864CEB52A0A8 ();
// 0x000001D8 System.Void RoomObjectScript::set_ObjectId(System.String)
extern void RoomObjectScript_set_ObjectId_mFA25700AACFA3C13730C5D73E6B5C572D0022E21 ();
// 0x000001D9 System.String RoomObjectScript::get_ParentObjectId()
extern void RoomObjectScript_get_ParentObjectId_m41177D414D6C59D15EAEA9774DC184BEB2E69A43 ();
// 0x000001DA System.Void RoomObjectScript::set_ParentObjectId(System.String)
extern void RoomObjectScript_set_ParentObjectId_m2A739415B2EF8F2E25DEBB786E60D80D5ADAC595 ();
// 0x000001DB System.String RoomObjectScript::get_ScriptContent()
extern void RoomObjectScript_get_ScriptContent_m05361315578984A894EBBAAB45F266EF17268AA1 ();
// 0x000001DC System.Void RoomObjectScript::set_ScriptContent(System.String)
extern void RoomObjectScript_set_ScriptContent_mA10F81B8413B43947527443AEAB0032353A16341 ();
// 0x000001DD System.Void RoomObjectScript::.ctor()
extern void RoomObjectScript__ctor_mF02A1BB5D5274E9FDDC186B0214D1792B23E917E ();
// 0x000001DE System.Void RoomObject::.ctor()
extern void RoomObject__ctor_m88AAF36719D09D0060BC25382DA6773613623A0C ();
// 0x000001DF System.String Metadata::get_name()
extern void Metadata_get_name_m493182F9BB90E808F05A1F2D093DCDB41E2D38A0 ();
// 0x000001E0 System.Void Metadata::set_name(System.String)
extern void Metadata_set_name_m3507FEC6FB3B649F54F82A3ABE4490DBCA57989E ();
// 0x000001E1 System.String Metadata::get_path_lower()
extern void Metadata_get_path_lower_mDAF5FA66A8F9059DAB81A364FA8A0F37268EFA5F ();
// 0x000001E2 System.Void Metadata::set_path_lower(System.String)
extern void Metadata_set_path_lower_m645851F28918768F300408CDBBE9A78B2BCF31A1 ();
// 0x000001E3 System.String Metadata::get_path_display()
extern void Metadata_get_path_display_m930AD9FF0F42CBFBF85399AEE70C9A44156BFC8C ();
// 0x000001E4 System.Void Metadata::set_path_display(System.String)
extern void Metadata_set_path_display_mEF778F4226A1A5F531992EAD81DE609B04B549AE ();
// 0x000001E5 System.String Metadata::get_id()
extern void Metadata_get_id_m02E041A7EBA041E26A214305EEA902550AD39340 ();
// 0x000001E6 System.Void Metadata::set_id(System.String)
extern void Metadata_set_id_m0794F0D279A861FAF11D37AF7ECA0A80A376D7CD ();
// 0x000001E7 System.DateTime Metadata::get_client_modified()
extern void Metadata_get_client_modified_m03B1FA946A2F64DEC175BBD9A1618284D4EE2C94 ();
// 0x000001E8 System.Void Metadata::set_client_modified(System.DateTime)
extern void Metadata_set_client_modified_m622A377B9DF78E696B54BF7F7DCCF9B863B6A11D ();
// 0x000001E9 System.DateTime Metadata::get_server_modified()
extern void Metadata_get_server_modified_m642C4197146ABBF47A8C758F07B713EC6F09C049 ();
// 0x000001EA System.Void Metadata::set_server_modified(System.DateTime)
extern void Metadata_set_server_modified_m76436F1C8C4A51B0471CC604B85AD45010EA2BB9 ();
// 0x000001EB System.String Metadata::get_rev()
extern void Metadata_get_rev_mAD63B8FF3413E64D10052CABABD9E8EF9111E8EA ();
// 0x000001EC System.Void Metadata::set_rev(System.String)
extern void Metadata_set_rev_mEAFECE116AC06D1FBAEC93E30FD9A1FFFEA8525E ();
// 0x000001ED System.Int32 Metadata::get_size()
extern void Metadata_get_size_m88A86E1D37AF949453C2FC4955E816748919E2BD ();
// 0x000001EE System.Void Metadata::set_size(System.Int32)
extern void Metadata_set_size_mCEFE80215EA12EA1501D7535A9C6B80E106EBB08 ();
// 0x000001EF System.Boolean Metadata::get_is_downloadable()
extern void Metadata_get_is_downloadable_mD3D7FF7E7D34CEB83C341EBB3B3EDF90EA515BEF ();
// 0x000001F0 System.Void Metadata::set_is_downloadable(System.Boolean)
extern void Metadata_set_is_downloadable_m3D3813B6B01A3535D55A07791C2A3FDA8DC33D28 ();
// 0x000001F1 System.String Metadata::get_content_hash()
extern void Metadata_get_content_hash_m02F014C267C688BF38BB91D995D7BE72B63FAE95 ();
// 0x000001F2 System.Void Metadata::set_content_hash(System.String)
extern void Metadata_set_content_hash_m5FFB961F6ECEA8F7C7D312D7CCE9E65EFD604196 ();
// 0x000001F3 System.Void Metadata::.ctor()
extern void Metadata__ctor_m95922140AA0AF669DE5F274FD022BE1AD3EE231E ();
// 0x000001F4 Metadata StorageDropBoxModel::get_metadata()
extern void StorageDropBoxModel_get_metadata_m7207B078B7F1C92EEDDA387CD228D7964883C595 ();
// 0x000001F5 System.Void StorageDropBoxModel::set_metadata(Metadata)
extern void StorageDropBoxModel_set_metadata_m49CC19B6D96D2BB8959D714BA2051E1800A6B93C ();
// 0x000001F6 System.String StorageDropBoxModel::get_link()
extern void StorageDropBoxModel_get_link_mDEB0123CD50AAF2B9C553001CAE3F4E1AB63F653 ();
// 0x000001F7 System.Void StorageDropBoxModel::set_link(System.String)
extern void StorageDropBoxModel_set_link_m350B2E1291AC4B92EB78E184AAF4D9FEFC4F5F69 ();
// 0x000001F8 System.Void StorageDropBoxModel::.ctor()
extern void StorageDropBoxModel__ctor_m0D82A92EC139752CE5C50BA4A8765375D868DBDB ();
// 0x000001F9 System.String StorageFileModel::get_type()
extern void StorageFileModel_get_type_m4D28D988C4D5C4D424CDBCD762D6D9CBD429AF87 ();
// 0x000001FA System.Void StorageFileModel::set_type(System.String)
extern void StorageFileModel_set_type_m0F08B974F96D52F3727C4E9531B5BED05F4DFE95 ();
// 0x000001FB System.String StorageFileModel::get_name()
extern void StorageFileModel_get_name_m85BEE8DE987B85F1EB3819C28A192CE8D1B7892A ();
// 0x000001FC System.Void StorageFileModel::set_name(System.String)
extern void StorageFileModel_set_name_m3C9324DA783CD8F4EA06605696345C69A87A84D8 ();
// 0x000001FD System.Void StorageFileModel::.ctor()
extern void StorageFileModel__ctor_m8188BF8535019E9DF2958B22E4DFCB691279D2EA ();
// 0x000001FE System.String StorageModel::get_id()
extern void StorageModel_get_id_mE28AE75D72A3A1B40F4CE7D4E2C4AF93665E8C78 ();
// 0x000001FF System.Void StorageModel::set_id(System.String)
extern void StorageModel_set_id_mEF166BB68B959B5D2211F698B1231A929083A5E4 ();
// 0x00000200 System.String StorageModel::get_name()
extern void StorageModel_get_name_m2A4F2FE44632BAEA1B9A9E6120EBB61167D97827 ();
// 0x00000201 System.Void StorageModel::set_name(System.String)
extern void StorageModel_set_name_mD4F2CE63E4E27246BAF8EB5F281768442B2D79AD ();
// 0x00000202 System.Void StorageModel::.ctor()
extern void StorageModel__ctor_m12E55019ECEBF5D59B997636531D02DEC736CADE ();
// 0x00000203 System.String category::get_Category()
extern void category_get_Category_mC1928132A45D3CEF7F6A5E3772F8018E18243324 ();
// 0x00000204 System.Void category::set_Category(System.String)
extern void category_set_Category_m773C6F40C0B3CD633F094DB8A1F5805B475851B6 ();
// 0x00000205 System.String category::get_Material()
extern void category_get_Material_m7F1A94FBFAE0846DE8C196EF8751692EDC24AE3D ();
// 0x00000206 System.Void category::set_Material(System.String)
extern void category_set_Material_m5BF1153D08D9EAEBE3DDEBFFD4119B40E04334D4 ();
// 0x00000207 System.Void category::.ctor()
extern void category__ctor_m9E7C9B111A55FA6D4E930FA2C12D07D44E9FA7BA ();
// 0x00000208 System.String content::get_CategoryName()
extern void content_get_CategoryName_m5551A5005025050F71B66741E3BCB4F93953F738 ();
// 0x00000209 System.Void content::set_CategoryName(System.String)
extern void content_set_CategoryName_mF4D575781CC711B42C600488A21F6B1D52755B9A ();
// 0x0000020A System.Boolean content::get_ISSetValue()
extern void content_get_ISSetValue_mBAFB9EE2CA7F9E65BA6297353E7B89BD0FC23031 ();
// 0x0000020B System.Void content::set_ISSetValue(System.Boolean)
extern void content_set_ISSetValue_m03E51D39426321AE07F1AC3945DA3C55191E8F6F ();
// 0x0000020C System.String content::get_CategoryValue()
extern void content_get_CategoryValue_mC3BBD7DE11B954BBD006A96F4815937D6C940971 ();
// 0x0000020D System.Void content::set_CategoryValue(System.String)
extern void content_set_CategoryValue_m14B3A46E67CC0E405A9EDC7D8949CE27629F6CBA ();
// 0x0000020E System.String content::get_SlideValue()
extern void content_get_SlideValue_mD8C14ED2663F1580A9ECAFEA21A448FBC900BBC0 ();
// 0x0000020F System.Void content::set_SlideValue(System.String)
extern void content_set_SlideValue_m8DBEAE98DC5F30412A121E52D29F608D530725C6 ();
// 0x00000210 System.Void content::.ctor()
extern void content__ctor_mCCCA623FC2441F0C89084D55DB67CA1C16E73417 ();
// 0x00000211 System.Collections.Generic.List`1<Header> TorusChartDataModel::get_Header()
extern void TorusChartDataModel_get_Header_mFEDA6E5E4A39297BAB5555B5D792F194004AFE8C ();
// 0x00000212 System.Void TorusChartDataModel::set_Header(System.Collections.Generic.List`1<Header>)
extern void TorusChartDataModel_set_Header_mDAE487703AE2CD8CE73C11B26F71F2D74F419BEC ();
// 0x00000213 System.Collections.Generic.List`1<Body> TorusChartDataModel::get_Body()
extern void TorusChartDataModel_get_Body_m011A74AEC7A82582D6DC17C9E26F53F153956E0A ();
// 0x00000214 System.Void TorusChartDataModel::set_Body(System.Collections.Generic.List`1<Body>)
extern void TorusChartDataModel_set_Body_m1276063EEE3064EBF2D693A4838CB4381A8DB664 ();
// 0x00000215 System.Void TorusChartDataModel::.ctor()
extern void TorusChartDataModel__ctor_mFD83F3DA4C569487D11E94D799EAD76E4C2EC7F6 ();
// 0x00000216 System.Void UserProfile::.ctor()
extern void UserProfile__ctor_mB062AF2F45A093C7296D9382A09FCBA6C19C7789 ();
// 0x00000217 System.String UserOrganization::get_orgname()
extern void UserOrganization_get_orgname_mC658F6189F9BA7BAE73227EEA662CB8D0DB5105F ();
// 0x00000218 System.Void UserOrganization::set_orgname(System.String)
extern void UserOrganization_set_orgname_mA200712D91D2BC71C10449972F50ABB8CF027D31 ();
// 0x00000219 System.String UserOrganization::get_tenantname()
extern void UserOrganization_get_tenantname_m69B8F188CE9DB5DD76F4DF5BBC929195E01E69B9 ();
// 0x0000021A System.Void UserOrganization::set_tenantname(System.String)
extern void UserOrganization_set_tenantname_mA17804D9869ED06D2D1B8EF00DBB9149EB6742FF ();
// 0x0000021B System.String UserOrganization::get_domain()
extern void UserOrganization_get_domain_mB568A21418F658726E9943A737139EE37DF36A4E ();
// 0x0000021C System.Void UserOrganization::set_domain(System.String)
extern void UserOrganization_set_domain_mDED5B27FD43F61DFDC9E955BAA249E53FDAE37F7 ();
// 0x0000021D System.String UserOrganization::get_tenantlogo()
extern void UserOrganization_get_tenantlogo_m39921F6D2F533647540EF02AF7A92CCFC2F23597 ();
// 0x0000021E System.Void UserOrganization::set_tenantlogo(System.String)
extern void UserOrganization_set_tenantlogo_mA43803A1E8C861C59B315B2C006F461955B0C166 ();
// 0x0000021F System.String UserOrganization::get_description()
extern void UserOrganization_get_description_m223D6FF3C677F0D46A95AF5133785C27E58585D9 ();
// 0x00000220 System.Void UserOrganization::set_description(System.String)
extern void UserOrganization_set_description_m62B5273A0FFBC84138CB20C65F5833A9AF67B658 ();
// 0x00000221 System.String UserOrganization::get_id()
extern void UserOrganization_get_id_m958606D9A574B1358568D6FEB7C20BB080867E63 ();
// 0x00000222 System.Void UserOrganization::set_id(System.String)
extern void UserOrganization_set_id_mAA9B0C06668E12D4D9BF9411CB864DD450372776 ();
// 0x00000223 System.String UserOrganization::get_sites_id()
extern void UserOrganization_get_sites_id_mA6B4F913F6D1F5E35847D72D4F6235D67A83915F ();
// 0x00000224 System.Void UserOrganization::set_sites_id(System.String)
extern void UserOrganization_set_sites_id_m4F9B0C3219FE2B2ECE82B04E2438E4A726012F9F ();
// 0x00000225 System.Void UserOrganization::.ctor()
extern void UserOrganization__ctor_m4FE079350EAA926C131B227DB407A9EE36703E3D ();
// 0x00000226 System.String UserInfoModel::get_id()
extern void UserInfoModel_get_id_m25CC3186130F44A1E12C99E62A4FC0ED42C71168 ();
// 0x00000227 System.Void UserInfoModel::set_id(System.String)
extern void UserInfoModel_set_id_m1344BCB2B848FF9F665920BDE7D78627EECC6092 ();
// 0x00000228 System.Int32 UserInfoModel::get_ttl()
extern void UserInfoModel_get_ttl_mD227AA8CDC799A40AB4825A405F88DA46B20DBC5 ();
// 0x00000229 System.Void UserInfoModel::set_ttl(System.Int32)
extern void UserInfoModel_set_ttl_m287843430707A96DA5C8923A0E0E126B4E8ADA91 ();
// 0x0000022A System.DateTime UserInfoModel::get_created()
extern void UserInfoModel_get_created_m68A1CEEFEA5D2102FF6ADE1D0E92E7CBD0BBF466 ();
// 0x0000022B System.Void UserInfoModel::set_created(System.DateTime)
extern void UserInfoModel_set_created_mF39917F02C61F49BB9E8FECBA8A5C12F6C601BEE ();
// 0x0000022C System.String UserInfoModel::get_user_id()
extern void UserInfoModel_get_user_id_m038D532AE3B4C973EF1FD0BC395A02D397D545E3 ();
// 0x0000022D System.Void UserInfoModel::set_user_id(System.String)
extern void UserInfoModel_set_user_id_mF01D1D38FC8CF9E8E6775C086ED9C2AF03499634 ();
// 0x0000022E System.String UserInfoModel::get_applicationDomain()
extern void UserInfoModel_get_applicationDomain_mF4B79A28F689DDEA6856335A6DB5D3F3C6CB4227 ();
// 0x0000022F System.Void UserInfoModel::set_applicationDomain(System.String)
extern void UserInfoModel_set_applicationDomain_m0F22952D929FCEA85709DE73C7A8B28863F74729 ();
// 0x00000230 UserProfile UserInfoModel::get_userProfile()
extern void UserInfoModel_get_userProfile_m3458D93B2243EA6C07231BE687B0EDE4F8951CB2 ();
// 0x00000231 System.Void UserInfoModel::set_userProfile(UserProfile)
extern void UserInfoModel_set_userProfile_m1A4A22FDF753D81F68A03F12F99373D9DEAF0CF4 ();
// 0x00000232 UserOrganization UserInfoModel::get_userOrganization()
extern void UserInfoModel_get_userOrganization_m79C8986DE79D213B8AE2559A0E113B6317FC5843 ();
// 0x00000233 System.Void UserInfoModel::set_userOrganization(UserOrganization)
extern void UserInfoModel_set_userOrganization_mE59F169BC38F1D0893941C6629D9EC949928900E ();
// 0x00000234 System.Void UserInfoModel::.ctor()
extern void UserInfoModel__ctor_m76EB52E9B1863D7DEAAAD5251932BD3AE6EBB84D ();
// 0x00000235 System.Void XapiModel::.ctor()
extern void XapiModel__ctor_m9B3C7B3F3E9415B4F6DCE126221AF6F3A7DD6607 ();
// 0x00000236 System.Void FrameWork::OnLoadLoader()
extern void FrameWork_OnLoadLoader_mF4C57B52D01147C33FFD27AA1B2F5F93FDFF1E3F ();
// 0x00000237 System.Void FrameWork::UnLoadLoader()
extern void FrameWork_UnLoadLoader_mB459BFFB96425338FC785943301B2FD7F7D3C614 ();
// 0x00000238 System.Void FrameWork::.ctor()
extern void FrameWork__ctor_m9669A3A5E82B9DDB9CCF8B3C6146C3110F2BCB85 ();
// 0x00000239 System.String Helper::GetDescription(T)
// 0x0000023A UnityEngine.Texture2D Helper::LoadPNG(System.String)
extern void Helper_LoadPNG_m1D99D0D3F8C2AFE67094774CD1C1BE29555721DE ();
// 0x0000023B T[] Helper::getJsonArray(System.String)
// 0x0000023C System.String Helper::arrayToJson(T[])
// 0x0000023D ProxyAPI RoomUtility::get_proxyAPI()
extern void RoomUtility_get_proxyAPI_m8AEC44486090AC6C55C1103B18500D0355426CF0 ();
// 0x0000023E System.Collections.Generic.List`1<ModeTypeModel> RoomUtility::get_ModeTypeModels()
extern void RoomUtility_get_ModeTypeModels_m70D3672AFA745D7CB82A5ED1393F59727E9C6F83 ();
// 0x0000023F System.Collections.Generic.List`1<RoomDetailModel> RoomUtility::get_RoomDetailModel()
extern void RoomUtility_get_RoomDetailModel_m21FC973FE1832C47300A0921779E95B1745C902B ();
// 0x00000240 System.Void RoomUtility::set_RoomDetailModel(System.Collections.Generic.List`1<RoomDetailModel>)
extern void RoomUtility_set_RoomDetailModel_m8EEB8BB5C91835DF937258486D8AC77E93A41126 ();
// 0x00000241 System.Void RoomUtility::GetModeType()
extern void RoomUtility_GetModeType_mA578F9815AA460B55E157A61E8DC47D8566507BA ();
// 0x00000242 System.Collections.Generic.List`1<RoomDetailModel> RoomUtility::GetRoomDetails()
extern void RoomUtility_GetRoomDetails_m9B7E0ADA3BC4C29A48F89B52DFAFA5E8014BD5A6 ();
// 0x00000243 System.Void RoomUtility::CreateRoom(RoomDetailModel)
extern void RoomUtility_CreateRoom_mF82351310385F0EA84C063B2D23AA1AEF6068CC6 ();
// 0x00000244 System.Void RoomUtility::UpateRoomDetails(RoomDetailModel)
extern void RoomUtility_UpateRoomDetails_mE7D9FB7FC9FD45A27FD582C3218602B0241FC3B6 ();
// 0x00000245 System.Void RoomUtility::UpateRoomObjectScripts(System.Collections.Generic.List`1<RoomObjectScript>)
extern void RoomUtility_UpateRoomObjectScripts_m85117DDFB4E674FABDF887F90348F2492CA306A1 ();
// 0x00000246 System.Void RoomUtility::GetRoomObjectScripts()
extern void RoomUtility_GetRoomObjectScripts_m23A84D8FFFE11DA962B12BC77A38BEA9E57930CE ();
// 0x00000247 System.Void RoomUtility::DeleteRoomObject(ExtensionObject)
extern void RoomUtility_DeleteRoomObject_m4B6AB09C72B779637D4ED97A4C9DDD24A043242F ();
// 0x00000248 UnityEngine.Vector3 RoomUtility::GetStepPosition()
extern void RoomUtility_GetStepPosition_mFDB9D892E57145A5B9E6865A2C604B25A7B9E70A ();
// 0x00000249 System.String RoomUtility::get_userIDArg()
extern void RoomUtility_get_userIDArg_m1E9BC48EAEE9577BFCF6F74769FDE55F53D20FA2 ();
// 0x0000024A System.Void RoomUtility::set_userIDArg(System.String)
extern void RoomUtility_set_userIDArg_m7EE2F7E1091BB050BA6E5B08EE9F4CF4A88B16BC ();
// 0x0000024B System.String RoomUtility::get_OrgIDArg()
extern void RoomUtility_get_OrgIDArg_m6CA6C17260BD8039A7AC7588E379448112D0A75E ();
// 0x0000024C System.Void RoomUtility::set_OrgIDArg(System.String)
extern void RoomUtility_set_OrgIDArg_m5517E95695908AC6CB7BC2F8C05FEEC10D07D437 ();
// 0x0000024D System.String RoomUtility::get_tokenArg()
extern void RoomUtility_get_tokenArg_m4A29EF057FD17659F64C788EA31BD34DF14AB456 ();
// 0x0000024E System.Void RoomUtility::set_tokenArg(System.String)
extern void RoomUtility_set_tokenArg_m1893B90B088E1E320D21CB1E69A5F927F365C020 ();
// 0x0000024F System.String RoomUtility::get_applicationDomainArg()
extern void RoomUtility_get_applicationDomainArg_m6463B0B6D9273B0728D2FFE153DEAC3390D81EAA ();
// 0x00000250 System.Void RoomUtility::set_applicationDomainArg(System.String)
extern void RoomUtility_set_applicationDomainArg_m0D6E9FF12BC37AB796AA1C49563BA9B9FCBA1D3A ();
// 0x00000251 System.String RoomUtility::get_RoomIDArg()
extern void RoomUtility_get_RoomIDArg_m54E1D3E3B2F9F44C93320AA076F73771B04F8391 ();
// 0x00000252 System.Void RoomUtility::set_RoomIDArg(System.String)
extern void RoomUtility_set_RoomIDArg_m9A4F97FB878A8DA3033C66B3155E8B9FE6888B9A ();
// 0x00000253 System.Void Example07LengthValidation::Init(MaterialUI.MaterialInputField)
extern void Example07LengthValidation_Init_m0C8BF48A6563207E0F83D85C515BD51A515212F0 ();
// 0x00000254 System.Boolean Example07LengthValidation::IsTextValid()
extern void Example07LengthValidation_IsTextValid_mA11875B8D99E42DFB2EED8ACDCE0CF1A6DADA7EC ();
// 0x00000255 System.Void Example07LengthValidation::.ctor()
extern void Example07LengthValidation__ctor_m5A8EEAB4577E7FE625F17ECDCEA560298841BD4B ();
// 0x00000256 System.Void Example07LetterValidation::Init(MaterialUI.MaterialInputField)
extern void Example07LetterValidation_Init_mDC31A3B1A2B7C5106B7599158B694DB0D29D0778 ();
// 0x00000257 System.Boolean Example07LetterValidation::IsTextValid()
extern void Example07LetterValidation_IsTextValid_m8CFD040599E1D232AC41D757473B9620213A6034 ();
// 0x00000258 System.Void Example07LetterValidation::.ctor()
extern void Example07LetterValidation__ctor_mBEA542FAB8453C8559E3918155F374F6849FEAB4 ();
// 0x00000259 System.Void Example08::Awake()
extern void Example08_Awake_m30E81DC6B35A0429DE821A996E6DD0E1F6A0C2CE ();
// 0x0000025A System.Void Example08::OnSliderValueChanged()
extern void Example08_OnSliderValueChanged_m1CF12085DE8B5304F67E40C1E02E4D57BB667184 ();
// 0x0000025B System.Void Example08::UpdateRGBImage()
extern void Example08_UpdateRGBImage_m3D65C133A3B6FAEA42FD2B26ED4AC368469BE094 ();
// 0x0000025C System.Void Example08::.ctor()
extern void Example08__ctor_m81FF69330C71006BC45C08CBB89049E11EF4B0C1 ();
// 0x0000025D System.Void Example09::OnButtonShowClicked()
extern void Example09_OnButtonShowClicked_m08492F1815B19DD47967E949A289BFFF9CB4A651 ();
// 0x0000025E System.Void Example09::OnButtonHideClicked()
extern void Example09_OnButtonHideClicked_m0A6751AAD400C4387BFBAEED5053CF479AFADAB2 ();
// 0x0000025F System.Void Example09::OnButtonIndeterminateClicked()
extern void Example09_OnButtonIndeterminateClicked_m88513FA879DD5831D64EEDA389DE3A87E28D9083 ();
// 0x00000260 System.Void Example09::OnButtonRandomProgressClicked()
extern void Example09_OnButtonRandomProgressClicked_mEDF82281B4D7DE9A0D523F668C5BC31CFBD94D3F ();
// 0x00000261 System.Void Example09::OnButtonRandomProgressAnimateClicked()
extern void Example09_OnButtonRandomProgressAnimateClicked_m94712C740C21613C0BEF003E0DB7E2B9FF15E255 ();
// 0x00000262 System.Void Example09::OnButtonRandomColorClicked()
extern void Example09_OnButtonRandomColorClicked_m3CA3896CE196A80ECCC3D704D171561966208B28 ();
// 0x00000263 UnityEngine.Color Example09::GetRandomColor()
extern void Example09_GetRandomColor_mE65A66FBF07767B9A8F465C9DAFE252A34762C19 ();
// 0x00000264 System.Void Example09::.ctor()
extern void Example09__ctor_m30485EC079DA13E0D04EB7AB4C6CE4FE7018DCB5 ();
// 0x00000265 System.Void Example10::OnIconNameButtonClicked()
extern void Example10_OnIconNameButtonClicked_mE9CD41EA61C9E9D4FCD995A6318919340B5DC459 ();
// 0x00000266 System.Void Example10::OnIconEnumButtonClicked()
extern void Example10_OnIconEnumButtonClicked_m9F91C29543593A9326AF111B4B6848936DDC54FA ();
// 0x00000267 System.Void Example10::OnIconRandomButtonClicked()
extern void Example10_OnIconRandomButtonClicked_mEFE1C5F383C24E71265B2CC04D558157790053E2 ();
// 0x00000268 MaterialUI.ImageData Example10::GetIconFromIconFont(System.String,System.String)
extern void Example10_GetIconFromIconFont_m3550728FCD083430A1D8567CE18388D683155CD1 ();
// 0x00000269 System.Void Example10::.ctor()
extern void Example10__ctor_m751F3727A1F991539F4879B8878E77EB0B62F546 ();
// 0x0000026A System.Void Example12::ChangeBarColor(System.Int32)
extern void Example12_ChangeBarColor_mF1C5C1EF085EFF96E1249B90005A97B25E931D6F ();
// 0x0000026B System.Void Example12::.ctor()
extern void Example12__ctor_m733AA682292E14DAD8B2829281A04DFD929561C1 ();
// 0x0000026C System.Void Example13::OnSimpleToastButtonClicked()
extern void Example13_OnSimpleToastButtonClicked_m1996DB119BB18ACBE394E90BE71D1411B48F6A53 ();
// 0x0000026D System.Void Example13::OnCustomToastButtonClicked()
extern void Example13_OnCustomToastButtonClicked_m43B59010F64171DEFA2AB081C00B5BA059D4B988 ();
// 0x0000026E System.Void Example13::OnSimpleSnackbarButtonClicked()
extern void Example13_OnSimpleSnackbarButtonClicked_m24B3D41288BEC0E9B000DAE7304F14E5A7122357 ();
// 0x0000026F System.Void Example13::OnCustomSnackbarButtonClicked()
extern void Example13_OnCustomSnackbarButtonClicked_m82625315C166B4B1EB3A2D0EEE7898E6B90D2201 ();
// 0x00000270 UnityEngine.Color Example13::GetRandomColor()
extern void Example13_GetRandomColor_m42DBD29AE6E5622A07A338FE61FAA55874E7167B ();
// 0x00000271 System.Void Example13::.ctor()
extern void Example13__ctor_mF028B6CBBB694785DFE768B987CB4453171C9FDE ();
// 0x00000272 System.Void Example14::OnProgressLinearButtonClicked()
extern void Example14_OnProgressLinearButtonClicked_m94FBFD57CF0A49328FD392C86510E9FB8898F875 ();
// 0x00000273 System.Void Example14::OnProgressCircularButtonClicked()
extern void Example14_OnProgressCircularButtonClicked_m7E3051E84F5407A9050AD078DD4C07D7DBA7CA3F ();
// 0x00000274 System.Collections.IEnumerator Example14::HideWindowAfterSeconds(MaterialUI.MaterialDialog,System.Single)
extern void Example14_HideWindowAfterSeconds_mDC6358A4104517A7EB402BD73F0D80E193253127 ();
// 0x00000275 System.Void Example14::OnSimpleListSmallButtonClicked()
extern void Example14_OnSimpleListSmallButtonClicked_m20EB9AE4EA5F28346B726E2D2AA4DE50B1049FCE ();
// 0x00000276 System.Void Example14::OnSimpleListBigVectorButtonClicked()
extern void Example14_OnSimpleListBigVectorButtonClicked_m168ED13D1CD71F2025609B45735E55995DFB9F96 ();
// 0x00000277 System.Void Example14::OnSimpleListBigSpriteButtonClicked()
extern void Example14_OnSimpleListBigSpriteButtonClicked_m6E62B5222EC85A592D41F63C097310E595AC5250 ();
// 0x00000278 System.Void Example14::OnAlertSimpleButtonClicked()
extern void Example14_OnAlertSimpleButtonClicked_m131FA900F9019B600D25DC91E6CC90E99C83A8D1 ();
// 0x00000279 System.Void Example14::OnAlertOneCallbackButtonClicked()
extern void Example14_OnAlertOneCallbackButtonClicked_mF705D54C00A16C7B40EE0D067CD5CF3ECF278760 ();
// 0x0000027A System.Void Example14::OnAlertTwoCallbacksButtonClicked()
extern void Example14_OnAlertTwoCallbacksButtonClicked_mC9EE278D3A2908330C89DD95CECB855C1BD9732F ();
// 0x0000027B System.Void Example14::OnAlertFromLeftButtonClicked()
extern void Example14_OnAlertFromLeftButtonClicked_m1AC2ADB6195313D46608E67E42E3626EB0EF864E ();
// 0x0000027C System.Void Example14::OnCheckboxListSmallButtonClicked()
extern void Example14_OnCheckboxListSmallButtonClicked_mF399C73382B77AD655D77C51F78ED1E2C46AB029 ();
// 0x0000027D System.Void Example14::OnCheckboxListBigButtonClicked()
extern void Example14_OnCheckboxListBigButtonClicked_m58C076F89E6A8800D5716D006234D78A8DC56069 ();
// 0x0000027E System.Void Example14::OnCheckboxValidateClicked(System.Boolean[])
extern void Example14_OnCheckboxValidateClicked_m3B562C13F83CFE28EA9C07488161C32191DD1C99 ();
// 0x0000027F System.Void Example14::OnRadioListSmallButtonClicked()
extern void Example14_OnRadioListSmallButtonClicked_m2E03E7E2FE763789E5287D5396F1C59603A5C4F7 ();
// 0x00000280 System.Void Example14::OnRadioListBigButtonClicked()
extern void Example14_OnRadioListBigButtonClicked_m8AC9668B3D54E98073667C141491D0E0DFE3254F ();
// 0x00000281 System.Void Example14::OnOneFieldPromptButtonClicked()
extern void Example14_OnOneFieldPromptButtonClicked_m588F09619330F6F6F80E4AE291780AE442BD0179 ();
// 0x00000282 System.Void Example14::OnTwoFieldsPromptButtonClicked()
extern void Example14_OnTwoFieldsPromptButtonClicked_m827C59FD870F4E344E134D25C68AB2071A29B447 ();
// 0x00000283 System.Void Example14::.ctor()
extern void Example14__ctor_m39B36308F0C7F761FEB37A908E676E2FDE16AC3B ();
// 0x00000284 System.Void Example14::<OnSimpleListSmallButtonClicked>b__6_0(System.Int32)
extern void Example14_U3COnSimpleListSmallButtonClickedU3Eb__6_0_m97715AA61DB81D954E0B160E18A0FE52897C4C34 ();
// 0x00000285 System.Void Example14::<OnSimpleListBigVectorButtonClicked>b__7_0(System.Int32)
extern void Example14_U3COnSimpleListBigVectorButtonClickedU3Eb__7_0_m5529A5E070CFFF5D6CE7BACD26AB7E9AB1FDA2B4 ();
// 0x00000286 System.Void Example14::<OnSimpleListBigSpriteButtonClicked>b__8_0(System.Int32)
extern void Example14_U3COnSimpleListBigSpriteButtonClickedU3Eb__8_0_mDEDD5E03174BB3BF9A4E00211004CC362B55AFBB ();
// 0x00000287 System.Void Example14::<OnRadioListSmallButtonClicked>b__16_0(System.Int32)
extern void Example14_U3COnRadioListSmallButtonClickedU3Eb__16_0_mBB9CF7343BAE6FB5540096F062FA62C84439E8C9 ();
// 0x00000288 System.Void Example14::<OnRadioListBigButtonClicked>b__17_0(System.Int32)
extern void Example14_U3COnRadioListBigButtonClickedU3Eb__17_0_m70A3233517652209108A1B8DE2A22932EAE69815 ();
// 0x00000289 System.Void Example15::OnTimePickerButtonClicked()
extern void Example15_OnTimePickerButtonClicked_m6B0E6C471AFF87048D74FFCA18A881F0B6B4FE3F ();
// 0x0000028A System.Void Example15::OnDatePickerButtonClicked()
extern void Example15_OnDatePickerButtonClicked_m285833E5430DAD79B91CA044B961A0DA87AF5777 ();
// 0x0000028B System.Void Example15::.ctor()
extern void Example15__ctor_mFA1EEB86C2B7CC04C9C14972C67E438994794C40 ();
// 0x0000028C System.Void Example19::Simple1(System.Boolean)
extern void Example19_Simple1_m8FC30F6DF6BD4D326284CB98BCB4227A3E2FAB2F ();
// 0x0000028D System.Void Example19::Simple2(System.Boolean)
extern void Example19_Simple2_m4A0A3F82BF410FB6F59B8B753C89751E8D2C8A7F ();
// 0x0000028E System.Void Example19::Advanced1(System.Boolean)
extern void Example19_Advanced1_m0A87BE07208D97CBDD82C77E389FCBE16BDE1787 ();
// 0x0000028F System.Void Example19::Advanced2(System.Int32)
extern void Example19_Advanced2_m96329DFAFD05FC430B30008B3EC6F4BEBA25376A ();
// 0x00000290 System.Void Example19::Issue(System.Boolean)
extern void Example19_Issue_m04AE0FEEC9F22C42BE202DD7B1CCD31E5A652FFF ();
// 0x00000291 System.Collections.IEnumerator Example19::IssueCoroutine(System.Boolean)
extern void Example19_IssueCoroutine_mEB376CE59F863E9E1F84A92E166827F88663BD92 ();
// 0x00000292 System.Void Example19::Workaround1(System.Boolean)
extern void Example19_Workaround1_m0F03A8373FBAB2C83BCEBFD3BCCB76366E9C1D36 ();
// 0x00000293 System.Void Example19::Workaround2(System.Boolean)
extern void Example19_Workaround2_m8B9494A89DE4E0FE29040EC6637EADBB4C9A01CD ();
// 0x00000294 System.Collections.IEnumerator Example19::Workaround2Coroutine(System.Boolean)
extern void Example19_Workaround2Coroutine_m2DEEEF16FCD13090A3283C4D77B67EDCE543E727 ();
// 0x00000295 System.Void Example19::Workaround2Workaround(UnityEngine.UI.Image,System.Boolean)
extern void Example19_Workaround2Workaround_m64E394D67DC18FDDCDAE516936E6840B91B6AB7E ();
// 0x00000296 System.Void Example19::.ctor()
extern void Example19__ctor_mCC3B1FD4403F4F43525DDD730D4D56F69FB21D74 ();
// 0x00000297 System.Void Example19::<Simple1>b__9_0(UnityEngine.Color)
extern void Example19_U3CSimple1U3Eb__9_0_m6268E475BA89D3D2E24728DF2A088FBDA445C8FA ();
// 0x00000298 System.Void Example19::<Simple2>b__10_1(UnityEngine.Color)
extern void Example19_U3CSimple2U3Eb__10_1_m58120A2FE94DD17A5CB4C0404ABE51D8C216FE44 ();
// 0x00000299 System.Void Example19::<Advanced1>b__11_0(UnityEngine.Color)
extern void Example19_U3CAdvanced1U3Eb__11_0_m1AC1490AFF5A9D233479866432D5343B2B1729D2 ();
// 0x0000029A System.Void Example19::<Advanced1>b__11_1(UnityEngine.Color)
extern void Example19_U3CAdvanced1U3Eb__11_1_mD58FA9A155F146F5A2D92149E026033CCF4B50F8 ();
// 0x0000029B System.Void Example19::<Advanced2>b__12_0(UnityEngine.Color)
extern void Example19_U3CAdvanced2U3Eb__12_0_m659BF172F2B29741303029E9E19D1B350A59C338 ();
// 0x0000029C System.Void NonDrawingGraphic::SetMaterialDirty()
extern void NonDrawingGraphic_SetMaterialDirty_mBFCBAA6C070E91361B50CBC21BD17730D9F95EE4 ();
// 0x0000029D System.Void NonDrawingGraphic::SetVerticesDirty()
extern void NonDrawingGraphic_SetVerticesDirty_mEB711E12FE00025E6D4C860F76E4684ACCE704E2 ();
// 0x0000029E System.Void NonDrawingGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void NonDrawingGraphic_OnPopulateMesh_mE3D3D5747611687CC56765F647350173A0590EFB ();
// 0x0000029F System.Void NonDrawingGraphic::.ctor()
extern void NonDrawingGraphic__ctor_m12D3DE778345F7BE8DA4ACACF656550EAE107BD1 ();
// 0x000002A0 System.Double DuoVia.FuzzyStrings.DiceCoefficientExtensions::DiceCoefficient(System.String,System.String)
extern void DiceCoefficientExtensions_DiceCoefficient_mD86176D56090F749E0A62E30373E00C4E0BB0B54 ();
// 0x000002A1 System.Double DuoVia.FuzzyStrings.DiceCoefficientExtensions::DiceCoefficient(System.String[],System.String[])
extern void DiceCoefficientExtensions_DiceCoefficient_m36C4E41523A5EF5F3B45D7B1911DA24B517FE3E5 ();
// 0x000002A2 System.String[] DuoVia.FuzzyStrings.DiceCoefficientExtensions::ToBiGrams(System.String)
extern void DiceCoefficientExtensions_ToBiGrams_mBBE736DA94E54F69A04A9FB4305771C86B526341 ();
// 0x000002A3 System.String[] DuoVia.FuzzyStrings.DiceCoefficientExtensions::ToTriGrams(System.String)
extern void DiceCoefficientExtensions_ToTriGrams_m0494A97587CC502C0C95D3315E9EC5708D53E008 ();
// 0x000002A4 System.String[] DuoVia.FuzzyStrings.DiceCoefficientExtensions::ToNGrams(System.String,System.Int32)
extern void DiceCoefficientExtensions_ToNGrams_m2785E9BC6FE6408A8860C00C33783B2ECBBFDF99 ();
// 0x000002A5 System.String DuoVia.FuzzyStrings.DoubleMetaphoneExtensions::ToDoubleMetaphone(System.String)
extern void DoubleMetaphoneExtensions_ToDoubleMetaphone_mDE3D04C9B588615AD4FFA7727C4F8B054DBF5A01 ();
// 0x000002A6 System.Boolean DuoVia.FuzzyStrings.DoubleMetaphoneExtensions::IsVowel(System.Char)
extern void DoubleMetaphoneExtensions_IsVowel_mA0083CA1ED3825D6EE471BDE519F69E02BC4A301 ();
// 0x000002A7 System.Boolean DuoVia.FuzzyStrings.DoubleMetaphoneExtensions::StartsWith(System.String,System.StringComparison,System.String[])
extern void DoubleMetaphoneExtensions_StartsWith_m626C6376D6CF2E045EC6BF03B96D1EAE57FB210D ();
// 0x000002A8 System.Boolean DuoVia.FuzzyStrings.DoubleMetaphoneExtensions::StringAt(System.String,System.Int32,System.String[])
extern void DoubleMetaphoneExtensions_StringAt_m5AC16DC6071F9D699D8257035F15988BFC661916 ();
// 0x000002A9 System.Int32 DuoVia.FuzzyStrings.LevenshteinDistanceExtensions::LevenshteinDistance(System.String,System.String,System.Boolean)
extern void LevenshteinDistanceExtensions_LevenshteinDistance_mB633CCDC4DD9380C218905929494D4F05700D6C6 ();
// 0x000002AA System.Int32 DuoVia.FuzzyStrings.LevenshteinDistanceExtensions::FindMinimum(System.Int32[])
extern void LevenshteinDistanceExtensions_FindMinimum_m732CB504EA80998B8980D37A3F95990AEE7C2596 ();
// 0x000002AB System.Double DuoVia.FuzzyStrings.LongestCommonSubsequenceExtensions::LongestCommonSubsequence(System.String,System.String,System.Boolean)
extern void LongestCommonSubsequenceExtensions_LongestCommonSubsequence_m9B7C818FDD90517C9F261C1AA937BC59D5754D2B ();
// 0x000002AC System.Int32 DuoVia.FuzzyStrings.LongestCommonSubsequenceExtensions::Square(System.Int32)
extern void LongestCommonSubsequenceExtensions_Square_mC17EA4ED08A33A2B5C693C0D9ACC930BC94B13A0 ();
// 0x000002AD System.Boolean DuoVia.FuzzyStrings.StringExtensions::FuzzyEquals(System.String,System.String,System.Double)
extern void StringExtensions_FuzzyEquals_mD0BC06C8FC9931AAC2A7D0873BB4C9BDA82B6308 ();
// 0x000002AE System.Double DuoVia.FuzzyStrings.StringExtensions::FuzzyMatch(System.String,System.String)
extern void StringExtensions_FuzzyMatch_m2597AFFB36D92A236319B3B447BB40E1ECE89BA3 ();
// 0x000002AF System.String DuoVia.FuzzyStrings.StringExtensions::Strip(System.String)
extern void StringExtensions_Strip_mF0549952A9F44B8638BF7E34DDFBCDE04EFAFB72 ();
// 0x000002B0 System.Double DuoVia.FuzzyStrings.StringExtensions::CompositeCoefficient(System.String,System.String)
extern void StringExtensions_CompositeCoefficient_m88D5980407A072141404495959029A180014E4DE ();
// 0x000002B1 System.Void DuoVia.FuzzyStrings.StringExtensions::.cctor()
extern void StringExtensions__cctor_m5F41853E209CCCC61A4009F4B0DAE029E251E2F8 ();
// 0x000002B2 System.Int32 MaterialUI.AutoTween::get_tweenId()
extern void AutoTween_get_tweenId_m8AF940DF8DCC33081A502B99A1E82A288DA1E317 ();
// 0x000002B3 System.Action MaterialUI.AutoTween::get_callback()
extern void AutoTween_get_callback_m8D168AAAF1415D1C148389B9DE913DC1E66C7D7C ();
// 0x000002B4 System.Void MaterialUI.AutoTween::set_callback(System.Action)
extern void AutoTween_set_callback_mD1A95DEDFCAE9528E24B1555D4CF154FB493EB59 ();
// 0x000002B5 System.Int32 MaterialUI.AutoTween::ValueLength()
// 0x000002B6 System.Void MaterialUI.AutoTween::OnUpdateValue()
// 0x000002B7 System.Void MaterialUI.AutoTween::OnFinalUpdateValue()
// 0x000002B8 System.Single MaterialUI.AutoTween::GetValue(System.Boolean,System.Int32)
// 0x000002B9 System.Void MaterialUI.AutoTween::Initialize(System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTween_Initialize_m1D8F79D37DFBFA771BB3921DBB8787F937A0F17A ();
// 0x000002BA System.Void MaterialUI.AutoTween::StartTween()
extern void AutoTween_StartTween_mFDC0AEF9E33BD4DCAED2AEB0A12E2DE77A0CE768 ();
// 0x000002BB System.Void MaterialUI.AutoTween::UpdateTween()
extern void AutoTween_UpdateTween_m921A03EFD7D8B318BB1E5EDFA3F34AC993FDC9D2 ();
// 0x000002BC System.Void MaterialUI.AutoTween::EndTween(System.Boolean)
extern void AutoTween_EndTween_mDE4A543057A254A57214FA14355BDCF74210107F ();
// 0x000002BD System.Void MaterialUI.AutoTween::SetupCurves()
extern void AutoTween_SetupCurves_mC6A893A5D5DA30522FD88AB39933DA56EE30E4D9 ();
// 0x000002BE System.Void MaterialUI.AutoTween::.ctor()
extern void AutoTween__ctor_m82B8A72099951CB66A0C4D18D416800E0E05824D ();
// 0x000002BF System.Single MaterialUI.AutoTweenInt::GetValue(System.Boolean,System.Int32)
extern void AutoTweenInt_GetValue_mE311989AA505B420D1C35880E0F77A87983F996E ();
// 0x000002C0 System.Int32 MaterialUI.AutoTweenInt::ValueLength()
extern void AutoTweenInt_ValueLength_mD7355DE0FB5FE2999640A0B0EEC399102F70CE24 ();
// 0x000002C1 System.Void MaterialUI.AutoTweenInt::Initialize(System.Action`1<System.Int32>,System.Func`1<System.Int32>,System.Func`1<System.Int32>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenInt_Initialize_mAFB0C3A99A3EE13F3940EC517C0585494C444283 ();
// 0x000002C2 System.Void MaterialUI.AutoTweenInt::StartTween()
extern void AutoTweenInt_StartTween_mEF6E5A42E53C4524EEE92C93E0D00832F33E7ECE ();
// 0x000002C3 System.Void MaterialUI.AutoTweenInt::OnUpdateValue()
extern void AutoTweenInt_OnUpdateValue_mE0B2C4072A1992D306135EF0F44536B967CA18BA ();
// 0x000002C4 System.Void MaterialUI.AutoTweenInt::OnFinalUpdateValue()
extern void AutoTweenInt_OnFinalUpdateValue_m8ACBB6A4C93F56DAE93AC5F6853DB5A72AC673E1 ();
// 0x000002C5 System.Void MaterialUI.AutoTweenInt::.ctor()
extern void AutoTweenInt__ctor_mB0A1AE114AE8C8AA82B147A39A25982D55D36B4C ();
// 0x000002C6 System.Single MaterialUI.AutoTweenFloat::GetValue(System.Boolean,System.Int32)
extern void AutoTweenFloat_GetValue_mD8A896EFBC078FCCF4EE6F3EACAD21DC0317E163 ();
// 0x000002C7 System.Int32 MaterialUI.AutoTweenFloat::ValueLength()
extern void AutoTweenFloat_ValueLength_m2E478D29B7F85D46763C44A47AB2F9EA0D89331A ();
// 0x000002C8 System.Void MaterialUI.AutoTweenFloat::Initialize(System.Action`1<System.Single>,System.Func`1<System.Single>,System.Func`1<System.Single>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenFloat_Initialize_mCEAA30EC31D8ED38DABE272F9A53DAB67FF3B6E1 ();
// 0x000002C9 System.Void MaterialUI.AutoTweenFloat::StartTween()
extern void AutoTweenFloat_StartTween_m63934E14D2CE3D7C25ACB927600BB9253EFD2A00 ();
// 0x000002CA System.Void MaterialUI.AutoTweenFloat::OnUpdateValue()
extern void AutoTweenFloat_OnUpdateValue_m49B81CABF08F9D8E6CF854B737225C7D79723C28 ();
// 0x000002CB System.Void MaterialUI.AutoTweenFloat::OnFinalUpdateValue()
extern void AutoTweenFloat_OnFinalUpdateValue_m0C96F3F44A6D08319317B260319CB7D563458B18 ();
// 0x000002CC System.Void MaterialUI.AutoTweenFloat::.ctor()
extern void AutoTweenFloat__ctor_m06CF328F0106B364FD6E83F29FC56B949008C527 ();
// 0x000002CD System.Single MaterialUI.AutoTweenVector2::GetValue(System.Boolean,System.Int32)
extern void AutoTweenVector2_GetValue_m4E7D4D4A6CCBEACEA59C4D53979A520287156822 ();
// 0x000002CE System.Int32 MaterialUI.AutoTweenVector2::ValueLength()
extern void AutoTweenVector2_ValueLength_m0360F1EDF471D436F3DAAB6DD84096EF9076EA10 ();
// 0x000002CF System.Void MaterialUI.AutoTweenVector2::Initialize(System.Action`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenVector2_Initialize_mA01357DC072DCD428EF1CBC02C06D857EDE6EAB8 ();
// 0x000002D0 System.Void MaterialUI.AutoTweenVector2::StartTween()
extern void AutoTweenVector2_StartTween_m988F45921878BEB6343F801DE6E658156CC32CC1 ();
// 0x000002D1 System.Void MaterialUI.AutoTweenVector2::OnUpdateValue()
extern void AutoTweenVector2_OnUpdateValue_m2388226A6BF0FAEA5C4D9F1D5CECF21F4E5A914C ();
// 0x000002D2 System.Void MaterialUI.AutoTweenVector2::OnFinalUpdateValue()
extern void AutoTweenVector2_OnFinalUpdateValue_mB36F50B1EFA4DF6169350F0A957AFF36079FB408 ();
// 0x000002D3 System.Void MaterialUI.AutoTweenVector2::.ctor()
extern void AutoTweenVector2__ctor_m629B71C5287B3B978331A17E3FA613C2F4FDC50A ();
// 0x000002D4 System.Single MaterialUI.AutoTweenVector3::GetValue(System.Boolean,System.Int32)
extern void AutoTweenVector3_GetValue_m83BE0D8C825909AA35A04C8C6FDBF091159752C3 ();
// 0x000002D5 System.Int32 MaterialUI.AutoTweenVector3::ValueLength()
extern void AutoTweenVector3_ValueLength_m0E3326F583895A2883E3C3D93FB3671887AE027C ();
// 0x000002D6 System.Void MaterialUI.AutoTweenVector3::Initialize(System.Action`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenVector3_Initialize_m76A6E1B0E92A02734FA2B01C4723E5A92447CC45 ();
// 0x000002D7 System.Void MaterialUI.AutoTweenVector3::StartTween()
extern void AutoTweenVector3_StartTween_mDDDE4A6F9B1C12DBAC52B453ECC00A84BC2CAA20 ();
// 0x000002D8 System.Void MaterialUI.AutoTweenVector3::OnUpdateValue()
extern void AutoTweenVector3_OnUpdateValue_mC52960C30D3CC67CB48A4889C9B2CA4B19F77FD8 ();
// 0x000002D9 System.Void MaterialUI.AutoTweenVector3::OnFinalUpdateValue()
extern void AutoTweenVector3_OnFinalUpdateValue_mEE0723AAE3C0867544388DF8629A58FE6297FF28 ();
// 0x000002DA System.Void MaterialUI.AutoTweenVector3::.ctor()
extern void AutoTweenVector3__ctor_mE8537C528963A989E6C59A82EBFA1919D9488B8A ();
// 0x000002DB System.Single MaterialUI.AutoTweenVector4::GetValue(System.Boolean,System.Int32)
extern void AutoTweenVector4_GetValue_mDBE0D70E0EE30F02CDCD8D5D46F91B188BD6477E ();
// 0x000002DC System.Int32 MaterialUI.AutoTweenVector4::ValueLength()
extern void AutoTweenVector4_ValueLength_m6002AA43C47FED2F677231F341E35676FEEBC041 ();
// 0x000002DD System.Void MaterialUI.AutoTweenVector4::Initialize(System.Action`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenVector4_Initialize_m5A7941153DF0A96196318E6229F4023528FA711C ();
// 0x000002DE System.Void MaterialUI.AutoTweenVector4::StartTween()
extern void AutoTweenVector4_StartTween_mA92D2EB491EB21E563A93058608DFBD2FCE67FEE ();
// 0x000002DF System.Void MaterialUI.AutoTweenVector4::OnUpdateValue()
extern void AutoTweenVector4_OnUpdateValue_m8A84DB1D3F751C357BA2F472268DA9AA131F607D ();
// 0x000002E0 System.Void MaterialUI.AutoTweenVector4::OnFinalUpdateValue()
extern void AutoTweenVector4_OnFinalUpdateValue_m48C0C613BA71AE53F95B7E51D45DA135D8612FA1 ();
// 0x000002E1 System.Void MaterialUI.AutoTweenVector4::.ctor()
extern void AutoTweenVector4__ctor_m742BD3B9495B904DE3DAFEB63A1B65F186C2E6EE ();
// 0x000002E2 System.Single MaterialUI.AutoTweenColor::GetValue(System.Boolean,System.Int32)
extern void AutoTweenColor_GetValue_m20C373C1B8D22CFD92E9955B7630CEE045AC9A81 ();
// 0x000002E3 System.Int32 MaterialUI.AutoTweenColor::ValueLength()
extern void AutoTweenColor_ValueLength_m4EBD5BE1516191FCE42E067108BC6BD6AB9F82A8 ();
// 0x000002E4 System.Void MaterialUI.AutoTweenColor::Initialize(System.Action`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Single,System.Single,MaterialUI.Tween_TweenType,System.Action,UnityEngine.AnimationCurve,System.Boolean,System.Int32)
extern void AutoTweenColor_Initialize_m212D1ED24C38EF486DBE637B49AC0553BA2C611A ();
// 0x000002E5 System.Void MaterialUI.AutoTweenColor::StartTween()
extern void AutoTweenColor_StartTween_mCF1ED89B6939F068E2E9C509C9343CDB4CC0DBD5 ();
// 0x000002E6 System.Void MaterialUI.AutoTweenColor::OnUpdateValue()
extern void AutoTweenColor_OnUpdateValue_m768688242D389ACF68343B5CE44753DB440CF9D3 ();
// 0x000002E7 System.Void MaterialUI.AutoTweenColor::OnFinalUpdateValue()
extern void AutoTweenColor_OnFinalUpdateValue_m33AD9E018679D6F13D5D906FBC067A0492152758 ();
// 0x000002E8 System.Void MaterialUI.AutoTweenColor::.ctor()
extern void AutoTweenColor__ctor_mC409B62BF00D0952BD0B8B71445DFA800AA296B6 ();
// 0x000002E9 System.Collections.Generic.List`1<MaterialUI.EasyTween_EasyTweenObject> MaterialUI.EasyTween::get_tweens()
extern void EasyTween_get_tweens_mC9FFF0E9A2426991BEB4705E21E327DEA3C77ADF ();
// 0x000002EA System.Void MaterialUI.EasyTween::set_tweens(System.Collections.Generic.List`1<MaterialUI.EasyTween_EasyTweenObject>)
extern void EasyTween_set_tweens_mFB860F69A9767C299DA67A6A1E4814B204525C7A ();
// 0x000002EB System.Void MaterialUI.EasyTween::Start()
extern void EasyTween_Start_m559D088257D31B226B20C337A8718A4C710C615E ();
// 0x000002EC System.Void MaterialUI.EasyTween::TweenAll()
extern void EasyTween_TweenAll_m2C4E76C8D5EECEED2F1D167F73B6AF8CEE8ACCC5 ();
// 0x000002ED System.Void MaterialUI.EasyTween::Tween(System.Int32)
extern void EasyTween_Tween_m0EC07A2794AD649A9D4B1AD546A8C945D71044D8 ();
// 0x000002EE System.Void MaterialUI.EasyTween::Tween(System.String)
extern void EasyTween_Tween_mA8CD0DD3B7E3C4219784C6279D215E7E65E60622 ();
// 0x000002EF System.Void MaterialUI.EasyTween::TweenSet(MaterialUI.EasyTween_EasyTweenObject)
extern void EasyTween_TweenSet_mDAA98231E1BA9217225F6976B4AA056DFA7BB047 ();
// 0x000002F0 System.Void MaterialUI.EasyTween::.ctor()
extern void EasyTween__ctor_m79265B07B96498B4BBA702C37E7C283EF9C5D71E ();
// 0x000002F1 System.Single[][] MaterialUI.Tween::GetAnimCurveKeys(MaterialUI.Tween_TweenType)
extern void Tween_GetAnimCurveKeys_mD97B9A674AA2FD4FA8F18F152C475A681AADE00A ();
// 0x000002F2 UnityEngine.AnimationCurve MaterialUI.Tween::CheckCurve(UnityEngine.AnimationCurve)
extern void Tween_CheckCurve_mC4C93A2C9617C3E6CE1DCDA727A58CB0F31D0BC1 ();
// 0x000002F3 System.Single MaterialUI.Tween::Evaluate(MaterialUI.Tween_TweenType,System.Single,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve)
extern void Tween_Evaluate_mEE9273B3B6CB88B1086A7CBB5358E14B0F150406 ();
// 0x000002F4 System.Single MaterialUI.Tween::Linear(System.Single,System.Single,System.Single,System.Single)
extern void Tween_Linear_mBBFDE8000F3130224523A8BA5520950B27CF6CBB ();
// 0x000002F5 UnityEngine.Vector2 MaterialUI.Tween::Linear(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_Linear_m4846926B64BB0CC64A2CCDDA234443CD90D97225 ();
// 0x000002F6 UnityEngine.Vector3 MaterialUI.Tween::Linear(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_Linear_mEF56FEA8DA5999665FB10CA523264562408642FC ();
// 0x000002F7 UnityEngine.Color MaterialUI.Tween::Linear(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_Linear_mEA4A7FD69B97B3BDB0D1EB96AE709D6974AC7DA3 ();
// 0x000002F8 System.Single MaterialUI.Tween::Sin(System.Single,System.Single,System.Single)
extern void Tween_Sin_mDDD49F49F9369747C69D7BBB4BAB3F9D0657400C ();
// 0x000002F9 UnityEngine.Vector2 MaterialUI.Tween::Sin(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Tween_Sin_m302759BD95A00977F0D84F3A3EA071683844A5AE ();
// 0x000002FA UnityEngine.Vector3 MaterialUI.Tween::Sin(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Tween_Sin_m860A26C93F66E909547925F5ADE251D73B980A30 ();
// 0x000002FB UnityEngine.Color MaterialUI.Tween::Sin(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void Tween_Sin_m98EDE3EDBBBB06B41C39751B24B0BFD6333C685B ();
// 0x000002FC System.Single MaterialUI.Tween::Overshoot(System.Single,System.Single,System.Single,System.Single)
extern void Tween_Overshoot_m67579699BF35994ADEE2DFFA9C55FEA1908F3D21 ();
// 0x000002FD UnityEngine.Vector2 MaterialUI.Tween::Overshoot(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_Overshoot_m44CE58EE33978BE06D7C2B965CD28FAB5897AEF0 ();
// 0x000002FE UnityEngine.Vector3 MaterialUI.Tween::Overshoot(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_Overshoot_m82E27ED5B5FF8EFB3F6440DAC9995B684BF35201 ();
// 0x000002FF UnityEngine.Color MaterialUI.Tween::Overshoot(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_Overshoot_m65BAC7277C9430F155055648301FB4A1D9A422F1 ();
// 0x00000300 System.Single MaterialUI.Tween::Bounce(System.Single,System.Single,System.Single,System.Single)
extern void Tween_Bounce_m8626DC79F0C1F24C993B5120BA0E1925BAB1B374 ();
// 0x00000301 UnityEngine.Vector2 MaterialUI.Tween::Bounce(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_Bounce_m2507C96E603C3E8C04B2B2963F78C4A397650FF9 ();
// 0x00000302 UnityEngine.Vector3 MaterialUI.Tween::Bounce(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_Bounce_m594C096E5C6F0FC89D8DA694E3FB6262F857B9A2 ();
// 0x00000303 UnityEngine.Color MaterialUI.Tween::Bounce(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_Bounce_m0F465A093D460DA7421E1855F275FF488D4718E3 ();
// 0x00000304 System.Single MaterialUI.Tween::CubeIn(System.Single,System.Single,System.Single,System.Single)
extern void Tween_CubeIn_m48621CB1D7356E1337F2C115EACB956A5CE89D30 ();
// 0x00000305 UnityEngine.Vector2 MaterialUI.Tween::CubeIn(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_CubeIn_m02E254302A1392BA9040BDE40ED820A301E5E388 ();
// 0x00000306 UnityEngine.Vector3 MaterialUI.Tween::CubeIn(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_CubeIn_m78593AC288D6F5820F98DBD8EEA043F9D6B3E6D7 ();
// 0x00000307 UnityEngine.Color MaterialUI.Tween::CubeIn(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_CubeIn_m790907FD6B5CC639FFB31CB83B81D5E0799D412D ();
// 0x00000308 System.Single MaterialUI.Tween::CubeOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_CubeOut_m044B4C58B5388BDBEFEE67CBEEAB86FD9238F1BF ();
// 0x00000309 UnityEngine.Vector2 MaterialUI.Tween::CubeOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_CubeOut_m9571201EF2F47CB41AA1E21F9B1FB47E197C123A ();
// 0x0000030A UnityEngine.Vector3 MaterialUI.Tween::CubeOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_CubeOut_m2F50BB13EEB9F127409E0FEB5BD02667D4EF1C0B ();
// 0x0000030B UnityEngine.Color MaterialUI.Tween::CubeOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_CubeOut_mBCCE8B34E8C4E725F627198DBCCFF5AA7D3BD31D ();
// 0x0000030C System.Single MaterialUI.Tween::CubeInOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_CubeInOut_m7280607B207190679C17D78F77E22B084E0228CE ();
// 0x0000030D UnityEngine.Vector2 MaterialUI.Tween::CubeInOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_CubeInOut_m24194D7EFCB926C6FF8B41EEE9444B336016FF7B ();
// 0x0000030E UnityEngine.Vector3 MaterialUI.Tween::CubeInOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_CubeInOut_m324CE1501D2B0CD922CCD4A124FD2BF7DAB27F8E ();
// 0x0000030F UnityEngine.Color MaterialUI.Tween::CubeInOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_CubeInOut_m1E2CE889496D5999B5298ED33CE4CED039B0B8CA ();
// 0x00000310 System.Single MaterialUI.Tween::CubeSoftOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_CubeSoftOut_m9504331FDF80AA9E0F83D592DF6F93C615C1DB85 ();
// 0x00000311 UnityEngine.Vector2 MaterialUI.Tween::CubeSoftOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_CubeSoftOut_m09CB81AA0EEF812CD7CFE4747F132CF553C8951D ();
// 0x00000312 UnityEngine.Vector3 MaterialUI.Tween::CubeSoftOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_CubeSoftOut_m977524D910FAFD14AFB41DEDA50AD9B2FBF13E0D ();
// 0x00000313 UnityEngine.Color MaterialUI.Tween::CubeSoftOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_CubeSoftOut_m00BEC2790CBFAB8E861DE2F5261228C82E798BDA ();
// 0x00000314 System.Single MaterialUI.Tween::QuintIn(System.Single,System.Single,System.Single,System.Single)
extern void Tween_QuintIn_m5F6F91BCD7E8145F7E41771CED21C8F779F75E88 ();
// 0x00000315 UnityEngine.Vector2 MaterialUI.Tween::QuintIn(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_QuintIn_m085ABFD9BEA555322A3EA688E3D45B6E42FE7F3B ();
// 0x00000316 UnityEngine.Vector3 MaterialUI.Tween::QuintIn(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_QuintIn_m2D37758663D58F53D33BD3BBAFF85B8979A29DF3 ();
// 0x00000317 UnityEngine.Color MaterialUI.Tween::QuintIn(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_QuintIn_m3E4BD495A52E70FC547FD0588BAEFADD973B8F01 ();
// 0x00000318 System.Single MaterialUI.Tween::QuintOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_QuintOut_m3482F4CBAC49F00FA047290AF0CAE369BE4BA30C ();
// 0x00000319 UnityEngine.Vector2 MaterialUI.Tween::QuintOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_QuintOut_m78CFFB2AC0E4A677C0C643CC494279C588FA084F ();
// 0x0000031A UnityEngine.Vector3 MaterialUI.Tween::QuintOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_QuintOut_mFB20677BD49764BEACCE79FD4FE3672FD5948FB7 ();
// 0x0000031B UnityEngine.Color MaterialUI.Tween::QuintOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_QuintOut_mFCC8509B754B0CC414A6CDF76C1169E98ACF69E1 ();
// 0x0000031C System.Single MaterialUI.Tween::QuintInOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_QuintInOut_mAD55FBE3D6070B7DE9E25B712E2D8D38A4BB6753 ();
// 0x0000031D UnityEngine.Vector2 MaterialUI.Tween::QuintInOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_QuintInOut_m63DDF2E1B80D1D07F0673A39B0A0B9639B8F4C9B ();
// 0x0000031E UnityEngine.Vector3 MaterialUI.Tween::QuintInOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_QuintInOut_m849D3B0F27B669AD25864B09F2CBC1095E2FC038 ();
// 0x0000031F UnityEngine.Color MaterialUI.Tween::QuintInOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_QuintInOut_mF76DDEA8D553F70CFB258FEDCC7484E5676944BB ();
// 0x00000320 System.Single MaterialUI.Tween::QuintSoftOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_QuintSoftOut_m3BBAFC1C5BBEB92901E1092713C23C61CDC9138C ();
// 0x00000321 UnityEngine.Vector2 MaterialUI.Tween::QuintSoftOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_QuintSoftOut_m7D1A3BA983773895DC9E4C2570379C5DD018A314 ();
// 0x00000322 UnityEngine.Vector3 MaterialUI.Tween::QuintSoftOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_QuintSoftOut_mB2BAE5B826F087E15C7DCABC1D04C4922610ECE1 ();
// 0x00000323 UnityEngine.Color MaterialUI.Tween::QuintSoftOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_QuintSoftOut_m2C925EC0B647CAB794178A4AC7B3C1EB20AECB6F ();
// 0x00000324 System.Single MaterialUI.Tween::SeptIn(System.Single,System.Single,System.Single,System.Single)
extern void Tween_SeptIn_m753FB9681D2D4650986788C8F066D47A487D150D ();
// 0x00000325 UnityEngine.Vector2 MaterialUI.Tween::SeptIn(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_SeptIn_m6DA5666A1DF907BE5959C258EFDED2CCE0E3B2A4 ();
// 0x00000326 UnityEngine.Vector3 MaterialUI.Tween::SeptIn(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_SeptIn_m3B564AE279DD8AE7417D34BA6989DFBB780156BC ();
// 0x00000327 UnityEngine.Color MaterialUI.Tween::SeptIn(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_SeptIn_m6CA7BCC719ECAFAEE34B62D3DA6AEAA71BA04065 ();
// 0x00000328 System.Single MaterialUI.Tween::SeptOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_SeptOut_m6A0BCD2C38B7243F997F23E87861D38059CB2170 ();
// 0x00000329 UnityEngine.Vector2 MaterialUI.Tween::SeptOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_SeptOut_m2231328F1540D8CC5BD5019D660129FE4237E1E1 ();
// 0x0000032A UnityEngine.Vector3 MaterialUI.Tween::SeptOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_SeptOut_m6E6D9937307A0FDBD8791C9852C8AD4B6BAAA886 ();
// 0x0000032B UnityEngine.Color MaterialUI.Tween::SeptOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_SeptOut_m5913366B9EE61F06A6A0C629AEAA45FC766F7391 ();
// 0x0000032C System.Single MaterialUI.Tween::SeptInOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_SeptInOut_m8063894F69C28E39316E659843A6C32CBC7B1D77 ();
// 0x0000032D UnityEngine.Vector2 MaterialUI.Tween::SeptInOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_SeptInOut_m74566DDA2493EF67AC61DB5B920049BBC3AD71FE ();
// 0x0000032E UnityEngine.Vector3 MaterialUI.Tween::SeptInOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_SeptInOut_m10E20474CAC5D2320E89F453F660EBFC98288A46 ();
// 0x0000032F UnityEngine.Color MaterialUI.Tween::SeptInOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_SeptInOut_m8034F8C1465109C3F440B9F9EC9FAC9CB701ADFC ();
// 0x00000330 System.Single MaterialUI.Tween::SeptSoftOut(System.Single,System.Single,System.Single,System.Single)
extern void Tween_SeptSoftOut_m239D302A762238630D83819696D2E5FBEFE93103 ();
// 0x00000331 UnityEngine.Vector2 MaterialUI.Tween::SeptSoftOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void Tween_SeptSoftOut_mABB28707CAAEEF59BD9BC4B244DCD9BFD2AFC4AD ();
// 0x00000332 UnityEngine.Vector3 MaterialUI.Tween::SeptSoftOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_SeptSoftOut_m011B89E4F9D21E849B83C7CD7B00D2964C8528DF ();
// 0x00000333 UnityEngine.Color MaterialUI.Tween::SeptSoftOut(UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void Tween_SeptSoftOut_mA41C8F30360FE2005730DC3AE5E6B8C4965D4264 ();
// 0x00000334 UnityEngine.UI.Graphic MaterialUI.ColorCopier::get_sourceGraphic()
extern void ColorCopier_get_sourceGraphic_m84DEF3E7675620006396F9226B76FAA6B7A17908 ();
// 0x00000335 System.Void MaterialUI.ColorCopier::set_sourceGraphic(UnityEngine.UI.Graphic)
extern void ColorCopier_set_sourceGraphic_m0FD1A589A14216055AEC8EE99CE66D8096433EA4 ();
// 0x00000336 UnityEngine.UI.Graphic MaterialUI.ColorCopier::get_destinationGraphic()
extern void ColorCopier_get_destinationGraphic_m81F8381EC734E054764BAD5DACD2003E9AD24D53 ();
// 0x00000337 System.Void MaterialUI.ColorCopier::set_destinationGraphic(UnityEngine.UI.Graphic)
extern void ColorCopier_set_destinationGraphic_m3BF49D1E40E29E622162B52DE6BBE7E89157536A ();
// 0x00000338 System.Void MaterialUI.ColorCopier::LateUpdate()
extern void ColorCopier_LateUpdate_m40F10AE90AD685EBC4B7EA7DA37BACAC1141E978 ();
// 0x00000339 System.Void MaterialUI.ColorCopier::UpdateColor()
extern void ColorCopier_UpdateColor_mB55D0DBC42DAF8D200D56D033BCA66CC09E0BE5B ();
// 0x0000033A System.Void MaterialUI.ColorCopier::.ctor()
extern void ColorCopier__ctor_m9BE311C6D287FBB3AEA1EBBCEB798340BF473994 ();
// 0x0000033B UnityEngine.GameObject MaterialUI.DragEventSender::get_horizontalTargetObject()
extern void DragEventSender_get_horizontalTargetObject_m19245468FA518D698C940DB301D8FE450CD7A21A ();
// 0x0000033C System.Void MaterialUI.DragEventSender::set_horizontalTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_horizontalTargetObject_m6B20491FA08DA98F6C2DBD90DFEB4BD083BDEE9E ();
// 0x0000033D UnityEngine.GameObject MaterialUI.DragEventSender::get_leftTargetObject()
extern void DragEventSender_get_leftTargetObject_mF966DBF2C3B7D0742BB982927596B0A924A492BF ();
// 0x0000033E System.Void MaterialUI.DragEventSender::set_leftTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_leftTargetObject_m1A06D4873895FB0467081D43EC6551BB8155D6E1 ();
// 0x0000033F UnityEngine.GameObject MaterialUI.DragEventSender::get_rightTargetObject()
extern void DragEventSender_get_rightTargetObject_m390E5DD67300D071FF48E5977A1A02C54514183F ();
// 0x00000340 System.Void MaterialUI.DragEventSender::set_rightTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_rightTargetObject_m7C4B76C9D6008DD6C84640E8CB948531C9AD7EFD ();
// 0x00000341 UnityEngine.GameObject MaterialUI.DragEventSender::get_verticalTargetObject()
extern void DragEventSender_get_verticalTargetObject_m08CDA824D673980BEB2E905E8A7AC19A81C00979 ();
// 0x00000342 System.Void MaterialUI.DragEventSender::set_verticalTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_verticalTargetObject_m2029FB6CD8D16EF04A9E7542AF187B3198DE7142 ();
// 0x00000343 UnityEngine.GameObject MaterialUI.DragEventSender::get_upTargetObject()
extern void DragEventSender_get_upTargetObject_mBD25A6A6EE8B640EDDA1AE13D4CC324323DFFBF4 ();
// 0x00000344 System.Void MaterialUI.DragEventSender::set_upTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_upTargetObject_m9E167A804BD74621EA522671AA214AD7CC91550D ();
// 0x00000345 UnityEngine.GameObject MaterialUI.DragEventSender::get_downTargetObject()
extern void DragEventSender_get_downTargetObject_m3D10D1E62CA2134C1D7DF47B7C7F0CCB6A1DFA5F ();
// 0x00000346 System.Void MaterialUI.DragEventSender::set_downTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_downTargetObject_m18D91466FCFD1EBF13DD995D47E91B10E4684398 ();
// 0x00000347 UnityEngine.GameObject MaterialUI.DragEventSender::get_anyDirectionTargetObject()
extern void DragEventSender_get_anyDirectionTargetObject_m0BA75862B174B14CD40F7D0DFB7E01305169408C ();
// 0x00000348 System.Void MaterialUI.DragEventSender::set_anyDirectionTargetObject(UnityEngine.GameObject)
extern void DragEventSender_set_anyDirectionTargetObject_m19C7990D639C280A5C855377FA41CB6DA8969EDD ();
// 0x00000349 System.Boolean MaterialUI.DragEventSender::get_combineLeftAndRight()
extern void DragEventSender_get_combineLeftAndRight_mD9B10846174489E7DF8AA15F0A0C2FED7871DFEB ();
// 0x0000034A System.Void MaterialUI.DragEventSender::set_combineLeftAndRight(System.Boolean)
extern void DragEventSender_set_combineLeftAndRight_m60B2045057B96528566CD31673F2089757578D10 ();
// 0x0000034B System.Boolean MaterialUI.DragEventSender::get_combineUpAndDown()
extern void DragEventSender_get_combineUpAndDown_mA1E63EF5553E9CBA49EC5FB4D292A393398227E7 ();
// 0x0000034C System.Void MaterialUI.DragEventSender::set_combineUpAndDown(System.Boolean)
extern void DragEventSender_set_combineUpAndDown_mAE5E52CDC5D847B3EB25A2F06DFC707E7BD9EBF8 ();
// 0x0000034D System.Void MaterialUI.DragEventSender::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragEventSender_OnDrag_m342C2F3ACCF06F1ABDF8A6271113FD5AEAD9C55F ();
// 0x0000034E System.Void MaterialUI.DragEventSender::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragEventSender_OnBeginDrag_m9C1A55F2963C8B8B68C241CD528B6E99468C850E ();
// 0x0000034F System.Void MaterialUI.DragEventSender::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragEventSender_OnEndDrag_mFAE5C051BF26B20C571149A95CD48CFE8EA60C5D ();
// 0x00000350 System.Void MaterialUI.DragEventSender::.ctor()
extern void DragEventSender__ctor_m64DE04320B35B747157DEB8802541FBD8A9E9F4C ();
// 0x00000351 System.Void MaterialUI.IRippleCreator::OnCreateRipple()
// 0x00000352 System.Void MaterialUI.IRippleCreator::OnDestroyRipple()
// 0x00000353 MaterialUI.RippleData MaterialUI.IRippleCreator::get_rippleData()
// 0x00000354 System.Void MaterialUI.ITextValidator::Init(MaterialUI.MaterialInputField)
// 0x00000355 System.Boolean MaterialUI.ITextValidator::IsTextValid()
// 0x00000356 MaterialUI.OptionDataList MaterialUI.IOptionDataListContainer::get_optionDataList()
// 0x00000357 System.Void MaterialUI.IOptionDataListContainer::set_optionDataList(MaterialUI.OptionDataList)
// 0x00000358 UnityEngine.Canvas MaterialUI.MaterialUIScaler::get_canvas()
extern void MaterialUIScaler_get_canvas_mF2A9417576A77163BD6C40386311759AA5550C12 ();
// 0x00000359 MaterialUI.MaterialUIScaler_ScalerMode MaterialUI.MaterialUIScaler::get_scalerMode()
extern void MaterialUIScaler_get_scalerMode_m42B6E855956CAED5E0043ECDF214066DA772CFDE ();
// 0x0000035A System.Void MaterialUI.MaterialUIScaler::set_scalerMode(MaterialUI.MaterialUIScaler_ScalerMode)
extern void MaterialUIScaler_set_scalerMode_mD2BFFA6FA5E47233D89F5CFF01BD97863EADD9DE ();
// 0x0000035B System.Single MaterialUI.MaterialUIScaler::get_scaleFactor()
extern void MaterialUIScaler_get_scaleFactor_mE6FE2FFA6BD08DBEB561CDF4556754DD318128B5 ();
// 0x0000035C System.Single MaterialUI.MaterialUIScaler::get_referencePixelsPerUnit()
extern void MaterialUIScaler_get_referencePixelsPerUnit_mBDD321F460924D58F6B10985C9211958C5A80197 ();
// 0x0000035D System.Void MaterialUI.MaterialUIScaler::set_referencePixelsPerUnit(System.Single)
extern void MaterialUIScaler_set_referencePixelsPerUnit_mBA1425E666899CB2C532FDA84B6BF904CB594C28 ();
// 0x0000035E System.Single MaterialUI.MaterialUIScaler::get_dynamicPixelsPerUnit()
extern void MaterialUIScaler_get_dynamicPixelsPerUnit_m6E9CB6AADC4FADFD8A67FFF6B7B59C24EBDD109A ();
// 0x0000035F System.Void MaterialUI.MaterialUIScaler::set_dynamicPixelsPerUnit(System.Single)
extern void MaterialUIScaler_set_dynamicPixelsPerUnit_m2C50CF16A3442613281047603D0B7493C3FFA608 ();
// 0x00000360 System.Single MaterialUI.MaterialUIScaler::get_baseDpi()
extern void MaterialUIScaler_get_baseDpi_m7FC517E10C8E878B4C6D8C22EE0EB9F73BB50F2C ();
// 0x00000361 System.Void MaterialUI.MaterialUIScaler::set_baseDpi(System.Single)
extern void MaterialUIScaler_set_baseDpi_mF946537201AA79AF5071FF63C894780524F6CB2A ();
// 0x00000362 System.Single MaterialUI.MaterialUIScaler::get_fallbackDpi()
extern void MaterialUIScaler_get_fallbackDpi_m944D295E9748AD28D05D471A6803ECEE38076611 ();
// 0x00000363 System.Void MaterialUI.MaterialUIScaler::set_fallbackDpi(System.Single)
extern void MaterialUIScaler_set_fallbackDpi_m8B4C6DDFAFB5542E36E5961BC34712DD327AEB15 ();
// 0x00000364 System.Single MaterialUI.MaterialUIScaler::get_scaleModifier()
extern void MaterialUIScaler_get_scaleModifier_m032FE3EA0DCBA651DFEB48472104F609F8EBDC9A ();
// 0x00000365 System.Void MaterialUI.MaterialUIScaler::set_scaleModifier(System.Single)
extern void MaterialUIScaler_set_scaleModifier_m1AD9566B290A706C708E49C91551A419D7933159 ();
// 0x00000366 System.Int32 MaterialUI.MaterialUIScaler::get_scaleSnapLevel()
extern void MaterialUIScaler_get_scaleSnapLevel_m6944CF8D3178650BCB3871059AA6988D1BD6D764 ();
// 0x00000367 System.Void MaterialUI.MaterialUIScaler::set_scaleSnapLevel(System.Int32)
extern void MaterialUIScaler_set_scaleSnapLevel_mF7CF26D2BA75FAFA84482BFCA1A0840C3C3A2C08 ();
// 0x00000368 MaterialUI.MaterialUIScaler MaterialUI.MaterialUIScaler::get_targetScaler()
extern void MaterialUIScaler_get_targetScaler_m86E0223BA7A2FDAD9DBE723BD6E990F43C9F5397 ();
// 0x00000369 System.Void MaterialUI.MaterialUIScaler::set_targetScaler(MaterialUI.MaterialUIScaler)
extern void MaterialUIScaler_set_targetScaler_m3F59754A1EDBDFF2FC70046663028BA5E3419545 ();
// 0x0000036A UnityEngine.UI.CanvasScaler MaterialUI.MaterialUIScaler::get_targetCanvasScaler()
extern void MaterialUIScaler_get_targetCanvasScaler_mB4DCE6589649DDD7B8CAE98E0683F992CEDE84E5 ();
// 0x0000036B MaterialUI.MaterialUIScaler_Orientation MaterialUI.MaterialUIScaler::get_orientation()
extern void MaterialUIScaler_get_orientation_mF7C840E9BB4BA99B92BB5EDB37367084D8F96D92 ();
// 0x0000036C MaterialUI.MaterialUIScaler_CanvasAreaChangedEvent MaterialUI.MaterialUIScaler::get_onCanvasAreaChanged()
extern void MaterialUIScaler_get_onCanvasAreaChanged_m58FC2E64020DA599D1F745E9C3320E30C9F92E1A ();
// 0x0000036D System.Void MaterialUI.MaterialUIScaler::set_onCanvasAreaChanged(MaterialUI.MaterialUIScaler_CanvasAreaChangedEvent)
extern void MaterialUIScaler_set_onCanvasAreaChanged_m9A380E2F66BBC29DE3AF0BDA26AC4B2E4C13AEB5 ();
// 0x0000036E System.Void MaterialUI.MaterialUIScaler::Handle()
extern void MaterialUIScaler_Handle_m54B5E210C68DDD648100E9DED4A75903C6A173A9 ();
// 0x0000036F System.Int32 MaterialUI.MaterialUIScaler::get_screenWidth()
extern void MaterialUIScaler_get_screenWidth_m4CA118D7E1799CBAAAB5F1028D993D9C821F4D34 ();
// 0x00000370 System.Int32 MaterialUI.MaterialUIScaler::get_screenHeight()
extern void MaterialUIScaler_get_screenHeight_m12E4FF7BA57104EB060F5FFDE7A8A56C2C99D03D ();
// 0x00000371 System.Single MaterialUI.MaterialUIScaler::get_dpi()
extern void MaterialUIScaler_get_dpi_m796D3F18565B17918DBCC3EFD5F81EBE67BB41BA ();
// 0x00000372 System.Single MaterialUI.MaterialUIScaler::get_screenSizeDigonal()
extern void MaterialUIScaler_get_screenSizeDigonal_m082CF8345C0BD5325ABB59220E687A62B8CEB13C ();
// 0x00000373 System.Void MaterialUI.MaterialUIScaler::HandleConstantPhysicalSize()
extern void MaterialUIScaler_HandleConstantPhysicalSize_m043CE33FCD93DF16F6D982FE508A9B8A60B375F9 ();
// 0x00000374 System.Void MaterialUI.MaterialUIScaler::HandleCopyOtherScaler()
extern void MaterialUIScaler_HandleCopyOtherScaler_m770A2D68661AC9EC887676E0BC9B4CBE0ED0144E ();
// 0x00000375 System.Void MaterialUI.MaterialUIScaler::HandleDontChangeScale()
extern void MaterialUIScaler_HandleDontChangeScale_m85C24CF2D43BE393729F3720BB9D9A777E99DAB3 ();
// 0x00000376 System.Void MaterialUI.MaterialUIScaler::ApplyScaleSettings(System.Single,System.Single,System.Boolean)
extern void MaterialUIScaler_ApplyScaleSettings_mED3EB6ABCFCDFCE0B24326B39A448267F858C94F ();
// 0x00000377 System.Void MaterialUI.MaterialUIScaler::OnEnable()
extern void MaterialUIScaler_OnEnable_m738B910D0F4D87559A98B898596A6982BB70EFCA ();
// 0x00000378 System.Void MaterialUI.MaterialUIScaler::Awake()
extern void MaterialUIScaler_Awake_m32D2A250AD9A3F0501AA4DF72C8D30E6C5F16B7D ();
// 0x00000379 System.Void MaterialUI.MaterialUIScaler::Update()
extern void MaterialUIScaler_Update_mB97B2DE707FCFF1546C51DFAB3C7118A99EB227F ();
// 0x0000037A System.Void MaterialUI.MaterialUIScaler::OnRectTransformDimensionsChange()
extern void MaterialUIScaler_OnRectTransformDimensionsChange_m759807255EA1A8CCE940BBD775B91FC1540C79A0 ();
// 0x0000037B System.Void MaterialUI.MaterialUIScaler::OnDisable()
extern void MaterialUIScaler_OnDisable_mFD341248EAC08C810BD2AE724CB5CF990AD66541 ();
// 0x0000037C MaterialUI.MaterialUIScaler MaterialUI.MaterialUIScaler::GetRootScaler(UnityEngine.Transform)
extern void MaterialUIScaler_GetRootScaler_mDA79F4FFDD8613EC8998BA053B013DC480925B7F ();
// 0x0000037D System.Void MaterialUI.MaterialUIScaler::.ctor()
extern void MaterialUIScaler__ctor_m462A2F17D0DE7A0F7FAD8FB2B69AF1436D36346E ();
// 0x0000037E System.Void MaterialUI.MaterialUIScaler::<OnEnable>b__71_0(System.Boolean,System.Boolean)
extern void MaterialUIScaler_U3COnEnableU3Eb__71_0_m8DC734FACB159E080B519FDCBAD6FC9DE3758053 ();
// 0x0000037F System.Void MaterialUI.MaterialUIScaler::<OnDisable>b__75_0(System.Boolean,System.Boolean)
extern void MaterialUIScaler_U3COnDisableU3Eb__75_0_mC70FA52BBB24BAA2E988226AC9644FF7C5A1592E ();
// 0x00000380 UnityEngine.RectTransform MaterialUI.RectTransformSnap::get_rectTransform()
extern void RectTransformSnap_get_rectTransform_m54D49EA9080B60B889EBC6838859161A4079E677 ();
// 0x00000381 UnityEngine.RectTransform MaterialUI.RectTransformSnap::get_sourceRectTransform()
extern void RectTransformSnap_get_sourceRectTransform_m247B732839CE552870C93F6DA28B627953559AFC ();
// 0x00000382 System.Void MaterialUI.RectTransformSnap::set_sourceRectTransform(UnityEngine.RectTransform)
extern void RectTransformSnap_set_sourceRectTransform_mF05E407501BD53E047726E12FB65D91AB9949491 ();
// 0x00000383 UnityEngine.Vector2 MaterialUI.RectTransformSnap::get_padding()
extern void RectTransformSnap_get_padding_m62EF85189163BFEA344230CCFEF9115708CC1057 ();
// 0x00000384 System.Void MaterialUI.RectTransformSnap::set_padding(UnityEngine.Vector2)
extern void RectTransformSnap_set_padding_mF442CAC844A7AFF6F7AF0C9C0B05290EA8F7BC16 ();
// 0x00000385 UnityEngine.Vector2 MaterialUI.RectTransformSnap::get_offset()
extern void RectTransformSnap_get_offset_m95F398768C6AB9D74EF350CB62B30C67A46F0D74 ();
// 0x00000386 System.Void MaterialUI.RectTransformSnap::set_offset(UnityEngine.Vector2)
extern void RectTransformSnap_set_offset_m5E41A3C8B1988A8EEDD2BB846550C1EE136ED09B ();
// 0x00000387 System.Boolean MaterialUI.RectTransformSnap::get_valuesArePercentage()
extern void RectTransformSnap_get_valuesArePercentage_mE97948EB05FB65AAE3D82E9F8A4037BD9CE56888 ();
// 0x00000388 System.Void MaterialUI.RectTransformSnap::set_valuesArePercentage(System.Boolean)
extern void RectTransformSnap_set_valuesArePercentage_mEEEAB716DE194C3E92A12DEF35184E795ADCA438 ();
// 0x00000389 UnityEngine.Vector2 MaterialUI.RectTransformSnap::get_paddingPercent()
extern void RectTransformSnap_get_paddingPercent_m80FDA12473918B6AAEC7D0071D7785378184D554 ();
// 0x0000038A System.Void MaterialUI.RectTransformSnap::set_paddingPercent(UnityEngine.Vector2)
extern void RectTransformSnap_set_paddingPercent_mD7C43BEB69E698072CF5BDD3B055CC50A1B1AE82 ();
// 0x0000038B UnityEngine.Vector2 MaterialUI.RectTransformSnap::get_offsetPercent()
extern void RectTransformSnap_get_offsetPercent_mB6B2CDC67F361600063755284541DB5B3EB7B983 ();
// 0x0000038C System.Void MaterialUI.RectTransformSnap::set_offsetPercent(UnityEngine.Vector2)
extern void RectTransformSnap_set_offsetPercent_m3E39D979BB3331D38FA8387513A01FA2B2442700 ();
// 0x0000038D System.Boolean MaterialUI.RectTransformSnap::get_snapEveryFrame()
extern void RectTransformSnap_get_snapEveryFrame_mD8CE68EAB1264BA162C93C2902CAA08190E37FC2 ();
// 0x0000038E System.Void MaterialUI.RectTransformSnap::set_snapEveryFrame(System.Boolean)
extern void RectTransformSnap_set_snapEveryFrame_m909DFC85B562F0D00DC5E0218E3A72A461D09073 ();
// 0x0000038F System.Boolean MaterialUI.RectTransformSnap::get_snapWidth()
extern void RectTransformSnap_get_snapWidth_mE185D1754FE44B1ED76CAF3EDCDCD8C00049097B ();
// 0x00000390 System.Void MaterialUI.RectTransformSnap::set_snapWidth(System.Boolean)
extern void RectTransformSnap_set_snapWidth_mB8C541F527324A671D24A198A508996DBF09DCCA ();
// 0x00000391 System.Boolean MaterialUI.RectTransformSnap::get_snapHeight()
extern void RectTransformSnap_get_snapHeight_m5533359193F8F5C9AE7A08366B442A9422BA6A77 ();
// 0x00000392 System.Void MaterialUI.RectTransformSnap::set_snapHeight(System.Boolean)
extern void RectTransformSnap_set_snapHeight_m6AA43B5FA0F9577D9FA3131DA51A8C58A3B9C184 ();
// 0x00000393 System.Boolean MaterialUI.RectTransformSnap::get_snapPositionX()
extern void RectTransformSnap_get_snapPositionX_m65B4DF74ECEA0021A9B5647D546AC46C347DD989 ();
// 0x00000394 System.Void MaterialUI.RectTransformSnap::set_snapPositionX(System.Boolean)
extern void RectTransformSnap_set_snapPositionX_mDDB4C80E1A1A83C9ADB2ADF966556CCBF9AA1980 ();
// 0x00000395 System.Boolean MaterialUI.RectTransformSnap::get_snapPositionY()
extern void RectTransformSnap_get_snapPositionY_m08E9E77DB06C8908D410FB595302430C47127CA3 ();
// 0x00000396 System.Void MaterialUI.RectTransformSnap::set_snapPositionY(System.Boolean)
extern void RectTransformSnap_set_snapPositionY_m584C98A29381510F59717C523C497BEC9A6EEA0A ();
// 0x00000397 System.Void MaterialUI.RectTransformSnap::Awake()
extern void RectTransformSnap_Awake_m50F8220E6C0ECCE4EC3F4330576EC3D90471934F ();
// 0x00000398 System.Void MaterialUI.RectTransformSnap::OnEnable()
extern void RectTransformSnap_OnEnable_m90C35ED93CBD649CCFAD78EDDB66DF4FAD940D5D ();
// 0x00000399 System.Void MaterialUI.RectTransformSnap::OnDisable()
extern void RectTransformSnap_OnDisable_mC13E78B9666B3EF73F0F4A0E56B2D94A5B78480C ();
// 0x0000039A System.Void MaterialUI.RectTransformSnap::OnValidate()
extern void RectTransformSnap_OnValidate_m562D14DB9549CFF20F109927A21A47462AB3121B ();
// 0x0000039B System.Void MaterialUI.RectTransformSnap::LateUpdate()
extern void RectTransformSnap_LateUpdate_m25EA1629524CD4448D7A16E50D163A6C4540CB42 ();
// 0x0000039C System.Void MaterialUI.RectTransformSnap::SetLayoutDirty()
extern void RectTransformSnap_SetLayoutDirty_m49EA273CA8C5C3B2D3E390E51008A106C810ACCA ();
// 0x0000039D System.Void MaterialUI.RectTransformSnap::CalculateLayoutInputHorizontal()
extern void RectTransformSnap_CalculateLayoutInputHorizontal_m7BC9F56D1DC86A96C5E2E843449A0CEC587B6873 ();
// 0x0000039E System.Void MaterialUI.RectTransformSnap::CalculateLayoutInputVertical()
extern void RectTransformSnap_CalculateLayoutInputVertical_m09A376510CDA9F6E8958C16775D6392A4CA4E9AB ();
// 0x0000039F System.Void MaterialUI.RectTransformSnap::SetLayoutHorizontal()
extern void RectTransformSnap_SetLayoutHorizontal_m9D85C2DC67D53B9C8CAD48CEC3A58D1E545E05DF ();
// 0x000003A0 System.Void MaterialUI.RectTransformSnap::SetLayoutVertical()
extern void RectTransformSnap_SetLayoutVertical_m2839234DBEDF917D3F2D9D0CB8192B9C41036C65 ();
// 0x000003A1 System.Single MaterialUI.RectTransformSnap::get_minWidth()
extern void RectTransformSnap_get_minWidth_mA95E996DF2E38EEF6EB0F6DA57E89CA5F3D298D2 ();
// 0x000003A2 System.Single MaterialUI.RectTransformSnap::get_preferredWidth()
extern void RectTransformSnap_get_preferredWidth_m529CC3FB1EDE5522FAFA2A2EBAD64A725F9BC591 ();
// 0x000003A3 System.Single MaterialUI.RectTransformSnap::get_flexibleWidth()
extern void RectTransformSnap_get_flexibleWidth_m40220382E08D7A3677AFB3262F69A8F0AA123B49 ();
// 0x000003A4 System.Single MaterialUI.RectTransformSnap::get_minHeight()
extern void RectTransformSnap_get_minHeight_mD274B8677D4A00A733567BB67A99C44EA7B3CA34 ();
// 0x000003A5 System.Single MaterialUI.RectTransformSnap::get_preferredHeight()
extern void RectTransformSnap_get_preferredHeight_m23B36DF84F958B26A7B5043328C73175D0102A2F ();
// 0x000003A6 System.Single MaterialUI.RectTransformSnap::get_flexibleHeight()
extern void RectTransformSnap_get_flexibleHeight_m0F9B9C5186CB2A80FCA4466769C7F12DE2BBF8E4 ();
// 0x000003A7 System.Int32 MaterialUI.RectTransformSnap::get_layoutPriority()
extern void RectTransformSnap_get_layoutPriority_m171E5AA95914EBAE85317F89E6802134C2C8F3BF ();
// 0x000003A8 System.Void MaterialUI.RectTransformSnap::.ctor()
extern void RectTransformSnap__ctor_mCE276E4646B5D02861FAC2009A25008B5149BE2B ();
// 0x000003A9 System.Boolean MaterialUI.MaterialRipple::get_ripplesEnabled()
extern void MaterialRipple_get_ripplesEnabled_m37F475C987E958ACBB0CE1866DAFF18183CDD43E ();
// 0x000003AA System.Void MaterialUI.MaterialRipple::set_ripplesEnabled(System.Boolean)
extern void MaterialRipple_set_ripplesEnabled_m32355BDB1A1335EE3F1CFFF440A0B4CC69589DD7 ();
// 0x000003AB System.Boolean MaterialUI.MaterialRipple::get_autoMatchGraphicColor()
extern void MaterialRipple_get_autoMatchGraphicColor_m82AF37DFB549B9ED8B314E009D8135411C255BD9 ();
// 0x000003AC System.Void MaterialUI.MaterialRipple::set_autoMatchGraphicColor(System.Boolean)
extern void MaterialRipple_set_autoMatchGraphicColor_mE643C70043C18399F1EEF6CD4C332C86D1560CCF ();
// 0x000003AD UnityEngine.UI.Graphic MaterialUI.MaterialRipple::get_colorMatchGraphic()
extern void MaterialRipple_get_colorMatchGraphic_m3A395A95BD7BBE08339DB1BC81D78033F41F1677 ();
// 0x000003AE System.Void MaterialUI.MaterialRipple::set_colorMatchGraphic(UnityEngine.UI.Graphic)
extern void MaterialRipple_set_colorMatchGraphic_mC6829FF2C75BACFA473BE6149DB38C9012894768 ();
// 0x000003AF MaterialUI.RippleData MaterialUI.MaterialRipple::get_rippleData()
extern void MaterialRipple_get_rippleData_mCD22847BAEB31C09FBF07897C241AAA0BE015C40 ();
// 0x000003B0 System.Void MaterialUI.MaterialRipple::set_rippleData(MaterialUI.RippleData)
extern void MaterialRipple_set_rippleData_m1780B2FEF3078D407102F192E4A560A4D9F179E4 ();
// 0x000003B1 UnityEngine.Color MaterialUI.MaterialRipple::get_rippleColor()
extern void MaterialRipple_get_rippleColor_mD2677CF78CBD1533C53B0A7ABAC7531BD12B5807 ();
// 0x000003B2 System.Void MaterialUI.MaterialRipple::set_rippleColor(UnityEngine.Color)
extern void MaterialRipple_set_rippleColor_m51B13539F9E0E69AC48FA2C4373F630A32747C17 ();
// 0x000003B3 UnityEngine.UI.Graphic MaterialUI.MaterialRipple::get_highlightGraphic()
extern void MaterialRipple_get_highlightGraphic_mA9488135A3AE1A2212F9E69D1BD6117863C19058 ();
// 0x000003B4 System.Void MaterialUI.MaterialRipple::set_highlightGraphic(UnityEngine.UI.Graphic)
extern void MaterialRipple_set_highlightGraphic_mC8C0F25F4BEB8667F3767ADD0223B3CD5940329D ();
// 0x000003B5 MaterialUI.MaterialRipple_HighlightActive MaterialUI.MaterialRipple::get_highlightWhen()
extern void MaterialRipple_get_highlightWhen_m7D56291537D04405425384D06E82DC899BFB2A0B ();
// 0x000003B6 System.Void MaterialUI.MaterialRipple::set_highlightWhen(MaterialUI.MaterialRipple_HighlightActive)
extern void MaterialRipple_set_highlightWhen_m9503AAD2C0773881475931EBA3572AAACFC0A22F ();
// 0x000003B7 System.Boolean MaterialUI.MaterialRipple::get_autoHighlightColor()
extern void MaterialRipple_get_autoHighlightColor_m7C844EEBCDB21CCCAF917B6BA5C57EF3483DF8AD ();
// 0x000003B8 System.Void MaterialUI.MaterialRipple::set_autoHighlightColor(System.Boolean)
extern void MaterialRipple_set_autoHighlightColor_m023B1E202CCE6C18DBA69537D20E311438F083E0 ();
// 0x000003B9 UnityEngine.Color MaterialUI.MaterialRipple::get_highlightColor()
extern void MaterialRipple_get_highlightColor_m06C20306F4811603350BC60E1D48E847B260A1CE ();
// 0x000003BA System.Void MaterialUI.MaterialRipple::set_highlightColor(UnityEngine.Color)
extern void MaterialRipple_set_highlightColor_m051F5B850F4A883114710A2F6A1E2077C5EBC2C7 ();
// 0x000003BB MaterialUI.MaterialRipple_MaskMode MaterialUI.MaterialRipple::get_maskMode()
extern void MaterialRipple_get_maskMode_mC35B00FA62C08DFF42F03085C6FFACE9EC445B43 ();
// 0x000003BC System.Void MaterialUI.MaterialRipple::set_maskMode(MaterialUI.MaterialRipple_MaskMode)
extern void MaterialRipple_set_maskMode_mF97EC862F1B7A337BD45E039B93BA6A53A2C95D3 ();
// 0x000003BD System.Boolean MaterialUI.MaterialRipple::get_checkForScroll()
extern void MaterialRipple_get_checkForScroll_m7D1DBC306226702C13A9CF9D5503FA8DF5268BD7 ();
// 0x000003BE System.Void MaterialUI.MaterialRipple::set_checkForScroll(System.Boolean)
extern void MaterialRipple_set_checkForScroll_m9BF0E2704F5CFE92E0F8C2C0414CD678E6A98836 ();
// 0x000003BF System.Int32 MaterialUI.MaterialRipple::get_rippleCount()
extern void MaterialRipple_get_rippleCount_m2B9858E9FE05B1768E2595997A4C7AA540B88000 ();
// 0x000003C0 UnityEngine.UI.Mask MaterialUI.MaterialRipple::get_mask()
extern void MaterialRipple_get_mask_m48FBAA34A73D351ECEF8A4BFE292A28240E22447 ();
// 0x000003C1 UnityEngine.UI.RectMask2D MaterialUI.MaterialRipple::get_rectMask2D()
extern void MaterialRipple_get_rectMask2D_m894832DC44F946E1C7E523788E5073505F128075 ();
// 0x000003C2 System.Single MaterialUI.MaterialRipple::get_autoHighlightBlendAmount()
extern void MaterialRipple_get_autoHighlightBlendAmount_mDCC3C45ECD50658231F93851AB4952184F73993C ();
// 0x000003C3 System.Void MaterialUI.MaterialRipple::set_autoHighlightBlendAmount(System.Single)
extern void MaterialRipple_set_autoHighlightBlendAmount_m0C6554FC2C631EFE9A2536ADF2E94434629957FF ();
// 0x000003C4 System.Void MaterialUI.MaterialRipple::Start()
extern void MaterialRipple_Start_m23DDBD04E448A8E896B7F3057669097D56976B67 ();
// 0x000003C5 System.Void MaterialUI.MaterialRipple::Update()
extern void MaterialRipple_Update_m74FB178DCE1C3F00C54DF0AA9350323371E760DD ();
// 0x000003C6 System.Void MaterialUI.MaterialRipple::CheckTransparency()
extern void MaterialRipple_CheckTransparency_m499D1B93066163C1312015B1E15AC61B4EA4FE94 ();
// 0x000003C7 System.Void MaterialUI.MaterialRipple::SetGraphicColor(UnityEngine.Color,System.Boolean)
extern void MaterialRipple_SetGraphicColor_m12D4C78945DBE7693255C82719125DFB9EE6E05C ();
// 0x000003C8 System.Void MaterialUI.MaterialRipple::RefreshSettings()
extern void MaterialRipple_RefreshSettings_mA6871EB9780A2299D210CFC735BF0904AF742F41 ();
// 0x000003C9 System.Void MaterialUI.MaterialRipple::RefreshTransparencySettings()
extern void MaterialRipple_RefreshTransparencySettings_m064C614D12AE68C9BCBB29F4FC411040A7A7A2B1 ();
// 0x000003CA System.Void MaterialUI.MaterialRipple::RefreshGraphicMatchColor()
extern void MaterialRipple_RefreshGraphicMatchColor_mA8BD2D150C07DD90F192324875A62E24D7756E47 ();
// 0x000003CB System.Void MaterialUI.MaterialRipple::RefreshAutoHighlightColor()
extern void MaterialRipple_RefreshAutoHighlightColor_mF85521ACA0CE1CA7C7A600977AF9811118A14998 ();
// 0x000003CC System.Void MaterialUI.MaterialRipple::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MaterialRipple_OnPointerEnter_mE19B705FC3B667D440D98FF6E7FA4E230AA880BE ();
// 0x000003CD System.Void MaterialUI.MaterialRipple::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void MaterialRipple_OnPointerDown_m1B40ECE006D21DE22FADE52C30ACCE0215C602D9 ();
// 0x000003CE System.Void MaterialUI.MaterialRipple::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void MaterialRipple_OnPointerUp_m1F6B4C9DF19ECF631F189BCED115B7ABD43BF8E9 ();
// 0x000003CF System.Void MaterialUI.MaterialRipple::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MaterialRipple_OnPointerExit_m38BBD55F746E727C874207365BA7AF136499793F ();
// 0x000003D0 System.Void MaterialUI.MaterialRipple::Highlight(System.Int32)
extern void MaterialRipple_Highlight_m40FBFF261964F1AD68975CEEAD4F4E3853641F1D ();
// 0x000003D1 System.Void MaterialUI.MaterialRipple::CreateRipple(UnityEngine.Vector2,System.Boolean)
extern void MaterialRipple_CreateRipple_m87A6E687D351ECFF8605B2EC2F6E27E0687FD447 ();
// 0x000003D2 System.Void MaterialUI.MaterialRipple::DestroyRipple()
extern void MaterialRipple_DestroyRipple_m46921A9D093D1D5424ABDCA2F4D7E56A942D8A0D ();
// 0x000003D3 System.Void MaterialUI.MaterialRipple::InstantDestroyAllRipples(System.Boolean)
extern void MaterialRipple_InstantDestroyAllRipples_m59B3BADBCEAB503260493FBADCE60BD644E00E75 ();
// 0x000003D4 System.Void MaterialUI.MaterialRipple::OnFirstRippleCreate()
extern void MaterialRipple_OnFirstRippleCreate_m7BCB156675A7DFA4A2E17AAE2C1481079A3141A0 ();
// 0x000003D5 System.Void MaterialUI.MaterialRipple::OnLastRippleDestroy()
extern void MaterialRipple_OnLastRippleDestroy_m9FBC8FFBF950D31AFE92C3EAD7C8A5159C1A9989 ();
// 0x000003D6 System.Void MaterialUI.MaterialRipple::OnCreateRipple()
extern void MaterialRipple_OnCreateRipple_m10653879934ADACCF21500294A46D0D181139510 ();
// 0x000003D7 System.Void MaterialUI.MaterialRipple::OnDestroyRipple()
extern void MaterialRipple_OnDestroyRipple_m2EE80D81A2B70B3224D7D8B057E4D66813FA66BA ();
// 0x000003D8 System.Collections.IEnumerator MaterialUI.MaterialRipple::ScrollCheck()
extern void MaterialRipple_ScrollCheck_m5734CEE69BAB1B68FD26B33DD5DD4EDCADBDCFB6 ();
// 0x000003D9 System.Collections.IEnumerator MaterialUI.MaterialRipple::ScrollCheckUp()
extern void MaterialRipple_ScrollCheckUp_m3E237E8DEAF9F6B7BCBCD61D5DB01CBCAFDE74A6 ();
// 0x000003DA System.Collections.IEnumerator MaterialUI.MaterialRipple::SelectCheck()
extern void MaterialRipple_SelectCheck_m9BEF63FE1D53CEC2889DE98DCC2B3033FC49899B ();
// 0x000003DB System.Void MaterialUI.MaterialRipple::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialRipple_OnSelect_m431BB267373252EFAC16138ED01D3B863E488546 ();
// 0x000003DC System.Void MaterialUI.MaterialRipple::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialRipple_OnDeselect_mBE60D1E498369D2E536B9B2E982327A1C248241D ();
// 0x000003DD System.Void MaterialUI.MaterialRipple::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void MaterialRipple_OnSubmit_m587FB342AFEB3B85082CB714AF2DCAC4EE83BB8B ();
// 0x000003DE System.Void MaterialUI.MaterialRipple::.ctor()
extern void MaterialRipple__ctor_m9037A8A2A2D4BDC27D190AFE84A2D8B198071A2B ();
// 0x000003DF System.Int32 MaterialUI.Ripple::get_id()
extern void Ripple_get_id_m846D684837CF5643897A2123FC3D7AFC6C447421 ();
// 0x000003E0 System.Void MaterialUI.Ripple::set_id(System.Int32)
extern void Ripple_set_id_m0010E583AACDD2E6CF0DBFDB1404CDB2A0533C3B ();
// 0x000003E1 MaterialUI.RippleData MaterialUI.Ripple::get_rippleData()
extern void Ripple_get_rippleData_mA0C4FF524320D46F0B36FD782BB4601EE349A5AF ();
// 0x000003E2 MaterialUI.MaterialUIScaler MaterialUI.Ripple::get_scaler()
extern void Ripple_get_scaler_mC8A88FEA8C960008E197D4E6AC026AAECEEE5E3E ();
// 0x000003E3 UnityEngine.CanvasGroup MaterialUI.Ripple::get_canvasGroup()
extern void Ripple_get_canvasGroup_mF0A897D97276F03B59180CCD53A1E9BE1260386B ();
// 0x000003E4 System.Void MaterialUI.Ripple::OnApplicationQuit()
extern void Ripple_OnApplicationQuit_m186B69A10FFF9E2AEE39C063BC46089C6AA96B73 ();
// 0x000003E5 System.Void MaterialUI.Ripple::ResetMaterial()
extern void Ripple_ResetMaterial_mB848C909B8FB0A8256FBF1A927549624B0B18430 ();
// 0x000003E6 System.Void MaterialUI.Ripple::Create(System.Int32,MaterialUI.VectorImageData)
extern void Ripple_Create_m2F0EF7EC6F8A0268BC079530E6CE094EA2A89A87 ();
// 0x000003E7 System.Void MaterialUI.Ripple::ClearData()
extern void Ripple_ClearData_m398776DDED5EB743CC05A30AA048B7E336783FB7 ();
// 0x000003E8 System.Void MaterialUI.Ripple::Setup(MaterialUI.RippleData,UnityEngine.Vector2,MaterialUI.IRippleCreator,System.Boolean)
extern void Ripple_Setup_m37BD46BDE3EE2BE0F984640B9EA561BADAAD0DE5 ();
// 0x000003E9 System.Single MaterialUI.Ripple::CalculateSize()
extern void Ripple_CalculateSize_m53C756C38EDB4D7A1C9FADC55B64996F22DDE885 ();
// 0x000003EA System.Void MaterialUI.Ripple::Expand()
extern void Ripple_Expand_m4C321754822C12A1DEB7D79B41182EACE0D1599F ();
// 0x000003EB System.Void MaterialUI.Ripple::Contract()
extern void Ripple_Contract_m807EEF099870F06FA9C47CDAAB6E2F76847BDF9C ();
// 0x000003EC System.Void MaterialUI.Ripple::InstantContract()
extern void Ripple_InstantContract_m8E5875162CC4D8D448BFA3ED80F8D4DD336587C1 ();
// 0x000003ED System.Void MaterialUI.Ripple::Update()
extern void Ripple_Update_m2D6806D7023517EF783E12DE0956F1D01558284A ();
// 0x000003EE System.Void MaterialUI.Ripple::LateUpdate()
extern void Ripple_LateUpdate_m27CB38C449B1ACF139A7F6F4C062F66CCBD87278 ();
// 0x000003EF System.Void MaterialUI.Ripple::SubmitSizeForSoftening()
extern void Ripple_SubmitSizeForSoftening_mCA092152E729A634D7A4423D5414E11B9DAE199E ();
// 0x000003F0 System.Void MaterialUI.Ripple::CalculateAverageSoftening()
extern void Ripple_CalculateAverageSoftening_mB3A1503580BF72F168FD529674C87D0CF4AE27E0 ();
// 0x000003F1 System.Single MaterialUI.Ripple::PowerExp(System.Single,System.Double[])
extern void Ripple_PowerExp_mF0909FFA813D949AEB71380B24ADB4D6087AB456 ();
// 0x000003F2 System.Void MaterialUI.Ripple::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void Ripple_OnPopulateMesh_mBC3AB0BE76ADECE69A5EEF779FB35A952037B675 ();
// 0x000003F3 System.Void MaterialUI.Ripple::.ctor()
extern void Ripple__ctor_m89AA7E00291F9E2DDAC17622C00DD5765380DBE0 ();
// 0x000003F4 System.Void MaterialUI.Ripple::.cctor()
extern void Ripple__cctor_mB7E4D52822A6A2A4C18965887C22F45680AA6B4E ();
// 0x000003F5 MaterialUI.RippleData MaterialUI.RippleData::Copy()
extern void RippleData_Copy_m7DD19AE6825B5A0E41DD44DEBBEB5C70125FFD8A ();
// 0x000003F6 System.Void MaterialUI.RippleData::.ctor()
extern void RippleData__ctor_m784A6C5B1E6A68429E285C8FC5B2380859E7FDA3 ();
// 0x000003F7 MaterialUI.RippleManager MaterialUI.RippleManager::get_instance()
extern void RippleManager_get_instance_m3608FC81DE356D51E057E5E4FD6F038E1CF83E1F ();
// 0x000003F8 MaterialUI.VectorImageData MaterialUI.RippleManager::get_rippleImageData()
extern void RippleManager_get_rippleImageData_mB03F9E945886922FAB3CC3297B750530206402B9 ();
// 0x000003F9 System.Void MaterialUI.RippleManager::OnApplicationQuit()
extern void RippleManager_OnApplicationQuit_m0B9E333F9FC47C916FFAF65ED7EEEFB36AB67630 ();
// 0x000003FA MaterialUI.Ripple MaterialUI.RippleManager::GetRipple()
extern void RippleManager_GetRipple_mB3A2DDE7145E3EDEFAE6F3FF817FC4BE17DBE704 ();
// 0x000003FB System.Void MaterialUI.RippleManager::CreateRipple()
extern void RippleManager_CreateRipple_mAF30148EFAEA47E110A58FEDDEED6DCDDDFB7CD8 ();
// 0x000003FC System.Void MaterialUI.RippleManager::ResetRipple(MaterialUI.Ripple)
extern void RippleManager_ResetRipple_m3088243652CF9A420289F5503685B57D2CAC314A ();
// 0x000003FD System.Void MaterialUI.RippleManager::ReleaseRipple(MaterialUI.Ripple)
extern void RippleManager_ReleaseRipple_m527C32D8ABA0492DF2BD516494A2B98BA4B8D097 ();
// 0x000003FE System.Void MaterialUI.RippleManager::.ctor()
extern void RippleManager__ctor_m8A787A32DB0F1FFE99DF71B54589FD80ACC6ECC2 ();
// 0x000003FF System.Boolean MaterialUI.AnimatedShadow::get_isVisible()
extern void AnimatedShadow_get_isVisible_m1B212C07EAC1417E2DA9CB1B399F2E98BFE1A27E ();
// 0x00000400 System.Void MaterialUI.AnimatedShadow::set_isVisible(System.Boolean)
extern void AnimatedShadow_set_isVisible_m9A90AB8C42228DBB4AB572EE114FE1B917D0E3AC ();
// 0x00000401 UnityEngine.CanvasGroup MaterialUI.AnimatedShadow::get_canvasGroup()
extern void AnimatedShadow_get_canvasGroup_m8D61B1A3607012E65A1C36C04AD191F20FBD764F ();
// 0x00000402 System.Void MaterialUI.AnimatedShadow::SetShadow(System.Boolean,System.Boolean)
extern void AnimatedShadow_SetShadow_m1BE7629A661C7817823E468FC48A846A9CA246E5 ();
// 0x00000403 System.Void MaterialUI.AnimatedShadow::SetShadow(System.Boolean,MaterialUI.Tween_TweenType,System.Boolean)
extern void AnimatedShadow_SetShadow_mFEE32F3088AD68953022CA6D7EA379220BAB5FDE ();
// 0x00000404 System.Void MaterialUI.AnimatedShadow::.ctor()
extern void AnimatedShadow__ctor_mE19889EC6B3414833E3C6AB561DAFB57C334F4A4 ();
// 0x00000405 MaterialUI.AnimatedShadow[] MaterialUI.MaterialShadow::get_animatedShadows()
extern void MaterialShadow_get_animatedShadows_m319C46EEE9A2D58CCFAFCFD0F5A1C404363A8C5A ();
// 0x00000406 System.Void MaterialUI.MaterialShadow::set_animatedShadows(MaterialUI.AnimatedShadow[])
extern void MaterialShadow_set_animatedShadows_mC60C4559C2A5A0E8B95C65668DD099A0D6F881C8 ();
// 0x00000407 System.Int32 MaterialUI.MaterialShadow::get_shadowNormalSize()
extern void MaterialShadow_get_shadowNormalSize_m7AEDBDA14E24F45AC8A4B6B8BEB650EA03844065 ();
// 0x00000408 System.Void MaterialUI.MaterialShadow::set_shadowNormalSize(System.Int32)
extern void MaterialShadow_set_shadowNormalSize_m290BF916B4E19EFBCF5D5B38405C4E6B9476EB2F ();
// 0x00000409 System.Int32 MaterialUI.MaterialShadow::get_shadowActiveSize()
extern void MaterialShadow_get_shadowActiveSize_m183CBD599578C7468273EF5D01EB62B9C1555B7A ();
// 0x0000040A System.Void MaterialUI.MaterialShadow::set_shadowActiveSize(System.Int32)
extern void MaterialShadow_set_shadowActiveSize_mA2264711A01476F4F54861270F062AAC7A0BE019 ();
// 0x0000040B MaterialUI.MaterialShadow_ShadowsActive MaterialUI.MaterialShadow::get_shadowsActiveWhen()
extern void MaterialShadow_get_shadowsActiveWhen_m89F024A388A3B13D3EC8EC9E1E629B33C443A940 ();
// 0x0000040C System.Void MaterialUI.MaterialShadow::set_shadowsActiveWhen(MaterialUI.MaterialShadow_ShadowsActive)
extern void MaterialShadow_set_shadowsActiveWhen_m486C23BE7553DBCD0ED7C6E297FEE4C5494ED06B ();
// 0x0000040D System.Boolean MaterialUI.MaterialShadow::get_isEnabled()
extern void MaterialShadow_get_isEnabled_mCAC28828029333BBE51C38FD111D6309E0E604F4 ();
// 0x0000040E System.Void MaterialUI.MaterialShadow::set_isEnabled(System.Boolean)
extern void MaterialShadow_set_isEnabled_m42857901BAFCD722962B88470F97AC5B49A8975C ();
// 0x0000040F System.Void MaterialUI.MaterialShadow::Update()
extern void MaterialShadow_Update_mE40C78992D32CFBCF6DBC989E56F564ABC05EBE0 ();
// 0x00000410 System.Void MaterialUI.MaterialShadow::OnDestroy()
extern void MaterialShadow_OnDestroy_m5E31B2CCF1BD9DAE528835875FDCFFC85CCE9209 ();
// 0x00000411 System.Void MaterialUI.MaterialShadow::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void MaterialShadow_OnPointerDown_m3B667F8526CEEA9B27F19244099F446FC65F3679 ();
// 0x00000412 System.Void MaterialUI.MaterialShadow::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void MaterialShadow_OnPointerUp_m80593F8D2CB4784509E7B434DEFD562945E0A416 ();
// 0x00000413 System.Void MaterialUI.MaterialShadow::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MaterialShadow_OnPointerEnter_m1C3296A6957503001E99C6410E7B0BAA7A209D59 ();
// 0x00000414 System.Void MaterialUI.MaterialShadow::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MaterialShadow_OnPointerExit_mA45C3E4017CE156DE5DEF337F8EA8C03D76C4989 ();
// 0x00000415 System.Void MaterialUI.MaterialShadow::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialShadow_OnSelect_m538C9CEADAD09C0496E9CE4246C82F85DB4E3F81 ();
// 0x00000416 System.Void MaterialUI.MaterialShadow::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialShadow_OnDeselect_mEAE3FE9FDD404F437C9A34CD9F19EDE406F17AE5 ();
// 0x00000417 System.Void MaterialUI.MaterialShadow::SetShadows(System.Int32)
extern void MaterialShadow_SetShadows_mC245B221089CA17C033843578AEFDA39BA7584EE ();
// 0x00000418 System.Void MaterialUI.MaterialShadow::SetShadowsInstant()
extern void MaterialShadow_SetShadowsInstant_mA388E87F0C972BC0A122162D201E55DAE8B199D1 ();
// 0x00000419 System.Void MaterialUI.MaterialShadow::.ctor()
extern void MaterialShadow__ctor_m6BE92663F2EB025917BCD571B46BCE246F354EC8 ();
// 0x0000041A UnityEngine.UI.Image MaterialUI.SpriteSwapper::get_targetImage()
extern void SpriteSwapper_get_targetImage_m293B917F638D0333B78B898AF8D1E51C81A332FB ();
// 0x0000041B System.Void MaterialUI.SpriteSwapper::set_targetImage(UnityEngine.UI.Image)
extern void SpriteSwapper_set_targetImage_m3E9B4747EB885080D8F92FF87A682598D1DDDD83 ();
// 0x0000041C MaterialUI.MaterialUIScaler MaterialUI.SpriteSwapper::get_rootScaler()
extern void SpriteSwapper_get_rootScaler_m534798369AEDFDF53A4B64B39AF834D93E337B64 ();
// 0x0000041D UnityEngine.Sprite MaterialUI.SpriteSwapper::get_sprite1X()
extern void SpriteSwapper_get_sprite1X_mA2155D36645AF7AFE751580695ADEDDF5619A558 ();
// 0x0000041E System.Void MaterialUI.SpriteSwapper::set_sprite1X(UnityEngine.Sprite)
extern void SpriteSwapper_set_sprite1X_mB2B76AFD0A1F9AC6E24AC79C9B2C5E0F04E267E9 ();
// 0x0000041F UnityEngine.Sprite MaterialUI.SpriteSwapper::get_sprite2X()
extern void SpriteSwapper_get_sprite2X_m7CE65695D092DF1FB215074925806AFBA5F4548D ();
// 0x00000420 System.Void MaterialUI.SpriteSwapper::set_sprite2X(UnityEngine.Sprite)
extern void SpriteSwapper_set_sprite2X_m1467ED95640A657E75650184FD7327289FA03A5D ();
// 0x00000421 UnityEngine.Sprite MaterialUI.SpriteSwapper::get_sprite4X()
extern void SpriteSwapper_get_sprite4X_m4809B344F5C190DCAF8CD67304C8BFADF0647A72 ();
// 0x00000422 System.Void MaterialUI.SpriteSwapper::set_sprite4X(UnityEngine.Sprite)
extern void SpriteSwapper_set_sprite4X_m0E1B7AB1746CDB9BC8778D9922AC7ED3B7E7C4F7 ();
// 0x00000423 System.Void MaterialUI.SpriteSwapper::OnEnable()
extern void SpriteSwapper_OnEnable_mE9EDEDF8A691285D394B90EBE7FB2574EBEC7983 ();
// 0x00000424 System.Void MaterialUI.SpriteSwapper::Start()
extern void SpriteSwapper_Start_mD0C8D7A87135A8102EA46F9A54492D36711704F7 ();
// 0x00000425 System.Void MaterialUI.SpriteSwapper::RefreshSprite()
extern void SpriteSwapper_RefreshSprite_m6774B259A7488152418B8E10439402B875A081C8 ();
// 0x00000426 System.Void MaterialUI.SpriteSwapper::SwapSprite(System.Single)
extern void SpriteSwapper_SwapSprite_m47B5B7E137329732B9FC52CCEB0DB345E1EBC201 ();
// 0x00000427 System.Void MaterialUI.SpriteSwapper::.ctor()
extern void SpriteSwapper__ctor_m505D95F671585A86CA726162C7D748878B1697FC ();
// 0x00000428 System.Void MaterialUI.SpriteSwapper::<Start>b__23_0(System.Boolean,System.Boolean)
extern void SpriteSwapper_U3CStartU3Eb__23_0_mE17275B5E6260770FA4B7BCF0AA1F398A1038A1D ();
// 0x00000429 System.Single MaterialUI.ToggleBase::get_animationDuration()
extern void ToggleBase_get_animationDuration_m789775EE0CF2FD9C89EE27FAF63F5D39DCD46CA1 ();
// 0x0000042A System.Void MaterialUI.ToggleBase::set_animationDuration(System.Single)
extern void ToggleBase_set_animationDuration_mF65FEF608CC124C7543C1032CCA47643E0AA6747 ();
// 0x0000042B UnityEngine.Color MaterialUI.ToggleBase::get_onColor()
extern void ToggleBase_get_onColor_mC6186142820492783801A6A1FCC38A2474A042F0 ();
// 0x0000042C System.Void MaterialUI.ToggleBase::set_onColor(UnityEngine.Color)
extern void ToggleBase_set_onColor_m738907C6BD910D12B43619C4867F3E00D2F385B5 ();
// 0x0000042D UnityEngine.Color MaterialUI.ToggleBase::get_offColor()
extern void ToggleBase_get_offColor_m723A794F2A3EE2E63C3A08BDFDC4574BB362D871 ();
// 0x0000042E System.Void MaterialUI.ToggleBase::set_offColor(UnityEngine.Color)
extern void ToggleBase_set_offColor_m632A260D8A6784CC12860461D47217CE6756F779 ();
// 0x0000042F UnityEngine.Color MaterialUI.ToggleBase::get_disabledColor()
extern void ToggleBase_get_disabledColor_m10C5638D3CA8D7B5035754B43F35F5DC7702A611 ();
// 0x00000430 System.Void MaterialUI.ToggleBase::set_disabledColor(UnityEngine.Color)
extern void ToggleBase_set_disabledColor_mF0CE39B97D4380DB9178CC4764105EC1CE7737D6 ();
// 0x00000431 System.Boolean MaterialUI.ToggleBase::get_changeGraphicColor()
extern void ToggleBase_get_changeGraphicColor_mB3C92D0A6590D068623E54914CF3FB1BF8A02C96 ();
// 0x00000432 System.Void MaterialUI.ToggleBase::set_changeGraphicColor(System.Boolean)
extern void ToggleBase_set_changeGraphicColor_m7BFDA25B9311E25CC4BFF31978265D005713A3F3 ();
// 0x00000433 UnityEngine.Color MaterialUI.ToggleBase::get_graphicOnColor()
extern void ToggleBase_get_graphicOnColor_m834D801726E792B63D6460D964F7295691E59DE8 ();
// 0x00000434 System.Void MaterialUI.ToggleBase::set_graphicOnColor(UnityEngine.Color)
extern void ToggleBase_set_graphicOnColor_mC9E281BE32C39AD3987D5CB6D94157A3E7305710 ();
// 0x00000435 UnityEngine.Color MaterialUI.ToggleBase::get_graphicOffColor()
extern void ToggleBase_get_graphicOffColor_m3281E3AF881431E90FE92BC489722F78C02C56C1 ();
// 0x00000436 System.Void MaterialUI.ToggleBase::set_graphicOffColor(UnityEngine.Color)
extern void ToggleBase_set_graphicOffColor_m8CCE1B37893E55014B2C7E1D384137EEEEBFB5F4 ();
// 0x00000437 UnityEngine.Color MaterialUI.ToggleBase::get_graphicDisabledColor()
extern void ToggleBase_get_graphicDisabledColor_m18BFE6E833048C4754ACF84FB12678F373275192 ();
// 0x00000438 System.Void MaterialUI.ToggleBase::set_graphicDisabledColor(UnityEngine.Color)
extern void ToggleBase_set_graphicDisabledColor_m07DE84E9D39039EB182C6C08CC1CA3A692D7845F ();
// 0x00000439 System.Boolean MaterialUI.ToggleBase::get_changeRippleColor()
extern void ToggleBase_get_changeRippleColor_mEC4C9252D3280F902A1C735D9D81BD6BCA71C0CE ();
// 0x0000043A System.Void MaterialUI.ToggleBase::set_changeRippleColor(System.Boolean)
extern void ToggleBase_set_changeRippleColor_mAFA22732E1C857C2ADF50BDB97740A2059BF58DD ();
// 0x0000043B UnityEngine.Color MaterialUI.ToggleBase::get_rippleOnColor()
extern void ToggleBase_get_rippleOnColor_m6491A995C8C1CB82E12ED9AD2ADF346D3CDDABEA ();
// 0x0000043C System.Void MaterialUI.ToggleBase::set_rippleOnColor(UnityEngine.Color)
extern void ToggleBase_set_rippleOnColor_mB552804F6898FEF025527A5A8632876134C2E141 ();
// 0x0000043D UnityEngine.Color MaterialUI.ToggleBase::get_rippleOffColor()
extern void ToggleBase_get_rippleOffColor_mE5C4FCA403C06CC3FE219438A0B9CA1B52736FCD ();
// 0x0000043E System.Void MaterialUI.ToggleBase::set_rippleOffColor(UnityEngine.Color)
extern void ToggleBase_set_rippleOffColor_m748BB117385BD2DEA1819CDE927C86287B965610 ();
// 0x0000043F UnityEngine.UI.Graphic MaterialUI.ToggleBase::get_graphic()
extern void ToggleBase_get_graphic_m0BDDE8BF8805C0B7D8548195ABAAC348733A2C4F ();
// 0x00000440 System.Void MaterialUI.ToggleBase::set_graphic(UnityEngine.UI.Graphic)
extern void ToggleBase_set_graphic_mE2B99F9DC01F1D84816A51172126AB870AFC2A7B ();
// 0x00000441 System.Boolean MaterialUI.ToggleBase::get_toggleGraphic()
extern void ToggleBase_get_toggleGraphic_mA30488E5C407B18399EEE0C1EFBD5C52983F6215 ();
// 0x00000442 System.Void MaterialUI.ToggleBase::set_toggleGraphic(System.Boolean)
extern void ToggleBase_set_toggleGraphic_m433B8D68EC9E122E1B49794FE921AC09CDC6FD02 ();
// 0x00000443 System.String MaterialUI.ToggleBase::get_toggleOnLabel()
extern void ToggleBase_get_toggleOnLabel_mEFF5D31F62241500C633FC2E6772FF09729F6941 ();
// 0x00000444 System.Void MaterialUI.ToggleBase::set_toggleOnLabel(System.String)
extern void ToggleBase_set_toggleOnLabel_m01AB942302CD3DC1B016DE9A54C096E7A155B0B2 ();
// 0x00000445 System.String MaterialUI.ToggleBase::get_toggleOffLabel()
extern void ToggleBase_get_toggleOffLabel_m7366C043A301CC419F9E3654978489439FE00427 ();
// 0x00000446 System.Void MaterialUI.ToggleBase::set_toggleOffLabel(System.String)
extern void ToggleBase_set_toggleOffLabel_m02BC510254A81A4B8B65FADDFDF537F22228B4CF ();
// 0x00000447 MaterialUI.ImageData MaterialUI.ToggleBase::get_toggleOnIcon()
extern void ToggleBase_get_toggleOnIcon_mFB84DC18B4DFAD741E259805EA0644FBFE500E65 ();
// 0x00000448 System.Void MaterialUI.ToggleBase::set_toggleOnIcon(MaterialUI.ImageData)
extern void ToggleBase_set_toggleOnIcon_mEF42848DF7E26A7B3F9A5D2B291B8A817BBEB4D5 ();
// 0x00000449 MaterialUI.ImageData MaterialUI.ToggleBase::get_toggleOffIcon()
extern void ToggleBase_get_toggleOffIcon_mE39A65731D0AFBAB1297B33441EF51E85CACAD59 ();
// 0x0000044A System.Void MaterialUI.ToggleBase::set_toggleOffIcon(MaterialUI.ImageData)
extern void ToggleBase_set_toggleOffIcon_m43415A44544FCB5A2EADD01CDD9FA1FEAD2806CD ();
// 0x0000044B MaterialUI.MaterialRipple MaterialUI.ToggleBase::get_materialRipple()
extern void ToggleBase_get_materialRipple_mA3734736D861D92DAE7DF904CB83301D776EFC54 ();
// 0x0000044C System.Void MaterialUI.ToggleBase::set_materialRipple(MaterialUI.MaterialRipple)
extern void ToggleBase_set_materialRipple_m50F7543C273835B976EE156414AD910BFDA66C00 ();
// 0x0000044D UnityEngine.UI.Toggle MaterialUI.ToggleBase::get_toggle()
extern void ToggleBase_get_toggle_mCFAD1FC2AD44632B1BB933C583F6D237B5D3FA15 ();
// 0x0000044E System.Void MaterialUI.ToggleBase::set_toggle(UnityEngine.UI.Toggle)
extern void ToggleBase_set_toggle_m56F61160A4B917232B67304B825F189197CB267B ();
// 0x0000044F System.String MaterialUI.ToggleBase::get_labelText()
extern void ToggleBase_get_labelText_m0DB76AAA9C089898686FB77463FFD690E7C5F118 ();
// 0x00000450 System.Void MaterialUI.ToggleBase::set_labelText(System.String)
extern void ToggleBase_set_labelText_m8884F192999C13039692D17F226644D61757287B ();
// 0x00000451 MaterialUI.ImageData MaterialUI.ToggleBase::get_icon()
extern void ToggleBase_get_icon_mF6E2C235BEB4DBEB51B2B8A4548BFA89FB4D6281 ();
// 0x00000452 System.Void MaterialUI.ToggleBase::set_icon(MaterialUI.ImageData)
extern void ToggleBase_set_icon_m76F7475A49EF54CC9501D5787211911B390AF658 ();
// 0x00000453 UnityEngine.CanvasGroup MaterialUI.ToggleBase::get_canvasGroup()
extern void ToggleBase_get_canvasGroup_mAD9DDF303117794F3ADF68A47A0CCBF8F5F19C94 ();
// 0x00000454 System.Boolean MaterialUI.ToggleBase::get_interactable()
extern void ToggleBase_get_interactable_m4C0DB5013AEF451D193975DABBE693C5E5688CE1 ();
// 0x00000455 System.Void MaterialUI.ToggleBase::set_interactable(System.Boolean)
extern void ToggleBase_set_interactable_m8428723738209E6D83F54561ED484F9C15185B7A ();
// 0x00000456 System.Void MaterialUI.ToggleBase::OnEnable()
extern void ToggleBase_OnEnable_m24BAD161D03C79032CB2E46D98A3B1C36D8A8717 ();
// 0x00000457 System.Void MaterialUI.ToggleBase::Start()
extern void ToggleBase_Start_mE3CC2C0B5DD7DA8567646D78F780A0D386DBD743 ();
// 0x00000458 System.Void MaterialUI.ToggleBase::Toggle()
extern void ToggleBase_Toggle_mEAFC13965C59FEBA755AC1E1C67FCB855D9F3036 ();
// 0x00000459 System.Void MaterialUI.ToggleBase::UpdateGraphicToggleState()
extern void ToggleBase_UpdateGraphicToggleState_m2C912F8565C59069009C20711393D05AB2F943CA ();
// 0x0000045A System.Void MaterialUI.ToggleBase::UpdateIconDataType()
extern void ToggleBase_UpdateIconDataType_m60C7CB13A54849F7BF1C0D1B7969E2A3164BB85A ();
// 0x0000045B System.Void MaterialUI.ToggleBase::TurnOn()
extern void ToggleBase_TurnOn_mDC885CE7900A31F6EB41A25BF9D3DAED112177B3 ();
// 0x0000045C System.Void MaterialUI.ToggleBase::TurnOnInstant()
extern void ToggleBase_TurnOnInstant_m2A39216359268053C120D61A426199C99EAFA41E ();
// 0x0000045D System.Void MaterialUI.ToggleBase::TurnOff()
extern void ToggleBase_TurnOff_mC85B842D92B85E23C6E595CAB09B1FC7F37DE189 ();
// 0x0000045E System.Void MaterialUI.ToggleBase::TurnOffInstant()
extern void ToggleBase_TurnOffInstant_m497FF3FCD2EA389C47A30736E9595F7E1BDBC7A3 ();
// 0x0000045F System.Void MaterialUI.ToggleBase::Enable()
extern void ToggleBase_Enable_mD50857192A855CCD6276B01A82917D93A1657DDF ();
// 0x00000460 System.Void MaterialUI.ToggleBase::Disable()
extern void ToggleBase_Disable_m8FCB21DE87D4DA0CED110C7C257745392E2E28FB ();
// 0x00000461 System.Void MaterialUI.ToggleBase::Update()
extern void ToggleBase_Update_m73ED0D5F00911E9505DBB4F33C29CCC947A5DFE4 ();
// 0x00000462 System.Void MaterialUI.ToggleBase::AnimOn()
extern void ToggleBase_AnimOn_m0D90C84AFF8B54A86567634F0C5867724C3B5EBD ();
// 0x00000463 System.Void MaterialUI.ToggleBase::AnimOnComplete()
extern void ToggleBase_AnimOnComplete_mE81E3D1B180176E580E48E47CEC18A0AD9FA7DAC ();
// 0x00000464 System.Void MaterialUI.ToggleBase::AnimOff()
extern void ToggleBase_AnimOff_m2E0D15E232AE28A8F8C0700235A42A6827F9011D ();
// 0x00000465 System.Void MaterialUI.ToggleBase::AnimOffComplete()
extern void ToggleBase_AnimOffComplete_m1731EAF49C177F2276A784BAC34B56062499D37D ();
// 0x00000466 System.Void MaterialUI.ToggleBase::SetOnColor()
extern void ToggleBase_SetOnColor_m6D57AA9ADABF4C33CF12858C77240EE01848BC1B ();
// 0x00000467 System.Void MaterialUI.ToggleBase::SetOffColor()
extern void ToggleBase_SetOffColor_mD0E2EC03A8B5B4D78B6187D5C9271ED4FCD3C61F ();
// 0x00000468 System.Void MaterialUI.ToggleBase::.ctor()
extern void ToggleBase__ctor_m227C61686A955FCE2FF6C3DB249122C80222CF93 ();
// 0x00000469 System.Single MaterialUI.VerticalScrollLayoutElement::get_maxHeight()
extern void VerticalScrollLayoutElement_get_maxHeight_m85AAA0D3800636C76BA0CD00BF3C630877A9848F ();
// 0x0000046A System.Void MaterialUI.VerticalScrollLayoutElement::set_maxHeight(System.Single)
extern void VerticalScrollLayoutElement_set_maxHeight_m31633131A56AF1791856932078990BB3E28D60F2 ();
// 0x0000046B UnityEngine.RectTransform MaterialUI.VerticalScrollLayoutElement::get_contentRectTransform()
extern void VerticalScrollLayoutElement_get_contentRectTransform_m87FC0BB1D5CF521C84051644CF645135CB69DFE9 ();
// 0x0000046C System.Void MaterialUI.VerticalScrollLayoutElement::set_contentRectTransform(UnityEngine.RectTransform)
extern void VerticalScrollLayoutElement_set_contentRectTransform_m7F2F1A2B89C3721271065C4FA90FB014ECEF6C0A ();
// 0x0000046D UnityEngine.UI.ScrollRect MaterialUI.VerticalScrollLayoutElement::get_scrollRect()
extern void VerticalScrollLayoutElement_get_scrollRect_m07F2DC33CF4C2787CB9043A6453E886B666634D6 ();
// 0x0000046E System.Void MaterialUI.VerticalScrollLayoutElement::set_scrollRect(UnityEngine.UI.ScrollRect)
extern void VerticalScrollLayoutElement_set_scrollRect_mFE8A53C20B391DA9BE13DC2DBB900A392CAA9210 ();
// 0x0000046F UnityEngine.RectTransform MaterialUI.VerticalScrollLayoutElement::get_scrollRectTransform()
extern void VerticalScrollLayoutElement_get_scrollRectTransform_mB0872616662FA697921EFC740EB82BEC58CA2AFD ();
// 0x00000470 System.Void MaterialUI.VerticalScrollLayoutElement::set_scrollRectTransform(UnityEngine.RectTransform)
extern void VerticalScrollLayoutElement_set_scrollRectTransform_m17B17ED0782D12B6C4ABDCF195137D9D553A60A7 ();
// 0x00000471 UnityEngine.UI.Image MaterialUI.VerticalScrollLayoutElement::get_scrollHandleImage()
extern void VerticalScrollLayoutElement_get_scrollHandleImage_m06CBF64CCDB063728433AD8D249725A9E431D33A ();
// 0x00000472 System.Void MaterialUI.VerticalScrollLayoutElement::set_scrollHandleImage(UnityEngine.UI.Image)
extern void VerticalScrollLayoutElement_set_scrollHandleImage_mDF0CD019A50ED849FA02FB6D7EDDE9EAB29CC5C4 ();
// 0x00000473 UnityEngine.UI.ScrollRect_MovementType MaterialUI.VerticalScrollLayoutElement::get_movementTypeWhenScrollable()
extern void VerticalScrollLayoutElement_get_movementTypeWhenScrollable_mC4B0B83676D7A10B3A64BDB31FEF4DCC2CB98BEA ();
// 0x00000474 System.Void MaterialUI.VerticalScrollLayoutElement::set_movementTypeWhenScrollable(UnityEngine.UI.ScrollRect_MovementType)
extern void VerticalScrollLayoutElement_set_movementTypeWhenScrollable_m0D9EB06AA24F84853377C2E9669ECF32630F9427 ();
// 0x00000475 System.Boolean MaterialUI.VerticalScrollLayoutElement::get_scrollEnabled()
extern void VerticalScrollLayoutElement_get_scrollEnabled_m343BF753498009B8E75ECFBEE78E7E3B507F1EAB ();
// 0x00000476 System.Void MaterialUI.VerticalScrollLayoutElement::RefreshLayout()
extern void VerticalScrollLayoutElement_RefreshLayout_m281482B846463D17DA28DC3FDDA30C1657CE870B ();
// 0x00000477 System.Void MaterialUI.VerticalScrollLayoutElement::CalculateLayoutInputHorizontal()
extern void VerticalScrollLayoutElement_CalculateLayoutInputHorizontal_m0BAD423F3ECC29DAC59577F847CB37C0062FB351 ();
// 0x00000478 System.Void MaterialUI.VerticalScrollLayoutElement::CalculateLayoutInputVertical()
extern void VerticalScrollLayoutElement_CalculateLayoutInputVertical_mCE243AD84307C5BBB786514250E6366D0E42677F ();
// 0x00000479 System.Single MaterialUI.VerticalScrollLayoutElement::get_minWidth()
extern void VerticalScrollLayoutElement_get_minWidth_m7AA28AE2BC17388A7CA17557A6D354380F22E32D ();
// 0x0000047A System.Single MaterialUI.VerticalScrollLayoutElement::get_preferredWidth()
extern void VerticalScrollLayoutElement_get_preferredWidth_mE26636931D4D0D9DB888A30AEEBC4183274127C8 ();
// 0x0000047B System.Single MaterialUI.VerticalScrollLayoutElement::get_flexibleWidth()
extern void VerticalScrollLayoutElement_get_flexibleWidth_m89143B6FAD6A74B0A3FEB9672D09930501D6FBB3 ();
// 0x0000047C System.Single MaterialUI.VerticalScrollLayoutElement::get_minHeight()
extern void VerticalScrollLayoutElement_get_minHeight_mDADD4AA4BBA27F00FF5A07C38EE146194AD57CD6 ();
// 0x0000047D System.Single MaterialUI.VerticalScrollLayoutElement::get_preferredHeight()
extern void VerticalScrollLayoutElement_get_preferredHeight_mED91FB17E2F951F55C39D579FDC95C573F3D6E83 ();
// 0x0000047E System.Single MaterialUI.VerticalScrollLayoutElement::get_flexibleHeight()
extern void VerticalScrollLayoutElement_get_flexibleHeight_mA74D5E83707885981AA6AD80A4D0DDBA68F7CEA5 ();
// 0x0000047F System.Int32 MaterialUI.VerticalScrollLayoutElement::get_layoutPriority()
extern void VerticalScrollLayoutElement_get_layoutPriority_mB5A3A6E75C64B0D6EB1745CC498A7995F93AD286 ();
// 0x00000480 System.Void MaterialUI.VerticalScrollLayoutElement::.ctor()
extern void VerticalScrollLayoutElement__ctor_m3A09A8D49CDB8AC7F2FCB8FD5CDDCAAFB4580DC4 ();
// 0x00000481 UnityEngine.RectTransform MaterialUI.MaterialButton::get_rectTransform()
extern void MaterialButton_get_rectTransform_m5E64A8CD44F3D12F8268DFEBFCA3EF944BA1A91F ();
// 0x00000482 UnityEngine.RectTransform MaterialUI.MaterialButton::get_contentRectTransform()
extern void MaterialButton_get_contentRectTransform_m35CA1E3008A7537CF66C0F46F513775A8637732F ();
// 0x00000483 System.Void MaterialUI.MaterialButton::set_contentRectTransform(UnityEngine.RectTransform)
extern void MaterialButton_set_contentRectTransform_m6915C9C44D9A7FB032E286975C47E69BC28B07B3 ();
// 0x00000484 UnityEngine.UI.Button MaterialUI.MaterialButton::get_buttonObject()
extern void MaterialButton_get_buttonObject_m4B9474F9F2DC4A70E678F1AAFDB23273E81BFCE4 ();
// 0x00000485 UnityEngine.UI.Graphic MaterialUI.MaterialButton::get_backgroundImage()
extern void MaterialButton_get_backgroundImage_mD217DA4D42C0142417E35E77865140107E2BB3A9 ();
// 0x00000486 System.Void MaterialUI.MaterialButton::set_backgroundImage(UnityEngine.UI.Graphic)
extern void MaterialButton_set_backgroundImage_mEF7DDEE56383B8A56BBA304480D620BE3146CFFB ();
// 0x00000487 UnityEngine.UI.Text MaterialUI.MaterialButton::get_text()
extern void MaterialButton_get_text_m7F9F611FF7C59EE2A8016DA39C813A70032D96C3 ();
// 0x00000488 System.Void MaterialUI.MaterialButton::set_text(UnityEngine.UI.Text)
extern void MaterialButton_set_text_m7F83A74769D5AF439FC2776A2C916D97EA6EC64F ();
// 0x00000489 UnityEngine.UI.Graphic MaterialUI.MaterialButton::get_icon()
extern void MaterialButton_get_icon_m3ECC5B065C3666693E3E51A4F45F8E781DD21872 ();
// 0x0000048A System.Void MaterialUI.MaterialButton::set_icon(UnityEngine.UI.Graphic)
extern void MaterialButton_set_icon_mE2DBDA172D41EC108BE987E64584E7926F7EE74B ();
// 0x0000048B MaterialUI.MaterialRipple MaterialUI.MaterialButton::get_materialRipple()
extern void MaterialButton_get_materialRipple_m4DA6B5AC31A9A5A882C9F135D6B9BE49FF5DBFC8 ();
// 0x0000048C MaterialUI.MaterialShadow MaterialUI.MaterialButton::get_materialShadow()
extern void MaterialButton_get_materialShadow_m29A77180D187967B0D5E18ECFD1211F4FD4AE608 ();
// 0x0000048D UnityEngine.CanvasGroup MaterialUI.MaterialButton::get_canvasGroup()
extern void MaterialButton_get_canvasGroup_mFE7CE83D4B3F76D985BC0FD778C440AF160B8DF4 ();
// 0x0000048E UnityEngine.CanvasGroup MaterialUI.MaterialButton::get_shadowsCanvasGroup()
extern void MaterialButton_get_shadowsCanvasGroup_mD63594CB4BDC42A4E0DE9FF066AB2F6F9473E209 ();
// 0x0000048F System.Void MaterialUI.MaterialButton::set_shadowsCanvasGroup(UnityEngine.CanvasGroup)
extern void MaterialButton_set_shadowsCanvasGroup_m1490ABF5E67DFA0C9AC9A7641EE4D6662E265690 ();
// 0x00000490 System.Boolean MaterialUI.MaterialButton::get_interactable()
extern void MaterialButton_get_interactable_m7414D7FB51A9794B1DE6E8580BAAC9844FE03101 ();
// 0x00000491 System.Void MaterialUI.MaterialButton::set_interactable(System.Boolean)
extern void MaterialButton_set_interactable_mDDD3461029EC1282C45F5783697F7663D4B7A21C ();
// 0x00000492 UnityEngine.Vector2 MaterialUI.MaterialButton::get_contentPadding()
extern void MaterialButton_get_contentPadding_mE50D5C6127E2D3652FE26EE787E88F7500FB9F8B ();
// 0x00000493 System.Void MaterialUI.MaterialButton::set_contentPadding(UnityEngine.Vector2)
extern void MaterialButton_set_contentPadding_m5CEED1A0A453F854FE781703C50AFBE9A12F48D1 ();
// 0x00000494 UnityEngine.Vector2 MaterialUI.MaterialButton::get_contentSize()
extern void MaterialButton_get_contentSize_m7C825DB236CD44549746BD4869F74268A2A0ACDF ();
// 0x00000495 UnityEngine.Vector2 MaterialUI.MaterialButton::get_size()
extern void MaterialButton_get_size_mB8FE03E6D7479567D6A994FD34EB04F9BA2D76DD ();
// 0x00000496 System.Boolean MaterialUI.MaterialButton::get_fitWidthToContent()
extern void MaterialButton_get_fitWidthToContent_m8F7B336A62300190CF930A475DCF2C05A008BE01 ();
// 0x00000497 System.Void MaterialUI.MaterialButton::set_fitWidthToContent(System.Boolean)
extern void MaterialButton_set_fitWidthToContent_m03CADBFEA5FAC388EA889E8BA6922F62BD621E09 ();
// 0x00000498 System.Boolean MaterialUI.MaterialButton::get_fitHeightToContent()
extern void MaterialButton_get_fitHeightToContent_m44982D032E209F2FAC873C845A03810D14252752 ();
// 0x00000499 System.Void MaterialUI.MaterialButton::set_fitHeightToContent(System.Boolean)
extern void MaterialButton_set_fitHeightToContent_mA4908F3EF522A1407F45FA6B187EB33C9481F84C ();
// 0x0000049A System.Boolean MaterialUI.MaterialButton::get_isCircularButton()
extern void MaterialButton_get_isCircularButton_mC5763BF4405350F78F7A41C3ACCAF42ED84A4419 ();
// 0x0000049B System.Void MaterialUI.MaterialButton::set_isCircularButton(System.Boolean)
extern void MaterialButton_set_isCircularButton_m06C41DA812E119B2BA7D75B5FBAE9EC7A97A2AC9 ();
// 0x0000049C System.Boolean MaterialUI.MaterialButton::get_isRaisedButton()
extern void MaterialButton_get_isRaisedButton_m6DBECFB46D70217E1B917A9B4BDA8BB249848F9F ();
// 0x0000049D System.Void MaterialUI.MaterialButton::set_isRaisedButton(System.Boolean)
extern void MaterialButton_set_isRaisedButton_m83AE762A5150B3C8E5BB7A984BB84778B41C7BE4 ();
// 0x0000049E System.Boolean MaterialUI.MaterialButton::get_resetRippleOnDisable()
extern void MaterialButton_get_resetRippleOnDisable_m563CA504B49E9AC81B70D87295C9F177BC696BF4 ();
// 0x0000049F System.Void MaterialUI.MaterialButton::set_resetRippleOnDisable(System.Boolean)
extern void MaterialButton_set_resetRippleOnDisable_m63F90BC081C3A9C89F2A587D9D4C2E738AF573F4 ();
// 0x000004A0 System.String MaterialUI.MaterialButton::get_textText()
extern void MaterialButton_get_textText_mF49745CCE42B1C4348C059EF1997E2C932A2D1A5 ();
// 0x000004A1 System.Void MaterialUI.MaterialButton::set_textText(System.String)
extern void MaterialButton_set_textText_mE40AEC2C82AB0D243A74B527E5AEB672D3D92D46 ();
// 0x000004A2 UnityEngine.Color MaterialUI.MaterialButton::get_textColor()
extern void MaterialButton_get_textColor_mF011CA6FD9883581AE7FD79894E9AD5236A2EE6C ();
// 0x000004A3 System.Void MaterialUI.MaterialButton::set_textColor(UnityEngine.Color)
extern void MaterialButton_set_textColor_mD016C7A3DF135625BB4ECA652B7D42C6713980C3 ();
// 0x000004A4 MaterialUI.VectorImageData MaterialUI.MaterialButton::get_iconVectorImageData()
extern void MaterialButton_get_iconVectorImageData_m6E1A9F142AE3C2D32F66331869D5CA103D3DE68D ();
// 0x000004A5 System.Void MaterialUI.MaterialButton::set_iconVectorImageData(MaterialUI.VectorImageData)
extern void MaterialButton_set_iconVectorImageData_m5187CA1F1AC6DDC9FDE83474FBC1772644187DC1 ();
// 0x000004A6 UnityEngine.Sprite MaterialUI.MaterialButton::get_iconSprite()
extern void MaterialButton_get_iconSprite_mC4E81C0E3C8ED5FCD70C2904718B9F214D1E77DC ();
// 0x000004A7 System.Void MaterialUI.MaterialButton::set_iconSprite(UnityEngine.Sprite)
extern void MaterialButton_set_iconSprite_mE2E6EB088A8E169EABF7830E7C7F7973F70AE945 ();
// 0x000004A8 UnityEngine.Color MaterialUI.MaterialButton::get_iconColor()
extern void MaterialButton_get_iconColor_mA96BBEB9F53A721C7C703835CB159F8FBE173729 ();
// 0x000004A9 System.Void MaterialUI.MaterialButton::set_iconColor(UnityEngine.Color)
extern void MaterialButton_set_iconColor_m1AEDFF73588462A057A1AA9D403506B1B2A2E1D0 ();
// 0x000004AA MaterialUI.VectorImageData MaterialUI.MaterialButton::get_backgroundVectorImageData()
extern void MaterialButton_get_backgroundVectorImageData_m3604A5487D42FA564D9327819A1527929346C6A6 ();
// 0x000004AB System.Void MaterialUI.MaterialButton::set_backgroundVectorImageData(MaterialUI.VectorImageData)
extern void MaterialButton_set_backgroundVectorImageData_m8852EAA1E2488119B8D9D815C1CF3216DE0953A5 ();
// 0x000004AC UnityEngine.Sprite MaterialUI.MaterialButton::get_backgroundSprite()
extern void MaterialButton_get_backgroundSprite_m0A50E28402632ACAB44B46915BBE0573FEA4DEC0 ();
// 0x000004AD System.Void MaterialUI.MaterialButton::set_backgroundSprite(UnityEngine.Sprite)
extern void MaterialButton_set_backgroundSprite_m5E5DA22F55B7BB5C200B06541D66406F66B7C2BD ();
// 0x000004AE UnityEngine.Color MaterialUI.MaterialButton::get_backgroundColor()
extern void MaterialButton_get_backgroundColor_mC387CC14DBAA864A07D043B942499BE2D2C4654A ();
// 0x000004AF System.Void MaterialUI.MaterialButton::set_backgroundColor(UnityEngine.Color)
extern void MaterialButton_set_backgroundColor_mB7F48329F2AE0E6E65B402289AB91D66C24E0A9E ();
// 0x000004B0 System.Void MaterialUI.MaterialButton::OnEnable()
extern void MaterialButton_OnEnable_mB8623931A17AE0A80CE8770DC936280D3584DCB8 ();
// 0x000004B1 System.Void MaterialUI.MaterialButton::OnDisable()
extern void MaterialButton_OnDisable_mA16AA783FCFDC092C36081B46C581D129AD33E48 ();
// 0x000004B2 System.Void MaterialUI.MaterialButton::SetButtonBackgroundColor(UnityEngine.Color,System.Boolean)
extern void MaterialButton_SetButtonBackgroundColor_mE24C57C77F1A83EDDC7F7C1528528F6DDCDB00D7 ();
// 0x000004B3 System.Void MaterialUI.MaterialButton::RefreshRippleMatchColor()
extern void MaterialButton_RefreshRippleMatchColor_mABAF9E058E35CBE4E1CD92B87D01F72A3388F02E ();
// 0x000004B4 System.Void MaterialUI.MaterialButton::Convert(System.Boolean)
extern void MaterialButton_Convert_m0493B971DF444781C913A41FCF672BAB37FCE79B ();
// 0x000004B5 System.Void MaterialUI.MaterialButton::ClearTracker()
extern void MaterialButton_ClearTracker_m61F75ECCA4B1F6A2E227E662A6F6F5D1BB4CA4D0 ();
// 0x000004B6 System.Void MaterialUI.MaterialButton::OnRectTransformDimensionsChange()
extern void MaterialButton_OnRectTransformDimensionsChange_m13F5BBEFFE46FB9FA4EF822C42D8BA84DB3696F1 ();
// 0x000004B7 System.Void MaterialUI.MaterialButton::OnCanvasGroupChanged()
extern void MaterialButton_OnCanvasGroupChanged_m7E1EF22B8C7AAA291291D135518EA261C841A67B ();
// 0x000004B8 System.Void MaterialUI.MaterialButton::OnDidApplyAnimationProperties()
extern void MaterialButton_OnDidApplyAnimationProperties_mF6ED99F08903794876AA3C9D50AD6C9DA88A9E19 ();
// 0x000004B9 System.Void MaterialUI.MaterialButton::SetLayoutDirty()
extern void MaterialButton_SetLayoutDirty_mB96F9E9847F8F1921484EDB44868DD084DA54AE0 ();
// 0x000004BA System.Void MaterialUI.MaterialButton::SetLayoutHorizontal()
extern void MaterialButton_SetLayoutHorizontal_m456880B41A1DD44BDDA1B98D781B1860BF63BFF2 ();
// 0x000004BB System.Void MaterialUI.MaterialButton::SetLayoutVertical()
extern void MaterialButton_SetLayoutVertical_mF8B2AFDB3CB40C3C2B8E3B9512B1E76C19299F32 ();
// 0x000004BC System.Void MaterialUI.MaterialButton::CalculateLayoutInputHorizontal()
extern void MaterialButton_CalculateLayoutInputHorizontal_m4BDF7344FD01DDA9BAEDFDCD65B4B276C0B80AD3 ();
// 0x000004BD System.Void MaterialUI.MaterialButton::CalculateLayoutInputVertical()
extern void MaterialButton_CalculateLayoutInputVertical_mFE5ABA62D5BBBC8D378AF53208BE6865DEA923DA ();
// 0x000004BE System.Single MaterialUI.MaterialButton::get_minWidth()
extern void MaterialButton_get_minWidth_mC55EF57BEEA5666A89A197E04B21BA66543D2521 ();
// 0x000004BF System.Single MaterialUI.MaterialButton::get_preferredWidth()
extern void MaterialButton_get_preferredWidth_m59F980B858551BC887C49C3DC4CABB37FC059460 ();
// 0x000004C0 System.Single MaterialUI.MaterialButton::get_flexibleWidth()
extern void MaterialButton_get_flexibleWidth_m90EDB76272422EA4E797A54F0A1283CDAE128EB6 ();
// 0x000004C1 System.Single MaterialUI.MaterialButton::get_minHeight()
extern void MaterialButton_get_minHeight_m51B579BD880BCEF8BDB5ADC31E7428AFE076C813 ();
// 0x000004C2 System.Single MaterialUI.MaterialButton::get_preferredHeight()
extern void MaterialButton_get_preferredHeight_mC496F027DB48C5D4AF9C824E9F2B8C610A4C5BC7 ();
// 0x000004C3 System.Single MaterialUI.MaterialButton::get_flexibleHeight()
extern void MaterialButton_get_flexibleHeight_mDEEB7EC2819F5E47D13B4DD83B5DFDB1341C786A ();
// 0x000004C4 System.Int32 MaterialUI.MaterialButton::get_layoutPriority()
extern void MaterialButton_get_layoutPriority_mCC325D37115AB44D44774501F94FAE2572022B6F ();
// 0x000004C5 System.Void MaterialUI.MaterialButton::.ctor()
extern void MaterialButton__ctor_m63CD75E9FB49528B89249AF469BA03332A233443 ();
// 0x000004C6 System.Void MaterialUI.MaterialButton::<SetButtonBackgroundColor>b__98_0(UnityEngine.Color)
extern void MaterialButton_U3CSetButtonBackgroundColorU3Eb__98_0_m2C26CD7C6C37BF6167FE043DDAA000C67771EC3A ();
// 0x000004C7 System.Void MaterialUI.MaterialMovableFab::.ctor()
extern void MaterialMovableFab__ctor_mC5FB1EBF455DE44B8CDA6F2CFB92C3C6C42C9BDB ();
// 0x000004C8 MaterialUI.DialogTitleSection MaterialUI.DialogAlert::get_titleSection()
extern void DialogAlert_get_titleSection_m7D231955F76C8F469F9DDA30E9452E49A29D02EF ();
// 0x000004C9 System.Void MaterialUI.DialogAlert::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogAlert_set_titleSection_m728A9786DC9F44BCECB96A7B09E902F68960EA26 ();
// 0x000004CA MaterialUI.DialogButtonSection MaterialUI.DialogAlert::get_buttonSection()
extern void DialogAlert_get_buttonSection_m14BFF6531A220CCA9E7652950820A915EDBFA56B ();
// 0x000004CB System.Void MaterialUI.DialogAlert::set_buttonSection(MaterialUI.DialogButtonSection)
extern void DialogAlert_set_buttonSection_m6F04489497FCBAA0D563F9B2F71888D5C3CD239A ();
// 0x000004CC UnityEngine.UI.Text MaterialUI.DialogAlert::get_bodyText()
extern void DialogAlert_get_bodyText_m0316C16A662EB9854DE10C418B52529ED2D3096F ();
// 0x000004CD System.Void MaterialUI.DialogAlert::Initialize(System.String,System.Action,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogAlert_Initialize_mFAF8ACADF44D6C7081E4462C83F1DD0320E2E244 ();
// 0x000004CE System.Void MaterialUI.DialogAlert::AffirmativeButtonClicked()
extern void DialogAlert_AffirmativeButtonClicked_mA560BF87DB543A8E8C3683F34DA7D0F7C656FCA9 ();
// 0x000004CF System.Void MaterialUI.DialogAlert::DismissiveButtonClicked()
extern void DialogAlert_DismissiveButtonClicked_m97F78BA783253E258CEB40E51B92AEFB40E58D60 ();
// 0x000004D0 System.Void MaterialUI.DialogAlert::Update()
extern void DialogAlert_Update_m914AC74B808092FE432641718873478732BFCF61 ();
// 0x000004D1 System.Void MaterialUI.DialogAlert::.ctor()
extern void DialogAlert__ctor_m29A30F133139C89808D2F9E5209082D3A2AC529B ();
// 0x000004D2 MaterialUI.MaterialDialog MaterialUI.DialogAnimator::get_dialog()
extern void DialogAnimator_get_dialog_m45E17A840A43AF0E8E673A788B13AED4C4AF185D ();
// 0x000004D3 System.Void MaterialUI.DialogAnimator::set_dialog(MaterialUI.MaterialDialog)
extern void DialogAnimator_set_dialog_mDEA43FA3AA0BB022F9192F92E54853337EE586B8 ();
// 0x000004D4 System.Single MaterialUI.DialogAnimator::get_animationDuration()
extern void DialogAnimator_get_animationDuration_mB9965BEDE699588AE07F976E26C2C85F81064FEA ();
// 0x000004D5 UnityEngine.CanvasGroup MaterialUI.DialogAnimator::get_background()
extern void DialogAnimator_get_background_m25CD93B1D2D7B79AB012CF9F57EB087E3AB0893A ();
// 0x000004D6 System.Single MaterialUI.DialogAnimator::get_backgroundAlpha()
extern void DialogAnimator_get_backgroundAlpha_m330435C6C898867973EDA49ACD9408769AFF1448 ();
// 0x000004D7 System.Void MaterialUI.DialogAnimator::set_backgroundAlpha(System.Single)
extern void DialogAnimator_set_backgroundAlpha_m3FBF1CDC28F0C4DD711FE8BFBDF88598FA92FCDA ();
// 0x000004D8 System.Void MaterialUI.DialogAnimator::.ctor(System.Single)
extern void DialogAnimator__ctor_m6B04E6047DCB4EA9542EF762C7FDFEE513728F4E ();
// 0x000004D9 System.Void MaterialUI.DialogAnimator::AnimateShow(System.Action)
extern void DialogAnimator_AnimateShow_mCEACF2523F179CF65C6002136F641F0F01E07B44 ();
// 0x000004DA System.Void MaterialUI.DialogAnimator::AnimateShowBackground()
extern void DialogAnimator_AnimateShowBackground_mE9E52B439CA64BB48633EA074E06450BB932DB10 ();
// 0x000004DB System.Void MaterialUI.DialogAnimator::AnimateHide(System.Action)
extern void DialogAnimator_AnimateHide_mFA4D71C00B9304D259A421FF42B722214BEE18D4 ();
// 0x000004DC System.Void MaterialUI.DialogAnimator::AnimateHideBackground()
extern void DialogAnimator_AnimateHideBackground_m257EAC9532F3EC592C22296064A950732E826436 ();
// 0x000004DD System.Void MaterialUI.DialogAnimator::OnBackgroundClick()
extern void DialogAnimator_OnBackgroundClick_mC3F74EA2B9FCBEA302DF932FFD8E12CE6DC972CB ();
// 0x000004DE System.Void MaterialUI.DialogAnimator::<AnimateShowBackground>b__16_0(System.Single)
extern void DialogAnimator_U3CAnimateShowBackgroundU3Eb__16_0_mF2275F818B762045D5E3635F84FFCEB814BA8C69 ();
// 0x000004DF System.Void MaterialUI.DialogAnimator::<AnimateHideBackground>b__18_0(System.Single)
extern void DialogAnimator_U3CAnimateHideBackgroundU3Eb__18_0_m07A3CBCD580B40538B114D882465FE2EDEED3FA3 ();
// 0x000004E0 System.Void MaterialUI.DialogAnimator::<AnimateHideBackground>b__18_1()
extern void DialogAnimator_U3CAnimateHideBackgroundU3Eb__18_1_m063ACA47A31367A824C990CE6CF005B0B8DE6853 ();
// 0x000004E1 MaterialUI.DialogAnimatorSlide_SlideDirection MaterialUI.DialogAnimatorSlide::get_slideInDirection()
extern void DialogAnimatorSlide_get_slideInDirection_m8F7118770EF616E247588DC60BCA8F8FFF901055 ();
// 0x000004E2 System.Void MaterialUI.DialogAnimatorSlide::set_slideInDirection(MaterialUI.DialogAnimatorSlide_SlideDirection)
extern void DialogAnimatorSlide_set_slideInDirection_m9BC6CAC2ED974D85B127CE7AA1F9B8A2ACA6A045 ();
// 0x000004E3 MaterialUI.DialogAnimatorSlide_SlideDirection MaterialUI.DialogAnimatorSlide::get_slideOutDirection()
extern void DialogAnimatorSlide_get_slideOutDirection_m6906940D4A7C5C4745595E033CB75A9D0A049CEA ();
// 0x000004E4 System.Void MaterialUI.DialogAnimatorSlide::set_slideOutDirection(MaterialUI.DialogAnimatorSlide_SlideDirection)
extern void DialogAnimatorSlide_set_slideOutDirection_m411B179DB7BD5E511E13CA97859574DB02923C0B ();
// 0x000004E5 MaterialUI.Tween_TweenType MaterialUI.DialogAnimatorSlide::get_slideInTweenType()
extern void DialogAnimatorSlide_get_slideInTweenType_m3DDAF3206848B3EA2F238A9A9850E95A76A6B39B ();
// 0x000004E6 System.Void MaterialUI.DialogAnimatorSlide::set_slideInTweenType(MaterialUI.Tween_TweenType)
extern void DialogAnimatorSlide_set_slideInTweenType_m463239879FB2A7B8FF9346C39D71042919944A8F ();
// 0x000004E7 MaterialUI.Tween_TweenType MaterialUI.DialogAnimatorSlide::get_slideOutTweenType()
extern void DialogAnimatorSlide_get_slideOutTweenType_mCA017E7856B594BFDD746904A6B5CE31D3EDD2B2 ();
// 0x000004E8 System.Void MaterialUI.DialogAnimatorSlide::set_slideOutTweenType(MaterialUI.Tween_TweenType)
extern void DialogAnimatorSlide_set_slideOutTweenType_m0BE7FF1CEE27DD6B28E78D2A7ED4DCC195B2097A ();
// 0x000004E9 System.Void MaterialUI.DialogAnimatorSlide::.ctor(System.Single)
extern void DialogAnimatorSlide__ctor_mDBDC97AC0C2860AAF295E6B5E913AEEA6614003B ();
// 0x000004EA System.Void MaterialUI.DialogAnimatorSlide::.ctor(System.Single,MaterialUI.DialogAnimatorSlide_SlideDirection,MaterialUI.DialogAnimatorSlide_SlideDirection)
extern void DialogAnimatorSlide__ctor_m97655F332E3A9FCDF2EF88FE2B47CCCFCFDBCA2B ();
// 0x000004EB System.Void MaterialUI.DialogAnimatorSlide::.ctor(System.Single,MaterialUI.DialogAnimatorSlide_SlideDirection,MaterialUI.DialogAnimatorSlide_SlideDirection,MaterialUI.Tween_TweenType,MaterialUI.Tween_TweenType)
extern void DialogAnimatorSlide__ctor_m26E3C2717BFC50D9B0CAED6270404D7C67E1919C ();
// 0x000004EC System.Void MaterialUI.DialogAnimatorSlide::AnimateShow(System.Action)
extern void DialogAnimatorSlide_AnimateShow_mF7D6122860064E3267CA83794E555E20FDAED1EC ();
// 0x000004ED System.Void MaterialUI.DialogAnimatorSlide::AnimateHide(System.Action)
extern void DialogAnimatorSlide_AnimateHide_mD704526374FFCCFE5E927D134D77FB62F39F9E72 ();
// 0x000004EE UnityEngine.Vector2 MaterialUI.DialogAnimatorSlide::GetSlidePosition(MaterialUI.DialogAnimatorSlide_SlideDirection)
extern void DialogAnimatorSlide_GetSlidePosition_m74BB32127E3D849E5C753CD020AEFF829FE59673 ();
// 0x000004EF System.Void MaterialUI.DialogAnimatorSlide::<AnimateShow>b__20_0(UnityEngine.Vector2)
extern void DialogAnimatorSlide_U3CAnimateShowU3Eb__20_0_m98CD909703262E1112514D1A378027F50BFB4A2B ();
// 0x000004F0 System.Void MaterialUI.DialogAnimatorSlide::<AnimateHide>b__21_0(UnityEngine.Vector2)
extern void DialogAnimatorSlide_U3CAnimateHideU3Eb__21_0_mF80375750BA8EFB03D08DCB4A32366F0E6A057BD ();
// 0x000004F1 MaterialUI.DialogAnimator MaterialUI.DialogBackground::get_dialogAnimator()
extern void DialogBackground_get_dialogAnimator_m4244F4415D43DB34C9C9A654D9548423DCB4099A ();
// 0x000004F2 System.Void MaterialUI.DialogBackground::set_dialogAnimator(MaterialUI.DialogAnimator)
extern void DialogBackground_set_dialogAnimator_mFD37FE722FDF53AAD46BD63003A58CA231A3E256 ();
// 0x000004F3 System.Void MaterialUI.DialogBackground::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DialogBackground_OnPointerClick_m72CF24985ABC90870055DE77AF2EE3410B2E1D92 ();
// 0x000004F4 System.Void MaterialUI.DialogBackground::.ctor()
extern void DialogBackground__ctor_m69DFC747DA3A83BA5F2560F1D31EAAE2C33833B3 ();
// 0x000004F5 MaterialUI.DialogTitleSection MaterialUI.DialogCheckboxList::get_titleSection()
extern void DialogCheckboxList_get_titleSection_m98D602D9376D0DA5105A752D3349505811F60721 ();
// 0x000004F6 System.Void MaterialUI.DialogCheckboxList::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogCheckboxList_set_titleSection_mB1A90127E7CCC5A1759F9FF1F91FB0760154C414 ();
// 0x000004F7 MaterialUI.DialogButtonSection MaterialUI.DialogCheckboxList::get_buttonSection()
extern void DialogCheckboxList_get_buttonSection_m4509ADC7CAC230898A574AC4E95C30776B3201B6 ();
// 0x000004F8 System.Void MaterialUI.DialogCheckboxList::set_buttonSection(MaterialUI.DialogButtonSection)
extern void DialogCheckboxList_set_buttonSection_m08D8B5240F21E30F61C58652C74B5840CD38D3E4 ();
// 0x000004F9 MaterialUI.VerticalScrollLayoutElement MaterialUI.DialogCheckboxList::get_listScrollLayoutElement()
extern void DialogCheckboxList_get_listScrollLayoutElement_m5F6233236EB4C302639128E61EE2B5F1ABA36E3D ();
// 0x000004FA System.Void MaterialUI.DialogCheckboxList::set_listScrollLayoutElement(MaterialUI.VerticalScrollLayoutElement)
extern void DialogCheckboxList_set_listScrollLayoutElement_m8B141DFE2E6A7259C57B1AD324FF7595F89764BE ();
// 0x000004FB System.Collections.Generic.List`1<MaterialUI.DialogCheckboxOption> MaterialUI.DialogCheckboxList::get_selectionItems()
extern void DialogCheckboxList_get_selectionItems_mD52215EB812723549F779CCFCE26BBA499789F8F ();
// 0x000004FC System.Boolean[] MaterialUI.DialogCheckboxList::get_selectedIndexes()
extern void DialogCheckboxList_get_selectedIndexes_m16AEC9146297DB57968203E0485E5B3964620315 ();
// 0x000004FD System.Void MaterialUI.DialogCheckboxList::set_selectedIndexes(System.Boolean[])
extern void DialogCheckboxList_set_selectedIndexes_mCD274BC8A87E83650D2BDCF7A09CCDDC7336A58E ();
// 0x000004FE System.String[] MaterialUI.DialogCheckboxList::get_optionList()
extern void DialogCheckboxList_get_optionList_mEF90E5627B1D6CE026D6F0D001FC20EBCBA82866 ();
// 0x000004FF System.Void MaterialUI.DialogCheckboxList::set_optionList(System.String[])
extern void DialogCheckboxList_set_optionList_mE57F3470035F593E4A9A1CCD810A17739083891F ();
// 0x00000500 System.Action`1<System.Boolean[]> MaterialUI.DialogCheckboxList::get_onAffirmativeButtonClicked()
extern void DialogCheckboxList_get_onAffirmativeButtonClicked_m3691656DA4D3FDD5510749F953497F5DBBD7424D ();
// 0x00000501 System.Void MaterialUI.DialogCheckboxList::set_onAffirmativeButtonClicked(System.Action`1<System.Boolean[]>)
extern void DialogCheckboxList_set_onAffirmativeButtonClicked_mB9B915328FA4C1AE7989137110FD00E270E6435D ();
// 0x00000502 System.Void MaterialUI.DialogCheckboxList::OnEnable()
extern void DialogCheckboxList_OnEnable_mF5B460E79315B1E869BB80749ACBD6BBA5B97401 ();
// 0x00000503 System.Void MaterialUI.DialogCheckboxList::Initialize(System.String[],System.Action`1<System.Boolean[]>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogCheckboxList_Initialize_m841860D0A8E2EB2A5CF7A773A5F7ACA00813C909 ();
// 0x00000504 MaterialUI.DialogCheckboxOption MaterialUI.DialogCheckboxList::CreateSelectionItem(System.Int32)
extern void DialogCheckboxList_CreateSelectionItem_mF54613617A857F55146458DB4A155E312B2AAD92 ();
// 0x00000505 System.Void MaterialUI.DialogCheckboxList::OnItemClick(System.Int32)
extern void DialogCheckboxList_OnItemClick_m788EFE97BCD4FD2442DD9FF82D5E0DB56AAFB974 ();
// 0x00000506 System.Void MaterialUI.DialogCheckboxList::AffirmativeButtonClicked()
extern void DialogCheckboxList_AffirmativeButtonClicked_m15DBA13F79A667A71943789EF72F05F756F416C7 ();
// 0x00000507 System.Void MaterialUI.DialogCheckboxList::DismissiveButtonClicked()
extern void DialogCheckboxList_DismissiveButtonClicked_m70A134B4644AF7D2BB5DF062BAD9C746730BE9C4 ();
// 0x00000508 System.Void MaterialUI.DialogCheckboxList::Update()
extern void DialogCheckboxList_Update_mE12D4A267B8C4317AE58AB86174C046961120738 ();
// 0x00000509 System.Void MaterialUI.DialogCheckboxList::.ctor()
extern void DialogCheckboxList__ctor_mD082DC0CF49A6BC44EBEDB02E1BA345A4D4C30F8 ();
// 0x0000050A UnityEngine.UI.Text MaterialUI.DialogCheckboxOption::get_itemText()
extern void DialogCheckboxOption_get_itemText_mBD61156BB292C1826E1A90043133A1A47D0A3C75 ();
// 0x0000050B MaterialUI.MaterialCheckbox MaterialUI.DialogCheckboxOption::get_itemCheckbox()
extern void DialogCheckboxOption_get_itemCheckbox_m484EEE168109E39ED78A4DCA967B49DF931D714A ();
// 0x0000050C MaterialUI.MaterialRipple MaterialUI.DialogCheckboxOption::get_itemRipple()
extern void DialogCheckboxOption_get_itemRipple_mB6DF6F2F90E6BC55D374857A7DDA015945EBA75A ();
// 0x0000050D UnityEngine.RectTransform MaterialUI.DialogCheckboxOption::get_rectTransform()
extern void DialogCheckboxOption_get_rectTransform_mE06808213C76A1A15930E54E17A9CAD455055013 ();
// 0x0000050E System.Void MaterialUI.DialogCheckboxOption::.ctor()
extern void DialogCheckboxOption__ctor_m60674C553C4C42F11D84B50D9842D950C563F831 ();
// 0x0000050F System.Action`1<System.Int32> MaterialUI.DialogClickableOption::get_onClickAction()
extern void DialogClickableOption_get_onClickAction_m38C86788AF1A10F81661DB5AEDFB81F78B51473A ();
// 0x00000510 System.Void MaterialUI.DialogClickableOption::set_onClickAction(System.Action`1<System.Int32>)
extern void DialogClickableOption_set_onClickAction_mEA0F51699EB1EB6DE1DEC54C6CB431CC568D2808 ();
// 0x00000511 System.Int32 MaterialUI.DialogClickableOption::get_index()
extern void DialogClickableOption_get_index_m9790D73D068D99F4CA8C6FD5F8BB7AAAA0CF18BF ();
// 0x00000512 System.Void MaterialUI.DialogClickableOption::set_index(System.Int32)
extern void DialogClickableOption_set_index_m0729CB4FA073C8CB4C6823037E1A5858C2388001 ();
// 0x00000513 System.Void MaterialUI.DialogClickableOption::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DialogClickableOption_OnPointerClick_m9B82D7099656E98011095742C1DCDA9CECA6D0AF ();
// 0x00000514 System.Void MaterialUI.DialogClickableOption::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void DialogClickableOption_OnSubmit_mE84097DF6C2705631F8DE1467121F60B0DA4289D ();
// 0x00000515 System.Void MaterialUI.DialogClickableOption::.ctor()
extern void DialogClickableOption__ctor_mE4F32E0D5DC35CB91116460C15DCFE43F74C2027 ();
// 0x00000516 MaterialUI.DialogTitleSection MaterialUI.DialogProgress::get_titleSection()
extern void DialogProgress_get_titleSection_m0FD1888E87087C722469024DA1191D4D52FCC246 ();
// 0x00000517 System.Void MaterialUI.DialogProgress::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogProgress_set_titleSection_mF9C83AC73DEE27E003B2787B9DC640EF03973046 ();
// 0x00000518 UnityEngine.UI.Text MaterialUI.DialogProgress::get_bodyText()
extern void DialogProgress_get_bodyText_m037CADE3421C3155F91A4A7C7FA6DDD6F616DEBA ();
// 0x00000519 MaterialUI.ProgressIndicator MaterialUI.DialogProgress::get_progressIndicator()
extern void DialogProgress_get_progressIndicator_m5E54A901E77C10D7A13C67B47EADF84F07F35DF8 ();
// 0x0000051A System.Void MaterialUI.DialogProgress::set_progressIndicator(MaterialUI.ProgressIndicator)
extern void DialogProgress_set_progressIndicator_mDA69B13613198C9D307E2625347D1851B3B358FE ();
// 0x0000051B System.Void MaterialUI.DialogProgress::Initialize(System.String,System.String,MaterialUI.ImageData,System.Boolean)
extern void DialogProgress_Initialize_m274EA1A197F618F96F395B1D0F362A5B3F51B337 ();
// 0x0000051C System.Void MaterialUI.DialogProgress::SetupIndicator(System.Boolean)
extern void DialogProgress_SetupIndicator_m507F71F01E88D5666E9B9BB28AF65C0403EE2AC0 ();
// 0x0000051D System.Void MaterialUI.DialogProgress::.ctor()
extern void DialogProgress__ctor_m05ED2526C7D9B191F7E7696B84BBA0B30A9B0C93 ();
// 0x0000051E MaterialUI.DialogTitleSection MaterialUI.DialogPrompt::get_titleSection()
extern void DialogPrompt_get_titleSection_m9F8ED03981F5768FAA5778542421AFAF069CD716 ();
// 0x0000051F System.Void MaterialUI.DialogPrompt::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogPrompt_set_titleSection_m30E362ACACF682CE8833ABBDBE3A91168F9C9FE5 ();
// 0x00000520 MaterialUI.DialogButtonSection MaterialUI.DialogPrompt::get_buttonSection()
extern void DialogPrompt_get_buttonSection_mC58CEAF8DE2F9961250C2226D7ECF016F853666D ();
// 0x00000521 System.Void MaterialUI.DialogPrompt::set_buttonSection(MaterialUI.DialogButtonSection)
extern void DialogPrompt_set_buttonSection_mF0BF419DD3BE6AAE1ABC634DF6C7CF1B00A0E804 ();
// 0x00000522 MaterialUI.MaterialInputField MaterialUI.DialogPrompt::get_firstInputField()
extern void DialogPrompt_get_firstInputField_m940B8A09D06F1C65200774BD453869CCEA6BE1A4 ();
// 0x00000523 MaterialUI.MaterialInputField MaterialUI.DialogPrompt::get_secondInputField()
extern void DialogPrompt_get_secondInputField_mF71172567149BCFFE304AADD3C5C8DF206E73252 ();
// 0x00000524 System.Action`1<System.String> MaterialUI.DialogPrompt::get_onAffirmativeOneButtonClicked()
extern void DialogPrompt_get_onAffirmativeOneButtonClicked_mD3230CA4F73A3D08B5F8F310393E987CDD750694 ();
// 0x00000525 System.Void MaterialUI.DialogPrompt::set_onAffirmativeOneButtonClicked(System.Action`1<System.String>)
extern void DialogPrompt_set_onAffirmativeOneButtonClicked_m8FA53FC9E85D9410D87D0240C66DC1DA6FDC31C1 ();
// 0x00000526 System.Action`2<System.String,System.String> MaterialUI.DialogPrompt::get_onAffirmativeTwoButtonClicked()
extern void DialogPrompt_get_onAffirmativeTwoButtonClicked_m9E861E8CF464D90AC018854363E4C67224208A97 ();
// 0x00000527 System.Void MaterialUI.DialogPrompt::set_onAffirmativeTwoButtonClicked(System.Action`2<System.String,System.String>)
extern void DialogPrompt_set_onAffirmativeTwoButtonClicked_mE3F46EFA1D445E757A08A3C3CC0EF6AEE5735724 ();
// 0x00000528 System.Void MaterialUI.DialogPrompt::Initialize(System.String,System.Action`1<System.String>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogPrompt_Initialize_m29DFAA2E62808A39A49444B5949670DDB25B84D1 ();
// 0x00000529 System.Void MaterialUI.DialogPrompt::Initialize(System.String,System.String,System.Action`2<System.String,System.String>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogPrompt_Initialize_m549B17DDC3EDFFC4FC617B035B33D1D1ACCF2A07 ();
// 0x0000052A System.Void MaterialUI.DialogPrompt::CommonInitialize(System.String,System.String,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogPrompt_CommonInitialize_mC64620683A217E6BE3FED6F3DC0B67F55FE20291 ();
// 0x0000052B System.Void MaterialUI.DialogPrompt::AffirmativeButtonClicked()
extern void DialogPrompt_AffirmativeButtonClicked_mBB000A81CBFDAD4C0D51F07B381228BE40B77EC0 ();
// 0x0000052C System.Void MaterialUI.DialogPrompt::DismissiveButtonClicked()
extern void DialogPrompt_DismissiveButtonClicked_m795117BFC145A5DB72FD0B35B881563AC75958B5 ();
// 0x0000052D System.Void MaterialUI.DialogPrompt::UpdateAffirmativeButtonState()
extern void DialogPrompt_UpdateAffirmativeButtonState_mD1EE2C94E7222CB079889D6E8C9EF83DB781F72C ();
// 0x0000052E System.Void MaterialUI.DialogPrompt::Show()
extern void DialogPrompt_Show_m180B4F5E7D5E90A18F7C5D73E4DBD9F4F3E0B079 ();
// 0x0000052F System.Void MaterialUI.DialogPrompt::Update()
extern void DialogPrompt_Update_mF1E55DD52844F3531A91C426926AA48B4B194F15 ();
// 0x00000530 System.Void MaterialUI.DialogPrompt::.ctor()
extern void DialogPrompt__ctor_m3EE3E85C31CD1F667D86D7DDC65A0CA085AA4108 ();
// 0x00000531 MaterialUI.DialogTitleSection MaterialUI.DialogRadioList::get_titleSection()
extern void DialogRadioList_get_titleSection_m9357ABC75B049AE7FCA99127B46CB7CD1DB2D28C ();
// 0x00000532 System.Void MaterialUI.DialogRadioList::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogRadioList_set_titleSection_m79925AAE613D66E555340B56FB39B00888FDB301 ();
// 0x00000533 MaterialUI.DialogButtonSection MaterialUI.DialogRadioList::get_buttonSection()
extern void DialogRadioList_get_buttonSection_m3E0F3EDDFD4EF668A074FA9CB0F55A74707B6C27 ();
// 0x00000534 System.Void MaterialUI.DialogRadioList::set_buttonSection(MaterialUI.DialogButtonSection)
extern void DialogRadioList_set_buttonSection_m3530DDBF0F0B820CCF6EC58B88BD5D30FB38B4F0 ();
// 0x00000535 MaterialUI.VerticalScrollLayoutElement MaterialUI.DialogRadioList::get_listScrollLayoutElement()
extern void DialogRadioList_get_listScrollLayoutElement_m8DB7379A78AC2642C7E4488C985838BA75E1D8DD ();
// 0x00000536 System.Void MaterialUI.DialogRadioList::set_listScrollLayoutElement(MaterialUI.VerticalScrollLayoutElement)
extern void DialogRadioList_set_listScrollLayoutElement_m7AB0F703BF1518F06B820E3B8C1969D6B41156CA ();
// 0x00000537 UnityEngine.UI.ToggleGroup MaterialUI.DialogRadioList::get_toggleGroup()
extern void DialogRadioList_get_toggleGroup_m6B15C3A74171A2FCAF24507CDFB2D95AF976B8F0 ();
// 0x00000538 System.Void MaterialUI.DialogRadioList::set_toggleGroup(UnityEngine.UI.ToggleGroup)
extern void DialogRadioList_set_toggleGroup_m00363A1C46D474EFBA3698823D02E8CEF5C24285 ();
// 0x00000539 System.Collections.Generic.List`1<MaterialUI.DialogRadioOption> MaterialUI.DialogRadioList::get_selectionItems()
extern void DialogRadioList_get_selectionItems_mE9AC2085B6EA430DAE3A92A21F173BAE8A109177 ();
// 0x0000053A System.Int32 MaterialUI.DialogRadioList::get_selectedIndex()
extern void DialogRadioList_get_selectedIndex_m41CD79EAEF1EC9FFB76D3A0F2C7F95F54295D071 ();
// 0x0000053B System.String[] MaterialUI.DialogRadioList::get_optionList()
extern void DialogRadioList_get_optionList_m0792BEA617DC9668BC1EA111CDA0462F0ACC32CC ();
// 0x0000053C System.Void MaterialUI.DialogRadioList::set_optionList(System.String[])
extern void DialogRadioList_set_optionList_mDCEA9EF009365FDF4FF8B01BA0208389DC30F33B ();
// 0x0000053D System.Action`1<System.Int32> MaterialUI.DialogRadioList::get_onAffirmativeButtonClicked()
extern void DialogRadioList_get_onAffirmativeButtonClicked_mB8FEF10EC3DC46D721D5DF2DE4ACC1884FB444A0 ();
// 0x0000053E System.Void MaterialUI.DialogRadioList::set_onAffirmativeButtonClicked(System.Action`1<System.Int32>)
extern void DialogRadioList_set_onAffirmativeButtonClicked_mFEC2D1B7F7AD73CE7594FB379B1D10AD2DAC3FE0 ();
// 0x0000053F System.Void MaterialUI.DialogRadioList::OnEnable()
extern void DialogRadioList_OnEnable_mE8804C2430007520D25B1F5390E02E93FF6BED07 ();
// 0x00000540 System.Void MaterialUI.DialogRadioList::Initialize(System.String[],System.Action`1<System.Int32>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String,System.Int32)
extern void DialogRadioList_Initialize_m6858AF6155A56726EDE9D3E9CA845011633F6155 ();
// 0x00000541 MaterialUI.DialogRadioOption MaterialUI.DialogRadioList::CreateSelectionItem(System.Int32)
extern void DialogRadioList_CreateSelectionItem_m486F75972AA33A952A0BA39982A73600797636FB ();
// 0x00000542 System.Void MaterialUI.DialogRadioList::OnItemClick(System.Int32)
extern void DialogRadioList_OnItemClick_m1C37DA9D16A9CC7D256A44B8A31ABECEDDB62CFD ();
// 0x00000543 System.Void MaterialUI.DialogRadioList::AffirmativeButtonClicked()
extern void DialogRadioList_AffirmativeButtonClicked_m7394A75C5606BDC59D1D4B53E8E464413F399A58 ();
// 0x00000544 System.Void MaterialUI.DialogRadioList::DismissiveButtonClicked()
extern void DialogRadioList_DismissiveButtonClicked_mBAB480BDCB7B631835C690D48039906B697B0EFA ();
// 0x00000545 System.Void MaterialUI.DialogRadioList::Update()
extern void DialogRadioList_Update_m0027643722FB3AFBE0743D6B1AE5A02F7DA32C9B ();
// 0x00000546 System.Void MaterialUI.DialogRadioList::.ctor()
extern void DialogRadioList__ctor_m3CB41482A9AED1AF09C554AFD8166E64D3D7F220 ();
// 0x00000547 UnityEngine.UI.Text MaterialUI.DialogRadioOption::get_itemText()
extern void DialogRadioOption_get_itemText_mD4B9F9ADB78BD07DD5EE9BFFFA560CEC1E0660ED ();
// 0x00000548 MaterialUI.MaterialRadio MaterialUI.DialogRadioOption::get_itemRadio()
extern void DialogRadioOption_get_itemRadio_m486C5D530266FF80087B9DE9F6230A7914F307B5 ();
// 0x00000549 MaterialUI.MaterialRipple MaterialUI.DialogRadioOption::get_itemRipple()
extern void DialogRadioOption_get_itemRipple_m15424F2228CB807FA43A66C02E20B6A1FB13CAF2 ();
// 0x0000054A UnityEngine.RectTransform MaterialUI.DialogRadioOption::get_rectTransform()
extern void DialogRadioOption_get_rectTransform_mB0215F7D06A4B6C1A50E8E19067D136C8F0738C8 ();
// 0x0000054B System.Void MaterialUI.DialogRadioOption::.ctor()
extern void DialogRadioOption__ctor_mE3F035E2F7E21D0C49964EA891B3370F73602151 ();
// 0x0000054C UnityEngine.UI.Text MaterialUI.DialogTitleSection::get_text()
extern void DialogTitleSection_get_text_m36FA01FCF5DA9330E1DE2F097AFCC04BFC7265FD ();
// 0x0000054D UnityEngine.UI.Graphic MaterialUI.DialogTitleSection::get_sprite()
extern void DialogTitleSection_get_sprite_m7448084AEF47C075BB4117835A16FDC85A2AEFC0 ();
// 0x0000054E MaterialUI.VectorImage MaterialUI.DialogTitleSection::get_vectorImageData()
extern void DialogTitleSection_get_vectorImageData_m3C4614171938BEBFAF81AD253882C6490D77190F ();
// 0x0000054F System.Void MaterialUI.DialogTitleSection::SetTitle(System.String,MaterialUI.ImageData)
extern void DialogTitleSection_SetTitle_m9C9CE192E5E58CA27785BDD176B1BA6C2294585A ();
// 0x00000550 System.Void MaterialUI.DialogTitleSection::.ctor()
extern void DialogTitleSection__ctor_m89DE42B7B8B6501CDAD3EB1AE5CB8C457CC7CC73 ();
// 0x00000551 System.Action MaterialUI.DialogButtonSection::get_onAffirmativeButtonClicked()
extern void DialogButtonSection_get_onAffirmativeButtonClicked_m88DEFB2684E897A5D2022E5979D8CAD82332A55A ();
// 0x00000552 System.Action MaterialUI.DialogButtonSection::get_onDismissiveButtonClicked()
extern void DialogButtonSection_get_onDismissiveButtonClicked_m41C012E7EE0A182CA6B99BFAD57E8717795A37FC ();
// 0x00000553 MaterialUI.MaterialButton MaterialUI.DialogButtonSection::get_affirmativeButton()
extern void DialogButtonSection_get_affirmativeButton_m7A81169691F69C85AFB9486A46BD43213553EB71 ();
// 0x00000554 MaterialUI.MaterialButton MaterialUI.DialogButtonSection::get_dismissiveButton()
extern void DialogButtonSection_get_dismissiveButton_m55370A5058BFAF9677CABAA953F0CFDF2FFCE48A ();
// 0x00000555 System.Void MaterialUI.DialogButtonSection::SetButtons(System.Action,System.String,System.Action,System.String)
extern void DialogButtonSection_SetButtons_mA7A64BB3474B745D2B11F9A9418F3CE1753F9084 ();
// 0x00000556 System.Void MaterialUI.DialogButtonSection::SetAffirmativeButton(System.Action,System.String)
extern void DialogButtonSection_SetAffirmativeButton_mF7119CFDEB892795F64E6F295355E80F2ECFB24A ();
// 0x00000557 System.Void MaterialUI.DialogButtonSection::SetDismissiveButton(System.Action,System.String)
extern void DialogButtonSection_SetDismissiveButton_mF7352F4377D4F6A76260617328E75D6CB397DDF5 ();
// 0x00000558 System.Void MaterialUI.DialogButtonSection::SetupButtonLayout(UnityEngine.RectTransform)
extern void DialogButtonSection_SetupButtonLayout_m4737547EE897E7765EC7D482F4A8AE724DFC6264 ();
// 0x00000559 System.Void MaterialUI.DialogButtonSection::OnAffirmativeButtonClicked()
extern void DialogButtonSection_OnAffirmativeButtonClicked_m81FC95F6380C833927C8190E7A29A820A8AB0CA2 ();
// 0x0000055A System.Void MaterialUI.DialogButtonSection::OnDismissiveButtonClicked()
extern void DialogButtonSection_OnDismissiveButtonClicked_mBCDBA53589EB3D12B27A47F763183EB89771D40D ();
// 0x0000055B System.Void MaterialUI.DialogButtonSection::.ctor()
extern void DialogButtonSection__ctor_mCE14BDD7757909803FD3467820BF7AA01244FDB9 ();
// 0x0000055C MaterialUI.DialogTitleSection MaterialUI.DialogSimpleList::get_titleSection()
extern void DialogSimpleList_get_titleSection_mAA52D0255CC5AE5FB7A51949F6FF95778F14B9FE ();
// 0x0000055D System.Void MaterialUI.DialogSimpleList::set_titleSection(MaterialUI.DialogTitleSection)
extern void DialogSimpleList_set_titleSection_m5875C39557678C8BE3A76DE083A9330B7D0E2490 ();
// 0x0000055E MaterialUI.VerticalScrollLayoutElement MaterialUI.DialogSimpleList::get_listScrollLayoutElement()
extern void DialogSimpleList_get_listScrollLayoutElement_mCA82FAC38AB410521CC7B343344A3C390B898948 ();
// 0x0000055F System.Void MaterialUI.DialogSimpleList::set_listScrollLayoutElement(MaterialUI.VerticalScrollLayoutElement)
extern void DialogSimpleList_set_listScrollLayoutElement_mA1E78ED085C48F672E20B2C0F6653955AD95D956 ();
// 0x00000560 System.Collections.Generic.List`1<MaterialUI.DialogSimpleOption> MaterialUI.DialogSimpleList::get_selectionItems()
extern void DialogSimpleList_get_selectionItems_mBF6786F505C7C79EA4E63B501E002A824AB52FEF ();
// 0x00000561 MaterialUI.OptionDataList MaterialUI.DialogSimpleList::get_optionDataList()
extern void DialogSimpleList_get_optionDataList_m4B045E7A6E6CE1A20E8B14EBDE226101E2F50D90 ();
// 0x00000562 System.Void MaterialUI.DialogSimpleList::set_optionDataList(MaterialUI.OptionDataList)
extern void DialogSimpleList_set_optionDataList_mEDA4EEDA300F508990FAFA62B15C5875FAFDB696 ();
// 0x00000563 System.Void MaterialUI.DialogSimpleList::Start()
extern void DialogSimpleList_Start_m8A986BD351EF5A07861250FBFEF16E32E663C67C ();
// 0x00000564 System.Void MaterialUI.DialogSimpleList::Initialize(MaterialUI.OptionDataList,System.Action`1<System.Int32>,System.String,MaterialUI.ImageData)
extern void DialogSimpleList_Initialize_m58D489BD007D42DACDCC89FB8806315EF14E85E6 ();
// 0x00000565 System.Void MaterialUI.DialogSimpleList::AddItem(MaterialUI.OptionData)
extern void DialogSimpleList_AddItem_m0CC9C28E486271130BD3C0105073A75F1222ECB8 ();
// 0x00000566 System.Void MaterialUI.DialogSimpleList::ClearItems()
extern void DialogSimpleList_ClearItems_m4F61171EEA30221495D949524DDB0AB3B29FAB30 ();
// 0x00000567 System.Void MaterialUI.DialogSimpleList::CreateSelectionItem(System.Int32)
extern void DialogSimpleList_CreateSelectionItem_m5D69575DEADEAB34F262EBD750E486C638DA07CE ();
// 0x00000568 System.Void MaterialUI.DialogSimpleList::OnItemClick(System.Int32)
extern void DialogSimpleList_OnItemClick_m80B1049DF30417E47C32C534465632CDF9411882 ();
// 0x00000569 System.Void MaterialUI.DialogSimpleList::Update()
extern void DialogSimpleList_Update_m03BAD789561603CB484508D7CC5C697880F7A5CF ();
// 0x0000056A System.Void MaterialUI.DialogSimpleList::.ctor()
extern void DialogSimpleList__ctor_m7F19644CDFD26EB8FCA27B038CB8CB77F4C44BBB ();
// 0x0000056B UnityEngine.UI.Text MaterialUI.DialogSimpleOption::get_itemText()
extern void DialogSimpleOption_get_itemText_m8B949036062EDD7803792E3C2A8EC821493202D2 ();
// 0x0000056C System.Void MaterialUI.DialogSimpleOption::set_itemText(UnityEngine.UI.Text)
extern void DialogSimpleOption_set_itemText_mFB558A7F998D3E3BCB31D6E08BAD8E0D156A9AA6 ();
// 0x0000056D UnityEngine.UI.Graphic MaterialUI.DialogSimpleOption::get_itemIcon()
extern void DialogSimpleOption_get_itemIcon_m01B961985AF071155549ED0A0E0666085B3DAC3A ();
// 0x0000056E System.Void MaterialUI.DialogSimpleOption::set_itemIcon(UnityEngine.UI.Graphic)
extern void DialogSimpleOption_set_itemIcon_m2248F4A9F9263AC94FBA14D91B2FFADEEBC210C6 ();
// 0x0000056F MaterialUI.MaterialRipple MaterialUI.DialogSimpleOption::get_itemRipple()
extern void DialogSimpleOption_get_itemRipple_m544D67CEDC28A32CC7183B32362E2C7D71AD25E3 ();
// 0x00000570 System.Void MaterialUI.DialogSimpleOption::set_itemRipple(MaterialUI.MaterialRipple)
extern void DialogSimpleOption_set_itemRipple_m928BFD571FA7DE1458D3FD03F0923E3F2DDED139 ();
// 0x00000571 UnityEngine.RectTransform MaterialUI.DialogSimpleOption::get_rectTransform()
extern void DialogSimpleOption_get_rectTransform_m48876E3ECBB144F2BA172627F84A1D69B0BAE392 ();
// 0x00000572 System.Void MaterialUI.DialogSimpleOption::.ctor()
extern void DialogSimpleOption__ctor_m524D45B309DB1138EBE6779185E25F09E1359301 ();
// 0x00000573 UnityEngine.RectTransform MaterialUI.MaterialDialog::get_rectTransform()
extern void MaterialDialog_get_rectTransform_m0F28B786CF86FCCE6B1E316D8E875EC63F1A32CA ();
// 0x00000574 UnityEngine.CanvasGroup MaterialUI.MaterialDialog::get_canvasGroup()
extern void MaterialDialog_get_canvasGroup_mAB57D6E94F34004A019C9251D778539FCF77A78B ();
// 0x00000575 MaterialUI.DialogAnimator MaterialUI.MaterialDialog::get_dialogAnimator()
extern void MaterialDialog_get_dialogAnimator_m3B05D16E6E106E3BD1044F497571A78BBD168F3B ();
// 0x00000576 System.Void MaterialUI.MaterialDialog::set_dialogAnimator(MaterialUI.DialogAnimator)
extern void MaterialDialog_set_dialogAnimator_m405908524FF4C5963692AE42FACD93C002603A9A ();
// 0x00000577 UnityEngine.CanvasGroup MaterialUI.MaterialDialog::get_backgroundCanvasGroup()
extern void MaterialDialog_get_backgroundCanvasGroup_mF9552A1E6E9E5E28D4773DE097BA0D6882F39F83 ();
// 0x00000578 System.Boolean MaterialUI.MaterialDialog::get_isModal()
extern void MaterialDialog_get_isModal_mA1B86CEB40800CCF7B5A263474EBF127747FE5AF ();
// 0x00000579 System.Void MaterialUI.MaterialDialog::set_isModal(System.Boolean)
extern void MaterialDialog_set_isModal_mEA462642107A238AC9208B5610B5ABA4E98BA03D ();
// 0x0000057A System.Boolean MaterialUI.MaterialDialog::get_destroyOnHide()
extern void MaterialDialog_get_destroyOnHide_mCC81AFA19155EC499F17B7E852BE9CA976E5761B ();
// 0x0000057B System.Void MaterialUI.MaterialDialog::set_destroyOnHide(System.Boolean)
extern void MaterialDialog_set_destroyOnHide_mBD7DE5EF59FA6F42EFEB20AD6B6CBB814E102D1D ();
// 0x0000057C System.Action MaterialUI.MaterialDialog::get_callbackShowAnimationOver()
extern void MaterialDialog_get_callbackShowAnimationOver_mF96219A25F3A70F6FDBCFA5032BB9C92978C83E8 ();
// 0x0000057D System.Void MaterialUI.MaterialDialog::set_callbackShowAnimationOver(System.Action)
extern void MaterialDialog_set_callbackShowAnimationOver_m95FC47E532E9A258DBC9A20A1867198AFA40BD2D ();
// 0x0000057E System.Action MaterialUI.MaterialDialog::get_callbackHideAnimationOver()
extern void MaterialDialog_get_callbackHideAnimationOver_mED92A160CB662D7066DD8810D3BCB7150C77C41A ();
// 0x0000057F System.Void MaterialUI.MaterialDialog::set_callbackHideAnimationOver(System.Action)
extern void MaterialDialog_set_callbackHideAnimationOver_m968B61DF155DC1D0F7D791836AF4DEE258013F55 ();
// 0x00000580 System.Void MaterialUI.MaterialDialog::Initialize()
extern void MaterialDialog_Initialize_mBCBAC6E50D654896852DE7487CB00AED77A2068A ();
// 0x00000581 System.Void MaterialUI.MaterialDialog::ShowModal()
extern void MaterialDialog_ShowModal_m3F6546BB2BE3E0D14A1FB6F24D8154208C388A3C ();
// 0x00000582 System.Void MaterialUI.MaterialDialog::Show()
extern void MaterialDialog_Show_mF4FA5A7BE4344B2CBD1A69D28C35855AED0391BD ();
// 0x00000583 System.Void MaterialUI.MaterialDialog::Hide()
extern void MaterialDialog_Hide_m74AE9042B27A8449BD6B7679D9346844CE96ECCE ();
// 0x00000584 System.Void MaterialUI.MaterialDialog::OnBackgroundClick()
extern void MaterialDialog_OnBackgroundClick_mF396ECCD1E6E02AEDB1C66E6F9445B8558FDF7F9 ();
// 0x00000585 System.Void MaterialUI.MaterialDialog::.ctor()
extern void MaterialDialog__ctor_m1B3C6627F12CB4F4C23CA82179149E320D2CEB11 ();
// 0x00000586 System.Void MaterialUI.MaterialDialog::<Hide>b__31_0()
extern void MaterialDialog_U3CHideU3Eb__31_0_m12951E35A5BAE6A4C88D2438E509E63EFD59015E ();
// 0x00000587 UnityEngine.UI.Text MaterialUI.DialogDatePicker::get_textDate()
extern void DialogDatePicker_get_textDate_m2D01A25A732C6A67D227708508D704674BE1D08B ();
// 0x00000588 UnityEngine.UI.Text MaterialUI.DialogDatePicker::get_textMonth()
extern void DialogDatePicker_get_textMonth_mEEEA33353C11E7A5E5B97B2045C3A89530951D5A ();
// 0x00000589 UnityEngine.UI.Text MaterialUI.DialogDatePicker::get_textYear()
extern void DialogDatePicker_get_textYear_m4FAA71344833E12C5DBACEF1AD70FE5741208095 ();
// 0x0000058A UnityEngine.UI.Image MaterialUI.DialogDatePicker::get_header()
extern void DialogDatePicker_get_header_m9E4329A50E3410FD579FA1B0DEDE415C3CBB9F8C ();
// 0x0000058B System.Void MaterialUI.DialogDatePicker::set_header(UnityEngine.UI.Image)
extern void DialogDatePicker_set_header_mCC064E348855CD04FC03A44D3F623CDA17F420EE ();
// 0x0000058C System.DateTime MaterialUI.DialogDatePicker::get_currentDate()
extern void DialogDatePicker_get_currentDate_m1CA210DF9071F4240AD19273748F3A5A0492AD47 ();
// 0x0000058D System.Void MaterialUI.DialogDatePicker::set_currentDate(System.DateTime)
extern void DialogDatePicker_set_currentDate_m6028FE227248B3B3AF296C1C2DFB8391AACAFD4D ();
// 0x0000058E System.DayOfWeek MaterialUI.DialogDatePicker::get_dayOfWeek()
extern void DialogDatePicker_get_dayOfWeek_mA4AEB47613A4F927D9047CDF99C6CCCA9EE06538 ();
// 0x0000058F System.Globalization.CultureInfo MaterialUI.DialogDatePicker::get_cultureInfo()
extern void DialogDatePicker_get_cultureInfo_m48226C6FE55BD63CDB9F26097415B8413FECE293 ();
// 0x00000590 System.String MaterialUI.DialogDatePicker::get_dateFormatPattern()
extern void DialogDatePicker_get_dateFormatPattern_mA03B1AD24194A82714696B13DF62007D1C526B91 ();
// 0x00000591 System.Void MaterialUI.DialogDatePicker::Awake()
extern void DialogDatePicker_Awake_m1741AA54DF259248FB3C791539610D005D95D6C9 ();
// 0x00000592 System.Void MaterialUI.DialogDatePicker::Initialize(System.Int32,System.Int32,System.Int32,System.Action`1<System.DateTime>,System.Action,UnityEngine.Color)
extern void DialogDatePicker_Initialize_m3DCB5324DE649F8B0554F9D2AB96295C1F057DC4 ();
// 0x00000593 System.Void MaterialUI.DialogDatePicker::SetColor(UnityEngine.Color)
extern void DialogDatePicker_SetColor_m6E4DC3638ECB7DD52DA8F093D8600E2E98B16A4F ();
// 0x00000594 System.Void MaterialUI.DialogDatePicker::SetDate(System.DateTime)
extern void DialogDatePicker_SetDate_m4992D3538A128D5AE2A61D2F2AE5A9CBA9DB89A3 ();
// 0x00000595 System.Void MaterialUI.DialogDatePicker::SetYear(System.Int32)
extern void DialogDatePicker_SetYear_m59180395BB24CFDF556168AE3349F395FD691E57 ();
// 0x00000596 System.Void MaterialUI.DialogDatePicker::SetDayOfWeek(System.DayOfWeek,System.Globalization.CultureInfo)
extern void DialogDatePicker_SetDayOfWeek_m0B37DB206965FEF58BDC82D5573A6D751A920AA3 ();
// 0x00000597 System.Void MaterialUI.DialogDatePicker::SetDateFormatPattern(System.String)
extern void DialogDatePicker_SetDateFormatPattern_m1304D9EEEEF2AEDEA65B55194B39BE62DE0793D2 ();
// 0x00000598 System.Void MaterialUI.DialogDatePicker::UpdateDateList(System.Collections.Generic.List`1<System.DateTime>)
extern void DialogDatePicker_UpdateDateList_m0EBDA7198A01FB1F52AA0B5B4A2D93D8F59D4498 ();
// 0x00000599 System.Void MaterialUI.DialogDatePicker::UpdateDatesText()
extern void DialogDatePicker_UpdateDatesText_m2721ABA8EDECA8426FC019449DE2C3D16891A35C ();
// 0x0000059A System.Void MaterialUI.DialogDatePicker::UpdateDaysText()
extern void DialogDatePicker_UpdateDaysText_mF0715BD082AE8E5E575D5CF99D4AA942DAA92D59 ();
// 0x0000059B System.Void MaterialUI.DialogDatePicker::OnDayItemValueChanged(MaterialUI.DialogDatePickerDayItem,System.Boolean)
extern void DialogDatePicker_OnDayItemValueChanged_m774EB71C5A8645F28162B9D22E59873670827EFE ();
// 0x0000059C System.Void MaterialUI.DialogDatePicker::OnPreviousMonthClicked()
extern void DialogDatePicker_OnPreviousMonthClicked_m80129F31E65DE9395D8729DA1147ACC88742F299 ();
// 0x0000059D System.Void MaterialUI.DialogDatePicker::OnNextMonthClicked()
extern void DialogDatePicker_OnNextMonthClicked_m41D3D5760AF2C6F46AC27958630E791D904FC30A ();
// 0x0000059E System.String MaterialUI.DialogDatePicker::GetFormattedDate(System.DateTime)
extern void DialogDatePicker_GetFormattedDate_m44221E9ABBB1EB0C41A771A7668252D596500F4B ();
// 0x0000059F System.Collections.Generic.List`1<System.DateTime> MaterialUI.DialogDatePicker::GetMonthDateList(System.Int32,System.Int32)
extern void DialogDatePicker_GetMonthDateList_mFC249F9FFB42569320F70E7C11CDEFA713833999 ();
// 0x000005A0 System.Void MaterialUI.DialogDatePicker::OnYearClicked()
extern void DialogDatePicker_OnYearClicked_mDC6F3093B46B50A4F4FB05C7EFC51A136FDDC0AF ();
// 0x000005A1 System.Void MaterialUI.DialogDatePicker::OnDateClicked()
extern void DialogDatePicker_OnDateClicked_m68969A7AB03D2B3FF5FFD49850ABDF1227DFB958 ();
// 0x000005A2 System.Void MaterialUI.DialogDatePicker::OnButtonOkClicked()
extern void DialogDatePicker_OnButtonOkClicked_m700722032A30BBFC3C9DA9879705972CC2CDC9BC ();
// 0x000005A3 System.Void MaterialUI.DialogDatePicker::OnButtonCancelClicked()
extern void DialogDatePicker_OnButtonCancelClicked_mF32E5873972F195268052488DB8FA418617547C5 ();
// 0x000005A4 System.Void MaterialUI.DialogDatePicker::.ctor()
extern void DialogDatePicker__ctor_m1AE855D50C7ADECD6FECD7FDD787BCDD011E5B51 ();
// 0x000005A5 System.Void MaterialUI.DialogDatePicker::<OnYearClicked>b__49_0(System.Single)
extern void DialogDatePicker_U3COnYearClickedU3Eb__49_0_m6D44BE70B670153208895532CFA189E1E0492297 ();
// 0x000005A6 System.Void MaterialUI.DialogDatePicker::<OnYearClicked>b__49_1(System.Single)
extern void DialogDatePicker_U3COnYearClickedU3Eb__49_1_m355006C55BFC069943A69B4ACB7A49E40B021C0E ();
// 0x000005A7 System.Void MaterialUI.DialogDatePicker::<OnDateClicked>b__50_0(System.Single)
extern void DialogDatePicker_U3COnDateClickedU3Eb__50_0_mD00BABD99DA916D95A1775B6F6C6A98AF54C9E8D ();
// 0x000005A8 System.Void MaterialUI.DialogDatePicker::<OnDateClicked>b__50_1(System.Single)
extern void DialogDatePicker_U3COnDateClickedU3Eb__50_1_m384CFD508F9C7C510926EA4DF39D08B0AF9B0F69 ();
// 0x000005A9 System.Void MaterialUI.DialogDatePicker::<OnDateClicked>b__50_2()
extern void DialogDatePicker_U3COnDateClickedU3Eb__50_2_m4025AFEC6B55B252F137B9F9AC978B8904B83020 ();
// 0x000005AA UnityEngine.UI.Text MaterialUI.DialogDatePickerDayItem::get_text()
extern void DialogDatePickerDayItem_get_text_mD776CD087FF36E45029955654AB1963ABC51901E ();
// 0x000005AB UnityEngine.UI.Toggle MaterialUI.DialogDatePickerDayItem::get_toggle()
extern void DialogDatePickerDayItem_get_toggle_m86E70902A2B3041EAFB1A5D30609943BCC0BC2CF ();
// 0x000005AC MaterialUI.VectorImage MaterialUI.DialogDatePickerDayItem::get_selectedImage()
extern void DialogDatePickerDayItem_get_selectedImage_mB973E4F3E0C8C13AE62C692BD99518D50D45F214 ();
// 0x000005AD System.DateTime MaterialUI.DialogDatePickerDayItem::get_dateTime()
extern void DialogDatePickerDayItem_get_dateTime_m48D9B1C0FC28A5C056988F972AD5DA5E647BE852 ();
// 0x000005AE System.Void MaterialUI.DialogDatePickerDayItem::Init(System.DateTime,System.Action`2<MaterialUI.DialogDatePickerDayItem,System.Boolean>)
extern void DialogDatePickerDayItem_Init_m61D4FB6743B70A8E42170D7C302D0207DAA329FA ();
// 0x000005AF System.Void MaterialUI.DialogDatePickerDayItem::UpdateState(System.DateTime)
extern void DialogDatePickerDayItem_UpdateState_m8860063417D3FD10D4B7AE4679E906D8C131C264 ();
// 0x000005B0 System.Void MaterialUI.DialogDatePickerDayItem::OnItemValueChange()
extern void DialogDatePickerDayItem_OnItemValueChange_m9FE3DB877E4E90BED1DA761B34B7AF2E123AC5AD ();
// 0x000005B1 System.Void MaterialUI.DialogDatePickerDayItem::.ctor()
extern void DialogDatePickerDayItem__ctor_m44B7709581D1D7C92D12EFAEB88A31C4C3FB6A26 ();
// 0x000005B2 UnityEngine.UI.Text MaterialUI.DialogDatePickerYearItem::get_text()
extern void DialogDatePickerYearItem_get_text_mE26D3A607F6679F2F735EB7908AF1232C83F8729 ();
// 0x000005B3 UnityEngine.UI.Toggle MaterialUI.DialogDatePickerYearItem::get_toggle()
extern void DialogDatePickerYearItem_get_toggle_m641FEA53B31FCCEE1D666718C750C1B4446481EA ();
// 0x000005B4 MaterialUI.VectorImage MaterialUI.DialogDatePickerYearItem::get_selectedImage()
extern void DialogDatePickerYearItem_get_selectedImage_m8755E7ABDAF42B9A4370C219B0A0D357F7EA3EE0 ();
// 0x000005B5 System.Int32 MaterialUI.DialogDatePickerYearItem::get_year()
extern void DialogDatePickerYearItem_get_year_mB81B72955A03729BA70B4B5BA8E64613E6FDB0C2 ();
// 0x000005B6 System.Void MaterialUI.DialogDatePickerYearItem::set_year(System.Int32)
extern void DialogDatePickerYearItem_set_year_m0473BF894A74020A648EE8EF5DB34394EC0D5914 ();
// 0x000005B7 System.Void MaterialUI.DialogDatePickerYearItem::UpdateState(System.Int32)
extern void DialogDatePickerYearItem_UpdateState_mB74109FF9D2C57787E75FDA84AAFDAADD99223B8 ();
// 0x000005B8 System.Void MaterialUI.DialogDatePickerYearItem::OnItemValueChange()
extern void DialogDatePickerYearItem_OnItemValueChange_m64FF73FFFA4AF2092A1F44EE62816E3F61D36FC4 ();
// 0x000005B9 System.Void MaterialUI.DialogDatePickerYearItem::.ctor()
extern void DialogDatePickerYearItem__ctor_m2E67669A869C042226FC5BB5C19B5E03FD1B61BB ();
// 0x000005BA System.Void MaterialUI.DialogDatePickerYearList::Awake()
extern void DialogDatePickerYearList_Awake_mFA11F31EE2C2D4E025CC92415ECB34906AC9C657 ();
// 0x000005BB MaterialUI.DialogDatePickerYearItem MaterialUI.DialogDatePickerYearList::CreateYearItem(System.Int32,System.Int32)
extern void DialogDatePickerYearList_CreateYearItem_m3F2787A3A664E4AFE47484645A6D05FC39718611 ();
// 0x000005BC System.Void MaterialUI.DialogDatePickerYearList::SetColor(UnityEngine.Color)
extern void DialogDatePickerYearList_SetColor_m3BC09A18F21A663C71B4E5F39710D67C7612C441 ();
// 0x000005BD System.Void MaterialUI.DialogDatePickerYearList::OnItemClick(System.Int32)
extern void DialogDatePickerYearList_OnItemClick_mF7BBEC177672063288F206E00C343C84991E22CE ();
// 0x000005BE System.Void MaterialUI.DialogDatePickerYearList::CenterToSelectedYear(System.Int32)
extern void DialogDatePickerYearList_CenterToSelectedYear_m143C342DD710C2DA40609F9D7CEB26B64D154E5D ();
// 0x000005BF System.Void MaterialUI.DialogDatePickerYearList::.ctor()
extern void DialogDatePickerYearList__ctor_m322259CBB518DFEF91B3E4ADA0ACD61514C801D0 ();
// 0x000005C0 UnityEngine.UI.Image MaterialUI.DialogTimePicker::get_clockNeedle()
extern void DialogTimePicker_get_clockNeedle_m5A6DF0DE6AD5A7C7A3D105581A83B72E607ED4D2 ();
// 0x000005C1 UnityEngine.UI.Text[] MaterialUI.DialogTimePicker::get_clockTextArray()
extern void DialogTimePicker_get_clockTextArray_m3614CC2EF8B72B7D11EEEAE389150D4CE4EF943F ();
// 0x000005C2 UnityEngine.UI.Text MaterialUI.DialogTimePicker::get_textAM()
extern void DialogTimePicker_get_textAM_mB57536E8587C4C143AE8B03F5AB57B9BC756BC49 ();
// 0x000005C3 UnityEngine.UI.Text MaterialUI.DialogTimePicker::get_textPM()
extern void DialogTimePicker_get_textPM_m201BD0E6AC7D7DB614BF292D6872E0B8251F83AA ();
// 0x000005C4 UnityEngine.UI.Text MaterialUI.DialogTimePicker::get_textHours()
extern void DialogTimePicker_get_textHours_m99F830D4108BE6F64825D90BC49572F7CAC10CE6 ();
// 0x000005C5 UnityEngine.UI.Text MaterialUI.DialogTimePicker::get_textMinutes()
extern void DialogTimePicker_get_textMinutes_m4105629C7424CBB9C6A4D154C1CB46DCBAE30863 ();
// 0x000005C6 System.Int32 MaterialUI.DialogTimePicker::get_currentHour()
extern void DialogTimePicker_get_currentHour_m4CBF661A9BDEEEECD2395A3BCA71B91A06252C0A ();
// 0x000005C7 System.Void MaterialUI.DialogTimePicker::set_currentHour(System.Int32)
extern void DialogTimePicker_set_currentHour_mDB4D0AFEB8CAAA12DD020E70BF1D8C44724D81E6 ();
// 0x000005C8 System.Int32 MaterialUI.DialogTimePicker::get_currentMinute()
extern void DialogTimePicker_get_currentMinute_m716D822EC1681AE1DF20234F51936B0629D97364 ();
// 0x000005C9 System.Void MaterialUI.DialogTimePicker::set_currentMinute(System.Int32)
extern void DialogTimePicker_set_currentMinute_m92392932EEBC66DF7E0D425340345EBDE2A0C9C0 ();
// 0x000005CA System.Boolean MaterialUI.DialogTimePicker::get_isAM()
extern void DialogTimePicker_get_isAM_m75FC0D6E54A4827D256B3F4532D1ED721F18B97A ();
// 0x000005CB System.Void MaterialUI.DialogTimePicker::set_isAM(System.Boolean)
extern void DialogTimePicker_set_isAM_mA6C4F96EC2884A343B0E0DA365BB34BE9F13F64A ();
// 0x000005CC System.Boolean MaterialUI.DialogTimePicker::get_isHoursSelected()
extern void DialogTimePicker_get_isHoursSelected_m10A76B39897705C0656230CBD0CED53407213388 ();
// 0x000005CD System.Void MaterialUI.DialogTimePicker::set_isHoursSelected(System.Boolean)
extern void DialogTimePicker_set_isHoursSelected_m52D3F6AE5F5C37356E5D7468E0FBC823494752F6 ();
// 0x000005CE System.Action`1<System.DateTime> MaterialUI.DialogTimePicker::get_onAffirmativeClicked()
extern void DialogTimePicker_get_onAffirmativeClicked_m3BE417644371422556B2CB3B9276FA9E4F39B1A3 ();
// 0x000005CF System.Void MaterialUI.DialogTimePicker::set_onAffirmativeClicked(System.Action`1<System.DateTime>)
extern void DialogTimePicker_set_onAffirmativeClicked_m7B44B7F9EBF817F5F43D6EA13B5C453C2ED87DD5 ();
// 0x000005D0 MaterialUI.MaterialButton MaterialUI.DialogTimePicker::get_affirmativeButton()
extern void DialogTimePicker_get_affirmativeButton_m961612D71DFC616195A55F2737993C200CAD4DB1 ();
// 0x000005D1 System.Void MaterialUI.DialogTimePicker::set_affirmativeButton(MaterialUI.MaterialButton)
extern void DialogTimePicker_set_affirmativeButton_mA2198F37F334712216007172BF7A70482AD8560C ();
// 0x000005D2 MaterialUI.MaterialButton MaterialUI.DialogTimePicker::get_dismissiveButton()
extern void DialogTimePicker_get_dismissiveButton_m6F0D5E158D59201F78F0CAC200E580FC3003A13E ();
// 0x000005D3 System.Void MaterialUI.DialogTimePicker::set_dismissiveButton(MaterialUI.MaterialButton)
extern void DialogTimePicker_set_dismissiveButton_mE36692BDB20648A93FF6C61CA3F1DFDA4C23567E ();
// 0x000005D4 UnityEngine.UI.Image MaterialUI.DialogTimePicker::get_header()
extern void DialogTimePicker_get_header_m72FF339C9E6E63E33B55FA85DF97BBF8DABE1F7F ();
// 0x000005D5 System.Void MaterialUI.DialogTimePicker::set_header(UnityEngine.UI.Image)
extern void DialogTimePicker_set_header_m29269B2F41F389E41BE09083A72ABAB2F5EB5476 ();
// 0x000005D6 MaterialUI.VectorImage MaterialUI.DialogTimePicker::get_needleCenter()
extern void DialogTimePicker_get_needleCenter_mB9A95C17A6DEF95782F454298C09952E1A384CCA ();
// 0x000005D7 System.Void MaterialUI.DialogTimePicker::set_needleCenter(MaterialUI.VectorImage)
extern void DialogTimePicker_set_needleCenter_m0DF7EAF276F0908DD02975D598038E0370AB1E50 ();
// 0x000005D8 MaterialUI.VectorImage MaterialUI.DialogTimePicker::get_needleEnd()
extern void DialogTimePicker_get_needleEnd_m42305735FE2C91E999A407FEE7BEDB67D7F3C5E5 ();
// 0x000005D9 System.Void MaterialUI.DialogTimePicker::set_needleEnd(MaterialUI.VectorImage)
extern void DialogTimePicker_set_needleEnd_mFED735B88EEB20A36E4E1C9A2542D5E8CF5B9083 ();
// 0x000005DA System.Void MaterialUI.DialogTimePicker::Initialize(System.DateTime,System.Action`1<System.DateTime>,UnityEngine.Color)
extern void DialogTimePicker_Initialize_mBE1A3A5A3965EAF3CB885A15862FAD89F141D504 ();
// 0x000005DB System.Void MaterialUI.DialogTimePicker::Show()
extern void DialogTimePicker_Show_m9D2CDDE965038412B91E9A97CE41215582AD8BC3 ();
// 0x000005DC System.Void MaterialUI.DialogTimePicker::SetColor(UnityEngine.Color)
extern void DialogTimePicker_SetColor_m5A9A67CDC8A0735D7326F5B5A47E7FCD04DBD843 ();
// 0x000005DD System.Void MaterialUI.DialogTimePicker::Update()
extern void DialogTimePicker_Update_mC39CA2F62529EC8CF75EB9C31FEC44E67E0E8823 ();
// 0x000005DE System.Single MaterialUI.DialogTimePicker::GetAngleFromHour(System.Int32)
extern void DialogTimePicker_GetAngleFromHour_m176FD43BD1F042EE5C1B88A8FFD3BCE4EF48CA65 ();
// 0x000005DF System.Int32 MaterialUI.DialogTimePicker::GetHourFromAngle(System.Single)
extern void DialogTimePicker_GetHourFromAngle_m55CAD730FC99C9AB7B425B68D52449C19DD6AE74 ();
// 0x000005E0 System.Single MaterialUI.DialogTimePicker::GetAngleFromMinute(System.Int32)
extern void DialogTimePicker_GetAngleFromMinute_mEB8C423923D5DA1B3A1168104A72FC3D119E50E1 ();
// 0x000005E1 System.Int32 MaterialUI.DialogTimePicker::GetMinuteFromAngle(System.Single)
extern void DialogTimePicker_GetMinuteFromAngle_m864D478970FECEFD5C9206BA07443E1564FFCA64 ();
// 0x000005E2 System.Void MaterialUI.DialogTimePicker::SetAngle(System.Single)
extern void DialogTimePicker_SetAngle_m942DC9C092D0799C2131684E014EB556E6AEF345 ();
// 0x000005E3 System.Void MaterialUI.DialogTimePicker::SetHours(System.Int32,System.Boolean)
extern void DialogTimePicker_SetHours_mCEB5809D286FAD00C838CAFF1C572B9D8E3D1C2F ();
// 0x000005E4 System.Void MaterialUI.DialogTimePicker::SetMinutes(System.Int32,System.Boolean)
extern void DialogTimePicker_SetMinutes_mCD15DD7735CF8C7BDBD08BBC2A1ECA8ED7CE2233 ();
// 0x000005E5 System.Void MaterialUI.DialogTimePicker::SetTime(System.Int32,System.Int32)
extern void DialogTimePicker_SetTime_m0AED0A0E7056BA8DA8F78A779B475DC1FE0FE8E9 ();
// 0x000005E6 System.Void MaterialUI.DialogTimePicker::UpdateNeedle()
extern void DialogTimePicker_UpdateNeedle_mBE1D8E909A7F93A2AE4B0B351D818B983284F77F ();
// 0x000005E7 System.Void MaterialUI.DialogTimePicker::UpdateNeedleAngle(System.Single)
extern void DialogTimePicker_UpdateNeedleAngle_m2B24D066C58CDE8DE3B53F6B36FAAB3B396D0073 ();
// 0x000005E8 System.Void MaterialUI.DialogTimePicker::UpdateClockTextArray()
extern void DialogTimePicker_UpdateClockTextArray_m6EA4ACDAF2AF052561A6BC77DD40563E4AC186C6 ();
// 0x000005E9 System.Void MaterialUI.DialogTimePicker::OnAMClicked()
extern void DialogTimePicker_OnAMClicked_mFEE8AE86BCFB1043DCB1187A82D264F485B284AA ();
// 0x000005EA System.Void MaterialUI.DialogTimePicker::OnPMClicked()
extern void DialogTimePicker_OnPMClicked_mD34D45E1F3888D8CEEDBA938B9664DB05FE6E93B ();
// 0x000005EB System.Void MaterialUI.DialogTimePicker::OnHoursClicked()
extern void DialogTimePicker_OnHoursClicked_m603987F5C6D940F200F45C1C39E716E6986D9284 ();
// 0x000005EC System.Void MaterialUI.DialogTimePicker::SelectHours()
extern void DialogTimePicker_SelectHours_mF38B888720646362FF490F39C62F86E7E9B4506B ();
// 0x000005ED System.Void MaterialUI.DialogTimePicker::OnMinutesClicked()
extern void DialogTimePicker_OnMinutesClicked_m920CFB727BDBAC188013EDB6EE92AC6EEC48A525 ();
// 0x000005EE System.Void MaterialUI.DialogTimePicker::SelectMinutes()
extern void DialogTimePicker_SelectMinutes_m8E1A158420539CFCF393ABC53540FBEA4E2089FA ();
// 0x000005EF System.Void MaterialUI.DialogTimePicker::OnButtonOkClicked()
extern void DialogTimePicker_OnButtonOkClicked_m4F02B0EA767CBD64FE37E71A45628EEEC42D2C2C ();
// 0x000005F0 System.Void MaterialUI.DialogTimePicker::.ctor()
extern void DialogTimePicker__ctor_mBD350B2FB7D794D37E0CC7FD8B01E1BA8543FAD1 ();
// 0x000005F1 System.Void MaterialUI.DialogTimePicker::<Show>b__63_0()
extern void DialogTimePicker_U3CShowU3Eb__63_0_m25B07553B4C130B4BBB5021B92D02F526FC15921 ();
// 0x000005F2 System.Void MaterialUI.DialogTimePickerClock::Start()
extern void DialogTimePickerClock_Start_m80EA5AB016A2132B685571F7DD72AE4631273FF4 ();
// 0x000005F3 System.Void MaterialUI.DialogTimePickerClock::Init()
extern void DialogTimePickerClock_Init_mB947053CF8C325A401EFC39F33D56CE85D47F6E4 ();
// 0x000005F4 System.Void MaterialUI.DialogTimePickerClock::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DialogTimePickerClock_OnDrag_mDEBB5CE153F3CDAE38DFCACC8C76BDA71B54CD82 ();
// 0x000005F5 System.Void MaterialUI.DialogTimePickerClock::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DialogTimePickerClock_OnPointerClick_mE7DA03E39B2BF4EA6D66C44A14904E9E78CFB239 ();
// 0x000005F6 System.Void MaterialUI.DialogTimePickerClock::handleData(UnityEngine.EventSystems.PointerEventData)
extern void DialogTimePickerClock_handleData_mB351EE182699CC2795F661D9BCD5F56A9AE4CA45 ();
// 0x000005F7 System.Void MaterialUI.DialogTimePickerClock::.ctor()
extern void DialogTimePickerClock__ctor_mB35FF991104936E43AD04E372CEE3F3D8AFBA5F4 ();
// 0x000005F8 UnityEngine.UI.Text MaterialUI.MaterialAppBar::get_titleText()
extern void MaterialAppBar_get_titleText_m3AEDBF46DAE3B654BECD7110B88C96116F0FC460 ();
// 0x000005F9 System.Void MaterialUI.MaterialAppBar::set_titleText(UnityEngine.UI.Text)
extern void MaterialAppBar_set_titleText_mF3FA16D3A9A6C58827463FF8DBB0E0C2E76CD686 ();
// 0x000005FA UnityEngine.UI.Graphic MaterialUI.MaterialAppBar::get_panelGraphic()
extern void MaterialAppBar_get_panelGraphic_m1EDF82B28B1365FEE126E54FBECCC1DCCEE52D28 ();
// 0x000005FB System.Void MaterialUI.MaterialAppBar::set_panelGraphic(UnityEngine.UI.Graphic)
extern void MaterialAppBar_set_panelGraphic_mDB7FD3742A2A44363626019908279E211B872C77 ();
// 0x000005FC MaterialUI.AnimatedShadow MaterialUI.MaterialAppBar::get_shadow()
extern void MaterialAppBar_get_shadow_mCB74E5E3D09497262D901D7661B09427DD9BF0AC ();
// 0x000005FD System.Void MaterialUI.MaterialAppBar::set_shadow(MaterialUI.AnimatedShadow)
extern void MaterialAppBar_set_shadow_m5D44687D2940E242324420B9A1D101DFADC30FDE ();
// 0x000005FE MaterialUI.MaterialButton[] MaterialUI.MaterialAppBar::get_buttons()
extern void MaterialAppBar_get_buttons_m2132C36A312C9A960A2E81A722E3E24F9D36F49C ();
// 0x000005FF System.Void MaterialUI.MaterialAppBar::set_buttons(MaterialUI.MaterialButton[])
extern void MaterialAppBar_set_buttons_mDD9AEBFDBD782FC5A8AAD85495B4B543110A913B ();
// 0x00000600 System.Single MaterialUI.MaterialAppBar::get_animationDuration()
extern void MaterialAppBar_get_animationDuration_mDF12690FF9B35F4335D039741CC7EE56BC62C9EF ();
// 0x00000601 System.Void MaterialUI.MaterialAppBar::set_animationDuration(System.Single)
extern void MaterialAppBar_set_animationDuration_m308ADA38BFEFC2A18B4AF20E8C1FC382DCA34DDD ();
// 0x00000602 UnityEngine.UI.Graphic MaterialUI.MaterialAppBar::get_shadowGraphic()
extern void MaterialAppBar_get_shadowGraphic_m6E60B92333A00A4093FBD8F9F2DCAFD1A8102C7C ();
// 0x00000603 System.Void MaterialUI.MaterialAppBar::SetPanelColor(UnityEngine.Color,System.Boolean)
extern void MaterialAppBar_SetPanelColor_mBB39A77E9E999BE03C1E0161821F8FE3EBD5677F ();
// 0x00000604 System.Void MaterialUI.MaterialAppBar::SetTitleTextColor(UnityEngine.Color,System.Boolean)
extern void MaterialAppBar_SetTitleTextColor_m26E13225039763551D642CF8AF77306F37ABEF72 ();
// 0x00000605 System.Void MaterialUI.MaterialAppBar::SetButtonGraphicColor(System.Int32,UnityEngine.Color,MaterialUI.MaterialAppBar_ButtonElement,System.Boolean)
extern void MaterialAppBar_SetButtonGraphicColor_m23D63D075607866E5ACB210E6BCCF7CC86E86A4D ();
// 0x00000606 System.Void MaterialUI.MaterialAppBar::SetButtonsGraphicColors(UnityEngine.Color,MaterialUI.MaterialAppBar_ButtonElement,System.Boolean)
extern void MaterialAppBar_SetButtonsGraphicColors_mB106C54333F41CBBBA47005DECF51FDA3586E244 ();
// 0x00000607 System.Void MaterialUI.MaterialAppBar::TweenGraphicColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Boolean,System.Action)
extern void MaterialAppBar_TweenGraphicColor_m5CF427E3FBFB06285FAB17FC1F3EEB34B2961372 ();
// 0x00000608 System.Void MaterialUI.MaterialAppBar::.ctor()
extern void MaterialAppBar__ctor_m0D5AB530ACA2C8BE743148392DE7990BD40B4B08 ();
// 0x00000609 System.Void MaterialUI.MaterialAppBar::<SetPanelColor>b__24_0(UnityEngine.Color)
extern void MaterialAppBar_U3CSetPanelColorU3Eb__24_0_mA957D6BD4703CBEF4C9296CB3ED31421B49378A5 ();
// 0x0000060A UnityEngine.UI.Graphic MaterialUI.MaterialCheckbox::get_checkImage()
extern void MaterialCheckbox_get_checkImage_m6C59140FBE9EF5A19A8AD08CFACA04055840602E ();
// 0x0000060B System.Void MaterialUI.MaterialCheckbox::set_checkImage(UnityEngine.UI.Graphic)
extern void MaterialCheckbox_set_checkImage_m97BD6482E31357E49C598E9F09AAD1A1285FA100 ();
// 0x0000060C UnityEngine.UI.Graphic MaterialUI.MaterialCheckbox::get_frameImage()
extern void MaterialCheckbox_get_frameImage_m80DB1A82DAFD8A013C39F2F8195D59695BB9D668 ();
// 0x0000060D System.Void MaterialUI.MaterialCheckbox::set_frameImage(UnityEngine.UI.Graphic)
extern void MaterialCheckbox_set_frameImage_mB5EAF2985702AABC945A7D0D0259A4FF0B4BD2E7 ();
// 0x0000060E UnityEngine.RectTransform MaterialUI.MaterialCheckbox::get_checkRectTransform()
extern void MaterialCheckbox_get_checkRectTransform_mE5C34D6E49B828F7BBA882CC58D4676E71908518 ();
// 0x0000060F System.Void MaterialUI.MaterialCheckbox::OnEnable()
extern void MaterialCheckbox_OnEnable_mE0FC45FCC13913CC46A66679729EAB8B8D134066 ();
// 0x00000610 System.Void MaterialUI.MaterialCheckbox::TurnOn()
extern void MaterialCheckbox_TurnOn_m30C008CEC9AB61EC54D15E9EFB4DB62354F997DC ();
// 0x00000611 System.Void MaterialUI.MaterialCheckbox::TurnOnInstant()
extern void MaterialCheckbox_TurnOnInstant_m26473D385FA03044B001C671131CCAC6249C0F8D ();
// 0x00000612 System.Void MaterialUI.MaterialCheckbox::TurnOff()
extern void MaterialCheckbox_TurnOff_m651FD9B9E11A6740FC386B03E92CE67799F5B389 ();
// 0x00000613 System.Void MaterialUI.MaterialCheckbox::TurnOffInstant()
extern void MaterialCheckbox_TurnOffInstant_m053F4446A14A9524D7F4E0C38AD2EF4693F149B4 ();
// 0x00000614 System.Void MaterialUI.MaterialCheckbox::Enable()
extern void MaterialCheckbox_Enable_mF8FEA23D4B91BB1575141D8E5DC51BF71FF78F40 ();
// 0x00000615 System.Void MaterialUI.MaterialCheckbox::Disable()
extern void MaterialCheckbox_Disable_m33C5DA458A48F1AC6238B6EC8B6C6C7E9399BF28 ();
// 0x00000616 System.Void MaterialUI.MaterialCheckbox::AnimOn()
extern void MaterialCheckbox_AnimOn_m6866E3FAA6F8A8193691414971E474518019497A ();
// 0x00000617 System.Void MaterialUI.MaterialCheckbox::AnimOnComplete()
extern void MaterialCheckbox_AnimOnComplete_m6DD3110A0E254224E867417DF0A1F41E6F092658 ();
// 0x00000618 System.Void MaterialUI.MaterialCheckbox::AnimOff()
extern void MaterialCheckbox_AnimOff_m784DAA17EB9A3B57C193AD926BC994FFF322A544 ();
// 0x00000619 System.Void MaterialUI.MaterialCheckbox::AnimOffComplete()
extern void MaterialCheckbox_AnimOffComplete_m7E0A47509F4AFAAFCC9AF6F2CC8E695AB133960E ();
// 0x0000061A System.Void MaterialUI.MaterialCheckbox::.ctor()
extern void MaterialCheckbox__ctor_m2CF29A6D68E4DFCC71543437FBEDB5A6BD9F6720 ();
// 0x0000061B MaterialUI.MaterialDropdown_VerticalPivotType MaterialUI.MaterialDropdown::get_verticalPivotType()
extern void MaterialDropdown_get_verticalPivotType_m17C662780078435F773516AEB8E085C6C8F9C02E ();
// 0x0000061C System.Void MaterialUI.MaterialDropdown::set_verticalPivotType(MaterialUI.MaterialDropdown_VerticalPivotType)
extern void MaterialDropdown_set_verticalPivotType_mF3678DAF56BE849BBB0DC2193424D701BA867114 ();
// 0x0000061D MaterialUI.MaterialDropdown_HorizontalPivotType MaterialUI.MaterialDropdown::get_horizontalPivotType()
extern void MaterialDropdown_get_horizontalPivotType_m8F6789A54EE273B7E5717E64286DAAA4433821A6 ();
// 0x0000061E System.Void MaterialUI.MaterialDropdown::set_horizontalPivotType(MaterialUI.MaterialDropdown_HorizontalPivotType)
extern void MaterialDropdown_set_horizontalPivotType_m51B2BEAFC02E02989214F733D8D7F34B51A97F4C ();
// 0x0000061F MaterialUI.MaterialDropdown_ExpandStartType MaterialUI.MaterialDropdown::get_expandStartType()
extern void MaterialDropdown_get_expandStartType_m045FA5893701D4131786B4C5122549C220D064F3 ();
// 0x00000620 System.Void MaterialUI.MaterialDropdown::set_expandStartType(MaterialUI.MaterialDropdown_ExpandStartType)
extern void MaterialDropdown_set_expandStartType_mA34428BA5D84F92B9D1DC9D52E6F429EE79B6475 ();
// 0x00000621 System.Single MaterialUI.MaterialDropdown::get_ignoreInputAfterShowTimer()
extern void MaterialDropdown_get_ignoreInputAfterShowTimer_m0C7A5B9A845DB3EC765DCE8C1A6AE35FE1D22157 ();
// 0x00000622 System.Void MaterialUI.MaterialDropdown::set_ignoreInputAfterShowTimer(System.Single)
extern void MaterialDropdown_set_ignoreInputAfterShowTimer_m1838845EED46DB772DD6397CB3F7D7DCA1CFD4BD ();
// 0x00000623 System.Single MaterialUI.MaterialDropdown::get_maxHeight()
extern void MaterialDropdown_get_maxHeight_m3A49672E7EAE90F5A81781E3D07F1A9CE9CE7ADF ();
// 0x00000624 System.Void MaterialUI.MaterialDropdown::set_maxHeight(System.Single)
extern void MaterialDropdown_set_maxHeight_m1753703B49A2C0F5169EEB79042F52B6AD5661E4 ();
// 0x00000625 System.Boolean MaterialUI.MaterialDropdown::get_capitalizeButtonText()
extern void MaterialDropdown_get_capitalizeButtonText_m5E2329D46984067C90BECF71697DF86390BF8C51 ();
// 0x00000626 System.Void MaterialUI.MaterialDropdown::set_capitalizeButtonText(System.Boolean)
extern void MaterialDropdown_set_capitalizeButtonText_m7BFA1C7160BD9D3339BB4D843A5F446C605CEFD9 ();
// 0x00000627 System.Boolean MaterialUI.MaterialDropdown::get_highlightCurrentlySelected()
extern void MaterialDropdown_get_highlightCurrentlySelected_m5CB56F8002D860C432D7EDA26F02F7CCE878943E ();
// 0x00000628 System.Void MaterialUI.MaterialDropdown::set_highlightCurrentlySelected(System.Boolean)
extern void MaterialDropdown_set_highlightCurrentlySelected_mD095CDFBD951A0A3E2EFF562F8CCE586698CDA11 ();
// 0x00000629 System.Boolean MaterialUI.MaterialDropdown::get_updateHeader()
extern void MaterialDropdown_get_updateHeader_m00669CB538C243327CEE7E8787D26F47E3EA3286 ();
// 0x0000062A System.Void MaterialUI.MaterialDropdown::set_updateHeader(System.Boolean)
extern void MaterialDropdown_set_updateHeader_mCD02153D9AC8FB3335236983D0AF8B899B75C0B5 ();
// 0x0000062B System.Single MaterialUI.MaterialDropdown::get_animationDuration()
extern void MaterialDropdown_get_animationDuration_m2C624B71CF3D14BFB0BD2EA763D2A9903DE1B65E ();
// 0x0000062C System.Void MaterialUI.MaterialDropdown::set_animationDuration(System.Single)
extern void MaterialDropdown_set_animationDuration_m717E0DA7219DF1F7D91E039D15B2DC540762F622 ();
// 0x0000062D System.Single MaterialUI.MaterialDropdown::get_minDistanceFromEdge()
extern void MaterialDropdown_get_minDistanceFromEdge_m49B6F75C01F15ECD0C0FC7032FA8EB7B0EC64D64 ();
// 0x0000062E System.Void MaterialUI.MaterialDropdown::set_minDistanceFromEdge(System.Single)
extern void MaterialDropdown_set_minDistanceFromEdge_m17F5EDFF8E54F4EED57216A19529520CB6DE3A08 ();
// 0x0000062F UnityEngine.Color MaterialUI.MaterialDropdown::get_panelColor()
extern void MaterialDropdown_get_panelColor_mE18F4C0E14045A8A6123DCDBC346DDDF6FAD717C ();
// 0x00000630 System.Void MaterialUI.MaterialDropdown::set_panelColor(UnityEngine.Color)
extern void MaterialDropdown_set_panelColor_m32ED9280418487ECC2EFE832368FAF66B79A9824 ();
// 0x00000631 MaterialUI.RippleData MaterialUI.MaterialDropdown::get_itemRippleData()
extern void MaterialDropdown_get_itemRippleData_m34FB18C0E092D8B24F985D13941C23941D2FA9C6 ();
// 0x00000632 System.Void MaterialUI.MaterialDropdown::set_itemRippleData(MaterialUI.RippleData)
extern void MaterialDropdown_set_itemRippleData_m14A2CE10B2C5771DC25A5E4EB44EE37E3F0D28BA ();
// 0x00000633 UnityEngine.Color MaterialUI.MaterialDropdown::get_itemTextColor()
extern void MaterialDropdown_get_itemTextColor_m3E212358B84399CB77CFD1A826B6F6F1FAE50541 ();
// 0x00000634 System.Void MaterialUI.MaterialDropdown::set_itemTextColor(UnityEngine.Color)
extern void MaterialDropdown_set_itemTextColor_m93C209EE2417233AB0ECB9C12A552B58E8CB74D5 ();
// 0x00000635 UnityEngine.Color MaterialUI.MaterialDropdown::get_itemIconColor()
extern void MaterialDropdown_get_itemIconColor_mD004FAB58449ECFBF57680B7D1CA267A5D7816DF ();
// 0x00000636 System.Void MaterialUI.MaterialDropdown::set_itemIconColor(UnityEngine.Color)
extern void MaterialDropdown_set_itemIconColor_m1C9B247B1A89ADCC89A5924F799811C14DE5ABC1 ();
// 0x00000637 UnityEngine.RectTransform MaterialUI.MaterialDropdown::get_baseTransform()
extern void MaterialDropdown_get_baseTransform_mE3C48B59B1B6AFBC5884E79E1B36FEFED0635BD0 ();
// 0x00000638 System.Void MaterialUI.MaterialDropdown::set_baseTransform(UnityEngine.RectTransform)
extern void MaterialDropdown_set_baseTransform_mA2F250D8ED1418788B1D84DD965F638693A14F52 ();
// 0x00000639 UnityEngine.UI.Selectable MaterialUI.MaterialDropdown::get_baseSelectable()
extern void MaterialDropdown_get_baseSelectable_mDBD10CD59A0EBF0F5EE0CE5A51C5BDEA60381186 ();
// 0x0000063A System.Void MaterialUI.MaterialDropdown::set_baseSelectable(UnityEngine.UI.Selectable)
extern void MaterialDropdown_set_baseSelectable_m3C305A879C9B07D59B0C154B8B5718FC5B889F86 ();
// 0x0000063B UnityEngine.UI.Text MaterialUI.MaterialDropdown::get_buttonTextContent()
extern void MaterialDropdown_get_buttonTextContent_m8A21088B652FE50D04B43411D20AD79671861E7A ();
// 0x0000063C System.Void MaterialUI.MaterialDropdown::set_buttonTextContent(UnityEngine.UI.Text)
extern void MaterialDropdown_set_buttonTextContent_mABA2983B63661A13131A4F36525172087EEBFDE6 ();
// 0x0000063D UnityEngine.UI.Graphic MaterialUI.MaterialDropdown::get_buttonImageContent()
extern void MaterialDropdown_get_buttonImageContent_mA4F41CDA76F705C4BD9D5739F0FF8FF0FF7ACE43 ();
// 0x0000063E System.Void MaterialUI.MaterialDropdown::set_buttonImageContent(UnityEngine.UI.Graphic)
extern void MaterialDropdown_set_buttonImageContent_mE4429F440D93725963CDA9D0BA8210309458EC91 ();
// 0x0000063F System.Int32 MaterialUI.MaterialDropdown::get_currentlySelected()
extern void MaterialDropdown_get_currentlySelected_m93D85C4FF1471B23A48557B45A8F4452947DCDE7 ();
// 0x00000640 System.Void MaterialUI.MaterialDropdown::set_currentlySelected(System.Int32)
extern void MaterialDropdown_set_currentlySelected_m75BBD1FD7942A6B5CA943B27E1DFE8A9339E5E20 ();
// 0x00000641 MaterialUI.OptionDataList MaterialUI.MaterialDropdown::get_optionDataList()
extern void MaterialDropdown_get_optionDataList_m8E17EF465851F6E97E049DDCE026C3C9CADB4602 ();
// 0x00000642 System.Void MaterialUI.MaterialDropdown::set_optionDataList(MaterialUI.OptionDataList)
extern void MaterialDropdown_set_optionDataList_m9FE200014B83DB38EF2769691BA3F9C5118E8DEE ();
// 0x00000643 MaterialUI.MaterialDropdown_MaterialDropdownEvent MaterialUI.MaterialDropdown::get_onItemSelected()
extern void MaterialDropdown_get_onItemSelected_m7A90E8CD07E72CEB5BF268373F40BEFB2791739C ();
// 0x00000644 System.Void MaterialUI.MaterialDropdown::set_onItemSelected(MaterialUI.MaterialDropdown_MaterialDropdownEvent)
extern void MaterialDropdown_set_onItemSelected_m4BAA097E067E9235DD82067918BC2760C819390E ();
// 0x00000645 System.Collections.Generic.List`1<MaterialUI.MaterialDropdown_DropdownListItem> MaterialUI.MaterialDropdown::get_listItems()
extern void MaterialDropdown_get_listItems_mF5DADC9EC229C54E3225A236254DC8353E94013A ();
// 0x00000646 System.Void MaterialUI.MaterialDropdown::set_listItems(System.Collections.Generic.List`1<MaterialUI.MaterialDropdown_DropdownListItem>)
extern void MaterialDropdown_set_listItems_m2F5120EA27E0F067F6337ACF48778F770F3FC064 ();
// 0x00000647 MaterialUI.MaterialUIScaler MaterialUI.MaterialDropdown::get_scaler()
extern void MaterialDropdown_get_scaler_m3D65B4BDD18D5A64F7F2D993BEC0F8438E63B7D3 ();
// 0x00000648 System.Void MaterialUI.MaterialDropdown::AddData(MaterialUI.OptionData)
extern void MaterialDropdown_AddData_m1682785E3D52952E1BFD750C1B58791F2AA07FA1 ();
// 0x00000649 System.Void MaterialUI.MaterialDropdown::AddData(MaterialUI.OptionData[])
extern void MaterialDropdown_AddData_mBBF08979E6D33F52358492794E6364EDA91BEA4B ();
// 0x0000064A System.Void MaterialUI.MaterialDropdown::RemoveData(MaterialUI.OptionData)
extern void MaterialDropdown_RemoveData_m13CC57B5CA6459BF56AB69C146E665F39A826038 ();
// 0x0000064B System.Void MaterialUI.MaterialDropdown::RemoveData(MaterialUI.OptionData[])
extern void MaterialDropdown_RemoveData_m57B85402A4B2EF4D799259AADE0F6DCDC6EA22B0 ();
// 0x0000064C System.Void MaterialUI.MaterialDropdown::ClearData()
extern void MaterialDropdown_ClearData_mCCF133EBAA73AB8C40E9DF231C87828CECE7E131 ();
// 0x0000064D System.Void MaterialUI.MaterialDropdown::Start()
extern void MaterialDropdown_Start_mBB08DBAE150163C0C8E447B2A5CF788B0547769E ();
// 0x0000064E System.Void MaterialUI.MaterialDropdown::Show()
extern void MaterialDropdown_Show_mB2F741A1C1DD657F20A52C7D89218DF300379305 ();
// 0x0000064F System.Void MaterialUI.MaterialDropdown::UpdateDropdownPos(UnityEngine.Vector3)
extern void MaterialDropdown_UpdateDropdownPos_m7379B5B5B5B74125A9F82DBAB75E14B083EE5557 ();
// 0x00000650 System.Void MaterialUI.MaterialDropdown::Hide()
extern void MaterialDropdown_Hide_m16E5770C70C6321C8739B710ADCD1161341B9D54 ();
// 0x00000651 System.Void MaterialUI.MaterialDropdown::Select(System.Int32,System.Boolean)
extern void MaterialDropdown_Select_m609C6DE961D858BDAEEC96F1560F395E303EE39F ();
// 0x00000652 UnityEngine.Vector3 MaterialUI.MaterialDropdown::CalculatedPosition()
extern void MaterialDropdown_CalculatedPosition_mF9ADDE56E870FC9E7BF71690419FC5829AAA4DCC ();
// 0x00000653 MaterialUI.MaterialDropdown_DropdownListItem MaterialUI.MaterialDropdown::CreateItem(MaterialUI.OptionData,System.Int32)
extern void MaterialDropdown_CreateItem_mEAF6225465C0819152DEF279D27B85FB635CD2F8 ();
// 0x00000654 System.Single MaterialUI.MaterialDropdown::CalculateMaxItemWidth()
extern void MaterialDropdown_CalculateMaxItemWidth_m0802186074516CCE3E563DF79822034C6DD7DC5A ();
// 0x00000655 System.Void MaterialUI.MaterialDropdown::.ctor()
extern void MaterialDropdown__ctor_m0CD30CEC255789A166F8C52BF8E8B6A0D8F49291 ();
// 0x00000656 System.Void MaterialUI.MaterialDropdown::<Show>b__119_0(System.Single)
extern void MaterialDropdown_U3CShowU3Eb__119_0_m34BFF04B0E6803966B9A114ED6E4C9C02C1BBD27 ();
// 0x00000657 System.Void MaterialUI.MaterialDropdown::<Show>b__119_1(System.Single)
extern void MaterialDropdown_U3CShowU3Eb__119_1_mA4BADFD9104BFD3AF69B8EE2C603B6CDBF054D25 ();
// 0x00000658 System.Void MaterialUI.MaterialDropdown::<Show>b__119_2(UnityEngine.Vector2)
extern void MaterialDropdown_U3CShowU3Eb__119_2_m49CF4D091F5A80AAFA2427B3F0EF4AF249FF778F ();
// 0x00000659 System.Void MaterialUI.MaterialDropdown::<Show>b__119_3()
extern void MaterialDropdown_U3CShowU3Eb__119_3_m4E7C1046285B95AC996379BD8E9FD15CA997591C ();
// 0x0000065A System.Void MaterialUI.MaterialDropdown::<Hide>b__121_0(System.Single)
extern void MaterialDropdown_U3CHideU3Eb__121_0_mAFF538678E30C3BB864743002389A4FB2060A002 ();
// 0x0000065B System.Void MaterialUI.MaterialDropdown::<Hide>b__121_1(System.Single)
extern void MaterialDropdown_U3CHideU3Eb__121_1_m022FE1498C1FAD14AFB01A5A99AA61DF46F12374 ();
// 0x0000065C System.Void MaterialUI.MaterialDropdown::<Hide>b__121_2()
extern void MaterialDropdown_U3CHideU3Eb__121_2_m1461A49AA9E379B2518368EF3053DB38BCA8592E ();
// 0x0000065D System.String MaterialUI.MaterialInputField::get_hintText()
extern void MaterialInputField_get_hintText_mD7D7EA1ACFFF7D966A22A16A0B38BB248FCBA9B5 ();
// 0x0000065E System.Void MaterialUI.MaterialInputField::set_hintText(System.String)
extern void MaterialInputField_set_hintText_m4527530061D3F7FE91311AE1B1BD5EAF60A2DA19 ();
// 0x0000065F System.Boolean MaterialUI.MaterialInputField::get_floatingHint()
extern void MaterialInputField_get_floatingHint_m2400EDB7EB5BE8214751C6B039AFC12972F709F9 ();
// 0x00000660 System.Void MaterialUI.MaterialInputField::set_floatingHint(System.Boolean)
extern void MaterialInputField_set_floatingHint_m1AA672759F860F1BBD0DF68D4FBB9D3014A5C83C ();
// 0x00000661 System.Boolean MaterialUI.MaterialInputField::get_hasValidation()
extern void MaterialInputField_get_hasValidation_m64A5E7609A4D4C5625F5A660737A8361E9205EDE ();
// 0x00000662 System.Void MaterialUI.MaterialInputField::set_hasValidation(System.Boolean)
extern void MaterialInputField_set_hasValidation_mC65E0F9BA60C5E8AF83FF8E786F5690CBF1E4D45 ();
// 0x00000663 System.Boolean MaterialUI.MaterialInputField::get_validateOnStart()
extern void MaterialInputField_get_validateOnStart_m59B6D71C2D6EB1772048480DE286B33E6537AE35 ();
// 0x00000664 System.Void MaterialUI.MaterialInputField::set_validateOnStart(System.Boolean)
extern void MaterialInputField_set_validateOnStart_m435EFCA782A90FA60E70EDCE4398A49FDF7AD813 ();
// 0x00000665 System.Boolean MaterialUI.MaterialInputField::get_hasCharacterCounter()
extern void MaterialInputField_get_hasCharacterCounter_m9CF4AC8D40047B6CAA19AFD75DBFDB0037789C10 ();
// 0x00000666 System.Void MaterialUI.MaterialInputField::set_hasCharacterCounter(System.Boolean)
extern void MaterialInputField_set_hasCharacterCounter_m3EBF832ABB5FB113648A5D9C0314997A8B80DBDB ();
// 0x00000667 System.Boolean MaterialUI.MaterialInputField::get_matchInputFieldCharacterLimit()
extern void MaterialInputField_get_matchInputFieldCharacterLimit_mE2141B2382186FE0511F3DA43FA616685D60E9F3 ();
// 0x00000668 System.Void MaterialUI.MaterialInputField::set_matchInputFieldCharacterLimit(System.Boolean)
extern void MaterialInputField_set_matchInputFieldCharacterLimit_m77187F6CF09612FD5BF86C7B5A19A182E73BF5DD ();
// 0x00000669 System.Int32 MaterialUI.MaterialInputField::get_characterLimit()
extern void MaterialInputField_get_characterLimit_m219CCF8417F404E2E5B7C6738ED6A6B20191BF86 ();
// 0x0000066A System.Void MaterialUI.MaterialInputField::set_characterLimit(System.Int32)
extern void MaterialInputField_set_characterLimit_mA409CA83A88C366C5907EDA8EEE350492F73EDA6 ();
// 0x0000066B System.Int32 MaterialUI.MaterialInputField::get_floatingHintFontSize()
extern void MaterialInputField_get_floatingHintFontSize_m8FD079509604526853A5716C2824E31A58EDBA59 ();
// 0x0000066C System.Void MaterialUI.MaterialInputField::set_floatingHintFontSize(System.Int32)
extern void MaterialInputField_set_floatingHintFontSize_m86EE03AD42165FDAEDEDE8907052FAC8BE406D43 ();
// 0x0000066D System.Boolean MaterialUI.MaterialInputField::get_fitHeightToContent()
extern void MaterialInputField_get_fitHeightToContent_m9A0A28A90325B3DFFD925D374854D5ADB07971A4 ();
// 0x0000066E System.Void MaterialUI.MaterialInputField::set_fitHeightToContent(System.Boolean)
extern void MaterialInputField_set_fitHeightToContent_mA6DCC74BE3FE7FCBECCEA7588961B2A9FC5D3000 ();
// 0x0000066F System.Boolean MaterialUI.MaterialInputField::get_manualPreferredWidth()
extern void MaterialInputField_get_manualPreferredWidth_m883F176AA63ED228ACCB71C6D503690BDE86036C ();
// 0x00000670 System.Void MaterialUI.MaterialInputField::set_manualPreferredWidth(System.Boolean)
extern void MaterialInputField_set_manualPreferredWidth_mCB37D14C33CC9ECC95FBECCF71F03FAF22058A94 ();
// 0x00000671 System.Boolean MaterialUI.MaterialInputField::get_manualPreferredHeight()
extern void MaterialInputField_get_manualPreferredHeight_m66A253FF070E44BAB68835E729DD3335BB88E5D4 ();
// 0x00000672 System.Void MaterialUI.MaterialInputField::set_manualPreferredHeight(System.Boolean)
extern void MaterialInputField_set_manualPreferredHeight_m9CBDED4368FDEA0AE8D17BA797D2F439FB49706F ();
// 0x00000673 UnityEngine.Vector2 MaterialUI.MaterialInputField::get_manualSize()
extern void MaterialInputField_get_manualSize_mE667A10DD749615BEF468B9E6F9005BD001F4698 ();
// 0x00000674 System.Void MaterialUI.MaterialInputField::set_manualSize(UnityEngine.Vector2)
extern void MaterialInputField_set_manualSize_m807077C824A0DA967305C36EEB8BD1F6E1D4438B ();
// 0x00000675 UnityEngine.GameObject MaterialUI.MaterialInputField::get_textValidator()
extern void MaterialInputField_get_textValidator_m6BD3E4471F17A26FFE75113F16B814C9DDD0B42D ();
// 0x00000676 System.Void MaterialUI.MaterialInputField::set_textValidator(UnityEngine.GameObject)
extern void MaterialInputField_set_textValidator_m8B1B6255C664A50A8A96184B8624EDFEB93460FE ();
// 0x00000677 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_rectTransform()
extern void MaterialInputField_get_rectTransform_mFF2770EB5DCA5CAF84578B2C163CB36E6F43AA64 ();
// 0x00000678 UnityEngine.UI.InputField MaterialUI.MaterialInputField::get_inputField()
extern void MaterialInputField_get_inputField_m52E7ABF11424893314DB7769D8120DA0B3E3585A ();
// 0x00000679 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_inputTextTransform()
extern void MaterialInputField_get_inputTextTransform_mA7BCF3F1D9881F6238E2C0924CDBF1BEAF90DB95 ();
// 0x0000067A UnityEngine.RectTransform MaterialUI.MaterialInputField::get_hintTextTransform()
extern void MaterialInputField_get_hintTextTransform_mD4D74FD71A309E32B66B83809EF49EFCE7734959 ();
// 0x0000067B System.Void MaterialUI.MaterialInputField::set_hintTextTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_hintTextTransform_m03809CAAA1B2D552E335E06917341674433DA4CF ();
// 0x0000067C UnityEngine.RectTransform MaterialUI.MaterialInputField::get_counterTextTransform()
extern void MaterialInputField_get_counterTextTransform_m40682FFA11C6A6A9C8BD1A0BB0D3FF54EADB4D8C ();
// 0x0000067D System.Void MaterialUI.MaterialInputField::set_counterTextTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_counterTextTransform_m338945E5F755AA6AD678B116319631C0DC6A141C ();
// 0x0000067E UnityEngine.RectTransform MaterialUI.MaterialInputField::get_validationTextTransform()
extern void MaterialInputField_get_validationTextTransform_mAB056E230EC57A500D0A942754FBB4636E329C9E ();
// 0x0000067F System.Void MaterialUI.MaterialInputField::set_validationTextTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_validationTextTransform_m729E72343066CB655D03FD2169FAB406B6E6F1F7 ();
// 0x00000680 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_lineTransform()
extern void MaterialInputField_get_lineTransform_mF9B15F82F6FF4C005D3A1CEBEC4D8B6FC87B4ACF ();
// 0x00000681 System.Void MaterialUI.MaterialInputField::set_lineTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_lineTransform_mD3FC092C1AF26B63DB5469793BAA93773B7F417C ();
// 0x00000682 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_activeLineTransform()
extern void MaterialInputField_get_activeLineTransform_m88B0AA748398C698808BD9D5593143A4270AE063 ();
// 0x00000683 System.Void MaterialUI.MaterialInputField::set_activeLineTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_activeLineTransform_m0C1C751474D1C7C69087D09F5E2F3A55681BD77D ();
// 0x00000684 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_leftContentTransform()
extern void MaterialInputField_get_leftContentTransform_m569F184F03F307BA7B6953988369A1A2CE7157C6 ();
// 0x00000685 System.Void MaterialUI.MaterialInputField::set_leftContentTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_leftContentTransform_m7ED8E259C09AC5956624EAF5E363D924B9B89FF7 ();
// 0x00000686 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_rightContentTransform()
extern void MaterialInputField_get_rightContentTransform_m4866979CC010D5B3EB0A87DC5E953C84C6D9035F ();
// 0x00000687 System.Void MaterialUI.MaterialInputField::set_rightContentTransform(UnityEngine.RectTransform)
extern void MaterialInputField_set_rightContentTransform_mF28CB0BA33A227F2799148E8E304236726B7E3EC ();
// 0x00000688 UnityEngine.UI.Text MaterialUI.MaterialInputField::get_inputText()
extern void MaterialInputField_get_inputText_m3E760959D1931D5414BEF07DB87EA0613D4AB2B0 ();
// 0x00000689 UnityEngine.UI.Text MaterialUI.MaterialInputField::get_hintTextObject()
extern void MaterialInputField_get_hintTextObject_m932E3E61375913A0BEBAEE793CB5CA19AC2BD194 ();
// 0x0000068A UnityEngine.UI.Text MaterialUI.MaterialInputField::get_counterText()
extern void MaterialInputField_get_counterText_mB577F8956F35F0F954A2DEBC90AAADADF7CFA536 ();
// 0x0000068B UnityEngine.UI.Text MaterialUI.MaterialInputField::get_validationText()
extern void MaterialInputField_get_validationText_m5AD7D30B37D09D203A25CA4338061B70C255ED51 ();
// 0x0000068C UnityEngine.UI.Image MaterialUI.MaterialInputField::get_lineImage()
extern void MaterialInputField_get_lineImage_m996EF593260F7ADA286DE3AE34A9BF3F8936C27A ();
// 0x0000068D UnityEngine.CanvasGroup MaterialUI.MaterialInputField::get_activeLineCanvasGroup()
extern void MaterialInputField_get_activeLineCanvasGroup_m74A7CB1450BA3ED7F074CE96B7CE0C98C0C1070B ();
// 0x0000068E UnityEngine.CanvasGroup MaterialUI.MaterialInputField::get_hintTextCanvasGroup()
extern void MaterialInputField_get_hintTextCanvasGroup_m8C258450B3F6525739A9E6699DD46CB458F882AC ();
// 0x0000068F UnityEngine.CanvasGroup MaterialUI.MaterialInputField::get_validationCanvasGroup()
extern void MaterialInputField_get_validationCanvasGroup_m3870EEB2ACFEFFE3E1D5E7E33A58C930DE27F7B7 ();
// 0x00000690 UnityEngine.RectTransform MaterialUI.MaterialInputField::get_caretTransform()
extern void MaterialInputField_get_caretTransform_mD7487ADF05E103710ADAA0E20BEE705EAEE6BB21 ();
// 0x00000691 UnityEngine.Color MaterialUI.MaterialInputField::get_leftContentActiveColor()
extern void MaterialInputField_get_leftContentActiveColor_m12174114C8484803F45165CFDD89B506338DADBF ();
// 0x00000692 System.Void MaterialUI.MaterialInputField::set_leftContentActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_leftContentActiveColor_mABC5E1EDA41B739972FC33A6099BF6B507AF4975 ();
// 0x00000693 UnityEngine.Color MaterialUI.MaterialInputField::get_leftContentInactiveColor()
extern void MaterialInputField_get_leftContentInactiveColor_mFCBFCAD4199D931EEAB4340F82BAF7D0E6F0E421 ();
// 0x00000694 System.Void MaterialUI.MaterialInputField::set_leftContentInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_leftContentInactiveColor_mA5B21FE31F2C96F37078BB5A42E6DC7F33AD7BF3 ();
// 0x00000695 UnityEngine.Color MaterialUI.MaterialInputField::get_rightContentActiveColor()
extern void MaterialInputField_get_rightContentActiveColor_mCA71FF17A3A05B347A4BEC68BBCFEF1EFD36D5F9 ();
// 0x00000696 System.Void MaterialUI.MaterialInputField::set_rightContentActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_rightContentActiveColor_mB878310FB5AD5B3096EE68335571319C72422D8B ();
// 0x00000697 UnityEngine.Color MaterialUI.MaterialInputField::get_rightContentInactiveColor()
extern void MaterialInputField_get_rightContentInactiveColor_m3D515021C8AA4A23309B98BB83865928C3597E60 ();
// 0x00000698 System.Void MaterialUI.MaterialInputField::set_rightContentInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_rightContentInactiveColor_m42D29F849A612074EFC2C5A1CFEF85154264CD50 ();
// 0x00000699 UnityEngine.Color MaterialUI.MaterialInputField::get_hintTextActiveColor()
extern void MaterialInputField_get_hintTextActiveColor_m5E847DDBD2038C524C1FB2D2FB71F91980E7C8F1 ();
// 0x0000069A System.Void MaterialUI.MaterialInputField::set_hintTextActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_hintTextActiveColor_mDD99F51BA4B92A64AD8E180A4E5F8E5FC1FC0CCF ();
// 0x0000069B UnityEngine.Color MaterialUI.MaterialInputField::get_hintTextInactiveColor()
extern void MaterialInputField_get_hintTextInactiveColor_mFF55B15989313996A456D562DDA6765028FE4033 ();
// 0x0000069C System.Void MaterialUI.MaterialInputField::set_hintTextInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_hintTextInactiveColor_m313A417C675B8F259E09E69D23B9AB3D6946B13C ();
// 0x0000069D UnityEngine.Color MaterialUI.MaterialInputField::get_lineActiveColor()
extern void MaterialInputField_get_lineActiveColor_m9CDC890E838427D77882BFC499CAC6D245A91C42 ();
// 0x0000069E System.Void MaterialUI.MaterialInputField::set_lineActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_lineActiveColor_m0D113FB1A9DC97E17B20BDB0386D977D6D2C9A95 ();
// 0x0000069F UnityEngine.Color MaterialUI.MaterialInputField::get_lineInactiveColor()
extern void MaterialInputField_get_lineInactiveColor_mA4FBB56B6C656648D9625A6B2E8F78467033628F ();
// 0x000006A0 System.Void MaterialUI.MaterialInputField::set_lineInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_lineInactiveColor_mF4FF28BEBE39DA468B3B2D63A3EDC2AC20C91BC3 ();
// 0x000006A1 UnityEngine.Color MaterialUI.MaterialInputField::get_validationActiveColor()
extern void MaterialInputField_get_validationActiveColor_mB474DB7EBCB30340EB1BF67AA60278ADE800240C ();
// 0x000006A2 System.Void MaterialUI.MaterialInputField::set_validationActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_validationActiveColor_mE4294F3BF61C769E1E777E9FE2E241D77E27FB20 ();
// 0x000006A3 UnityEngine.Color MaterialUI.MaterialInputField::get_validationInactiveColor()
extern void MaterialInputField_get_validationInactiveColor_m3269957D63201E7F9CF1A6E194DE7DF129E9D504 ();
// 0x000006A4 System.Void MaterialUI.MaterialInputField::set_validationInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_validationInactiveColor_m49D94CCC7174675903944094A87B52D3F07876E9 ();
// 0x000006A5 UnityEngine.Color MaterialUI.MaterialInputField::get_counterActiveColor()
extern void MaterialInputField_get_counterActiveColor_m44CA05C05A1E897EF99A68F4DAB446B3A3CA1829 ();
// 0x000006A6 System.Void MaterialUI.MaterialInputField::set_counterActiveColor(UnityEngine.Color)
extern void MaterialInputField_set_counterActiveColor_m5373B7EAF13C41AEFD9EBDF1412CE041663CF370 ();
// 0x000006A7 UnityEngine.Color MaterialUI.MaterialInputField::get_counterInactiveColor()
extern void MaterialInputField_get_counterInactiveColor_mB02EA59BBE066C2ADC7E3B5CFF0DF3CFF7FF3D4E ();
// 0x000006A8 System.Void MaterialUI.MaterialInputField::set_counterInactiveColor(UnityEngine.Color)
extern void MaterialInputField_set_counterInactiveColor_mD4FEB4D55843F2A784B761D8021E52EAA0C49E7F ();
// 0x000006A9 UnityEngine.UI.Graphic MaterialUI.MaterialInputField::get_leftContentGraphic()
extern void MaterialInputField_get_leftContentGraphic_mBB46170B0F6694BE61A9E649BB4003171414AB3E ();
// 0x000006AA System.Void MaterialUI.MaterialInputField::set_leftContentGraphic(UnityEngine.UI.Graphic)
extern void MaterialInputField_set_leftContentGraphic_mA266D66CF87190635D9D82EBF701C7FAAD4601D0 ();
// 0x000006AB UnityEngine.UI.Graphic MaterialUI.MaterialInputField::get_rightContentGraphic()
extern void MaterialInputField_get_rightContentGraphic_m66318058F3C2927CC24D8251B718F2008FE100EF ();
// 0x000006AC System.Void MaterialUI.MaterialInputField::set_rightContentGraphic(UnityEngine.UI.Graphic)
extern void MaterialInputField_set_rightContentGraphic_m5966630D98A996085EF2DD4627757231AA0C7AD7 ();
// 0x000006AD System.Single MaterialUI.MaterialInputField::get_hintTextFloatingValue()
extern void MaterialInputField_get_hintTextFloatingValue_mFE5DCEF0713A8FAC3DE3648809EC078D7CECE744 ();
// 0x000006AE System.Void MaterialUI.MaterialInputField::set_hintTextFloatingValue(System.Single)
extern void MaterialInputField_set_hintTextFloatingValue_m8130177AE53019D637EAAD938331820702745D2A ();
// 0x000006AF System.Boolean MaterialUI.MaterialInputField::get_interactable()
extern void MaterialInputField_get_interactable_mF5B3A86883E6CF791F19402ED5610C8A9AC7650F ();
// 0x000006B0 System.Void MaterialUI.MaterialInputField::set_interactable(System.Boolean)
extern void MaterialInputField_set_interactable_m3C79F100EE85123B70289E7A806AA04F9C89A042 ();
// 0x000006B1 UnityEngine.CanvasGroup MaterialUI.MaterialInputField::get_canvasGroup()
extern void MaterialInputField_get_canvasGroup_mAB126D7F0F1DD4993E21BADC45B9A90D4FA15704 ();
// 0x000006B2 UnityEngine.Sprite MaterialUI.MaterialInputField::get_lineDisabledSprite()
extern void MaterialInputField_get_lineDisabledSprite_m7D581041627C9C50AC4A3EDD567D45A90E81D8CA ();
// 0x000006B3 MaterialUI.ITextValidator MaterialUI.MaterialInputField::get_customTextValidator()
extern void MaterialInputField_get_customTextValidator_m1A916B82045C0F1368C9EA776B6A03B1E85250A1 ();
// 0x000006B4 System.Void MaterialUI.MaterialInputField::set_customTextValidator(MaterialUI.ITextValidator)
extern void MaterialInputField_set_customTextValidator_m7A7F87F6D0DF84C266E39C1E77FB8B79F658C727 ();
// 0x000006B5 System.Void MaterialUI.MaterialInputField::OnEnable()
extern void MaterialInputField_OnEnable_m4E84D2D65F1D5D79A03C2FA969343D1AFB24196F ();
// 0x000006B6 System.Void MaterialUI.MaterialInputField::OnDisable()
extern void MaterialInputField_OnDisable_m476483B7E8C551A92BA357332DC442410381F768 ();
// 0x000006B7 System.Void MaterialUI.MaterialInputField::OnRectTransformDimensionsChange()
extern void MaterialInputField_OnRectTransformDimensionsChange_m35E975BD56F8AC956B9CB8714B588FCB9F3457DC ();
// 0x000006B8 System.Void MaterialUI.MaterialInputField::OnDidApplyAnimationProperties()
extern void MaterialInputField_OnDidApplyAnimationProperties_m89AB275957EA1EC57BD7D6EBED305CAA3C629EB6 ();
// 0x000006B9 System.Void MaterialUI.MaterialInputField::Start()
extern void MaterialInputField_Start_m4D85F5022350CCE1362C7CA02F07BFB5280FD125 ();
// 0x000006BA System.Void MaterialUI.MaterialInputField::Update()
extern void MaterialInputField_Update_mAF79BA6B4BC77CF851F99186AC38EA3CECEE4E82 ();
// 0x000006BB System.Void MaterialUI.MaterialInputField::ClearText()
extern void MaterialInputField_ClearText_m480F9813599E7C0691C737C473EADAAABD16B2D6 ();
// 0x000006BC System.Void MaterialUI.MaterialInputField::OnTextChanged()
extern void MaterialInputField_OnTextChanged_m64280BF78883F9E54E67FDA828556E50B3CD27A9 ();
// 0x000006BD System.Void MaterialUI.MaterialInputField::CheckHintText()
extern void MaterialInputField_CheckHintText_m9E0E6305F4C7654B2F4B14A342128FD02FB6E709 ();
// 0x000006BE System.Void MaterialUI.MaterialInputField::ValidateText()
extern void MaterialInputField_ValidateText_m56C91C9BEA5ADCF2D4B6FBEF4559390CF03C65AB ();
// 0x000006BF System.Void MaterialUI.MaterialInputField::UpdateCounter()
extern void MaterialInputField_UpdateCounter_m01821AC75EDB5F0303C498726CBA3E9FEF52B1E9 ();
// 0x000006C0 System.Void MaterialUI.MaterialInputField::UpdateSelectionState()
extern void MaterialInputField_UpdateSelectionState_mA4CE8CBD3272BD732DC4A9DB570B339E1873F01A ();
// 0x000006C1 System.Boolean MaterialUI.MaterialInputField::IsSelected()
extern void MaterialInputField_IsSelected_mA13A07690790084C5168DFDA9B861E7F25CEB523 ();
// 0x000006C2 System.Single MaterialUI.MaterialInputField::GetTextHeight()
extern void MaterialInputField_GetTextHeight_m98D7FB6D0C4F58001C32FCD0234CA5431D3BBED1 ();
// 0x000006C3 System.Single MaterialUI.MaterialInputField::GetSmallHintTextHeight()
extern void MaterialInputField_GetSmallHintTextHeight_mAC2456CFA6EBE44BFB8287AAC42A1EFE01DD5D0B ();
// 0x000006C4 System.Void MaterialUI.MaterialInputField::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialInputField_OnSelect_mE476E728A6BAEB2BA7D356B3B2EC6FB7E2622D72 ();
// 0x000006C5 System.Void MaterialUI.MaterialInputField::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialInputField_OnDeselect_m122855A0633D3FAB15ACEBEF102024AED834E265 ();
// 0x000006C6 System.Void MaterialUI.MaterialInputField::AnimateHintTextSelect()
extern void MaterialInputField_AnimateHintTextSelect_mAC999772BFACB376B9C57F6E22F82DD7B4A26CBB ();
// 0x000006C7 System.Void MaterialUI.MaterialInputField::AnimateHintTextDeselect()
extern void MaterialInputField_AnimateHintTextDeselect_mB5D3E76A94408D4D9D437D8C1BEA528A1F612680 ();
// 0x000006C8 System.Void MaterialUI.MaterialInputField::AnimateActiveLineSelect(System.Boolean)
extern void MaterialInputField_AnimateActiveLineSelect_m7E6457C1305CB92C44FE1D6D97F7525CC20396C7 ();
// 0x000006C9 System.Void MaterialUI.MaterialInputField::AnimateActiveLineDeselect(System.Boolean)
extern void MaterialInputField_AnimateActiveLineDeselect_mF19B713EF197D7E0FF17C3AFE65705288076776B ();
// 0x000006CA System.Void MaterialUI.MaterialInputField::CancelHintTextTweeners()
extern void MaterialInputField_CancelHintTextTweeners_mCC1D9083D89D7F4170E30404152AEB81046BB3FC ();
// 0x000006CB System.Void MaterialUI.MaterialInputField::CancelActivelineTweeners()
extern void MaterialInputField_CancelActivelineTweeners_m8FBAFC6490376387239E5994BC18B89058D7F818 ();
// 0x000006CC System.Void MaterialUI.MaterialInputField::SetHintLayoutToFloatingValue()
extern void MaterialInputField_SetHintLayoutToFloatingValue_m2F1E1E92C0D626881FFD113E52AF6DDB3712EDB5 ();
// 0x000006CD System.Void MaterialUI.MaterialInputField::SetLayoutDirty()
extern void MaterialInputField_SetLayoutDirty_mFC3C4A7E8E6369051CFDB698B00F5D3EC0EB6018 ();
// 0x000006CE System.Void MaterialUI.MaterialInputField::SetLayoutHorizontal()
extern void MaterialInputField_SetLayoutHorizontal_m92389656A1A4F6E79D22613A49B9588C386ADC81 ();
// 0x000006CF System.Void MaterialUI.MaterialInputField::SetLayoutVertical()
extern void MaterialInputField_SetLayoutVertical_mEEF045AB6724DD8753FBF85B008B7524E0D439D5 ();
// 0x000006D0 System.Void MaterialUI.MaterialInputField::CalculateLayoutInputHorizontal()
extern void MaterialInputField_CalculateLayoutInputHorizontal_m16BE33FDB662E595EC21BD2CF24A3BB153D417AC ();
// 0x000006D1 System.Void MaterialUI.MaterialInputField::CalculateLayoutInputVertical()
extern void MaterialInputField_CalculateLayoutInputVertical_m4D18350A23BC4BB6F47FB81028CFA19DBA9A4224 ();
// 0x000006D2 System.Single MaterialUI.MaterialInputField::get_minWidth()
extern void MaterialInputField_get_minWidth_m336FE02CC4AC457F46D97AAAAE5C2A7B10DFE1A6 ();
// 0x000006D3 System.Single MaterialUI.MaterialInputField::get_preferredWidth()
extern void MaterialInputField_get_preferredWidth_m422D63AE6153BE882EF3072C115A723EDC7A41F7 ();
// 0x000006D4 System.Single MaterialUI.MaterialInputField::get_flexibleWidth()
extern void MaterialInputField_get_flexibleWidth_mCE6967E565E76215361BCE8FD859FE6D83913313 ();
// 0x000006D5 System.Single MaterialUI.MaterialInputField::get_minHeight()
extern void MaterialInputField_get_minHeight_m926D908CCFA4D463CF22014687839C985BA2C629 ();
// 0x000006D6 System.Single MaterialUI.MaterialInputField::get_preferredHeight()
extern void MaterialInputField_get_preferredHeight_mF7E5052787083CEFA0013645B5A84F5BC55EC1C6 ();
// 0x000006D7 System.Single MaterialUI.MaterialInputField::get_flexibleHeight()
extern void MaterialInputField_get_flexibleHeight_mF939B7BC79A2EB8F93783BD81F3D46DD6938F77C ();
// 0x000006D8 System.Int32 MaterialUI.MaterialInputField::get_layoutPriority()
extern void MaterialInputField_get_layoutPriority_mB0E0895AFA04E20F5317F3DAC040A3960A678A78 ();
// 0x000006D9 System.Void MaterialUI.MaterialInputField::.ctor()
extern void MaterialInputField__ctor_mB5BECE341CD3AE2195E4A24DE429638B8868B6DD ();
// 0x000006DA System.Void MaterialUI.MaterialInputField::<ValidateText>b__227_0(System.Single)
extern void MaterialInputField_U3CValidateTextU3Eb__227_0_mABD94A5F92EB90136E7F076D81A748F54B2757EF ();
// 0x000006DB System.Void MaterialUI.MaterialInputField::<ValidateText>b__227_1(System.Single)
extern void MaterialInputField_U3CValidateTextU3Eb__227_1_m1984B492C9EBB8E981B7F0940D8AAD098CF0CD50 ();
// 0x000006DC System.Void MaterialUI.MaterialInputField::<ValidateText>b__227_2()
extern void MaterialInputField_U3CValidateTextU3Eb__227_2_mB5D70A5446B70B15E959617967F5866C11C1967B ();
// 0x000006DD System.Void MaterialUI.MaterialInputField::<UpdateSelectionState>b__229_0(UnityEngine.Color)
extern void MaterialInputField_U3CUpdateSelectionStateU3Eb__229_0_m80FC1AE38219B4BBEF61DD4A4DEAE3507DEEBD14 ();
// 0x000006DE System.Void MaterialUI.MaterialInputField::<UpdateSelectionState>b__229_1(UnityEngine.Color)
extern void MaterialInputField_U3CUpdateSelectionStateU3Eb__229_1_mA50F5DB22AC1DDED9C8497C1AD74BE7D978D1ED8 ();
// 0x000006DF System.Void MaterialUI.MaterialInputField::<UpdateSelectionState>b__229_2(UnityEngine.Color)
extern void MaterialInputField_U3CUpdateSelectionStateU3Eb__229_2_m52BC0ABCC87E6DDD071A1BFD34997BE4656872C0 ();
// 0x000006E0 System.Void MaterialUI.MaterialInputField::<UpdateSelectionState>b__229_3(UnityEngine.Color)
extern void MaterialInputField_U3CUpdateSelectionStateU3Eb__229_3_mFDAB06986DA51EB4831279F61E910F887865C518 ();
// 0x000006E1 System.Void MaterialUI.MaterialInputField::<UpdateSelectionState>b__229_4(UnityEngine.Color)
extern void MaterialInputField_U3CUpdateSelectionStateU3Eb__229_4_m5FE28BABF1DA48DCA47FF40C33A995FE511CB3AE ();
// 0x000006E2 System.Void MaterialUI.MaterialInputField::<AnimateHintTextSelect>b__235_0(System.Single)
extern void MaterialInputField_U3CAnimateHintTextSelectU3Eb__235_0_mBEC514013FE557EA08452200748D54433EE3187B ();
// 0x000006E3 System.Void MaterialUI.MaterialInputField::<AnimateHintTextSelect>b__235_1()
extern void MaterialInputField_U3CAnimateHintTextSelectU3Eb__235_1_m6C61624A06974A7299F23F071729E0865AE9E5F7 ();
// 0x000006E4 System.Void MaterialUI.MaterialInputField::<AnimateHintTextDeselect>b__236_0(System.Single)
extern void MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_0_mA091D32D25668370201D117F8644B6BEA692BF8E ();
// 0x000006E5 System.Single MaterialUI.MaterialInputField::<AnimateHintTextDeselect>b__236_1()
extern void MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_1_m8D2795BB2769E8C3D04315B17A0977FEB6D52E3F ();
// 0x000006E6 System.Void MaterialUI.MaterialInputField::<AnimateHintTextDeselect>b__236_2()
extern void MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_2_m3D312FA172553A173D527E88C5027ABC2C24B3C2 ();
// 0x000006E7 System.Void MaterialUI.MaterialInputField::<AnimateActiveLineSelect>b__237_0(System.Single)
extern void MaterialInputField_U3CAnimateActiveLineSelectU3Eb__237_0_mB2F174A25CA6A91986A1AE3AAE7B0F11B8D6B67E ();
// 0x000006E8 System.Void MaterialUI.MaterialInputField::<AnimateActiveLineSelect>b__237_1(System.Single)
extern void MaterialInputField_U3CAnimateActiveLineSelectU3Eb__237_1_mD9E44D515BB02CEC06E886CD7AF68121B85E9AFF ();
// 0x000006E9 System.Void MaterialUI.MaterialInputField::<AnimateActiveLineDeselect>b__238_0(System.Single)
extern void MaterialInputField_U3CAnimateActiveLineDeselectU3Eb__238_0_m881F0E4AFEFDA146F89551D0A4791CFC9F9DF21E ();
// 0x000006EA UnityEngine.UI.Image MaterialUI.MaterialNavDrawer::get_backgroundImage()
extern void MaterialNavDrawer_get_backgroundImage_m38665550C3518A408917E915265C0D763140BB14 ();
// 0x000006EB System.Void MaterialUI.MaterialNavDrawer::set_backgroundImage(UnityEngine.UI.Image)
extern void MaterialNavDrawer_set_backgroundImage_m95C6BBF9BB5F9670A1CBA15DE1C97BE47253077C ();
// 0x000006EC UnityEngine.UI.Image MaterialUI.MaterialNavDrawer::get_shadowImage()
extern void MaterialNavDrawer_get_shadowImage_m9ECAA73E301103F77EF7ACBBC2200572B92AB764 ();
// 0x000006ED System.Void MaterialUI.MaterialNavDrawer::set_shadowImage(UnityEngine.UI.Image)
extern void MaterialNavDrawer_set_shadowImage_m2456A1CC531E35FEF2D42A8A3951CD0EE7170A97 ();
// 0x000006EE UnityEngine.GameObject MaterialUI.MaterialNavDrawer::get_panelLayer()
extern void MaterialNavDrawer_get_panelLayer_m85E1AEC378E3994933D638957EE4E74C2D352FF9 ();
// 0x000006EF System.Void MaterialUI.MaterialNavDrawer::set_panelLayer(UnityEngine.GameObject)
extern void MaterialNavDrawer_set_panelLayer_mA23CD9EE358C7FF8FB414DC81D467E4374728F27 ();
// 0x000006F0 System.Boolean MaterialUI.MaterialNavDrawer::get_darkenBackground()
extern void MaterialNavDrawer_get_darkenBackground_m2A104524718D9E53B2A9C3CB0E9CD5CA38E5E16A ();
// 0x000006F1 System.Void MaterialUI.MaterialNavDrawer::set_darkenBackground(System.Boolean)
extern void MaterialNavDrawer_set_darkenBackground_mA17F02309DBC6827018B84C036118574FFFCEB5A ();
// 0x000006F2 System.Boolean MaterialUI.MaterialNavDrawer::get_tapBackgroundToClose()
extern void MaterialNavDrawer_get_tapBackgroundToClose_m5C449A0A64456B3A92000E9A71D0D94F1EDC71C0 ();
// 0x000006F3 System.Void MaterialUI.MaterialNavDrawer::set_tapBackgroundToClose(System.Boolean)
extern void MaterialNavDrawer_set_tapBackgroundToClose_mF7E224E193E0B9F3873E3D6F20EA0AB7E82F8246 ();
// 0x000006F4 System.Boolean MaterialUI.MaterialNavDrawer::get_openOnStart()
extern void MaterialNavDrawer_get_openOnStart_mC057CCE1A07979D02FA530BA6CFCD24CE88B3D69 ();
// 0x000006F5 System.Void MaterialUI.MaterialNavDrawer::set_openOnStart(System.Boolean)
extern void MaterialNavDrawer_set_openOnStart_m8701AA5D45F8EEFD459F9A15F83E5098A12961A6 ();
// 0x000006F6 System.Single MaterialUI.MaterialNavDrawer::get_animationDuration()
extern void MaterialNavDrawer_get_animationDuration_mFD8169B1C644F2034CE556EDAF1062112B0BC2C6 ();
// 0x000006F7 System.Void MaterialUI.MaterialNavDrawer::set_animationDuration(System.Single)
extern void MaterialNavDrawer_set_animationDuration_m26F98E6A5052B0FB0505015A832022983A010F10 ();
// 0x000006F8 MaterialUI.MaterialUIScaler MaterialUI.MaterialNavDrawer::get_scaler()
extern void MaterialNavDrawer_get_scaler_mDF698382F2E757BD53FCEFF1100D3F802B8A27A0 ();
// 0x000006F9 System.Void MaterialUI.MaterialNavDrawer::Awake()
extern void MaterialNavDrawer_Awake_mE0656B97EE702A735806EB4B4930754BE8EE60D9 ();
// 0x000006FA System.Void MaterialUI.MaterialNavDrawer::Start()
extern void MaterialNavDrawer_Start_m7486A0F452839FAED7000EDB79B0C0A814106368 ();
// 0x000006FB System.Void MaterialUI.MaterialNavDrawer::BackgroundTap()
extern void MaterialNavDrawer_BackgroundTap_m499FD5EC77C2A0233C9E21274F34E2A87BA38431 ();
// 0x000006FC System.Void MaterialUI.MaterialNavDrawer::Open()
extern void MaterialNavDrawer_Open_m724056666BCBE2912BDC3C8164D6F3AC75B123E0 ();
// 0x000006FD System.Void MaterialUI.MaterialNavDrawer::Close()
extern void MaterialNavDrawer_Close_m4AA1D11297AED08DDDFACA479205BE305B569260 ();
// 0x000006FE System.Void MaterialUI.MaterialNavDrawer::RefreshBackgroundSize()
extern void MaterialNavDrawer_RefreshBackgroundSize_m3AB383A34865B3461892E20020A00D648487E1CE ();
// 0x000006FF System.Void MaterialUI.MaterialNavDrawer::Update()
extern void MaterialNavDrawer_Update_m911697701248B7CC2133E6CA51D80D6A32A11AB3 ();
// 0x00000700 System.Void MaterialUI.MaterialNavDrawer::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void MaterialNavDrawer_OnBeginDrag_m7D4BB338B615A7F07903C5098D8F6B8904501DBC ();
// 0x00000701 System.Void MaterialUI.MaterialNavDrawer::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void MaterialNavDrawer_OnDrag_m252B94D6D840E89A669C7C7CD45EC293B1F04E63 ();
// 0x00000702 System.Void MaterialUI.MaterialNavDrawer::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void MaterialNavDrawer_OnEndDrag_mA8CD2A7BCBBD0BAF2DF0169FFDB35D4009200194 ();
// 0x00000703 System.Void MaterialUI.MaterialNavDrawer::.ctor()
extern void MaterialNavDrawer__ctor_mDE03AB3B114CE7B6D8BE266867EF2E82AE8A8E14 ();
// 0x00000704 System.Boolean MaterialUI.MaterialSlider::get_hasPopup()
extern void MaterialSlider_get_hasPopup_mA7F51BECD9D8AAB3971852290FAA30CEF3BC0BAB ();
// 0x00000705 System.Void MaterialUI.MaterialSlider::set_hasPopup(System.Boolean)
extern void MaterialSlider_set_hasPopup_mC387FAD581D9F6FF2C921B8564C480A11939AA58 ();
// 0x00000706 System.Boolean MaterialUI.MaterialSlider::get_hasDots()
extern void MaterialSlider_get_hasDots_m53F42F5F6675209B8CCEC140C34078B3815E00AC ();
// 0x00000707 System.Void MaterialUI.MaterialSlider::set_hasDots(System.Boolean)
extern void MaterialSlider_set_hasDots_mC3BF4E6DEFDAF4822B484896BED309E3543D739E ();
// 0x00000708 System.Single MaterialUI.MaterialSlider::get_animationDuration()
extern void MaterialSlider_get_animationDuration_m4B8BDD8CA96B35708E6FEE253678D5D4D0B5637F ();
// 0x00000709 System.Void MaterialUI.MaterialSlider::set_animationDuration(System.Single)
extern void MaterialSlider_set_animationDuration_m12C9060170F3D40C02DF27E67493400BDFFE6279 ();
// 0x0000070A UnityEngine.Color MaterialUI.MaterialSlider::get_enabledColor()
extern void MaterialSlider_get_enabledColor_m669DBF3F7584825144DB05784322042DB79D6D2C ();
// 0x0000070B System.Void MaterialUI.MaterialSlider::set_enabledColor(UnityEngine.Color)
extern void MaterialSlider_set_enabledColor_m3C65412BCB5B099AD24EA1E3AEF4D05072A14370 ();
// 0x0000070C UnityEngine.Color MaterialUI.MaterialSlider::get_disabledColor()
extern void MaterialSlider_get_disabledColor_m6C5D604E21B52DED1902250B2382A6DE2965F634 ();
// 0x0000070D System.Void MaterialUI.MaterialSlider::set_disabledColor(UnityEngine.Color)
extern void MaterialSlider_set_disabledColor_m44235DDF301CF3CCF647C8B4726AD38A916FA400 ();
// 0x0000070E UnityEngine.Color MaterialUI.MaterialSlider::get_backgroundColor()
extern void MaterialSlider_get_backgroundColor_mB6767D2A7460A1750FD9BCD3CD3474F2D1E2420F ();
// 0x0000070F System.Void MaterialUI.MaterialSlider::set_backgroundColor(UnityEngine.Color)
extern void MaterialSlider_set_backgroundColor_m35E02D03F20260020186CED7EF2613B754D764CB ();
// 0x00000710 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_sliderHandleTransform()
extern void MaterialSlider_get_sliderHandleTransform_m92D7E0AC05FD33898D80F7CAE67E6F6940D2497D ();
// 0x00000711 System.Void MaterialUI.MaterialSlider::set_sliderHandleTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_sliderHandleTransform_mD5A4D60BBF55C848545301F4CA2FEE729667C7BA ();
// 0x00000712 UnityEngine.UI.Graphic MaterialUI.MaterialSlider::get_handleGraphic()
extern void MaterialSlider_get_handleGraphic_m16E7B2A94BD54C4348F1D596124CC8A56A3C7304 ();
// 0x00000713 System.Void MaterialUI.MaterialSlider::set_handleGraphic(UnityEngine.UI.Graphic)
extern void MaterialSlider_set_handleGraphic_m9E37FE4FCF088790F700BFE58576CFDEDCE668AD ();
// 0x00000714 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_handleGraphicTransform()
extern void MaterialSlider_get_handleGraphicTransform_m58D2AABF86B28017510735B83ABA88E61A62EBFB ();
// 0x00000715 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_popupTransform()
extern void MaterialSlider_get_popupTransform_m6AA070C4010E4D1BC98F52F58F2A401238B550C3 ();
// 0x00000716 System.Void MaterialUI.MaterialSlider::set_popupTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_popupTransform_m51A0C986A069035F111E1EF254B3589F27073968 ();
// 0x00000717 UnityEngine.UI.Text MaterialUI.MaterialSlider::get_popupText()
extern void MaterialSlider_get_popupText_mB3C7034D78797780E47E47E53AFD289007AFC27F ();
// 0x00000718 System.Void MaterialUI.MaterialSlider::set_popupText(UnityEngine.UI.Text)
extern void MaterialSlider_set_popupText_mC0E5B65BADF2EA973EE2D4F250B96F7C8933146B ();
// 0x00000719 UnityEngine.UI.Text MaterialUI.MaterialSlider::get_valueText()
extern void MaterialSlider_get_valueText_m074C4D0B6FAA867B841979F367B05C1FC38B4A98 ();
// 0x0000071A System.Void MaterialUI.MaterialSlider::set_valueText(UnityEngine.UI.Text)
extern void MaterialSlider_set_valueText_mC343C3B9073E80A0FD8DED5D2B2515B3CA70D4EF ();
// 0x0000071B MaterialUI.MaterialInputField MaterialUI.MaterialSlider::get_inputField()
extern void MaterialSlider_get_inputField_mD0F9D929D3C4284BAF8832A7A92D5003993722C7 ();
// 0x0000071C System.Void MaterialUI.MaterialSlider::set_inputField(MaterialUI.MaterialInputField)
extern void MaterialSlider_set_inputField_m0A45DBE2999439ED1443E6425CC0FC9E07A7CE19 ();
// 0x0000071D UnityEngine.RectTransform MaterialUI.MaterialSlider::get_fillTransform()
extern void MaterialSlider_get_fillTransform_m052A286BE20710BB0B1A7F8E77E267B7CB9E6613 ();
// 0x0000071E System.Void MaterialUI.MaterialSlider::set_fillTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_fillTransform_mFC1E5953A5BD4F715B6C2B16525CA446AD403203 ();
// 0x0000071F UnityEngine.UI.Graphic MaterialUI.MaterialSlider::get_backgroundGraphic()
extern void MaterialSlider_get_backgroundGraphic_m4C421C21BD1B7B91382ED15FA9B75C0343DC6F88 ();
// 0x00000720 System.Void MaterialUI.MaterialSlider::set_backgroundGraphic(UnityEngine.UI.Graphic)
extern void MaterialSlider_set_backgroundGraphic_m8C55E3A028787F949E1BF66988F15C37998F4AD5 ();
// 0x00000721 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_leftContentTransform()
extern void MaterialSlider_get_leftContentTransform_mC52E6714292AA904843F68F0DACB34779BE072AA ();
// 0x00000722 System.Void MaterialUI.MaterialSlider::set_leftContentTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_leftContentTransform_m76F338C3E030E0CA4911D7C8A6432511A8BC72DE ();
// 0x00000723 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_rightContentTransform()
extern void MaterialSlider_get_rightContentTransform_m9948E636EE00B40D630B8FCB1863E45CE6A3264A ();
// 0x00000724 System.Void MaterialUI.MaterialSlider::set_rightContentTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_rightContentTransform_m9AF62291FCA5B72D9368A91837BEF2F224FCEBD2 ();
// 0x00000725 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_sliderContentTransform()
extern void MaterialSlider_get_sliderContentTransform_mCCF21D2C3AF342648404F59B977F09CCC1F337A3 ();
// 0x00000726 System.Void MaterialUI.MaterialSlider::set_sliderContentTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_sliderContentTransform_mB0C36C4890C15BB0F358E2C654D0001BBB960A43 ();
// 0x00000727 UnityEngine.RectTransform MaterialUI.MaterialSlider::get_rectTransform()
extern void MaterialSlider_get_rectTransform_m1F38832F7D07453C3CA9C871F3D3CD9C89011739 ();
// 0x00000728 System.Void MaterialUI.MaterialSlider::set_rectTransform(UnityEngine.RectTransform)
extern void MaterialSlider_set_rectTransform_m48CB1B25666F62C49506CA15B3B39F480BB70AAB ();
// 0x00000729 MaterialUI.VectorImageData MaterialUI.MaterialSlider::get_dotTemplateIcon()
extern void MaterialSlider_get_dotTemplateIcon_mD329531FB0AC2909775E384C3CAD1A0E617F10B2 ();
// 0x0000072A System.Void MaterialUI.MaterialSlider::set_dotTemplateIcon(MaterialUI.VectorImageData)
extern void MaterialSlider_set_dotTemplateIcon_mA58DC8962A53EE142A6D1D7D771B61176CB105E0 ();
// 0x0000072B UnityEngine.RectTransform MaterialUI.MaterialSlider::get_fillAreaTransform()
extern void MaterialSlider_get_fillAreaTransform_mACAE7EB37D28FF2A5783F8221D2D4A5B1BDAD760 ();
// 0x0000072C UnityEngine.UI.Slider MaterialUI.MaterialSlider::get_slider()
extern void MaterialSlider_get_slider_m13B05D63FB530567CEBF8AE48D3A783602924C92 ();
// 0x0000072D UnityEngine.CanvasGroup MaterialUI.MaterialSlider::get_canvasGroup()
extern void MaterialSlider_get_canvasGroup_m89EE82A8F9D8646EF690C001B09003D857CD6691 ();
// 0x0000072E UnityEngine.Canvas MaterialUI.MaterialSlider::get_rootCanvas()
extern void MaterialSlider_get_rootCanvas_m4EC0492BC4BD39E470C8AC492EF5D300B1A63757 ();
// 0x0000072F System.Boolean MaterialUI.MaterialSlider::get_isSelected()
extern void MaterialSlider_get_isSelected_m65E969F28406BF8C1AD518CA0763663C78786A40 ();
// 0x00000730 System.Boolean MaterialUI.MaterialSlider::get_hasManualPreferredWidth()
extern void MaterialSlider_get_hasManualPreferredWidth_m3D339E57D87CCB606BCA7F3B1884E80F173CA972 ();
// 0x00000731 System.Void MaterialUI.MaterialSlider::set_hasManualPreferredWidth(System.Boolean)
extern void MaterialSlider_set_hasManualPreferredWidth_m348B3BD1AC8D1AA6ADC4C38C7E81CEBFEB847575 ();
// 0x00000732 System.Single MaterialUI.MaterialSlider::get_manualPreferredWidth()
extern void MaterialSlider_get_manualPreferredWidth_m9C2E7F16F2D1F6AF9D43ED6D045996AE6BE87078 ();
// 0x00000733 System.Void MaterialUI.MaterialSlider::set_manualPreferredWidth(System.Single)
extern void MaterialSlider_set_manualPreferredWidth_mCEED1678DE1CE36D027F0A2145DB33DAFBDCF912 ();
// 0x00000734 System.Boolean MaterialUI.MaterialSlider::get_interactable()
extern void MaterialSlider_get_interactable_mDCE8974AEE002C12330276DF42B47534215C84FC ();
// 0x00000735 System.Void MaterialUI.MaterialSlider::set_interactable(System.Boolean)
extern void MaterialSlider_set_interactable_mD59D3F8CCBB4B5A580B3FCBA8C4B83F69AA81566 ();
// 0x00000736 System.Boolean MaterialUI.MaterialSlider::get_lowLeftDisabledOpacity()
extern void MaterialSlider_get_lowLeftDisabledOpacity_mA3B9FC9E75201D7F52BEB9C244DDBE36BC2B298E ();
// 0x00000737 System.Void MaterialUI.MaterialSlider::set_lowLeftDisabledOpacity(System.Boolean)
extern void MaterialSlider_set_lowLeftDisabledOpacity_m0CD7602DCC41331FFC61D6597969E48137AFB527 ();
// 0x00000738 System.Boolean MaterialUI.MaterialSlider::get_lowRightDisabledOpacity()
extern void MaterialSlider_get_lowRightDisabledOpacity_m4CC599B6599AEA75B76131C9B86FF02D1AB6CEBA ();
// 0x00000739 System.Void MaterialUI.MaterialSlider::set_lowRightDisabledOpacity(System.Boolean)
extern void MaterialSlider_set_lowRightDisabledOpacity_m2EFC33592818278EC2981729F804D9888F281BD8 ();
// 0x0000073A UnityEngine.CanvasGroup MaterialUI.MaterialSlider::get_leftCanvasGroup()
extern void MaterialSlider_get_leftCanvasGroup_m7CBFB37ACDFD4E99EE58E93BF3263D29C7FF2CBE ();
// 0x0000073B UnityEngine.CanvasGroup MaterialUI.MaterialSlider::get_rightCanvasGroup()
extern void MaterialSlider_get_rightCanvasGroup_mC1E090D8ACF023DE4BED7491F667F77CB0FA62B7 ();
// 0x0000073C System.Void MaterialUI.MaterialSlider::OnEnable()
extern void MaterialSlider_OnEnable_m269031EF9EC4BB1550FE8F59838EB80E5B086454 ();
// 0x0000073D System.Void MaterialUI.MaterialSlider::OnDisable()
extern void MaterialSlider_OnDisable_mF670192FCF48D4363BFF3ECCAB34D0F409FD855E ();
// 0x0000073E System.Void MaterialUI.MaterialSlider::OnCanvasChanged(System.Boolean,System.Boolean)
extern void MaterialSlider_OnCanvasChanged_m50FC2C95D9715904977A9C5B906DCF2583A6553F ();
// 0x0000073F System.Void MaterialUI.MaterialSlider::Start()
extern void MaterialSlider_Start_m68452B2F8C2ECE88C5B9421062DB741A35991B5A ();
// 0x00000740 System.Void MaterialUI.MaterialSlider::SetTracker()
extern void MaterialSlider_SetTracker_m7587498C16DF6ADCC21C1F018A3DFCE1806D2495 ();
// 0x00000741 System.Void MaterialUI.MaterialSlider::Update()
extern void MaterialSlider_Update_mA81C6832CC704FACE48DCAE9B6EE4F45CB11E7A1 ();
// 0x00000742 System.Void MaterialUI.MaterialSlider::DestroyDots()
extern void MaterialSlider_DestroyDots_m91CA45D346E106EE6198569B3325C242B55F9344 ();
// 0x00000743 System.Void MaterialUI.MaterialSlider::RebuildDots()
extern void MaterialSlider_RebuildDots_mB5910D654C45D747085A39FA8B5930C91C96BACB ();
// 0x00000744 System.Int32 MaterialUI.MaterialSlider::SliderValueRange()
extern void MaterialSlider_SliderValueRange_m32305C3760A68BB2C4839B5C7576380C5ABE4B04 ();
// 0x00000745 UnityEngine.UI.Graphic MaterialUI.MaterialSlider::CreateDot()
extern void MaterialSlider_CreateDot_m40E54B6B05B381FD1A267676A43BD816E0B6C9EA ();
// 0x00000746 System.Void MaterialUI.MaterialSlider::AnimateOn()
extern void MaterialSlider_AnimateOn_m3AC3BD7F6049C5BDB6948235771C10C2B0FA23A0 ();
// 0x00000747 System.Void MaterialUI.MaterialSlider::AnimateOff()
extern void MaterialSlider_AnimateOff_mA8D2D5DEE7C63D6E27C24D3F299297494B22F31E ();
// 0x00000748 System.Void MaterialUI.MaterialSlider::OnInputChange(System.String)
extern void MaterialSlider_OnInputChange_m464F5F2848F61A6FE5B21A67D9172B059F892C4D ();
// 0x00000749 System.Void MaterialUI.MaterialSlider::OnInputEnd()
extern void MaterialSlider_OnInputEnd_m8D1F15F6BE335AC75A007833324D18506B9D1B9F ();
// 0x0000074A System.Void MaterialUI.MaterialSlider::OnSliderValueChanged(System.Single)
extern void MaterialSlider_OnSliderValueChanged_m792054C56964A5F5DD4892982132A401A73A96D8 ();
// 0x0000074B System.Collections.IEnumerator MaterialUI.MaterialSlider::OnSliderValueInit()
extern void MaterialSlider_OnSliderValueInit_m548D723EAC9B8EC1590DD8AF35FACD267E2A68AC ();
// 0x0000074C System.Void MaterialUI.MaterialSlider::OnBeforeValidate()
extern void MaterialSlider_OnBeforeValidate_mA3E83AE03AD80C97A47CF4384FB9D634E0D28CAE ();
// 0x0000074D System.Void MaterialUI.MaterialSlider::UpdateColors()
extern void MaterialSlider_UpdateColors_mAA4F1117D3C81B4F30AF8F34E44784F376C90311 ();
// 0x0000074E System.Void MaterialUI.MaterialSlider::OnRectTransformDimensionsChange()
extern void MaterialSlider_OnRectTransformDimensionsChange_mE1B5897FD6A770662EB1C70F1D4448BD814AC57C ();
// 0x0000074F System.Void MaterialUI.MaterialSlider::OnTransformParentChanged()
extern void MaterialSlider_OnTransformParentChanged_m0E13A3E1C3B0FACD87E3E02691EFEDACB7C8A30B ();
// 0x00000750 System.Void MaterialUI.MaterialSlider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void MaterialSlider_OnPointerDown_m1E7F694B846E1AD508C98195E39B14A3921324EC ();
// 0x00000751 System.Void MaterialUI.MaterialSlider::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void MaterialSlider_OnPointerUp_mAB94DE44B788B9422BB08E7126AD3DCCDEA37E46 ();
// 0x00000752 System.Void MaterialUI.MaterialSlider::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialSlider_OnSelect_mEB0F3EB2D09BC69B53550114EBB494EB0DF05416 ();
// 0x00000753 System.Void MaterialUI.MaterialSlider::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void MaterialSlider_OnDeselect_mB64B010FBA4241A846CC78A77C3BBCB9297F7405 ();
// 0x00000754 System.Void MaterialUI.MaterialSlider::CalculateLayoutInputHorizontal()
extern void MaterialSlider_CalculateLayoutInputHorizontal_m29866A977CB5C3AAFD64CC1DBAA79658F3A2F894 ();
// 0x00000755 System.Void MaterialUI.MaterialSlider::SetLayoutHorizontal()
extern void MaterialSlider_SetLayoutHorizontal_m8946BE08F87963953EEA06DDD8B75E208E72FEEC ();
// 0x00000756 System.Void MaterialUI.MaterialSlider::CalculateLayoutInputVertical()
extern void MaterialSlider_CalculateLayoutInputVertical_mB92BAFD80907B2D005277EB6E025D49E138AA38B ();
// 0x00000757 System.Void MaterialUI.MaterialSlider::SetLayoutVertical()
extern void MaterialSlider_SetLayoutVertical_mAE2BC6B629E77BAB61AB203FE8C969D2EA2BE762 ();
// 0x00000758 System.Single MaterialUI.MaterialSlider::get_minWidth()
extern void MaterialSlider_get_minWidth_m8D5B6AA07B070B4CC75271A6DE1BFC211E7C7750 ();
// 0x00000759 System.Single MaterialUI.MaterialSlider::get_preferredWidth()
extern void MaterialSlider_get_preferredWidth_m4E173C9287DCD28706FDAD387C645A25E02A8071 ();
// 0x0000075A System.Single MaterialUI.MaterialSlider::get_flexibleWidth()
extern void MaterialSlider_get_flexibleWidth_m440B3260E6E626F697396862CBEEDDAA80495969 ();
// 0x0000075B System.Single MaterialUI.MaterialSlider::get_minHeight()
extern void MaterialSlider_get_minHeight_m5C71D867A290910484EBDBD63C15DC23363C83D0 ();
// 0x0000075C System.Single MaterialUI.MaterialSlider::get_preferredHeight()
extern void MaterialSlider_get_preferredHeight_m35060A443CE3B65D385CAB2174C3F09E17D8216F ();
// 0x0000075D System.Single MaterialUI.MaterialSlider::get_flexibleHeight()
extern void MaterialSlider_get_flexibleHeight_m6D9D2D3B75E472A7A0A169FB17ED160523254F58 ();
// 0x0000075E System.Int32 MaterialUI.MaterialSlider::get_layoutPriority()
extern void MaterialSlider_get_layoutPriority_m1DBE4660FD38DE652E29A1CF4C343100B4E1064A ();
// 0x0000075F System.Void MaterialUI.MaterialSlider::.ctor()
extern void MaterialSlider__ctor_m05943E880A4B15B8B5B534368D57EC340B6AB764 ();
// 0x00000760 System.Void MaterialUI.MaterialSlider::<AnimateOn>b__148_0(UnityEngine.Vector2)
extern void MaterialSlider_U3CAnimateOnU3Eb__148_0_m99D07FD861D03D6F835BF41B80CB2FE0C593F1D9 ();
// 0x00000761 System.Void MaterialUI.MaterialSlider::<AnimateOn>b__148_1(System.Single)
extern void MaterialSlider_U3CAnimateOnU3Eb__148_1_m86A6967A8AA545A1718EEA6D227205B431F3FF4F ();
// 0x00000762 System.Void MaterialUI.MaterialSlider::<AnimateOn>b__148_2(UnityEngine.Vector3)
extern void MaterialSlider_U3CAnimateOnU3Eb__148_2_mE9A95CDD647BCF7EE1978D9646137B8E7836017A ();
// 0x00000763 System.Void MaterialUI.MaterialSlider::<AnimateOn>b__148_3(UnityEngine.Vector2)
extern void MaterialSlider_U3CAnimateOnU3Eb__148_3_m1E6A41BDAC8B9918450F9BFF6992BBB4961DE9FE ();
// 0x00000764 System.Void MaterialUI.MaterialSlider::<AnimateOn>b__148_4(UnityEngine.Color)
extern void MaterialSlider_U3CAnimateOnU3Eb__148_4_mCBB5763602705B5482C6BBD91DF30AB4751A751D ();
// 0x00000765 UnityEngine.Color MaterialUI.MaterialSlider::<AnimateOn>b__148_5()
extern void MaterialSlider_U3CAnimateOnU3Eb__148_5_mACF568E4E7E07B954BEFB94E870A708912EEBE0C ();
// 0x00000766 UnityEngine.Color MaterialUI.MaterialSlider::<AnimateOn>b__148_6()
extern void MaterialSlider_U3CAnimateOnU3Eb__148_6_mCBE4E34BCCE2DEFA712A8429AD8003707B62D232 ();
// 0x00000767 System.Void MaterialUI.MaterialSlider::<AnimateOff>b__149_0(System.Single)
extern void MaterialSlider_U3CAnimateOffU3Eb__149_0_mCE07447C9E435010A6CFB6C8710028D113500676 ();
// 0x00000768 System.Void MaterialUI.MaterialSlider::<AnimateOff>b__149_1(UnityEngine.Vector3)
extern void MaterialSlider_U3CAnimateOffU3Eb__149_1_m388C5B5AF6C0CE3954C2509E6079AF622BC3198C ();
// 0x00000769 System.Void MaterialUI.MaterialSlider::<AnimateOff>b__149_2(UnityEngine.Vector2)
extern void MaterialSlider_U3CAnimateOffU3Eb__149_2_m259B1DEFE757E68AEB98AE1BDEA1B59B37C4C4D6 ();
// 0x0000076A System.Void MaterialUI.MaterialSlider::<AnimateOff>b__149_3(UnityEngine.Color)
extern void MaterialSlider_U3CAnimateOffU3Eb__149_3_m40501AD9D864448BD7B730BADF739FF195AA018E ();
// 0x0000076B System.Void MaterialUI.MaterialSlider::<OnSliderValueChanged>b__152_0(System.Single)
extern void MaterialSlider_U3COnSliderValueChangedU3Eb__152_0_mA6E2CCEBB5F59C07B52B12F7A7E2C20D7E1056F4 ();
// 0x0000076C System.Void MaterialUI.MaterialSlider::<OnSliderValueChanged>b__152_1(System.Single)
extern void MaterialSlider_U3COnSliderValueChangedU3Eb__152_1_m872E05D29A049570BEE72B3503F9D37C0A7A340D ();
// 0x0000076D UnityEngine.Color MaterialUI.MaterialSwitch::get_backOnColor()
extern void MaterialSwitch_get_backOnColor_m2BE9FCEDAABCBA3934F35F315865AFE86EE0D558 ();
// 0x0000076E System.Void MaterialUI.MaterialSwitch::set_backOnColor(UnityEngine.Color)
extern void MaterialSwitch_set_backOnColor_m8774D715E17361901711A631C84C817708CCEFC6 ();
// 0x0000076F UnityEngine.Color MaterialUI.MaterialSwitch::get_backOffColor()
extern void MaterialSwitch_get_backOffColor_m8E695C0868DE2A5EADD92481AD7185094B292F51 ();
// 0x00000770 System.Void MaterialUI.MaterialSwitch::set_backOffColor(UnityEngine.Color)
extern void MaterialSwitch_set_backOffColor_m61621341F2E64C8ECE0E41B94EB6681C1CA5A1B4 ();
// 0x00000771 UnityEngine.Color MaterialUI.MaterialSwitch::get_backDisabledColor()
extern void MaterialSwitch_get_backDisabledColor_mCA2980C93CAB38C8B13980C02C3AB0AB19C1F46D ();
// 0x00000772 System.Void MaterialUI.MaterialSwitch::set_backDisabledColor(UnityEngine.Color)
extern void MaterialSwitch_set_backDisabledColor_mDF281851AE6A51E8BA506395B9C81623B5C5CED6 ();
// 0x00000773 UnityEngine.UI.Graphic MaterialUI.MaterialSwitch::get_switchImage()
extern void MaterialSwitch_get_switchImage_m093967040952CF97470078B558F398A123798A38 ();
// 0x00000774 System.Void MaterialUI.MaterialSwitch::set_switchImage(UnityEngine.UI.Graphic)
extern void MaterialSwitch_set_switchImage_m5B563DB692EDDF08C30C18D6BE9C973F7B6963EF ();
// 0x00000775 UnityEngine.UI.Graphic MaterialUI.MaterialSwitch::get_backImage()
extern void MaterialSwitch_get_backImage_mAC533D10EAF06D1BB0012019FAFB70F16A057E5E ();
// 0x00000776 System.Void MaterialUI.MaterialSwitch::set_backImage(UnityEngine.UI.Graphic)
extern void MaterialSwitch_set_backImage_mE16FFE2AAB6C059F8548C5B800AAB80A88CC7859 ();
// 0x00000777 UnityEngine.UI.Graphic MaterialUI.MaterialSwitch::get_shadowImage()
extern void MaterialSwitch_get_shadowImage_mC2538D015E2E37A637B3D9D28B7AA7CA0BB94C78 ();
// 0x00000778 System.Void MaterialUI.MaterialSwitch::set_shadowImage(UnityEngine.UI.Graphic)
extern void MaterialSwitch_set_shadowImage_m1C7A0652E1CE3B8790F5861AB901F3B125E1A989 ();
// 0x00000779 UnityEngine.RectTransform MaterialUI.MaterialSwitch::get_switchRectTransform()
extern void MaterialSwitch_get_switchRectTransform_mA06F378F695F1B87C884E44F9C4E1C78907623E9 ();
// 0x0000077A System.Void MaterialUI.MaterialSwitch::TurnOn()
extern void MaterialSwitch_TurnOn_m64AB5303944616080CE5CEADFC815EDA3A5A667E ();
// 0x0000077B System.Void MaterialUI.MaterialSwitch::TurnOnInstant()
extern void MaterialSwitch_TurnOnInstant_m8817803E289FDBB2BB175AF0341699DBC443107A ();
// 0x0000077C System.Void MaterialUI.MaterialSwitch::TurnOff()
extern void MaterialSwitch_TurnOff_m42A8D0A523DDA15EAB3DFC16F4A03960A70B1847 ();
// 0x0000077D System.Void MaterialUI.MaterialSwitch::TurnOffInstant()
extern void MaterialSwitch_TurnOffInstant_mE7CFAB4399367BA094BC7507444C026227332E69 ();
// 0x0000077E System.Void MaterialUI.MaterialSwitch::Enable()
extern void MaterialSwitch_Enable_m5C2DC314E70655FB69F004F66BCD0FC49F886BDE ();
// 0x0000077F System.Void MaterialUI.MaterialSwitch::Disable()
extern void MaterialSwitch_Disable_m09EC17F4EF419C7DBAF47C1C909A9F3BC2685EC7 ();
// 0x00000780 System.Void MaterialUI.MaterialSwitch::AnimOn()
extern void MaterialSwitch_AnimOn_m553ECBAB5311AB3DA77BC9BAAC7DDE1481B81435 ();
// 0x00000781 System.Void MaterialUI.MaterialSwitch::AnimOnComplete()
extern void MaterialSwitch_AnimOnComplete_m874BCC11366A00F8AE0ACC42BBF74C9A6ABCEACC ();
// 0x00000782 System.Void MaterialUI.MaterialSwitch::AnimOff()
extern void MaterialSwitch_AnimOff_mE181274158A5915BBBB9C038D96519C81AA6E688 ();
// 0x00000783 System.Void MaterialUI.MaterialSwitch::AnimOffComplete()
extern void MaterialSwitch_AnimOffComplete_mCFCE3E1004AC8A1FE9F8C4574B87FDBA1D6AD298 ();
// 0x00000784 System.Void MaterialUI.MaterialSwitch::.ctor()
extern void MaterialSwitch__ctor_m522D6C7FEC1A047F08A275256C185D850B13A4A6 ();
// 0x00000785 MaterialUI.MaterialDropdown MaterialUI.DropdownTrigger::get_dropdown()
extern void DropdownTrigger_get_dropdown_m0456496201237CE88EE97A8C8A4E3433AC8114F4 ();
// 0x00000786 System.Void MaterialUI.DropdownTrigger::set_dropdown(MaterialUI.MaterialDropdown)
extern void DropdownTrigger_set_dropdown_mF9FCC458902D8715FF564E0EFB3C35758248B347 ();
// 0x00000787 System.Int32 MaterialUI.DropdownTrigger::get_index()
extern void DropdownTrigger_get_index_m40CBD4E15CBF4B65399E41F202F47A6EFE3F6E8E ();
// 0x00000788 System.Void MaterialUI.DropdownTrigger::set_index(System.Int32)
extern void DropdownTrigger_set_index_mD9FA457F745982FF1767545751E3FB9CCBA9C20A ();
// 0x00000789 System.Void MaterialUI.DropdownTrigger::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void DropdownTrigger_OnPointerClick_mAF41EAEA99B52E3D612DF1694718D40AC9FA3055 ();
// 0x0000078A System.Void MaterialUI.DropdownTrigger::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void DropdownTrigger_OnSubmit_mBB1153E3F449C8A9CE11291E0958291DFDB56798 ();
// 0x0000078B System.Void MaterialUI.DropdownTrigger::.ctor()
extern void DropdownTrigger__ctor_mB70A92B5DB4459E4ADA5555D28578359B83DD90C ();
// 0x0000078C MaterialUI.ImageDataType MaterialUI.OptionDataList::get_imageType()
extern void OptionDataList_get_imageType_mBAA5E1149AA101ED05D371AC9CDF7FD5C6CE55C8 ();
// 0x0000078D System.Void MaterialUI.OptionDataList::set_imageType(MaterialUI.ImageDataType)
extern void OptionDataList_set_imageType_m95757887742DE03FF8C92BD321365FC2EB4159B8 ();
// 0x0000078E System.Collections.Generic.List`1<MaterialUI.OptionData> MaterialUI.OptionDataList::get_options()
extern void OptionDataList_get_options_m6453FDB449402EED08C92E2967343F7FE2226602 ();
// 0x0000078F System.Void MaterialUI.OptionDataList::set_options(System.Collections.Generic.List`1<MaterialUI.OptionData>)
extern void OptionDataList_set_options_m82010DEBA8EEE55C6E2402F7C63B322A81789399 ();
// 0x00000790 System.Void MaterialUI.OptionDataList::.ctor()
extern void OptionDataList__ctor_m24D431B4381ED9495C7260B07BE35B411834E9D1 ();
// 0x00000791 System.String MaterialUI.OptionData::get_text()
extern void OptionData_get_text_m33F35E0912BE59359B07438B9B3773B71623DC42 ();
// 0x00000792 System.Void MaterialUI.OptionData::set_text(System.String)
extern void OptionData_set_text_m3D2FA9356D95D8956DBA2415EBB1A9F806AA5952 ();
// 0x00000793 MaterialUI.ImageData MaterialUI.OptionData::get_imageData()
extern void OptionData_get_imageData_m127F2E0B8699F7F33DAA4E08799A4B723CBBA6EE ();
// 0x00000794 System.Void MaterialUI.OptionData::set_imageData(MaterialUI.ImageData)
extern void OptionData_set_imageData_mAFE0CD6C453D805F27772A93EA5D8400B23CDE1C ();
// 0x00000795 System.Action MaterialUI.OptionData::get_onOptionSelected()
extern void OptionData_get_onOptionSelected_m97C97275E208D17D45807499A44596188BD46BFC ();
// 0x00000796 System.Void MaterialUI.OptionData::set_onOptionSelected(System.Action)
extern void OptionData_set_onOptionSelected_m0D06F1D7F129930D456714CD500C28B35090D77D ();
// 0x00000797 System.Void MaterialUI.OptionData::.ctor()
extern void OptionData__ctor_m2CBEC14C14B913BCDE679FCCEB47A5DAA34FDD29 ();
// 0x00000798 System.Void MaterialUI.OptionData::.ctor(System.String)
extern void OptionData__ctor_m5B8450E2A63947CF4ED7F4BB64ECD5FF7E5F106A ();
// 0x00000799 System.Void MaterialUI.OptionData::.ctor(System.String,MaterialUI.ImageData,System.Action)
extern void OptionData__ctor_mB20CEA44658D2CA3F7CA1E345EE0A23BEB001474 ();
// 0x0000079A UnityEngine.Color MaterialUI.OverscrollConfig::get_overscrollColor()
extern void OverscrollConfig_get_overscrollColor_mFF29DD6ADB11A88DEC1F590FEC7FE80B83244C82 ();
// 0x0000079B System.Void MaterialUI.OverscrollConfig::set_overscrollColor(UnityEngine.Color)
extern void OverscrollConfig_set_overscrollColor_m0C0365F8555350942633684A455E279A7D0A9E89 ();
// 0x0000079C System.Single MaterialUI.OverscrollConfig::get_overscrollScale()
extern void OverscrollConfig_get_overscrollScale_m439E08F52994C260228C5C810FB9AECC9DCB52EB ();
// 0x0000079D System.Void MaterialUI.OverscrollConfig::set_overscrollScale(System.Single)
extern void OverscrollConfig_set_overscrollScale_m2EE7F4AF6F068F6BA37BC01DA354D50244E9A12F ();
// 0x0000079E System.Void MaterialUI.OverscrollConfig::Start()
extern void OverscrollConfig_Start_m236D411077F597AF014927B91814F26CC37A8D3E ();
// 0x0000079F System.Void MaterialUI.OverscrollConfig::CreateOverscroll(System.Int32)
extern void OverscrollConfig_CreateOverscroll_mBB10C80D2B287435559F3244C6B61CA8C6AAEBA9 ();
// 0x000007A0 System.Void MaterialUI.OverscrollConfig::Setup()
extern void OverscrollConfig_Setup_mA74DCD72C76CC176F47C0134588CBBCC21FDB81F ();
// 0x000007A1 System.Void MaterialUI.OverscrollConfig::Update()
extern void OverscrollConfig_Update_mF68509542AD293CE29E543F4464C01FCC3C40787 ();
// 0x000007A2 System.Void MaterialUI.OverscrollConfig::OnEdgeHit(System.Int32)
extern void OverscrollConfig_OnEdgeHit_m0322D48AE0207AA84D03C4FEDD3BD3125541AF13 ();
// 0x000007A3 System.Void MaterialUI.OverscrollConfig::OnEdgeStray(System.Boolean)
extern void OverscrollConfig_OnEdgeStray_m84CD26F97625A598467B79721994E6AE9E2F9397 ();
// 0x000007A4 System.Void MaterialUI.OverscrollConfig::SetupPointer(System.Int32)
extern void OverscrollConfig_SetupPointer_m79BA59D1E933F167A88348E0F8948D871751A715 ();
// 0x000007A5 System.Collections.IEnumerator MaterialUI.OverscrollConfig::GetDelayedSize()
extern void OverscrollConfig_GetDelayedSize_m6A28E8CECDF9F48ECD1C568547D3C52AC826FE8C ();
// 0x000007A6 System.Void MaterialUI.OverscrollConfig::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern void OverscrollConfig_OnInitializePotentialDrag_m4340F0A07BD7A9EEFF04187700B31D44F92089F2 ();
// 0x000007A7 System.Void MaterialUI.OverscrollConfig::.ctor()
extern void OverscrollConfig__ctor_m0AA67AE743AF2998BDF16A75315F237E36402100 ();
// 0x000007A8 MaterialUI.PanelExpander_ExpandMode MaterialUI.PanelExpander::get_expandMode()
extern void PanelExpander_get_expandMode_mDC726A9E97D6D5E9B3C92D381C1E94C57DC2F48C ();
// 0x000007A9 System.Void MaterialUI.PanelExpander::set_expandMode(MaterialUI.PanelExpander_ExpandMode)
extern void PanelExpander_set_expandMode_m0982DDF78E2B77D4257EBD468567C72EA097145D ();
// 0x000007AA UnityEngine.RectTransform MaterialUI.PanelExpander::get_panelRootRectTransform()
extern void PanelExpander_get_panelRootRectTransform_m7221471AD31BF00A4240B35D1AD275E98AA29253 ();
// 0x000007AB System.Void MaterialUI.PanelExpander::set_panelRootRectTransform(UnityEngine.RectTransform)
extern void PanelExpander_set_panelRootRectTransform_mDD1824337AD40D80716CD62ED39CDFBE6D95A6E1 ();
// 0x000007AC UnityEngine.CanvasGroup MaterialUI.PanelExpander::get_panelContentCanvasGroup()
extern void PanelExpander_get_panelContentCanvasGroup_m77F0B0F82F73977EB8D8D3CD2C081F1D209FC3A6 ();
// 0x000007AD System.Void MaterialUI.PanelExpander::set_panelContentCanvasGroup(UnityEngine.CanvasGroup)
extern void PanelExpander_set_panelContentCanvasGroup_mE27E96BD5EA809398223F831146F83A976F6366F ();
// 0x000007AE UnityEngine.CanvasGroup MaterialUI.PanelExpander::get_panelShadowCanvasGroup()
extern void PanelExpander_get_panelShadowCanvasGroup_m9A387739D61CD01305D572A0AAEA4ECB8990A682 ();
// 0x000007AF System.Void MaterialUI.PanelExpander::set_panelShadowCanvasGroup(UnityEngine.CanvasGroup)
extern void PanelExpander_set_panelShadowCanvasGroup_mBF5D134E1E7308DB4DC2B889DE71C75944371180 ();
// 0x000007B0 UnityEngine.RectTransform MaterialUI.PanelExpander::get_baseRectTransform()
extern void PanelExpander_get_baseRectTransform_mC09EF747EBD2CD6D502D2471D268B0B29C0005FD ();
// 0x000007B1 System.Void MaterialUI.PanelExpander::set_baseRectTransform(UnityEngine.RectTransform)
extern void PanelExpander_set_baseRectTransform_m3DCA95C40D48E2A96F4B6FA4142E060167D891E1 ();
// 0x000007B2 System.Boolean MaterialUI.PanelExpander::get_autoExpandedSize()
extern void PanelExpander_get_autoExpandedSize_m1DF6914C640BDF93B6CB90B11D684C9AA3C33CC8 ();
// 0x000007B3 System.Void MaterialUI.PanelExpander::set_autoExpandedSize(System.Boolean)
extern void PanelExpander_set_autoExpandedSize_m36CA645A5F7BD96D9CA1A5973381BEF40F27E760 ();
// 0x000007B4 UnityEngine.Vector2 MaterialUI.PanelExpander::get_expandedSize()
extern void PanelExpander_get_expandedSize_mBAE2672351FAA4F9E5E2634AC69ACB200DCDAEF9 ();
// 0x000007B5 System.Void MaterialUI.PanelExpander::set_expandedSize(UnityEngine.Vector2)
extern void PanelExpander_set_expandedSize_mF267E5DB37331E3C7632BAEB078E756F3E6752C9 ();
// 0x000007B6 System.Boolean MaterialUI.PanelExpander::get_autoExpandedPosition()
extern void PanelExpander_get_autoExpandedPosition_m871AFA3D08FDD72132912630B91012FCCE0CDC77 ();
// 0x000007B7 System.Void MaterialUI.PanelExpander::set_autoExpandedPosition(System.Boolean)
extern void PanelExpander_set_autoExpandedPosition_mEB0C420E495A3147EF71BADA240623C60377FACE ();
// 0x000007B8 UnityEngine.Vector2 MaterialUI.PanelExpander::get_expandedPosition()
extern void PanelExpander_get_expandedPosition_m0278F5D73C32396EC78B40E0802BAE2F848D10C8 ();
// 0x000007B9 System.Void MaterialUI.PanelExpander::set_expandedPosition(UnityEngine.Vector2)
extern void PanelExpander_set_expandedPosition_m4FCEC9149C5A245549B887CD84C0D6960DE1813B ();
// 0x000007BA System.Single MaterialUI.PanelExpander::get_animationDuration()
extern void PanelExpander_get_animationDuration_mB5BBAD96A0DDBE5DF877641E3F4A9355AEEDFD73 ();
// 0x000007BB System.Void MaterialUI.PanelExpander::set_animationDuration(System.Single)
extern void PanelExpander_set_animationDuration_m57FBCF73C1671BF038345884B8C0A9C9394AFCB5 ();
// 0x000007BC UnityEngine.RectTransform MaterialUI.PanelExpander::get_rippleMask()
extern void PanelExpander_get_rippleMask_mED903E9DFD3D2151144949791C2F8E411DC678F8 ();
// 0x000007BD System.Boolean MaterialUI.PanelExpander::get_rippleHasShadow()
extern void PanelExpander_get_rippleHasShadow_m5844A04DCC7A20B3F0789D05F04AA2665B4E99C3 ();
// 0x000007BE System.Void MaterialUI.PanelExpander::set_rippleHasShadow(System.Boolean)
extern void PanelExpander_set_rippleHasShadow_m45E30C9F7AED30AA1AD0FE6AE90A6842DD77937C ();
// 0x000007BF System.Boolean MaterialUI.PanelExpander::get_darkenBackground()
extern void PanelExpander_get_darkenBackground_m13F4902717A5B884F6ECC6ABFDF6C016E34E86F1 ();
// 0x000007C0 System.Void MaterialUI.PanelExpander::set_darkenBackground(System.Boolean)
extern void PanelExpander_set_darkenBackground_mF982E511F1A80D3D62843A49079524BDF198B373 ();
// 0x000007C1 System.Boolean MaterialUI.PanelExpander::get_clickBackgroundToClose()
extern void PanelExpander_get_clickBackgroundToClose_m4DABC7CBE9AF2392D60653957FAD99AB8ADAB304 ();
// 0x000007C2 System.Void MaterialUI.PanelExpander::set_clickBackgroundToClose(System.Boolean)
extern void PanelExpander_set_clickBackgroundToClose_m2B315C0E834A92A92C68C47D56427FE07D4D40DD ();
// 0x000007C3 System.Void MaterialUI.PanelExpander::Show()
extern void PanelExpander_Show_m2A1063D81409755E103CD6F6026D9C7617501D30 ();
// 0x000007C4 System.Void MaterialUI.PanelExpander::Hide()
extern void PanelExpander_Hide_m927B364D5390E18F5EADB435BB875569BE9823CF ();
// 0x000007C5 System.Void MaterialUI.PanelExpander::OnBackgroundClick()
extern void PanelExpander_OnBackgroundClick_mA8ADBB22DC258E91268024DDF47B37DD50BCEDF2 ();
// 0x000007C6 System.Void MaterialUI.PanelExpander::CalculateExpandMode()
extern void PanelExpander_CalculateExpandMode_mD7C4CAC82ADBAC7188B85F8543D91B19BFB5FB6E ();
// 0x000007C7 System.Void MaterialUI.PanelExpander::.ctor()
extern void PanelExpander__ctor_mA2581EDCBAF15B05AD3CA0B97CCC5426A1DD66B6 ();
// 0x000007C8 System.Void MaterialUI.PanelExpander::<Show>b__67_2(UnityEngine.EventSystems.BaseEventData)
extern void PanelExpander_U3CShowU3Eb__67_2_m2663F4E486CB95E945036544745367456541B672 ();
// 0x000007C9 System.Void MaterialUI.PanelExpander::<Show>b__67_0(System.Single)
extern void PanelExpander_U3CShowU3Eb__67_0_m0F0CA86DA1EF5B24BC5EBEF6AF847227F926135C ();
// 0x000007CA System.Void MaterialUI.PanelExpander::<Show>b__67_1(System.Single)
extern void PanelExpander_U3CShowU3Eb__67_1_m8E06C002B20EBBBB0ACE7E37DEFFB0EDFCA8D614 ();
// 0x000007CB System.Void MaterialUI.PanelExpander::<Show>b__67_4(UnityEngine.Color)
extern void PanelExpander_U3CShowU3Eb__67_4_mB08545C6CB1E9977F18CA696FEAC52669BA6CA3F ();
// 0x000007CC System.Void MaterialUI.PanelExpander::<Hide>b__68_0(System.Single)
extern void PanelExpander_U3CHideU3Eb__68_0_m4E450A9DD4368DFD991E0FBBCCE6E4AF5DDBA924 ();
// 0x000007CD System.Void MaterialUI.PanelExpander::<Hide>b__68_1()
extern void PanelExpander_U3CHideU3Eb__68_1_m30A967DE325906F834986D04AF8BDFC664865E98 ();
// 0x000007CE System.Void MaterialUI.PanelExpander::<Hide>b__68_2(System.Single)
extern void PanelExpander_U3CHideU3Eb__68_2_m2FCB98121B446BB8EE65DBD40C0E1DB33A45D2F6 ();
// 0x000007CF System.Void MaterialUI.PanelExpander::<Hide>b__68_3()
extern void PanelExpander_U3CHideU3Eb__68_3_mB759E1CBAF6AED5016ADA27674000FA398899795 ();
// 0x000007D0 System.Void MaterialUI.PanelExpander::<Hide>b__68_8()
extern void PanelExpander_U3CHideU3Eb__68_8_mF059801F091D93DFC0E7B67CE00260EE53BDA12C ();
// 0x000007D1 System.Void MaterialUI.PanelExpander::<Hide>b__68_4(UnityEngine.Color)
extern void PanelExpander_U3CHideU3Eb__68_4_m39CC8AB117E1D81364E949275A75D68C515D9E93 ();
// 0x000007D2 System.Void MaterialUI.PanelExpander::<Hide>b__68_5()
extern void PanelExpander_U3CHideU3Eb__68_5_m4633D83E5574D2E424E94C3CD567BB78AB84E71A ();
// 0x000007D3 System.Void MaterialUI.PanelExpander::<Hide>b__68_6()
extern void PanelExpander_U3CHideU3Eb__68_6_mA82040F740658E2A5DB8F0ED919DA65ED9CDBEBA ();
// 0x000007D4 UnityEngine.RectTransform MaterialUI.CircularProgressIndicator::get_circleRectTransform()
extern void CircularProgressIndicator_get_circleRectTransform_mF2E7FF9F7E321305A6B7C92F9DFFDFFFFEAD846B ();
// 0x000007D5 System.Void MaterialUI.CircularProgressIndicator::set_circleRectTransform(UnityEngine.RectTransform)
extern void CircularProgressIndicator_set_circleRectTransform_m73EC2EF7BF8D4AF2A76CB2E11E411DC37E908423 ();
// 0x000007D6 UnityEngine.UI.Image MaterialUI.CircularProgressIndicator::get_circleImage()
extern void CircularProgressIndicator_get_circleImage_m73C902760C2B02AE883B76F0261B1E82FF8C6427 ();
// 0x000007D7 MaterialUI.VectorImage MaterialUI.CircularProgressIndicator::get_circleIcon()
extern void CircularProgressIndicator_get_circleIcon_mEDC8973EC243098B98B8C8654ADBF1496B92ECBF ();
// 0x000007D8 System.Void MaterialUI.CircularProgressIndicator::Start()
extern void CircularProgressIndicator_Start_m48DB1888BBCF4D9CF85F1E0A276443328ED45CEB ();
// 0x000007D9 System.Void MaterialUI.CircularProgressIndicator::Update()
extern void CircularProgressIndicator_Update_m0B39BAA271F8CD4BB2B8A6AC3EA2611BC8747C67 ();
// 0x000007DA System.Void MaterialUI.CircularProgressIndicator::UpdateAnimCircle()
extern void CircularProgressIndicator_UpdateAnimCircle_m09A5F9A2A89BDB00900C540C8A02E8F0DF398C41 ();
// 0x000007DB System.Void MaterialUI.CircularProgressIndicator::UpdateAnimColor()
extern void CircularProgressIndicator_UpdateAnimColor_m191C8F9128BDFDAE96CD4FE8B69571DB81BAB019 ();
// 0x000007DC System.Void MaterialUI.CircularProgressIndicator::UpdateAnimSize()
extern void CircularProgressIndicator_UpdateAnimSize_mCD05E525D0205FF0F38F204A1FFBD1EB088B8148 ();
// 0x000007DD System.Void MaterialUI.CircularProgressIndicator::Show(System.Boolean)
extern void CircularProgressIndicator_Show_m6A069D0BC724FDDD05070225CA9D0572050C34D2 ();
// 0x000007DE System.Void MaterialUI.CircularProgressIndicator::Hide()
extern void CircularProgressIndicator_Hide_m4CA267708419474384DBA136367BB8CA670AAA35 ();
// 0x000007DF System.Void MaterialUI.CircularProgressIndicator::StartIndeterminate()
extern void CircularProgressIndicator_StartIndeterminate_mC54F1EE1CBB595BF32BE6BF29790336DA6329F80 ();
// 0x000007E0 System.Void MaterialUI.CircularProgressIndicator::SetProgress(System.Single,System.Boolean)
extern void CircularProgressIndicator_SetProgress_m8FB3A88C440FB8AC04093C743D6F44AB99BDE68D ();
// 0x000007E1 System.Void MaterialUI.CircularProgressIndicator::SetColor(UnityEngine.Color)
extern void CircularProgressIndicator_SetColor_m0B0ED5C0A16534A2A2C243AA4A0BE1C1BE5493B6 ();
// 0x000007E2 System.Void MaterialUI.CircularProgressIndicator::SetAnimCurrents()
extern void CircularProgressIndicator_SetAnimCurrents_m1BCC46235E5372A9013E49DBC34EB6C2F241B35C ();
// 0x000007E3 System.Void MaterialUI.CircularProgressIndicator::FlipCircle(System.Boolean)
extern void CircularProgressIndicator_FlipCircle_m714A108C613F80F22E9D6E34FAC8149ECA1CB74C ();
// 0x000007E4 System.Single MaterialUI.CircularProgressIndicator::GetMinWidth()
extern void CircularProgressIndicator_GetMinWidth_m85B88CC912262C66654FE4A3B879819B3B956B4E ();
// 0x000007E5 System.Single MaterialUI.CircularProgressIndicator::GetMinHeight()
extern void CircularProgressIndicator_GetMinHeight_m34BD5FD6788FBEF7F94E0A16198532E3ED9D247A ();
// 0x000007E6 System.Void MaterialUI.CircularProgressIndicator::.ctor()
extern void CircularProgressIndicator__ctor_m442F974C6648D14A955B2B5AC90C8100AFAB4130 ();
// 0x000007E7 UnityEngine.RectTransform MaterialUI.LinearProgressIndicator::get_barRectTransform()
extern void LinearProgressIndicator_get_barRectTransform_mB3F49AFC8B25DA2FC8E634B43624EF518CFDD5A9 ();
// 0x000007E8 System.Void MaterialUI.LinearProgressIndicator::set_barRectTransform(UnityEngine.RectTransform)
extern void LinearProgressIndicator_set_barRectTransform_mBC8D3167AD0B13C252CF5C674781DCF7AD4B1156 ();
// 0x000007E9 UnityEngine.UI.Image MaterialUI.LinearProgressIndicator::get_barImage()
extern void LinearProgressIndicator_get_barImage_m66CADF15494103552FD9F4C290C5219F1EA5B4F4 ();
// 0x000007EA UnityEngine.UI.Image MaterialUI.LinearProgressIndicator::get_backgroundImage()
extern void LinearProgressIndicator_get_backgroundImage_m5B64C28F588C41ACAB1F651188126F98A79E0D5F ();
// 0x000007EB System.Void MaterialUI.LinearProgressIndicator::set_backgroundImage(UnityEngine.UI.Image)
extern void LinearProgressIndicator_set_backgroundImage_m240FEA58428E73E6D430BAEE93008D2C4307154F ();
// 0x000007EC System.Void MaterialUI.LinearProgressIndicator::Start()
extern void LinearProgressIndicator_Start_m06AE8EF60990F7A2B94544B539D792384063761B ();
// 0x000007ED System.Void MaterialUI.LinearProgressIndicator::Update()
extern void LinearProgressIndicator_Update_m4E6CA3844A340FAB16D28076112357303075EE57 ();
// 0x000007EE System.Void MaterialUI.LinearProgressIndicator::UpdateAnimSize()
extern void LinearProgressIndicator_UpdateAnimSize_m06FB22D76CB82ABD1928872ABD38958DC7BA531E ();
// 0x000007EF System.Void MaterialUI.LinearProgressIndicator::UpdateAnimColor()
extern void LinearProgressIndicator_UpdateAnimColor_m4E8B49C8A0917181DAB1D9ED2DB559E6AC819FF6 ();
// 0x000007F0 System.Void MaterialUI.LinearProgressIndicator::UpdateAnimBar()
extern void LinearProgressIndicator_UpdateAnimBar_mED200F4DFF8C3772B647FA479689D322B7F49BFF ();
// 0x000007F1 System.Void MaterialUI.LinearProgressIndicator::Show(System.Boolean)
extern void LinearProgressIndicator_Show_mB9108D5F03C2C812143A4C3F69590676E60F6EE5 ();
// 0x000007F2 System.Void MaterialUI.LinearProgressIndicator::Hide()
extern void LinearProgressIndicator_Hide_m0039A20071553B4727DA61B67A7C61F4E7E9491C ();
// 0x000007F3 System.Void MaterialUI.LinearProgressIndicator::StartIndeterminate()
extern void LinearProgressIndicator_StartIndeterminate_m86819E0F4139704379907701A526B2292435AD72 ();
// 0x000007F4 System.Void MaterialUI.LinearProgressIndicator::SetProgress(System.Single,System.Boolean)
extern void LinearProgressIndicator_SetProgress_m3EF6DA971FDABAAB7BC96763A1B887F1FFFA29C6 ();
// 0x000007F5 System.Void MaterialUI.LinearProgressIndicator::SetColor(UnityEngine.Color)
extern void LinearProgressIndicator_SetColor_mAA6261BFC399C3507FFF81342F7412AFE4723F21 ();
// 0x000007F6 System.Single MaterialUI.LinearProgressIndicator::GetMinWidth()
extern void LinearProgressIndicator_GetMinWidth_m62AD6DB674279713589D042FB60A14D7CA1F36EB ();
// 0x000007F7 System.Single MaterialUI.LinearProgressIndicator::GetMinHeight()
extern void LinearProgressIndicator_GetMinHeight_mCADBB51C8A29D92C03FE6F9A4B88BD8D641C2389 ();
// 0x000007F8 System.Void MaterialUI.LinearProgressIndicator::.ctor()
extern void LinearProgressIndicator__ctor_m7B32C087D0BF40BB6FF4E59B271B645092D52A36 ();
// 0x000007F9 System.Single MaterialUI.ProgressIndicator::get_currentProgress()
extern void ProgressIndicator_get_currentProgress_m2DD33B30CB5B4CFA49BD935D3ADC5DCD3E84D871 ();
// 0x000007FA UnityEngine.RectTransform MaterialUI.ProgressIndicator::get_baseObjectOverride()
extern void ProgressIndicator_get_baseObjectOverride_m3304DC64E4AA85ECC044F57B0B656E8FD706E79B ();
// 0x000007FB System.Void MaterialUI.ProgressIndicator::set_baseObjectOverride(UnityEngine.RectTransform)
extern void ProgressIndicator_set_baseObjectOverride_m22A3231FB59A12DBC5BE4C4C5338530EC7C178FD ();
// 0x000007FC UnityEngine.RectTransform MaterialUI.ProgressIndicator::get_scaledRectTransform()
extern void ProgressIndicator_get_scaledRectTransform_mF28792495254C8444462D209FE045AD25A62DFCD ();
// 0x000007FD UnityEngine.RectTransform MaterialUI.ProgressIndicator::get_rectTransform()
extern void ProgressIndicator_get_rectTransform_m274E157F1465A5FCA34B4345C9AB9E3692FBF800 ();
// 0x000007FE System.Void MaterialUI.ProgressIndicator::Show(System.Boolean)
extern void ProgressIndicator_Show_mC48A9F4779CD7AB36ABBF9FC51D082A44F5C5940 ();
// 0x000007FF System.Void MaterialUI.ProgressIndicator::Hide()
extern void ProgressIndicator_Hide_m14A4B8C8F962F80AA2BCE6847828CB96CFABD114 ();
// 0x00000800 System.Void MaterialUI.ProgressIndicator::StartIndeterminate()
extern void ProgressIndicator_StartIndeterminate_mA7AC785F3643FF5A0FA6BCAEFA67AE34C424C955 ();
// 0x00000801 System.Void MaterialUI.ProgressIndicator::SetProgress(System.Single,System.Boolean)
extern void ProgressIndicator_SetProgress_mE3D66EC4E8D95697F228EBD02DD41A1D6F16B253 ();
// 0x00000802 System.Void MaterialUI.ProgressIndicator::SetColor(UnityEngine.Color)
extern void ProgressIndicator_SetColor_m1E2300571EFAEB7B5C8AB6D9775F1BDB4DFCC634 ();
// 0x00000803 System.Single MaterialUI.ProgressIndicator::GetMinWidth()
extern void ProgressIndicator_GetMinWidth_m2D9833E5AC92B68CC44A7EA5410E000F5F0CB4A6 ();
// 0x00000804 System.Single MaterialUI.ProgressIndicator::GetMinHeight()
extern void ProgressIndicator_GetMinHeight_m7608CD61CED66E64942806A1A7DA2ADC850DFCC4 ();
// 0x00000805 System.Void MaterialUI.ProgressIndicator::CalculateLayoutInputHorizontal()
extern void ProgressIndicator_CalculateLayoutInputHorizontal_m99EC4DA7D45AC1C0ABBF60AEF6DF9D310FBA3615 ();
// 0x00000806 System.Void MaterialUI.ProgressIndicator::CalculateLayoutInputVertical()
extern void ProgressIndicator_CalculateLayoutInputVertical_m30078A475F10CFFC3B3F8406BFA83FBF279FBEE1 ();
// 0x00000807 System.Single MaterialUI.ProgressIndicator::get_preferredWidth()
extern void ProgressIndicator_get_preferredWidth_m6F583EDCA8ADBC49D113AE66DCE3F8C07542A7C9 ();
// 0x00000808 System.Single MaterialUI.ProgressIndicator::get_minWidth()
extern void ProgressIndicator_get_minWidth_m6D97455DBF85A8DD85E258899D5B9C9ECBCACA32 ();
// 0x00000809 System.Single MaterialUI.ProgressIndicator::get_flexibleWidth()
extern void ProgressIndicator_get_flexibleWidth_m3AEE2FC255EA9BCE704A9FABDCE63F7E96279A2C ();
// 0x0000080A System.Single MaterialUI.ProgressIndicator::get_preferredHeight()
extern void ProgressIndicator_get_preferredHeight_m0174135911A565E1E7CB86DC2CF1E9961CCCDD4C ();
// 0x0000080B System.Single MaterialUI.ProgressIndicator::get_minHeight()
extern void ProgressIndicator_get_minHeight_m8F8D055C8F93CA73EBF2BB5C4E86AF2A5CB7BE30 ();
// 0x0000080C System.Single MaterialUI.ProgressIndicator::get_flexibleHeight()
extern void ProgressIndicator_get_flexibleHeight_m01C327AC8C7F5552965628CAC21E514673F061F2 ();
// 0x0000080D System.Int32 MaterialUI.ProgressIndicator::get_layoutPriority()
extern void ProgressIndicator_get_layoutPriority_mD0895CC071C02AABD1ACA7A66F6E84C5C632C2E2 ();
// 0x0000080E System.Void MaterialUI.ProgressIndicator::.ctor()
extern void ProgressIndicator__ctor_mEDFDEA8357D36C4F21EF63CACBC2BED7F1F4458B ();
// 0x0000080F System.Void MaterialUI.MaterialRadio::TurnOn()
extern void MaterialRadio_TurnOn_m5FA4A706AED9414DF2D32816EE281B8BCAE85E29 ();
// 0x00000810 System.Void MaterialUI.MaterialRadio::TurnOnInstant()
extern void MaterialRadio_TurnOnInstant_m604B91CDA0E32568FA7952A60FFFD8018FFA322B ();
// 0x00000811 System.Void MaterialUI.MaterialRadio::TurnOff()
extern void MaterialRadio_TurnOff_m416383476DBA9448D08A45274A1AB1C3A0DCC747 ();
// 0x00000812 System.Void MaterialUI.MaterialRadio::TurnOffInstant()
extern void MaterialRadio_TurnOffInstant_m42FB0646FA2FCB4D6FDBDFCE8C7D629D7EA14EDA ();
// 0x00000813 System.Void MaterialUI.MaterialRadio::Enable()
extern void MaterialRadio_Enable_mAB541A3B02D87AA9A6CA0FD34936DE252C5CF02C ();
// 0x00000814 System.Void MaterialUI.MaterialRadio::Disable()
extern void MaterialRadio_Disable_m728F24DD5ACDBB8DDE766182D1B1D9923982B788 ();
// 0x00000815 System.Void MaterialUI.MaterialRadio::AnimOn()
extern void MaterialRadio_AnimOn_m173F26A98F01292010C146D3B34D04A9ECF0CFF5 ();
// 0x00000816 System.Void MaterialUI.MaterialRadio::AnimOnComplete()
extern void MaterialRadio_AnimOnComplete_m2A89A0A3DE4B6A11597EF0EEBBC2555E624F76F9 ();
// 0x00000817 System.Void MaterialUI.MaterialRadio::AnimOff()
extern void MaterialRadio_AnimOff_m1EFA680A5A37117AF4C7BAD9EEE0868712C41F58 ();
// 0x00000818 System.Void MaterialUI.MaterialRadio::AnimOffComplete()
extern void MaterialRadio_AnimOffComplete_m77346F7E4D6EF7CD83B4783CC5201B4DD3B34577 ();
// 0x00000819 System.Void MaterialUI.MaterialRadio::.ctor()
extern void MaterialRadio__ctor_mF393AEECB82353B1D7F73D7A53F226255B8FC411 ();
// 0x0000081A System.Boolean MaterialUI.MaterialRadioGroup::get_isControllingChildren()
extern void MaterialRadioGroup_get_isControllingChildren_m0C508039585ADCB1E1F9601D01A9AE1CC19A6FD8 ();
// 0x0000081B System.Void MaterialUI.MaterialRadioGroup::set_isControllingChildren(System.Boolean)
extern void MaterialRadioGroup_set_isControllingChildren_m92C4B496B7069F9A946D71FC618B270D7D4CC9DA ();
// 0x0000081C System.Single MaterialUI.MaterialRadioGroup::get_animationDuration()
extern void MaterialRadioGroup_get_animationDuration_m44538EBC94BD78BBAAE3116AC96C28DD135DA297 ();
// 0x0000081D System.Void MaterialUI.MaterialRadioGroup::set_animationDuration(System.Single)
extern void MaterialRadioGroup_set_animationDuration_m6F966984932FDDED218259948AB73B497685297B ();
// 0x0000081E UnityEngine.Color MaterialUI.MaterialRadioGroup::get_onColor()
extern void MaterialRadioGroup_get_onColor_m689F4FA38FD83AF1AF7A2EFA8B6737186AC80C45 ();
// 0x0000081F System.Void MaterialUI.MaterialRadioGroup::set_onColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_onColor_mC2789FE37D50B71D203A40DBCF046777057093BC ();
// 0x00000820 UnityEngine.Color MaterialUI.MaterialRadioGroup::get_offColor()
extern void MaterialRadioGroup_get_offColor_m4E6C000D3B539A067616DB1490162A68CD3BBAB3 ();
// 0x00000821 System.Void MaterialUI.MaterialRadioGroup::set_offColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_offColor_mEAA358EF185978F3C746DA669D90859E8CC4C844 ();
// 0x00000822 UnityEngine.Color MaterialUI.MaterialRadioGroup::get_disabledColor()
extern void MaterialRadioGroup_get_disabledColor_mE152A7B35DE2BC062E0E437D4C25D4D6C59C6D05 ();
// 0x00000823 System.Void MaterialUI.MaterialRadioGroup::set_disabledColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_disabledColor_m14634FF58CA4F00D5BB9F07FEB2192E9831998E7 ();
// 0x00000824 System.Boolean MaterialUI.MaterialRadioGroup::get_changeGraphicColor()
extern void MaterialRadioGroup_get_changeGraphicColor_m3596CFEE052E7F4E8E152EFFB66D48C24ED5F16E ();
// 0x00000825 System.Void MaterialUI.MaterialRadioGroup::set_changeGraphicColor(System.Boolean)
extern void MaterialRadioGroup_set_changeGraphicColor_m23721D018BEDCB1A39F64E1D2A371E1449CD0C09 ();
// 0x00000826 UnityEngine.Color MaterialUI.MaterialRadioGroup::get_graphicOnColor()
extern void MaterialRadioGroup_get_graphicOnColor_m4AEEF42D853A4D8A1254AEAFFFD0CF25F68F761A ();
// 0x00000827 System.Void MaterialUI.MaterialRadioGroup::set_graphicOnColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_graphicOnColor_m7DD5D1BA8DBF8AB770C56EF690F2A363398A31BD ();
// 0x00000828 UnityEngine.Color MaterialUI.MaterialRadioGroup::get_graphicOffColor()
extern void MaterialRadioGroup_get_graphicOffColor_m665D3FED5EF2BDB57D251C0D433CE32645FC9EDE ();
// 0x00000829 System.Void MaterialUI.MaterialRadioGroup::set_graphicOffColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_graphicOffColor_m41F76CFA3B451A354635FA557014F6E7B7475E14 ();
// 0x0000082A UnityEngine.Color MaterialUI.MaterialRadioGroup::get_graphicDisabledColor()
extern void MaterialRadioGroup_get_graphicDisabledColor_m779EF0B15906E13BAF76EE5511A974C93F14604E ();
// 0x0000082B System.Void MaterialUI.MaterialRadioGroup::set_graphicDisabledColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_graphicDisabledColor_mCBAC0CE34C7506C7278C3770DD2C7812E471242C ();
// 0x0000082C System.Boolean MaterialUI.MaterialRadioGroup::get_changeRippleColor()
extern void MaterialRadioGroup_get_changeRippleColor_m131DAF3DD62048544A100B83277384EDF7831C72 ();
// 0x0000082D System.Void MaterialUI.MaterialRadioGroup::set_changeRippleColor(System.Boolean)
extern void MaterialRadioGroup_set_changeRippleColor_m9531BA7A1F6E2031B43696CC576C3BC3A0EDCACF ();
// 0x0000082E UnityEngine.Color MaterialUI.MaterialRadioGroup::get_rippleOnColor()
extern void MaterialRadioGroup_get_rippleOnColor_m9D334103C8F262BA34DB56B087CCBF4B21C6056E ();
// 0x0000082F System.Void MaterialUI.MaterialRadioGroup::set_rippleOnColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_rippleOnColor_mF79368E3D8A0BFB4D27668A894202E9487981832 ();
// 0x00000830 UnityEngine.Color MaterialUI.MaterialRadioGroup::get_rippleOffColor()
extern void MaterialRadioGroup_get_rippleOffColor_mBF313DB1F00B09816EA5ABE9A9895B746D30121A ();
// 0x00000831 System.Void MaterialUI.MaterialRadioGroup::set_rippleOffColor(UnityEngine.Color)
extern void MaterialRadioGroup_set_rippleOffColor_mB1D9186052B8E9682452C21BBC55149A1E833748 ();
// 0x00000832 System.Void MaterialUI.MaterialRadioGroup::Refresh()
extern void MaterialRadioGroup_Refresh_m01A0A0AF7443B05EF46A83A015E13E9905B67B73 ();
// 0x00000833 System.Void MaterialUI.MaterialRadioGroup::Start()
extern void MaterialRadioGroup_Start_m24F44BE6D2DDDD366E3CF532F52E5D590CD48B8C ();
// 0x00000834 System.Void MaterialUI.MaterialRadioGroup::.ctor()
extern void MaterialRadioGroup__ctor_m93C889643B561A36AF6F2CD7C447F62D3EAD8953 ();
// 0x00000835 System.Boolean MaterialUI.MaterialScreen::get_optionsControlledByScreenView()
extern void MaterialScreen_get_optionsControlledByScreenView_m8C9AF4E6728035A022BC5E6BA064E83D51E22835 ();
// 0x00000836 System.Void MaterialUI.MaterialScreen::set_optionsControlledByScreenView(System.Boolean)
extern void MaterialScreen_set_optionsControlledByScreenView_mBB7509C793760AE49D066222945DCDE03B160985 ();
// 0x00000837 System.Boolean MaterialUI.MaterialScreen::get_disableWhenNotVisible()
extern void MaterialScreen_get_disableWhenNotVisible_m565391D21838509FFB9C07642F541CFFF2F3AA16 ();
// 0x00000838 System.Void MaterialUI.MaterialScreen::set_disableWhenNotVisible(System.Boolean)
extern void MaterialScreen_set_disableWhenNotVisible_mAF9DE63F335A5B6D558FA44D71F3235A7C5840C2 ();
// 0x00000839 System.Boolean MaterialUI.MaterialScreen::get_fadeIn()
extern void MaterialScreen_get_fadeIn_mD386173FFCFAC0E0D30C8E8C0C14A0E073E2AFC3 ();
// 0x0000083A System.Void MaterialUI.MaterialScreen::set_fadeIn(System.Boolean)
extern void MaterialScreen_set_fadeIn_mD59986635AC4F9BB3D2784BB0B294BF5D5EED050 ();
// 0x0000083B MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_fadeInTweenType()
extern void MaterialScreen_get_fadeInTweenType_m028EDA4FC3E0B5989C671B9730FEBF35342A7C67 ();
// 0x0000083C System.Void MaterialUI.MaterialScreen::set_fadeInTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_fadeInTweenType_mD754A792B958DF9959B5A05F5020FE1E516E12E3 ();
// 0x0000083D System.Single MaterialUI.MaterialScreen::get_fadeInAlpha()
extern void MaterialScreen_get_fadeInAlpha_m94B5939770F256BEB3C35C31C2B7ED5D539E0C00 ();
// 0x0000083E System.Void MaterialUI.MaterialScreen::set_fadeInAlpha(System.Single)
extern void MaterialScreen_set_fadeInAlpha_mCF946CD83F23207A32BC7ACE1A95AC56F5C949CB ();
// 0x0000083F UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_fadeInCustomCurve()
extern void MaterialScreen_get_fadeInCustomCurve_mA1B1FE3383BF28F65C50DBC2D4CBF0A7FB11D1CF ();
// 0x00000840 System.Void MaterialUI.MaterialScreen::set_fadeInCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_fadeInCustomCurve_mC9581F4B447DED184C7B5092311215BECE4151F3 ();
// 0x00000841 System.Boolean MaterialUI.MaterialScreen::get_scaleIn()
extern void MaterialScreen_get_scaleIn_m155B87B2C3D3AADED2E713D2A912C849A32F39B0 ();
// 0x00000842 System.Void MaterialUI.MaterialScreen::set_scaleIn(System.Boolean)
extern void MaterialScreen_set_scaleIn_m9ADDDDB9A7527AE86A19184E55CEEF9ED077FDCA ();
// 0x00000843 MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_scaleInTweenType()
extern void MaterialScreen_get_scaleInTweenType_m7C40133127E35173B70BA55811D79B3FBC6420AA ();
// 0x00000844 System.Void MaterialUI.MaterialScreen::set_scaleInTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_scaleInTweenType_m34A6AD414B5EAEC164C5D389D78CA5BA15EC5689 ();
// 0x00000845 System.Single MaterialUI.MaterialScreen::get_scaleInScale()
extern void MaterialScreen_get_scaleInScale_m2F72FB01827FA074773F21D46A7F99C7D93A1376 ();
// 0x00000846 System.Void MaterialUI.MaterialScreen::set_scaleInScale(System.Single)
extern void MaterialScreen_set_scaleInScale_m24E05B438E8764810DF327EC638D0B7335567AF1 ();
// 0x00000847 UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_scaleInCustomCurve()
extern void MaterialScreen_get_scaleInCustomCurve_m1E0505A7E24FA24B547AC0B6DF1017454880171A ();
// 0x00000848 System.Void MaterialUI.MaterialScreen::set_scaleInCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_scaleInCustomCurve_m93BD32ED5904B2A89AD938539B8D2FECC972E40B ();
// 0x00000849 System.Boolean MaterialUI.MaterialScreen::get_slideIn()
extern void MaterialScreen_get_slideIn_m20AB3006703AD4B79DD4511FCB0426C701A74A62 ();
// 0x0000084A System.Void MaterialUI.MaterialScreen::set_slideIn(System.Boolean)
extern void MaterialScreen_set_slideIn_mD8E7A48F2B32BCA2BCB0541B3105DC00ADD02D12 ();
// 0x0000084B MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_slideInTweenType()
extern void MaterialScreen_get_slideInTweenType_m90706B218BD4F130940A79C97611BC2D3D9E5B85 ();
// 0x0000084C System.Void MaterialUI.MaterialScreen::set_slideInTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_slideInTweenType_m7E387230917F26F9B79E4FA3412A631E62BF9E94 ();
// 0x0000084D MaterialUI.ScreenView_SlideDirection MaterialUI.MaterialScreen::get_slideInDirection()
extern void MaterialScreen_get_slideInDirection_m47A6BED321BCCFDCAA78AF133F77C71CA42E832D ();
// 0x0000084E System.Void MaterialUI.MaterialScreen::set_slideInDirection(MaterialUI.ScreenView_SlideDirection)
extern void MaterialScreen_set_slideInDirection_m433AAB96891EF3EC37239840D2D7B271015107C1 ();
// 0x0000084F System.Boolean MaterialUI.MaterialScreen::get_autoSlideInAmount()
extern void MaterialScreen_get_autoSlideInAmount_m8B17D8FC23F03DF0A7DD2C6685A053C8E8CE7445 ();
// 0x00000850 System.Void MaterialUI.MaterialScreen::set_autoSlideInAmount(System.Boolean)
extern void MaterialScreen_set_autoSlideInAmount_mED2E07993A8865EC36DA24417CE3EF23FB2B94D4 ();
// 0x00000851 System.Single MaterialUI.MaterialScreen::get_slideInAmount()
extern void MaterialScreen_get_slideInAmount_m4F2EE292C2D653AA1E40B12207DF59D328A1DB28 ();
// 0x00000852 System.Void MaterialUI.MaterialScreen::set_slideInAmount(System.Single)
extern void MaterialScreen_set_slideInAmount_mFC0481F46A13C778334AFB60531DF49C5B406569 ();
// 0x00000853 System.Single MaterialUI.MaterialScreen::get_slideInPercent()
extern void MaterialScreen_get_slideInPercent_m3CD3BF6EF149207A1AAA463EC41F57EA46A93AFC ();
// 0x00000854 System.Void MaterialUI.MaterialScreen::set_slideInPercent(System.Single)
extern void MaterialScreen_set_slideInPercent_m722606E17FEC3F64803AA5E53F7A18D18B921ABA ();
// 0x00000855 UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_slideInCustomCurve()
extern void MaterialScreen_get_slideInCustomCurve_m2889F45DDDEBB33705D6ABADF3CBAE73C66D1A7C ();
// 0x00000856 System.Void MaterialUI.MaterialScreen::set_slideInCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_slideInCustomCurve_mE99E8F14E2B555DA58AB21412125CAEF6F8AEDBA ();
// 0x00000857 System.Boolean MaterialUI.MaterialScreen::get_rippleIn()
extern void MaterialScreen_get_rippleIn_mE83C408E37BB976C8A7F916C3A070CB6646E13E6 ();
// 0x00000858 System.Void MaterialUI.MaterialScreen::set_rippleIn(System.Boolean)
extern void MaterialScreen_set_rippleIn_mB5C44F0CFAA6D577360D2B112473E807C9A2B11B ();
// 0x00000859 MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_rippleInTweenType()
extern void MaterialScreen_get_rippleInTweenType_m5884B16DE52BE15A365817512DD953586E0383C4 ();
// 0x0000085A System.Void MaterialUI.MaterialScreen::set_rippleInTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_rippleInTweenType_m24D29308F95A5057E6B87B22AFA1C3115744BAB6 ();
// 0x0000085B MaterialUI.ScreenView_RippleType MaterialUI.MaterialScreen::get_rippleInType()
extern void MaterialScreen_get_rippleInType_m8713A7392D577348F9F63AC7F6C0F253E0C0893A ();
// 0x0000085C System.Void MaterialUI.MaterialScreen::set_rippleInType(MaterialUI.ScreenView_RippleType)
extern void MaterialScreen_set_rippleInType_m5710701B43ADA8CA5DCE0E07D280E1AB9F09E9EF ();
// 0x0000085D UnityEngine.Vector2 MaterialUI.MaterialScreen::get_rippleInPosition()
extern void MaterialScreen_get_rippleInPosition_m53FBC20467C84A02F2439DC181C15B04CD02A784 ();
// 0x0000085E System.Void MaterialUI.MaterialScreen::set_rippleInPosition(UnityEngine.Vector2)
extern void MaterialScreen_set_rippleInPosition_m10494943025CD1B526709DDCB820B43D77999B86 ();
// 0x0000085F UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_rippleInCustomCurve()
extern void MaterialScreen_get_rippleInCustomCurve_m1C97B8EC417442382068EF3F7007F9362C3FE85B ();
// 0x00000860 System.Void MaterialUI.MaterialScreen::set_rippleInCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_rippleInCustomCurve_mA2E1958362D27DC91F873D9FCE22DC6C32D10A4D ();
// 0x00000861 System.Boolean MaterialUI.MaterialScreen::get_fadeOut()
extern void MaterialScreen_get_fadeOut_m63E43181609D88F255897FA1B13C82A3E43B106C ();
// 0x00000862 System.Void MaterialUI.MaterialScreen::set_fadeOut(System.Boolean)
extern void MaterialScreen_set_fadeOut_m60219EB0A990E917A0F2E733306B522EBA977680 ();
// 0x00000863 MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_fadeOutTweenType()
extern void MaterialScreen_get_fadeOutTweenType_mF22EEEDB87DABE84CC9B9BCBD8B3B723DC4A5F72 ();
// 0x00000864 System.Void MaterialUI.MaterialScreen::set_fadeOutTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_fadeOutTweenType_m54238F9613B1BA64A3AB0F2AE53F6711EC9E344D ();
// 0x00000865 System.Single MaterialUI.MaterialScreen::get_fadeOutAlpha()
extern void MaterialScreen_get_fadeOutAlpha_m5BF6C8703F4B920C13FDA950A04662AD002F983A ();
// 0x00000866 System.Void MaterialUI.MaterialScreen::set_fadeOutAlpha(System.Single)
extern void MaterialScreen_set_fadeOutAlpha_mB78F24F4C9D42DB41F53CCEF274607B477E1BFAB ();
// 0x00000867 UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_fadeOutCustomCurve()
extern void MaterialScreen_get_fadeOutCustomCurve_m558914C1E7C467FB4A49E30222C7CDC85895108C ();
// 0x00000868 System.Void MaterialUI.MaterialScreen::set_fadeOutCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_fadeOutCustomCurve_m3BBEA4A9E4DE2EEC34FB55E712633C848F8E352F ();
// 0x00000869 System.Boolean MaterialUI.MaterialScreen::get_scaleOut()
extern void MaterialScreen_get_scaleOut_m037C8B56ABD51CEE89DF252DE52F18AB6116C1CA ();
// 0x0000086A System.Void MaterialUI.MaterialScreen::set_scaleOut(System.Boolean)
extern void MaterialScreen_set_scaleOut_mBFEFFEB951DD1F7E86A213480D11C4936056B157 ();
// 0x0000086B MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_scaleOutTweenType()
extern void MaterialScreen_get_scaleOutTweenType_m0B41CA5C8896E85DEB90F2A6EE91F980A5A869F7 ();
// 0x0000086C System.Void MaterialUI.MaterialScreen::set_scaleOutTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_scaleOutTweenType_m7B0AFDF662059FBAB7E2263EDDE670623FD7BF35 ();
// 0x0000086D System.Single MaterialUI.MaterialScreen::get_scaleOutScale()
extern void MaterialScreen_get_scaleOutScale_m1DCCA8FE7F946AD853C1F8DB6B70A62FC5901811 ();
// 0x0000086E System.Void MaterialUI.MaterialScreen::set_scaleOutScale(System.Single)
extern void MaterialScreen_set_scaleOutScale_mF2D6CA337E7FC4A7E3B530C5321B33A0AEC22AB4 ();
// 0x0000086F UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_scaleOutCustomCurve()
extern void MaterialScreen_get_scaleOutCustomCurve_mBEF670F50E8142411EF9AF57A22017AC72704530 ();
// 0x00000870 System.Void MaterialUI.MaterialScreen::set_scaleOutCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_scaleOutCustomCurve_m344C176BBF41E2F34C4AAD45607414846246A532 ();
// 0x00000871 System.Boolean MaterialUI.MaterialScreen::get_slideOut()
extern void MaterialScreen_get_slideOut_mEAC0FF8EF86893A069FEC6FCAFA72A4BA29BB726 ();
// 0x00000872 System.Void MaterialUI.MaterialScreen::set_slideOut(System.Boolean)
extern void MaterialScreen_set_slideOut_m64E206E169B6D606CD585D49704654BBBD71EC79 ();
// 0x00000873 MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_slideOutTweenType()
extern void MaterialScreen_get_slideOutTweenType_mDEADABA9E877CB345577C801E6F10602B7E92645 ();
// 0x00000874 System.Void MaterialUI.MaterialScreen::set_slideOutTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_slideOutTweenType_mAC28AF16462F2B3BC7B0DDF0058BD1AF60228AFD ();
// 0x00000875 MaterialUI.ScreenView_SlideDirection MaterialUI.MaterialScreen::get_slideOutDirection()
extern void MaterialScreen_get_slideOutDirection_m7EE939ECD255D8D509EE8FAE1EFA150E8FD29D84 ();
// 0x00000876 System.Void MaterialUI.MaterialScreen::set_slideOutDirection(MaterialUI.ScreenView_SlideDirection)
extern void MaterialScreen_set_slideOutDirection_mBDDB59543D38584035D50F1D39708FF71C117E09 ();
// 0x00000877 System.Boolean MaterialUI.MaterialScreen::get_autoSlideOutAmount()
extern void MaterialScreen_get_autoSlideOutAmount_m06971E28D9F2A41CD08FC0B8942ABEC4781883DC ();
// 0x00000878 System.Void MaterialUI.MaterialScreen::set_autoSlideOutAmount(System.Boolean)
extern void MaterialScreen_set_autoSlideOutAmount_mCFA75194A916410273AE4BDE5535BB72E2CF5144 ();
// 0x00000879 System.Single MaterialUI.MaterialScreen::get_slideOutAmount()
extern void MaterialScreen_get_slideOutAmount_m619EA66BF2E76ADCED0A492416F07B2239AB2FA6 ();
// 0x0000087A System.Void MaterialUI.MaterialScreen::set_slideOutAmount(System.Single)
extern void MaterialScreen_set_slideOutAmount_mBA4594C5D2C6B8480E871D3E43A4093A0CCE7CAA ();
// 0x0000087B System.Single MaterialUI.MaterialScreen::get_slideOutPercent()
extern void MaterialScreen_get_slideOutPercent_mC79A64DE6266DA2844A1F128699B28A73F8B73BD ();
// 0x0000087C System.Void MaterialUI.MaterialScreen::set_slideOutPercent(System.Single)
extern void MaterialScreen_set_slideOutPercent_mFEDFB3197ED578378D052ADC89EEE2E8662908EF ();
// 0x0000087D UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_slideOutCustomCurve()
extern void MaterialScreen_get_slideOutCustomCurve_m8B771CC33E3FF342DA665251E5EE737226E981CC ();
// 0x0000087E System.Void MaterialUI.MaterialScreen::set_slideOutCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_slideOutCustomCurve_m1EC1038737580903205F73B7A11F9601DAA4826A ();
// 0x0000087F System.Boolean MaterialUI.MaterialScreen::get_rippleOut()
extern void MaterialScreen_get_rippleOut_m2E61AF7B55FEDDD961CAFA3703E945611BE67A1C ();
// 0x00000880 System.Void MaterialUI.MaterialScreen::set_rippleOut(System.Boolean)
extern void MaterialScreen_set_rippleOut_m71447291C32328334B995526E0B06140905279C2 ();
// 0x00000881 MaterialUI.Tween_TweenType MaterialUI.MaterialScreen::get_rippleOutTweenType()
extern void MaterialScreen_get_rippleOutTweenType_m23CC2EECD0F7AD05849EB94F729AC94C720F8EEC ();
// 0x00000882 System.Void MaterialUI.MaterialScreen::set_rippleOutTweenType(MaterialUI.Tween_TweenType)
extern void MaterialScreen_set_rippleOutTweenType_m34D35C2C95E35224FE3F2D1A0D1ECA4CB9EE7C78 ();
// 0x00000883 MaterialUI.ScreenView_RippleType MaterialUI.MaterialScreen::get_rippleOutType()
extern void MaterialScreen_get_rippleOutType_mEEDFDC956CFD849CE5082710A293156FB77BE127 ();
// 0x00000884 System.Void MaterialUI.MaterialScreen::set_rippleOutType(MaterialUI.ScreenView_RippleType)
extern void MaterialScreen_set_rippleOutType_mF0D9E5298A2014B58E02F1793529C67178A76ADD ();
// 0x00000885 UnityEngine.Vector2 MaterialUI.MaterialScreen::get_rippleOutPosition()
extern void MaterialScreen_get_rippleOutPosition_mCBBCC44B95C94191580BBE9412A7C24130168332 ();
// 0x00000886 System.Void MaterialUI.MaterialScreen::set_rippleOutPosition(UnityEngine.Vector2)
extern void MaterialScreen_set_rippleOutPosition_m8585FA98F124C3AEBCC912AC81CFD540C054CFB9 ();
// 0x00000887 UnityEngine.AnimationCurve MaterialUI.MaterialScreen::get_rippleOutCustomCurve()
extern void MaterialScreen_get_rippleOutCustomCurve_mAC10EC568B347ABFDBE5CF728132006D9ADA159D ();
// 0x00000888 System.Void MaterialUI.MaterialScreen::set_rippleOutCustomCurve(UnityEngine.AnimationCurve)
extern void MaterialScreen_set_rippleOutCustomCurve_m9C5F83F6790CE8F1E43B6D1F1143FB2821B9CC8F ();
// 0x00000889 System.Single MaterialUI.MaterialScreen::get_transitionDuration()
extern void MaterialScreen_get_transitionDuration_m5B455DFD6225501FC8A3D53915557545E1C4BB4A ();
// 0x0000088A System.Void MaterialUI.MaterialScreen::set_transitionDuration(System.Single)
extern void MaterialScreen_set_transitionDuration_m28B916C7C00974577C57B35C91047319A3AAF7E6 ();
// 0x0000088B UnityEngine.RectTransform MaterialUI.MaterialScreen::get_ripple()
extern void MaterialScreen_get_ripple_mC8C2233E5599F22599CCC69492CD408B7A7374A8 ();
// 0x0000088C MaterialUI.ScreenView MaterialUI.MaterialScreen::get_screenView()
extern void MaterialScreen_get_screenView_mAA0F9BC36AACCCBA69DE1623EAF2A10E5BFB92EE ();
// 0x0000088D UnityEngine.RectTransform MaterialUI.MaterialScreen::get_rectTransform()
extern void MaterialScreen_get_rectTransform_mA59CB3F132FE37EE0E5752C199A9195034A2255A ();
// 0x0000088E UnityEngine.CanvasGroup MaterialUI.MaterialScreen::get_canvasGroup()
extern void MaterialScreen_get_canvasGroup_mFAB2691E52F1DEF51B883F34C3D1A3DAA2FC4F20 ();
// 0x0000088F System.Int32 MaterialUI.MaterialScreen::get_screenIndex()
extern void MaterialScreen_get_screenIndex_m82779D8F23D5C684D5CAE8F52B1B56B13584B0C1 ();
// 0x00000890 System.Void MaterialUI.MaterialScreen::set_screenIndex(System.Int32)
extern void MaterialScreen_set_screenIndex_mEDA40476FD8AD87F6DB2A36B405E19C6D9D0A07B ();
// 0x00000891 System.Void MaterialUI.MaterialScreen::CheckValues()
extern void MaterialScreen_CheckValues_mE67E05A57542B5B0CA484EB38043D3BF7C55DE56 ();
// 0x00000892 System.Void MaterialUI.MaterialScreen::SetupRipple()
extern void MaterialScreen_SetupRipple_m16F0C641140A9282D09D81A065BD93E578A9A992 ();
// 0x00000893 System.Void MaterialUI.MaterialScreen::TransitionIn()
extern void MaterialScreen_TransitionIn_mB6F9C838AD6901474BEBFB5E1BE22A545FEE5093 ();
// 0x00000894 System.Void MaterialUI.MaterialScreen::TransitionOut()
extern void MaterialScreen_TransitionOut_m932239AC67020C10FB0E0A3318DD9C10740A71AD ();
// 0x00000895 System.Void MaterialUI.MaterialScreen::TransitionOutWithoutTransition()
extern void MaterialScreen_TransitionOutWithoutTransition_m17E78A4B6C11F448CFBB8402C50B7F18E95A71FE ();
// 0x00000896 System.Void MaterialUI.MaterialScreen::Update()
extern void MaterialScreen_Update_mCDCF4E73513DBF9BD13A9883F414152891682378 ();
// 0x00000897 UnityEngine.Vector2 MaterialUI.MaterialScreen::GetRipplePosition()
extern void MaterialScreen_GetRipplePosition_mF2CE22A50679EA253D8E85E8F4B38793F173D024 ();
// 0x00000898 UnityEngine.Vector2 MaterialUI.MaterialScreen::GetRippleTargetSize()
extern void MaterialScreen_GetRippleTargetSize_m5D52724B97F39C9CAD4D29E5A477834CDFA8DB89 ();
// 0x00000899 UnityEngine.Vector3 MaterialUI.MaterialScreen::GetRippleTargetPosition()
extern void MaterialScreen_GetRippleTargetPosition_mCFCAD34C893E01D68FC9F6A1FD51B5F4DB86F55C ();
// 0x0000089A System.Void MaterialUI.MaterialScreen::Interrupt()
extern void MaterialScreen_Interrupt_mDF89B9202350B09234EFAEA32EF6D5E5D65AD13E ();
// 0x0000089B System.Void MaterialUI.MaterialScreen::.ctor()
extern void MaterialScreen__ctor_m407B2B587259AC2C77BC0B64547A52D865E84132 ();
// 0x0000089C MaterialUI.MaterialScreen[] MaterialUI.ScreenView::get_materialScreen()
extern void ScreenView_get_materialScreen_mC040184521061D840DE905C6B0FA1343DD66EF3D ();
// 0x0000089D System.Void MaterialUI.ScreenView::set_materialScreen(MaterialUI.MaterialScreen[])
extern void ScreenView_set_materialScreen_mBF6D27B5E44B9371B928E9B8B5F21E1D2325ECF2 ();
// 0x0000089E System.Int32 MaterialUI.ScreenView::get_currentScreen()
extern void ScreenView_get_currentScreen_mE68140E10C85B299BB8D4FE8F08F1221F1680B56 ();
// 0x0000089F System.Void MaterialUI.ScreenView::set_currentScreen(System.Int32)
extern void ScreenView_set_currentScreen_m161C465E81EB717A96D862ECDDEE069FB60AB54B ();
// 0x000008A0 System.Int32 MaterialUI.ScreenView::get_lastScreen()
extern void ScreenView_get_lastScreen_m9D32B27FE9E66CFE508F6B8AAE5A9DA2D0595426 ();
// 0x000008A1 System.Boolean MaterialUI.ScreenView::get_fadeIn()
extern void ScreenView_get_fadeIn_m448908128DD9DEF5BA22C8C09B0F73A0F3F885AA ();
// 0x000008A2 System.Void MaterialUI.ScreenView::set_fadeIn(System.Boolean)
extern void ScreenView_set_fadeIn_m1DECDC95DE03799D16F8038BC56873270B0795AA ();
// 0x000008A3 MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_fadeInTweenType()
extern void ScreenView_get_fadeInTweenType_mB7F0568BB2879A97C3DCDCC5EB5CFEF09084FFE5 ();
// 0x000008A4 System.Void MaterialUI.ScreenView::set_fadeInTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_fadeInTweenType_m1E44954340A2D1A72FEC36575D7BC956158DD594 ();
// 0x000008A5 System.Single MaterialUI.ScreenView::get_fadeInAlpha()
extern void ScreenView_get_fadeInAlpha_mA11BF52E9444099B879CEA1926BD9246A0E3B9A4 ();
// 0x000008A6 System.Void MaterialUI.ScreenView::set_fadeInAlpha(System.Single)
extern void ScreenView_set_fadeInAlpha_m60D51F7C270FE535317BBD4D9621FB3DF82F8F21 ();
// 0x000008A7 UnityEngine.AnimationCurve MaterialUI.ScreenView::get_fadeInCustomCurve()
extern void ScreenView_get_fadeInCustomCurve_mC9925269A84FBF31BC3D3EDB3014DC62813B39A7 ();
// 0x000008A8 System.Void MaterialUI.ScreenView::set_fadeInCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_fadeInCustomCurve_m0CC30AC3EC837651B06695EB93C79D2CF203F02C ();
// 0x000008A9 System.Boolean MaterialUI.ScreenView::get_scaleIn()
extern void ScreenView_get_scaleIn_m5A9AD92BD2B792F5B81E5E1004D13306D7039F6D ();
// 0x000008AA System.Void MaterialUI.ScreenView::set_scaleIn(System.Boolean)
extern void ScreenView_set_scaleIn_m372E17173379F8EB97B8C4C446B032B7001ED068 ();
// 0x000008AB MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_scaleInTweenType()
extern void ScreenView_get_scaleInTweenType_m587B8A07EB5F324BBAB0A77D4E8934AADEBB9A29 ();
// 0x000008AC System.Void MaterialUI.ScreenView::set_scaleInTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_scaleInTweenType_mC954E31AC8CF9C759ED4BF603EF227E48286B976 ();
// 0x000008AD System.Single MaterialUI.ScreenView::get_scaleInScale()
extern void ScreenView_get_scaleInScale_m23B299F9091235C89C833A23CAD2D7801C803772 ();
// 0x000008AE System.Void MaterialUI.ScreenView::set_scaleInScale(System.Single)
extern void ScreenView_set_scaleInScale_m0C93A22E80925A6DC2E8D54C51A95BC06A47EC54 ();
// 0x000008AF UnityEngine.AnimationCurve MaterialUI.ScreenView::get_scaleInCustomCurve()
extern void ScreenView_get_scaleInCustomCurve_m870C5E47B730A4F4FF253AE9486781127E2D583E ();
// 0x000008B0 System.Void MaterialUI.ScreenView::set_scaleInCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_scaleInCustomCurve_m3F2B01399238DCBD1363824D95291FA0433AEDA1 ();
// 0x000008B1 System.Boolean MaterialUI.ScreenView::get_slideIn()
extern void ScreenView_get_slideIn_mB76CEC39B3501B8F320C5DF6D7369F862F4DCF24 ();
// 0x000008B2 System.Void MaterialUI.ScreenView::set_slideIn(System.Boolean)
extern void ScreenView_set_slideIn_mB9B8339B7C4CF391EA4FE70F93678D39F10CEF9A ();
// 0x000008B3 MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_slideInTweenType()
extern void ScreenView_get_slideInTweenType_m10963CC9F24EA7DDB696E15732B893D3DE2DC680 ();
// 0x000008B4 System.Void MaterialUI.ScreenView::set_slideInTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_slideInTweenType_mD8A8E652185D1AE213F6EE17A7CFB0E06ECD0370 ();
// 0x000008B5 MaterialUI.ScreenView_SlideDirection MaterialUI.ScreenView::get_slideInDirection()
extern void ScreenView_get_slideInDirection_m027147453FC600A887409192C11CC5C7DB9F0228 ();
// 0x000008B6 System.Void MaterialUI.ScreenView::set_slideInDirection(MaterialUI.ScreenView_SlideDirection)
extern void ScreenView_set_slideInDirection_m4D99D1C31F5B80736CAA806E2D3557829F77E696 ();
// 0x000008B7 System.Boolean MaterialUI.ScreenView::get_autoSlideInAmount()
extern void ScreenView_get_autoSlideInAmount_m24B8748BC9856C3D513E225883982B2251E794B0 ();
// 0x000008B8 System.Void MaterialUI.ScreenView::set_autoSlideInAmount(System.Boolean)
extern void ScreenView_set_autoSlideInAmount_mBFEB0E81DC37E9F978C2B3D474182919201D2443 ();
// 0x000008B9 System.Single MaterialUI.ScreenView::get_slideInAmount()
extern void ScreenView_get_slideInAmount_mCCF93643C9E542F7845EB75AB12AB08A6040C36E ();
// 0x000008BA System.Void MaterialUI.ScreenView::set_slideInAmount(System.Single)
extern void ScreenView_set_slideInAmount_mB2F14FCBEB2865F2F055169AF9C0798FB3DBACFB ();
// 0x000008BB System.Single MaterialUI.ScreenView::get_slideInPercent()
extern void ScreenView_get_slideInPercent_m30B87BE1AAD1BD1500CCB9985C8A6A7FBC07C11E ();
// 0x000008BC System.Void MaterialUI.ScreenView::set_slideInPercent(System.Single)
extern void ScreenView_set_slideInPercent_mA46A64706CD582E1C7137EDC9C50EBC23E442DB3 ();
// 0x000008BD UnityEngine.AnimationCurve MaterialUI.ScreenView::get_slideInCustomCurve()
extern void ScreenView_get_slideInCustomCurve_m8D9170EBA0263141FF37B33F54F1240575783EEA ();
// 0x000008BE System.Void MaterialUI.ScreenView::set_slideInCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_slideInCustomCurve_mB6EDBA583ADCC24B451CD250BAD2DAE3F0084BC6 ();
// 0x000008BF System.Boolean MaterialUI.ScreenView::get_rippleIn()
extern void ScreenView_get_rippleIn_m564BD7AE70A32FA65D45F76E52EC2AB6E62D89CE ();
// 0x000008C0 System.Void MaterialUI.ScreenView::set_rippleIn(System.Boolean)
extern void ScreenView_set_rippleIn_mB0DF425B603F68140A1ECAC2544A5E666E5B20E9 ();
// 0x000008C1 MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_rippleInTweenType()
extern void ScreenView_get_rippleInTweenType_m6B1BD7BC706C7DE478FD2CC8ACBF8AA92F28ADB2 ();
// 0x000008C2 System.Void MaterialUI.ScreenView::set_rippleInTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_rippleInTweenType_m6A393091E852AE5BE3BEF580704AC0EBB4A67E7F ();
// 0x000008C3 MaterialUI.ScreenView_RippleType MaterialUI.ScreenView::get_rippleInType()
extern void ScreenView_get_rippleInType_mEE5649B9810F5D04D222A7723BF2FA4D0E4C1FC8 ();
// 0x000008C4 System.Void MaterialUI.ScreenView::set_rippleInType(MaterialUI.ScreenView_RippleType)
extern void ScreenView_set_rippleInType_mDAD063E9AFB1B7A921A2FD3ED115C7A3EFAE610C ();
// 0x000008C5 UnityEngine.Vector2 MaterialUI.ScreenView::get_rippleInPosition()
extern void ScreenView_get_rippleInPosition_mC069B22275D70959BC9ED9BE0222D906F9FE194B ();
// 0x000008C6 System.Void MaterialUI.ScreenView::set_rippleInPosition(UnityEngine.Vector2)
extern void ScreenView_set_rippleInPosition_m7B45A137E1F2E129A5EF4F13962DE4EF87EA9002 ();
// 0x000008C7 UnityEngine.AnimationCurve MaterialUI.ScreenView::get_rippleInCustomCurve()
extern void ScreenView_get_rippleInCustomCurve_m5B0780D2BFC6ECF0CCBA95153961BA0B84763DB0 ();
// 0x000008C8 System.Void MaterialUI.ScreenView::set_rippleInCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_rippleInCustomCurve_m31866CBE97167AEA8CF73E3DB089453E22D122EE ();
// 0x000008C9 System.Boolean MaterialUI.ScreenView::get_fadeOut()
extern void ScreenView_get_fadeOut_m298313FE709B654985943EF9295C4C4F1DC71BAB ();
// 0x000008CA System.Void MaterialUI.ScreenView::set_fadeOut(System.Boolean)
extern void ScreenView_set_fadeOut_mC2C35D06ECF760DCB3498264A316C6BB5B4EA397 ();
// 0x000008CB MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_fadeOutTweenType()
extern void ScreenView_get_fadeOutTweenType_m21C7C85F99C9BF76CD9FF9C5E15E8EE6B764CE50 ();
// 0x000008CC System.Void MaterialUI.ScreenView::set_fadeOutTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_fadeOutTweenType_mDF7E60A8419894027674AF9260B2894F4A783BFA ();
// 0x000008CD System.Single MaterialUI.ScreenView::get_fadeOutAlpha()
extern void ScreenView_get_fadeOutAlpha_m23D8FAC77F98D54E8412417292F217C25E823084 ();
// 0x000008CE System.Void MaterialUI.ScreenView::set_fadeOutAlpha(System.Single)
extern void ScreenView_set_fadeOutAlpha_m97835883865F95019771F2F72E27C7E5E6B3728A ();
// 0x000008CF UnityEngine.AnimationCurve MaterialUI.ScreenView::get_fadeOutCustomCurve()
extern void ScreenView_get_fadeOutCustomCurve_m7E64D45F73D369A0A183C46E8B022050B20C78EE ();
// 0x000008D0 System.Void MaterialUI.ScreenView::set_fadeOutCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_fadeOutCustomCurve_mAB5404C0B151205F7AB86D140E6EE706BBF121D1 ();
// 0x000008D1 System.Boolean MaterialUI.ScreenView::get_scaleOut()
extern void ScreenView_get_scaleOut_m8C6D35CDD364BFF392839D7E45B2D64A1B49F145 ();
// 0x000008D2 System.Void MaterialUI.ScreenView::set_scaleOut(System.Boolean)
extern void ScreenView_set_scaleOut_m3056BC3F1B6238131C91557070B83B078882A66F ();
// 0x000008D3 MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_scaleOutTweenType()
extern void ScreenView_get_scaleOutTweenType_m5836753A04BFF07643BBEB53CC12DFC71D1252AB ();
// 0x000008D4 System.Void MaterialUI.ScreenView::set_scaleOutTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_scaleOutTweenType_m1A34F297050D88113A700D959F3913862C5A3F18 ();
// 0x000008D5 System.Single MaterialUI.ScreenView::get_scaleOutScale()
extern void ScreenView_get_scaleOutScale_mD9D8DF7E864AF3A4C70EA5BBFAA6A389B2E6237E ();
// 0x000008D6 System.Void MaterialUI.ScreenView::set_scaleOutScale(System.Single)
extern void ScreenView_set_scaleOutScale_mA62CB704399899E41901A1D38BF3DBB1B8077EB1 ();
// 0x000008D7 UnityEngine.AnimationCurve MaterialUI.ScreenView::get_scaleOutCustomCurve()
extern void ScreenView_get_scaleOutCustomCurve_m8BCB567F745FAE3D8B0F832C0ECD6B378AC1C18B ();
// 0x000008D8 System.Void MaterialUI.ScreenView::set_scaleOutCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_scaleOutCustomCurve_m4CAABC799CE5D0B0C8605F822F2E4D18811D367D ();
// 0x000008D9 System.Boolean MaterialUI.ScreenView::get_slideOut()
extern void ScreenView_get_slideOut_m33E34413073D6E054440A797BE21D9F25F053720 ();
// 0x000008DA System.Void MaterialUI.ScreenView::set_slideOut(System.Boolean)
extern void ScreenView_set_slideOut_m4493DD5CDC18EE4C1BD4A0B5BF17CBEE11366506 ();
// 0x000008DB MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_slideOutTweenType()
extern void ScreenView_get_slideOutTweenType_mA3169EE1295EA60390FBD281DF4986D67EBFBFB5 ();
// 0x000008DC System.Void MaterialUI.ScreenView::set_slideOutTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_slideOutTweenType_m6537B9613890FC5EAE05E56DED7F78D8E0AC1035 ();
// 0x000008DD MaterialUI.ScreenView_SlideDirection MaterialUI.ScreenView::get_slideOutDirection()
extern void ScreenView_get_slideOutDirection_mF79BC144A20A6F6A33BD95A2565EB8AC9223505C ();
// 0x000008DE System.Void MaterialUI.ScreenView::set_slideOutDirection(MaterialUI.ScreenView_SlideDirection)
extern void ScreenView_set_slideOutDirection_m5EF9FBCAD203BBC5B2AA65519DBABC06C269AE36 ();
// 0x000008DF System.Boolean MaterialUI.ScreenView::get_autoSlideOutAmount()
extern void ScreenView_get_autoSlideOutAmount_m3DC088A0EF45C6975484E8C9A6EE07669E97DE94 ();
// 0x000008E0 System.Void MaterialUI.ScreenView::set_autoSlideOutAmount(System.Boolean)
extern void ScreenView_set_autoSlideOutAmount_m133C5F0BDDD0CF38F91AD270014C10BE4EBDE167 ();
// 0x000008E1 System.Single MaterialUI.ScreenView::get_slideOutAmount()
extern void ScreenView_get_slideOutAmount_m16D04247CA653FD9A5E3EFF0E127313351ACEA5D ();
// 0x000008E2 System.Void MaterialUI.ScreenView::set_slideOutAmount(System.Single)
extern void ScreenView_set_slideOutAmount_m262DB81070004C64C2C2A61C86758C8C7170E976 ();
// 0x000008E3 System.Single MaterialUI.ScreenView::get_slideOutPercent()
extern void ScreenView_get_slideOutPercent_m5F3A72ACE4DFFD579637140BDC329CF07C4636E3 ();
// 0x000008E4 System.Void MaterialUI.ScreenView::set_slideOutPercent(System.Single)
extern void ScreenView_set_slideOutPercent_m0CF36F53F1AF6DBD13BA820FE09BE9DEBE568BCF ();
// 0x000008E5 UnityEngine.AnimationCurve MaterialUI.ScreenView::get_slideOutCustomCurve()
extern void ScreenView_get_slideOutCustomCurve_m20AA838B18BD726EA93A84C63ADC4BA30A140661 ();
// 0x000008E6 System.Void MaterialUI.ScreenView::set_slideOutCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_slideOutCustomCurve_m987E68D8FC632C389E302C3568807FB9CE46ED4C ();
// 0x000008E7 System.Boolean MaterialUI.ScreenView::get_rippleOut()
extern void ScreenView_get_rippleOut_m506D46423A528EED8B9BA6F558854020BDB020D6 ();
// 0x000008E8 System.Void MaterialUI.ScreenView::set_rippleOut(System.Boolean)
extern void ScreenView_set_rippleOut_m285970BF64DBB1703FE22431217D83D957A61651 ();
// 0x000008E9 MaterialUI.Tween_TweenType MaterialUI.ScreenView::get_rippleOutTweenType()
extern void ScreenView_get_rippleOutTweenType_mF05CE39068906519435A7C6980F97ABEA215E68D ();
// 0x000008EA System.Void MaterialUI.ScreenView::set_rippleOutTweenType(MaterialUI.Tween_TweenType)
extern void ScreenView_set_rippleOutTweenType_m99EC35E8E9BE3381E19D63C65F39231758A65728 ();
// 0x000008EB MaterialUI.ScreenView_RippleType MaterialUI.ScreenView::get_rippleOutType()
extern void ScreenView_get_rippleOutType_m1749C1337E907F7497C48B0DD8B23F547875E896 ();
// 0x000008EC System.Void MaterialUI.ScreenView::set_rippleOutType(MaterialUI.ScreenView_RippleType)
extern void ScreenView_set_rippleOutType_m34D0A4DCADA36BEBDA538F2B6928230AF4ABC481 ();
// 0x000008ED UnityEngine.Vector2 MaterialUI.ScreenView::get_rippleOutPosition()
extern void ScreenView_get_rippleOutPosition_m635E9CD024B36D93BAB0D28F26C7B74BBF929231 ();
// 0x000008EE System.Void MaterialUI.ScreenView::set_rippleOutPosition(UnityEngine.Vector2)
extern void ScreenView_set_rippleOutPosition_m6DB91438DAE53DC051DB135F0DE9CE80176970BC ();
// 0x000008EF UnityEngine.AnimationCurve MaterialUI.ScreenView::get_rippleOutCustomCurve()
extern void ScreenView_get_rippleOutCustomCurve_mEE4E7B649255B7D13698DD18B53564E5ACD6058D ();
// 0x000008F0 System.Void MaterialUI.ScreenView::set_rippleOutCustomCurve(UnityEngine.AnimationCurve)
extern void ScreenView_set_rippleOutCustomCurve_mA7D4824CCB5FC71098B7B49B73D6B681751DCC53 ();
// 0x000008F1 System.Single MaterialUI.ScreenView::get_transitionDuration()
extern void ScreenView_get_transitionDuration_m713421A9156B8548E458C22373B44B324044FFD1 ();
// 0x000008F2 System.Void MaterialUI.ScreenView::set_transitionDuration(System.Single)
extern void ScreenView_set_transitionDuration_m75209A2B6E159D264AEF709AFB4E8A3D247909D5 ();
// 0x000008F3 MaterialUI.ScreenView_Type MaterialUI.ScreenView::get_transitionType()
extern void ScreenView_get_transitionType_mD90A20112553B932A036A73FC46FA7D5865C2367 ();
// 0x000008F4 System.Void MaterialUI.ScreenView::set_transitionType(MaterialUI.ScreenView_Type)
extern void ScreenView_set_transitionType_m5BFE2FF1CABA86F8C12E72AAA8BF7E9FB3D393F0 ();
// 0x000008F5 UnityEngine.Canvas MaterialUI.ScreenView::get_canvas()
extern void ScreenView_get_canvas_m90CD99FC5F6C13E336A2ABB7D02F8E1A90B7FCFA ();
// 0x000008F6 System.Void MaterialUI.ScreenView::set_canvas(UnityEngine.Canvas)
extern void ScreenView_set_canvas_m558E8AC9E140B92A06BAFF06D28D5A5A8B0EC570 ();
// 0x000008F7 UnityEngine.UI.GraphicRaycaster MaterialUI.ScreenView::get_graphicRaycaster()
extern void ScreenView_get_graphicRaycaster_m7086A4A54E591555AE8B1F92EC3CBDAFF1337481 ();
// 0x000008F8 System.Void MaterialUI.ScreenView::set_graphicRaycaster(UnityEngine.UI.GraphicRaycaster)
extern void ScreenView_set_graphicRaycaster_mC478F36C944497E4FDE0014FD690AF63273E047D ();
// 0x000008F9 MaterialUI.ScreenView_OnScreenTransitionUnityEvent MaterialUI.ScreenView::get_onScreenEndTransition()
extern void ScreenView_get_onScreenEndTransition_mAAD64C6D83CBEB6A49653826583590B8420301A5 ();
// 0x000008FA System.Void MaterialUI.ScreenView::set_onScreenEndTransition(MaterialUI.ScreenView_OnScreenTransitionUnityEvent)
extern void ScreenView_set_onScreenEndTransition_mE6A8391586AD252280AEA5872B89DCE0D067CE79 ();
// 0x000008FB MaterialUI.ScreenView_OnScreenTransitionUnityEvent MaterialUI.ScreenView::get_onScreenBeginTransition()
extern void ScreenView_get_onScreenBeginTransition_mD786325A01B1FC7400654A83845CE3F3AE1AF394 ();
// 0x000008FC System.Void MaterialUI.ScreenView::set_onScreenBeginTransition(MaterialUI.ScreenView_OnScreenTransitionUnityEvent)
extern void ScreenView_set_onScreenBeginTransition_m5C45CD074E45BF5C9FB3DDA0FA2EBBFE22BF3965 ();
// 0x000008FD System.Void MaterialUI.ScreenView::Start()
extern void ScreenView_Start_m0A85BC40968AD95F7281294C6C1F013161BFD3F5 ();
// 0x000008FE System.Void MaterialUI.ScreenView::Back()
extern void ScreenView_Back_m64EF03911E611F7C51B961AF633F6CDCB01F3AE6 ();
// 0x000008FF System.Void MaterialUI.ScreenView::Back(MaterialUI.ScreenView_Type)
extern void ScreenView_Back_m60AE0A9F884EA58B0FD2452035BEF75B35B86DB5 ();
// 0x00000900 System.Void MaterialUI.ScreenView::Transition(System.Int32)
extern void ScreenView_Transition_m3F4C90D7F4F6DFEC701AD31319F63DF1B9595DFF ();
// 0x00000901 System.Void MaterialUI.ScreenView::Transition(System.Int32,MaterialUI.ScreenView_Type)
extern void ScreenView_Transition_m7D34F0D19A709BDBAED70D8588BEB97296F92798 ();
// 0x00000902 System.Void MaterialUI.ScreenView::NextScreen(System.Boolean)
extern void ScreenView_NextScreen_m59E15077E2AFC5CFCDFE24051ADCE2A059D5116E ();
// 0x00000903 System.Void MaterialUI.ScreenView::PreviousScreen(System.Boolean)
extern void ScreenView_PreviousScreen_m65FD7EF62F0BBA8F8C3C3FE0AAF136A6C7A4BF3C ();
// 0x00000904 System.Void MaterialUI.ScreenView::Transition(System.String)
extern void ScreenView_Transition_m7658081324584E36CE2ED2A57EA7E67170BCB79A ();
// 0x00000905 System.Void MaterialUI.ScreenView::Transition(System.String,MaterialUI.ScreenView_Type)
extern void ScreenView_Transition_m14C443DCFB616B427A409DC735BF0D36249C4788 ();
// 0x00000906 System.Void MaterialUI.ScreenView::OnScreenEndTransition(System.Int32)
extern void ScreenView_OnScreenEndTransition_mCA1051BDBE8AB8E1122A40E01DCA663C1ACA7039 ();
// 0x00000907 System.Void MaterialUI.ScreenView::.ctor()
extern void ScreenView__ctor_m5CB9E8E76D011B0714922E7CCD0D1DEA64AF16B0 ();
// 0x00000908 System.String MaterialUI.Snackbar::get_actionName()
extern void Snackbar_get_actionName_m62160401F151F9A0026769C5B69FE66CCE7F9F4D ();
// 0x00000909 System.Void MaterialUI.Snackbar::set_actionName(System.String)
extern void Snackbar_set_actionName_mC5E29574483A474A45D135096769E00F473C4FB5 ();
// 0x0000090A System.Action MaterialUI.Snackbar::get_onActionButtonClicked()
extern void Snackbar_get_onActionButtonClicked_mA986C52C76C13F37439323D2BCB91C3D98B9F2AF ();
// 0x0000090B System.Void MaterialUI.Snackbar::set_onActionButtonClicked(System.Action)
extern void Snackbar_set_onActionButtonClicked_m2547F28A17DD5D5515B9E20695A67EA6F32A5FDD ();
// 0x0000090C System.Void MaterialUI.Snackbar::.ctor(System.String,System.Single,UnityEngine.Color,UnityEngine.Color,System.Int32,System.String,System.Action)
extern void Snackbar__ctor_m114AB2D290FC0CCF79125DC20050C2404FD02B4B ();
// 0x0000090D System.Void MaterialUI.SnackbarAnimator::Show(MaterialUI.Snackbar)
extern void SnackbarAnimator_Show_mB8C1F0ED6967B2D8DF055F56F9DF14034DB3A44F ();
// 0x0000090E System.Void MaterialUI.SnackbarAnimator::OnActionButtonClicked()
extern void SnackbarAnimator_OnActionButtonClicked_m0F30748BDD4C21E32C0042A2AFA5099DF4AD253C ();
// 0x0000090F System.Void MaterialUI.SnackbarAnimator::OnAnimDone()
extern void SnackbarAnimator_OnAnimDone_mBF6543BF5524836B3FD76F0775FA23D9AD1563D4 ();
// 0x00000910 System.Collections.IEnumerator MaterialUI.SnackbarAnimator::Setup()
extern void SnackbarAnimator_Setup_mADF8AFBBED028A6B0C12EDCF22D22CFE74A98A5C ();
// 0x00000911 System.Void MaterialUI.SnackbarAnimator::.ctor()
extern void SnackbarAnimator__ctor_m623953092C3C3BB55FAE2DC507D765F34DD4D2A4 ();
// 0x00000912 UnityEngine.UI.Text MaterialUI.TabItem::get_itemText()
extern void TabItem_get_itemText_mE7DAC31087DA178EFB3E1E501CE478B74D248203 ();
// 0x00000913 System.Void MaterialUI.TabItem::set_itemText(UnityEngine.UI.Text)
extern void TabItem_set_itemText_m6763075A8F21C19388D305DFF16AB5F6F1EBD941 ();
// 0x00000914 UnityEngine.UI.Graphic MaterialUI.TabItem::get_itemIcon()
extern void TabItem_get_itemIcon_m934A2883FA835085F33148A58252D0BCC5D4728B ();
// 0x00000915 System.Void MaterialUI.TabItem::set_itemIcon(UnityEngine.UI.Graphic)
extern void TabItem_set_itemIcon_m29482407C2012482599350CAD0DEA635E53123DC ();
// 0x00000916 MaterialUI.TabView MaterialUI.TabItem::get_tabView()
extern void TabItem_get_tabView_mB5167D4BA17AAAD87373DEB59547AE39589A5C16 ();
// 0x00000917 System.Void MaterialUI.TabItem::set_tabView(MaterialUI.TabView)
extern void TabItem_set_tabView_m67D37C974798469DB56725D7C317C12597807F17 ();
// 0x00000918 System.Int32 MaterialUI.TabItem::get_id()
extern void TabItem_get_id_m7626B93139145EEC7C29A7C27056C8795B67F516 ();
// 0x00000919 System.Void MaterialUI.TabItem::set_id(System.Int32)
extern void TabItem_set_id_mC3AD27509A4E25BCFA5EB553A90AE810F6B20F35 ();
// 0x0000091A UnityEngine.RectTransform MaterialUI.TabItem::get_rectTransform()
extern void TabItem_get_rectTransform_m0495C42140EDCC36AA5E99605EBD8BDEFF2CD62B ();
// 0x0000091B UnityEngine.CanvasGroup MaterialUI.TabItem::get_canvasGroup()
extern void TabItem_get_canvasGroup_m9EDFE25E0BE74AFAC7ECC38B362A3A7A2D71692C ();
// 0x0000091C System.Void MaterialUI.TabItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TabItem_OnPointerClick_mC27F85C266B5BE6E2BC91D4BE033D9C0A96C9A69 ();
// 0x0000091D System.Void MaterialUI.TabItem::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TabItem_OnPointerDown_mDDB9259629FF0BE02EC1E2CDA287855DECCE9D36 ();
// 0x0000091E System.Void MaterialUI.TabItem::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void TabItem_OnSubmit_mC7F0CFCE6EC74B73456E4F12F04F1BECFE29FCFD ();
// 0x0000091F System.Void MaterialUI.TabItem::SetupGraphic(MaterialUI.ImageDataType)
extern void TabItem_SetupGraphic_mB00FA1B526059DE12EB2A31A600852BED90692EA ();
// 0x00000920 System.Void MaterialUI.TabItem::.ctor()
extern void TabItem__ctor_mD8B029B0E665FAE69A0906ED405FF49ADA06A72A ();
// 0x00000921 System.Boolean MaterialUI.TabPage::get_interactable()
extern void TabPage_get_interactable_m9977D2392F9E9E469D04F1250D9205DFC7522A9D ();
// 0x00000922 System.Void MaterialUI.TabPage::set_interactable(System.Boolean)
extern void TabPage_set_interactable_mD4BEDCA3AF533B89FD1E83E7DC5A539C88627A10 ();
// 0x00000923 System.Boolean MaterialUI.TabPage::get_disableWhenNotVisible()
extern void TabPage_get_disableWhenNotVisible_mBED55F54FBC9F3AE6DD223CA5015559231F93325 ();
// 0x00000924 System.Void MaterialUI.TabPage::set_disableWhenNotVisible(System.Boolean)
extern void TabPage_set_disableWhenNotVisible_m569E7B98AA9A6D883EA3B3CEC8B2AB8A1A8B41E8 ();
// 0x00000925 System.Boolean MaterialUI.TabPage::get_showDisabledPanel()
extern void TabPage_get_showDisabledPanel_m08C2BC74A43B1ED005A1CDCBA5E05B6A4B9E4B15 ();
// 0x00000926 System.Void MaterialUI.TabPage::set_showDisabledPanel(System.Boolean)
extern void TabPage_set_showDisabledPanel_mD2A0737FF382C220C42EF45FF0048E4A443E349D ();
// 0x00000927 System.String MaterialUI.TabPage::get_tabName()
extern void TabPage_get_tabName_m01DE9F8D8B721F14F679B372634D9AAC5343F2CD ();
// 0x00000928 System.Void MaterialUI.TabPage::set_tabName(System.String)
extern void TabPage_set_tabName_m9A1F01C28E065023C5B5F9F6735DA56F09809B0B ();
// 0x00000929 MaterialUI.ImageData MaterialUI.TabPage::get_tabIcon()
extern void TabPage_get_tabIcon_m8C407B6AFC979584D34A5861715215CDA371252B ();
// 0x0000092A System.Void MaterialUI.TabPage::set_tabIcon(MaterialUI.ImageData)
extern void TabPage_set_tabIcon_m9157EBC799ACE4F510038940856ACBB64808A0B7 ();
// 0x0000092B UnityEngine.CanvasGroup MaterialUI.TabPage::get_canvasGroup()
extern void TabPage_get_canvasGroup_m79BB00D6937A3BAE2A2ED50318D0AD085F5E1F49 ();
// 0x0000092C UnityEngine.RectTransform MaterialUI.TabPage::get_rectTransform()
extern void TabPage_get_rectTransform_m7FD3EF48B9513E639C3AC48A0C1B84C4397CB92B ();
// 0x0000092D MaterialUI.TabView MaterialUI.TabPage::get_tabView()
extern void TabPage_get_tabView_mF03EA859EA99C262C804D198392BC89F42CCC213 ();
// 0x0000092E System.Void MaterialUI.TabPage::Update()
extern void TabPage_Update_m7F9FA872676CD52ADD1A8151A70849EAE8CE86BD ();
// 0x0000092F System.Void MaterialUI.TabPage::DisableIfAllowed()
extern void TabPage_DisableIfAllowed_m7184ED4DB6CB40F49B424797F8FE6576538FFD95 ();
// 0x00000930 System.Void MaterialUI.TabPage::.ctor()
extern void TabPage__ctor_m53BB6C58D417BCC1151245469343A311CCE87CB6 ();
// 0x00000931 MaterialUI.TabView MaterialUI.TabPagesScrollDetector::get_tabView()
extern void TabPagesScrollDetector_get_tabView_m3E92F768CB31EDB608C7B9573DE01717871C38C5 ();
// 0x00000932 System.Void MaterialUI.TabPagesScrollDetector::set_tabView(MaterialUI.TabView)
extern void TabPagesScrollDetector_set_tabView_m41D50A31B1C79C331301131AA198973C1E4F4EC7 ();
// 0x00000933 System.Void MaterialUI.TabPagesScrollDetector::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void TabPagesScrollDetector_OnDrag_mD30C341F4645A1AE615287A0960632E049C4CCAF ();
// 0x00000934 System.Void MaterialUI.TabPagesScrollDetector::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void TabPagesScrollDetector_OnEndDrag_mF8416A58556275E4A8E3A08E14ADA3E7E64041B5 ();
// 0x00000935 System.Void MaterialUI.TabPagesScrollDetector::.ctor()
extern void TabPagesScrollDetector__ctor_m0C007B32B70A1B0B4CE712863F9E399915692C0F ();
// 0x00000936 System.Single MaterialUI.TabView::get_shrinkTabsToFitThreshold()
extern void TabView_get_shrinkTabsToFitThreshold_mC6C9F0177BAF7A2BF453CF9CC84C047B4ED2F3C0 ();
// 0x00000937 System.Void MaterialUI.TabView::set_shrinkTabsToFitThreshold(System.Single)
extern void TabView_set_shrinkTabsToFitThreshold_m67C818D71EB868A3F73D36845CCCBFDE5F72F2F8 ();
// 0x00000938 System.Boolean MaterialUI.TabView::get_forceStretchTabsOnLanscape()
extern void TabView_get_forceStretchTabsOnLanscape_m70EC84EF69AA4D66037506292305EC8558A72E3D ();
// 0x00000939 System.Void MaterialUI.TabView::set_forceStretchTabsOnLanscape(System.Boolean)
extern void TabView_set_forceStretchTabsOnLanscape_m56C9316375B1B336D7AAC3C00170B0FF5F88460B ();
// 0x0000093A UnityEngine.RectTransform MaterialUI.TabView::get_tabsContainer()
extern void TabView_get_tabsContainer_m19EAF248E07156315A149F34E1E46274D38C35A0 ();
// 0x0000093B System.Void MaterialUI.TabView::set_tabsContainer(UnityEngine.RectTransform)
extern void TabView_set_tabsContainer_m62BDE762695B0B8926A1CF9E50953821707E1D98 ();
// 0x0000093C MaterialUI.TabPage[] MaterialUI.TabView::get_pages()
extern void TabView_get_pages_m06C444FF56847286EFBC4943AAE47161A6BEB73A ();
// 0x0000093D System.Void MaterialUI.TabView::set_pages(MaterialUI.TabPage[])
extern void TabView_set_pages_mFB52BFB4CD929CD0685AAB8D62F1C9307F36DE13 ();
// 0x0000093E System.Int32 MaterialUI.TabView::get_currentPage()
extern void TabView_get_currentPage_mF2999B7C3E943DB35DB48517B06BC79F3C741A31 ();
// 0x0000093F System.Void MaterialUI.TabView::set_currentPage(System.Int32)
extern void TabView_set_currentPage_m2A67312FB80A58B65AB011774E1C7AAE8BA0F1D8 ();
// 0x00000940 MaterialUI.TabItem MaterialUI.TabView::get_tabItemTemplate()
extern void TabView_get_tabItemTemplate_m95D4A1A9419FFE6F67078CD224503BFE3762293B ();
// 0x00000941 System.Void MaterialUI.TabView::set_tabItemTemplate(MaterialUI.TabItem)
extern void TabView_set_tabItemTemplate_mEE9620143B81FC6EB3BB92668CC1F30D2712D327 ();
// 0x00000942 UnityEngine.RectTransform MaterialUI.TabView::get_pagesContainer()
extern void TabView_get_pagesContainer_mE80479D1347D0D0502C157E0824CDB03840BC318 ();
// 0x00000943 System.Void MaterialUI.TabView::set_pagesContainer(UnityEngine.RectTransform)
extern void TabView_set_pagesContainer_mEF23535A0C695084BBA2BDD23959FE359C2A9359 ();
// 0x00000944 UnityEngine.RectTransform MaterialUI.TabView::get_pagesRect()
extern void TabView_get_pagesRect_m52C364640DD97FE6861955ACB08EAFDBA897A5F5 ();
// 0x00000945 System.Void MaterialUI.TabView::set_pagesRect(UnityEngine.RectTransform)
extern void TabView_set_pagesRect_mBDC561CBCB65EC6AC77E27C1B360C044F0079295 ();
// 0x00000946 UnityEngine.RectTransform MaterialUI.TabView::get_indicator()
extern void TabView_get_indicator_mB60B18F071BA6816719DC7C07128BC34FFA123C9 ();
// 0x00000947 System.Void MaterialUI.TabView::set_indicator(UnityEngine.RectTransform)
extern void TabView_set_indicator_mEA5EDC6B6B2F74EE1DA3E257FB0F11224EC5F565 ();
// 0x00000948 UnityEngine.UI.ScrollRect MaterialUI.TabView::get_pagesScrollRect()
extern void TabView_get_pagesScrollRect_m49013587C6AFFBAC208D89AB704B547D0732B38D ();
// 0x00000949 System.Single MaterialUI.TabView::get_tabWidth()
extern void TabView_get_tabWidth_m35E5CAFF0402C57548C04FB05220290962CADB07 ();
// 0x0000094A System.Single MaterialUI.TabView::get_tabPadding()
extern void TabView_get_tabPadding_m0AB02555FCC3A0F6B6491CC4A8107873CA67FAC4 ();
// 0x0000094B System.Void MaterialUI.TabView::set_tabPadding(System.Single)
extern void TabView_set_tabPadding_mFC219C82517A8EDE22602280DDBCC54780F1F971 ();
// 0x0000094C MaterialUI.TabItem[] MaterialUI.TabView::get_tabs()
extern void TabView_get_tabs_m529D78B6DF112B5D69C2A1331B7221E0A663A2A7 ();
// 0x0000094D System.Boolean MaterialUI.TabView::get_lowerUnselectedTabAlpha()
extern void TabView_get_lowerUnselectedTabAlpha_m29B6C62C43882348BFC71650CF9016D32038311D ();
// 0x0000094E System.Void MaterialUI.TabView::set_lowerUnselectedTabAlpha(System.Boolean)
extern void TabView_set_lowerUnselectedTabAlpha_m1ED66C4185EF4253C465B34C2FB353053BB17450 ();
// 0x0000094F System.Boolean MaterialUI.TabView::get_canScrollBetweenTabs()
extern void TabView_get_canScrollBetweenTabs_m872E1C9D249E4690A1A7C34EEE4C61921C73105E ();
// 0x00000950 System.Void MaterialUI.TabView::set_canScrollBetweenTabs(System.Boolean)
extern void TabView_set_canScrollBetweenTabs_m4B8E691E3B36E4BDA56F0181AFC8FFA46DB67B28 ();
// 0x00000951 UnityEngine.RectTransform MaterialUI.TabView::get_rectTransform()
extern void TabView_get_rectTransform_mD0CB9C7DCB4F627FD28FD37BB6E7BA0D15E5B7F7 ();
// 0x00000952 System.Void MaterialUI.TabView::Start()
extern void TabView_Start_m5485712005B71BE43126D4CCC6258EDB97801388 ();
// 0x00000953 System.Void MaterialUI.TabView::InitializeTabs()
extern void TabView_InitializeTabs_m42FC15E13CF2050EE8312FDC9DDD580E5328726B ();
// 0x00000954 System.Void MaterialUI.TabView::InitializePages()
extern void TabView_InitializePages_mA4A41FE711D423AB9219CD6FF1214EE1D8065A57 ();
// 0x00000955 System.Single MaterialUI.TabView::GetMaxTabTextWidth()
extern void TabView_GetMaxTabTextWidth_m2DA1C8B432EC69F509463ECE8627353A8D51F772 ();
// 0x00000956 System.Void MaterialUI.TabView::SetPage(System.Int32)
extern void TabView_SetPage_mB18A347A0ACAB08A81B6CBCEE9DB03251465DBDD ();
// 0x00000957 System.Void MaterialUI.TabView::SetPage(System.Int32,System.Boolean)
extern void TabView_SetPage_mC92977AF09073AEA9BCC12808E49E9E75A5FDE0F ();
// 0x00000958 System.Void MaterialUI.TabView::OnPagesTweenEnd()
extern void TabView_OnPagesTweenEnd_m19386D005D62E60AAA34A015B699F5752B2697D8 ();
// 0x00000959 System.Void MaterialUI.TabView::TweenPagesContainer(System.Int32,System.Boolean)
extern void TabView_TweenPagesContainer_m5967FFC235AC3E57ABE3DCAE6DC2501F301FCB30 ();
// 0x0000095A System.Void MaterialUI.TabView::TweenTabsContainer(System.Int32,System.Boolean)
extern void TabView_TweenTabsContainer_mEDBA8926BCD52963EF26685AE8AA73FA5AB8E864 ();
// 0x0000095B System.Void MaterialUI.TabView::TweenIndicator(System.Int32,System.Boolean)
extern void TabView_TweenIndicator_m6219BE14AABD7AFC966A43C5702D7C1AEB021D2B ();
// 0x0000095C System.Void MaterialUI.TabView::TabItemPointerDown(System.Int32)
extern void TabView_TabItemPointerDown_mE6E069C323E721E868A60208363BD070A7C17E99 ();
// 0x0000095D System.Void MaterialUI.TabView::TabPagePointerUp(System.Single)
extern void TabView_TabPagePointerUp_mF63755B4CCA10664D6B5C121565F4FA66F804662 ();
// 0x0000095E System.Int32 MaterialUI.TabView::NearestPage(System.Int32)
extern void TabView_NearestPage_m9A543F5F0AFB22C00CEE988A49DF13C3105900F6 ();
// 0x0000095F System.Void MaterialUI.TabView::TabPageDrag()
extern void TabView_TabPageDrag_m35267E68287CDABB17628F0F200C460068ADE6E1 ();
// 0x00000960 System.Void MaterialUI.TabView::.ctor()
extern void TabView__ctor_m2593687D496413F7BF4D2EADCFF9683CD837D98E ();
// 0x00000961 System.Void MaterialUI.TabView::<Start>b__65_0(System.Boolean,System.Boolean)
extern void TabView_U3CStartU3Eb__65_0_m3CCCEADB12B10B18AE5BCB33FB3FB4101CC20155 ();
// 0x00000962 System.Void MaterialUI.TabView::<TweenPagesContainer>b__72_0(UnityEngine.Vector2)
extern void TabView_U3CTweenPagesContainerU3Eb__72_0_m30D8BF203C99A7973ACA8E88ABECD6F7CD572D9E ();
// 0x00000963 System.Void MaterialUI.TabView::<TweenTabsContainer>b__73_0(UnityEngine.Vector2)
extern void TabView_U3CTweenTabsContainerU3Eb__73_0_mB3FD5113B97BB1AC5E9A36D68D6336D69B4CFF15 ();
// 0x00000964 System.Void MaterialUI.TabView::<TweenIndicator>b__74_0(UnityEngine.Vector2)
extern void TabView_U3CTweenIndicatorU3Eb__74_0_mDD09C72E055114CE6C26791D93CA21A1FB23DEB0 ();
// 0x00000965 System.String MaterialUI.Toast::get_content()
extern void Toast_get_content_m7B012246101CE3EF06A36EA61140CD1934777619 ();
// 0x00000966 System.Void MaterialUI.Toast::set_content(System.String)
extern void Toast_set_content_mD4893957DA443B4EF8D15C4418643363A623C194 ();
// 0x00000967 System.Single MaterialUI.Toast::get_duration()
extern void Toast_get_duration_mF2F87F12A6B1A8C7215B704E12181673BE5C2500 ();
// 0x00000968 System.Void MaterialUI.Toast::set_duration(System.Single)
extern void Toast_set_duration_m8E015BFB9B59BE62D0CBA20D0ABB18AA6A659899 ();
// 0x00000969 UnityEngine.Color MaterialUI.Toast::get_panelColor()
extern void Toast_get_panelColor_mF8028C6A94B86D706DEFE659A727A5DFAC28D7F1 ();
// 0x0000096A System.Void MaterialUI.Toast::set_panelColor(UnityEngine.Color)
extern void Toast_set_panelColor_mC6E17512913651C966DE0739C0C3CC8BEEA01EE1 ();
// 0x0000096B UnityEngine.Color MaterialUI.Toast::get_textColor()
extern void Toast_get_textColor_mF6A151AD1B8941538689B8E6BC702014FF64243E ();
// 0x0000096C System.Void MaterialUI.Toast::set_textColor(UnityEngine.Color)
extern void Toast_set_textColor_mA9D85D6F30765C88F174D350B5FFFAE6A8E3B165 ();
// 0x0000096D System.Int32 MaterialUI.Toast::get_fontSize()
extern void Toast_get_fontSize_m96E918F2521EF576B3FF5A0219B9CE181E6BF810 ();
// 0x0000096E System.Void MaterialUI.Toast::set_fontSize(System.Int32)
extern void Toast_set_fontSize_m258C8FA35B4C61805172FB3D3C5322CF9B6B4B37 ();
// 0x0000096F System.Void MaterialUI.Toast::.ctor(System.String,System.Single,UnityEngine.Color,UnityEngine.Color,System.Int32)
extern void Toast__ctor_m1F6B22FDBA19BA9F8F6E3A952758B7CC2709C59D ();
// 0x00000970 System.Void MaterialUI.ToastAnimator::OnEnable()
extern void ToastAnimator_OnEnable_m990C84FEC9C5A9AC75E1F5312B354043C3CE49C6 ();
// 0x00000971 System.Void MaterialUI.ToastAnimator::Init()
extern void ToastAnimator_Init_m0B199E08986B8D64DEAD32DAC492982765D6103B ();
// 0x00000972 System.Void MaterialUI.ToastAnimator::Show(MaterialUI.Toast)
extern void ToastAnimator_Show_mC58A7169924D27D09B9B1A35F24F055CA64C60FA ();
// 0x00000973 System.Void MaterialUI.ToastAnimator::Update()
extern void ToastAnimator_Update_m4B879865789893A83B5BB03095A8195072FF5CFB ();
// 0x00000974 System.Collections.IEnumerator MaterialUI.ToastAnimator::WaitTime()
extern void ToastAnimator_WaitTime_mD40FBEE27423178B80EDD99415761A93C81D1749 ();
// 0x00000975 System.Void MaterialUI.ToastAnimator::OnAnimDone()
extern void ToastAnimator_OnAnimDone_m7D7E50EF39F9C0FD128E8C19DB63C33428AC443D ();
// 0x00000976 System.Void MaterialUI.ToastAnimator::.ctor()
extern void ToastAnimator__ctor_mBF4EC0B86503845E0093F14CDCB7E173AEF76121 ();
// 0x00000977 MaterialUI.ImageDataType MaterialUI.ImageData::get_imageDataType()
extern void ImageData_get_imageDataType_m47313CB0BAD4F965EFF74BBBD5F252A29694C5FA ();
// 0x00000978 System.Void MaterialUI.ImageData::set_imageDataType(MaterialUI.ImageDataType)
extern void ImageData_set_imageDataType_m8C4F7454B557B765011CC835F163228DDAA1D3DB ();
// 0x00000979 UnityEngine.Sprite MaterialUI.ImageData::get_sprite()
extern void ImageData_get_sprite_mD879D608D11E479E951D4A7F775351B59B4D7728 ();
// 0x0000097A System.Void MaterialUI.ImageData::set_sprite(UnityEngine.Sprite)
extern void ImageData_set_sprite_m35EEDF6A0CD2F31F12554CB668DA677414BE2A81 ();
// 0x0000097B MaterialUI.VectorImageData MaterialUI.ImageData::get_vectorImageData()
extern void ImageData_get_vectorImageData_m0E1361FBAC6654A1ADDD95B5DED4E8E33F25839D ();
// 0x0000097C System.Void MaterialUI.ImageData::set_vectorImageData(MaterialUI.VectorImageData)
extern void ImageData_set_vectorImageData_mA74576ADC12796EF7C2EB5F2578FDF147AC31AF3 ();
// 0x0000097D System.Void MaterialUI.ImageData::.ctor(UnityEngine.Sprite)
extern void ImageData__ctor_m78D727641D91DB9969C4FB6AA1E92F34F51D0535 ();
// 0x0000097E System.Void MaterialUI.ImageData::.ctor(MaterialUI.VectorImageData)
extern void ImageData__ctor_m4E7F6F5C356D56F6071493A723FAA510023646AF ();
// 0x0000097F System.Boolean MaterialUI.ImageData::ContainsData(System.Boolean)
extern void ImageData_ContainsData_m66874F2A15C8666BD895BA0B64EB79E430738562 ();
// 0x00000980 MaterialUI.ImageData[] MaterialUI.ImageData::ArrayFromSpriteArray(UnityEngine.Sprite[])
extern void ImageData_ArrayFromSpriteArray_mC7B1F93314D8C6E40E8408C917B383CBE9244FA8 ();
// 0x00000981 MaterialUI.ImageData[] MaterialUI.ImageData::ArrayFromVectorArray(MaterialUI.VectorImageData[])
extern void ImageData_ArrayFromVectorArray_m41C04A8B80B87332AF92EF34B53369B49A03740F ();
// 0x00000982 System.Void MaterialUI.MaterialIconHelper::.cctor()
extern void MaterialIconHelper__cctor_m60E76BF6A1272DE818B769245C02F5A234FF700A ();
// 0x00000983 MaterialUI.ImageData MaterialUI.MaterialIconHelper::GetIcon(MaterialUI.MaterialIconEnum)
extern void MaterialIconHelper_GetIcon_m85A699DEE71B8D3FC2CE9CDF1BF33167A25CEAD4 ();
// 0x00000984 MaterialUI.ImageData MaterialUI.MaterialIconHelper::GetIcon(System.String)
extern void MaterialIconHelper_GetIcon_mB176DAFE7FCBE29645FA5D98783F5E618FEEC7BA ();
// 0x00000985 MaterialUI.ImageData MaterialUI.MaterialIconHelper::GetRandomIcon()
extern void MaterialIconHelper_GetRandomIcon_m0CCD248F149343A45FD5A83C0AEF94044AF0349B ();
// 0x00000986 System.Void MaterialUI.MaterialUIIconHelper::.cctor()
extern void MaterialUIIconHelper__cctor_m0FD12AE1E9D6820F875B518380A9B4FB7C31023E ();
// 0x00000987 MaterialUI.ImageData MaterialUI.MaterialUIIconHelper::GetIcon(System.String)
extern void MaterialUIIconHelper_GetIcon_m4F07CACDC8DB96481AB121BD9337C0B91F50C2A7 ();
// 0x00000988 MaterialUI.ImageData MaterialUI.MaterialUIIconHelper::GetRandomIcon()
extern void MaterialUIIconHelper_GetRandomIcon_mA5F414298E7B477781C28AE098166076C6CB4223 ();
// 0x00000989 System.String MaterialUI.Glyph::get_name()
extern void Glyph_get_name_m24CF8983933215030054129B7374338FAA73F2F1 ();
// 0x0000098A System.Void MaterialUI.Glyph::set_name(System.String)
extern void Glyph_set_name_m569CEAA0EC3961E389BE7CAED46FAC0324DB71B5 ();
// 0x0000098B System.String MaterialUI.Glyph::get_unicode()
extern void Glyph_get_unicode_m3B39A95853A6E6D2706FD917B983BC78D38AB396 ();
// 0x0000098C System.Void MaterialUI.Glyph::set_unicode(System.String)
extern void Glyph_set_unicode_mD53B42E95FD3519602798C220DD4417345D6BBBB ();
// 0x0000098D System.Void MaterialUI.Glyph::.ctor()
extern void Glyph__ctor_m7DCC1F26CC1FD50F5C2FF1211A54E455DDF6CECB ();
// 0x0000098E System.Void MaterialUI.Glyph::.ctor(System.String,System.String,System.Boolean)
extern void Glyph__ctor_m1D933159347D47533906412C8CB55EA76DC09C3B ();
// 0x0000098F MaterialUI.Glyph MaterialUI.VectorImageData::get_glyph()
extern void VectorImageData_get_glyph_mDC19C14D55ECAD0FE0AAE5B12E3C7BD4BD38A824 ();
// 0x00000990 System.Void MaterialUI.VectorImageData::set_glyph(MaterialUI.Glyph)
extern void VectorImageData_set_glyph_m1096F14DD58633D67F620955EC5FC78CD9E199C2 ();
// 0x00000991 UnityEngine.Font MaterialUI.VectorImageData::get_font()
extern void VectorImageData_get_font_m899CA8BF0BD2C3022DA71F712AA41C381394E628 ();
// 0x00000992 System.Void MaterialUI.VectorImageData::set_font(UnityEngine.Font)
extern void VectorImageData_set_font_m5FDF140F1A0F0AD2C9C9B7FE1F65B3D412E5646B ();
// 0x00000993 System.Void MaterialUI.VectorImageData::.ctor()
extern void VectorImageData__ctor_m1E793056B33C6B2BBFD643EDBAB30FB69D070294 ();
// 0x00000994 System.Void MaterialUI.VectorImageData::.ctor(MaterialUI.Glyph,UnityEngine.Font)
extern void VectorImageData__ctor_m8D8AD84C4DB510E6EE7E44E913B128D5F674F7A8 ();
// 0x00000995 System.Boolean MaterialUI.VectorImageData::ContainsData()
extern void VectorImageData_ContainsData_m3279973291C19A1B25F86543EE559CE90E16200A ();
// 0x00000996 System.Collections.Generic.List`1<MaterialUI.Glyph> MaterialUI.VectorImageSet::get_iconGlyphList()
extern void VectorImageSet_get_iconGlyphList_m8D55B11E23FF7CD7FCD438E1ADAD9951B9D1B012 ();
// 0x00000997 System.Void MaterialUI.VectorImageSet::set_iconGlyphList(System.Collections.Generic.List`1<MaterialUI.Glyph>)
extern void VectorImageSet_set_iconGlyphList_m97F54AC38A6B8D77129810BE113B4F86CD47D94D ();
// 0x00000998 System.Void MaterialUI.VectorImageSet::.ctor()
extern void VectorImageSet__ctor_m95FC5BD43B0D5F8C3B104673DF2E0EECD1814B73 ();
// 0x00000999 System.Single MaterialUI.VectorImage::get_size()
extern void VectorImage_get_size_mF574C0DC7A298B548D4DC5F26B6D17AF72DED59D ();
// 0x0000099A System.Void MaterialUI.VectorImage::set_size(System.Single)
extern void VectorImage_set_size_mE6C08D268FDE8850339E331AFD92E7CA17BD18BA ();
// 0x0000099B System.Single MaterialUI.VectorImage::get_scaledSize()
extern void VectorImage_get_scaledSize_m75762E8C407954D1F533C6C6847FFE64058F8BD2 ();
// 0x0000099C MaterialUI.VectorImage_SizeMode MaterialUI.VectorImage::get_sizeMode()
extern void VectorImage_get_sizeMode_mFD01469CE229B1648B1E6B5F2A5C30485A3714A7 ();
// 0x0000099D System.Void MaterialUI.VectorImage::set_sizeMode(MaterialUI.VectorImage_SizeMode)
extern void VectorImage_set_sizeMode_mDB2A16EFB133C46A97F8BCFB05CC9B202691455B ();
// 0x0000099E MaterialUI.MaterialUIScaler MaterialUI.VectorImage::get_materialUiScaler()
extern void VectorImage_get_materialUiScaler_m11BFD68684F76F71CCFB21267AC839F5D395D986 ();
// 0x0000099F MaterialUI.VectorImageData MaterialUI.VectorImage::get_vectorImageData()
extern void VectorImage_get_vectorImageData_m14D2D4213409747DB38FC7F15237C233306AC71E ();
// 0x000009A0 System.Void MaterialUI.VectorImage::set_vectorImageData(MaterialUI.VectorImageData)
extern void VectorImage_set_vectorImageData_mEC90C234A36F541263A5490E9C724636B6051157 ();
// 0x000009A1 System.Void MaterialUI.VectorImage::Refresh()
extern void VectorImage_Refresh_mA5A6D06D7A2762C6217346AFAF558E3407DE6D82 ();
// 0x000009A2 System.Void MaterialUI.VectorImage::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void VectorImage_OnPopulateMesh_mF1B639EE71B659863637B496EA2BB3980EFA650E ();
// 0x000009A3 System.Void MaterialUI.VectorImage::SetLayoutDirty()
extern void VectorImage_SetLayoutDirty_m14543F897C3C56AAF0EAD79E8CD66F7007BD9912 ();
// 0x000009A4 System.Void MaterialUI.VectorImage::SetVerticesDirty()
extern void VectorImage_SetVerticesDirty_m47C754A22E51BE60A3630AD4423788718EC245E6 ();
// 0x000009A5 System.Void MaterialUI.VectorImage::SetMaterialDirty()
extern void VectorImage_SetMaterialDirty_m9CF3CB39A412CCF583CB5584EB922C73379C461A ();
// 0x000009A6 System.Void MaterialUI.VectorImage::OnEnable()
extern void VectorImage_OnEnable_m4D927807590B8AEF0F130AFF58E0FF855FACD92D ();
// 0x000009A7 System.Void MaterialUI.VectorImage::OnDisable()
extern void VectorImage_OnDisable_mEE83D0F8A2EB094D4FBF22EF92CF58489BF3B7C0 ();
// 0x000009A8 System.Void MaterialUI.VectorImage::Start()
extern void VectorImage_Start_m6E88727D61D68C4C969634A3C9811CCE0C36CE0E ();
// 0x000009A9 System.Void MaterialUI.VectorImage::updateFontAndText()
extern void VectorImage_updateFontAndText_m5E8921737A015221C211D6FE13DF0AF6DA902380 ();
// 0x000009AA System.Void MaterialUI.VectorImage::RefreshScale()
extern void VectorImage_RefreshScale_m2FB255BC3B2CE587BB03F7C9695BFB37EB138036 ();
// 0x000009AB System.Void MaterialUI.VectorImage::CalculateLayoutInputHorizontal()
extern void VectorImage_CalculateLayoutInputHorizontal_mB1411BAE9CC3AAD81969ADCD2ACAE9A598DAF07B ();
// 0x000009AC System.Void MaterialUI.VectorImage::CalculateLayoutInputVertical()
extern void VectorImage_CalculateLayoutInputVertical_m396F651C0185BB756E557003AA5701A83DD9A038 ();
// 0x000009AD System.Void MaterialUI.VectorImage::OnRectTransformDimensionsChange()
extern void VectorImage_OnRectTransformDimensionsChange_m6E9A6200E8EACB92F17590667F1F3FE286048EC8 ();
// 0x000009AE System.Single MaterialUI.VectorImage::get_preferredWidth()
extern void VectorImage_get_preferredWidth_mFDFE2865A2B2D3BEAFD781170DD7780411FB765B ();
// 0x000009AF System.Single MaterialUI.VectorImage::get_minWidth()
extern void VectorImage_get_minWidth_m2DA71460CEE7762AC998EE9846E2F97C4659E6AC ();
// 0x000009B0 System.Single MaterialUI.VectorImage::get_flexibleWidth()
extern void VectorImage_get_flexibleWidth_m1D81950AB1B56E639E9F89E1E22F74FA01DF6D7A ();
// 0x000009B1 System.Single MaterialUI.VectorImage::get_preferredHeight()
extern void VectorImage_get_preferredHeight_m86454825B4D89248010B418C8120A00F0A194744 ();
// 0x000009B2 System.Single MaterialUI.VectorImage::get_minHeight()
extern void VectorImage_get_minHeight_mF0440EEB591318E983CFBD6A168C3C0008D362EA ();
// 0x000009B3 System.Single MaterialUI.VectorImage::get_flexibleHeight()
extern void VectorImage_get_flexibleHeight_m064131D4CBAF4FC842C85A202AEBC88969AE1651 ();
// 0x000009B4 System.Void MaterialUI.VectorImage::.ctor()
extern void VectorImage__ctor_m5998DA07058E2A7D9692318A0331D7C064DB04AB ();
// 0x000009B5 System.String MaterialUI.VectorImageManager::get_fontDestinationFolder()
extern void VectorImageManager_get_fontDestinationFolder_m593D67BC61263C4AADF130D94A4FDF5CF206AEEE ();
// 0x000009B6 System.Void MaterialUI.VectorImageManager::set_fontDestinationFolder(System.String)
extern void VectorImageManager_set_fontDestinationFolder_m4C3A650BC9A681EA0D0BEC6ACD4B64D5C51CDBD1 ();
// 0x000009B7 System.Boolean MaterialUI.VectorImageManager::FontDirExists()
extern void VectorImageManager_FontDirExists_mBC2684C4A459A522DF9D76127A7D508D3569FCB7 ();
// 0x000009B8 System.String[] MaterialUI.VectorImageManager::GetFontDirectories(System.String)
extern void VectorImageManager_GetFontDirectories_m5407B1B01F15BBC199DE021D2633E0828CB412AB ();
// 0x000009B9 System.String[] MaterialUI.VectorImageManager::GetAllIconSetNames()
extern void VectorImageManager_GetAllIconSetNames_m69F906F5577B03118E24CD387C3580CAB7558C4B ();
// 0x000009BA System.Boolean MaterialUI.VectorImageManager::IsMaterialDesignIconFont(System.String)
extern void VectorImageManager_IsMaterialDesignIconFont_m846EA0561CF94EC46D25D6CDE89B245980B60A2E ();
// 0x000009BB System.Boolean MaterialUI.VectorImageManager::IsMaterialUIIconFont(System.String)
extern void VectorImageManager_IsMaterialUIIconFont_mCC274AA72860D7D95E45B06A23536E461BA52F4A ();
// 0x000009BC UnityEngine.Font MaterialUI.VectorImageManager::GetIconFont(System.String)
extern void VectorImageManager_GetIconFont_mF1661A8EBF37E15FADCD951B4A7690704CC72682 ();
// 0x000009BD MaterialUI.VectorImageSet MaterialUI.VectorImageManager::GetIconSet(System.String)
extern void VectorImageManager_GetIconSet_m048F90EEE183139E7A845F2F690947D9A658159F ();
// 0x000009BE System.String MaterialUI.VectorImageManager::GetIconCodeFromName(System.String,System.String)
extern void VectorImageManager_GetIconCodeFromName_mFDEEB3408B9048D8F81A0D22D8D272CE5D507B21 ();
// 0x000009BF System.String MaterialUI.VectorImageManager::GetIconNameFromCode(System.String,System.String)
extern void VectorImageManager_GetIconNameFromCode_mFD6CF0E466CACBEE4EBA45D913DFC19968313EDD ();
// 0x000009C0 MaterialUI.DialogManager MaterialUI.DialogManager::get_instance()
extern void DialogManager_get_instance_m22ED7F0A01751ABE570608AE12FB61756C3DF78F ();
// 0x000009C1 UnityEngine.RectTransform MaterialUI.DialogManager::get_rectTransform()
extern void DialogManager_get_rectTransform_mDADFA4E6948231FA946C06AA11AC88958DE79B5B ();
// 0x000009C2 System.Void MaterialUI.DialogManager::Awake()
extern void DialogManager_Awake_mC85DD11AAE18C922891C2E98A45BB0654591992E ();
// 0x000009C3 System.Void MaterialUI.DialogManager::OnDestroy()
extern void DialogManager_OnDestroy_m781E83283D74E7199B766924D2A059419A3ADC46 ();
// 0x000009C4 System.Void MaterialUI.DialogManager::OnApplicationQuit()
extern void DialogManager_OnApplicationQuit_m606F0CDC2A82A41DF345BC3F0BB28A71183865BB ();
// 0x000009C5 System.Void MaterialUI.DialogManager::InitDialogSystem()
extern void DialogManager_InitDialogSystem_m21E369EBACA453F530B7E857591766E30252A515 ();
// 0x000009C6 MaterialUI.DialogAlert MaterialUI.DialogManager::ShowAlert(System.String,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowAlert_m2829121D289E78A92207395289B8F040E96FBA99 ();
// 0x000009C7 MaterialUI.DialogAlert MaterialUI.DialogManager::ShowAlert(System.String,System.Action,System.String,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowAlert_m23B6EDACE245E1DDA81179452C49C60B8BC341A1 ();
// 0x000009C8 MaterialUI.DialogAlert MaterialUI.DialogManager::ShowAlert(System.String,System.Action,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogManager_ShowAlert_mE4151C148D1295E7C62CFF96251B5BEC59C70C7F ();
// 0x000009C9 MaterialUI.DialogAlert MaterialUI.DialogManager::CreateAlert()
extern void DialogManager_CreateAlert_mA9763F548323FCD023772D166CA664544B5CC108 ();
// 0x000009CA MaterialUI.DialogProgress MaterialUI.DialogManager::ShowProgressLinear(System.String)
extern void DialogManager_ShowProgressLinear_m11CF02166B721B6DF2BD0771019F8C4A4CD4E145 ();
// 0x000009CB MaterialUI.DialogProgress MaterialUI.DialogManager::ShowProgressLinear(System.String,System.String,MaterialUI.ImageData,System.Boolean)
extern void DialogManager_ShowProgressLinear_mB48E62492BD838CE1F4C58D66265AAABE3017C79 ();
// 0x000009CC MaterialUI.DialogProgress MaterialUI.DialogManager::CreateProgressLinear()
extern void DialogManager_CreateProgressLinear_mBDF636C1BACA83AC4ECBE98762D041CE0A946F82 ();
// 0x000009CD MaterialUI.DialogProgress MaterialUI.DialogManager::ShowProgressCircular(System.String)
extern void DialogManager_ShowProgressCircular_m5292B379D23DD26B8371A71539BC9D1D1BC20490 ();
// 0x000009CE MaterialUI.DialogProgress MaterialUI.DialogManager::ShowProgressCircular(System.String,System.String,MaterialUI.ImageData,System.Boolean)
extern void DialogManager_ShowProgressCircular_mA6F92A6E42790FAFC5DDA893BFE956308AD35EBD ();
// 0x000009CF MaterialUI.DialogProgress MaterialUI.DialogManager::CreateProgressCircular()
extern void DialogManager_CreateProgressCircular_m6FEB1A22C04557F20328D3DA72F43BC297698BC0 ();
// 0x000009D0 MaterialUI.DialogSimpleList MaterialUI.DialogManager::ShowSimpleList(System.String[],System.Action`1<System.Int32>)
extern void DialogManager_ShowSimpleList_mA63F80EC91E05E2CFC62524BEAE114B59281814B ();
// 0x000009D1 MaterialUI.DialogSimpleList MaterialUI.DialogManager::ShowSimpleList(System.String[],System.Action`1<System.Int32>,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowSimpleList_mCFEF0E0CF95D94A62121980E4B25D3179CAB2C7C ();
// 0x000009D2 MaterialUI.DialogSimpleList MaterialUI.DialogManager::ShowSimpleList(MaterialUI.OptionDataList,System.Action`1<System.Int32>)
extern void DialogManager_ShowSimpleList_mD6B7804759858040B45387E49A4D54239825C95E ();
// 0x000009D3 MaterialUI.DialogSimpleList MaterialUI.DialogManager::ShowSimpleList(MaterialUI.OptionDataList,System.Action`1<System.Int32>,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowSimpleList_m717EAC54A8337787333AF80364E00CD9C7882B13 ();
// 0x000009D4 MaterialUI.DialogSimpleList MaterialUI.DialogManager::CreateSimpleList()
extern void DialogManager_CreateSimpleList_m9842FF60A17B38AB45DBC0D498F833D166910AC0 ();
// 0x000009D5 MaterialUI.DialogCheckboxList MaterialUI.DialogManager::ShowCheckboxList(System.String[],System.Action`1<System.Boolean[]>,System.String)
extern void DialogManager_ShowCheckboxList_m5B61D23A6FDF1E61171C4D27180E4A9CBAECB5C0 ();
// 0x000009D6 MaterialUI.DialogCheckboxList MaterialUI.DialogManager::ShowCheckboxList(System.String[],System.Action`1<System.Boolean[]>,System.String,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowCheckboxList_m7A06AA47DD966A444A0D8B8927A89626F4B7980A ();
// 0x000009D7 MaterialUI.DialogCheckboxList MaterialUI.DialogManager::ShowCheckboxList(System.String[],System.Action`1<System.Boolean[]>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogManager_ShowCheckboxList_m40DB453D26A379978503EA1C9B47505314D9ED4F ();
// 0x000009D8 MaterialUI.DialogCheckboxList MaterialUI.DialogManager::CreateCheckboxList()
extern void DialogManager_CreateCheckboxList_m2678974FFCEB77354FEA6C11A7FF201859C07A92 ();
// 0x000009D9 MaterialUI.DialogRadioList MaterialUI.DialogManager::ShowRadioList(System.String[],System.Action`1<System.Int32>,System.String)
extern void DialogManager_ShowRadioList_m73E13686EE5FDD0EFAF74914D389A136C3205C41 ();
// 0x000009DA MaterialUI.DialogRadioList MaterialUI.DialogManager::ShowRadioList(System.String[],System.Action`1<System.Int32>,System.String,System.Int32)
extern void DialogManager_ShowRadioList_m478FCC175862370C2A78DB97BA100DFAFA1E8AE1 ();
// 0x000009DB MaterialUI.DialogRadioList MaterialUI.DialogManager::ShowRadioList(System.String[],System.Action`1<System.Int32>,System.String,System.String,MaterialUI.ImageData)
extern void DialogManager_ShowRadioList_m5969ECF70E5AEBD7F8EA29FD953F6A577F234A23 ();
// 0x000009DC MaterialUI.DialogRadioList MaterialUI.DialogManager::ShowRadioList(System.String[],System.Action`1<System.Int32>,System.String,System.String,MaterialUI.ImageData,System.Int32)
extern void DialogManager_ShowRadioList_m81E72E44D27E7C476D7E5231CC63D7C114F6DFBF ();
// 0x000009DD MaterialUI.DialogRadioList MaterialUI.DialogManager::ShowRadioList(System.String[],System.Action`1<System.Int32>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String,System.Int32)
extern void DialogManager_ShowRadioList_m0520AC54309AFD5D385BAE9DAF415D741D72F9E4 ();
// 0x000009DE MaterialUI.DialogRadioList MaterialUI.DialogManager::CreateRadioList()
extern void DialogManager_CreateRadioList_m5DF2264908C94EAEB7FD138593E1D6830040050B ();
// 0x000009DF T MaterialUI.DialogManager::CreateCustomDialog(System.String)
// 0x000009E0 MaterialUI.DialogTimePicker MaterialUI.DialogManager::ShowTimePicker(System.DateTime,System.Action`1<System.DateTime>)
extern void DialogManager_ShowTimePicker_m935478769E5E541A1FE17D2852B34BF9F10E61BE ();
// 0x000009E1 MaterialUI.DialogTimePicker MaterialUI.DialogManager::ShowTimePicker(System.DateTime,System.Action`1<System.DateTime>,UnityEngine.Color)
extern void DialogManager_ShowTimePicker_m88B70FFAB385912BA010887F9E534834639D1FC0 ();
// 0x000009E2 MaterialUI.DialogDatePicker MaterialUI.DialogManager::ShowDatePicker(System.Int32,System.Int32,System.Int32,System.Action`1<System.DateTime>)
extern void DialogManager_ShowDatePicker_mF8B3FF5418B7F9893EA3BCA98C8F16C75C39E556 ();
// 0x000009E3 MaterialUI.DialogDatePicker MaterialUI.DialogManager::ShowDatePicker(System.Int32,System.Int32,System.Int32,System.Action`1<System.DateTime>,UnityEngine.Color)
extern void DialogManager_ShowDatePicker_m4135F30E11D7C74FBB108BE8FFD14E0C4C79ECE2 ();
// 0x000009E4 MaterialUI.DialogDatePicker MaterialUI.DialogManager::ShowDatePicker(System.Int32,System.Int32,System.Int32,System.Action`1<System.DateTime>,System.Action,UnityEngine.Color)
extern void DialogManager_ShowDatePicker_m0DC469804501CDC5DCC7A89454DA50038834D204 ();
// 0x000009E5 MaterialUI.DialogDatePicker MaterialUI.DialogManager::CreateDatePicker()
extern void DialogManager_CreateDatePicker_m595B917B30591F8AE3398CEBD30DDA43EDE2A224 ();
// 0x000009E6 MaterialUI.DialogPrompt MaterialUI.DialogManager::ShowPrompt(System.String,System.Action`1<System.String>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogManager_ShowPrompt_mE0ED6CD86B71305CA27C51DC6F6FEE4C13E2C07C ();
// 0x000009E7 MaterialUI.DialogPrompt MaterialUI.DialogManager::ShowPrompt(System.String,System.String,System.Action`2<System.String,System.String>,System.String,System.String,MaterialUI.ImageData,System.Action,System.String)
extern void DialogManager_ShowPrompt_m57C4FB44223F8DB78514085089971B797B3914E7 ();
// 0x000009E8 MaterialUI.DialogPrompt MaterialUI.DialogManager::CreatePrompt()
extern void DialogManager_CreatePrompt_m399C6705B363ED50D0B764347AEEB482FA1321E6 ();
// 0x000009E9 System.Void MaterialUI.DialogManager::.ctor()
extern void DialogManager__ctor_m83CBCDA1E6A70CE6B695F064C8DA6373323F3C0C ();
// 0x000009EA UnityEngine.GameObject MaterialUI.PrefabManager::GetGameObject(System.String)
extern void PrefabManager_GetGameObject_mC174023A3A249D32A39050333D4C38E4DA975205 ();
// 0x000009EB UnityEngine.GameObject MaterialUI.PrefabManager::InstantiateGameObject(System.String,UnityEngine.Transform)
extern void PrefabManager_InstantiateGameObject_m3D752D58512A2D0DD3AE11BF673F2F8B24A1114F ();
// 0x000009EC System.Void MaterialUI.PrefabManager::.cctor()
extern void PrefabManager__cctor_m7915A436FFDF70673395E6BF16AACFD2EF25F17C ();
// 0x000009ED MaterialUI.SnackbarManager MaterialUI.SnackbarManager::get_instance()
extern void SnackbarManager_get_instance_m1C28645751BA8CCE73D7F7891BF58D1EE1C24D87 ();
// 0x000009EE System.Void MaterialUI.SnackbarManager::Awake()
extern void SnackbarManager_Awake_m31F47D9B103B3AD8903AB9C94091E78FE5C223F0 ();
// 0x000009EF System.Void MaterialUI.SnackbarManager::InitSystem()
extern void SnackbarManager_InitSystem_m1485DE07CA0FA0D3594A9D71E2614826F63157BC ();
// 0x000009F0 System.Void MaterialUI.SnackbarManager::OnDestroy()
extern void SnackbarManager_OnDestroy_m97D8D653701E2851D851F66CC80C4B42AAD96444 ();
// 0x000009F1 System.Void MaterialUI.SnackbarManager::OnApplicationQuit()
extern void SnackbarManager_OnApplicationQuit_mD5E6BE460A9B15E3328AE716EA16F1BC343F037A ();
// 0x000009F2 System.Void MaterialUI.SnackbarManager::Show(System.String)
extern void SnackbarManager_Show_m5F28849C225070C506EDE516A721E5F83FE0687E ();
// 0x000009F3 System.Void MaterialUI.SnackbarManager::Show(System.String,System.String,System.Action)
extern void SnackbarManager_Show_m21AD84AD759E00B91ECA7AD94E46C1BA6AF657CB ();
// 0x000009F4 System.Void MaterialUI.SnackbarManager::Show(System.String,System.Single,System.String,System.Action)
extern void SnackbarManager_Show_m750E1DDFE6F9E9225A9E661E152A34E9916D1C0B ();
// 0x000009F5 System.Void MaterialUI.SnackbarManager::Show(System.String,System.Single,UnityEngine.Color,UnityEngine.Color,System.Int32,System.String,System.Action)
extern void SnackbarManager_Show_m533CB17677D274426361DFEFB95A6AADB2EDB433 ();
// 0x000009F6 System.Void MaterialUI.SnackbarManager::StartQueue()
extern void SnackbarManager_StartQueue_m327D02E6C87E96B35FA936B313429D90FCA3F820 ();
// 0x000009F7 System.Boolean MaterialUI.SnackbarManager::OnSnackbarCompleted()
extern void SnackbarManager_OnSnackbarCompleted_mE058A637AFBF7A7C1A253E4D82296F70E8611205 ();
// 0x000009F8 System.Void MaterialUI.SnackbarManager::.ctor()
extern void SnackbarManager__ctor_m278B4BC398CA439DE589750D88450D4F137FC58F ();
// 0x000009F9 MaterialUI.ToastManager MaterialUI.ToastManager::get_instance()
extern void ToastManager_get_instance_mC43DA8EC276470007603B6731535433B071044E6 ();
// 0x000009FA System.Void MaterialUI.ToastManager::Awake()
extern void ToastManager_Awake_m1B6CF9E9DCAFBE4C39533AEB830D6D0A17A4284E ();
// 0x000009FB System.Void MaterialUI.ToastManager::InitSystem()
extern void ToastManager_InitSystem_mABCD74DFC8931B93117234E7F2551FCF6C7AF27E ();
// 0x000009FC System.Void MaterialUI.ToastManager::OnDestroy()
extern void ToastManager_OnDestroy_mC3B7DDDE805D7BDB1F6A6205BE0AA7EFCBF264B7 ();
// 0x000009FD System.Void MaterialUI.ToastManager::OnApplicationQuit()
extern void ToastManager_OnApplicationQuit_m91B9773FCB5C3802745D894AD6191A4480E9BF20 ();
// 0x000009FE System.Void MaterialUI.ToastManager::Show(System.String,UnityEngine.Transform)
extern void ToastManager_Show_m39893BEDCA74BAAA061846AF9CF699991E02A5E3 ();
// 0x000009FF System.Void MaterialUI.ToastManager::Show(System.String,System.Single,UnityEngine.Transform)
extern void ToastManager_Show_m51A22CE667803D12627E244AF40C445EFA963003 ();
// 0x00000A00 System.Void MaterialUI.ToastManager::Show(System.String,System.Single,UnityEngine.Color,UnityEngine.Color,System.Int32,UnityEngine.Transform)
extern void ToastManager_Show_m303D0CDBBB576A7F1EE94375FC3CDDBA5E97A879 ();
// 0x00000A01 System.Void MaterialUI.ToastManager::StartQueue()
extern void ToastManager_StartQueue_m595D629590B1C550FC30DD772963EB96EC8C20D7 ();
// 0x00000A02 System.Boolean MaterialUI.ToastManager::OnToastCompleted()
extern void ToastManager_OnToastCompleted_m529657B7FDC4B4BF1CC0CF8BEEB596D46F328542 ();
// 0x00000A03 System.Void MaterialUI.ToastManager::SetCanvas(UnityEngine.Canvas)
extern void ToastManager_SetCanvas_m92C8B8640A8014E95A7047BB6D85F6D980EBFD73 ();
// 0x00000A04 System.Void MaterialUI.ToastManager::.ctor()
extern void ToastManager__ctor_mAE31DC367E893A31CAC02F1CB74919E30145CB25 ();
// 0x00000A05 MaterialUI.TweenManager MaterialUI.TweenManager::get_instance()
extern void TweenManager_get_instance_m9A152A1F20DF156747878FD98A199CA2BDE7DAF2 ();
// 0x00000A06 System.Int32 MaterialUI.TweenManager::get_totalTweenCount()
extern void TweenManager_get_totalTweenCount_m9CB02CC7CCCF147B13C45DA2A7782A5A86817640 ();
// 0x00000A07 System.Int32 MaterialUI.TweenManager::get_activeTweenCount()
extern void TweenManager_get_activeTweenCount_mB4280A182522F9E76D0A61AEC7B756950AB7F354 ();
// 0x00000A08 System.Int32 MaterialUI.TweenManager::get_dormantTweenCount()
extern void TweenManager_get_dormantTweenCount_m9069304026F9F77A3491AEFF45D2BD03736CA996 ();
// 0x00000A09 System.Void MaterialUI.TweenManager::Update()
extern void TweenManager_Update_mD3BD66C4B074527EC9640062DA9403B8F1491CB5 ();
// 0x00000A0A System.Void MaterialUI.TweenManager::Release(MaterialUI.AutoTween)
extern void TweenManager_Release_m1F2695AC40D9F706C184D63A70EE58BDF4730892 ();
// 0x00000A0B System.Boolean MaterialUI.TweenManager::TweenIsActive(System.Int32)
extern void TweenManager_TweenIsActive_m0BA0CE7E131F1DC8BCC44978F5E0A65A20F95BB8 ();
// 0x00000A0C MaterialUI.AutoTween MaterialUI.TweenManager::GetAutoTween(System.Int32)
extern void TweenManager_GetAutoTween_mA81DFB7079BB217CD92BE998543A1A2DF3871863 ();
// 0x00000A0D System.Void MaterialUI.TweenManager::EndTween(System.Int32,System.Boolean)
extern void TweenManager_EndTween_m20AC8289C4096080AA5CBF7CA22F7BBC849E0D4D ();
// 0x00000A0E System.Int32 MaterialUI.TweenManager::TweenValue(System.Action`1<T>,T,T,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
// 0x00000A0F System.Int32 MaterialUI.TweenManager::TweenValue(System.Action`1<T>,System.Func`1<T>,T,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
// 0x00000A10 System.Int32 MaterialUI.TweenManager::TweenValue(System.Action`1<T>,System.Func`1<T>,System.Func`1<T>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
// 0x00000A11 System.Int32 MaterialUI.TweenManager::TweenTweenValueCustom(System.Action`1<T>,T,T,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
// 0x00000A12 System.Int32 MaterialUI.TweenManager::TweenValueCustom(System.Action`1<T>,System.Func`1<T>,T,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
// 0x00000A13 System.Int32 MaterialUI.TweenManager::TweenValueCustom(System.Action`1<T>,System.Func`1<T>,System.Func`1<T>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
// 0x00000A14 System.Int32 MaterialUI.TweenManager::TimedCallback(System.Single,System.Action)
extern void TweenManager_TimedCallback_mB541383FE665EF815BA9F53A7EF7D62FA1CC3B4E ();
// 0x00000A15 System.Int32 MaterialUI.TweenManager::TweenFloat(System.Action`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenFloat_m744720F392448C4BDC8CE03C7A79CF349C73234C ();
// 0x00000A16 System.Int32 MaterialUI.TweenManager::TweenFloat(System.Action`1<System.Single>,System.Func`1<System.Single>,System.Single,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenFloat_m9B007ED204004C43BA4F3A720971050556FD75E3 ();
// 0x00000A17 System.Int32 MaterialUI.TweenManager::TweenFloat(System.Action`1<System.Single>,System.Func`1<System.Single>,System.Func`1<System.Single>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenFloat_m2B9C33285BEFF96BF956A2EE76D89E446270445A ();
// 0x00000A18 System.Int32 MaterialUI.TweenManager::TweenFloatCustom(System.Action`1<System.Single>,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenFloatCustom_m88A39A8536E732814AA1E2325B8433E61CC5D330 ();
// 0x00000A19 System.Int32 MaterialUI.TweenManager::TweenFloatCustom(System.Action`1<System.Single>,System.Func`1<System.Single>,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenFloatCustom_m4ABB23405A0CD4F140E6564D6B928FCA7EE3B599 ();
// 0x00000A1A System.Int32 MaterialUI.TweenManager::TweenFloatCustom(System.Action`1<System.Single>,System.Func`1<System.Single>,System.Func`1<System.Single>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenFloatCustom_m5E07C267F0571CE1B03D459687DA50D5CD7812DB ();
// 0x00000A1B System.Int32 MaterialUI.TweenManager::TweenInt(System.Action`1<System.Int32>,System.Int32,System.Int32,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenInt_mE32E0068F383CD490B0DC06CEBAF757E28BC3054 ();
// 0x00000A1C System.Int32 MaterialUI.TweenManager::TweenInt(System.Action`1<System.Int32>,System.Func`1<System.Int32>,System.Int32,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenInt_m9D25F498E087B63BA35480956510A0F2C3D476FA ();
// 0x00000A1D System.Int32 MaterialUI.TweenManager::TweenInt(System.Action`1<System.Int32>,System.Func`1<System.Int32>,System.Func`1<System.Int32>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenInt_mD6B444B938FB0774D94883922F093F818B4BB1A1 ();
// 0x00000A1E System.Int32 MaterialUI.TweenManager::TweenIntCustom(System.Action`1<System.Int32>,System.Int32,System.Int32,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenIntCustom_m51A7D57D60EB2E5AC432CC6E0E95124BBB210D49 ();
// 0x00000A1F System.Int32 MaterialUI.TweenManager::TweenIntCustom(System.Action`1<System.Int32>,System.Func`1<System.Int32>,System.Int32,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenIntCustom_m7106C9D654DE7835028AAC6EE758AECBEEB4902F ();
// 0x00000A20 System.Int32 MaterialUI.TweenManager::TweenIntCustom(System.Action`1<System.Int32>,System.Func`1<System.Int32>,System.Func`1<System.Int32>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenIntCustom_mD01C5F0C7F2BEA422669A56A60F002E57DAFAD3D ();
// 0x00000A21 System.Int32 MaterialUI.TweenManager::TweenVector2(System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector2_m02EC0A8881E671F79B8401E037100F84720AE07B ();
// 0x00000A22 System.Int32 MaterialUI.TweenManager::TweenVector2(System.Action`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector2_m0ADCB30ADAF136F98EF10719E446414988B8D893 ();
// 0x00000A23 System.Int32 MaterialUI.TweenManager::TweenVector2(System.Action`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector2_m7AC80E782CF3CC8145C07049614601066A83E576 ();
// 0x00000A24 System.Int32 MaterialUI.TweenManager::TweenVector2Custom(System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector2Custom_m8B6B7E0DE223C9033950B6497ECD76BA23248BC2 ();
// 0x00000A25 System.Int32 MaterialUI.TweenManager::TweenVector2Custom(System.Action`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector2Custom_m2019BBE13640FD218551FA3BB20A430F8C4495DE ();
// 0x00000A26 System.Int32 MaterialUI.TweenManager::TweenVector2Custom(System.Action`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Func`1<UnityEngine.Vector2>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector2Custom_mA1F1CDAC17E81E8FB15ADAEFE6236815AAD67DD3 ();
// 0x00000A27 System.Int32 MaterialUI.TweenManager::TweenVector3(System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector3_m4315F197A1ADD1ADFEE398F2C7C3C5E870D7136E ();
// 0x00000A28 System.Int32 MaterialUI.TweenManager::TweenVector3(System.Action`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector3_m38451C5A3A522A56B65C6A27EA9A47129F2353EA ();
// 0x00000A29 System.Int32 MaterialUI.TweenManager::TweenVector3(System.Action`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector3_mC487EB784CB701227D4CA7B63475E2BDC4FC0FDD ();
// 0x00000A2A System.Int32 MaterialUI.TweenManager::TweenVector3Custom(System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector3Custom_m37C5D0C68E09F4EDD93505FB25E46C038129E861 ();
// 0x00000A2B System.Int32 MaterialUI.TweenManager::TweenVector3Custom(System.Action`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector3Custom_m047DF0EE2B84D60E7321E52015BE762C702DA44C ();
// 0x00000A2C System.Int32 MaterialUI.TweenManager::TweenVector3Custom(System.Action`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Func`1<UnityEngine.Vector3>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector3Custom_mDC8FEB45BDCC6118DCD88D5557C190AE909B1FCE ();
// 0x00000A2D System.Int32 MaterialUI.TweenManager::TweenVector4(System.Action`1<UnityEngine.Vector4>,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector4_mC1D01A0A744E7A5D40DA697D5B992B7C5DD927D4 ();
// 0x00000A2E System.Int32 MaterialUI.TweenManager::TweenVector4(System.Action`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,UnityEngine.Vector4,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector4_mD0670D086735CD0801A03CBBBA794A13FB678C14 ();
// 0x00000A2F System.Int32 MaterialUI.TweenManager::TweenVector4(System.Action`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenVector4_m44B74C219D06EAC08F466739099D9E6328D5B9E5 ();
// 0x00000A30 System.Int32 MaterialUI.TweenManager::TweenVector4Custom(System.Action`1<UnityEngine.Vector4>,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector4Custom_m035E503686E0E9EE57E031BC3BDEE3FB55FD73F8 ();
// 0x00000A31 System.Int32 MaterialUI.TweenManager::TweenVector4Custom(System.Action`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,UnityEngine.Vector4,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector4Custom_mC40DA5DABAE59B13393171741BAD6CA5387A96F2 ();
// 0x00000A32 System.Int32 MaterialUI.TweenManager::TweenVector4Custom(System.Action`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Func`1<UnityEngine.Vector4>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenVector4Custom_m79B26099141901B996A541C63E186C08035BBF98 ();
// 0x00000A33 System.Int32 MaterialUI.TweenManager::TweenColor(System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenColor_m4EC58BA73468504FA55F0B4DF9932150CC147E84 ();
// 0x00000A34 System.Int32 MaterialUI.TweenManager::TweenColor(System.Action`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,UnityEngine.Color,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenColor_m30E4A7A9B9E703687677F16E4D3992850019D9BD ();
// 0x00000A35 System.Int32 MaterialUI.TweenManager::TweenColor(System.Action`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Single,System.Single,System.Action,System.Boolean,MaterialUI.Tween_TweenType)
extern void TweenManager_TweenColor_mB0CE851306D1B6B2D904E3D07F203F1D8E640AAB ();
// 0x00000A36 System.Int32 MaterialUI.TweenManager::TweenColorCustom(System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenColorCustom_m92565E6AA5925EDE0227DD21D3B8C6E485217594 ();
// 0x00000A37 System.Int32 MaterialUI.TweenManager::TweenColorCustom(System.Action`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,UnityEngine.Color,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenColorCustom_mB054D6306EEF0DCAB19FE642A97991A1CC37203A ();
// 0x00000A38 System.Int32 MaterialUI.TweenManager::TweenColorCustom(System.Action`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Func`1<UnityEngine.Color>,System.Single,UnityEngine.AnimationCurve,System.Single,System.Action,System.Boolean)
extern void TweenManager_TweenColorCustom_mB639D5D8D84221742814917293ABEB36A12F9B46 ();
// 0x00000A39 System.Void MaterialUI.TweenManager::.ctor()
extern void TweenManager__ctor_mAF7C25C4540A9388A5659936D768232DA2269140 ();
// 0x00000A3A System.Void MaterialUI.AdaptiveUIGrid::OnEnable()
extern void AdaptiveUIGrid_OnEnable_m1274B5D3FC6819F94D2B5C7B96F98A6FEC4B9E72 ();
// 0x00000A3B System.Void MaterialUI.AdaptiveUIGrid::GenerateGutterTexture()
extern void AdaptiveUIGrid_GenerateGutterTexture_mAB3F215F5F758DD364408012F9A2706B9E1B8B91 ();
// 0x00000A3C System.Void MaterialUI.AdaptiveUIGrid::OnDrawGizmos()
extern void AdaptiveUIGrid_OnDrawGizmos_mE6366281F527267F23D7E40D1EB686A6489994EE ();
// 0x00000A3D System.Void MaterialUI.AdaptiveUIGrid::OnGUI()
extern void AdaptiveUIGrid_OnGUI_m22AF7E148EB16A27008F7784E1EB3BEA06CB4A56 ();
// 0x00000A3E System.Void MaterialUI.AdaptiveUIGrid::DrawGrid(System.Boolean)
extern void AdaptiveUIGrid_DrawGrid_m6BC378B6262A4540B5BD55AD20831A66B521A316 ();
// 0x00000A3F System.Void MaterialUI.AdaptiveUIGrid::DrawTexture(UnityEngine.Rect,System.Boolean)
extern void AdaptiveUIGrid_DrawTexture_mB074BCFB4F5D7559B04E168DFA98B484E0DD8BBF ();
// 0x00000A40 System.Void MaterialUI.AdaptiveUIGrid::.ctor()
extern void AdaptiveUIGrid__ctor_m848FC68562546EA1AE77B9F71EA860936BDECBB9 ();
// 0x00000A41 T MaterialUI.FuncExtension::InvokeIfNotNull(System.Func`1<T>)
// 0x00000A42 System.Void MaterialUI.TransformExtension::SetParentAndScale(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,System.Boolean)
extern void TransformExtension_SetParentAndScale_m0E90D7E926614408BDBFCBD00F263EC6934ED91B ();
// 0x00000A43 UnityEngine.Canvas MaterialUI.TransformExtension::GetRootCanvas(UnityEngine.Transform)
extern void TransformExtension_GetRootCanvas_mC59DE68EF3C0A27C97FF2BC93ED27F5E7CA78B99 ();
// 0x00000A44 UnityEngine.Canvas MaterialUI.CanvasExtension::Copy(UnityEngine.Canvas,UnityEngine.GameObject)
extern void CanvasExtension_Copy_mCB5E802C66834AA3AB3378ED8E3A51FF7B8D5484 ();
// 0x00000A45 System.Void MaterialUI.CanvasExtension::CopySettingsToOtherCanvas(UnityEngine.Canvas,UnityEngine.Canvas)
extern void CanvasExtension_CopySettingsToOtherCanvas_m51D55283246266D95D2E954CA7EA8907ED900795 ();
// 0x00000A46 System.Void MaterialUI.ActionExtension::InvokeIfNotNull(System.Action)
extern void ActionExtension_InvokeIfNotNull_mC655B3DD6C373D3ED7F6D9E923545C3B3837F00E ();
// 0x00000A47 System.Void MaterialUI.ActionExtension::InvokeIfNotNull(System.Action`1<T>,T)
// 0x00000A48 System.Void MaterialUI.UnityEventExtension::InvokeIfNotNull(UnityEngine.Events.UnityEvent)
extern void UnityEventExtension_InvokeIfNotNull_m02B03D6CBB3849775E75617F0546BF081DF659D8 ();
// 0x00000A49 System.Void MaterialUI.UnityEventExtension::InvokeIfNotNull(UnityEngine.Events.UnityEvent`1<T>,T)
// 0x00000A4A T MaterialUI.GameObjectExtension::GetAddComponent(UnityEngine.GameObject)
// 0x00000A4B T MaterialUI.GameObjectExtension::GetChildByName(UnityEngine.GameObject,System.String)
// 0x00000A4C T MaterialUI.MonoBehaviourExtension::GetAddComponent(UnityEngine.MonoBehaviour)
// 0x00000A4D T MaterialUI.MonoBehaviourExtension::GetChildByName(UnityEngine.MonoBehaviour,System.String)
// 0x00000A4E T MaterialUI.ComponentExtension::GetChildByName(UnityEngine.Component,System.String)
// 0x00000A4F UnityEngine.Color MaterialUI.ColorExtension::WithAlpha(UnityEngine.Color,System.Single)
extern void ColorExtension_WithAlpha_m9A1BB3387063A83CD2B74A668A54EAA666D514D4 ();
// 0x00000A50 System.Boolean MaterialUI.ColorExtension::Approximately(UnityEngine.Color,UnityEngine.Color,System.Boolean)
extern void ColorExtension_Approximately_mB20B67B8D7520676264FC987B10AD0E78D8F3AA5 ();
// 0x00000A51 UnityEngine.Vector2 MaterialUI.RectTransformExtension::GetProperSize(UnityEngine.RectTransform)
extern void RectTransformExtension_GetProperSize_m30C93C3C7BE6E504FDB9DDEF2C097250D380CCF2 ();
// 0x00000A52 UnityEngine.Vector3 MaterialUI.RectTransformExtension::GetPositionRegardlessOfPivot(UnityEngine.RectTransform)
extern void RectTransformExtension_GetPositionRegardlessOfPivot_m6890137D5D55BA8E20567F96350DA9FBB367E332 ();
// 0x00000A53 UnityEngine.Vector3 MaterialUI.RectTransformExtension::GetLocalPositionRegardlessOfPivot(UnityEngine.RectTransform)
extern void RectTransformExtension_GetLocalPositionRegardlessOfPivot_mEA111DE1457830D2B209B0E9ED013FA2483429AC ();
// 0x00000A54 System.Void MaterialUI.RectTransformExtension::SetPositionRegardlessOfPivot(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtension_SetPositionRegardlessOfPivot_mF6A232F9C6981A7DCFFB5057AA6A728C7726ECC4 ();
// 0x00000A55 System.Void MaterialUI.RectTransformExtension::SetLocalPositionRegardlessOfPivot(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtension_SetLocalPositionRegardlessOfPivot_m8E19A703C4F6742768A53C26B73E1457815B9C8B ();
// 0x00000A56 System.Void MaterialUI.RectTransformExtension::SetAnchorX(UnityEngine.RectTransform,System.Single,System.Single)
extern void RectTransformExtension_SetAnchorX_mE90F60D541391DC605A7B9F02706E905AFAAFE87 ();
// 0x00000A57 System.Void MaterialUI.RectTransformExtension::SetAnchorY(UnityEngine.RectTransform,System.Single,System.Single)
extern void RectTransformExtension_SetAnchorY_mF2D505605C4E66FC122BD2027CEF73DDC5731399 ();
// 0x00000A58 System.Boolean MaterialUI.GraphicExtension::IsSpriteOrVectorImage(UnityEngine.UI.Graphic)
extern void GraphicExtension_IsSpriteOrVectorImage_mEBF55B77068CAD11EFCD66789A339F8DB4E63458 ();
// 0x00000A59 System.Void MaterialUI.GraphicExtension::SetImage(UnityEngine.UI.Graphic,UnityEngine.Sprite)
extern void GraphicExtension_SetImage_mD6F432644C5B0B6DA6073407A236715DB7D403B6 ();
// 0x00000A5A System.Void MaterialUI.GraphicExtension::SetImage(UnityEngine.UI.Graphic,MaterialUI.VectorImageData)
extern void GraphicExtension_SetImage_m611A2F3D4A72A88B74B7591E6456317D8E77EE30 ();
// 0x00000A5B System.Void MaterialUI.GraphicExtension::SetImage(UnityEngine.UI.Graphic,MaterialUI.ImageData)
extern void GraphicExtension_SetImage_m13582C55F3DE6C1D552FB741A3E4E72406E574C8 ();
// 0x00000A5C UnityEngine.Sprite MaterialUI.GraphicExtension::GetSpriteImage(UnityEngine.UI.Graphic)
extern void GraphicExtension_GetSpriteImage_m9163D20201F6B1FBB01861BA5C7142DD8BC2E134 ();
// 0x00000A5D MaterialUI.VectorImageData MaterialUI.GraphicExtension::GetVectorImage(UnityEngine.UI.Graphic)
extern void GraphicExtension_GetVectorImage_mA29743B009A6BE5BC5DB2D6FA768F30BCBFA7D6B ();
// 0x00000A5E MaterialUI.ImageData MaterialUI.GraphicExtension::GetImageData(UnityEngine.UI.Graphic)
extern void GraphicExtension_GetImageData_m6FE79402EBA4D3D7B70A76FD72C2160F5EE63D41 ();
// 0x00000A5F System.Single MaterialUI.FPSCounter::get_updateInterval()
extern void FPSCounter_get_updateInterval_mE89AA3AFF6A42E34E1859FFF7B28497AACAA89AC ();
// 0x00000A60 System.Void MaterialUI.FPSCounter::set_updateInterval(System.Single)
extern void FPSCounter_set_updateInterval_m65437A1F890446AF771664FDB04E56296CB5372F ();
// 0x00000A61 UnityEngine.UI.Text MaterialUI.FPSCounter::get_text()
extern void FPSCounter_get_text_mFDF23AAF363D27649BCF70AE0386FF07E7044A60 ();
// 0x00000A62 System.Void MaterialUI.FPSCounter::set_text(UnityEngine.UI.Text)
extern void FPSCounter_set_text_m8A016248AAB9C92CB05EE1F1E65C3299EB65EEB4 ();
// 0x00000A63 System.Void MaterialUI.FPSCounter::Start()
extern void FPSCounter_Start_m912D9D3FBAF682073418FFBAD0AEB95B2007F46D ();
// 0x00000A64 System.Void MaterialUI.FPSCounter::Update()
extern void FPSCounter_Update_m34039710F4F1BD7CAD483868D1AF75E758217464 ();
// 0x00000A65 System.Void MaterialUI.FPSCounter::.ctor()
extern void FPSCounter__ctor_m709BC45C9391B25E97ECA17F009D475157419FD7 ();
// 0x00000A66 System.Void MaterialUI.HSBColor::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void HSBColor__ctor_m772835AA5BD96FF559F6019FBB5C8E4CB9286AB1_AdjustorThunk ();
// 0x00000A67 System.Void MaterialUI.HSBColor::.ctor(System.Single,System.Single,System.Single)
extern void HSBColor__ctor_m976D67139060126C84DA81EA8C830A4CBD158E37_AdjustorThunk ();
// 0x00000A68 System.Void MaterialUI.HSBColor::.ctor(UnityEngine.Color)
extern void HSBColor__ctor_m2A5BB5E6CCE9F8FA86C7BBC1FB379C0E09BAF854_AdjustorThunk ();
// 0x00000A69 System.Single MaterialUI.HSBColor::get_h()
extern void HSBColor_get_h_m33B7AB6E834B2D7B8E0A0A5E9929C00B4BF468F7_AdjustorThunk ();
// 0x00000A6A System.Void MaterialUI.HSBColor::set_h(System.Single)
extern void HSBColor_set_h_mFBE97BE339ACFFA77E657CE3B53DEF7AE42B1815_AdjustorThunk ();
// 0x00000A6B System.Single MaterialUI.HSBColor::get_s()
extern void HSBColor_get_s_m53DA357D05660713B04F089D48BD6D49FBDB527A_AdjustorThunk ();
// 0x00000A6C System.Void MaterialUI.HSBColor::set_s(System.Single)
extern void HSBColor_set_s_mDD7480DDD5DFC4C9D9A8A4F550127742E9FBF8E2_AdjustorThunk ();
// 0x00000A6D System.Single MaterialUI.HSBColor::get_b()
extern void HSBColor_get_b_m4C70FCFCC2587E4A05B499E8B8A915F2B10973E0_AdjustorThunk ();
// 0x00000A6E System.Void MaterialUI.HSBColor::set_b(System.Single)
extern void HSBColor_set_b_m588E9E39867FC93E6333A50759D0CF16A63E0B3D_AdjustorThunk ();
// 0x00000A6F System.Single MaterialUI.HSBColor::get_a()
extern void HSBColor_get_a_m25C5F3835222CB3B4B9A0CE548D81133FA5E4F67_AdjustorThunk ();
// 0x00000A70 System.Void MaterialUI.HSBColor::set_a(System.Single)
extern void HSBColor_set_a_m6051236D225BFD7ED3CA7C7AAC649B92E68AE096_AdjustorThunk ();
// 0x00000A71 MaterialUI.HSBColor MaterialUI.HSBColor::FromColor(UnityEngine.Color)
extern void HSBColor_FromColor_mBCDCE686EC0884660A71148D69C7CE8EEF53B3DC ();
// 0x00000A72 UnityEngine.Color MaterialUI.HSBColor::ToColor(MaterialUI.HSBColor)
extern void HSBColor_ToColor_m0D24B6C1262E9E4DD0F50F07F09D9A044735020C ();
// 0x00000A73 UnityEngine.Color MaterialUI.HSBColor::ToColor()
extern void HSBColor_ToColor_mCC905901A4D1116304060F3F3A0A3BA82C863C38_AdjustorThunk ();
// 0x00000A74 System.String MaterialUI.HSBColor::ToString()
extern void HSBColor_ToString_mFB2FCA5179D3683853B9EBDA16A07CBB809BC382_AdjustorThunk ();
// 0x00000A75 MaterialUI.HSBColor MaterialUI.HSBColor::Lerp(MaterialUI.HSBColor,MaterialUI.HSBColor,System.Single)
extern void HSBColor_Lerp_m4DC6E58BF94A442B956F856BE230579CAF8CB3FE ();
// 0x00000A76 System.Void MaterialUI.BaseTextValidator::Init(MaterialUI.MaterialInputField)
extern void BaseTextValidator_Init_m601DB1ACAF56B3074C2E5A11508778D53AB447EE ();
// 0x00000A77 System.Void MaterialUI.BaseTextValidator::.ctor()
extern void BaseTextValidator__ctor_m2DF8A7FE53ABA3464F20F1BC624445438BDB9E34 ();
// 0x00000A78 System.Void MaterialUI.EmptyTextValidator::.ctor(System.String)
extern void EmptyTextValidator__ctor_m7FD0CB3A0250F7EC5C77721E06FBEFB194992F55 ();
// 0x00000A79 System.Boolean MaterialUI.EmptyTextValidator::IsTextValid()
extern void EmptyTextValidator_IsTextValid_m25E9C03911DB607E2BFEE1D46290009125FF4006 ();
// 0x00000A7A System.Void MaterialUI.EmailTextValidator::.ctor(System.String)
extern void EmailTextValidator__ctor_m057A098D1035CAE5DE214A3B738709B4CB86044C ();
// 0x00000A7B System.Boolean MaterialUI.EmailTextValidator::IsTextValid()
extern void EmailTextValidator_IsTextValid_m01DC9082E6B99FEDC9CCCE6956ED35D53A1A4872 ();
// 0x00000A7C System.Void MaterialUI.NameTextValidator::.ctor(System.String)
extern void NameTextValidator__ctor_m5E2E64F58A8521815462BB126EEE988158D3F962 ();
// 0x00000A7D System.Void MaterialUI.NameTextValidator::.ctor(System.Int32,System.String)
extern void NameTextValidator__ctor_m2749FC10ACBBA4AF162657527CF12984EAC68EA7 ();
// 0x00000A7E System.Boolean MaterialUI.NameTextValidator::IsTextValid()
extern void NameTextValidator_IsTextValid_m023C4FBDBB17EE6900BDE1A2932721154712FF52 ();
// 0x00000A7F System.Void MaterialUI.MinLengthTextValidator::.ctor(System.Int32,System.String)
extern void MinLengthTextValidator__ctor_m7E3AFC87545FF4F8D94C9BF9AE15E4CAA109414A ();
// 0x00000A80 System.Boolean MaterialUI.MinLengthTextValidator::IsTextValid()
extern void MinLengthTextValidator_IsTextValid_mB5D4DD99254B3F58B8769D500053FBDBC2D01A5C ();
// 0x00000A81 System.Void MaterialUI.MaxLengthTextValidator::.ctor(System.Int32,System.String)
extern void MaxLengthTextValidator__ctor_m2EB16AD4733BB40820FE5B99BC24B68D3AB7CC27 ();
// 0x00000A82 System.Boolean MaterialUI.MaxLengthTextValidator::IsTextValid()
extern void MaxLengthTextValidator_IsTextValid_m358B3DF61484DE7348AC1CC983958B7777C827EC ();
// 0x00000A83 System.Void MaterialUI.ExactLengthTextValidator::.ctor(System.Int32,System.String)
extern void ExactLengthTextValidator__ctor_m846F2C99D47C1FB52A39068617FA604F7396DB36 ();
// 0x00000A84 System.Boolean MaterialUI.ExactLengthTextValidator::IsTextValid()
extern void ExactLengthTextValidator_IsTextValid_mE36FF8DEAA07ECF6CDE769AE434329944B409C81 ();
// 0x00000A85 System.Void MaterialUI.SamePasswordTextValidator::.ctor(UnityEngine.UI.InputField,System.String)
extern void SamePasswordTextValidator__ctor_m743CA8EC5D68B8435172D7649D1AC824BFCB5CC5 ();
// 0x00000A86 System.Boolean MaterialUI.SamePasswordTextValidator::IsTextValid()
extern void SamePasswordTextValidator_IsTextValid_mD5388AC31FCE886FA5DAFCD70E14C5BEC5464F23 ();
// 0x00000A87 System.Void MaterialUI.DirectoryExistTextValidator::.ctor(System.String)
extern void DirectoryExistTextValidator__ctor_mAE2642639568EF5E4AB2AE6F6BBC73BAB2A8115E ();
// 0x00000A88 System.Boolean MaterialUI.DirectoryExistTextValidator::IsTextValid()
extern void DirectoryExistTextValidator_IsTextValid_m23AB3899784DC71BF49A6DC705C534211EE806E4 ();
// 0x00000A89 System.Void MaterialUI.FileExistTextValidator::.ctor(System.String)
extern void FileExistTextValidator__ctor_m05DC492B9CAD42DF9231B23921B7D75AA91C3CB2 ();
// 0x00000A8A System.Boolean MaterialUI.FileExistTextValidator::IsTextValid()
extern void FileExistTextValidator_IsTextValid_mD9F288687B716DFAD8A9F87557F23E7116D40D1C ();
// 0x00000A8B System.Void MaterialUI.ButtonRectInstantiationHelper::HelpInstantiate(System.Int32[])
extern void ButtonRectInstantiationHelper_HelpInstantiate_mBA32856DF39D65E79F412D836FE5D80E50535220 ();
// 0x00000A8C System.Void MaterialUI.ButtonRectInstantiationHelper::.ctor()
extern void ButtonRectInstantiationHelper__ctor_m3BBCDD1C6F1EA87C0FD917D3B3BE6A82C6C56A23 ();
// 0x00000A8D System.Void MaterialUI.ButtonRoundInstantiationHelper::HelpInstantiate(System.Int32[])
extern void ButtonRoundInstantiationHelper_HelpInstantiate_m1156A7EE075B7851B974A27CA095634CB013070B ();
// 0x00000A8E System.Void MaterialUI.ButtonRoundInstantiationHelper::.ctor()
extern void ButtonRoundInstantiationHelper__ctor_mBC333F2EC9A2E7A342FA7909725A797D6504F7FF ();
// 0x00000A8F System.Void MaterialUI.CircleProgressIndicatorInstantiationHelper::HelpInstantiate(System.Int32[])
extern void CircleProgressIndicatorInstantiationHelper_HelpInstantiate_m1F5D1BC244DA5F7E6A2FFED75E9D02E37D8A7DFA ();
// 0x00000A90 System.Void MaterialUI.CircleProgressIndicatorInstantiationHelper::.ctor()
extern void CircleProgressIndicatorInstantiationHelper__ctor_m4C4D53CC3604A7DE4AE6EA6E78FEB22EA1630558 ();
// 0x00000A91 System.Void MaterialUI.DividerInstantiationHelper::HelpInstantiate(System.Int32[])
extern void DividerInstantiationHelper_HelpInstantiate_m37B29EEE9FE1252D4ADFB786876DFBABEEFEBC4E ();
// 0x00000A92 System.Void MaterialUI.DividerInstantiationHelper::.ctor()
extern void DividerInstantiationHelper__ctor_m39C51410301BFA7642CD0517CBA3E3C97E202766 ();
// 0x00000A93 System.Void MaterialUI.EmptyUIObjectInstantiationHelper::HelpInstantiate(System.Int32[])
extern void EmptyUIObjectInstantiationHelper_HelpInstantiate_m04A15C9619F4A8BFCBD22C14C4852832EDD6DA6E ();
// 0x00000A94 System.Void MaterialUI.EmptyUIObjectInstantiationHelper::.ctor()
extern void EmptyUIObjectInstantiationHelper__ctor_m1248609778688DC36FB58DDA46E55230F62D2D50 ();
// 0x00000A95 System.Void MaterialUI.InputFieldInstantiationHelper::HelpInstantiate(System.Int32[])
extern void InputFieldInstantiationHelper_HelpInstantiate_mD88ABF56B1B849BB9EFF257B34DDCDC6E0806429 ();
// 0x00000A96 System.Void MaterialUI.InputFieldInstantiationHelper::.ctor()
extern void InputFieldInstantiationHelper__ctor_mE73C7253748EEABD715A556C582C3F120DE705B2 ();
// 0x00000A97 System.Void MaterialUI.InstantiationHelper::HelpInstantiate(System.Int32[])
extern void InstantiationHelper_HelpInstantiate_m23A58367913DC3CE8CC1120511D16C953C7B14FB ();
// 0x00000A98 System.Void MaterialUI.InstantiationHelper::.ctor()
extern void InstantiationHelper__ctor_m2D3753FD7A095F02A7498C163E22814E814C8A67 ();
// 0x00000A99 System.Void MaterialUI.MaterialSliderInstantiationHelper::HelpInstantiate(System.Int32[])
extern void MaterialSliderInstantiationHelper_HelpInstantiate_mCD0FF2CE291BB2AE41F19D0022C44296007C5526 ();
// 0x00000A9A System.Void MaterialUI.MaterialSliderInstantiationHelper::.ctor()
extern void MaterialSliderInstantiationHelper__ctor_m29A8CCE5A4C2C9DBFF3E99F99517BBFA2D4D4A16 ();
// 0x00000A9B System.Void MaterialUI.PanelInstantiationHelper::HelpInstantiate(System.Int32[])
extern void PanelInstantiationHelper_HelpInstantiate_mC248C7EC6C33690069FDC5B43F38F899ECC5E26C ();
// 0x00000A9C System.Void MaterialUI.PanelInstantiationHelper::.ctor()
extern void PanelInstantiationHelper__ctor_mD022181B71E5BE9C1E9AA19D592E5C77C9AEBC28 ();
// 0x00000A9D System.Void MaterialUI.RadioInstantiationHelper::HelpInstantiate(System.Int32[])
extern void RadioInstantiationHelper_HelpInstantiate_mD07414F26B281B93BFDC2E9F5D6488868C05B29F ();
// 0x00000A9E System.Void MaterialUI.RadioInstantiationHelper::.ctor()
extern void RadioInstantiationHelper__ctor_m852B28210C695BC3B34A6E450601D96A1E540E8B ();
// 0x00000A9F System.Void MaterialUI.TabViewInstantiationHelper::HelpInstantiate(System.Int32[])
extern void TabViewInstantiationHelper_HelpInstantiate_m2584BA502A26A08DCB262B643A89CB65AC110B27 ();
// 0x00000AA0 System.Void MaterialUI.TabViewInstantiationHelper::.ctor()
extern void TabViewInstantiationHelper__ctor_m5EC7D4C89DB8F1AFD9CE655D29C0FAD19120DF4F ();
// 0x00000AA1 System.Void MaterialUI.ToggleInstantiationHelper::HelpInstantiate(System.Int32[])
extern void ToggleInstantiationHelper_HelpInstantiate_mB55FD3BCEEE9734E979D75592A09073766BE8295 ();
// 0x00000AA2 System.Void MaterialUI.ToggleInstantiationHelper::.ctor()
extern void ToggleInstantiationHelper__ctor_m795E52372D8C4E5AE28C7C678F171D5A734BE44B ();
// 0x00000AA3 System.String MaterialUI.MaterialColor::ColorToHex(UnityEngine.Color32)
extern void MaterialColor_ColorToHex_mD323C0E6F28D8A29AEA9E779A789926AF702BFA7 ();
// 0x00000AA4 UnityEngine.Color MaterialUI.MaterialColor::HexToColor(System.String,System.Single)
extern void MaterialColor_HexToColor_mEFC850CD0BC92CC61BB3B83C45AE06455442B26E ();
// 0x00000AA5 UnityEngine.Color MaterialUI.MaterialColor::HighlightColor(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void MaterialColor_HighlightColor_m80D7E786BC97914D7FB6A5C89F1FD14247867FAF ();
// 0x00000AA6 System.Boolean MaterialUI.MaterialColor::LightOrDarkElements(UnityEngine.Color)
extern void MaterialColor_LightOrDarkElements_m78B209D5C38FAAC439E0523F2FEEC59B7A14CFF7 ();
// 0x00000AA7 System.Single MaterialUI.MaterialColor::ColorDistance(UnityEngine.Color,UnityEngine.Color)
extern void MaterialColor_ColorDistance_m9BD3BEB242D9E6F112D5A6F722D77FF228C26232 ();
// 0x00000AA8 UnityEngine.Color MaterialUI.MaterialColor::ClosestMaterialColor(UnityEngine.Color)
extern void MaterialColor_ClosestMaterialColor_m7D2BF24B2A2CABC5E3E34D81795680A90F61A96D ();
// 0x00000AA9 UnityEngine.Color[] MaterialUI.MaterialColor::ClosestMaterialColorSet(UnityEngine.Color)
extern void MaterialColor_ClosestMaterialColorSet_m4A9B54917ED7332935C6C121BCDFCA2FACA5F199 ();
// 0x00000AAA UnityEngine.Color MaterialUI.MaterialColor::OffsetShade(UnityEngine.Color,System.Int32,UnityEngine.Color[])
extern void MaterialColor_OffsetShade_m33344B87553368932CB1CC25BA2ABFFA501482B7 ();
// 0x00000AAB UnityEngine.Color MaterialUI.MaterialColor::Random500(System.Boolean)
extern void MaterialColor_Random500_m065D6112F963670392819D590EA019BD965C00AA ();
// 0x00000AAC UnityEngine.Color[] MaterialUI.MaterialColor::get_colorSets()
extern void MaterialColor_get_colorSets_mFEC6A736AB44A8229AD487FCA463B90A6429C695 ();
// 0x00000AAD UnityEngine.Color[] MaterialUI.MaterialColor::get_redSet()
extern void MaterialColor_get_redSet_m4FBA4EE4FD2B30D45A6FA0990054E79D4D94E70C ();
// 0x00000AAE UnityEngine.Color MaterialUI.MaterialColor::get_red50()
extern void MaterialColor_get_red50_mC1F3A822F07BE9A224B45BD7311B470AC2E14172 ();
// 0x00000AAF UnityEngine.Color MaterialUI.MaterialColor::get_red100()
extern void MaterialColor_get_red100_m5A902F6CE379D8BF3CDA90ADE9B0D2535F6835A0 ();
// 0x00000AB0 UnityEngine.Color MaterialUI.MaterialColor::get_red200()
extern void MaterialColor_get_red200_m63D4B31459FD1B7B8E6F018BBAFA9A3B5CFEA74A ();
// 0x00000AB1 UnityEngine.Color MaterialUI.MaterialColor::get_red300()
extern void MaterialColor_get_red300_m2DA30DB6A9CEB819E16CD57D9C9A8FA0DDAA445E ();
// 0x00000AB2 UnityEngine.Color MaterialUI.MaterialColor::get_red400()
extern void MaterialColor_get_red400_mC2181312D6A036C50BCC113B4EB79DF06D571A4E ();
// 0x00000AB3 UnityEngine.Color MaterialUI.MaterialColor::get_red500()
extern void MaterialColor_get_red500_m4C6122F2225CD4C2C6669E0AC5FDED522527C874 ();
// 0x00000AB4 UnityEngine.Color MaterialUI.MaterialColor::get_red600()
extern void MaterialColor_get_red600_m05CDADC3FE0CDB7CD6E58CED857B128475E22EE9 ();
// 0x00000AB5 UnityEngine.Color MaterialUI.MaterialColor::get_red700()
extern void MaterialColor_get_red700_m0B39F82EE49F36CAE12EEFD898094507EA7E141C ();
// 0x00000AB6 UnityEngine.Color MaterialUI.MaterialColor::get_red800()
extern void MaterialColor_get_red800_m3BAAD19403134CD7957710BAD0F05C3200F8C167 ();
// 0x00000AB7 UnityEngine.Color MaterialUI.MaterialColor::get_red900()
extern void MaterialColor_get_red900_m8DC522AE45240385DF8DB0C761C874B56E3D015D ();
// 0x00000AB8 UnityEngine.Color MaterialUI.MaterialColor::get_redA100()
extern void MaterialColor_get_redA100_m2070DBF783ABA6A2424B5705F0AF18BC0A796A58 ();
// 0x00000AB9 UnityEngine.Color MaterialUI.MaterialColor::get_redA200()
extern void MaterialColor_get_redA200_m79A85B96434BFA9AFA99633B8BC18A700EFD87E0 ();
// 0x00000ABA UnityEngine.Color MaterialUI.MaterialColor::get_redA400()
extern void MaterialColor_get_redA400_m126B6E80B12C9BFDF3606EE77F3B00F11ED8226A ();
// 0x00000ABB UnityEngine.Color MaterialUI.MaterialColor::get_redA700()
extern void MaterialColor_get_redA700_mADF55A59EDED941BFB3BE77702EE5B4F123425E5 ();
// 0x00000ABC UnityEngine.Color[] MaterialUI.MaterialColor::get_pinkSet()
extern void MaterialColor_get_pinkSet_mABC51C7B6C981D987F4D50E3D9590D9AA5D55962 ();
// 0x00000ABD UnityEngine.Color MaterialUI.MaterialColor::get_pink50()
extern void MaterialColor_get_pink50_mEEE568642CD3620D88AB21DA0D0223035A106FE9 ();
// 0x00000ABE UnityEngine.Color MaterialUI.MaterialColor::get_pink100()
extern void MaterialColor_get_pink100_m0D22BCD015FEA9CF1BC5DE91A3359824CEAE0EE9 ();
// 0x00000ABF UnityEngine.Color MaterialUI.MaterialColor::get_pink200()
extern void MaterialColor_get_pink200_m3E7237C90DD804390F2802F265E9E1F57787F3CC ();
// 0x00000AC0 UnityEngine.Color MaterialUI.MaterialColor::get_pink300()
extern void MaterialColor_get_pink300_mB0A7C9DCBA61CC94BFBF9850206DACDD862BB4F7 ();
// 0x00000AC1 UnityEngine.Color MaterialUI.MaterialColor::get_pink400()
extern void MaterialColor_get_pink400_mEFBE6F67583C1C8061F76D82C6DCADB633AF979B ();
// 0x00000AC2 UnityEngine.Color MaterialUI.MaterialColor::get_pink500()
extern void MaterialColor_get_pink500_mE5FABEDF9DB73250AF6D893D94460A5FA7CE4844 ();
// 0x00000AC3 UnityEngine.Color MaterialUI.MaterialColor::get_pink600()
extern void MaterialColor_get_pink600_mD55E447DAFE3A91ACE24FBEC86C5C4FF432ABD81 ();
// 0x00000AC4 UnityEngine.Color MaterialUI.MaterialColor::get_pink700()
extern void MaterialColor_get_pink700_m64774BFADD166CB304A24BBF33CC73BBFF4828E6 ();
// 0x00000AC5 UnityEngine.Color MaterialUI.MaterialColor::get_pink800()
extern void MaterialColor_get_pink800_m7539A468C120EF99319F82C6B3F3661C1AD942A4 ();
// 0x00000AC6 UnityEngine.Color MaterialUI.MaterialColor::get_pink900()
extern void MaterialColor_get_pink900_m5BF5D30F93FEAAE751DCA850CD70B9F1B3942503 ();
// 0x00000AC7 UnityEngine.Color MaterialUI.MaterialColor::get_pinkA100()
extern void MaterialColor_get_pinkA100_mF07F1125F624F27DC9D4BCFCF136B8C8461E9619 ();
// 0x00000AC8 UnityEngine.Color MaterialUI.MaterialColor::get_pinkA200()
extern void MaterialColor_get_pinkA200_m12E15292B94739E1C4EC91EA6421EA245CD2BA0B ();
// 0x00000AC9 UnityEngine.Color MaterialUI.MaterialColor::get_pinkA400()
extern void MaterialColor_get_pinkA400_m3C915B5E21A0CCD2518D6735F7936D75DD32740C ();
// 0x00000ACA UnityEngine.Color MaterialUI.MaterialColor::get_pinkA700()
extern void MaterialColor_get_pinkA700_m02C62D9DF5369CF20812F60C4FBC6BAF7A9D7648 ();
// 0x00000ACB UnityEngine.Color[] MaterialUI.MaterialColor::get_purpleSet()
extern void MaterialColor_get_purpleSet_mC6B7B4FAF82AC29555909B3835E3287C5C251B02 ();
// 0x00000ACC UnityEngine.Color MaterialUI.MaterialColor::get_purple50()
extern void MaterialColor_get_purple50_mEBDC8DA27C472D63F93ED8F22BA73A631CDB010F ();
// 0x00000ACD UnityEngine.Color MaterialUI.MaterialColor::get_purple100()
extern void MaterialColor_get_purple100_m0D011D6656571A8BD5B0BA7FB9F903C82F3C6944 ();
// 0x00000ACE UnityEngine.Color MaterialUI.MaterialColor::get_purple200()
extern void MaterialColor_get_purple200_m96AEA5FD6C4AA68269E75012E721A30A49B29588 ();
// 0x00000ACF UnityEngine.Color MaterialUI.MaterialColor::get_purple300()
extern void MaterialColor_get_purple300_m9AE3218E492453574C23CBBC785D5F3DCEFB1A0C ();
// 0x00000AD0 UnityEngine.Color MaterialUI.MaterialColor::get_purple400()
extern void MaterialColor_get_purple400_mB62DBD1DF2A2173DE866D872D6739C1CDCD19A66 ();
// 0x00000AD1 UnityEngine.Color MaterialUI.MaterialColor::get_purple500()
extern void MaterialColor_get_purple500_mBE59AF1CF61372072579F959F08869666B76DBCA ();
// 0x00000AD2 UnityEngine.Color MaterialUI.MaterialColor::get_purple600()
extern void MaterialColor_get_purple600_m38B2963DC2B00ACE3810191983CFA64BAF454D82 ();
// 0x00000AD3 UnityEngine.Color MaterialUI.MaterialColor::get_purple700()
extern void MaterialColor_get_purple700_m4505B4A1BC82372DA8918B01CB1A15F65E11735F ();
// 0x00000AD4 UnityEngine.Color MaterialUI.MaterialColor::get_purple800()
extern void MaterialColor_get_purple800_mD7B5B49F21D005C4AEEC2E8485765ECDA33C59D8 ();
// 0x00000AD5 UnityEngine.Color MaterialUI.MaterialColor::get_purple900()
extern void MaterialColor_get_purple900_mC4115C73F59667764062054AB0E34169E25945B9 ();
// 0x00000AD6 UnityEngine.Color MaterialUI.MaterialColor::get_purpleA100()
extern void MaterialColor_get_purpleA100_m76FD783DE8646CE0384FC4B12287D91EAD84F66A ();
// 0x00000AD7 UnityEngine.Color MaterialUI.MaterialColor::get_purpleA200()
extern void MaterialColor_get_purpleA200_mAF0E0341045E54E276E3D55F109B256984388B7F ();
// 0x00000AD8 UnityEngine.Color MaterialUI.MaterialColor::get_purpleA400()
extern void MaterialColor_get_purpleA400_mFF1A154B6DB3BE8E2EE6843FD2FFE0E94BEFC46B ();
// 0x00000AD9 UnityEngine.Color MaterialUI.MaterialColor::get_purpleA700()
extern void MaterialColor_get_purpleA700_m42C7E6C1C19D833419B2A11AB4966C348379CB0F ();
// 0x00000ADA UnityEngine.Color[] MaterialUI.MaterialColor::get_deepPurpleSet()
extern void MaterialColor_get_deepPurpleSet_m4EDF675DDEBF2F61F4B377D74815EF76EE330A76 ();
// 0x00000ADB UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple50()
extern void MaterialColor_get_deepPurple50_mCE604EEE0942409EAD60AAB560A29A02C55D633B ();
// 0x00000ADC UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple100()
extern void MaterialColor_get_deepPurple100_m8F54C61BE0F1F9A5075660F7B575599AE37E688C ();
// 0x00000ADD UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple200()
extern void MaterialColor_get_deepPurple200_mFD540B3D3F801354CCCC78D2496FB8762C36ABA8 ();
// 0x00000ADE UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple300()
extern void MaterialColor_get_deepPurple300_mAE14F120C9DD5D74107C3BBA3DE982D2C2D97164 ();
// 0x00000ADF UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple400()
extern void MaterialColor_get_deepPurple400_m4DE43AA68E64C30EFD9FEF4FF2390AF28C284CC5 ();
// 0x00000AE0 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple500()
extern void MaterialColor_get_deepPurple500_m0EA54BDA447A405025F7CD145276CECB06592858 ();
// 0x00000AE1 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple600()
extern void MaterialColor_get_deepPurple600_mC20234C64814AFBB69AD654E03127720C30CC74B ();
// 0x00000AE2 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple700()
extern void MaterialColor_get_deepPurple700_mBA7B909A9220A01B3215234EA18B37A9DD9DE056 ();
// 0x00000AE3 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple800()
extern void MaterialColor_get_deepPurple800_mA0C14ED4D2FFE4AF36C8BC646FF06E370763ECE1 ();
// 0x00000AE4 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurple900()
extern void MaterialColor_get_deepPurple900_m3AAAF826F8EB4446CB4B818A38001FD3FD5A2311 ();
// 0x00000AE5 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurpleA100()
extern void MaterialColor_get_deepPurpleA100_mFE8D39A80E715F3396D49F2D095DB0FCCBB3A1B8 ();
// 0x00000AE6 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurpleA200()
extern void MaterialColor_get_deepPurpleA200_mD6E4F1BBCDB3ED8CC35225A9A946F1FA4B60878F ();
// 0x00000AE7 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurpleA400()
extern void MaterialColor_get_deepPurpleA400_mEC4C107494926F39432FF1EB39802E75F347D22C ();
// 0x00000AE8 UnityEngine.Color MaterialUI.MaterialColor::get_deepPurpleA700()
extern void MaterialColor_get_deepPurpleA700_m39ACB1C61B0D700FC7FE9ABB86A83EA1F3DD6835 ();
// 0x00000AE9 UnityEngine.Color[] MaterialUI.MaterialColor::get_indigoSet()
extern void MaterialColor_get_indigoSet_m778817FE00A889574C97CED25ECA076103E15C21 ();
// 0x00000AEA UnityEngine.Color MaterialUI.MaterialColor::get_indigo50()
extern void MaterialColor_get_indigo50_m30E1A0AD1E777573693249EB46A206654E46B0CF ();
// 0x00000AEB UnityEngine.Color MaterialUI.MaterialColor::get_indigo100()
extern void MaterialColor_get_indigo100_mE53BF0A8884D500F889CC948B24135401755F377 ();
// 0x00000AEC UnityEngine.Color MaterialUI.MaterialColor::get_indigo200()
extern void MaterialColor_get_indigo200_mCB084E39A0CDF16492B8DDC13CE9128F0EA717EC ();
// 0x00000AED UnityEngine.Color MaterialUI.MaterialColor::get_indigo300()
extern void MaterialColor_get_indigo300_mA09FB010F478DA6F31BF93802B68D7BD1CA1F31E ();
// 0x00000AEE UnityEngine.Color MaterialUI.MaterialColor::get_indigo400()
extern void MaterialColor_get_indigo400_m45FBD9349D613200A3C637497FB2061D2E85571F ();
// 0x00000AEF UnityEngine.Color MaterialUI.MaterialColor::get_indigo500()
extern void MaterialColor_get_indigo500_m8AF9072DFCC9588359BD8BE85420429CCFEBE510 ();
// 0x00000AF0 UnityEngine.Color MaterialUI.MaterialColor::get_indigo600()
extern void MaterialColor_get_indigo600_m4ED0ACB258B9EC713AEF370AFF1581C7724681ED ();
// 0x00000AF1 UnityEngine.Color MaterialUI.MaterialColor::get_indigo700()
extern void MaterialColor_get_indigo700_m0F9D42BFEAD6E34F8BFE31C68B886A9A374FA6D7 ();
// 0x00000AF2 UnityEngine.Color MaterialUI.MaterialColor::get_indigo800()
extern void MaterialColor_get_indigo800_m533476A558D73BFB0D77741D3B53162E22A9236D ();
// 0x00000AF3 UnityEngine.Color MaterialUI.MaterialColor::get_indigo900()
extern void MaterialColor_get_indigo900_m7497C4729CFD91FF040FC36CCE900EB64EDF7060 ();
// 0x00000AF4 UnityEngine.Color MaterialUI.MaterialColor::get_indigoA100()
extern void MaterialColor_get_indigoA100_m2973710490C19DF288E798BDC8421EA38712AFD7 ();
// 0x00000AF5 UnityEngine.Color MaterialUI.MaterialColor::get_indigoA200()
extern void MaterialColor_get_indigoA200_m6B66B10EDD1CB874D4A27C56670FAF4CD469D411 ();
// 0x00000AF6 UnityEngine.Color MaterialUI.MaterialColor::get_indigoA400()
extern void MaterialColor_get_indigoA400_mD7D2C96A14266F2D11AC71B41BF8875AB65A6D9E ();
// 0x00000AF7 UnityEngine.Color MaterialUI.MaterialColor::get_indigoA700()
extern void MaterialColor_get_indigoA700_m844ECE88DE125EEFF9DA8D1FC6C600B0246C3491 ();
// 0x00000AF8 UnityEngine.Color[] MaterialUI.MaterialColor::get_blueSet()
extern void MaterialColor_get_blueSet_mDEB923A9CC934A11126BB2FC92DC78F0FEA16456 ();
// 0x00000AF9 UnityEngine.Color MaterialUI.MaterialColor::get_blue50()
extern void MaterialColor_get_blue50_m3EFA34B523B6D4FF418A4B33C3DE58A7A867CABA ();
// 0x00000AFA UnityEngine.Color MaterialUI.MaterialColor::get_blue100()
extern void MaterialColor_get_blue100_mF000256D400575720F78FBE18D367DD97B35A49A ();
// 0x00000AFB UnityEngine.Color MaterialUI.MaterialColor::get_blue200()
extern void MaterialColor_get_blue200_m487930DE4CFAC387BF6620413CF30CBB1558EE06 ();
// 0x00000AFC UnityEngine.Color MaterialUI.MaterialColor::get_blue300()
extern void MaterialColor_get_blue300_m39850FAA6017D01F8D9DE096183FB3B170A08C8C ();
// 0x00000AFD UnityEngine.Color MaterialUI.MaterialColor::get_blue400()
extern void MaterialColor_get_blue400_mD549033B7178D2B9E917AA73A09C729FE38C563A ();
// 0x00000AFE UnityEngine.Color MaterialUI.MaterialColor::get_blue500()
extern void MaterialColor_get_blue500_m7B36BD820D5DE617F1FA1EC4B7C20979D31A977E ();
// 0x00000AFF UnityEngine.Color MaterialUI.MaterialColor::get_blue600()
extern void MaterialColor_get_blue600_m081858F60A782C6D97E036F5FC04099570D048E1 ();
// 0x00000B00 UnityEngine.Color MaterialUI.MaterialColor::get_blue700()
extern void MaterialColor_get_blue700_m92D1F79CD3C3C29E14F08A6245743E479679838E ();
// 0x00000B01 UnityEngine.Color MaterialUI.MaterialColor::get_blue800()
extern void MaterialColor_get_blue800_mECB5BFB4BDEFB883880537EBAC3619A058E31594 ();
// 0x00000B02 UnityEngine.Color MaterialUI.MaterialColor::get_blue900()
extern void MaterialColor_get_blue900_mFBF14DEB4F8AC051FF39D6EC62DFF3E10D243632 ();
// 0x00000B03 UnityEngine.Color MaterialUI.MaterialColor::get_blueA100()
extern void MaterialColor_get_blueA100_m96C00ED1931E421B9E40DAC7ADC78A50AB1288F0 ();
// 0x00000B04 UnityEngine.Color MaterialUI.MaterialColor::get_blueA200()
extern void MaterialColor_get_blueA200_m4E2C56995B3BD58299FA0B0F314F3E9A3C4358FE ();
// 0x00000B05 UnityEngine.Color MaterialUI.MaterialColor::get_blueA400()
extern void MaterialColor_get_blueA400_m76C3F5615927C06008D53B98FDBC8DC880B07F07 ();
// 0x00000B06 UnityEngine.Color MaterialUI.MaterialColor::get_blueA700()
extern void MaterialColor_get_blueA700_mBA6CFDC470697853CD80ECFA45F215473056373D ();
// 0x00000B07 UnityEngine.Color[] MaterialUI.MaterialColor::get_lightBlueSet()
extern void MaterialColor_get_lightBlueSet_mE5A9AE8CCC82790784E7B0BB67DF24A575238E16 ();
// 0x00000B08 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue50()
extern void MaterialColor_get_lightBlue50_m2A1C047619EC7E47019AF210AE4359400A2EB2B6 ();
// 0x00000B09 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue100()
extern void MaterialColor_get_lightBlue100_mEC7111ECC649EB51A73EF216018556DD348861B2 ();
// 0x00000B0A UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue200()
extern void MaterialColor_get_lightBlue200_m935A78268591EC2CEA3445A599CDB5231B48F3B1 ();
// 0x00000B0B UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue300()
extern void MaterialColor_get_lightBlue300_m219C3F7508C394701AA5EADFA0993D660365583E ();
// 0x00000B0C UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue400()
extern void MaterialColor_get_lightBlue400_mC4BF862C492F7E162A9583E5018A5F9DB3844CB4 ();
// 0x00000B0D UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue500()
extern void MaterialColor_get_lightBlue500_m6812A06A2B128E3A19355521D1EDD64F2F045BDB ();
// 0x00000B0E UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue600()
extern void MaterialColor_get_lightBlue600_m1CD706409E090D225BB16396240425F7BD6153B1 ();
// 0x00000B0F UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue700()
extern void MaterialColor_get_lightBlue700_mB3AFDF626EB2AE98E9208BBA7C377D8DC42CF39B ();
// 0x00000B10 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue800()
extern void MaterialColor_get_lightBlue800_m7909E760A3150FB4CD6E5257379666501895D10A ();
// 0x00000B11 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlue900()
extern void MaterialColor_get_lightBlue900_m989946385603C91C4E20A2469A5F04147BC0444C ();
// 0x00000B12 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlueA100()
extern void MaterialColor_get_lightBlueA100_m06EE54795DD6175564182330C470D55FF36D8E00 ();
// 0x00000B13 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlueA200()
extern void MaterialColor_get_lightBlueA200_mCAAEBA8C4E2815C73F866E6AFF41F3BEC3A8C0CA ();
// 0x00000B14 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlueA400()
extern void MaterialColor_get_lightBlueA400_m52F7F7C0F28FA29C9C77F65E2FA24EFF4762FCED ();
// 0x00000B15 UnityEngine.Color MaterialUI.MaterialColor::get_lightBlueA700()
extern void MaterialColor_get_lightBlueA700_mE137E14C9CDC9C4DFA27488CE8E6B4E37D6DE284 ();
// 0x00000B16 UnityEngine.Color[] MaterialUI.MaterialColor::get_cyanSet()
extern void MaterialColor_get_cyanSet_mE82879D96F961D7FD1CB32B7C3757F438A687A7D ();
// 0x00000B17 UnityEngine.Color MaterialUI.MaterialColor::get_cyan50()
extern void MaterialColor_get_cyan50_mB949CC1F7F89DDB82D6521CCC9685F4D9A1AF775 ();
// 0x00000B18 UnityEngine.Color MaterialUI.MaterialColor::get_cyan100()
extern void MaterialColor_get_cyan100_m1DE4C2C948FF35F1582C22CBE0306480AE60E665 ();
// 0x00000B19 UnityEngine.Color MaterialUI.MaterialColor::get_cyan200()
extern void MaterialColor_get_cyan200_m6B2251465184C11ACE05AC63DD2D88C9D61CF97D ();
// 0x00000B1A UnityEngine.Color MaterialUI.MaterialColor::get_cyan300()
extern void MaterialColor_get_cyan300_mF5E13ABDA9747DA6F9D8A3ED21612BDE91793B51 ();
// 0x00000B1B UnityEngine.Color MaterialUI.MaterialColor::get_cyan400()
extern void MaterialColor_get_cyan400_mBBDC816895B33DE9D76AE15E0E0B070DFEC81169 ();
// 0x00000B1C UnityEngine.Color MaterialUI.MaterialColor::get_cyan500()
extern void MaterialColor_get_cyan500_m95E1261D8E7620DB4C12B4E531A034954E276BFE ();
// 0x00000B1D UnityEngine.Color MaterialUI.MaterialColor::get_cyan600()
extern void MaterialColor_get_cyan600_mE72132DBA7810A265C23447D33DE06F2ECDE6F1D ();
// 0x00000B1E UnityEngine.Color MaterialUI.MaterialColor::get_cyan700()
extern void MaterialColor_get_cyan700_m34F3E27F8286FA75C17F7A7B1BC1B3BA120BCFB7 ();
// 0x00000B1F UnityEngine.Color MaterialUI.MaterialColor::get_cyan800()
extern void MaterialColor_get_cyan800_mB90C903853113FECF3BA5AA8AB4F678E485F02B7 ();
// 0x00000B20 UnityEngine.Color MaterialUI.MaterialColor::get_cyan900()
extern void MaterialColor_get_cyan900_mB1643997366965BE2A3EF4CFAA84439DD21B18E7 ();
// 0x00000B21 UnityEngine.Color MaterialUI.MaterialColor::get_cyanA100()
extern void MaterialColor_get_cyanA100_m6DB11574BF70230633F05E3AA153598207EF04C5 ();
// 0x00000B22 UnityEngine.Color MaterialUI.MaterialColor::get_cyanA200()
extern void MaterialColor_get_cyanA200_m80FA3F29CC8DDA217BA47E6F56C4F6C23A8F38E8 ();
// 0x00000B23 UnityEngine.Color MaterialUI.MaterialColor::get_cyanA400()
extern void MaterialColor_get_cyanA400_mD2C650C9D2050671C2618B6DAC516A81C65BA9ED ();
// 0x00000B24 UnityEngine.Color MaterialUI.MaterialColor::get_cyanA700()
extern void MaterialColor_get_cyanA700_m376C2EEFD9A9B7B17ABF16AFD77345FDD77A18B4 ();
// 0x00000B25 UnityEngine.Color[] MaterialUI.MaterialColor::get_tealSet()
extern void MaterialColor_get_tealSet_m455D6725636AB2DE35BCC4A4606B5CC95C5D5DBF ();
// 0x00000B26 UnityEngine.Color MaterialUI.MaterialColor::get_teal50()
extern void MaterialColor_get_teal50_m26DE4526E9F96EDACA843D0A94F8CE3126AE7394 ();
// 0x00000B27 UnityEngine.Color MaterialUI.MaterialColor::get_teal100()
extern void MaterialColor_get_teal100_mA6F3C82F21FD29F43257650660A6622055E0134D ();
// 0x00000B28 UnityEngine.Color MaterialUI.MaterialColor::get_teal200()
extern void MaterialColor_get_teal200_mB3C62175E36F5321D06962BE3FB1B0B893C3C201 ();
// 0x00000B29 UnityEngine.Color MaterialUI.MaterialColor::get_teal300()
extern void MaterialColor_get_teal300_m4B4DC2DB654C2092834A45CE045CFD083F1DE2D2 ();
// 0x00000B2A UnityEngine.Color MaterialUI.MaterialColor::get_teal400()
extern void MaterialColor_get_teal400_mF582883C1AE2EE40CE49D86AFB84AC31720061C1 ();
// 0x00000B2B UnityEngine.Color MaterialUI.MaterialColor::get_teal500()
extern void MaterialColor_get_teal500_mFB2EA7078E56DF7B8A0F9C91699B5B5A1493334C ();
// 0x00000B2C UnityEngine.Color MaterialUI.MaterialColor::get_teal600()
extern void MaterialColor_get_teal600_m54CB988684954F62104F5B0264639C8D4241CF51 ();
// 0x00000B2D UnityEngine.Color MaterialUI.MaterialColor::get_teal700()
extern void MaterialColor_get_teal700_mBF03658D91AFFA71ECE40B0C6C997EE8E3B051FD ();
// 0x00000B2E UnityEngine.Color MaterialUI.MaterialColor::get_teal800()
extern void MaterialColor_get_teal800_m0F9D822F8B0BF6DF06283A24E5D8767C08D66A05 ();
// 0x00000B2F UnityEngine.Color MaterialUI.MaterialColor::get_teal900()
extern void MaterialColor_get_teal900_mA9F9168FF0A04172169C7A92C026C38A9B3DA6D3 ();
// 0x00000B30 UnityEngine.Color MaterialUI.MaterialColor::get_tealA100()
extern void MaterialColor_get_tealA100_mE7D5AC541A997B4EB07FE12A2E93E8C718E56D8F ();
// 0x00000B31 UnityEngine.Color MaterialUI.MaterialColor::get_tealA200()
extern void MaterialColor_get_tealA200_m0F9F8F6CFFFC420014A8F99B96CF3133CC59C6B7 ();
// 0x00000B32 UnityEngine.Color MaterialUI.MaterialColor::get_tealA400()
extern void MaterialColor_get_tealA400_m0906872E245C6E397963E1F71F50997DB2A66D7D ();
// 0x00000B33 UnityEngine.Color MaterialUI.MaterialColor::get_tealA700()
extern void MaterialColor_get_tealA700_mBB4F2199C147D77C1D13AF5B3A671D7E241C0248 ();
// 0x00000B34 UnityEngine.Color[] MaterialUI.MaterialColor::get_greenSet()
extern void MaterialColor_get_greenSet_m8EDC5182EC5111ACEEF5B108C6581C02FD55F8BE ();
// 0x00000B35 UnityEngine.Color MaterialUI.MaterialColor::get_green50()
extern void MaterialColor_get_green50_m911BAC8F5826EE311807BE5273240F266D5C4C8E ();
// 0x00000B36 UnityEngine.Color MaterialUI.MaterialColor::get_green100()
extern void MaterialColor_get_green100_m92102FE960DC60BC9D63DCC8C72261A0A93FE3C1 ();
// 0x00000B37 UnityEngine.Color MaterialUI.MaterialColor::get_green200()
extern void MaterialColor_get_green200_m3F9ADAA188FD7D2303A8C249CA352FC40C7B8F27 ();
// 0x00000B38 UnityEngine.Color MaterialUI.MaterialColor::get_green300()
extern void MaterialColor_get_green300_m314AE96B35BB75F63929AF1FE783708B1DE332B2 ();
// 0x00000B39 UnityEngine.Color MaterialUI.MaterialColor::get_green400()
extern void MaterialColor_get_green400_m1B261146F6D3818B9C16CB65CAA8DDDBF41A037D ();
// 0x00000B3A UnityEngine.Color MaterialUI.MaterialColor::get_green500()
extern void MaterialColor_get_green500_mD4EDB14B7D253057BD35B21791565251BD211433 ();
// 0x00000B3B UnityEngine.Color MaterialUI.MaterialColor::get_green600()
extern void MaterialColor_get_green600_m028972BE9342A397716BFBA0D04B6CC99E04CE20 ();
// 0x00000B3C UnityEngine.Color MaterialUI.MaterialColor::get_green700()
extern void MaterialColor_get_green700_mF1524F14175AED207BA9E5DCAC159002E8382BC5 ();
// 0x00000B3D UnityEngine.Color MaterialUI.MaterialColor::get_green800()
extern void MaterialColor_get_green800_mD63002889401B4B17463B41967424AEFC106705E ();
// 0x00000B3E UnityEngine.Color MaterialUI.MaterialColor::get_green900()
extern void MaterialColor_get_green900_m1CE69E6FD779FE7639CE651A877B8B07C1B1C570 ();
// 0x00000B3F UnityEngine.Color MaterialUI.MaterialColor::get_greenA100()
extern void MaterialColor_get_greenA100_mA640F9AC43B84F6E24D18065753745590976D24E ();
// 0x00000B40 UnityEngine.Color MaterialUI.MaterialColor::get_greenA200()
extern void MaterialColor_get_greenA200_m7344617CE54AB497D57DEFE9FC21A84D1841FB78 ();
// 0x00000B41 UnityEngine.Color MaterialUI.MaterialColor::get_greenA400()
extern void MaterialColor_get_greenA400_mC768CBF00C0ABE237FC82C90EE11C0EFE962F876 ();
// 0x00000B42 UnityEngine.Color MaterialUI.MaterialColor::get_greenA700()
extern void MaterialColor_get_greenA700_m977F5CDC1A82F7A838E1EC456CF472B1CF3AA8C0 ();
// 0x00000B43 UnityEngine.Color[] MaterialUI.MaterialColor::get_lightGreenSet()
extern void MaterialColor_get_lightGreenSet_m46BC95BB5C3BA3944EC6ADC61F592387BE169BC7 ();
// 0x00000B44 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen50()
extern void MaterialColor_get_lightGreen50_mEEAB7334DA1446DBC4CC28BE5B3E37FF3BE7D8DB ();
// 0x00000B45 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen100()
extern void MaterialColor_get_lightGreen100_m538BAF8F6AB5AB0735C0D7149E3693AF77F18CEF ();
// 0x00000B46 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen200()
extern void MaterialColor_get_lightGreen200_m7B780A6D199028C3E7BAAB66A741D0874177B27D ();
// 0x00000B47 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen300()
extern void MaterialColor_get_lightGreen300_m89199F853548EF2EF8F0290B9E56E8606A96A0AC ();
// 0x00000B48 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen400()
extern void MaterialColor_get_lightGreen400_m35ED5523D2F3077DF05F560C458F3F1F1E2C2AFA ();
// 0x00000B49 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen500()
extern void MaterialColor_get_lightGreen500_mAFB10CA5E7028AB093F8A599E23A1429C1775156 ();
// 0x00000B4A UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen600()
extern void MaterialColor_get_lightGreen600_mF55EA343FD7E3A29A591DEA577FA702C81F47EDE ();
// 0x00000B4B UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen700()
extern void MaterialColor_get_lightGreen700_mBF13C128FAB03F2A66BBC15D04529D365C837F13 ();
// 0x00000B4C UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen800()
extern void MaterialColor_get_lightGreen800_m5E15973E1C162DC6ABEB249DD8CD50831C3F06AE ();
// 0x00000B4D UnityEngine.Color MaterialUI.MaterialColor::get_lightGreen900()
extern void MaterialColor_get_lightGreen900_m286D3FC9992DE58EBE0B90523AE8D246860BD511 ();
// 0x00000B4E UnityEngine.Color MaterialUI.MaterialColor::get_lightGreenA100()
extern void MaterialColor_get_lightGreenA100_mC32494F0139C83F1320D68EA9A2880A21205C57C ();
// 0x00000B4F UnityEngine.Color MaterialUI.MaterialColor::get_lightGreenA200()
extern void MaterialColor_get_lightGreenA200_mBDBCCD697647D2F3E734F859B89606B8B89827FE ();
// 0x00000B50 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreenA400()
extern void MaterialColor_get_lightGreenA400_m8016DE01BB2F56632197AB4C3495E1E7BB3F14E0 ();
// 0x00000B51 UnityEngine.Color MaterialUI.MaterialColor::get_lightGreenA700()
extern void MaterialColor_get_lightGreenA700_mDF6332C3D0909DF61C32EE4329B28EA0A01A3195 ();
// 0x00000B52 UnityEngine.Color[] MaterialUI.MaterialColor::get_limeSet()
extern void MaterialColor_get_limeSet_mB6A27A38119C064E7F831303379D9DCA27DF72AE ();
// 0x00000B53 UnityEngine.Color MaterialUI.MaterialColor::get_lime50()
extern void MaterialColor_get_lime50_m144C40817CCA794374DB460D19DDD332E14E6E59 ();
// 0x00000B54 UnityEngine.Color MaterialUI.MaterialColor::get_lime100()
extern void MaterialColor_get_lime100_mCE51652D619893F72F4257F63DE95F822CA6F3AB ();
// 0x00000B55 UnityEngine.Color MaterialUI.MaterialColor::get_lime200()
extern void MaterialColor_get_lime200_mE560A875D15152CDE68D1045847AB14365207E8C ();
// 0x00000B56 UnityEngine.Color MaterialUI.MaterialColor::get_lime300()
extern void MaterialColor_get_lime300_mE0AFF873667FC70088563A827D452134901646D1 ();
// 0x00000B57 UnityEngine.Color MaterialUI.MaterialColor::get_lime400()
extern void MaterialColor_get_lime400_m6236D031ECFF485929F2FF958CCAB71D63C6B45A ();
// 0x00000B58 UnityEngine.Color MaterialUI.MaterialColor::get_lime500()
extern void MaterialColor_get_lime500_mA8894D1E8B98BDFEA4E12CE5B928B94FD88734CC ();
// 0x00000B59 UnityEngine.Color MaterialUI.MaterialColor::get_lime600()
extern void MaterialColor_get_lime600_mE8DFF78473C623FE9D9A304D503D7C098342E25C ();
// 0x00000B5A UnityEngine.Color MaterialUI.MaterialColor::get_lime700()
extern void MaterialColor_get_lime700_mA1210828EEBB2727EFAF2524FEEF91DC4656A277 ();
// 0x00000B5B UnityEngine.Color MaterialUI.MaterialColor::get_lime800()
extern void MaterialColor_get_lime800_m794056780FF9E0DDAFD90FA887D76357F4C953F7 ();
// 0x00000B5C UnityEngine.Color MaterialUI.MaterialColor::get_lime900()
extern void MaterialColor_get_lime900_m9FEE09DFF780AC2C7D7D95B29DE539E267E9A7B8 ();
// 0x00000B5D UnityEngine.Color MaterialUI.MaterialColor::get_limeA100()
extern void MaterialColor_get_limeA100_m16B2C148DC9DC4F85A90DDB26E1E1F94AE76EABA ();
// 0x00000B5E UnityEngine.Color MaterialUI.MaterialColor::get_limeA200()
extern void MaterialColor_get_limeA200_m471BA6BD2D26CFDF662306D1D811218924CAB387 ();
// 0x00000B5F UnityEngine.Color MaterialUI.MaterialColor::get_limeA400()
extern void MaterialColor_get_limeA400_m6E55E37E778DE96702C674AFC10E81B4D5E86489 ();
// 0x00000B60 UnityEngine.Color MaterialUI.MaterialColor::get_limeA700()
extern void MaterialColor_get_limeA700_m5557797514A264DC862904FED0AA0DF701406DDC ();
// 0x00000B61 UnityEngine.Color[] MaterialUI.MaterialColor::get_yellowSet()
extern void MaterialColor_get_yellowSet_mC1E2993B63F0A50D91B0AFAA80DB5881F3F04DA8 ();
// 0x00000B62 UnityEngine.Color MaterialUI.MaterialColor::get_yellow50()
extern void MaterialColor_get_yellow50_mF388C22ED5732F843BAE75E22BF1239759D6693B ();
// 0x00000B63 UnityEngine.Color MaterialUI.MaterialColor::get_yellow100()
extern void MaterialColor_get_yellow100_mE9522B786F2501A3998B626C00A186373F170BCD ();
// 0x00000B64 UnityEngine.Color MaterialUI.MaterialColor::get_yellow200()
extern void MaterialColor_get_yellow200_m0F15EBF53FA70C6FAD586097E145FD5A119460DB ();
// 0x00000B65 UnityEngine.Color MaterialUI.MaterialColor::get_yellow300()
extern void MaterialColor_get_yellow300_mE9E3F7CE241E08568CD2E6B46BAD09B6A019744F ();
// 0x00000B66 UnityEngine.Color MaterialUI.MaterialColor::get_yellow400()
extern void MaterialColor_get_yellow400_m155634FF7BF88525D779F8942E438EB471FE844C ();
// 0x00000B67 UnityEngine.Color MaterialUI.MaterialColor::get_yellow500()
extern void MaterialColor_get_yellow500_m184876D6952E647B02DCF994E07B8E6479C0A5DE ();
// 0x00000B68 UnityEngine.Color MaterialUI.MaterialColor::get_yellow600()
extern void MaterialColor_get_yellow600_mCCC525E03C625D201026C41224231E03E782E140 ();
// 0x00000B69 UnityEngine.Color MaterialUI.MaterialColor::get_yellow700()
extern void MaterialColor_get_yellow700_m8EF1F3EB735CCDF56A87B6B2C334BEB0007F0923 ();
// 0x00000B6A UnityEngine.Color MaterialUI.MaterialColor::get_yellow800()
extern void MaterialColor_get_yellow800_mD9ED35F32EF2B7EF1DBFC3D0F346255A9E6533AB ();
// 0x00000B6B UnityEngine.Color MaterialUI.MaterialColor::get_yellow900()
extern void MaterialColor_get_yellow900_mDDCCF6EBDF633FE4BF52DCA51D7B0AD018C428ED ();
// 0x00000B6C UnityEngine.Color MaterialUI.MaterialColor::get_yellowA100()
extern void MaterialColor_get_yellowA100_m16A1454C2C1AF7342E6524A8F2D6D145F069B94E ();
// 0x00000B6D UnityEngine.Color MaterialUI.MaterialColor::get_yellowA200()
extern void MaterialColor_get_yellowA200_m9CF4882B4BACCFBBB7DE0F670F3138EC20E92ABB ();
// 0x00000B6E UnityEngine.Color MaterialUI.MaterialColor::get_yellowA400()
extern void MaterialColor_get_yellowA400_m962146CFBC6ED1A92AA4965F239D494E1E23AA65 ();
// 0x00000B6F UnityEngine.Color MaterialUI.MaterialColor::get_yellowA700()
extern void MaterialColor_get_yellowA700_m3A392A3641FCDCF772ADC8BD51A60BD3DA0B7880 ();
// 0x00000B70 UnityEngine.Color[] MaterialUI.MaterialColor::get_amberSet()
extern void MaterialColor_get_amberSet_mE5CA0FD1795B796CFD49A6A64C6799F1D8898C02 ();
// 0x00000B71 UnityEngine.Color MaterialUI.MaterialColor::get_amber50()
extern void MaterialColor_get_amber50_m29B2DC6012CE85321A74B13608081322441EFEC4 ();
// 0x00000B72 UnityEngine.Color MaterialUI.MaterialColor::get_amber100()
extern void MaterialColor_get_amber100_mD2A247D525F0B023D8D51B7C91A24537B6324AF4 ();
// 0x00000B73 UnityEngine.Color MaterialUI.MaterialColor::get_amber200()
extern void MaterialColor_get_amber200_mF82C563A65B23D7C3FF575E6ECB20130D36C8E54 ();
// 0x00000B74 UnityEngine.Color MaterialUI.MaterialColor::get_amber300()
extern void MaterialColor_get_amber300_m7EB50E2B61542D87004DBC318C822B6541EB522F ();
// 0x00000B75 UnityEngine.Color MaterialUI.MaterialColor::get_amber400()
extern void MaterialColor_get_amber400_mE7FC372641B70805285739ECA81582D8415C40EE ();
// 0x00000B76 UnityEngine.Color MaterialUI.MaterialColor::get_amber500()
extern void MaterialColor_get_amber500_m67801E7F39F52C37B66C50ED5D10548FF710CB46 ();
// 0x00000B77 UnityEngine.Color MaterialUI.MaterialColor::get_amber600()
extern void MaterialColor_get_amber600_mA37C23C761B2525719161F2012B56C19A66D68B7 ();
// 0x00000B78 UnityEngine.Color MaterialUI.MaterialColor::get_amber700()
extern void MaterialColor_get_amber700_m86B3F6C219C68DCB87CC68CCF8F2C86EEB5CFCEE ();
// 0x00000B79 UnityEngine.Color MaterialUI.MaterialColor::get_amber800()
extern void MaterialColor_get_amber800_m7B720C7FD9D8A9995FAAB908EDAC8EABA51DA387 ();
// 0x00000B7A UnityEngine.Color MaterialUI.MaterialColor::get_amber900()
extern void MaterialColor_get_amber900_mFE0F10869212E53A4DE81D9FADEFD1E2E66D765B ();
// 0x00000B7B UnityEngine.Color MaterialUI.MaterialColor::get_amberA100()
extern void MaterialColor_get_amberA100_m26B9C3474F715F74B22E273E2C83CA7B8FFCB98D ();
// 0x00000B7C UnityEngine.Color MaterialUI.MaterialColor::get_amberA200()
extern void MaterialColor_get_amberA200_m63A6FA635EB76F61B6762F590B6EEA24FD68481F ();
// 0x00000B7D UnityEngine.Color MaterialUI.MaterialColor::get_amberA400()
extern void MaterialColor_get_amberA400_m9D14E5BFD85B710816556A9C3C77B5ECBCC5346E ();
// 0x00000B7E UnityEngine.Color MaterialUI.MaterialColor::get_amberA700()
extern void MaterialColor_get_amberA700_mF7213F85C239123A66A4912B05E1C3DE4D45E211 ();
// 0x00000B7F UnityEngine.Color[] MaterialUI.MaterialColor::get_orangeSet()
extern void MaterialColor_get_orangeSet_m95800A61E8B9AB4E1B6C3411A5D4E464965C4D41 ();
// 0x00000B80 UnityEngine.Color MaterialUI.MaterialColor::get_orange50()
extern void MaterialColor_get_orange50_mA7D9FC4E49DAD79833665ECEBF0F106B46CB682B ();
// 0x00000B81 UnityEngine.Color MaterialUI.MaterialColor::get_orange100()
extern void MaterialColor_get_orange100_m6592815A30E62D238B8DC71F66AF06E1B7E9E505 ();
// 0x00000B82 UnityEngine.Color MaterialUI.MaterialColor::get_orange200()
extern void MaterialColor_get_orange200_mA311F4C6F269BF05622F79386F25E1CEE80289EC ();
// 0x00000B83 UnityEngine.Color MaterialUI.MaterialColor::get_orange300()
extern void MaterialColor_get_orange300_m4547220EFA3C2A05BAFB1132321BD1EF67FB5B0A ();
// 0x00000B84 UnityEngine.Color MaterialUI.MaterialColor::get_orange400()
extern void MaterialColor_get_orange400_m49DA383A952ADA0EE56EAB5212509E94FCEDAC2F ();
// 0x00000B85 UnityEngine.Color MaterialUI.MaterialColor::get_orange500()
extern void MaterialColor_get_orange500_m2195A678B68F941D1623855176512DDE84B9B6B7 ();
// 0x00000B86 UnityEngine.Color MaterialUI.MaterialColor::get_orange600()
extern void MaterialColor_get_orange600_m4FBD12C4419FBDE09A28C33F6014842C58314311 ();
// 0x00000B87 UnityEngine.Color MaterialUI.MaterialColor::get_orange700()
extern void MaterialColor_get_orange700_m8E015D3A2F5542849EB588148921F8CEF1FF9E5F ();
// 0x00000B88 UnityEngine.Color MaterialUI.MaterialColor::get_orange800()
extern void MaterialColor_get_orange800_mF710144EE6FD5A401541062374DD2E2B940A70C8 ();
// 0x00000B89 UnityEngine.Color MaterialUI.MaterialColor::get_orange900()
extern void MaterialColor_get_orange900_m63BE33AA8F1D88D751CCAAEF29AB6358F8815596 ();
// 0x00000B8A UnityEngine.Color MaterialUI.MaterialColor::get_orangeA100()
extern void MaterialColor_get_orangeA100_m40E94EC5AFE4868A4C589E17DDA77543FBEB9737 ();
// 0x00000B8B UnityEngine.Color MaterialUI.MaterialColor::get_orangeA200()
extern void MaterialColor_get_orangeA200_m79E2AA105531B6C51C21998B49FE90C2644E225B ();
// 0x00000B8C UnityEngine.Color MaterialUI.MaterialColor::get_orangeA400()
extern void MaterialColor_get_orangeA400_mB51AE5A92153AC9C2B3D0200B868F9CB07C7CCC9 ();
// 0x00000B8D UnityEngine.Color MaterialUI.MaterialColor::get_orangeA700()
extern void MaterialColor_get_orangeA700_mED213A63C3BE473D263D80138CA919A5779654DB ();
// 0x00000B8E UnityEngine.Color[] MaterialUI.MaterialColor::get_deepOrangeSet()
extern void MaterialColor_get_deepOrangeSet_mC47709C121C72AF5E61E22DD18B09F00DD3C7A33 ();
// 0x00000B8F UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange50()
extern void MaterialColor_get_deepOrange50_m4E27DF59AF9FE1B2844402E5F810A7901AAD1608 ();
// 0x00000B90 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange100()
extern void MaterialColor_get_deepOrange100_mFEC63CC28307C2EF1E73042B10E9BD00C8F6A5FC ();
// 0x00000B91 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange200()
extern void MaterialColor_get_deepOrange200_mD3DAC9103CF558841816F023E1F3C970DBFB2BBA ();
// 0x00000B92 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange300()
extern void MaterialColor_get_deepOrange300_mA80B6AD77C253D9FE2C58E3CAB6045A1B9A69BEE ();
// 0x00000B93 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange400()
extern void MaterialColor_get_deepOrange400_mA72119B4EB016BDFEDA3A917078A9CD0FA463221 ();
// 0x00000B94 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange500()
extern void MaterialColor_get_deepOrange500_m07509EC1CF525878A94366632BA77BA8D50640F2 ();
// 0x00000B95 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange600()
extern void MaterialColor_get_deepOrange600_mCFDE75ADAEB2A007B2D0D772664EAAFC62BB5967 ();
// 0x00000B96 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange700()
extern void MaterialColor_get_deepOrange700_m465EA2F8DDC4A7F86084538E7423AF5CE73153F1 ();
// 0x00000B97 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange800()
extern void MaterialColor_get_deepOrange800_m332AC1B98425BFB4AD9D4A3F7AAF5D3D3F069F95 ();
// 0x00000B98 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrange900()
extern void MaterialColor_get_deepOrange900_mB08D138B0593DA4D9C2D9AC8D249A1C13E3993B5 ();
// 0x00000B99 UnityEngine.Color MaterialUI.MaterialColor::get_deepOrangeA100()
extern void MaterialColor_get_deepOrangeA100_mC78455634E75A1AA602BDF80DD2118EC2B257727 ();
// 0x00000B9A UnityEngine.Color MaterialUI.MaterialColor::get_deepOrangeA200()
extern void MaterialColor_get_deepOrangeA200_mC9C6075004A7024FDCAD93F014C8A9E024D4B6C1 ();
// 0x00000B9B UnityEngine.Color MaterialUI.MaterialColor::get_deepOrangeA400()
extern void MaterialColor_get_deepOrangeA400_mEE163B046530AAC6D22C5D011011A8DBCA7E9949 ();
// 0x00000B9C UnityEngine.Color MaterialUI.MaterialColor::get_deepOrangeA700()
extern void MaterialColor_get_deepOrangeA700_m6133342287404F10F161DB04F58814CC6F94A0C4 ();
// 0x00000B9D UnityEngine.Color[] MaterialUI.MaterialColor::get_brownSet()
extern void MaterialColor_get_brownSet_mA6ED0DB4086455A71D747BEA55ADEC27B7593CE9 ();
// 0x00000B9E UnityEngine.Color MaterialUI.MaterialColor::get_brown50()
extern void MaterialColor_get_brown50_mEDE9315DA77FE2C44B5B1AD880C6040A7D2A143D ();
// 0x00000B9F UnityEngine.Color MaterialUI.MaterialColor::get_brown100()
extern void MaterialColor_get_brown100_mAB6DE0459EF7F2D17DB2CF40A241C35FA2089F3E ();
// 0x00000BA0 UnityEngine.Color MaterialUI.MaterialColor::get_brown200()
extern void MaterialColor_get_brown200_mF003F25EB252548A1907A44B635D44B4B5A8F9FB ();
// 0x00000BA1 UnityEngine.Color MaterialUI.MaterialColor::get_brown300()
extern void MaterialColor_get_brown300_m98AA200E5F2BBE88D4AC5FDDBFC5B3ECE22CACE0 ();
// 0x00000BA2 UnityEngine.Color MaterialUI.MaterialColor::get_brown400()
extern void MaterialColor_get_brown400_m320A15878B6FB839570CF55A2305422D3CA61A9F ();
// 0x00000BA3 UnityEngine.Color MaterialUI.MaterialColor::get_brown500()
extern void MaterialColor_get_brown500_mEB1750DE64F6F0B45DCDB5DF332F1D9DBB629FE5 ();
// 0x00000BA4 UnityEngine.Color MaterialUI.MaterialColor::get_brown600()
extern void MaterialColor_get_brown600_m2E532051A0FEE41338B1AFF1F552D4319CC71E49 ();
// 0x00000BA5 UnityEngine.Color MaterialUI.MaterialColor::get_brown700()
extern void MaterialColor_get_brown700_mC7EC73C6B0D81941445FB71ADFF722D2F6202592 ();
// 0x00000BA6 UnityEngine.Color MaterialUI.MaterialColor::get_brown800()
extern void MaterialColor_get_brown800_mDB2E765D880C08908120433E84E89B635DA5BD0E ();
// 0x00000BA7 UnityEngine.Color MaterialUI.MaterialColor::get_brown900()
extern void MaterialColor_get_brown900_mE2C41A94BF3F7BC3F17E805B635329854F988070 ();
// 0x00000BA8 UnityEngine.Color[] MaterialUI.MaterialColor::get_blueGreySet()
extern void MaterialColor_get_blueGreySet_mDFFF537AEF52077F5849D443A5F92E3979C2410C ();
// 0x00000BA9 UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey50()
extern void MaterialColor_get_blueGrey50_mAEF651155746726758D83C0A742C055390769DC9 ();
// 0x00000BAA UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey100()
extern void MaterialColor_get_blueGrey100_m73B5BED568510B350F211497771BDB485942C0EC ();
// 0x00000BAB UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey200()
extern void MaterialColor_get_blueGrey200_mC4B4D0190ED5BA514E2383D4874BAE2873C4AC8E ();
// 0x00000BAC UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey300()
extern void MaterialColor_get_blueGrey300_mCA2FCBF6D5A20C7A3C5A1E2948B47939FFC6B2C9 ();
// 0x00000BAD UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey400()
extern void MaterialColor_get_blueGrey400_m0254E82B9A45AE32768082FBB77930469B8B4F6B ();
// 0x00000BAE UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey500()
extern void MaterialColor_get_blueGrey500_m6CCFA86892904A14FA130D1882F4AA8F67D34A40 ();
// 0x00000BAF UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey600()
extern void MaterialColor_get_blueGrey600_m14FA4D93E3C9A7A3DC97C81750320F230E5C1EDC ();
// 0x00000BB0 UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey700()
extern void MaterialColor_get_blueGrey700_mA00014C2865932E102048261CBB056476C2D778A ();
// 0x00000BB1 UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey800()
extern void MaterialColor_get_blueGrey800_m03E9E3DDF9BF9DDEB7681ECE9DBAA2A8B8081931 ();
// 0x00000BB2 UnityEngine.Color MaterialUI.MaterialColor::get_blueGrey900()
extern void MaterialColor_get_blueGrey900_m1359F2F0B21E5AE0DEAB855009136325C89C17AB ();
// 0x00000BB3 UnityEngine.Color[] MaterialUI.MaterialColor::get_greySet()
extern void MaterialColor_get_greySet_m53EDFC208D71FB414DB38AD956B541679C3BAA8F ();
// 0x00000BB4 UnityEngine.Color MaterialUI.MaterialColor::get_grey50()
extern void MaterialColor_get_grey50_m159A28A34EBD92DF24737631DFD83DA3A9F035DD ();
// 0x00000BB5 UnityEngine.Color MaterialUI.MaterialColor::get_grey100()
extern void MaterialColor_get_grey100_m19F79899EB584B3ECEFAE64768751CA0B02D6468 ();
// 0x00000BB6 UnityEngine.Color MaterialUI.MaterialColor::get_grey200()
extern void MaterialColor_get_grey200_m947A94AC106A8789052E381CFD188168123990EB ();
// 0x00000BB7 UnityEngine.Color MaterialUI.MaterialColor::get_grey300()
extern void MaterialColor_get_grey300_mA3833153DDB558A942F79908C25BA546863BA946 ();
// 0x00000BB8 UnityEngine.Color MaterialUI.MaterialColor::get_grey400()
extern void MaterialColor_get_grey400_mA45DF946E8678BFF3E8AA09C48867B97B95C7CF3 ();
// 0x00000BB9 UnityEngine.Color MaterialUI.MaterialColor::get_grey500()
extern void MaterialColor_get_grey500_m16D9E213BD57E5A1A04FC5010A5B6E992B6D4E32 ();
// 0x00000BBA UnityEngine.Color MaterialUI.MaterialColor::get_grey600()
extern void MaterialColor_get_grey600_m95B26E6D3E8F2FB9B6BAD5D46870B3671001BCA6 ();
// 0x00000BBB UnityEngine.Color MaterialUI.MaterialColor::get_grey700()
extern void MaterialColor_get_grey700_m964F61914E4C484B928616731DC24A1A7980373B ();
// 0x00000BBC UnityEngine.Color MaterialUI.MaterialColor::get_grey800()
extern void MaterialColor_get_grey800_m71864204E4E661A131CF8A0262DDE191FACE50A7 ();
// 0x00000BBD UnityEngine.Color MaterialUI.MaterialColor::get_grey900()
extern void MaterialColor_get_grey900_m06C3FADD71EA559A6F2527E84F59107F2801EE96 ();
// 0x00000BBE UnityEngine.Color MaterialUI.MaterialColor::get_textDark()
extern void MaterialColor_get_textDark_m012169304CE2197EF97994EE8AA5F928B6FEE3ED ();
// 0x00000BBF UnityEngine.Color MaterialUI.MaterialColor::get_textLight()
extern void MaterialColor_get_textLight_m629377FED1B4B75ADCE0A60B50708CEAD637814F ();
// 0x00000BC0 UnityEngine.Color MaterialUI.MaterialColor::get_textSecondaryDark()
extern void MaterialColor_get_textSecondaryDark_mBB97307B47AFE4144A3E98616B4F8C3006B893E6 ();
// 0x00000BC1 UnityEngine.Color MaterialUI.MaterialColor::get_textSecondaryLight()
extern void MaterialColor_get_textSecondaryLight_m36BF646132A333ACD6155ABD3D06324EDD10A317 ();
// 0x00000BC2 UnityEngine.Color MaterialUI.MaterialColor::get_textHintDark()
extern void MaterialColor_get_textHintDark_mBA7C5D0D85B7323D39E6F40ED440816F121EAF73 ();
// 0x00000BC3 UnityEngine.Color MaterialUI.MaterialColor::get_textHintLight()
extern void MaterialColor_get_textHintLight_mEA222F1BF75B47A6EAE1357BE63C1B23E687361B ();
// 0x00000BC4 UnityEngine.Color MaterialUI.MaterialColor::get_iconDark()
extern void MaterialColor_get_iconDark_m1287A696051182AC94BDC0E9EFACE4742F766679 ();
// 0x00000BC5 UnityEngine.Color MaterialUI.MaterialColor::get_iconLight()
extern void MaterialColor_get_iconLight_m8132EA5B320822A41D67033CBFE7C99D6164E708 ();
// 0x00000BC6 UnityEngine.Color MaterialUI.MaterialColor::get_disabledDark()
extern void MaterialColor_get_disabledDark_m42E1D2082F85FCAEFCEF7BBF1AAE0FBCBFAA4EBD ();
// 0x00000BC7 UnityEngine.Color MaterialUI.MaterialColor::get_disabledLight()
extern void MaterialColor_get_disabledLight_mD43647D7BBD898D51372ECFE4218FBB1B23DAC78 ();
// 0x00000BC8 UnityEngine.Color MaterialUI.MaterialColor::get_dividerDark()
extern void MaterialColor_get_dividerDark_m7008AFFA8A93E007FBD8234D6013770337A5D884 ();
// 0x00000BC9 UnityEngine.Color MaterialUI.MaterialColor::get_dividerLight()
extern void MaterialColor_get_dividerLight_mFA7C528CDF0E2E8371D4FF60553DFC4D32EC0935 ();
// 0x00000BCA System.Void MaterialUI.MaterialColor::.cctor()
extern void MaterialColor__cctor_m03618B8A3DF292C8761A494427ADCA36007A9989 ();
// 0x00000BCB System.String MaterialUI.IconDecoder::Decode(System.String)
extern void IconDecoder_Decode_mCD51CC440E3477DF92FDFFB26A9736F45A573D10 ();
// 0x00000BCC System.Void MaterialUI.IconDecoder::.cctor()
extern void IconDecoder__cctor_m91B85BCB52F443E2565788F1E00F2939A468BA16 ();
// 0x00000BCD System.Void MaterialUI.Utils::SetBoolValueIfTrue(System.Boolean&,System.Boolean)
extern void Utils_SetBoolValueIfTrue_mB103E1205C482E2BECBE2CBF90F1266AD57F70D9 ();
// 0x00000BCE System.Single MaterialUI.Utils::GetScreenDiagonal()
extern void Utils_GetScreenDiagonal_m83D6DA09188FEEA63EDDD9DF6C310833BC730E95 ();
// 0x00000BCF System.Single MaterialUI.Utils::GetScreenDiagonal(UnityEngine.Vector2,System.Single)
extern void Utils_GetScreenDiagonal_m601A7359E0D83F4020113FCFCE253416FF37C7E3 ();
// 0x00000BD0 System.Void MaterialUI.ObserveAttribute::.ctor(System.String[])
extern void ObserveAttribute__ctor_m786E6FE614B4C5A0DD99F67F0C766C18F96249C1 ();
// 0x00000BD1 System.Void MaterialUI.PopupAttribute::.ctor(System.Object[])
extern void PopupAttribute__ctor_m6370173710EC04E4814E9AD8A4FE98107CF97E1A ();
// 0x00000BD2 System.String Assets.Script.Utility.MethodExtensions::RemoveQuotes(System.String)
extern void MethodExtensions_RemoveQuotes_m0A45D9C0EAFE4572A802D73C1061B1A643E14341 ();
// 0x00000BD3 System.String Assets.Scripts.Model.RootObjectModel::get_NodeId()
extern void RootObjectModel_get_NodeId_mAB0ECD7CB7F64A00D2313A6F23703FD73472DFF6 ();
// 0x00000BD4 System.Void Assets.Scripts.Model.RootObjectModel::set_NodeId(System.String)
extern void RootObjectModel_set_NodeId_mD4299216768ABDCF0E7EEDA63C3105F1953A246A ();
// 0x00000BD5 System.String Assets.Scripts.Model.RootObjectModel::get_PresentationType()
extern void RootObjectModel_get_PresentationType_m7E2477BA1380049E7D9C0E7D3295E2EAF097F03E ();
// 0x00000BD6 System.Void Assets.Scripts.Model.RootObjectModel::set_PresentationType(System.String)
extern void RootObjectModel_set_PresentationType_mFA0F03ED1118C178CC6A5954D273C197C45AE70E ();
// 0x00000BD7 System.String Assets.Scripts.Model.RootObjectModel::get_MessageType()
extern void RootObjectModel_get_MessageType_m9E0E46C473AB0242591C1476A828AE3C9E27BB15 ();
// 0x00000BD8 System.Void Assets.Scripts.Model.RootObjectModel::set_MessageType(System.String)
extern void RootObjectModel_set_MessageType_m8B14EE8D13A1953224B1B2FAF1A1E110EE7DABEB ();
// 0x00000BD9 System.String Assets.Scripts.Model.RootObjectModel::get_ResponseField()
extern void RootObjectModel_get_ResponseField_m09366F7FFAC7B1E77F28B6E0BAFE23C2C5E6A961 ();
// 0x00000BDA System.Void Assets.Scripts.Model.RootObjectModel::set_ResponseField(System.String)
extern void RootObjectModel_set_ResponseField_mCFEAAF40F8718E13801B72DC4A9496C7F630E84C ();
// 0x00000BDB System.Collections.Generic.List`1<Assets.Scripts.Model.AzureFunction> Assets.Scripts.Model.RootObjectModel::get_AzureFunction()
extern void RootObjectModel_get_AzureFunction_m8273AD4799022D28BFE4F898CDD434C77715FA66 ();
// 0x00000BDC System.Void Assets.Scripts.Model.RootObjectModel::set_AzureFunction(System.Collections.Generic.List`1<Assets.Scripts.Model.AzureFunction>)
extern void RootObjectModel_set_AzureFunction_m540B40FBAF462385033BE67B29C27FC2D75AA092 ();
// 0x00000BDD System.String Assets.Scripts.Model.RootObjectModel::get_ChartType()
extern void RootObjectModel_get_ChartType_m9EA17EAF1E6BF132304D759CCBFA241FE38FABA0 ();
// 0x00000BDE System.Void Assets.Scripts.Model.RootObjectModel::set_ChartType(System.String)
extern void RootObjectModel_set_ChartType_m024D530D40E8843893BC4F05387F6C4FFB9412A7 ();
// 0x00000BDF System.Void Assets.Scripts.Model.RootObjectModel::.ctor()
extern void RootObjectModel__ctor_mABA6F6206DF45965F954F75EA680CE0CAD748015 ();
// 0x00000BE0 System.String Assets.Scripts.Model.Param::get_username()
extern void Param_get_username_m9B67898E1D5A53366972F4D419A6F2907DAB333C ();
// 0x00000BE1 System.Void Assets.Scripts.Model.Param::set_username(System.String)
extern void Param_set_username_m47DFA1551C7AF21F85E7B8AF0DC5667482F8F4B8 ();
// 0x00000BE2 System.String Assets.Scripts.Model.Param::get_packname()
extern void Param_get_packname_m0279216ADE8CD31E9211A9AEA89DEF84E2D5CC89 ();
// 0x00000BE3 System.Void Assets.Scripts.Model.Param::set_packname(System.String)
extern void Param_set_packname_mB120077CF38EFC99DD9C967A38A9A8C7E8F01296 ();
// 0x00000BE4 System.String Assets.Scripts.Model.Param::get_queryname()
extern void Param_get_queryname_mD448447BC897717C106CDA22F59ADE3D2651C5A7 ();
// 0x00000BE5 System.Void Assets.Scripts.Model.Param::set_queryname(System.String)
extern void Param_set_queryname_m3600723B2F314D4EA5247E10A454B764F624586F ();
// 0x00000BE6 System.Void Assets.Scripts.Model.Param::.ctor()
extern void Param__ctor_m160CF74A60840E5351A687C0290C2D42E777BA89 ();
// 0x00000BE7 System.String Assets.Scripts.Model.AzureFunction::get_AuthType()
extern void AzureFunction_get_AuthType_m366C325126730CF30BD5B3D842CCA039B9CDF30F ();
// 0x00000BE8 System.Void Assets.Scripts.Model.AzureFunction::set_AuthType(System.String)
extern void AzureFunction_set_AuthType_m89109D26E790A668E9A60BCEAB6D2E30A3C748E7 ();
// 0x00000BE9 System.String Assets.Scripts.Model.AzureFunction::get_RequestURL()
extern void AzureFunction_get_RequestURL_m7A294224FA9593C26846C8548265DCC3E64C13AB ();
// 0x00000BEA System.Void Assets.Scripts.Model.AzureFunction::set_RequestURL(System.String)
extern void AzureFunction_set_RequestURL_mFDD34A7D1F3708476453A1948D5C045495944457 ();
// 0x00000BEB System.String Assets.Scripts.Model.AzureFunction::get_FunctionKey()
extern void AzureFunction_get_FunctionKey_m830D04C07B69B128A3D4493DF8522D6BD3E0652B ();
// 0x00000BEC System.Void Assets.Scripts.Model.AzureFunction::set_FunctionKey(System.String)
extern void AzureFunction_set_FunctionKey_m40338133C493B38BA7B1317C6CBA1C0811034A69 ();
// 0x00000BED System.String Assets.Scripts.Model.AzureFunction::get_FunctionName()
extern void AzureFunction_get_FunctionName_mC98A2500CBAFB1B8A80719F5FF1C48F0E8E498DC ();
// 0x00000BEE System.Void Assets.Scripts.Model.AzureFunction::set_FunctionName(System.String)
extern void AzureFunction_set_FunctionName_m33D4E8D43420EFD1A3DCDF9E7865AB8BCAF6F74D ();
// 0x00000BEF System.Collections.Generic.List`1<Assets.Scripts.Model.Param> Assets.Scripts.Model.AzureFunction::get_Params()
extern void AzureFunction_get_Params_mD98613E9388D0F27AB9E52133A2430CD5281C6E4 ();
// 0x00000BF0 System.Void Assets.Scripts.Model.AzureFunction::set_Params(System.Collections.Generic.List`1<Assets.Scripts.Model.Param>)
extern void AzureFunction_set_Params_m48179295EBAAD068085C83F8A949F20ACB6E5541 ();
// 0x00000BF1 System.Void Assets.Scripts.Model.AzureFunction::.ctor()
extern void AzureFunction__ctor_m885B50DD6FD3F22470F4D0477BA8958AB41196E9 ();
// 0x00000BF2 Assets.Base.DeepLink.ProcessDeepLinkMngr Assets.Base.DeepLink.ProcessDeepLinkMngr::get_Instance()
extern void ProcessDeepLinkMngr_get_Instance_m1C7ED9BF8721C711BF4DCCED9A542D5521F7B5AC ();
// 0x00000BF3 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr::set_Instance(Assets.Base.DeepLink.ProcessDeepLinkMngr)
extern void ProcessDeepLinkMngr_set_Instance_mD1772020E3D01F1FDFB8FB5CBAAFDA39A1BF0522 ();
// 0x00000BF4 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr::Awake()
extern void ProcessDeepLinkMngr_Awake_m014D94E89B580FC209E61CC885EBFD701D3FBE9A ();
// 0x00000BF5 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr::RenderRoomObJ(System.String)
extern void ProcessDeepLinkMngr_RenderRoomObJ_mC667BD98EB023911CCAEF15A436925E5B800ED82 ();
// 0x00000BF6 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr::RenderRoom(System.String)
extern void ProcessDeepLinkMngr_RenderRoom_mF4F8EE32E922697E5835853B5098566FF0A4D08F ();
// 0x00000BF7 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr::.ctor()
extern void ProcessDeepLinkMngr__ctor_mCC4EB0B29DD959A72A67C5C20CD16E8F9392EE33 ();
// 0x00000BF8 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m082960ACC7C4FD71A9A2131DE82E8147B002CE2C ();
// 0x00000BF9 System.Void TextBoard_<DelayFunction>d__6::.ctor(System.Int32)
extern void U3CDelayFunctionU3Ed__6__ctor_mD360CCE2B6876E130EA9CFE00CCF5AB4C0415AA3 ();
// 0x00000BFA System.Void TextBoard_<DelayFunction>d__6::System.IDisposable.Dispose()
extern void U3CDelayFunctionU3Ed__6_System_IDisposable_Dispose_m2E9D7D26F12DBE60AE806E44C96F1C9A0AFBBCFD ();
// 0x00000BFB System.Boolean TextBoard_<DelayFunction>d__6::MoveNext()
extern void U3CDelayFunctionU3Ed__6_MoveNext_m9CB0E7F7963A4311450342F18613E03C971219B4 ();
// 0x00000BFC System.Object TextBoard_<DelayFunction>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayFunctionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A759CAE3225E79B58AEDE42D4A557C58879CE13 ();
// 0x00000BFD System.Void TextBoard_<DelayFunction>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDelayFunctionU3Ed__6_System_Collections_IEnumerator_Reset_mD6D45010081B0E1928694B244F9E8E188DEDD009 ();
// 0x00000BFE System.Object TextBoard_<DelayFunction>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDelayFunctionU3Ed__6_System_Collections_IEnumerator_get_Current_m21F3388D688B0291A485B77BFFEC791A7CFC0213 ();
// 0x00000BFF System.Void ProxyAPI_<PostReq>d__4::.ctor(System.Int32)
extern void U3CPostReqU3Ed__4__ctor_m8BCAAAB2493A7D925150C66278E7CDDA020C6524 ();
// 0x00000C00 System.Void ProxyAPI_<PostReq>d__4::System.IDisposable.Dispose()
extern void U3CPostReqU3Ed__4_System_IDisposable_Dispose_m8552799457FCBD3BD23DA788C21627643C6D223E ();
// 0x00000C01 System.Boolean ProxyAPI_<PostReq>d__4::MoveNext()
extern void U3CPostReqU3Ed__4_MoveNext_mED2A7980C743BEDC2091C094C2C3AEF2D6A342E3 ();
// 0x00000C02 System.Object ProxyAPI_<PostReq>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostReqU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05F86707AA6717C4ABED2E8F529C1C69D74BFCD8 ();
// 0x00000C03 System.Void ProxyAPI_<PostReq>d__4::System.Collections.IEnumerator.Reset()
extern void U3CPostReqU3Ed__4_System_Collections_IEnumerator_Reset_mE43979B4BC71C9E79FEE52FAE5F1EF92B52A62B6 ();
// 0x00000C04 System.Object ProxyAPI_<PostReq>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CPostReqU3Ed__4_System_Collections_IEnumerator_get_Current_m25F11D2D0EEB70D770C51660CF36CD7821B183DE ();
// 0x00000C05 System.Void ProxyAPI_<GetReq>d__8::.ctor(System.Int32)
extern void U3CGetReqU3Ed__8__ctor_m28118B5B585069D7F8F3A1813B9CEB6D5AAF672B ();
// 0x00000C06 System.Void ProxyAPI_<GetReq>d__8::System.IDisposable.Dispose()
extern void U3CGetReqU3Ed__8_System_IDisposable_Dispose_mAB1221A457E756973DF16F9BF7F2CFFC0869BABA ();
// 0x00000C07 System.Boolean ProxyAPI_<GetReq>d__8::MoveNext()
extern void U3CGetReqU3Ed__8_MoveNext_m03FA422DC0727ADF61B8D6E08DAEC316E86A5622 ();
// 0x00000C08 System.Object ProxyAPI_<GetReq>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetReqU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF3C1E18F79B952DE0320B39BDD02CD0842C91A3 ();
// 0x00000C09 System.Void ProxyAPI_<GetReq>d__8::System.Collections.IEnumerator.Reset()
extern void U3CGetReqU3Ed__8_System_Collections_IEnumerator_Reset_mF9C2862616D2FFBD6D2D5504137EC1EE3C8A8075 ();
// 0x00000C0A System.Object ProxyAPI_<GetReq>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CGetReqU3Ed__8_System_Collections_IEnumerator_get_Current_m39B7611399CEC839B2C999A579734E66D1E57134 ();
// 0x00000C0B System.Void ProxyAPI_<UploadMultiple>d__10::.ctor(System.Int32)
extern void U3CUploadMultipleU3Ed__10__ctor_m06D3B56CEA09F5669C4A43465FCB1880508CEB4A ();
// 0x00000C0C System.Void ProxyAPI_<UploadMultiple>d__10::System.IDisposable.Dispose()
extern void U3CUploadMultipleU3Ed__10_System_IDisposable_Dispose_mC0F1F21AC0428632151641116E5DDC64171463B6 ();
// 0x00000C0D System.Boolean ProxyAPI_<UploadMultiple>d__10::MoveNext()
extern void U3CUploadMultipleU3Ed__10_MoveNext_mC7333759E6F261F087C12B8398A2A7534B0EB5C3 ();
// 0x00000C0E System.Object ProxyAPI_<UploadMultiple>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUploadMultipleU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m715061F2DF31681F5B5A59A5B68D829B78526FB4 ();
// 0x00000C0F System.Void ProxyAPI_<UploadMultiple>d__10::System.Collections.IEnumerator.Reset()
extern void U3CUploadMultipleU3Ed__10_System_Collections_IEnumerator_Reset_m65EC30DA5F8C72B38871966B5B83F346CF0A0A0C ();
// 0x00000C10 System.Object ProxyAPI_<UploadMultiple>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CUploadMultipleU3Ed__10_System_Collections_IEnumerator_get_Current_m94859A019F3CA780B4B337C90D82559268EF42A2 ();
// 0x00000C11 System.Void ProxyAPI_<GetFileUrl>d__11::.ctor(System.Int32)
extern void U3CGetFileUrlU3Ed__11__ctor_mAEE3FAA626051F32715CC7293DDDECBDECE5A7FF ();
// 0x00000C12 System.Void ProxyAPI_<GetFileUrl>d__11::System.IDisposable.Dispose()
extern void U3CGetFileUrlU3Ed__11_System_IDisposable_Dispose_mED43604786B4EC885DF0D69817ECBD774838F814 ();
// 0x00000C13 System.Boolean ProxyAPI_<GetFileUrl>d__11::MoveNext()
extern void U3CGetFileUrlU3Ed__11_MoveNext_m9F4C764C6602EA5E907DCD011A6D8C2C3E7EDC30 ();
// 0x00000C14 System.Object ProxyAPI_<GetFileUrl>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetFileUrlU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1F28127B95DC44EAC230670EE2AD37D875D6D5F ();
// 0x00000C15 System.Void ProxyAPI_<GetFileUrl>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGetFileUrlU3Ed__11_System_Collections_IEnumerator_Reset_m870EB6DD83530A05E89E4DD41A403C879167569B ();
// 0x00000C16 System.Object ProxyAPI_<GetFileUrl>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGetFileUrlU3Ed__11_System_Collections_IEnumerator_get_Current_mBFF932F85B75E1D84B97451F1400FA2747449201 ();
// 0x00000C17 System.Void ProxyAPI_<_DeleteReq>d__13::.ctor(System.Int32)
extern void U3C_DeleteReqU3Ed__13__ctor_m5211A75B03D44129D5D886170EBF061F2FF9212E ();
// 0x00000C18 System.Void ProxyAPI_<_DeleteReq>d__13::System.IDisposable.Dispose()
extern void U3C_DeleteReqU3Ed__13_System_IDisposable_Dispose_m6109D992D8F8D05F6AE398D98379324C20D5496F ();
// 0x00000C19 System.Boolean ProxyAPI_<_DeleteReq>d__13::MoveNext()
extern void U3C_DeleteReqU3Ed__13_MoveNext_m6440B2D8E47F0E9FC7F718039F39EF04A569DB52 ();
// 0x00000C1A System.Object ProxyAPI_<_DeleteReq>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_DeleteReqU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22FE3C19043FAAF58E0A32D31027EECDBF73D664 ();
// 0x00000C1B System.Void ProxyAPI_<_DeleteReq>d__13::System.Collections.IEnumerator.Reset()
extern void U3C_DeleteReqU3Ed__13_System_Collections_IEnumerator_Reset_m15219ECD0530AB8BC764ECFB51EBB0BB9B534CD5 ();
// 0x00000C1C System.Object ProxyAPI_<_DeleteReq>d__13::System.Collections.IEnumerator.get_Current()
extern void U3C_DeleteReqU3Ed__13_System_Collections_IEnumerator_get_Current_mDF160E969EA0D59143527D9210A5E61387C41533 ();
// 0x00000C1D System.Void RoomManager_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mD8944BEE5D8067BD0037429BC8342BAFF8FA37E6 ();
// 0x00000C1E System.Boolean RoomManager_<>c__DisplayClass24_0::<RenderRoom>b__0(RoomDetailModel)
extern void U3CU3Ec__DisplayClass24_0_U3CRenderRoomU3Eb__0_mC499BA271045B15194C0EA137374764A7E5DF096 ();
// 0x00000C1F System.Boolean RoomManager_<>c__DisplayClass24_0::<RenderRoom>b__1(ModeTypeModel)
extern void U3CU3Ec__DisplayClass24_0_U3CRenderRoomU3Eb__1_m96158D9BC3F6361E0136479AA7F7F9F76FC9E901 ();
// 0x00000C20 System.Void RoomManager_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m5D15F4EBF887E3A9B66715D64EFEE58E8F38B517 ();
// 0x00000C21 System.Boolean RoomManager_<>c__DisplayClass25_0::<RenderRoom>b__0(RoomDetailModel)
extern void U3CU3Ec__DisplayClass25_0_U3CRenderRoomU3Eb__0_m0061CCD09FE6D4115ABF1FB33C198DC105C0757E ();
// 0x00000C22 System.Void RoomManager_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mADE8DEA52C89DAE66645688B83B0AABF5C66A0EC ();
// 0x00000C23 System.Boolean RoomManager_<>c__DisplayClass29_0::<RenderTitle>b__2(EnvironmentModel)
extern void U3CU3Ec__DisplayClass29_0_U3CRenderTitleU3Eb__2_m1CEA2C4A9EBE3FE89D1CD4F911C953551A1296DF ();
// 0x00000C24 System.Void RoomManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m00F951C0A9356AA1577DE7F4F0D3A395CAF69CBE ();
// 0x00000C25 System.Void RoomManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mC56523C54D2645AD702B2B2CC48B93F9B59ADB0C ();
// 0x00000C26 System.DateTime RoomManager_<>c::<RenderTitle>b__29_0(RoomDetailModel)
extern void U3CU3Ec_U3CRenderTitleU3Eb__29_0_m1A4AB48044F03827AD9FDD8F8653DE17D986E5CC ();
// 0x00000C27 System.String RoomManager_<>c::<RenderTitle>b__29_1(UnityEngine.GameObject)
extern void U3CU3Ec_U3CRenderTitleU3Eb__29_1_mA175809561622227F7C726B1D3017EAE6073C60E ();
// 0x00000C28 System.DateTime RoomManager_<>c::<RoomsRenderView>b__30_0(RoomDetailModel)
extern void U3CU3Ec_U3CRoomsRenderViewU3Eb__30_0_mD8831F29B3106CFD0F3BF79CF4DF0FC5CFF9419A ();
// 0x00000C29 System.Boolean RoomManager_<>c::<openRoom>b__32_0(RoomDetailModel)
extern void U3CU3Ec_U3CopenRoomU3Eb__32_0_m0EA551B444782651AF9FF3DD8A296953A0D68050 ();
// 0x00000C2A System.Void RoomManager_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mAD7867C3720D2AA1385467A484E12599C40E4688 ();
// 0x00000C2B System.Boolean RoomManager_<>c__DisplayClass30_0::<RoomsRenderView>b__1(EnvironmentModel)
extern void U3CU3Ec__DisplayClass30_0_U3CRoomsRenderViewU3Eb__1_mB354AEE4B125F99ED44848CF28D99148D4551A43 ();
// 0x00000C2C System.Void RoomManager_<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_m38197037B136250897B7427CEB636A3B2727921F ();
// 0x00000C2D System.Void RoomManager_<>c__DisplayClass30_1::<RoomsRenderView>b__2()
extern void U3CU3Ec__DisplayClass30_1_U3CRoomsRenderViewU3Eb__2_mAA8CD92163251368146658A3378F7A0DF8DCFCBE ();
// 0x00000C2E System.Void RoomManager_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mEFBAB32A244B20D743464E5E99F102B63A9F8B76 ();
// 0x00000C2F System.Boolean RoomManager_<>c__DisplayClass34_0::<DeleteRoomList>b__0(RoomDetailModel)
extern void U3CU3Ec__DisplayClass34_0_U3CDeleteRoomListU3Eb__0_m098E88A527B15AB829B903E3DC8787D8A629A3EE ();
// 0x00000C30 System.Void RoomStateManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m8DF4342C36C41F5C04F721524826E0C9A8D1C239 ();
// 0x00000C31 System.Void RoomStateManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m5C2551A553AB80D833244667085AFFC46DE1A306 ();
// 0x00000C32 System.Boolean RoomStateManager_<>c::<RenderRoomObject>b__12_0(EnvironmentModel)
extern void U3CU3Ec_U3CRenderRoomObjectU3Eb__12_0_m3A3D691E4761FCEAE888BB48EF3445387A7ADBD5 ();
// 0x00000C33 System.String XapiModel_Actor::get_mbox()
extern void Actor_get_mbox_m5BA24E43EF37B6E89674F0B238FB80181BFDFE97 ();
// 0x00000C34 System.Void XapiModel_Actor::set_mbox(System.String)
extern void Actor_set_mbox_m6A46636907977416D66AADE1564B24765CFFFA18 ();
// 0x00000C35 System.Void XapiModel_Actor::.ctor()
extern void Actor__ctor_mB456A84F1494F15D72922DE8C858ADDA0209C690 ();
// 0x00000C36 System.String XapiModel_Display::get_Username()
extern void Display_get_Username_m65504F10897013C56A66E256C0010C4E5FED4A0E ();
// 0x00000C37 System.Void XapiModel_Display::set_Username(System.String)
extern void Display_set_Username_m760AA8C358AFAF94C2ED0FB77D6A4E7C702EFF64 ();
// 0x00000C38 System.String XapiModel_Display::get_Domain()
extern void Display_get_Domain_mD70D5E987EAC0C0DE5CBCA022F2DEA265DB7D545 ();
// 0x00000C39 System.Void XapiModel_Display::set_Domain(System.String)
extern void Display_set_Domain_m652E07EFB3A1E91BC5799A0A5CEF9F608014A5E9 ();
// 0x00000C3A System.String XapiModel_Display::get_Type()
extern void Display_get_Type_m0771BD1BBFCB38D8B3B5EBBCC4027947B5C2CCD0 ();
// 0x00000C3B System.Void XapiModel_Display::set_Type(System.String)
extern void Display_set_Type_m24FBBFFF4388DB70F52AE1B8E8AA2A5A1765FCAF ();
// 0x00000C3C System.Void XapiModel_Display::.ctor()
extern void Display__ctor_m84BEB549B6DFE019BB5294D9640F2EFE920E4403 ();
// 0x00000C3D XapiModel_Display XapiModel_Verb::get_display()
extern void Verb_get_display_m88056E16B980985355C37CCE79D4427C5E5120E0 ();
// 0x00000C3E System.Void XapiModel_Verb::set_display(XapiModel_Display)
extern void Verb_set_display_mFF7618CC2E4415DA1DDB35A9735AA8374F1A022B ();
// 0x00000C3F System.String XapiModel_Verb::get_id()
extern void Verb_get_id_mD5E8F10B8F2BB1A09590DC21E15AB1B55576F637 ();
// 0x00000C40 System.Void XapiModel_Verb::set_id(System.String)
extern void Verb_set_id_m7E78808817A2658BD09E75F41CDF484D253A2CD5 ();
// 0x00000C41 System.Void XapiModel_Verb::.ctor()
extern void Verb__ctor_m1CFB28D89507D67F7713DDCCD1A236C63C7AA547 ();
// 0x00000C42 System.String XapiModel_Object::get_id()
extern void Object_get_id_m5B6CF1BE1333FB8189548D26F1C8A6D709C4672B ();
// 0x00000C43 System.Void XapiModel_Object::set_id(System.String)
extern void Object_set_id_m74234E18FA198B8827D836D3357C17BD830EEA5A ();
// 0x00000C44 System.Void XapiModel_Object::.ctor()
extern void Object__ctor_m0D9B45E92A7F8E511C49877A2BD7239A318B631A ();
// 0x00000C45 XapiModel_Actor XapiModel_DataModel::get_actor()
extern void DataModel_get_actor_mADBADFE9A03A3E9518652BFEF459349E548E9D7A ();
// 0x00000C46 System.Void XapiModel_DataModel::set_actor(XapiModel_Actor)
extern void DataModel_set_actor_m92F0A1CCF72835D50308414F3CD92BBB9DA2792D ();
// 0x00000C47 XapiModel_Verb XapiModel_DataModel::get_verb()
extern void DataModel_get_verb_m9AE1F34E985660E00D09001A62913530B61B3C22 ();
// 0x00000C48 System.Void XapiModel_DataModel::set_verb(XapiModel_Verb)
extern void DataModel_set_verb_m3A0CD9319DA89EC0B06AFE7785C746706D5E8C1D ();
// 0x00000C49 XapiModel_Object XapiModel_DataModel::get_object()
extern void DataModel_get_object_m757124F4E20673B2617B1484ECAC7035BE11FA5D ();
// 0x00000C4A System.Void XapiModel_DataModel::set_object(XapiModel_Object)
extern void DataModel_set_object_m711B3D4B46158052E276E938D94B877DA18C54F8 ();
// 0x00000C4B System.Void XapiModel_DataModel::.ctor()
extern void DataModel__ctor_m4304E75B7C98FBF0CAFAE1D78B6B87F5E2DF5E50 ();
// 0x00000C4C System.Void Helper_Wrapper`1::.ctor()
// 0x00000C4D System.Void RoomUtility_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m104E2E0E93C4ADD4C21939E43CD448A2C9B077DB ();
// 0x00000C4E System.Boolean RoomUtility_<>c__DisplayClass17_0::<UpateRoomDetails>b__0(RoomDetailModel)
extern void U3CU3Ec__DisplayClass17_0_U3CUpateRoomDetailsU3Eb__0_mF439D16A24B9775B00DD4447BB3AC63B4F8D8A37 ();
// 0x00000C4F System.Void Example10_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m2EC6150ABD7BFC381C9E7E4B775CB9662BAD6822 ();
// 0x00000C50 System.Boolean Example10_<>c__DisplayClass4_0::<GetIconFromIconFont>b__0(MaterialUI.Glyph)
extern void U3CU3Ec__DisplayClass4_0_U3CGetIconFromIconFontU3Eb__0_mA7208A3AB0C7BC1779665EFB09A135855B89C924 ();
// 0x00000C51 System.Void Example13_<>c::.cctor()
extern void U3CU3Ec__cctor_m1790DD2ECA68B3BA7A020611414E649920678643 ();
// 0x00000C52 System.Void Example13_<>c::.ctor()
extern void U3CU3Ec__ctor_mC5B45206BF481A77279A84C119D6BEC10344BD00 ();
// 0x00000C53 System.Void Example13_<>c::<OnSimpleSnackbarButtonClicked>b__2_0()
extern void U3CU3Ec_U3COnSimpleSnackbarButtonClickedU3Eb__2_0_m20117736ED22E5198639D5DA74727F31850EFED9 ();
// 0x00000C54 System.Void Example13_<>c::<OnCustomSnackbarButtonClicked>b__3_0()
extern void U3CU3Ec_U3COnCustomSnackbarButtonClickedU3Eb__3_0_mA4B64326D431ACAA5B4BBC24345B9040BD017902 ();
// 0x00000C55 System.Void Example14_<HideWindowAfterSeconds>d__5::.ctor(System.Int32)
extern void U3CHideWindowAfterSecondsU3Ed__5__ctor_m12B6C6F2521697E380F54062203BF3AD8A28460B ();
// 0x00000C56 System.Void Example14_<HideWindowAfterSeconds>d__5::System.IDisposable.Dispose()
extern void U3CHideWindowAfterSecondsU3Ed__5_System_IDisposable_Dispose_mF7CA6E5C9070500D325AFCD96EDDD1F843C24AEF ();
// 0x00000C57 System.Boolean Example14_<HideWindowAfterSeconds>d__5::MoveNext()
extern void U3CHideWindowAfterSecondsU3Ed__5_MoveNext_m5E202C26D21746905ACF710424E4BF8EF802F984 ();
// 0x00000C58 System.Object Example14_<HideWindowAfterSeconds>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHideWindowAfterSecondsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7723BEC00D7BC9EA6621D96A7ECB7EF5AC86FD48 ();
// 0x00000C59 System.Void Example14_<HideWindowAfterSeconds>d__5::System.Collections.IEnumerator.Reset()
extern void U3CHideWindowAfterSecondsU3Ed__5_System_Collections_IEnumerator_Reset_m38A45590143A544FD9463F0D938ACF3F6762A5B3 ();
// 0x00000C5A System.Object Example14_<HideWindowAfterSeconds>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CHideWindowAfterSecondsU3Ed__5_System_Collections_IEnumerator_get_Current_mB921526FC4A4DAE6E49394D7F35464B0F3FFE991 ();
// 0x00000C5B System.Void Example14_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m995D6CF811ACEA4F99C08787D38ECC8F9FFE301D ();
// 0x00000C5C System.Void Example14_<>c__DisplayClass7_0::<OnSimpleListBigVectorButtonClicked>b__1()
extern void U3CU3Ec__DisplayClass7_0_U3COnSimpleListBigVectorButtonClickedU3Eb__1_mF43729E42673F0490929B4A4E7F95D18E9994AFE ();
// 0x00000C5D System.Void Example14_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m5DB4AE4DD92B13303AB79BACE63B3D2778E82895 ();
// 0x00000C5E System.Void Example14_<>c__DisplayClass8_0::<OnSimpleListBigSpriteButtonClicked>b__1()
extern void U3CU3Ec__DisplayClass8_0_U3COnSimpleListBigSpriteButtonClickedU3Eb__1_m3DACFA927719CC5B54658319C4D8CF06B7F6918D ();
// 0x00000C5F System.Void Example14_<>c::.cctor()
extern void U3CU3Ec__cctor_m26ADB8F2A075B1EE3EC9BA1A1D0C6CCD815F1B7D ();
// 0x00000C60 System.Void Example14_<>c::.ctor()
extern void U3CU3Ec__ctor_mC7D97BD561D058ECB1C2472C804B9D5180B9EA94 ();
// 0x00000C61 System.Void Example14_<>c::<OnAlertOneCallbackButtonClicked>b__10_0()
extern void U3CU3Ec_U3COnAlertOneCallbackButtonClickedU3Eb__10_0_m78D47AB8D0A89C3E4913AE365297921B2B479DB5 ();
// 0x00000C62 System.Void Example14_<>c::<OnAlertTwoCallbacksButtonClicked>b__11_0()
extern void U3CU3Ec_U3COnAlertTwoCallbacksButtonClickedU3Eb__11_0_m3132C30710BB3E939D058807B584674413BF6D8A ();
// 0x00000C63 System.Void Example14_<>c::<OnAlertTwoCallbacksButtonClicked>b__11_1()
extern void U3CU3Ec_U3COnAlertTwoCallbacksButtonClickedU3Eb__11_1_mEF8898A0A84196396AE15A0F22783B7B8856E9A3 ();
// 0x00000C64 System.Void Example14_<>c::<OnCheckboxListBigButtonClicked>b__14_0()
extern void U3CU3Ec_U3COnCheckboxListBigButtonClickedU3Eb__14_0_mA6FD074FCAA1D495A3DECA792952860746E7FE72 ();
// 0x00000C65 System.Void Example14_<>c::<OnRadioListBigButtonClicked>b__17_1()
extern void U3CU3Ec_U3COnRadioListBigButtonClickedU3Eb__17_1_m107A409FE98145BF66BAFF2B2EAC87EE63B40B93 ();
// 0x00000C66 System.Void Example14_<>c::<OnOneFieldPromptButtonClicked>b__18_0(System.String)
extern void U3CU3Ec_U3COnOneFieldPromptButtonClickedU3Eb__18_0_m6637AFBC4FCEA64C2ECF92679794A55B021B8B9A ();
// 0x00000C67 System.Void Example14_<>c::<OnOneFieldPromptButtonClicked>b__18_1()
extern void U3CU3Ec_U3COnOneFieldPromptButtonClickedU3Eb__18_1_m27CE77DDA9AAB1B8BEBF3CA97A96962C56DD971E ();
// 0x00000C68 System.Void Example14_<>c::<OnTwoFieldsPromptButtonClicked>b__19_0(System.String,System.String)
extern void U3CU3Ec_U3COnTwoFieldsPromptButtonClickedU3Eb__19_0_m68C1D992C6BF62D749CFDF77A9FE0C7C04D5BB7F ();
// 0x00000C69 System.Void Example14_<>c::<OnTwoFieldsPromptButtonClicked>b__19_1()
extern void U3CU3Ec_U3COnTwoFieldsPromptButtonClickedU3Eb__19_1_m1273B26834082176CEA6F1EB5CF43B6D7BE99BA2 ();
// 0x00000C6A System.Void Example15_<>c::.cctor()
extern void U3CU3Ec__cctor_m96EC7144DC87AFEF2531C1872238552C049843F2 ();
// 0x00000C6B System.Void Example15_<>c::.ctor()
extern void U3CU3Ec__ctor_m8625221AC73F595EE90B38A2E0C1F9F27585AEFA ();
// 0x00000C6C System.Void Example15_<>c::<OnTimePickerButtonClicked>b__0_0(System.DateTime)
extern void U3CU3Ec_U3COnTimePickerButtonClickedU3Eb__0_0_m5D8BB3CB988E592BFDB5DF550DB1EDAFE7E6A1E4 ();
// 0x00000C6D System.Void Example15_<>c::<OnDatePickerButtonClicked>b__1_0(System.DateTime)
extern void U3CU3Ec_U3COnDatePickerButtonClickedU3Eb__1_0_mD0320E02D837F37AB6935E03608230D43B09E65C ();
// 0x00000C6E System.Void Example19_<>c::.cctor()
extern void U3CU3Ec__cctor_m14F55AC3428BC22869C089F29DF32F5765972BD8 ();
// 0x00000C6F System.Void Example19_<>c::.ctor()
extern void U3CU3Ec__ctor_m16811C3F529FB124BFE3C61CF87F8156D697A5F7 ();
// 0x00000C70 System.Void Example19_<>c::<Simple2>b__10_0()
extern void U3CU3Ec_U3CSimple2U3Eb__10_0_mF947AA11BA5AACB6F3C81511BDE8D869F733CAF3 ();
// 0x00000C71 System.Void Example19_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mFF6E605208EC19F64C1367D87696B73D3A50777A ();
// 0x00000C72 System.Void Example19_<>c__DisplayClass14_0::<IssueCoroutine>b__0(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass14_0_U3CIssueCoroutineU3Eb__0_mFF9DAD2D37F68A74D1D30290D9A5966E871430A9 ();
// 0x00000C73 System.Void Example19_<IssueCoroutine>d__14::.ctor(System.Int32)
extern void U3CIssueCoroutineU3Ed__14__ctor_mC01A4B229FFA051D3BFAF5612E8448B7F1DBB3C2 ();
// 0x00000C74 System.Void Example19_<IssueCoroutine>d__14::System.IDisposable.Dispose()
extern void U3CIssueCoroutineU3Ed__14_System_IDisposable_Dispose_mE3F549DCFFC54A1419EAC0B64742C7171CBDDC5A ();
// 0x00000C75 System.Boolean Example19_<IssueCoroutine>d__14::MoveNext()
extern void U3CIssueCoroutineU3Ed__14_MoveNext_m4EEF3440A7392B857C17DD0F4F761EA721D41B8A ();
// 0x00000C76 System.Object Example19_<IssueCoroutine>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIssueCoroutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D63A2AC7D57FE94B35AB01DC486D0C1A8F0D682 ();
// 0x00000C77 System.Void Example19_<IssueCoroutine>d__14::System.Collections.IEnumerator.Reset()
extern void U3CIssueCoroutineU3Ed__14_System_Collections_IEnumerator_Reset_m77C6DBB0764589F07DBC1D93C30F843C3BB39DB9 ();
// 0x00000C78 System.Object Example19_<IssueCoroutine>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CIssueCoroutineU3Ed__14_System_Collections_IEnumerator_get_Current_m34E251F4ABA18E51A66FDED6E68602A3B4A5D6AB ();
// 0x00000C79 System.Void Example19_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mF57255707EA9485C80EBA3C3D30B85E5B62CD64E ();
// 0x00000C7A System.Void Example19_<>c__DisplayClass15_0::<Workaround1>b__0(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass15_0_U3CWorkaround1U3Eb__0_mFD6D54D85C0164D7BC25804FF388E15AF2EAB4C2 ();
// 0x00000C7B System.Void Example19_<Workaround2Coroutine>d__17::.ctor(System.Int32)
extern void U3CWorkaround2CoroutineU3Ed__17__ctor_m27070577FF40EBC117F3880F7FAB9394488B958E ();
// 0x00000C7C System.Void Example19_<Workaround2Coroutine>d__17::System.IDisposable.Dispose()
extern void U3CWorkaround2CoroutineU3Ed__17_System_IDisposable_Dispose_mD2BA7E50E4BD83171B2068C0E4FF245B2800B586 ();
// 0x00000C7D System.Boolean Example19_<Workaround2Coroutine>d__17::MoveNext()
extern void U3CWorkaround2CoroutineU3Ed__17_MoveNext_mD0AB09C1ABA46EC255AC46A2827F36C0E7DE856A ();
// 0x00000C7E System.Object Example19_<Workaround2Coroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWorkaround2CoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DA2847FE3EAD41FBD5639A409D6E065945FCE69 ();
// 0x00000C7F System.Void Example19_<Workaround2Coroutine>d__17::System.Collections.IEnumerator.Reset()
extern void U3CWorkaround2CoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m7147C14CF3F89AA753CE06AD93FA25ACE0B1442F ();
// 0x00000C80 System.Object Example19_<Workaround2Coroutine>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CWorkaround2CoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_mB6DF43445D31CDDE78FE63F7819D7A759016955F ();
// 0x00000C81 System.Void Example19_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m230FACE807584EDC858AF5534E1A7F51FC572E9C ();
// 0x00000C82 System.Void Example19_<>c__DisplayClass18_0::<Workaround2Workaround>b__0(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass18_0_U3CWorkaround2WorkaroundU3Eb__0_m0AD415CF9BEEC3447EE3AA8E14D924113E15E348 ();
// 0x00000C83 System.Void DuoVia.FuzzyStrings.DiceCoefficientExtensions_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m2837CA93BA76BEA1BD6FF63999DE70C7ED15F4AB ();
// 0x00000C84 System.Boolean DuoVia.FuzzyStrings.DiceCoefficientExtensions_<>c__DisplayClass1_0::<DiceCoefficient>b__0(System.String)
extern void U3CU3Ec__DisplayClass1_0_U3CDiceCoefficientU3Eb__0_mFBE3CDAD5A6DE64DECE4579D23C116832D7E0849 ();
// 0x00000C85 System.Boolean DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::get_Alternative()
extern void MetaphoneData_get_Alternative_mB7DCE3926C57C63BDB92FFA4A16D572220A20ED3 ();
// 0x00000C86 System.Void DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::set_Alternative(System.Boolean)
extern void MetaphoneData_set_Alternative_m6E48BC766512AEE587E67802AF09B824ED28C1ED ();
// 0x00000C87 System.Int32 DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::get_PrimaryLength()
extern void MetaphoneData_get_PrimaryLength_m9C8B0E5C544B947612C7AA2C6FF107B2128C3B1B ();
// 0x00000C88 System.Int32 DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::get_SecondaryLength()
extern void MetaphoneData_get_SecondaryLength_m2CF482E0662748FFF71F51BB6812A5D55CFE2ED4 ();
// 0x00000C89 System.Void DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::Add(System.String)
extern void MetaphoneData_Add_m063CA3F76C76D075B469F9349BC7FACE77A54DE3 ();
// 0x00000C8A System.Void DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::Add(System.String,System.String)
extern void MetaphoneData_Add_m3C16654A0FD5C5959417727B43622DDBEEC8DBB1 ();
// 0x00000C8B System.String DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::ToString()
extern void MetaphoneData_ToString_m50EA25D33F652819761907A496CC276AC214163A ();
// 0x00000C8C System.Void DuoVia.FuzzyStrings.DoubleMetaphoneExtensions_MetaphoneData::.ctor()
extern void MetaphoneData__ctor_m0B206E6640950721D07F237B9CA093F3C9AD61ED ();
// 0x00000C8D System.String MaterialUI.EasyTween_EasyTweenObject::get_tag()
extern void EasyTweenObject_get_tag_mB7F6F3B4A5F245D5649CF025A9E5093A5AF7D7CB ();
// 0x00000C8E System.Void MaterialUI.EasyTween_EasyTweenObject::set_tag(System.String)
extern void EasyTweenObject_set_tag_m4A7DFEA930D471F87967FEA683F33F2FB42EBEF9 ();
// 0x00000C8F UnityEngine.GameObject MaterialUI.EasyTween_EasyTweenObject::get_targetGameObject()
extern void EasyTweenObject_get_targetGameObject_m2E81D115122199C97E01E822FAE6DA33358BDCF7 ();
// 0x00000C90 System.Void MaterialUI.EasyTween_EasyTweenObject::set_targetGameObject(UnityEngine.GameObject)
extern void EasyTweenObject_set_targetGameObject_m8A2978935CAA2130FE5CA7CF3780045A30D57766 ();
// 0x00000C91 MaterialUI.Tween_TweenType MaterialUI.EasyTween_EasyTweenObject::get_tweenType()
extern void EasyTweenObject_get_tweenType_mDB55CD224BD88CD8CF6B3A5099FF43249ECC28BE ();
// 0x00000C92 System.Void MaterialUI.EasyTween_EasyTweenObject::set_tweenType(MaterialUI.Tween_TweenType)
extern void EasyTweenObject_set_tweenType_m7DDF4A67C4B6A44836F0B2B349E1526B51797339 ();
// 0x00000C93 UnityEngine.AnimationCurve MaterialUI.EasyTween_EasyTweenObject::get_customCurve()
extern void EasyTweenObject_get_customCurve_mE1EA3412B706357F2329857F525ADB0E36A4FF30 ();
// 0x00000C94 System.Void MaterialUI.EasyTween_EasyTweenObject::set_customCurve(UnityEngine.AnimationCurve)
extern void EasyTweenObject_set_customCurve_m78218F01DA677100919D4DD3E1817CA1FD0466F3 ();
// 0x00000C95 System.Single MaterialUI.EasyTween_EasyTweenObject::get_duration()
extern void EasyTweenObject_get_duration_m0414BBEEEBC5D90970249D905E8BBD996175AD75 ();
// 0x00000C96 System.Void MaterialUI.EasyTween_EasyTweenObject::set_duration(System.Single)
extern void EasyTweenObject_set_duration_m3E5AB832D1FD0097C96BAF2863B176DE50B3462C ();
// 0x00000C97 System.Single MaterialUI.EasyTween_EasyTweenObject::get_delay()
extern void EasyTweenObject_get_delay_m124E944832DAD76ABD3822AC3A8596686399003C ();
// 0x00000C98 System.Void MaterialUI.EasyTween_EasyTweenObject::set_delay(System.Single)
extern void EasyTweenObject_set_delay_mBB95AFA19446E69F2AC56DC6E4624A75E3C0BBAE ();
// 0x00000C99 System.Boolean MaterialUI.EasyTween_EasyTweenObject::get_tweenOnStart()
extern void EasyTweenObject_get_tweenOnStart_m237F587F656041136EEBC233981AAAB6CB4AD55C ();
// 0x00000C9A System.Void MaterialUI.EasyTween_EasyTweenObject::set_tweenOnStart(System.Boolean)
extern void EasyTweenObject_set_tweenOnStart_mFB7B816E110F568143E5190DEDE00A3F33B37431 ();
// 0x00000C9B System.Boolean MaterialUI.EasyTween_EasyTweenObject::get_hasCallback()
extern void EasyTweenObject_get_hasCallback_m0DEC5BFA8CE503F21EDFEDDE43DA81456756DAC0 ();
// 0x00000C9C System.Void MaterialUI.EasyTween_EasyTweenObject::set_hasCallback(System.Boolean)
extern void EasyTweenObject_set_hasCallback_mF00D299024C6C4F7F4C93A9A374A544B9345EF24 ();
// 0x00000C9D UnityEngine.GameObject MaterialUI.EasyTween_EasyTweenObject::get_callbackGameObject()
extern void EasyTweenObject_get_callbackGameObject_m94C03DD13E6527C15AB9609DA8AC0CC4CFEE3B0D ();
// 0x00000C9E System.Void MaterialUI.EasyTween_EasyTweenObject::set_callbackGameObject(UnityEngine.GameObject)
extern void EasyTweenObject_set_callbackGameObject_m00054AFED8B835F29020791CA5902A3F0A4F048F ();
// 0x00000C9F UnityEngine.Component MaterialUI.EasyTween_EasyTweenObject::get_callbackComponent()
extern void EasyTweenObject_get_callbackComponent_mFF87264010BE6C7EB996439D8E467477000E32BB ();
// 0x00000CA0 System.Void MaterialUI.EasyTween_EasyTweenObject::set_callbackComponent(UnityEngine.Component)
extern void EasyTweenObject_set_callbackComponent_m261A30F7329E76EE8CAC6E54F89FB00BAC706382 ();
// 0x00000CA1 System.String MaterialUI.EasyTween_EasyTweenObject::get_callbackComponentName()
extern void EasyTweenObject_get_callbackComponentName_mEC21E28ECE90A89F0324E47A31E92A8731D93C01 ();
// 0x00000CA2 System.Void MaterialUI.EasyTween_EasyTweenObject::set_callbackComponentName(System.String)
extern void EasyTweenObject_set_callbackComponentName_m980B6E4DC4BBD5072FF36ADDCDADB9578E381EED ();
// 0x00000CA3 System.Reflection.MethodInfo MaterialUI.EasyTween_EasyTweenObject::get_callbackMethodInfo()
extern void EasyTweenObject_get_callbackMethodInfo_m2FE080A708F82A8AD01B21BA7B70CCB621B30528 ();
// 0x00000CA4 System.Void MaterialUI.EasyTween_EasyTweenObject::set_callbackMethodInfo(System.Reflection.MethodInfo)
extern void EasyTweenObject_set_callbackMethodInfo_mD63F3F285B73F8C23AC9864109B4C67486601869 ();
// 0x00000CA5 System.String MaterialUI.EasyTween_EasyTweenObject::get_callbackName()
extern void EasyTweenObject_get_callbackName_m5D7BB57F83D96D04D7A4D8F46D9296CD4A3B2C7D ();
// 0x00000CA6 System.Void MaterialUI.EasyTween_EasyTweenObject::set_callbackName(System.String)
extern void EasyTweenObject_set_callbackName_mE44918E296BDAB31B23E289649C64265D7DC70AD ();
// 0x00000CA7 System.Boolean MaterialUI.EasyTween_EasyTweenObject::get_optionsVisible()
extern void EasyTweenObject_get_optionsVisible_mFDE15582E80A159EE0F2E6301AF63E81154BBD21 ();
// 0x00000CA8 System.Void MaterialUI.EasyTween_EasyTweenObject::set_optionsVisible(System.Boolean)
extern void EasyTweenObject_set_optionsVisible_m0600F570A0A745B0B8164257C747866F6CBF9A2B ();
// 0x00000CA9 System.Boolean MaterialUI.EasyTween_EasyTweenObject::get_subOptionsVisible()
extern void EasyTweenObject_get_subOptionsVisible_m92AEC389A821012D0CB70921159289A7550B67D3 ();
// 0x00000CAA System.Void MaterialUI.EasyTween_EasyTweenObject::set_subOptionsVisible(System.Boolean)
extern void EasyTweenObject_set_subOptionsVisible_m59A813D36638AB0512EE081EFDC509695115B73E ();
// 0x00000CAB System.Collections.Generic.List`1<MaterialUI.EasyTween_EasyTweenSubObject> MaterialUI.EasyTween_EasyTweenObject::get_subTweens()
extern void EasyTweenObject_get_subTweens_m753F140D1B884BE703450B74BC103D6CA32C1284 ();
// 0x00000CAC System.Void MaterialUI.EasyTween_EasyTweenObject::set_subTweens(System.Collections.Generic.List`1<MaterialUI.EasyTween_EasyTweenSubObject>)
extern void EasyTweenObject_set_subTweens_m65806B8EEE40627A5048D53DD7F6F7DFE9F35E81 ();
// 0x00000CAD System.Void MaterialUI.EasyTween_EasyTweenObject::.ctor()
extern void EasyTweenObject__ctor_m26134C5A8BBCE86D816F2548515987FF5B48AAAF ();
// 0x00000CAE System.Void MaterialUI.EasyTween_EasyTweenSubObject::.ctor()
extern void EasyTweenSubObject__ctor_m91337A01947BC8BC8E7FCD819DDA2B2753B5FA99 ();
// 0x00000CAF System.Void MaterialUI.MaterialUIScaler_CanvasAreaChangedEvent::.ctor()
extern void CanvasAreaChangedEvent__ctor_m4B47E0343D9CE36876F2624E2DFBE1F77E7DE4DD ();
// 0x00000CB0 System.Void MaterialUI.MaterialRipple_<ScrollCheck>d__87::.ctor(System.Int32)
extern void U3CScrollCheckU3Ed__87__ctor_m93041A2516A537D2D5CE533022B154E01FB82100 ();
// 0x00000CB1 System.Void MaterialUI.MaterialRipple_<ScrollCheck>d__87::System.IDisposable.Dispose()
extern void U3CScrollCheckU3Ed__87_System_IDisposable_Dispose_m68F97647762E3FD93FBF18E76096271F53D3DDE4 ();
// 0x00000CB2 System.Boolean MaterialUI.MaterialRipple_<ScrollCheck>d__87::MoveNext()
extern void U3CScrollCheckU3Ed__87_MoveNext_m1AC9266F333B8F4B373188A80E2B698D6E858043 ();
// 0x00000CB3 System.Object MaterialUI.MaterialRipple_<ScrollCheck>d__87::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScrollCheckU3Ed__87_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD1F945824F15275D1D4DF987FD51CFB5DFF69E8 ();
// 0x00000CB4 System.Void MaterialUI.MaterialRipple_<ScrollCheck>d__87::System.Collections.IEnumerator.Reset()
extern void U3CScrollCheckU3Ed__87_System_Collections_IEnumerator_Reset_m2BAC06A86295781D34E35EC601567200797CCC40 ();
// 0x00000CB5 System.Object MaterialUI.MaterialRipple_<ScrollCheck>d__87::System.Collections.IEnumerator.get_Current()
extern void U3CScrollCheckU3Ed__87_System_Collections_IEnumerator_get_Current_m276EBFBECF986346CC62B421117B94C77AB3E968 ();
// 0x00000CB6 System.Void MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::.ctor(System.Int32)
extern void U3CScrollCheckUpU3Ed__88__ctor_m2348E7E34C648EB5C794E6DFE5A3592E90E1093A ();
// 0x00000CB7 System.Void MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::System.IDisposable.Dispose()
extern void U3CScrollCheckUpU3Ed__88_System_IDisposable_Dispose_mF1728237B624520402C0FD2181BE4B9A4F2B4EFD ();
// 0x00000CB8 System.Boolean MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::MoveNext()
extern void U3CScrollCheckUpU3Ed__88_MoveNext_mD0CB4B7A60AD6CA6DF7AEFB3B1E0FC3047F59A36 ();
// 0x00000CB9 System.Object MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScrollCheckUpU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5237017C88568EF6AE59B134F1AF99847C9D771F ();
// 0x00000CBA System.Void MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::System.Collections.IEnumerator.Reset()
extern void U3CScrollCheckUpU3Ed__88_System_Collections_IEnumerator_Reset_m1BD1E0558471B053CCB14430A5EA841117081F2A ();
// 0x00000CBB System.Object MaterialUI.MaterialRipple_<ScrollCheckUp>d__88::System.Collections.IEnumerator.get_Current()
extern void U3CScrollCheckUpU3Ed__88_System_Collections_IEnumerator_get_Current_mE4403AB1E675D8C58B20D56DA5F968D2C559270A ();
// 0x00000CBC System.Void MaterialUI.MaterialRipple_<SelectCheck>d__89::.ctor(System.Int32)
extern void U3CSelectCheckU3Ed__89__ctor_m1B9C6DFDD4271ACD9AAEFC6E9AB55DEC5F892EC4 ();
// 0x00000CBD System.Void MaterialUI.MaterialRipple_<SelectCheck>d__89::System.IDisposable.Dispose()
extern void U3CSelectCheckU3Ed__89_System_IDisposable_Dispose_m27A55D0F23A580F785A511AAD1CB46EDC7EA9B7B ();
// 0x00000CBE System.Boolean MaterialUI.MaterialRipple_<SelectCheck>d__89::MoveNext()
extern void U3CSelectCheckU3Ed__89_MoveNext_m90633F7FDEE2E56F256FC3BA72782818A01F7E2D ();
// 0x00000CBF System.Object MaterialUI.MaterialRipple_<SelectCheck>d__89::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSelectCheckU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07F877A5B5580F6BF84F52883D92D261AAD73E03 ();
// 0x00000CC0 System.Void MaterialUI.MaterialRipple_<SelectCheck>d__89::System.Collections.IEnumerator.Reset()
extern void U3CSelectCheckU3Ed__89_System_Collections_IEnumerator_Reset_m0A4A2F52E80347823C2FC3356BAF4A9B9DD52659 ();
// 0x00000CC1 System.Object MaterialUI.MaterialRipple_<SelectCheck>d__89::System.Collections.IEnumerator.get_Current()
extern void U3CSelectCheckU3Ed__89_System_Collections_IEnumerator_get_Current_m1EEF5059625583902A4233AFB5452FB1F2BB79D4 ();
// 0x00000CC2 System.Void MaterialUI.AnimatedShadow_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m03C07A33B5D823C60094492F9E60804AADE837A2 ();
// 0x00000CC3 System.Void MaterialUI.AnimatedShadow_<>c__DisplayClass9_0::<SetShadow>b__0(System.Single)
extern void U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__0_m6FBD666837B59BE0E9844B83EE4BAE8E322395A0 ();
// 0x00000CC4 System.Single MaterialUI.AnimatedShadow_<>c__DisplayClass9_0::<SetShadow>b__1()
extern void U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__1_m527F32F5310536F0DA70842483BB94CDFE9A6ED4 ();
// 0x00000CC5 System.Void MaterialUI.AnimatedShadow_<>c__DisplayClass9_0::<SetShadow>b__2()
extern void U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__2_m3E17B0A7F1F847AD641FB83BB7B35230D45DBDFC ();
// 0x00000CC6 System.Void MaterialUI.DialogCheckboxList_OptionSelectedEvent::.ctor(System.Object,System.IntPtr)
extern void OptionSelectedEvent__ctor_m7E28B0533205A202D02FE9C7B27A8C5F8BCAB9D0 ();
// 0x00000CC7 System.Void MaterialUI.DialogCheckboxList_OptionSelectedEvent::Invoke(System.Int32)
extern void OptionSelectedEvent_Invoke_m75921165B03B2D023EF938926EB0DC1DF45FF8C8 ();
// 0x00000CC8 System.IAsyncResult MaterialUI.DialogCheckboxList_OptionSelectedEvent::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void OptionSelectedEvent_BeginInvoke_mE717D1AFA0FD3DC9D7CF5AEF9F5E5A5238E60B25 ();
// 0x00000CC9 System.Void MaterialUI.DialogCheckboxList_OptionSelectedEvent::EndInvoke(System.IAsyncResult)
extern void OptionSelectedEvent_EndInvoke_mDA3FB3F0E657DA05C0E9C6D6B75457E3857017D0 ();
// 0x00000CCA System.Void MaterialUI.MaterialAppBar_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mFF214DCE157AF69C016BF8E8D3E9E77C5C838796 ();
// 0x00000CCB System.Void MaterialUI.MaterialAppBar_<>c__DisplayClass26_0::<SetButtonGraphicColor>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CSetButtonGraphicColorU3Eb__0_m2B651E1927CBF59ED54DCCC04155AC17159DE12F ();
// 0x00000CCC System.Void MaterialUI.MaterialAppBar_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m9927BA2CAF233C9944282F9662A7A9A4304DC082 ();
// 0x00000CCD System.Void MaterialUI.MaterialAppBar_<>c__DisplayClass28_0::<TweenGraphicColor>b__0(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass28_0_U3CTweenGraphicColorU3Eb__0_mF6D6068F2C6B2A7D107553B9F5F54561B91C5D37 ();
// 0x00000CCE UnityEngine.RectTransform MaterialUI.MaterialDropdown_DropdownListItem::get_rectTransform()
extern void DropdownListItem_get_rectTransform_m0A3AACF957244A012D5FF8C92F2E3E2A523A76EE ();
// 0x00000CCF System.Void MaterialUI.MaterialDropdown_DropdownListItem::set_rectTransform(UnityEngine.RectTransform)
extern void DropdownListItem_set_rectTransform_m26AC866743D3718C9F6C5C026B128634A4700605 ();
// 0x00000CD0 UnityEngine.CanvasGroup MaterialUI.MaterialDropdown_DropdownListItem::get_canvasGroup()
extern void DropdownListItem_get_canvasGroup_m80A500FF71E0A28B01C91B87B489D9D2CD669AD9 ();
// 0x00000CD1 System.Void MaterialUI.MaterialDropdown_DropdownListItem::set_canvasGroup(UnityEngine.CanvasGroup)
extern void DropdownListItem_set_canvasGroup_mDD5028057F5453E90F936441DAF72B1AB2F8672E ();
// 0x00000CD2 UnityEngine.UI.Text MaterialUI.MaterialDropdown_DropdownListItem::get_text()
extern void DropdownListItem_get_text_mB385D9F41B2A2A6887917F07CBE431F1EFEBD19D ();
// 0x00000CD3 System.Void MaterialUI.MaterialDropdown_DropdownListItem::set_text(UnityEngine.UI.Text)
extern void DropdownListItem_set_text_m78C44D3B233933D98E56D6CFCFDA5EACAD1ABF9C ();
// 0x00000CD4 UnityEngine.UI.Graphic MaterialUI.MaterialDropdown_DropdownListItem::get_image()
extern void DropdownListItem_get_image_m5259F5FB879993432D0389DAB609076DFBE5D5F9 ();
// 0x00000CD5 System.Void MaterialUI.MaterialDropdown_DropdownListItem::set_image(UnityEngine.UI.Graphic)
extern void DropdownListItem_set_image_mA3241B16B6DF60753316AC0E1B905CF6DDA509CB ();
// 0x00000CD6 System.Void MaterialUI.MaterialDropdown_DropdownListItem::.ctor()
extern void DropdownListItem__ctor_m4AE51D9DF3181E8AD9C54EF9EF6C85A5C9C8C4FB ();
// 0x00000CD7 System.Void MaterialUI.MaterialDropdown_MaterialDropdownEvent::.ctor()
extern void MaterialDropdownEvent__ctor_m78433955EB50200DB3D7377B2FABA811278EA950 ();
// 0x00000CD8 System.Void MaterialUI.MaterialDropdown_<>c__DisplayClass119_0::.ctor()
extern void U3CU3Ec__DisplayClass119_0__ctor_m98C6CF07209D33159F9B8FC8E39A2C1248E2A935 ();
// 0x00000CD9 System.Void MaterialUI.MaterialDropdown_<>c__DisplayClass119_0::<Show>b__4(System.Single)
extern void U3CU3Ec__DisplayClass119_0_U3CShowU3Eb__4_m60149DA2754B77310A15FBE7C7EC1D96137744AC ();
// 0x00000CDA System.Void MaterialUI.MaterialDropdown_<>c__DisplayClass121_0::.ctor()
extern void U3CU3Ec__DisplayClass121_0__ctor_m9876C9DD88945A400A8CFFBA3B13C1862749DF5B ();
// 0x00000CDB System.Void MaterialUI.MaterialDropdown_<>c__DisplayClass121_0::<Hide>b__3(System.Single)
extern void U3CU3Ec__DisplayClass121_0_U3CHideU3Eb__3_m435B4AE95D8FA2BC085D6A66FD0DC9495F89F82E ();
// 0x00000CDC System.Void MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::.ctor(System.Int32)
extern void U3COnSliderValueInitU3Ed__153__ctor_m7F1D86BDB11478FD11BA8E56B0B2ED1313642760 ();
// 0x00000CDD System.Void MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::System.IDisposable.Dispose()
extern void U3COnSliderValueInitU3Ed__153_System_IDisposable_Dispose_m7416FF901C6E466FC250131C5F59FE6CE0223395 ();
// 0x00000CDE System.Boolean MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::MoveNext()
extern void U3COnSliderValueInitU3Ed__153_MoveNext_m9EE1C81D2B8794FE961873153F2C510C562F88D5 ();
// 0x00000CDF System.Object MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnSliderValueInitU3Ed__153_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84D9F0CC64EEF54529870C623F2A35707865F9EB ();
// 0x00000CE0 System.Void MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::System.Collections.IEnumerator.Reset()
extern void U3COnSliderValueInitU3Ed__153_System_Collections_IEnumerator_Reset_mCB5872AED1977316436CD0C04909E9CD284AC21E ();
// 0x00000CE1 System.Object MaterialUI.MaterialSlider_<OnSliderValueInit>d__153::System.Collections.IEnumerator.get_Current()
extern void U3COnSliderValueInitU3Ed__153_System_Collections_IEnumerator_get_Current_m0ACC57CDBA987EFD3C76EFFD6A219B708DFDA907 ();
// 0x00000CE2 System.Void MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::.ctor(System.Int32)
extern void U3CGetDelayedSizeU3Ed__38__ctor_m40F5922DE4DAF7B8E3638675A91ADE7D1C76380A ();
// 0x00000CE3 System.Void MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::System.IDisposable.Dispose()
extern void U3CGetDelayedSizeU3Ed__38_System_IDisposable_Dispose_m5BBA2330173A126A1877E1970CFD0D65AFDF7E07 ();
// 0x00000CE4 System.Boolean MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::MoveNext()
extern void U3CGetDelayedSizeU3Ed__38_MoveNext_m199F04D577B4AE9CA38A04FF326256FFD71A47AA ();
// 0x00000CE5 System.Object MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDelayedSizeU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAA6C7EDFCFBDE1D5E1766526DC49D7FB38A036F ();
// 0x00000CE6 System.Void MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::System.Collections.IEnumerator.Reset()
extern void U3CGetDelayedSizeU3Ed__38_System_Collections_IEnumerator_Reset_m6F1C0BED5594727D0D5DE962BDAA994090772AA4 ();
// 0x00000CE7 System.Object MaterialUI.OverscrollConfig_<GetDelayedSize>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CGetDelayedSizeU3Ed__38_System_Collections_IEnumerator_get_Current_mE6693B9C174CE84C8558C5BBE491F21E16277DB5 ();
// 0x00000CE8 System.Void MaterialUI.PanelExpander_<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_m8CFFFEE81BECA1E2B3F4969336AF0C78007BA2EA ();
// 0x00000CE9 System.Void MaterialUI.PanelExpander_<>c__DisplayClass67_0::<Show>b__3(System.Single)
extern void U3CU3Ec__DisplayClass67_0_U3CShowU3Eb__3_mB98B5A04445F624EDC687EF5417A661716AF43B8 ();
// 0x00000CEA System.Void MaterialUI.PanelExpander_<>c__DisplayClass68_0::.ctor()
extern void U3CU3Ec__DisplayClass68_0__ctor_m7B9B7E29F4FF9F2A5EB6F4B1CEA1F03D21836C3D ();
// 0x00000CEB System.Void MaterialUI.PanelExpander_<>c__DisplayClass68_0::<Hide>b__7(System.Single)
extern void U3CU3Ec__DisplayClass68_0_U3CHideU3Eb__7_m217ADBAE7846F86410C3BDFD71295EEE376B24CE ();
// 0x00000CEC System.Void MaterialUI.ScreenView_OnScreenTransitionUnityEvent::.ctor()
extern void OnScreenTransitionUnityEvent__ctor_mAC57095099E84EC944ECCE2F7601D4B9D0811094 ();
// 0x00000CED System.Void MaterialUI.SnackbarAnimator_<Setup>d__6::.ctor(System.Int32)
extern void U3CSetupU3Ed__6__ctor_m9200961092E3FFD2AAFE4F0EE5597D839C1A5BEF ();
// 0x00000CEE System.Void MaterialUI.SnackbarAnimator_<Setup>d__6::System.IDisposable.Dispose()
extern void U3CSetupU3Ed__6_System_IDisposable_Dispose_mEE88D17E611A41225A7F584B730F152ACA412C41 ();
// 0x00000CEF System.Boolean MaterialUI.SnackbarAnimator_<Setup>d__6::MoveNext()
extern void U3CSetupU3Ed__6_MoveNext_mB7584537923AE0FF8E19233C59C4624EF96523B3 ();
// 0x00000CF0 System.Object MaterialUI.SnackbarAnimator_<Setup>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetupU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE424FDA23426B7C62524DF164681EB5FA3D385 ();
// 0x00000CF1 System.Void MaterialUI.SnackbarAnimator_<Setup>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSetupU3Ed__6_System_Collections_IEnumerator_Reset_m4E600DF3B1633D7BE632FDAAC4520A04DAF1E5DF ();
// 0x00000CF2 System.Object MaterialUI.SnackbarAnimator_<Setup>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSetupU3Ed__6_System_Collections_IEnumerator_get_Current_m83A63929A06057E3AEFF4F507B41C6846E30E8C7 ();
// 0x00000CF3 System.Void MaterialUI.TabView_<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m2ACD24A5A82F392E55941DDAA5571A409E1AED4C ();
// 0x00000CF4 System.Void MaterialUI.TabView_<>c__DisplayClass70_1::.ctor()
extern void U3CU3Ec__DisplayClass70_1__ctor_mFAC930CF73C498EEF4C48E1246F4FA010D14A2A2 ();
// 0x00000CF5 System.Void MaterialUI.TabView_<>c__DisplayClass70_1::<SetPage>b__0(System.Single)
extern void U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__0_mF0D7714DD80BC60F670A2E002E08312842B7AFF8 ();
// 0x00000CF6 System.Single MaterialUI.TabView_<>c__DisplayClass70_1::<SetPage>b__1()
extern void U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__1_mFDD2F7F450CDA9F047A6FE2B0B7D9E6BCDF277AF ();
// 0x00000CF7 System.Single MaterialUI.TabView_<>c__DisplayClass70_1::<SetPage>b__2()
extern void U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__2_mEB84AE71EFC28B7A3F9CF6F23C3CF734C8E8AE0B ();
// 0x00000CF8 System.Void MaterialUI.ToastAnimator_<WaitTime>d__23::.ctor(System.Int32)
extern void U3CWaitTimeU3Ed__23__ctor_m70E166FF4CB68CD4DE52D36E0A13763E85907996 ();
// 0x00000CF9 System.Void MaterialUI.ToastAnimator_<WaitTime>d__23::System.IDisposable.Dispose()
extern void U3CWaitTimeU3Ed__23_System_IDisposable_Dispose_m6DE32DB29176AA4A70757C2DDD69B26D5A3D07DC ();
// 0x00000CFA System.Boolean MaterialUI.ToastAnimator_<WaitTime>d__23::MoveNext()
extern void U3CWaitTimeU3Ed__23_MoveNext_m3D25B9847BC8CB9BF1A9650149E07D176C641F2F ();
// 0x00000CFB System.Object MaterialUI.ToastAnimator_<WaitTime>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitTimeU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EBAEE344A682F12394BA86E16BFC3448238B0C3 ();
// 0x00000CFC System.Void MaterialUI.ToastAnimator_<WaitTime>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitTimeU3Ed__23_System_Collections_IEnumerator_Reset_mB0FB99C99C6C404B5F3BA308605982574C8B4D07 ();
// 0x00000CFD System.Object MaterialUI.ToastAnimator_<WaitTime>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitTimeU3Ed__23_System_Collections_IEnumerator_get_Current_m354F94063D3DFCCB477B0D0581F5268160FB657B ();
// 0x00000CFE System.Void MaterialUI.MaterialIconHelper_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m526A4A9EA0A60E6D81F5447F33DE71515CEA4C1D ();
// 0x00000CFF System.Boolean MaterialUI.MaterialIconHelper_<>c__DisplayClass4_0::<GetIcon>b__0(MaterialUI.Glyph)
extern void U3CU3Ec__DisplayClass4_0_U3CGetIconU3Eb__0_mD24EA926AF896937C4FFA8DBB38C1009B59A36C6 ();
// 0x00000D00 System.Void MaterialUI.MaterialUIIconHelper_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m8672F4EC45FA31085C4E9B27C927124CBBA18054 ();
// 0x00000D01 System.Boolean MaterialUI.MaterialUIIconHelper_<>c__DisplayClass3_0::<GetIcon>b__0(MaterialUI.Glyph)
extern void U3CU3Ec__DisplayClass3_0_U3CGetIconU3Eb__0_mBDDFE4B33384B35DAF8B87B9BEDC97946212355E ();
// 0x00000D02 System.Collections.Generic.Queue`1<T> MaterialUI.TweenManager_TweenQueue`1::get_tweens()
// 0x00000D03 T MaterialUI.TweenManager_TweenQueue`1::GetTween()
// 0x00000D04 System.Void MaterialUI.TweenManager_TweenQueue`1::.ctor()
// 0x00000D05 System.Void MaterialUI.TweenManager_<>c__DisplayClass18_0`1::.ctor()
// 0x00000D06 T MaterialUI.TweenManager_<>c__DisplayClass18_0`1::<TweenValue>b__0()
// 0x00000D07 T MaterialUI.TweenManager_<>c__DisplayClass18_0`1::<TweenValue>b__1()
// 0x00000D08 System.Void MaterialUI.TweenManager_<>c__DisplayClass19_0`1::.ctor()
// 0x00000D09 T MaterialUI.TweenManager_<>c__DisplayClass19_0`1::<TweenValue>b__0()
// 0x00000D0A System.Void MaterialUI.TweenManager_<>c__DisplayClass21_0`1::.ctor()
// 0x00000D0B T MaterialUI.TweenManager_<>c__DisplayClass21_0`1::<TweenTweenValueCustom>b__0()
// 0x00000D0C T MaterialUI.TweenManager_<>c__DisplayClass21_0`1::<TweenTweenValueCustom>b__1()
// 0x00000D0D System.Void MaterialUI.TweenManager_<>c__DisplayClass22_0`1::.ctor()
// 0x00000D0E T MaterialUI.TweenManager_<>c__DisplayClass22_0`1::<TweenValueCustom>b__0()
// 0x00000D0F System.Void MaterialUI.TweenManager_<>c::.cctor()
extern void U3CU3Ec__cctor_mF665BC097A41CA8FC3894E71D0C5303B26FDA7C0 ();
// 0x00000D10 System.Void MaterialUI.TweenManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mDBE7C576B0AB15B1028E34ADFEB74441A9BFD9C1 ();
// 0x00000D11 System.Void MaterialUI.TweenManager_<>c::<TimedCallback>b__24_0(System.Single)
extern void U3CU3Ec_U3CTimedCallbackU3Eb__24_0_m3BD2C4BDCE4483B2721AB23C89BB34CE1EA35679 ();
// 0x00000D12 System.Void MaterialUI.TweenManager_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m272952AC1C7BFA2F4A9224B4FE7500BD6BD4FF30 ();
// 0x00000D13 System.Single MaterialUI.TweenManager_<>c__DisplayClass26_0::<TweenFloat>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CTweenFloatU3Eb__0_mF45C909FDD5420FB35FD767D6F1226B2AB3E95F0 ();
// 0x00000D14 System.Single MaterialUI.TweenManager_<>c__DisplayClass26_0::<TweenFloat>b__1()
extern void U3CU3Ec__DisplayClass26_0_U3CTweenFloatU3Eb__1_m19667DC3922012DA4D52FED525E0F5FD8069780D ();
// 0x00000D15 System.Void MaterialUI.TweenManager_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m032723C67D74381580F394A026B4CC279B6EA2B0 ();
// 0x00000D16 System.Single MaterialUI.TweenManager_<>c__DisplayClass27_0::<TweenFloat>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CTweenFloatU3Eb__0_mF6E0BF7110761FE5877621F1D68C298891DEC185 ();
// 0x00000D17 System.Void MaterialUI.TweenManager_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mAEB30B2325EFA86E7E4EBFD4C2250DB0E7AC22C8 ();
// 0x00000D18 System.Single MaterialUI.TweenManager_<>c__DisplayClass29_0::<TweenFloatCustom>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CTweenFloatCustomU3Eb__0_mA5ED021D0D6164959893B3F93879C3C35FF93809 ();
// 0x00000D19 System.Single MaterialUI.TweenManager_<>c__DisplayClass29_0::<TweenFloatCustom>b__1()
extern void U3CU3Ec__DisplayClass29_0_U3CTweenFloatCustomU3Eb__1_m8902E16FE21D7F811E4967A11BC2CA8BC0ED2462 ();
// 0x00000D1A System.Void MaterialUI.TweenManager_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m597E943A45A66A6F3A62CA7B2B3562CDBAA36D31 ();
// 0x00000D1B System.Single MaterialUI.TweenManager_<>c__DisplayClass30_0::<TweenFloatCustom>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CTweenFloatCustomU3Eb__0_mDAD50934A0DAE978FFFFF6CDC721EA50F53921B7 ();
// 0x00000D1C System.Void MaterialUI.TweenManager_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m21843AAFC1095C81F4EAE1C686B7F5DAB1A2E720 ();
// 0x00000D1D System.Int32 MaterialUI.TweenManager_<>c__DisplayClass33_0::<TweenInt>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CTweenIntU3Eb__0_m5834C37B4DB3BABC4297E316D56E67949B6C42C2 ();
// 0x00000D1E System.Int32 MaterialUI.TweenManager_<>c__DisplayClass33_0::<TweenInt>b__1()
extern void U3CU3Ec__DisplayClass33_0_U3CTweenIntU3Eb__1_m8D1A4D914C6EB3FB8A491C9FB7F7CB8B72F0F44E ();
// 0x00000D1F System.Void MaterialUI.TweenManager_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m6C512F0B77F16750E6985EB16466FB7538C1E237 ();
// 0x00000D20 System.Int32 MaterialUI.TweenManager_<>c__DisplayClass34_0::<TweenInt>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CTweenIntU3Eb__0_m9E831B956695EBF8C1BBD10DEB93D57636845194 ();
// 0x00000D21 System.Void MaterialUI.TweenManager_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mDBD077B697AF2073D5CF77B0634821D87D08D861 ();
// 0x00000D22 System.Int32 MaterialUI.TweenManager_<>c__DisplayClass36_0::<TweenIntCustom>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CTweenIntCustomU3Eb__0_mB81D0865345746B37B2EA788737AB12AA41E4BF3 ();
// 0x00000D23 System.Int32 MaterialUI.TweenManager_<>c__DisplayClass36_0::<TweenIntCustom>b__1()
extern void U3CU3Ec__DisplayClass36_0_U3CTweenIntCustomU3Eb__1_mCC7F3D7271DC1C4DDC69E51BE14958DD5ED63909 ();
// 0x00000D24 System.Void MaterialUI.TweenManager_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m8BFEC34A8B82FDB368F968CE93EC56DF5FC59041 ();
// 0x00000D25 System.Int32 MaterialUI.TweenManager_<>c__DisplayClass37_0::<TweenIntCustom>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CTweenIntCustomU3Eb__0_mF2B634416D16DD554026BD8F9AFF07E98CE30087 ();
// 0x00000D26 System.Void MaterialUI.TweenManager_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mB981D307D7DF6B6EE16EE1453028043BBEC76D32 ();
// 0x00000D27 UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass40_0::<TweenVector2>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CTweenVector2U3Eb__0_m7E39A4BE609F823456631A3F3617918D2E045FF1 ();
// 0x00000D28 UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass40_0::<TweenVector2>b__1()
extern void U3CU3Ec__DisplayClass40_0_U3CTweenVector2U3Eb__1_m69EEA4DB74F642553392265870303ACDEC1DE380 ();
// 0x00000D29 System.Void MaterialUI.TweenManager_<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mC302124A0E4A79795F192B63C68CCBCA7EEEB940 ();
// 0x00000D2A UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass41_0::<TweenVector2>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CTweenVector2U3Eb__0_m8B1C28687BFADC12A2DD9855ABAE5CB95D2072BF ();
// 0x00000D2B System.Void MaterialUI.TweenManager_<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m29BFF7B4D0BAE35C0E2E149C2D249B07AFD88186 ();
// 0x00000D2C UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass43_0::<TweenVector2Custom>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CTweenVector2CustomU3Eb__0_m4310BB95FE6254A9FA8DB8351830D3F1AEEAB1EC ();
// 0x00000D2D UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass43_0::<TweenVector2Custom>b__1()
extern void U3CU3Ec__DisplayClass43_0_U3CTweenVector2CustomU3Eb__1_m53974F90739EDD03331F34F7D05821151684DA67 ();
// 0x00000D2E System.Void MaterialUI.TweenManager_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m34D7420FB10380F8F332DF7F45A53AB90540BFA0 ();
// 0x00000D2F UnityEngine.Vector2 MaterialUI.TweenManager_<>c__DisplayClass44_0::<TweenVector2Custom>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CTweenVector2CustomU3Eb__0_m4288DA26BCD3D5393663C6296F2B78E0514176C6 ();
// 0x00000D30 System.Void MaterialUI.TweenManager_<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m174737F2C03B3C9E7E166E28425D4D5110C0B814 ();
// 0x00000D31 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass47_0::<TweenVector3>b__0()
extern void U3CU3Ec__DisplayClass47_0_U3CTweenVector3U3Eb__0_m154428AD77878642CB083E65F207673405777A2A ();
// 0x00000D32 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass47_0::<TweenVector3>b__1()
extern void U3CU3Ec__DisplayClass47_0_U3CTweenVector3U3Eb__1_m6DDA20967266BFCE36ED32365D3E53A3CA1A8842 ();
// 0x00000D33 System.Void MaterialUI.TweenManager_<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m36EC395ED756CB8599B4CF7082D3B6E95A19DBBD ();
// 0x00000D34 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass48_0::<TweenVector3>b__0()
extern void U3CU3Ec__DisplayClass48_0_U3CTweenVector3U3Eb__0_m24682362285B5EADB23BA63CEFD875BE2CE1238B ();
// 0x00000D35 System.Void MaterialUI.TweenManager_<>c__DisplayClass50_0::.ctor()
extern void U3CU3Ec__DisplayClass50_0__ctor_mB76ACD26212574CCBD9BB6FAA38B75972790571D ();
// 0x00000D36 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass50_0::<TweenVector3Custom>b__0()
extern void U3CU3Ec__DisplayClass50_0_U3CTweenVector3CustomU3Eb__0_mAE91C5AC66A663A8FB0CB532DA0DCD1C88DA1541 ();
// 0x00000D37 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass50_0::<TweenVector3Custom>b__1()
extern void U3CU3Ec__DisplayClass50_0_U3CTweenVector3CustomU3Eb__1_m7744D53C1AADB493922600E6002DCD77503FCD38 ();
// 0x00000D38 System.Void MaterialUI.TweenManager_<>c__DisplayClass51_0::.ctor()
extern void U3CU3Ec__DisplayClass51_0__ctor_m66322FB784B6CB7990F9FCF36DE01D797BA378B7 ();
// 0x00000D39 UnityEngine.Vector3 MaterialUI.TweenManager_<>c__DisplayClass51_0::<TweenVector3Custom>b__0()
extern void U3CU3Ec__DisplayClass51_0_U3CTweenVector3CustomU3Eb__0_mA191E6DFDEB5D769BC61C5C28D3DF8074802C18C ();
// 0x00000D3A System.Void MaterialUI.TweenManager_<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_m73908220ECB8349B9CDB280240C32398C435757C ();
// 0x00000D3B UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass54_0::<TweenVector4>b__0()
extern void U3CU3Ec__DisplayClass54_0_U3CTweenVector4U3Eb__0_m3817E490A6EB9AFBB907176F5EFBE024AF51299F ();
// 0x00000D3C UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass54_0::<TweenVector4>b__1()
extern void U3CU3Ec__DisplayClass54_0_U3CTweenVector4U3Eb__1_m1C3AFBAC969CCA8B26AFEEB62EB8222C82DE2675 ();
// 0x00000D3D System.Void MaterialUI.TweenManager_<>c__DisplayClass55_0::.ctor()
extern void U3CU3Ec__DisplayClass55_0__ctor_m60E751A4F9D1AF2ECE9E6C8D6E6521AA2FFFE1C6 ();
// 0x00000D3E UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass55_0::<TweenVector4>b__0()
extern void U3CU3Ec__DisplayClass55_0_U3CTweenVector4U3Eb__0_m60033FAABDC8217B0738B83CEEBBBC5CF0DC66E2 ();
// 0x00000D3F System.Void MaterialUI.TweenManager_<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_mC6A5416D56FC7B53D2B3889F277C6F3D5DF4C7B3 ();
// 0x00000D40 UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass57_0::<TweenVector4Custom>b__0()
extern void U3CU3Ec__DisplayClass57_0_U3CTweenVector4CustomU3Eb__0_m5B13C5523A6B905037520D9E73C12FC5B8D8E103 ();
// 0x00000D41 UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass57_0::<TweenVector4Custom>b__1()
extern void U3CU3Ec__DisplayClass57_0_U3CTweenVector4CustomU3Eb__1_m65C6FE9544BFFB8847991DCC19D4A0F55880F0DB ();
// 0x00000D42 System.Void MaterialUI.TweenManager_<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_mB073F944DA40D2AFBF8D5CC2029F66AF6C946A61 ();
// 0x00000D43 UnityEngine.Vector4 MaterialUI.TweenManager_<>c__DisplayClass58_0::<TweenVector4Custom>b__0()
extern void U3CU3Ec__DisplayClass58_0_U3CTweenVector4CustomU3Eb__0_m517112B11D2A59947AC0A3EDDCC567EDA8E6E19E ();
// 0x00000D44 System.Void MaterialUI.TweenManager_<>c__DisplayClass61_0::.ctor()
extern void U3CU3Ec__DisplayClass61_0__ctor_mBE2AB1C949FECA762AD5858927C7751C63A954EB ();
// 0x00000D45 UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass61_0::<TweenColor>b__0()
extern void U3CU3Ec__DisplayClass61_0_U3CTweenColorU3Eb__0_m4899ACD811932106797C41399A01516CAAC4989D ();
// 0x00000D46 UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass61_0::<TweenColor>b__1()
extern void U3CU3Ec__DisplayClass61_0_U3CTweenColorU3Eb__1_mD7DC9D667B29FC57322C796D789F116915FC892F ();
// 0x00000D47 System.Void MaterialUI.TweenManager_<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m0C100D5A36426B07CCEDF17BF3D2EF885890DFA9 ();
// 0x00000D48 UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass62_0::<TweenColor>b__0()
extern void U3CU3Ec__DisplayClass62_0_U3CTweenColorU3Eb__0_m4ECDB3EA506B3B9FC21AC0D1D23B9B71D2C80D2D ();
// 0x00000D49 System.Void MaterialUI.TweenManager_<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_mB97593B69E45571632CBD1CF661D528C1B363163 ();
// 0x00000D4A UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass64_0::<TweenColorCustom>b__0()
extern void U3CU3Ec__DisplayClass64_0_U3CTweenColorCustomU3Eb__0_mE8D366A3FDFE11A711248C2CAF08D5AE37068690 ();
// 0x00000D4B UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass64_0::<TweenColorCustom>b__1()
extern void U3CU3Ec__DisplayClass64_0_U3CTweenColorCustomU3Eb__1_m22CF1ECD9E95AFF1766613142D0FF1DD95DC7E48 ();
// 0x00000D4C System.Void MaterialUI.TweenManager_<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_m2E19E057C9E9FE4EC61B68A0B8C578D38EBE820D ();
// 0x00000D4D UnityEngine.Color MaterialUI.TweenManager_<>c__DisplayClass65_0::<TweenColorCustom>b__0()
extern void U3CU3Ec__DisplayClass65_0_U3CTweenColorCustomU3Eb__0_m745AE216E29F1C2F4B77093234C0EDEF51696827 ();
// 0x00000D4E System.Void MaterialUI.IconDecoder_<>c::.cctor()
extern void U3CU3Ec__cctor_m4BFCF57DC71897088892FDD9551A80917D5B8121 ();
// 0x00000D4F System.Void MaterialUI.IconDecoder_<>c::.ctor()
extern void U3CU3Ec__ctor_mCB8F94A1CA144C35FB02113C7DFBE0CDC999E724 ();
// 0x00000D50 System.String MaterialUI.IconDecoder_<>c::<Decode>b__1_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CDecodeU3Eb__1_0_m84EEDBA70E67F935D67C1BCBD05706A1B018B597 ();
// 0x00000D51 System.Void Assets.Base.DeepLink.ProcessDeepLinkMngr_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mA8FF201A577C09BE5FBD251CB96DF006EDA209E3 ();
// 0x00000D52 System.Boolean Assets.Base.DeepLink.ProcessDeepLinkMngr_<>c__DisplayClass9_0::<RenderRoom>b__0(RoomDetailModel)
extern void U3CU3Ec__DisplayClass9_0_U3CRenderRoomU3Eb__0_m9A427F27FF22A753C64A7EEE559AA7C3DFE164D4 ();
// 0x00000D53 System.Boolean Assets.Base.DeepLink.ProcessDeepLinkMngr_<>c__DisplayClass9_0::<RenderRoom>b__1(ModeTypeModel)
extern void U3CU3Ec__DisplayClass9_0_U3CRenderRoomU3Eb__1_m8DC507B17E2100F97B3F9931468E805B12D23FA2 ();
static Il2CppMethodPointer s_methodPointers[3411] = 
{
	DrawingController_DisableWriting_m2131923E352FD568A2C10D8FD776A8403EC240F0,
	DrawingController_EnableWriting_m37344EED1A62573BF41ABBA91CDBDB78A1B01495,
	DrawingController_Start_mA7971C3A7FCE358ED961D73F99D30AD8BD580B54,
	DrawingController_Update_m5FF76A3D117703BA89A797498A2F39563061AEEC,
	DrawingController_InterpolatePositions_mE2321F262804BB3EEC9E883082C8C9A33C92211D,
	DrawingController_drawCircle_m6F7DC7E0073B39CFBF00CB111BEB951AAE99EA22,
	DrawingController_Brush_m2CB8865174DEFDE7A0DC6025F955B7D99D42DC5C,
	DrawingController_SetPixel_m58A927469AD7D70224B94E2EB0F9B5EB0D8FE9F7,
	DrawingController_SetBrushColor_m9C12F7CBD17FBF60E3B462A81C1CDFADFE0C7E96,
	DrawingController_SetEraser_m08A06C73D7A3440C16C160C819037E1B4D24DA74,
	DrawingController_ClearPanel_mCF52907FCCB1DFB007FAD9B36AF827B2CFE47293,
	DrawingController_OnDestroy_m19D720854FD2068164251FBCA6675A08244AB369,
	DrawingController__ctor_m0C87419280108FC18D123546FDD17A6A55E1F83D,
	TextBoard_ExportValue_m38D033770AEA4D207B36100EA7F2797DA325302D,
	TextBoard_importvalue_m1865965F44E51908604202CB2EC23DBA4E7EA4EE,
	TextBoard_DelayFunction_m148DC4EBE0DDCD72E7B75C8BE35A123A58E3681A,
	TextBoard_OnKeyDown_m8D2E3CC35B4CA5F4B96BEC1C6C755A7ACDD20EA1,
	TextBoard_OnKeyUP_m7263444BADA996D7B3C0085956FE5E062C9E39E9,
	TextBoard_OnKey_m362F0BF901A717091BD08A028411EF9501A6BF70,
	TextBoard__ctor_m4F719D7DDBCC1161287AE658CC3BDAAD424B9D6F,
	ExtensionObject_SetID_m2616110E4B0837BA66CEC58EE6351D759D295754,
	ExtensionObject_CheckForRelatives_mAE49FDD221E89D7EFB9EAE99FCA4DBC188B12D78,
	ExtensionObject__ctor_mD38062EB15F56503651F714C072D486BBCD1D4D5,
	ExtensionObject__cctor_mCE3EA9996F6C72423022DB8F369AE8C21C5D2036,
	ProxyAPI_get_unityWebRequest_m6292BD6503A519B95D34D6D52BDD2C3C89AA9444,
	ProxyAPI_set_unityWebRequest_m3AD98316E1378965EF1ECBBDCEE22B7E24FFE6FA,
	ProxyAPI_PostReq_mD1E30D8348F7C1998A735B42B8D4015497D83A1E,
	ProxyAPI_PostRequest_m6F06C0DFD82AD3E87681FF7C9062CBEC99513C83,
	ProxyAPI_GetRequest_m4E2C093EA858872413FDFEC041E89D4923B7958E,
	ProxyAPI_GetRequestData_m43ACA4865541A9A437FD0BC4E080BE872339FDB0,
	ProxyAPI_GetReq_mE5FC0B227D6C7FFC6716910932F1176DE25C190F,
	ProxyAPI_UploadFile_mB6ADD6E88AE12C89058E8AEF0FD7DDFD18BC8774,
	ProxyAPI_UploadMultiple_mB5C332AEA9ABDBC33F43D6A8F09C25B2A79FEDE6,
	ProxyAPI_GetFileUrl_mAC8B4A47E01D331DCD62C26EB2B1A60233DAEF35,
	ProxyAPI_DeleteRequest_m4D0F818C77E433307D8C28B98309D37094A74062,
	ProxyAPI__DeleteReq_m8181639B9ECE78CC248FEADDD05FF79C9293E25E,
	ProxyAPI__ctor_m3F6BE70D765716DC081884C885CBEC604D9B3A73,
	RoomManagementAPI__ctor_m2304E4B5422EE39795C668075F0057738225DCE5,
	RoomManagementAPI__cctor_mC2729D6769F550701F37D70173322D5951166838,
	RoomManager_get_RoomName_m63350857141E1E8D966E8731D590D95F1195400D,
	RoomManager_set_RoomName_m31EBC51565D7C80FEEA83C3E502A8F629BF1DBE0,
	RoomManager_get_IsRender_mE23C30B9C87D34C25983105DF70CA238B7C6776D,
	RoomManager_set_IsRender_mCAA6A5E8B80E46E5E77043DA00ABB3E50BA58DF6,
	RoomManager_Start_m3BEB349960C5482D5D17C1747EE1DB18ADDE7662,
	RoomManager_RoomRender_m47430319B45D2A695FD8BD8E4E484E83F854C646,
	RoomManager_GetEnvironments_mCEBC114649D4C78297F4B6466E2F9E2968CDFC26,
	RoomManager_GameObjectRoomEventHandler_mD5DCFD0275D020628E5FED53A6903DC206E7D106,
	RoomManager_RenderRoomObJ_m2D54561FB9B86CE82314F29EC973012DA6774D40,
	RoomManager_RenderRoom_m438F0146EAFD8EF5716C50E06CF74C0696A58600,
	RoomManager_RenderRoom_m1DECFB5A63802CC947C9D20369DCDD3B3AB861E7,
	RoomManager_GameObjectEventHandler_mA3D1B701303969CB8D152CBEE6B41275AEFA38B4,
	RoomManager_LoadEvent_m6EFC4998669AC39C2944C4DAD10E7BF711BC2667,
	RoomManager_RoomRenderClear_mE993717F563866ED0668BE8CF086175650A4087A,
	RoomManager_RenderTitle_m2E68E79436DCF4A32EB3406280385F6789077AF5,
	RoomManager_RoomsRenderView_mA02B651AEEE2C124484E11982DC05705623801AA,
	RoomManager_OpenRoomEvent_m2BE66163E2C2C39D0F5207FA375C2CF89935EF06,
	RoomManager_openRoom_m9D11789EAEE5ACDAF3C9929DE0F10C337ABDBB99,
	RoomManager_DeleteRoomData_m285B2039D5F3FFE20565B90E974E7A4AC33ABE47,
	RoomManager_DeleteRoomList_m0930D24D89EEE2FA42576E2466362D54A309BB9F,
	RoomManager_DeleteListViewRoom_m8E00A94E0F50777D4FCF7EE33F989154E7FD0A4A,
	RoomManager__ctor_m647CD75713362D4AE755130C4C3148F47AF980C3,
	RoomManager_U3CGameObjectRoomEventHandlerU3Eb__22_0_mD8E786072A27B8D5E759F361EED6C12336BF7165,
	RoomManager_U3CGameObjectEventHandlerU3Eb__26_0_m92786B7DC786E507F62DF402BE365D31BF97CD34,
	RoomManager_U3CRoomsRenderViewU3Eb__30_3_m43A3A89BA7ECEEA5CA0D668997EF564E19128120,
	RoomStateManager_Start_m8A8C7EDD83AB6EBA70242713F71A2C9C88B8C61F,
	RoomStateManager_Update_mE204472A689A436AED50F92D8F49240FB5BA8435,
	RoomStateManager_RenderRoomObject_m68E340185666F4889B7887AED67E2096BFF7627B,
	RoomStateManager_RoomSaveData_mD6E7E4984D7BC261CB41B017B0DED4D946DDC240,
	RoomStateManager_RoomRenderData_m3F10B7FC6B009209C16ECFE059790EF7512D8B5D,
	RoomStateManager_PerviousScene_mC4BE3BFFA3B5CCA406FBE182A1FF3009D509057B,
	RoomStateManager__ctor_mA73581202F7338531C151967CD9DD8B963405B67,
	SceneObject__ctor_mA213B91EE80D8FABD13EEAC98A1D3D1B5036B1BB,
	PositionObject_get_x_m91A18DC5F9632B883BA76D6DAB51FB12D015428A,
	PositionObject_set_x_mD12F10E300C76DE5F6D826B171E5511D56071CF8,
	PositionObject_get_y_m8ACF2EEC5A0FF1216547E96DA9E5F1BE8A30D523,
	PositionObject_set_y_m21927C781AC12FC0F10A1ACEB0DDC3CA32F234A9,
	PositionObject_get_z_m30B004EA666A4D3FE013881BEFB9B6F43475D99D,
	PositionObject_set_z_m317D3FC1745CA6B590262B5FC11AE7C1A087F594,
	PositionObject__ctor_m99427B687D53B6FB73E25DDECA9AD3636198DD65,
	LocalScaleObject_get_x_m792638A1830FDF7137FC2ED35749B17D602E2496,
	LocalScaleObject_set_x_m5C4BE0FCDE8167567E12E7A2EBD6DACA0B7CE40A,
	LocalScaleObject_get_y_m80731F62762689E6B61A328112642F8632827AAF,
	LocalScaleObject_set_y_m0B7D1723B724489DAD4734612260A98FCBC75389,
	LocalScaleObject_get_z_mE0EB41F8CE326D8C93D1061AE670522A605DC904,
	LocalScaleObject_set_z_mE04D7EE77B0C3C84A12F0638191C28B348BE667E,
	LocalScaleObject__ctor_m7C0F776DD4884128D93FBAD33C7F06067D573FE5,
	RotationObject_get_x_m6192CCBC031508889E8C809A22D9F290187BED96,
	RotationObject_set_x_mCE2C08178F31E28A4237697AEE0ECCB07A6B5192,
	RotationObject_get_y_m172B00CA6B967A95EB12AB0AD57776EA532D6AAE,
	RotationObject_set_y_mE6E11E731EA2853480DDF876BE608027DC9B9ED7,
	RotationObject_get_z_m65671FEE94919C3C993E803AA4D362DC594F0E9C,
	RotationObject_set_z_mA8E8328B7DE001791054DE67D98A7E31A84FD2E5,
	RotationObject__ctor_m1DB1A07D6D10F7F03BB5DEC7648E5189D183BC85,
	CollabarationSceneObject_get_collaboration_id_m812F7E461A88035730B89E76E9ED55A869FF0678,
	CollabarationSceneObject_set_collaboration_id_m5B76B401CA844AC0E842C69F924535E0DD62992B,
	CollabarationSceneObject_get_objectName_m743AC3E854DBE78B7A12D2255821B60D0B164F3E,
	CollabarationSceneObject_set_objectName_mB14949A09B5F39C4458EBEAAC4A82B83ECE10691,
	CollabarationSceneObject_get_fileType_mEAFCB5CD80AFA7ECCFAA06D3E2D27CDF9F3047F0,
	CollabarationSceneObject_set_fileType_m25FF26B2B448DE6C3FBEB0322D578281ECCEEF39,
	CollabarationSceneObject_get_isAssestBundle_m9153546A3BBCC756C303A31367D2829AFE3F3564,
	CollabarationSceneObject_set_isAssestBundle_m640788F71687B04B6257236FE7974EC0D0BE63C0,
	CollabarationSceneObject_get_isScriptAttached_m85465000FD3F9BCBB1D1366DCDFB3BE6B80EF569,
	CollabarationSceneObject_set_isScriptAttached_m32473B0026E1C2455CF89E9DCDBB1757DD5ADA09,
	CollabarationSceneObject_get_created_at_m9BAD9B29BDAD0F63700D01D38AA8145231D95C09,
	CollabarationSceneObject_set_created_at_m994F225745DCEF865BA449D2D3EFC43D94C651AB,
	CollabarationSceneObject_get_created_by_m534260D8E7FE7EEC5D975D4B2D3535A93878C150,
	CollabarationSceneObject_set_created_by_m54838A4F8D25AF909996ADBFE9E121A0FE6A8B2C,
	CollabarationSceneObject_get_id_m557C3DC818087D122780F1B47E8173A921527625,
	CollabarationSceneObject_set_id_m161DFCA73280200B4539CC24780531647906AA8F,
	CollabarationSceneObject__ctor_m1C471E092CE08F87C78D2C198DE1B2826A54C299,
	ApplicationContext_get_Token_m8A51A88F0D695497A30D51E008ECE05A7E9C2EAD,
	ApplicationContext_get_DropboxlocalPath_m05B6A98612CF1E68D30500AEA824D5DF95F5D304,
	ApplicationContext_get_APIDropBoxDowanldURI_m901569CFFF2151C6FCEBF2FDD5319CCF334BB0C4,
	ApplicationContext_get_APIDropBoxGetFileURI_m31E0133EA30771A34724DC90DF7D9452704324D7,
	ApplicationContext_get_persDataPath_m2943E4EB79B3139E541A1CD245461CFA1FDDBABF,
	ApplicationContext_get_PersDataScript_mCECC86EF6BAFF9699B0C433202ABBBA570115DD3,
	ApplicationContext_get_PersistentAssestBundle_m6B473E0CA101797834B57D039BCF2BE90EA66BFC,
	ApplicationContext_get_PersistentObject_mF933AB0ED26D02A0EBD2C674AFBED8297891F192,
	ApplicationContext_get_PersistentRoomDataDetails_mC52BC2E8375865E9D66C4F24AEAD6848627DD4B2,
	ApplicationContext_get_UserInfoModel_mD24B1FF89ABC497A861E1ED0A8DE1119EED87F9A,
	ApplicationContext_set_UserInfoModel_mE8F360ED673D0F1E4388088D58D8E4F5DED64011,
	ApplicationContext_get_ISGizmoSelectParenGameobject_m51180CA878CAEC0796C1F0826DFA9EE4F8568443,
	ApplicationContext_set_ISGizmoSelectParenGameobject_m7073A1226CEDCAB8407A4FC7D84F8BE4C5019C15,
	ApplicationContext_get_ViewerSettingModels_m10CDCC00DFCB249837E713210A951E1C6FF35F75,
	ApplicationContext_set_ViewerSettingModels_mC0DA470497231CEDA89B4C8529ED208444D90970,
	ApplicationContext_get_IsPropertyPanel_m0EE84EE7953178514C92FD8E893CE6329F94CEBC,
	ApplicationContext_set_IsPropertyPanel_mB48B947C4F6423220B592AC638D3E8AD3AFE0589,
	ApplicationContext_get_IsNavigationPanel_m2898DB85F55F85C4221964339D0854C05788D38C,
	ApplicationContext_set_IsNavigationPanel_mE69689A1D37B3596FC729E91EEF11B4E706D27C1,
	ApplicationContext_get_PersDataPath_m20858D66D4828C1ED0E3CEBC7F6336E89D61DDE0,
	ApplicationContext_get_PersAssestPathLog_m7635B1BD359760E1C5CE1420816ACC453B8AA339,
	ApplicationContext_UpdateModel_m1A388AFD80A6D9F0CEB7C6F4B8B922847D0B5F65,
	ApplicationContext_get_StorageModel_m5D205CADDCCF49B0B338DA4F8075E7A98A8BFF0A,
	ApplicationContext_set_StorageModel_mF2C2082AE4EFE61165AD1D00F6535C7C8BC084A1,
	ApplicationContext_get_EnvironmentModels_mA618F9758C178A1E6067E0CEAE18D2E7B32EEBE6,
	ApplicationContext_set_EnvironmentModels_m490E6890FB6D7BCB08BFA02CBC73C0CF15FC9E31,
	ChangeSkybox_Start_mA0E4579790C2535D81F5129C43D6991280139837,
	ChangeSkybox_Update_m21BEF5117F52A32890930167ED1594C26AA8E03F,
	ChangeSkybox_three_m03492A68B665CB909A1301EA6E34952CDFDE9E55,
	ChangeSkybox_Default_mC919F5821CAF770C81C07A1078A1FE3233B390DC,
	ChangeSkybox_Openroom_m662CE63CEBF9A09888EB7D2C4B59CB78E479E435,
	ChangeSkybox_Warehouse_Room_mC3CA3E568888A139AABE3C149FF0E4C34DA1FB47,
	ChangeSkybox__ctor_m18FC0E3AA2DEE526CAC2E488389903FAE53E02E4,
	PanelActive_Hide_Showpanel_mC92D0CF8E6B6C0F699903D9E9709C8D6100353EB,
	PanelActive__ctor_m516D8E1BCB77CFE1645A748B225CFCF904E1C78D,
	MenuEvent_Start_m8AFF7E3C4A6CC870401883B5461D35646B116796,
	MenuEvent_ModeBasedDisplay_m0F160028988C910FF72DB8AD75973B62F08E9E26,
	MenuEvent_GetArg_m7FFC2EC1CA65AF8E2C577FD2E3CDB8101ADBCA86,
	MenuEvent_PDFBoard_m79BA87A3089E425941D850EBBEBA8855EF010F8D,
	MenuEvent_WordBoard_m0A8E9814D76AE0486EAD2D8427246688C43BAC68,
	MenuEvent_Textboard_m574EC5231DFC5C3FAE63E14FDBBC22582F702A48,
	MenuEvent_StreamBoard_m990BFE67EB3C128CF97B604FCE5BCEE942851762,
	MenuEvent_VideoBoard_mE95B42C295CB417E6501637BAE0A65C817429A6C,
	MenuEvent_AudioBoard_m1D841C39F4E6EECF6B7BFE8D50EFF4A69C6F3B51,
	MenuEvent_WhiteBoard_mC0B8DCCC05AC6F981AED7935D9DF0E5CD3AF563D,
	MenuEvent_WebBrowserBoard_m341C8BF3F8CE9AA7243E00565B36ABA01CFF80F8,
	MenuEvent_LiveStreamBoard_m2EED7B9ADCD5CAA17719004561A7AD365C6F3B45,
	MenuEvent_RealTimeStreamingProtocol_mE28507537B7AA00DFB427280F1B6551A86925512,
	MenuEvent_rader_m184EBBF8F7CDFDB2AA4B2E1917DED0723CD5D292,
	MenuEvent_Bar_m84115A03E637F4A74FFDD46E3095F681B63251F0,
	MenuEvent_Torus_m4B8E17F5C42B6A30DF358C33A9E299D773D2A9A8,
	MenuEvent_Pie_m73623D994B02CD36DDA5EDB17972810DB72454F1,
	MenuEvent_Graph_m4FFDAD0045E92907543FA64003DD476F6295D632,
	MenuEvent_Bubble_m4BAF3E72FE88A95C8128B5ADDA71059D5CFE3817,
	MenuEvent_ThreeDRadar_mF09CD8BCB45121FBD195C4A371498B022B7CB1BF,
	MenuEvent_ThreeDBar_m24EF3B11DD627E7CCE43A3B728A500A883D7F2AA,
	MenuEvent_ThreeDTorus_m709A45474E42011F4192303905D6244AE5A339F2,
	MenuEvent_ThreeDPie_m7A86E7A265462986CBEDA89686AF25F0EC67B94D,
	MenuEvent_ThreeDGraph_m2FD206C5D28EA96962A2AD911037053F4839B6F7,
	MenuEvent_ThreeDBubble_m6ABB1897A59FEAA8B6D7EE231B1D2B3B95C648B8,
	MenuEvent__ctor_m6243BA20AEC4CD6C09C136C93F29FDEDB8A7B5E7,
	MenuEvent__cctor_m7245187AE33FAF8A7ED3D51BDC91BDF9408E1B6D,
	MenuEvent_U3CStartU3Eb__70_0_mFE2B98CAE2B9BBAE7AA076657C002C07715986F0,
	MenuEvent_U3CStartU3Eb__70_1_mD655DDC3E8F8F4CD95EFF1D8C41856D471166648,
	MenuEvent_U3CStartU3Eb__70_2_m9F997FFF6741515EA512AF2673B3D6F4F55EAB5F,
	MenuEvent_U3CStartU3Eb__70_3_m88EC6D0DB41CC79BB3B7011A99E7BC39EF06F821,
	MenuEvent_U3CStartU3Eb__70_4_m2F3F540B469725844101DA2026B628B5B09E0A77,
	MenuEvent_U3CStartU3Eb__70_5_mEBD546E15871282556EA49085F7EF4BE72492837,
	MenuEvent_U3CStartU3Eb__70_6_mB67D00F6C359A67D59F38E65E02BF6E1BB8CF14D,
	MenuEvent_U3CStartU3Eb__70_7_m712294EC31BB94AAE54E6A742A73F2ED84709498,
	MenuEvent_U3CStartU3Eb__70_8_m9CBC81DCA92B3B4497343281B6EAC17D829BC5CB,
	MenuEvent_U3CStartU3Eb__70_9_m49B008F6B22AEC5CCF559009D977B6F89F8134EB,
	MenuEvent_U3CStartU3Eb__70_10_mA7F9D40B3E87F5B912AB74804C237D8C3275B0D0,
	MenuEvent_U3CStartU3Eb__70_11_mC6CBA197447EF5DA1C928238261A6022218213C7,
	MenuEvent_U3CStartU3Eb__70_12_m634429FC960D2DF1D52AA5480A5862A8CFB430F4,
	MenuEvent_U3CStartU3Eb__70_13_m7699DD436676C2B936E59DC662D69E8B23C237EA,
	MenuEvent_U3CStartU3Eb__70_14_mA90793F8E92C6E75E53CB9D3D3FF5CB5D388929F,
	MenuEvent_U3CStartU3Eb__70_15_m6E770A93865FBA2CD186F4565582324A873DDC3F,
	MenuEvent_U3CStartU3Eb__70_16_m0161EB1317B32EA4740F7F89571FAFB60EEE9F5C,
	MenuEvent_U3CStartU3Eb__70_17_m833D6FC81410AA6E09DCEAB21DF2F6EEFBA0366B,
	MenuEvent_U3CStartU3Eb__70_18_mC6880A6640A34A76953067EAD1542805B260268B,
	MenuEvent_U3CStartU3Eb__70_19_m06E82AC33FCD21947E8406A66F3CD8174404F30A,
	MenuEvent_U3CStartU3Eb__70_20_m26B16B899776309A250DB0CF79F29826323CA353,
	MenuEvent_U3CStartU3Eb__70_21_m05899A6711696D9017C0CC93081EB2CF29AB0AF3,
	MenuEvent_U3CStartU3Eb__70_22_mF6810A2F5E1C11564B8968C1A34247E3137694C4,
	MenuEvent_U3CStartU3Eb__70_23_mF5C4354A334CF40983EA552746117E6A2608D799,
	MenuEvent_U3CStartU3Eb__70_24_mD23001AE41528AF168E5A439AA2CCD3C7CF7CA92,
	MenuEvent_U3CStartU3Eb__70_25_mB1F0EA65A7B7E7842D20720F18DE7BDFC9B934E7,
	MenuEvent_U3CStartU3Eb__70_26_m0610FB107D38CDD7E38B62C90FFCCDC89827D781,
	AssestBundelModel_get_GameObject_mA0586A05195C2BCC55D8D297206D14ECC18FBA65,
	AssestBundelModel_set_GameObject_m5BD1340C646AC0362F8220C599BF238DAD4182C7,
	AssestBundelModel_get_AssmClassName_mF6BF78ED3C1C3EC9D76BA03E9C53FF9CABE72AC7,
	AssestBundelModel_set_AssmClassName_m7E74D7EF3D145A5E8BB1AC2EB754B6A9149E6C99,
	AssestBundelModel_get_Paremeters_mC574C5814A8B36C0BC4A4BA426EB142F805C528C,
	AssestBundelModel_set_Paremeters_m4096E7888F8294482EA9DC9ACCBA006CE2175235,
	AssestBundelModel_get_GameParam_m4837D1DEB27266135FECFD3F31225D5E9EA5D5DA,
	AssestBundelModel_set_GameParam_m52F9B095BA9606EFB20BECBAE20563269DF2F251,
	AssestBundelModel__ctor_m14BD98C34415AC83BB6C1CAEDCCD214A03DEFA43,
	GameParam_get_Inactive_mCF1982D03738084791B649D134FEAC28A0C64670,
	GameParam_set_Inactive_m48C162103C3581F317E4BECCBF4B5E3A9EAAC648,
	GameParam_get_ChildGameObject_m7B9D79CFA45A5E1E5876D6ACBE3CC1ED8698BF15,
	GameParam_set_ChildGameObject_mFEBA659802E5C533AA67DDBBF0A8BEFA6BBC3F46,
	GameParam_get_ParentObject_mA0007BB2A579F957FE43207933E1C8E471B6740B,
	GameParam_set_ParentObject_m3BB06A11DD32C0985A2F4DB92DB89F9C0F41DA70,
	GameParam_get_FiledObject_m5EA622FCE6C9CEA56C4EF5B81B31F68591C50436,
	GameParam_set_FiledObject_mABACE95C867E90DB028A6180B20465E5E04C0A50,
	GameParam__ctor_m34A1F322F9219F37F0B3A909559F61AE84A11228,
	BarChartContent_get_CategoryName_m613A1D630D02B4D67808243318857D6F53FECBBC,
	BarChartContent_set_CategoryName_mB5999C16C259BC623092141D25B4E5A7221CD266,
	BarChartContent_get_CategoryValue_m63FF3914A648FFA36B8EAC49731D48629ACF82CC,
	BarChartContent_set_CategoryValue_mE4DB659D2B3089A6275DAF1BADABFF99A53D6997,
	BarChartContent__ctor_mB91A5B552645B1419CA247528DB5D66F1D549005,
	BarChartGroup_get_GroupName_mEB70A208C2795E30EEDBD3E672DA7ADD1BB90A40,
	BarChartGroup_set_GroupName_m582421E5B6A2A987EF138A52CFF95E65C35651B8,
	BarChartGroup_get_Header_mA32B0D256E8CD6FCEF8FDD5C58CC3085F86385D3,
	BarChartGroup_set_Header_m7E3B25AC30263C20EF83BBFFD1C5F3EF4D1E26C7,
	BarChartGroup_get_Body_m2A4B6BE1F5A321EE2B2E13AB4816DBACEC597CAA,
	BarChartGroup_set_Body_mFDECD26AD4F3079298DB154AACC8A6E9B8643D97,
	BarChartGroup__ctor_m0F9C20905C048E1FC199384334B35D968E4D5290,
	BarChartDataModel_get_BarChartGroup_m0D5C2B8953DFB0F66DA91DC6420EC470E275EC97,
	BarChartDataModel_set_BarChartGroup_m3FF612CA997F9361293827D4F5A3ECA4397CB12A,
	BarChartDataModel__ctor_m80D876EC59DE3A6D875AE75C4BB458AB79C06F61,
	RoomObjectStorageModel_get_storageProvider_m1CA5818CD9ACE01AAF2BAD0935FBA3E6DEEE2FBA,
	RoomObjectStorageModel_set_storageProvider_m46C460A0FB7A1CF3E0558B1675A109891619E118,
	RoomObjectStorageModel_get_storage_id_m1003A84CCE8029661DE4CB91FFA33D4D71C041B3,
	RoomObjectStorageModel_set_storage_id_m3CD1ECF35688E5277B641D19D458218602F50F32,
	RoomObjectStorageModel_get_viewerstorage_id_m9B0F72A1AE63CA0C149B81891B529888A74E2991,
	RoomObjectStorageModel_set_viewerstorage_id_mBA49385709CB9B9945F625712C712E4DCDD58D6B,
	RoomObjectStorageModel_get_filename_mD687DB3AAC9D112E51470FF693E01B3E3EA1B311,
	RoomObjectStorageModel_set_filename_m586C6B4B824CF52B49D8E2F1005A852139D0AB75,
	RoomObjectStorageModel_get_fileid_mA420FB67B4B74C4B27C0D3C9C2133AA94FFFC964,
	RoomObjectStorageModel_set_fileid_mEF68FC116D4DEBC9D2D98B75B52152FD49560BF8,
	RoomObjectStorageModel_get_fileurl_mD8DCCBCE210DD9FC6C5EE59A16C69407A018A02A,
	RoomObjectStorageModel_set_fileurl_mA0ED9539805FCECD38D5ADE5A722A29A6447172A,
	RoomObjectStorageModel_get_filesize_m86C7789299680CFF589D069F75B7E6EF9AA9F181,
	RoomObjectStorageModel_set_filesize_m95B5F0E59F3B6032E6C850482CAD68D9295B1586,
	RoomObjectStorageModel_get_objectType_m3D60615CE7585C67D22BFFFE73EF2CE21518ED03,
	RoomObjectStorageModel_set_objectType_m3EC90A5ECFAAB51A9952F22680A52619331A4E0B,
	RoomObjectStorageModel_get_created_at_mA037D465BB8F2CCBEEE54EADE5BE6ADF9F3C5AEE,
	RoomObjectStorageModel_set_created_at_mB0889EBD92C7797386538ADB2FD0D77352CE6871,
	RoomObjectStorageModel_get_created_by_mAD693807C3E64520102CF5BBFFDF8E0940324BFB,
	RoomObjectStorageModel_set_created_by_m43F68C34E835B7DD14FE3B472248BAFC35830257,
	RoomObjectStorageModel_get_id_m46CF807F195DC0342B2F139A6CE6E899FE64D58F,
	RoomObjectStorageModel_set_id_mE2019C45E5C1E6C9F64A0177346CB062344AC871,
	RoomObjectStorageModel_get_org_id_m484471F1366BBADA2BF07C120CA300F136AE0292,
	RoomObjectStorageModel_set_org_id_m7E27F0BA528EC392160B1D5A89C16A6CDF4496AE,
	RoomObjectStorageModel__ctor_m0B937CABF1EEC21B780BF0D769EBDC1BB52953C0,
	ViewerSettingModel_get_storage_id_m5FF12F90C2B4B422EB40A9A9196256E541861ABF,
	ViewerSettingModel_set_storage_id_mB97D2B69F4C0516ABB1D9DCC1BE376AA74D3E4CC,
	ViewerSettingModel_get_org_id_m7E0604510778185605A2688FE2D4A830B9155630,
	ViewerSettingModel_set_org_id_mBCAF5D5D365ACCC5FF3F30F1FABC1EEF898076FD,
	ViewerSettingModel_get_storageProvider_mBCD5F7D1D2F4F46695C71420AEC30A1C33570656,
	ViewerSettingModel_set_storageProvider_m60859E4979426C7929A82548D0CD819A9B9D933F,
	ViewerSettingModel_get_parentFolder_m29738338B5A05DB36746546225794E7EFF8CAE11,
	ViewerSettingModel_set_parentFolder_mAA7EE034EF5E4128DC2A830DC3B98BB7A4B1FA58,
	ViewerSettingModel_get_accessToken_m60EE5898BDC146F7C5627436F19E9625650D6CAB,
	ViewerSettingModel_set_accessToken_m91BF7DC0260CB0730EF96FE3EE47771C9C301DFC,
	ViewerSettingModel_get_folderId_m45730235A55C5ABCF7DAE5B6227229F4FDC7DDFA,
	ViewerSettingModel_set_folderId_mA616A0D06AF8BAC9CF99BD39422A73C422E4C00A,
	ViewerSettingModel_get_created_by_mD373B28BADF441C5C853B9859D761714F0939777,
	ViewerSettingModel_set_created_by_m98D269C55BECDD53FDB4F4BF9ADF0FD3BA7748DC,
	ViewerSettingModel_get_created_at_m8C5E5869D0457F51DAF3D59B2B5507B287AA0A0C,
	ViewerSettingModel_set_created_at_mC51A4BD76DDD3D977E284F9845CBD69B4125F883,
	ViewerSettingModel_get_id_m6AFDCC3C2DE3356B7513412FE9ED1A091A7DC37F,
	ViewerSettingModel_set_id_m74E3E6386224FDCECB61A080FE4CE1DDC46C7F87,
	ViewerSettingModel__ctor_m4F670AF27F003B0C4402BA87122DBE31F86DD115,
	DownloadMetaModel_get_name_mABE90A7DA5D43B995DAAEBA288E25EE35C6EDBF2,
	DownloadMetaModel_set_name_mAC0B435E2D933D80639C71D229F035F37421ADC0,
	DownloadMetaModel_get_path_lower_m176B951A51057FA35D787ABB29F92C9F2F914A91,
	DownloadMetaModel_set_path_lower_m1E334C8C557B19D2E71D1174179DA82F5419E701,
	DownloadMetaModel_get_path_display_m4E6953A3C5BA372E993E8044F75FD047A768636E,
	DownloadMetaModel_set_path_display_m046532BCCBBFB99834FBC36D3C95F65D3B60B34A,
	DownloadMetaModel_get_id_mBAC4DCC0DC075C32DD32AC431DC5E277D6F7E05A,
	DownloadMetaModel_set_id_m3A9D8D825AD2FB671DF3AF14399804ED9258DBA2,
	DownloadMetaModel_get_client_modified_m31F1D5149328B73930FF687C4075AAC0FF578C5B,
	DownloadMetaModel_set_client_modified_mF849850AD0DD621372412B183C90B3449E12239D,
	DownloadMetaModel_get_server_modified_m02DEA2AA17E2A60847A5319BB6620E11BD3B878D,
	DownloadMetaModel_set_server_modified_m61E59D680F492769AFF9A073E858B23FEC14AD46,
	DownloadMetaModel_get_rev_m5C32E9F54AAE0211BDE95194F451E89984EA5A3B,
	DownloadMetaModel_set_rev_mD9511068D9F92AF13AA4D3A347BD89A47E6D564F,
	DownloadMetaModel_get_size_m34AD60D28499449BE541C501F7EF78D38BC6AC98,
	DownloadMetaModel_set_size_mA60574CA8A9067234368E5EB624C6D424A96B0F5,
	DownloadMetaModel_get_is_downloadable_mFA5A538D2F613D0B1A000E0CACCC4D000051F008,
	DownloadMetaModel_set_is_downloadable_m8885F0E20CF3AEEF6A8A3D5A225EC0DBACD06A2D,
	DownloadMetaModel_get_content_hash_m50C372C335F364EE9D79E6EEFAAF196178AAD210,
	DownloadMetaModel_set_content_hash_m4C01DB8907A44447836D14791C3399D51BCA5136,
	DownloadMetaModel__ctor_m4C093A4395CB69C8F36C4AC6A89D0C95DDCEC94D,
	DownloadModel_get_metadata_mBEDFEA2C4DDCEE8F79AC1166B632959C1339940A,
	DownloadModel_set_metadata_m8D49A26E682EB5E8588760B3BEF6EA0B50356E2B,
	DownloadModel_get_link_m1B0640698BF0A6215EBED923FC78F3A63BE4FEE0,
	DownloadModel_set_link_m2F31EDC77E92499DF20C4C860C447488C3F0F735,
	DownloadModel__ctor_m119FAD86C6584F3D904B18C6BD9A60EEBEF31498,
	DropBoxItems__ctor_m6F17FE7E405A3D5DF4FE540BCB279CF14C1C814F,
	entries__ctor_mB56E141F3276D5D1091ED4900C7348E54E662BC8,
	EnvironmentModel_get_name_m3046CDDBE1ABB242361DB77F4E6A4CD6F5FD33D4,
	EnvironmentModel_set_name_m1123B41F3809DC0F9D5F7674728F1D3DA536431C,
	EnvironmentModel_get_description_m3E02CD14864985F7956F3F082531622EDDEB3D48,
	EnvironmentModel_set_description_mC6B9FF729B922FF44705FB7D11010D4BA2A35C6B,
	EnvironmentModel_get_environmentType_mE4C28338828410A6EBDD1236B5FE1F1FB9F51A11,
	EnvironmentModel_set_environmentType_m7427326FF33DD148607385155D3DA2133D028F38,
	EnvironmentModel_get_objectName_mBF28FE63217BB63F5CBF8C4A4C6C2D353CC9E536,
	EnvironmentModel_set_objectName_mAF8BBE0ABB0F22B3684F3491644E70A470EC54C2,
	EnvironmentModel_get_viewerobject_id_m4EE43F7DB83A8203A8F766EFDFCC806EF1D33F02,
	EnvironmentModel_set_viewerobject_id_m06908F3DD945AEE5423A0DC8455B60D7D7460C92,
	EnvironmentModel_get_created_at_m7C45D6A75F1D40B5B3152D6620C0BA2E31837B25,
	EnvironmentModel_set_created_at_m275CE19B9209F4111CA45D786E7B26598CC9DF40,
	EnvironmentModel_get_created_by_mB4C2CB6F2EB01A8772B88DD0242AF761CB2A8B11,
	EnvironmentModel_set_created_by_m9D23B0550072C5F4657B208EC21B458154622121,
	EnvironmentModel_get_id_m9280FF39A52776916B2DB67C7E3705716A6428A4,
	EnvironmentModel_set_id_m23F84B2FE75A2D427AD0FC4C873367FB450E03AC,
	EnvironmentModel__ctor_m080F96F2809FD2C197BCE2C0C8F34930A4CED291,
	LobbyList_get_roomName_mE506E6A89C62857EFF0642D0B6DB754F76183D3A,
	LobbyList_set_roomName_m648CE486D8535E542E2136BEDDB67A79864E26F6,
	LobbyList_get_hostName_mC747CD3DC35F1386F98610300017FD1F9618E6A3,
	LobbyList_set_hostName_m27B52358B08EC8F61A42EE5F3D069CE7B8D723E5,
	LobbyList_get_communicationType_m5A9F5DFC2AF3B2E41759F8F2B9C9ACB5FB8FB3D4,
	LobbyList_set_communicationType_m6D3EE250C3A32857EC9B8B5404290304B27AA4F6,
	LobbyList_get_audioOptions_mEA18EAE908B67EEBD80BCEFEAC19FC7D9DBF7365,
	LobbyList_set_audioOptions_m3E325A0A829DA8A128D8431F650AADFFF305DE27,
	LobbyList_get_requirdMeetingPassword_m0901280C5D35C4B8CD0F4E45D4F10FEDE32218BE,
	LobbyList_set_requirdMeetingPassword_m7829C8036B4F79F28B9455A0072E7B5492DD3194,
	LobbyList_get_enableJoinBeforeHost_m358108D04BE04E5A438C70223A4FAEB4259B1A60,
	LobbyList_set_enableJoinBeforeHost_m44819E871DC787562EEE3D9F20CB859842C6D7EC,
	LobbyList_get_hostVideo_m88F260B640ABC750AB57AC12A67E7B253DD521AA,
	LobbyList_set_hostVideo_mC71D0AF405A2EBF5C38949468D66E85A5EED1965,
	LobbyList_get_participantsVideo_m342D72DF89618B2C494B1CE5487233C16B0C2AF1,
	LobbyList_set_participantsVideo_mFFF791BD193C4010F329A4BE997A79F31F77E1B7,
	LobbyList_get_muteParticipants_m59258C45228F0B2B17A8C686213B01CC9A4C714A,
	LobbyList_set_muteParticipants_m7D4BBF862B0DC4290335AFF5BAE5B1FED30F4A85,
	LobbyList_get_usePersonalMeetingId_mA9454A5A33C881EA45AB73A50E4677305DAEA1C4,
	LobbyList_set_usePersonalMeetingId_m9D19CA5B1C8ABC2363338F0FA6240182190109BA,
	LobbyList_get_recordType_mA50145971694FDACA5DF7B58396744557C6C3CED,
	LobbyList_set_recordType_mDF9006465345A88911D306818AF6B3E0B802E7C3,
	LobbyList_get_defaultRecordEnable_m8EAEA5F964AB6C0D4DF80ACFD33471B3D24562EC,
	LobbyList_set_defaultRecordEnable_m7BEE274CA616FFC56932EEE5E78F879E98E4F72B,
	LobbyList_get_recordFormat_m6CE8434D3D9F179C23F41BFFDE6ED4F46BE76B36,
	LobbyList_set_recordFormat_m8E817FA03F75C3B4F73417C24D4376D6041FCD99,
	LobbyList_get_mediaStorage_mA022B1979A4EB47F702E9E35FDDDD2E05872A257,
	LobbyList_set_mediaStorage_mA7687AAEEE07614211373109CE49336350693BA9,
	LobbyList_get_mediaFile_mE53FE7503F2927813C3402167E4FACEEF951DADB,
	LobbyList_set_mediaFile_mC822526F1A8D629AD014BB2F7769EB2633CDBEF5,
	LobbyList_get_calenderType_mCB9F9C57DF71FDE721302C3302AABE361CD08E45,
	LobbyList_set_calenderType_m4D0DD97B95E1EBA778C8CC4F3AF4BC0BD625509A,
	LobbyList_get_id_m7DA2F99DFA46232D96440FCBB06DB7F8078EBD00,
	LobbyList_set_id_m7A83DCFE90A2DFE82D5BEC66E7DECCDC46F19AC2,
	LobbyList_get_events_id_m14D313B42084645D3ED403249D53D6068E3A2D34,
	LobbyList_set_events_id_mCD2A60A404DC92AA083FF8486FB861485522F729,
	LobbyList__ctor_m59D8AB49DCAF636B95D1EA100A3F9F5910EE05B0,
	ModeTypeModel__ctor_mC0B6155E0A637C05B0691B9713DABDA0CF7FF249,
	ObjectQuaternion_get_X_mC44A3D7B4C5E54DC463A6A7494E6F6871F1BDE11,
	ObjectQuaternion_set_X_m1861DCDFBC6912F7D3405717501D7EFD90F09014,
	ObjectQuaternion_get_y_m874955519CD23CD1745874F57119A0EE7AD6F36A,
	ObjectQuaternion_set_y_m9ADF2283319F849DC30C568B1F873EE4209EC006,
	ObjectQuaternion_get_z_m7A1F58E7BDE89B779044AD1BD6B00D6BD7C3E8CE,
	ObjectQuaternion_set_z_mCC0D7ABEEBEAF1D04D92007763ADC7F0E59D9B62,
	ObjectQuaternion_get_w_m73B5527B214B6B51D4C86561CA4D110CB5DEDD26,
	ObjectQuaternion_set_w_m6F9E139C0BE05C5F28B126A4A76D88736F137977,
	ObjectQuaternion__ctor_m747D17964616B8EB612FBE4BDFF266499D9B660C,
	ObjectVector_get_X_mAF666378C689AA12377B9B5119DD1650B5EAD25D,
	ObjectVector_set_X_m748480B292546D367B8B4D8D59549317319CC88A,
	ObjectVector_get_y_mFCB8209233BA05D005BDD2BBE3E5FED6E134A7A3,
	ObjectVector_set_y_mB62FA04A7E044E5F9E1D6C845CAFC85990AFA7DB,
	ObjectVector_get_z_m1DA8E680EEBF938716E9E3C24CA6618C0D414F25,
	ObjectVector_set_z_m3B6B842285FF1CC1BB53ADC0ADF646479B6BD4A9,
	ObjectVector__ctor_m7E727D8CDE06661398A636444B6F0D7D6B8FD085,
	ObjectmovementModel_get_username_m90BF9566A2B4016C1607B7FDCF8127083B60336F,
	ObjectmovementModel_set_username_mAB05BDF49C899571B5CF76AD08020C4F44100BD8,
	ObjectmovementModel_get_objectname_mD05D51988E48C30E7524E2F7BD8EED3AA991B89E,
	ObjectmovementModel_set_objectname_m21ED9D72CCFEDF3746AE18891B3C28D47404AFB9,
	ObjectmovementModel_get_roomobject_m67D0998261C14B093DE618074F1940065B1AE638,
	ObjectmovementModel_set_roomobject_m335F93E3B5C0EE91B5AADFD2B356C2B25AA37AE2,
	ObjectmovementModel__ctor_m0E066813F45C73CA5282D74385C759C6947FEF68,
	MovementPosition_get_x_mF5FE14AA62FA98636BE38C069A2CCEC9B7D0F8F4,
	MovementPosition_set_x_m8124EC724C7F87CA77093462C6FEB0616794FF53,
	MovementPosition_get_y_m13F47B746AF92E5037AA824BA05E73F389224B22,
	MovementPosition_set_y_m02F49CFC04AAC62E5991F2F606365AA04A3B7BDC,
	MovementPosition_get_z_mC9C202ECCFCCCA0BB76704B7D5AE85D0035038DF,
	MovementPosition_set_z_mF9CE734B7092DF3E461A896C594E51D83624B7C0,
	MovementPosition__ctor_m001CAFC1C91A0A93A532A2614B8E3AF03A8BB572,
	MovementRotation_get_x_m0FC0AE79B48B7FF7A7D2E7AF45AC3A87D65724F4,
	MovementRotation_set_x_mE4DE245FD060905851A7BF45713381F4C93E0A26,
	MovementRotation_get_y_mA307CADDFAD771D26CF516F76601E4F26229FB4D,
	MovementRotation_set_y_m892CF0D76D9DBD8A826D7EC2D1EE76CFB2EFB214,
	MovementRotation_get_z_m5DAF9485954901A457E102768480E6F5657B21FB,
	MovementRotation_set_z_mD7EEC34996CD571EE39B7A1D2C2AA8B4FF1D0D21,
	MovementRotation_get_w_m0CF906EE637BA5F4053F8B898F759B8597935AF3,
	MovementRotation_set_w_m081BD98AE50F740BEA5DADC7CB07F74D374011C3,
	MovementRotation__ctor_m1BA68F6EE6A3F7104486F3B66F00C608EF2F3CD9,
	MovementScale_get_x_mDA1C3122082DBC1C676CB4C92E2D3C6B51947C75,
	MovementScale_set_x_m8C34A2A284FDCC501486463E249DDE57D0266BFA,
	MovementScale_get_Y_mFE72795FD1E3572EC27103CD2BE58C6399E0397A,
	MovementScale_set_Y_mDBB3DC293CFC1833F74613CF6059FE507D59E898,
	MovementScale_get_Z_m68ED4D53AB22AF9B6E91FD6DB9F0934F559F898D,
	MovementScale_set_Z_m76896E3D731C14BD773F46FF672C225E94BA7BC7,
	MovementScale__ctor_mE272D142066DB226C7E62F35A36FE064D3FEE920,
	Movement_get_position_mF70D81C7EC4FA8B09DE07A19B8D8F7D2A326C9E8,
	Movement_set_position_m71866B328DEC07A94A52CAF2D172E0825AFA8817,
	Movement_get_rotation_mCC61BB00842AFE1E8D2FA8C3262B6EE69DCF98D5,
	Movement_set_rotation_m237048B5091C5857A299B4F5DF62BB602929323A,
	Movement_get_scale_m71E7D3F001FC63082B3B49C6ECBDAF56322BA48E,
	Movement_set_scale_m42883E72EA44AF241F3C193E5D7FA066B5858121,
	Movement__ctor_m1611A5910E25AEB0A8B91660D64BABA1A91987B4,
	Header_get_Category_mA7B7CB3C31D54769A33702C4289DC7CF8FE5AAD0,
	Header_set_Category_mDA6E4AAEBBA27D92B35AA2697826787CFB2FAE1B,
	Header_get_Material_mC797E87E6352546C3F130299EB5D4B9C7203FBE9,
	Header_set_Material_m977382A36D00EE01C2F4D46DFCB727C0069D71A6,
	Header__ctor_m659D4907FE9D76091688814A1B294AD48A47A9C5,
	Body_get_CategoryName_m50894D61BB4288EAC4F7AEA4BBF5F29F7F74A875,
	Body_set_CategoryName_m590C27CD7EEEC94F8681B76D6873869F4348FA45,
	Body_get_ISSetValue_m50A091534416017BD66A23B9012D1808434473C7,
	Body_set_ISSetValue_m84E9538F37C0814F8D1A01D56D5D95C285004022,
	Body_get_CategoryValue_m585DF2D76D05AA07DB9AF96173A360AE28F510DF,
	Body_set_CategoryValue_mE28D6DD5D521B57D474BB8341A8EB7E8B6D28902,
	Body_get_SlideValue_mABE0EA0419BD2EA38BBF29B19E90470DC1621DC0,
	Body_set_SlideValue_m1177BFDFA527AFD194550ECB447618C721383148,
	Body__ctor_m934AE05AD5882F055254FF2C1D7F37EEEE08AE21,
	PieChartDataModel_get_Header_mECA0DDE9BC1DD8E99268CED42BDB3916CC977AD3,
	PieChartDataModel_set_Header_m11489E6F9372B971F6DAD893F24BED60772F0050,
	PieChartDataModel_get_Body_m9DE204578C43723309580D6A173A8C70A672197B,
	PieChartDataModel_set_Body_mCFCEADBE91F9D52C341B084510E0987E57DAA1B3,
	PieChartDataModel__ctor_m0DD0895771938C95A345C4AD660ACC52A7874E77,
	RadarChartcontent_get_CategoryName_m871B16F410AF4E817CAD808ACC2675037E1245EB,
	RadarChartcontent_set_CategoryName_m24443F157AA59BE458ED9788616F42171658A4FA,
	RadarChartcontent_get_CategoryValue_m793E6B1243FCA7192C38C9AA9D383F261D216798,
	RadarChartcontent_set_CategoryValue_m0DFEC2560D7B62F3BD893694408ED51ABEB638A0,
	RadarChartcontent__ctor_m235BECAFAE4E5100AF166E3C5358F7D2A998B933,
	RadarChartgroup_get_GroupName_mE8555850804E1D3BE6324CC3D7D68F70EE00BC5E,
	RadarChartgroup_set_GroupName_mC9FCC2A264984AF29EDA06773BA149317D2C249C,
	RadarChartgroup_get_Header_m9838FF5779AC1843098B7D453E0DE92A9AF1568A,
	RadarChartgroup_set_Header_mDD2B0415C29C2B0BE180818C9C92C316552CB9CB,
	RadarChartgroup_get_Body_m4BF341168EF0BA68F2364844A011A64572B1B87F,
	RadarChartgroup_set_Body_m351B682AEF07CF18CE59A0367CAF153799B87605,
	RadarChartgroup__ctor_m09D2F69960A9B49BE654789F73E4193E26EC0F42,
	RadarChartDataModel_get_RadarChartgroup_mF6F607B96E100FAD47DE649842D11BB80A26752C,
	RadarChartDataModel_set_RadarChartgroup_m3AC32F7F20B72C65C7C95DAC4CF71389D1619CAA,
	RadarChartDataModel__ctor_mBBABF4FD5139489B7F3EEE308ADE0E999BFCDDAC,
	ResponceModel_get_code_mA340672AFCA50005BAF01F2770CBF338B244E2A9,
	ResponceModel_set_code_mE139A6FE7AAF6008620073306627E70C36ACD167,
	ResponceModel_get_message_m1721E866CF63E99E055F29F6F518B513DB1B718C,
	ResponceModel_set_message_m1DFC3FC9473E121BBB2361DABDDE98D66921F166,
	ResponceModel__ctor_m70C11A79EF043C7B19E61F14683C41BC8464F745,
	RoomDetailModel__ctor_mD8071FC5BB9048C44C22BDDF93D80599DD812A03,
	Participant__ctor_m72EF25AB2746C58D19F9C1F971809558CC5CEEAC,
	RoomDetailsModel_get_RoomName_m8EA0C4E074284AD07DB847064C4FB2F2415999A3,
	RoomDetailsModel_set_RoomName_m4D801AB5C4788582BE04F6DD6C8A8BE2D9CE76A9,
	RoomDetailsModel_get_Description_m2ED4CADADF3D765671F49BD059E477403092DEAF,
	RoomDetailsModel_set_Description_m7421CE06B44910A103FF9F349C53B42F383EE28F,
	RoomDetailsModel_get_RoomId_m065BAF7B49E8ABEAC55E2A9FFE31DDF86CE77357,
	RoomDetailsModel_set_RoomId_m69201C04BEE6EA8F774ECC82C59495E4AF0FA2BF,
	RoomDetailsModel_get_RoomEnvironment_m318148B04AEDCC82EF81C247557CF64829A1AF19,
	RoomDetailsModel_set_RoomEnvironment_mED83D8586DE952559071FA4E694431C1A6E2CB48,
	RoomDetailsModel_get_CreatedAt_m73F63E09B333546082EA6149BDB13E5C32A0293F,
	RoomDetailsModel_set_CreatedAt_mF025BE612402B547BA25098841999335517F480D,
	RoomDetailsModel_get_RoomObject_m43C17EA42BC03291D075C51089265AD606B6B79D,
	RoomDetailsModel_set_RoomObject_mA622A0AA454A0FCEB73FF12A95B213921AFD946A,
	RoomDetailsModel_get_ModeType_m6315351859A18CA39E501DE4595F9866F03D7A83,
	RoomDetailsModel_set_ModeType_m59161590D4F8EEEDB55D7CEEEA4C6E6AF70F98C3,
	RoomDetailsModel__ctor_m9FC7424450C915BC2B09BAFEF7165C4595102CD3,
	RoomObjectScript_get_RoomID_mCAF19D5698FAF5531FAAA8DBAE2A34B47842FCA5,
	RoomObjectScript_set_RoomID_m919BCE127548C6D250D07F8B0AD4410E71223125,
	RoomObjectScript_get_ObjectId_m1E981427AAE3B4A15AF710E8B632864CEB52A0A8,
	RoomObjectScript_set_ObjectId_mFA25700AACFA3C13730C5D73E6B5C572D0022E21,
	RoomObjectScript_get_ParentObjectId_m41177D414D6C59D15EAEA9774DC184BEB2E69A43,
	RoomObjectScript_set_ParentObjectId_m2A739415B2EF8F2E25DEBB786E60D80D5ADAC595,
	RoomObjectScript_get_ScriptContent_m05361315578984A894EBBAAB45F266EF17268AA1,
	RoomObjectScript_set_ScriptContent_mA10F81B8413B43947527443AEAB0032353A16341,
	RoomObjectScript__ctor_mF02A1BB5D5274E9FDDC186B0214D1792B23E917E,
	RoomObject__ctor_m88AAF36719D09D0060BC25382DA6773613623A0C,
	Metadata_get_name_m493182F9BB90E808F05A1F2D093DCDB41E2D38A0,
	Metadata_set_name_m3507FEC6FB3B649F54F82A3ABE4490DBCA57989E,
	Metadata_get_path_lower_mDAF5FA66A8F9059DAB81A364FA8A0F37268EFA5F,
	Metadata_set_path_lower_m645851F28918768F300408CDBBE9A78B2BCF31A1,
	Metadata_get_path_display_m930AD9FF0F42CBFBF85399AEE70C9A44156BFC8C,
	Metadata_set_path_display_mEF778F4226A1A5F531992EAD81DE609B04B549AE,
	Metadata_get_id_m02E041A7EBA041E26A214305EEA902550AD39340,
	Metadata_set_id_m0794F0D279A861FAF11D37AF7ECA0A80A376D7CD,
	Metadata_get_client_modified_m03B1FA946A2F64DEC175BBD9A1618284D4EE2C94,
	Metadata_set_client_modified_m622A377B9DF78E696B54BF7F7DCCF9B863B6A11D,
	Metadata_get_server_modified_m642C4197146ABBF47A8C758F07B713EC6F09C049,
	Metadata_set_server_modified_m76436F1C8C4A51B0471CC604B85AD45010EA2BB9,
	Metadata_get_rev_mAD63B8FF3413E64D10052CABABD9E8EF9111E8EA,
	Metadata_set_rev_mEAFECE116AC06D1FBAEC93E30FD9A1FFFEA8525E,
	Metadata_get_size_m88A86E1D37AF949453C2FC4955E816748919E2BD,
	Metadata_set_size_mCEFE80215EA12EA1501D7535A9C6B80E106EBB08,
	Metadata_get_is_downloadable_mD3D7FF7E7D34CEB83C341EBB3B3EDF90EA515BEF,
	Metadata_set_is_downloadable_m3D3813B6B01A3535D55A07791C2A3FDA8DC33D28,
	Metadata_get_content_hash_m02F014C267C688BF38BB91D995D7BE72B63FAE95,
	Metadata_set_content_hash_m5FFB961F6ECEA8F7C7D312D7CCE9E65EFD604196,
	Metadata__ctor_m95922140AA0AF669DE5F274FD022BE1AD3EE231E,
	StorageDropBoxModel_get_metadata_m7207B078B7F1C92EEDDA387CD228D7964883C595,
	StorageDropBoxModel_set_metadata_m49CC19B6D96D2BB8959D714BA2051E1800A6B93C,
	StorageDropBoxModel_get_link_mDEB0123CD50AAF2B9C553001CAE3F4E1AB63F653,
	StorageDropBoxModel_set_link_m350B2E1291AC4B92EB78E184AAF4D9FEFC4F5F69,
	StorageDropBoxModel__ctor_m0D82A92EC139752CE5C50BA4A8765375D868DBDB,
	StorageFileModel_get_type_m4D28D988C4D5C4D424CDBCD762D6D9CBD429AF87,
	StorageFileModel_set_type_m0F08B974F96D52F3727C4E9531B5BED05F4DFE95,
	StorageFileModel_get_name_m85BEE8DE987B85F1EB3819C28A192CE8D1B7892A,
	StorageFileModel_set_name_m3C9324DA783CD8F4EA06605696345C69A87A84D8,
	StorageFileModel__ctor_m8188BF8535019E9DF2958B22E4DFCB691279D2EA,
	StorageModel_get_id_mE28AE75D72A3A1B40F4CE7D4E2C4AF93665E8C78,
	StorageModel_set_id_mEF166BB68B959B5D2211F698B1231A929083A5E4,
	StorageModel_get_name_m2A4F2FE44632BAEA1B9A9E6120EBB61167D97827,
	StorageModel_set_name_mD4F2CE63E4E27246BAF8EB5F281768442B2D79AD,
	StorageModel__ctor_m12E55019ECEBF5D59B997636531D02DEC736CADE,
	category_get_Category_mC1928132A45D3CEF7F6A5E3772F8018E18243324,
	category_set_Category_m773C6F40C0B3CD633F094DB8A1F5805B475851B6,
	category_get_Material_m7F1A94FBFAE0846DE8C196EF8751692EDC24AE3D,
	category_set_Material_m5BF1153D08D9EAEBE3DDEBFFD4119B40E04334D4,
	category__ctor_m9E7C9B111A55FA6D4E930FA2C12D07D44E9FA7BA,
	content_get_CategoryName_m5551A5005025050F71B66741E3BCB4F93953F738,
	content_set_CategoryName_mF4D575781CC711B42C600488A21F6B1D52755B9A,
	content_get_ISSetValue_mBAFB9EE2CA7F9E65BA6297353E7B89BD0FC23031,
	content_set_ISSetValue_m03E51D39426321AE07F1AC3945DA3C55191E8F6F,
	content_get_CategoryValue_mC3BBD7DE11B954BBD006A96F4815937D6C940971,
	content_set_CategoryValue_m14B3A46E67CC0E405A9EDC7D8949CE27629F6CBA,
	content_get_SlideValue_mD8C14ED2663F1580A9ECAFEA21A448FBC900BBC0,
	content_set_SlideValue_m8DBEAE98DC5F30412A121E52D29F608D530725C6,
	content__ctor_mCCCA623FC2441F0C89084D55DB67CA1C16E73417,
	TorusChartDataModel_get_Header_mFEDA6E5E4A39297BAB5555B5D792F194004AFE8C,
	TorusChartDataModel_set_Header_mDAE487703AE2CD8CE73C11B26F71F2D74F419BEC,
	TorusChartDataModel_get_Body_m011A74AEC7A82582D6DC17C9E26F53F153956E0A,
	TorusChartDataModel_set_Body_m1276063EEE3064EBF2D693A4838CB4381A8DB664,
	TorusChartDataModel__ctor_mFD83F3DA4C569487D11E94D799EAD76E4C2EC7F6,
	UserProfile__ctor_mB062AF2F45A093C7296D9382A09FCBA6C19C7789,
	UserOrganization_get_orgname_mC658F6189F9BA7BAE73227EEA662CB8D0DB5105F,
	UserOrganization_set_orgname_mA200712D91D2BC71C10449972F50ABB8CF027D31,
	UserOrganization_get_tenantname_m69B8F188CE9DB5DD76F4DF5BBC929195E01E69B9,
	UserOrganization_set_tenantname_mA17804D9869ED06D2D1B8EF00DBB9149EB6742FF,
	UserOrganization_get_domain_mB568A21418F658726E9943A737139EE37DF36A4E,
	UserOrganization_set_domain_mDED5B27FD43F61DFDC9E955BAA249E53FDAE37F7,
	UserOrganization_get_tenantlogo_m39921F6D2F533647540EF02AF7A92CCFC2F23597,
	UserOrganization_set_tenantlogo_mA43803A1E8C861C59B315B2C006F461955B0C166,
	UserOrganization_get_description_m223D6FF3C677F0D46A95AF5133785C27E58585D9,
	UserOrganization_set_description_m62B5273A0FFBC84138CB20C65F5833A9AF67B658,
	UserOrganization_get_id_m958606D9A574B1358568D6FEB7C20BB080867E63,
	UserOrganization_set_id_mAA9B0C06668E12D4D9BF9411CB864DD450372776,
	UserOrganization_get_sites_id_mA6B4F913F6D1F5E35847D72D4F6235D67A83915F,
	UserOrganization_set_sites_id_m4F9B0C3219FE2B2ECE82B04E2438E4A726012F9F,
	UserOrganization__ctor_m4FE079350EAA926C131B227DB407A9EE36703E3D,
	UserInfoModel_get_id_m25CC3186130F44A1E12C99E62A4FC0ED42C71168,
	UserInfoModel_set_id_m1344BCB2B848FF9F665920BDE7D78627EECC6092,
	UserInfoModel_get_ttl_mD227AA8CDC799A40AB4825A405F88DA46B20DBC5,
	UserInfoModel_set_ttl_m287843430707A96DA5C8923A0E0E126B4E8ADA91,
	UserInfoModel_get_created_m68A1CEEFEA5D2102FF6ADE1D0E92E7CBD0BBF466,
	UserInfoModel_set_created_mF39917F02C61F49BB9E8FECBA8A5C12F6C601BEE,
	UserInfoModel_get_user_id_m038D532AE3B4C973EF1FD0BC395A02D397D545E3,
	UserInfoModel_set_user_id_mF01D1D38FC8CF9E8E6775C086ED9C2AF03499634,
	UserInfoModel_get_applicationDomain_mF4B79A28F689DDEA6856335A6DB5D3F3C6CB4227,
	UserInfoModel_set_applicationDomain_m0F22952D929FCEA85709DE73C7A8B28863F74729,
	UserInfoModel_get_userProfile_m3458D93B2243EA6C07231BE687B0EDE4F8951CB2,
	UserInfoModel_set_userProfile_m1A4A22FDF753D81F68A03F12F99373D9DEAF0CF4,
	UserInfoModel_get_userOrganization_m79C8986DE79D213B8AE2559A0E113B6317FC5843,
	UserInfoModel_set_userOrganization_mE59F169BC38F1D0893941C6629D9EC949928900E,
	UserInfoModel__ctor_m76EB52E9B1863D7DEAAAD5251932BD3AE6EBB84D,
	XapiModel__ctor_m9B3C7B3F3E9415B4F6DCE126221AF6F3A7DD6607,
	FrameWork_OnLoadLoader_mF4C57B52D01147C33FFD27AA1B2F5F93FDFF1E3F,
	FrameWork_UnLoadLoader_mB459BFFB96425338FC785943301B2FD7F7D3C614,
	FrameWork__ctor_m9669A3A5E82B9DDB9CCF8B3C6146C3110F2BCB85,
	NULL,
	Helper_LoadPNG_m1D99D0D3F8C2AFE67094774CD1C1BE29555721DE,
	NULL,
	NULL,
	RoomUtility_get_proxyAPI_m8AEC44486090AC6C55C1103B18500D0355426CF0,
	RoomUtility_get_ModeTypeModels_m70D3672AFA745D7CB82A5ED1393F59727E9C6F83,
	RoomUtility_get_RoomDetailModel_m21FC973FE1832C47300A0921779E95B1745C902B,
	RoomUtility_set_RoomDetailModel_m8EEB8BB5C91835DF937258486D8AC77E93A41126,
	RoomUtility_GetModeType_mA578F9815AA460B55E157A61E8DC47D8566507BA,
	RoomUtility_GetRoomDetails_m9B7E0ADA3BC4C29A48F89B52DFAFA5E8014BD5A6,
	RoomUtility_CreateRoom_mF82351310385F0EA84C063B2D23AA1AEF6068CC6,
	RoomUtility_UpateRoomDetails_mE7D9FB7FC9FD45A27FD582C3218602B0241FC3B6,
	RoomUtility_UpateRoomObjectScripts_m85117DDFB4E674FABDF887F90348F2492CA306A1,
	RoomUtility_GetRoomObjectScripts_m23A84D8FFFE11DA962B12BC77A38BEA9E57930CE,
	RoomUtility_DeleteRoomObject_m4B6AB09C72B779637D4ED97A4C9DDD24A043242F,
	RoomUtility_GetStepPosition_mFDB9D892E57145A5B9E6865A2C604B25A7B9E70A,
	RoomUtility_get_userIDArg_m1E9BC48EAEE9577BFCF6F74769FDE55F53D20FA2,
	RoomUtility_set_userIDArg_m7EE2F7E1091BB050BA6E5B08EE9F4CF4A88B16BC,
	RoomUtility_get_OrgIDArg_m6CA6C17260BD8039A7AC7588E379448112D0A75E,
	RoomUtility_set_OrgIDArg_m5517E95695908AC6CB7BC2F8C05FEEC10D07D437,
	RoomUtility_get_tokenArg_m4A29EF057FD17659F64C788EA31BD34DF14AB456,
	RoomUtility_set_tokenArg_m1893B90B088E1E320D21CB1E69A5F927F365C020,
	RoomUtility_get_applicationDomainArg_m6463B0B6D9273B0728D2FFE153DEAC3390D81EAA,
	RoomUtility_set_applicationDomainArg_m0D6E9FF12BC37AB796AA1C49563BA9B9FCBA1D3A,
	RoomUtility_get_RoomIDArg_m54E1D3E3B2F9F44C93320AA076F73771B04F8391,
	RoomUtility_set_RoomIDArg_m9A4F97FB878A8DA3033C66B3155E8B9FE6888B9A,
	Example07LengthValidation_Init_m0C8BF48A6563207E0F83D85C515BD51A515212F0,
	Example07LengthValidation_IsTextValid_mA11875B8D99E42DFB2EED8ACDCE0CF1A6DADA7EC,
	Example07LengthValidation__ctor_m5A8EEAB4577E7FE625F17ECDCEA560298841BD4B,
	Example07LetterValidation_Init_mDC31A3B1A2B7C5106B7599158B694DB0D29D0778,
	Example07LetterValidation_IsTextValid_m8CFD040599E1D232AC41D757473B9620213A6034,
	Example07LetterValidation__ctor_mBEA542FAB8453C8559E3918155F374F6849FEAB4,
	Example08_Awake_m30E81DC6B35A0429DE821A996E6DD0E1F6A0C2CE,
	Example08_OnSliderValueChanged_m1CF12085DE8B5304F67E40C1E02E4D57BB667184,
	Example08_UpdateRGBImage_m3D65C133A3B6FAEA42FD2B26ED4AC368469BE094,
	Example08__ctor_m81FF69330C71006BC45C08CBB89049E11EF4B0C1,
	Example09_OnButtonShowClicked_m08492F1815B19DD47967E949A289BFFF9CB4A651,
	Example09_OnButtonHideClicked_m0A6751AAD400C4387BFBAEED5053CF479AFADAB2,
	Example09_OnButtonIndeterminateClicked_m88513FA879DD5831D64EEDA389DE3A87E28D9083,
	Example09_OnButtonRandomProgressClicked_mEDF82281B4D7DE9A0D523F668C5BC31CFBD94D3F,
	Example09_OnButtonRandomProgressAnimateClicked_m94712C740C21613C0BEF003E0DB7E2B9FF15E255,
	Example09_OnButtonRandomColorClicked_m3CA3896CE196A80ECCC3D704D171561966208B28,
	Example09_GetRandomColor_mE65A66FBF07767B9A8F465C9DAFE252A34762C19,
	Example09__ctor_m30485EC079DA13E0D04EB7AB4C6CE4FE7018DCB5,
	Example10_OnIconNameButtonClicked_mE9CD41EA61C9E9D4FCD995A6318919340B5DC459,
	Example10_OnIconEnumButtonClicked_m9F91C29543593A9326AF111B4B6848936DDC54FA,
	Example10_OnIconRandomButtonClicked_mEFE1C5F383C24E71265B2CC04D558157790053E2,
	Example10_GetIconFromIconFont_m3550728FCD083430A1D8567CE18388D683155CD1,
	Example10__ctor_m751F3727A1F991539F4879B8878E77EB0B62F546,
	Example12_ChangeBarColor_mF1C5C1EF085EFF96E1249B90005A97B25E931D6F,
	Example12__ctor_m733AA682292E14DAD8B2829281A04DFD929561C1,
	Example13_OnSimpleToastButtonClicked_m1996DB119BB18ACBE394E90BE71D1411B48F6A53,
	Example13_OnCustomToastButtonClicked_m43B59010F64171DEFA2AB081C00B5BA059D4B988,
	Example13_OnSimpleSnackbarButtonClicked_m24B3D41288BEC0E9B000DAE7304F14E5A7122357,
	Example13_OnCustomSnackbarButtonClicked_m82625315C166B4B1EB3A2D0EEE7898E6B90D2201,
	Example13_GetRandomColor_m42DBD29AE6E5622A07A338FE61FAA55874E7167B,
	Example13__ctor_mF028B6CBBB694785DFE768B987CB4453171C9FDE,
	Example14_OnProgressLinearButtonClicked_m94FBFD57CF0A49328FD392C86510E9FB8898F875,
	Example14_OnProgressCircularButtonClicked_m7E3051E84F5407A9050AD078DD4C07D7DBA7CA3F,
	Example14_HideWindowAfterSeconds_mDC6358A4104517A7EB402BD73F0D80E193253127,
	Example14_OnSimpleListSmallButtonClicked_m20EB9AE4EA5F28346B726E2D2AA4DE50B1049FCE,
	Example14_OnSimpleListBigVectorButtonClicked_m168ED13D1CD71F2025609B45735E55995DFB9F96,
	Example14_OnSimpleListBigSpriteButtonClicked_m6E62B5222EC85A592D41F63C097310E595AC5250,
	Example14_OnAlertSimpleButtonClicked_m131FA900F9019B600D25DC91E6CC90E99C83A8D1,
	Example14_OnAlertOneCallbackButtonClicked_mF705D54C00A16C7B40EE0D067CD5CF3ECF278760,
	Example14_OnAlertTwoCallbacksButtonClicked_mC9EE278D3A2908330C89DD95CECB855C1BD9732F,
	Example14_OnAlertFromLeftButtonClicked_m1AC2ADB6195313D46608E67E42E3626EB0EF864E,
	Example14_OnCheckboxListSmallButtonClicked_mF399C73382B77AD655D77C51F78ED1E2C46AB029,
	Example14_OnCheckboxListBigButtonClicked_m58C076F89E6A8800D5716D006234D78A8DC56069,
	Example14_OnCheckboxValidateClicked_m3B562C13F83CFE28EA9C07488161C32191DD1C99,
	Example14_OnRadioListSmallButtonClicked_m2E03E7E2FE763789E5287D5396F1C59603A5C4F7,
	Example14_OnRadioListBigButtonClicked_m8AC9668B3D54E98073667C141491D0E0DFE3254F,
	Example14_OnOneFieldPromptButtonClicked_m588F09619330F6F6F80E4AE291780AE442BD0179,
	Example14_OnTwoFieldsPromptButtonClicked_m827C59FD870F4E344E134D25C68AB2071A29B447,
	Example14__ctor_m39B36308F0C7F761FEB37A908E676E2FDE16AC3B,
	Example14_U3COnSimpleListSmallButtonClickedU3Eb__6_0_m97715AA61DB81D954E0B160E18A0FE52897C4C34,
	Example14_U3COnSimpleListBigVectorButtonClickedU3Eb__7_0_m5529A5E070CFFF5D6CE7BACD26AB7E9AB1FDA2B4,
	Example14_U3COnSimpleListBigSpriteButtonClickedU3Eb__8_0_mDEDD5E03174BB3BF9A4E00211004CC362B55AFBB,
	Example14_U3COnRadioListSmallButtonClickedU3Eb__16_0_mBB9CF7343BAE6FB5540096F062FA62C84439E8C9,
	Example14_U3COnRadioListBigButtonClickedU3Eb__17_0_m70A3233517652209108A1B8DE2A22932EAE69815,
	Example15_OnTimePickerButtonClicked_m6B0E6C471AFF87048D74FFCA18A881F0B6B4FE3F,
	Example15_OnDatePickerButtonClicked_m285833E5430DAD79B91CA044B961A0DA87AF5777,
	Example15__ctor_mFA1EEB86C2B7CC04C9C14972C67E438994794C40,
	Example19_Simple1_m8FC30F6DF6BD4D326284CB98BCB4227A3E2FAB2F,
	Example19_Simple2_m4A0A3F82BF410FB6F59B8B753C89751E8D2C8A7F,
	Example19_Advanced1_m0A87BE07208D97CBDD82C77E389FCBE16BDE1787,
	Example19_Advanced2_m96329DFAFD05FC430B30008B3EC6F4BEBA25376A,
	Example19_Issue_m04AE0FEEC9F22C42BE202DD7B1CCD31E5A652FFF,
	Example19_IssueCoroutine_mEB376CE59F863E9E1F84A92E166827F88663BD92,
	Example19_Workaround1_m0F03A8373FBAB2C83BCEBFD3BCCB76366E9C1D36,
	Example19_Workaround2_m8B9494A89DE4E0FE29040EC6637EADBB4C9A01CD,
	Example19_Workaround2Coroutine_m2DEEEF16FCD13090A3283C4D77B67EDCE543E727,
	Example19_Workaround2Workaround_m64E394D67DC18FDDCDAE516936E6840B91B6AB7E,
	Example19__ctor_mCC3B1FD4403F4F43525DDD730D4D56F69FB21D74,
	Example19_U3CSimple1U3Eb__9_0_m6268E475BA89D3D2E24728DF2A088FBDA445C8FA,
	Example19_U3CSimple2U3Eb__10_1_m58120A2FE94DD17A5CB4C0404ABE51D8C216FE44,
	Example19_U3CAdvanced1U3Eb__11_0_m1AC1490AFF5A9D233479866432D5343B2B1729D2,
	Example19_U3CAdvanced1U3Eb__11_1_mD58FA9A155F146F5A2D92149E026033CCF4B50F8,
	Example19_U3CAdvanced2U3Eb__12_0_m659BF172F2B29741303029E9E19D1B350A59C338,
	NonDrawingGraphic_SetMaterialDirty_mBFCBAA6C070E91361B50CBC21BD17730D9F95EE4,
	NonDrawingGraphic_SetVerticesDirty_mEB711E12FE00025E6D4C860F76E4684ACCE704E2,
	NonDrawingGraphic_OnPopulateMesh_mE3D3D5747611687CC56765F647350173A0590EFB,
	NonDrawingGraphic__ctor_m12D3DE778345F7BE8DA4ACACF656550EAE107BD1,
	DiceCoefficientExtensions_DiceCoefficient_mD86176D56090F749E0A62E30373E00C4E0BB0B54,
	DiceCoefficientExtensions_DiceCoefficient_m36C4E41523A5EF5F3B45D7B1911DA24B517FE3E5,
	DiceCoefficientExtensions_ToBiGrams_mBBE736DA94E54F69A04A9FB4305771C86B526341,
	DiceCoefficientExtensions_ToTriGrams_m0494A97587CC502C0C95D3315E9EC5708D53E008,
	DiceCoefficientExtensions_ToNGrams_m2785E9BC6FE6408A8860C00C33783B2ECBBFDF99,
	DoubleMetaphoneExtensions_ToDoubleMetaphone_mDE3D04C9B588615AD4FFA7727C4F8B054DBF5A01,
	DoubleMetaphoneExtensions_IsVowel_mA0083CA1ED3825D6EE471BDE519F69E02BC4A301,
	DoubleMetaphoneExtensions_StartsWith_m626C6376D6CF2E045EC6BF03B96D1EAE57FB210D,
	DoubleMetaphoneExtensions_StringAt_m5AC16DC6071F9D699D8257035F15988BFC661916,
	LevenshteinDistanceExtensions_LevenshteinDistance_mB633CCDC4DD9380C218905929494D4F05700D6C6,
	LevenshteinDistanceExtensions_FindMinimum_m732CB504EA80998B8980D37A3F95990AEE7C2596,
	LongestCommonSubsequenceExtensions_LongestCommonSubsequence_m9B7C818FDD90517C9F261C1AA937BC59D5754D2B,
	LongestCommonSubsequenceExtensions_Square_mC17EA4ED08A33A2B5C693C0D9ACC930BC94B13A0,
	StringExtensions_FuzzyEquals_mD0BC06C8FC9931AAC2A7D0873BB4C9BDA82B6308,
	StringExtensions_FuzzyMatch_m2597AFFB36D92A236319B3B447BB40E1ECE89BA3,
	StringExtensions_Strip_mF0549952A9F44B8638BF7E34DDFBCDE04EFAFB72,
	StringExtensions_CompositeCoefficient_m88D5980407A072141404495959029A180014E4DE,
	StringExtensions__cctor_m5F41853E209CCCC61A4009F4B0DAE029E251E2F8,
	AutoTween_get_tweenId_m8AF940DF8DCC33081A502B99A1E82A288DA1E317,
	AutoTween_get_callback_m8D168AAAF1415D1C148389B9DE913DC1E66C7D7C,
	AutoTween_set_callback_mD1A95DEDFCAE9528E24B1555D4CF154FB493EB59,
	NULL,
	NULL,
	NULL,
	NULL,
	AutoTween_Initialize_m1D8F79D37DFBFA771BB3921DBB8787F937A0F17A,
	AutoTween_StartTween_mFDC0AEF9E33BD4DCAED2AEB0A12E2DE77A0CE768,
	AutoTween_UpdateTween_m921A03EFD7D8B318BB1E5EDFA3F34AC993FDC9D2,
	AutoTween_EndTween_mDE4A543057A254A57214FA14355BDCF74210107F,
	AutoTween_SetupCurves_mC6A893A5D5DA30522FD88AB39933DA56EE30E4D9,
	AutoTween__ctor_m82B8A72099951CB66A0C4D18D416800E0E05824D,
	AutoTweenInt_GetValue_mE311989AA505B420D1C35880E0F77A87983F996E,
	AutoTweenInt_ValueLength_mD7355DE0FB5FE2999640A0B0EEC399102F70CE24,
	AutoTweenInt_Initialize_mAFB0C3A99A3EE13F3940EC517C0585494C444283,
	AutoTweenInt_StartTween_mEF6E5A42E53C4524EEE92C93E0D00832F33E7ECE,
	AutoTweenInt_OnUpdateValue_mE0B2C4072A1992D306135EF0F44536B967CA18BA,
	AutoTweenInt_OnFinalUpdateValue_m8ACBB6A4C93F56DAE93AC5F6853DB5A72AC673E1,
	AutoTweenInt__ctor_mB0A1AE114AE8C8AA82B147A39A25982D55D36B4C,
	AutoTweenFloat_GetValue_mD8A896EFBC078FCCF4EE6F3EACAD21DC0317E163,
	AutoTweenFloat_ValueLength_m2E478D29B7F85D46763C44A47AB2F9EA0D89331A,
	AutoTweenFloat_Initialize_mCEAA30EC31D8ED38DABE272F9A53DAB67FF3B6E1,
	AutoTweenFloat_StartTween_m63934E14D2CE3D7C25ACB927600BB9253EFD2A00,
	AutoTweenFloat_OnUpdateValue_m49B81CABF08F9D8E6CF854B737225C7D79723C28,
	AutoTweenFloat_OnFinalUpdateValue_m0C96F3F44A6D08319317B260319CB7D563458B18,
	AutoTweenFloat__ctor_m06CF328F0106B364FD6E83F29FC56B949008C527,
	AutoTweenVector2_GetValue_m4E7D4D4A6CCBEACEA59C4D53979A520287156822,
	AutoTweenVector2_ValueLength_m0360F1EDF471D436F3DAAB6DD84096EF9076EA10,
	AutoTweenVector2_Initialize_mA01357DC072DCD428EF1CBC02C06D857EDE6EAB8,
	AutoTweenVector2_StartTween_m988F45921878BEB6343F801DE6E658156CC32CC1,
	AutoTweenVector2_OnUpdateValue_m2388226A6BF0FAEA5C4D9F1D5CECF21F4E5A914C,
	AutoTweenVector2_OnFinalUpdateValue_mB36F50B1EFA4DF6169350F0A957AFF36079FB408,
	AutoTweenVector2__ctor_m629B71C5287B3B978331A17E3FA613C2F4FDC50A,
	AutoTweenVector3_GetValue_m83BE0D8C825909AA35A04C8C6FDBF091159752C3,
	AutoTweenVector3_ValueLength_m0E3326F583895A2883E3C3D93FB3671887AE027C,
	AutoTweenVector3_Initialize_m76A6E1B0E92A02734FA2B01C4723E5A92447CC45,
	AutoTweenVector3_StartTween_mDDDE4A6F9B1C12DBAC52B453ECC00A84BC2CAA20,
	AutoTweenVector3_OnUpdateValue_mC52960C30D3CC67CB48A4889C9B2CA4B19F77FD8,
	AutoTweenVector3_OnFinalUpdateValue_mEE0723AAE3C0867544388DF8629A58FE6297FF28,
	AutoTweenVector3__ctor_mE8537C528963A989E6C59A82EBFA1919D9488B8A,
	AutoTweenVector4_GetValue_mDBE0D70E0EE30F02CDCD8D5D46F91B188BD6477E,
	AutoTweenVector4_ValueLength_m6002AA43C47FED2F677231F341E35676FEEBC041,
	AutoTweenVector4_Initialize_m5A7941153DF0A96196318E6229F4023528FA711C,
	AutoTweenVector4_StartTween_mA92D2EB491EB21E563A93058608DFBD2FCE67FEE,
	AutoTweenVector4_OnUpdateValue_m8A84DB1D3F751C357BA2F472268DA9AA131F607D,
	AutoTweenVector4_OnFinalUpdateValue_m48C0C613BA71AE53F95B7E51D45DA135D8612FA1,
	AutoTweenVector4__ctor_m742BD3B9495B904DE3DAFEB63A1B65F186C2E6EE,
	AutoTweenColor_GetValue_m20C373C1B8D22CFD92E9955B7630CEE045AC9A81,
	AutoTweenColor_ValueLength_m4EBD5BE1516191FCE42E067108BC6BD6AB9F82A8,
	AutoTweenColor_Initialize_m212D1ED24C38EF486DBE637B49AC0553BA2C611A,
	AutoTweenColor_StartTween_mCF1ED89B6939F068E2E9C509C9343CDB4CC0DBD5,
	AutoTweenColor_OnUpdateValue_m768688242D389ACF68343B5CE44753DB440CF9D3,
	AutoTweenColor_OnFinalUpdateValue_m33AD9E018679D6F13D5D906FBC067A0492152758,
	AutoTweenColor__ctor_mC409B62BF00D0952BD0B8B71445DFA800AA296B6,
	EasyTween_get_tweens_mC9FFF0E9A2426991BEB4705E21E327DEA3C77ADF,
	EasyTween_set_tweens_mFB860F69A9767C299DA67A6A1E4814B204525C7A,
	EasyTween_Start_m559D088257D31B226B20C337A8718A4C710C615E,
	EasyTween_TweenAll_m2C4E76C8D5EECEED2F1D167F73B6AF8CEE8ACCC5,
	EasyTween_Tween_m0EC07A2794AD649A9D4B1AD546A8C945D71044D8,
	EasyTween_Tween_mA8CD0DD3B7E3C4219784C6279D215E7E65E60622,
	EasyTween_TweenSet_mDAA98231E1BA9217225F6976B4AA056DFA7BB047,
	EasyTween__ctor_m79265B07B96498B4BBA702C37E7C283EF9C5D71E,
	Tween_GetAnimCurveKeys_mD97B9A674AA2FD4FA8F18F152C475A681AADE00A,
	Tween_CheckCurve_mC4C93A2C9617C3E6CE1DCDA727A58CB0F31D0BC1,
	Tween_Evaluate_mEE9273B3B6CB88B1086A7CBB5358E14B0F150406,
	Tween_Linear_mBBFDE8000F3130224523A8BA5520950B27CF6CBB,
	Tween_Linear_m4846926B64BB0CC64A2CCDDA234443CD90D97225,
	Tween_Linear_mEF56FEA8DA5999665FB10CA523264562408642FC,
	Tween_Linear_mEA4A7FD69B97B3BDB0D1EB96AE709D6974AC7DA3,
	Tween_Sin_mDDD49F49F9369747C69D7BBB4BAB3F9D0657400C,
	Tween_Sin_m302759BD95A00977F0D84F3A3EA071683844A5AE,
	Tween_Sin_m860A26C93F66E909547925F5ADE251D73B980A30,
	Tween_Sin_m98EDE3EDBBBB06B41C39751B24B0BFD6333C685B,
	Tween_Overshoot_m67579699BF35994ADEE2DFFA9C55FEA1908F3D21,
	Tween_Overshoot_m44CE58EE33978BE06D7C2B965CD28FAB5897AEF0,
	Tween_Overshoot_m82E27ED5B5FF8EFB3F6440DAC9995B684BF35201,
	Tween_Overshoot_m65BAC7277C9430F155055648301FB4A1D9A422F1,
	Tween_Bounce_m8626DC79F0C1F24C993B5120BA0E1925BAB1B374,
	Tween_Bounce_m2507C96E603C3E8C04B2B2963F78C4A397650FF9,
	Tween_Bounce_m594C096E5C6F0FC89D8DA694E3FB6262F857B9A2,
	Tween_Bounce_m0F465A093D460DA7421E1855F275FF488D4718E3,
	Tween_CubeIn_m48621CB1D7356E1337F2C115EACB956A5CE89D30,
	Tween_CubeIn_m02E254302A1392BA9040BDE40ED820A301E5E388,
	Tween_CubeIn_m78593AC288D6F5820F98DBD8EEA043F9D6B3E6D7,
	Tween_CubeIn_m790907FD6B5CC639FFB31CB83B81D5E0799D412D,
	Tween_CubeOut_m044B4C58B5388BDBEFEE67CBEEAB86FD9238F1BF,
	Tween_CubeOut_m9571201EF2F47CB41AA1E21F9B1FB47E197C123A,
	Tween_CubeOut_m2F50BB13EEB9F127409E0FEB5BD02667D4EF1C0B,
	Tween_CubeOut_mBCCE8B34E8C4E725F627198DBCCFF5AA7D3BD31D,
	Tween_CubeInOut_m7280607B207190679C17D78F77E22B084E0228CE,
	Tween_CubeInOut_m24194D7EFCB926C6FF8B41EEE9444B336016FF7B,
	Tween_CubeInOut_m324CE1501D2B0CD922CCD4A124FD2BF7DAB27F8E,
	Tween_CubeInOut_m1E2CE889496D5999B5298ED33CE4CED039B0B8CA,
	Tween_CubeSoftOut_m9504331FDF80AA9E0F83D592DF6F93C615C1DB85,
	Tween_CubeSoftOut_m09CB81AA0EEF812CD7CFE4747F132CF553C8951D,
	Tween_CubeSoftOut_m977524D910FAFD14AFB41DEDA50AD9B2FBF13E0D,
	Tween_CubeSoftOut_m00BEC2790CBFAB8E861DE2F5261228C82E798BDA,
	Tween_QuintIn_m5F6F91BCD7E8145F7E41771CED21C8F779F75E88,
	Tween_QuintIn_m085ABFD9BEA555322A3EA688E3D45B6E42FE7F3B,
	Tween_QuintIn_m2D37758663D58F53D33BD3BBAFF85B8979A29DF3,
	Tween_QuintIn_m3E4BD495A52E70FC547FD0588BAEFADD973B8F01,
	Tween_QuintOut_m3482F4CBAC49F00FA047290AF0CAE369BE4BA30C,
	Tween_QuintOut_m78CFFB2AC0E4A677C0C643CC494279C588FA084F,
	Tween_QuintOut_mFB20677BD49764BEACCE79FD4FE3672FD5948FB7,
	Tween_QuintOut_mFCC8509B754B0CC414A6CDF76C1169E98ACF69E1,
	Tween_QuintInOut_mAD55FBE3D6070B7DE9E25B712E2D8D38A4BB6753,
	Tween_QuintInOut_m63DDF2E1B80D1D07F0673A39B0A0B9639B8F4C9B,
	Tween_QuintInOut_m849D3B0F27B669AD25864B09F2CBC1095E2FC038,
	Tween_QuintInOut_mF76DDEA8D553F70CFB258FEDCC7484E5676944BB,
	Tween_QuintSoftOut_m3BBAFC1C5BBEB92901E1092713C23C61CDC9138C,
	Tween_QuintSoftOut_m7D1A3BA983773895DC9E4C2570379C5DD018A314,
	Tween_QuintSoftOut_mB2BAE5B826F087E15C7DCABC1D04C4922610ECE1,
	Tween_QuintSoftOut_m2C925EC0B647CAB794178A4AC7B3C1EB20AECB6F,
	Tween_SeptIn_m753FB9681D2D4650986788C8F066D47A487D150D,
	Tween_SeptIn_m6DA5666A1DF907BE5959C258EFDED2CCE0E3B2A4,
	Tween_SeptIn_m3B564AE279DD8AE7417D34BA6989DFBB780156BC,
	Tween_SeptIn_m6CA7BCC719ECAFAEE34B62D3DA6AEAA71BA04065,
	Tween_SeptOut_m6A0BCD2C38B7243F997F23E87861D38059CB2170,
	Tween_SeptOut_m2231328F1540D8CC5BD5019D660129FE4237E1E1,
	Tween_SeptOut_m6E6D9937307A0FDBD8791C9852C8AD4B6BAAA886,
	Tween_SeptOut_m5913366B9EE61F06A6A0C629AEAA45FC766F7391,
	Tween_SeptInOut_m8063894F69C28E39316E659843A6C32CBC7B1D77,
	Tween_SeptInOut_m74566DDA2493EF67AC61DB5B920049BBC3AD71FE,
	Tween_SeptInOut_m10E20474CAC5D2320E89F453F660EBFC98288A46,
	Tween_SeptInOut_m8034F8C1465109C3F440B9F9EC9FAC9CB701ADFC,
	Tween_SeptSoftOut_m239D302A762238630D83819696D2E5FBEFE93103,
	Tween_SeptSoftOut_mABB28707CAAEEF59BD9BC4B244DCD9BFD2AFC4AD,
	Tween_SeptSoftOut_m011B89E4F9D21E849B83C7CD7B00D2964C8528DF,
	Tween_SeptSoftOut_mA41C8F30360FE2005730DC3AE5E6B8C4965D4264,
	ColorCopier_get_sourceGraphic_m84DEF3E7675620006396F9226B76FAA6B7A17908,
	ColorCopier_set_sourceGraphic_m0FD1A589A14216055AEC8EE99CE66D8096433EA4,
	ColorCopier_get_destinationGraphic_m81F8381EC734E054764BAD5DACD2003E9AD24D53,
	ColorCopier_set_destinationGraphic_m3BF49D1E40E29E622162B52DE6BBE7E89157536A,
	ColorCopier_LateUpdate_m40F10AE90AD685EBC4B7EA7DA37BACAC1141E978,
	ColorCopier_UpdateColor_mB55D0DBC42DAF8D200D56D033BCA66CC09E0BE5B,
	ColorCopier__ctor_m9BE311C6D287FBB3AEA1EBBCEB798340BF473994,
	DragEventSender_get_horizontalTargetObject_m19245468FA518D698C940DB301D8FE450CD7A21A,
	DragEventSender_set_horizontalTargetObject_m6B20491FA08DA98F6C2DBD90DFEB4BD083BDEE9E,
	DragEventSender_get_leftTargetObject_mF966DBF2C3B7D0742BB982927596B0A924A492BF,
	DragEventSender_set_leftTargetObject_m1A06D4873895FB0467081D43EC6551BB8155D6E1,
	DragEventSender_get_rightTargetObject_m390E5DD67300D071FF48E5977A1A02C54514183F,
	DragEventSender_set_rightTargetObject_m7C4B76C9D6008DD6C84640E8CB948531C9AD7EFD,
	DragEventSender_get_verticalTargetObject_m08CDA824D673980BEB2E905E8A7AC19A81C00979,
	DragEventSender_set_verticalTargetObject_m2029FB6CD8D16EF04A9E7542AF187B3198DE7142,
	DragEventSender_get_upTargetObject_mBD25A6A6EE8B640EDDA1AE13D4CC324323DFFBF4,
	DragEventSender_set_upTargetObject_m9E167A804BD74621EA522671AA214AD7CC91550D,
	DragEventSender_get_downTargetObject_m3D10D1E62CA2134C1D7DF47B7C7F0CCB6A1DFA5F,
	DragEventSender_set_downTargetObject_m18D91466FCFD1EBF13DD995D47E91B10E4684398,
	DragEventSender_get_anyDirectionTargetObject_m0BA75862B174B14CD40F7D0DFB7E01305169408C,
	DragEventSender_set_anyDirectionTargetObject_m19C7990D639C280A5C855377FA41CB6DA8969EDD,
	DragEventSender_get_combineLeftAndRight_mD9B10846174489E7DF8AA15F0A0C2FED7871DFEB,
	DragEventSender_set_combineLeftAndRight_m60B2045057B96528566CD31673F2089757578D10,
	DragEventSender_get_combineUpAndDown_mA1E63EF5553E9CBA49EC5FB4D292A393398227E7,
	DragEventSender_set_combineUpAndDown_mAE5E52CDC5D847B3EB25A2F06DFC707E7BD9EBF8,
	DragEventSender_OnDrag_m342C2F3ACCF06F1ABDF8A6271113FD5AEAD9C55F,
	DragEventSender_OnBeginDrag_m9C1A55F2963C8B8B68C241CD528B6E99468C850E,
	DragEventSender_OnEndDrag_mFAE5C051BF26B20C571149A95CD48CFE8EA60C5D,
	DragEventSender__ctor_m64DE04320B35B747157DEB8802541FBD8A9E9F4C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MaterialUIScaler_get_canvas_mF2A9417576A77163BD6C40386311759AA5550C12,
	MaterialUIScaler_get_scalerMode_m42B6E855956CAED5E0043ECDF214066DA772CFDE,
	MaterialUIScaler_set_scalerMode_mD2BFFA6FA5E47233D89F5CFF01BD97863EADD9DE,
	MaterialUIScaler_get_scaleFactor_mE6FE2FFA6BD08DBEB561CDF4556754DD318128B5,
	MaterialUIScaler_get_referencePixelsPerUnit_mBDD321F460924D58F6B10985C9211958C5A80197,
	MaterialUIScaler_set_referencePixelsPerUnit_mBA1425E666899CB2C532FDA84B6BF904CB594C28,
	MaterialUIScaler_get_dynamicPixelsPerUnit_m6E9CB6AADC4FADFD8A67FFF6B7B59C24EBDD109A,
	MaterialUIScaler_set_dynamicPixelsPerUnit_m2C50CF16A3442613281047603D0B7493C3FFA608,
	MaterialUIScaler_get_baseDpi_m7FC517E10C8E878B4C6D8C22EE0EB9F73BB50F2C,
	MaterialUIScaler_set_baseDpi_mF946537201AA79AF5071FF63C894780524F6CB2A,
	MaterialUIScaler_get_fallbackDpi_m944D295E9748AD28D05D471A6803ECEE38076611,
	MaterialUIScaler_set_fallbackDpi_m8B4C6DDFAFB5542E36E5961BC34712DD327AEB15,
	MaterialUIScaler_get_scaleModifier_m032FE3EA0DCBA651DFEB48472104F609F8EBDC9A,
	MaterialUIScaler_set_scaleModifier_m1AD9566B290A706C708E49C91551A419D7933159,
	MaterialUIScaler_get_scaleSnapLevel_m6944CF8D3178650BCB3871059AA6988D1BD6D764,
	MaterialUIScaler_set_scaleSnapLevel_mF7CF26D2BA75FAFA84482BFCA1A0840C3C3A2C08,
	MaterialUIScaler_get_targetScaler_m86E0223BA7A2FDAD9DBE723BD6E990F43C9F5397,
	MaterialUIScaler_set_targetScaler_m3F59754A1EDBDFF2FC70046663028BA5E3419545,
	MaterialUIScaler_get_targetCanvasScaler_mB4DCE6589649DDD7B8CAE98E0683F992CEDE84E5,
	MaterialUIScaler_get_orientation_mF7C840E9BB4BA99B92BB5EDB37367084D8F96D92,
	MaterialUIScaler_get_onCanvasAreaChanged_m58FC2E64020DA599D1F745E9C3320E30C9F92E1A,
	MaterialUIScaler_set_onCanvasAreaChanged_m9A380E2F66BBC29DE3AF0BDA26AC4B2E4C13AEB5,
	MaterialUIScaler_Handle_m54B5E210C68DDD648100E9DED4A75903C6A173A9,
	MaterialUIScaler_get_screenWidth_m4CA118D7E1799CBAAAB5F1028D993D9C821F4D34,
	MaterialUIScaler_get_screenHeight_m12E4FF7BA57104EB060F5FFDE7A8A56C2C99D03D,
	MaterialUIScaler_get_dpi_m796D3F18565B17918DBCC3EFD5F81EBE67BB41BA,
	MaterialUIScaler_get_screenSizeDigonal_m082CF8345C0BD5325ABB59220E687A62B8CEB13C,
	MaterialUIScaler_HandleConstantPhysicalSize_m043CE33FCD93DF16F6D982FE508A9B8A60B375F9,
	MaterialUIScaler_HandleCopyOtherScaler_m770A2D68661AC9EC887676E0BC9B4CBE0ED0144E,
	MaterialUIScaler_HandleDontChangeScale_m85C24CF2D43BE393729F3720BB9D9A777E99DAB3,
	MaterialUIScaler_ApplyScaleSettings_mED3EB6ABCFCDFCE0B24326B39A448267F858C94F,
	MaterialUIScaler_OnEnable_m738B910D0F4D87559A98B898596A6982BB70EFCA,
	MaterialUIScaler_Awake_m32D2A250AD9A3F0501AA4DF72C8D30E6C5F16B7D,
	MaterialUIScaler_Update_mB97B2DE707FCFF1546C51DFAB3C7118A99EB227F,
	MaterialUIScaler_OnRectTransformDimensionsChange_m759807255EA1A8CCE940BBD775B91FC1540C79A0,
	MaterialUIScaler_OnDisable_mFD341248EAC08C810BD2AE724CB5CF990AD66541,
	MaterialUIScaler_GetRootScaler_mDA79F4FFDD8613EC8998BA053B013DC480925B7F,
	MaterialUIScaler__ctor_m462A2F17D0DE7A0F7FAD8FB2B69AF1436D36346E,
	MaterialUIScaler_U3COnEnableU3Eb__71_0_m8DC734FACB159E080B519FDCBAD6FC9DE3758053,
	MaterialUIScaler_U3COnDisableU3Eb__75_0_mC70FA52BBB24BAA2E988226AC9644FF7C5A1592E,
	RectTransformSnap_get_rectTransform_m54D49EA9080B60B889EBC6838859161A4079E677,
	RectTransformSnap_get_sourceRectTransform_m247B732839CE552870C93F6DA28B627953559AFC,
	RectTransformSnap_set_sourceRectTransform_mF05E407501BD53E047726E12FB65D91AB9949491,
	RectTransformSnap_get_padding_m62EF85189163BFEA344230CCFEF9115708CC1057,
	RectTransformSnap_set_padding_mF442CAC844A7AFF6F7AF0C9C0B05290EA8F7BC16,
	RectTransformSnap_get_offset_m95F398768C6AB9D74EF350CB62B30C67A46F0D74,
	RectTransformSnap_set_offset_m5E41A3C8B1988A8EEDD2BB846550C1EE136ED09B,
	RectTransformSnap_get_valuesArePercentage_mE97948EB05FB65AAE3D82E9F8A4037BD9CE56888,
	RectTransformSnap_set_valuesArePercentage_mEEEAB716DE194C3E92A12DEF35184E795ADCA438,
	RectTransformSnap_get_paddingPercent_m80FDA12473918B6AAEC7D0071D7785378184D554,
	RectTransformSnap_set_paddingPercent_mD7C43BEB69E698072CF5BDD3B055CC50A1B1AE82,
	RectTransformSnap_get_offsetPercent_mB6B2CDC67F361600063755284541DB5B3EB7B983,
	RectTransformSnap_set_offsetPercent_m3E39D979BB3331D38FA8387513A01FA2B2442700,
	RectTransformSnap_get_snapEveryFrame_mD8CE68EAB1264BA162C93C2902CAA08190E37FC2,
	RectTransformSnap_set_snapEveryFrame_m909DFC85B562F0D00DC5E0218E3A72A461D09073,
	RectTransformSnap_get_snapWidth_mE185D1754FE44B1ED76CAF3EDCDCD8C00049097B,
	RectTransformSnap_set_snapWidth_mB8C541F527324A671D24A198A508996DBF09DCCA,
	RectTransformSnap_get_snapHeight_m5533359193F8F5C9AE7A08366B442A9422BA6A77,
	RectTransformSnap_set_snapHeight_m6AA43B5FA0F9577D9FA3131DA51A8C58A3B9C184,
	RectTransformSnap_get_snapPositionX_m65B4DF74ECEA0021A9B5647D546AC46C347DD989,
	RectTransformSnap_set_snapPositionX_mDDB4C80E1A1A83C9ADB2ADF966556CCBF9AA1980,
	RectTransformSnap_get_snapPositionY_m08E9E77DB06C8908D410FB595302430C47127CA3,
	RectTransformSnap_set_snapPositionY_m584C98A29381510F59717C523C497BEC9A6EEA0A,
	RectTransformSnap_Awake_m50F8220E6C0ECCE4EC3F4330576EC3D90471934F,
	RectTransformSnap_OnEnable_m90C35ED93CBD649CCFAD78EDDB66DF4FAD940D5D,
	RectTransformSnap_OnDisable_mC13E78B9666B3EF73F0F4A0E56B2D94A5B78480C,
	RectTransformSnap_OnValidate_m562D14DB9549CFF20F109927A21A47462AB3121B,
	RectTransformSnap_LateUpdate_m25EA1629524CD4448D7A16E50D163A6C4540CB42,
	RectTransformSnap_SetLayoutDirty_m49EA273CA8C5C3B2D3E390E51008A106C810ACCA,
	RectTransformSnap_CalculateLayoutInputHorizontal_m7BC9F56D1DC86A96C5E2E843449A0CEC587B6873,
	RectTransformSnap_CalculateLayoutInputVertical_m09A376510CDA9F6E8958C16775D6392A4CA4E9AB,
	RectTransformSnap_SetLayoutHorizontal_m9D85C2DC67D53B9C8CAD48CEC3A58D1E545E05DF,
	RectTransformSnap_SetLayoutVertical_m2839234DBEDF917D3F2D9D0CB8192B9C41036C65,
	RectTransformSnap_get_minWidth_mA95E996DF2E38EEF6EB0F6DA57E89CA5F3D298D2,
	RectTransformSnap_get_preferredWidth_m529CC3FB1EDE5522FAFA2A2EBAD64A725F9BC591,
	RectTransformSnap_get_flexibleWidth_m40220382E08D7A3677AFB3262F69A8F0AA123B49,
	RectTransformSnap_get_minHeight_mD274B8677D4A00A733567BB67A99C44EA7B3CA34,
	RectTransformSnap_get_preferredHeight_m23B36DF84F958B26A7B5043328C73175D0102A2F,
	RectTransformSnap_get_flexibleHeight_m0F9B9C5186CB2A80FCA4466769C7F12DE2BBF8E4,
	RectTransformSnap_get_layoutPriority_m171E5AA95914EBAE85317F89E6802134C2C8F3BF,
	RectTransformSnap__ctor_mCE276E4646B5D02861FAC2009A25008B5149BE2B,
	MaterialRipple_get_ripplesEnabled_m37F475C987E958ACBB0CE1866DAFF18183CDD43E,
	MaterialRipple_set_ripplesEnabled_m32355BDB1A1335EE3F1CFFF440A0B4CC69589DD7,
	MaterialRipple_get_autoMatchGraphicColor_m82AF37DFB549B9ED8B314E009D8135411C255BD9,
	MaterialRipple_set_autoMatchGraphicColor_mE643C70043C18399F1EEF6CD4C332C86D1560CCF,
	MaterialRipple_get_colorMatchGraphic_m3A395A95BD7BBE08339DB1BC81D78033F41F1677,
	MaterialRipple_set_colorMatchGraphic_mC6829FF2C75BACFA473BE6149DB38C9012894768,
	MaterialRipple_get_rippleData_mCD22847BAEB31C09FBF07897C241AAA0BE015C40,
	MaterialRipple_set_rippleData_m1780B2FEF3078D407102F192E4A560A4D9F179E4,
	MaterialRipple_get_rippleColor_mD2677CF78CBD1533C53B0A7ABAC7531BD12B5807,
	MaterialRipple_set_rippleColor_m51B13539F9E0E69AC48FA2C4373F630A32747C17,
	MaterialRipple_get_highlightGraphic_mA9488135A3AE1A2212F9E69D1BD6117863C19058,
	MaterialRipple_set_highlightGraphic_mC8C0F25F4BEB8667F3767ADD0223B3CD5940329D,
	MaterialRipple_get_highlightWhen_m7D56291537D04405425384D06E82DC899BFB2A0B,
	MaterialRipple_set_highlightWhen_m9503AAD2C0773881475931EBA3572AAACFC0A22F,
	MaterialRipple_get_autoHighlightColor_m7C844EEBCDB21CCCAF917B6BA5C57EF3483DF8AD,
	MaterialRipple_set_autoHighlightColor_m023B1E202CCE6C18DBA69537D20E311438F083E0,
	MaterialRipple_get_highlightColor_m06C20306F4811603350BC60E1D48E847B260A1CE,
	MaterialRipple_set_highlightColor_m051F5B850F4A883114710A2F6A1E2077C5EBC2C7,
	MaterialRipple_get_maskMode_mC35B00FA62C08DFF42F03085C6FFACE9EC445B43,
	MaterialRipple_set_maskMode_mF97EC862F1B7A337BD45E039B93BA6A53A2C95D3,
	MaterialRipple_get_checkForScroll_m7D1DBC306226702C13A9CF9D5503FA8DF5268BD7,
	MaterialRipple_set_checkForScroll_m9BF0E2704F5CFE92E0F8C2C0414CD678E6A98836,
	MaterialRipple_get_rippleCount_m2B9858E9FE05B1768E2595997A4C7AA540B88000,
	MaterialRipple_get_mask_m48FBAA34A73D351ECEF8A4BFE292A28240E22447,
	MaterialRipple_get_rectMask2D_m894832DC44F946E1C7E523788E5073505F128075,
	MaterialRipple_get_autoHighlightBlendAmount_mDCC3C45ECD50658231F93851AB4952184F73993C,
	MaterialRipple_set_autoHighlightBlendAmount_m0C6554FC2C631EFE9A2536ADF2E94434629957FF,
	MaterialRipple_Start_m23DDBD04E448A8E896B7F3057669097D56976B67,
	MaterialRipple_Update_m74FB178DCE1C3F00C54DF0AA9350323371E760DD,
	MaterialRipple_CheckTransparency_m499D1B93066163C1312015B1E15AC61B4EA4FE94,
	MaterialRipple_SetGraphicColor_m12D4C78945DBE7693255C82719125DFB9EE6E05C,
	MaterialRipple_RefreshSettings_mA6871EB9780A2299D210CFC735BF0904AF742F41,
	MaterialRipple_RefreshTransparencySettings_m064C614D12AE68C9BCBB29F4FC411040A7A7A2B1,
	MaterialRipple_RefreshGraphicMatchColor_mA8BD2D150C07DD90F192324875A62E24D7756E47,
	MaterialRipple_RefreshAutoHighlightColor_mF85521ACA0CE1CA7C7A600977AF9811118A14998,
	MaterialRipple_OnPointerEnter_mE19B705FC3B667D440D98FF6E7FA4E230AA880BE,
	MaterialRipple_OnPointerDown_m1B40ECE006D21DE22FADE52C30ACCE0215C602D9,
	MaterialRipple_OnPointerUp_m1F6B4C9DF19ECF631F189BCED115B7ABD43BF8E9,
	MaterialRipple_OnPointerExit_m38BBD55F746E727C874207365BA7AF136499793F,
	MaterialRipple_Highlight_m40FBFF261964F1AD68975CEEAD4F4E3853641F1D,
	MaterialRipple_CreateRipple_m87A6E687D351ECFF8605B2EC2F6E27E0687FD447,
	MaterialRipple_DestroyRipple_m46921A9D093D1D5424ABDCA2F4D7E56A942D8A0D,
	MaterialRipple_InstantDestroyAllRipples_m59B3BADBCEAB503260493FBADCE60BD644E00E75,
	MaterialRipple_OnFirstRippleCreate_m7BCB156675A7DFA4A2E17AAE2C1481079A3141A0,
	MaterialRipple_OnLastRippleDestroy_m9FBC8FFBF950D31AFE92C3EAD7C8A5159C1A9989,
	MaterialRipple_OnCreateRipple_m10653879934ADACCF21500294A46D0D181139510,
	MaterialRipple_OnDestroyRipple_m2EE80D81A2B70B3224D7D8B057E4D66813FA66BA,
	MaterialRipple_ScrollCheck_m5734CEE69BAB1B68FD26B33DD5DD4EDCADBDCFB6,
	MaterialRipple_ScrollCheckUp_m3E237E8DEAF9F6B7BCBCD61D5DB01CBCAFDE74A6,
	MaterialRipple_SelectCheck_m9BEF63FE1D53CEC2889DE98DCC2B3033FC49899B,
	MaterialRipple_OnSelect_m431BB267373252EFAC16138ED01D3B863E488546,
	MaterialRipple_OnDeselect_mBE60D1E498369D2E536B9B2E982327A1C248241D,
	MaterialRipple_OnSubmit_m587FB342AFEB3B85082CB714AF2DCAC4EE83BB8B,
	MaterialRipple__ctor_m9037A8A2A2D4BDC27D190AFE84A2D8B198071A2B,
	Ripple_get_id_m846D684837CF5643897A2123FC3D7AFC6C447421,
	Ripple_set_id_m0010E583AACDD2E6CF0DBFDB1404CDB2A0533C3B,
	Ripple_get_rippleData_mA0C4FF524320D46F0B36FD782BB4601EE349A5AF,
	Ripple_get_scaler_mC8A88FEA8C960008E197D4E6AC026AAECEEE5E3E,
	Ripple_get_canvasGroup_mF0A897D97276F03B59180CCD53A1E9BE1260386B,
	Ripple_OnApplicationQuit_m186B69A10FFF9E2AEE39C063BC46089C6AA96B73,
	Ripple_ResetMaterial_mB848C909B8FB0A8256FBF1A927549624B0B18430,
	Ripple_Create_m2F0EF7EC6F8A0268BC079530E6CE094EA2A89A87,
	Ripple_ClearData_m398776DDED5EB743CC05A30AA048B7E336783FB7,
	Ripple_Setup_m37BD46BDE3EE2BE0F984640B9EA561BADAAD0DE5,
	Ripple_CalculateSize_m53C756C38EDB4D7A1C9FADC55B64996F22DDE885,
	Ripple_Expand_m4C321754822C12A1DEB7D79B41182EACE0D1599F,
	Ripple_Contract_m807EEF099870F06FA9C47CDAAB6E2F76847BDF9C,
	Ripple_InstantContract_m8E5875162CC4D8D448BFA3ED80F8D4DD336587C1,
	Ripple_Update_m2D6806D7023517EF783E12DE0956F1D01558284A,
	Ripple_LateUpdate_m27CB38C449B1ACF139A7F6F4C062F66CCBD87278,
	Ripple_SubmitSizeForSoftening_mCA092152E729A634D7A4423D5414E11B9DAE199E,
	Ripple_CalculateAverageSoftening_mB3A1503580BF72F168FD529674C87D0CF4AE27E0,
	Ripple_PowerExp_mF0909FFA813D949AEB71380B24ADB4D6087AB456,
	Ripple_OnPopulateMesh_mBC3AB0BE76ADECE69A5EEF779FB35A952037B675,
	Ripple__ctor_m89AA7E00291F9E2DDAC17622C00DD5765380DBE0,
	Ripple__cctor_mB7E4D52822A6A2A4C18965887C22F45680AA6B4E,
	RippleData_Copy_m7DD19AE6825B5A0E41DD44DEBBEB5C70125FFD8A,
	RippleData__ctor_m784A6C5B1E6A68429E285C8FC5B2380859E7FDA3,
	RippleManager_get_instance_m3608FC81DE356D51E057E5E4FD6F038E1CF83E1F,
	RippleManager_get_rippleImageData_mB03F9E945886922FAB3CC3297B750530206402B9,
	RippleManager_OnApplicationQuit_m0B9E333F9FC47C916FFAF65ED7EEEFB36AB67630,
	RippleManager_GetRipple_mB3A2DDE7145E3EDEFAE6F3FF817FC4BE17DBE704,
	RippleManager_CreateRipple_mAF30148EFAEA47E110A58FEDDEED6DCDDDFB7CD8,
	RippleManager_ResetRipple_m3088243652CF9A420289F5503685B57D2CAC314A,
	RippleManager_ReleaseRipple_m527C32D8ABA0492DF2BD516494A2B98BA4B8D097,
	RippleManager__ctor_m8A787A32DB0F1FFE99DF71B54589FD80ACC6ECC2,
	AnimatedShadow_get_isVisible_m1B212C07EAC1417E2DA9CB1B399F2E98BFE1A27E,
	AnimatedShadow_set_isVisible_m9A90AB8C42228DBB4AB572EE114FE1B917D0E3AC,
	AnimatedShadow_get_canvasGroup_m8D61B1A3607012E65A1C36C04AD191F20FBD764F,
	AnimatedShadow_SetShadow_m1BE7629A661C7817823E468FC48A846A9CA246E5,
	AnimatedShadow_SetShadow_mFEE32F3088AD68953022CA6D7EA379220BAB5FDE,
	AnimatedShadow__ctor_mE19889EC6B3414833E3C6AB561DAFB57C334F4A4,
	MaterialShadow_get_animatedShadows_m319C46EEE9A2D58CCFAFCFD0F5A1C404363A8C5A,
	MaterialShadow_set_animatedShadows_mC60C4559C2A5A0E8B95C65668DD099A0D6F881C8,
	MaterialShadow_get_shadowNormalSize_m7AEDBDA14E24F45AC8A4B6B8BEB650EA03844065,
	MaterialShadow_set_shadowNormalSize_m290BF916B4E19EFBCF5D5B38405C4E6B9476EB2F,
	MaterialShadow_get_shadowActiveSize_m183CBD599578C7468273EF5D01EB62B9C1555B7A,
	MaterialShadow_set_shadowActiveSize_mA2264711A01476F4F54861270F062AAC7A0BE019,
	MaterialShadow_get_shadowsActiveWhen_m89F024A388A3B13D3EC8EC9E1E629B33C443A940,
	MaterialShadow_set_shadowsActiveWhen_m486C23BE7553DBCD0ED7C6E297FEE4C5494ED06B,
	MaterialShadow_get_isEnabled_mCAC28828029333BBE51C38FD111D6309E0E604F4,
	MaterialShadow_set_isEnabled_m42857901BAFCD722962B88470F97AC5B49A8975C,
	MaterialShadow_Update_mE40C78992D32CFBCF6DBC989E56F564ABC05EBE0,
	MaterialShadow_OnDestroy_m5E31B2CCF1BD9DAE528835875FDCFFC85CCE9209,
	MaterialShadow_OnPointerDown_m3B667F8526CEEA9B27F19244099F446FC65F3679,
	MaterialShadow_OnPointerUp_m80593F8D2CB4784509E7B434DEFD562945E0A416,
	MaterialShadow_OnPointerEnter_m1C3296A6957503001E99C6410E7B0BAA7A209D59,
	MaterialShadow_OnPointerExit_mA45C3E4017CE156DE5DEF337F8EA8C03D76C4989,
	MaterialShadow_OnSelect_m538C9CEADAD09C0496E9CE4246C82F85DB4E3F81,
	MaterialShadow_OnDeselect_mEAE3FE9FDD404F437C9A34CD9F19EDE406F17AE5,
	MaterialShadow_SetShadows_mC245B221089CA17C033843578AEFDA39BA7584EE,
	MaterialShadow_SetShadowsInstant_mA388E87F0C972BC0A122162D201E55DAE8B199D1,
	MaterialShadow__ctor_m6BE92663F2EB025917BCD571B46BCE246F354EC8,
	SpriteSwapper_get_targetImage_m293B917F638D0333B78B898AF8D1E51C81A332FB,
	SpriteSwapper_set_targetImage_m3E9B4747EB885080D8F92FF87A682598D1DDDD83,
	SpriteSwapper_get_rootScaler_m534798369AEDFDF53A4B64B39AF834D93E337B64,
	SpriteSwapper_get_sprite1X_mA2155D36645AF7AFE751580695ADEDDF5619A558,
	SpriteSwapper_set_sprite1X_mB2B76AFD0A1F9AC6E24AC79C9B2C5E0F04E267E9,
	SpriteSwapper_get_sprite2X_m7CE65695D092DF1FB215074925806AFBA5F4548D,
	SpriteSwapper_set_sprite2X_m1467ED95640A657E75650184FD7327289FA03A5D,
	SpriteSwapper_get_sprite4X_m4809B344F5C190DCAF8CD67304C8BFADF0647A72,
	SpriteSwapper_set_sprite4X_m0E1B7AB1746CDB9BC8778D9922AC7ED3B7E7C4F7,
	SpriteSwapper_OnEnable_mE9EDEDF8A691285D394B90EBE7FB2574EBEC7983,
	SpriteSwapper_Start_mD0C8D7A87135A8102EA46F9A54492D36711704F7,
	SpriteSwapper_RefreshSprite_m6774B259A7488152418B8E10439402B875A081C8,
	SpriteSwapper_SwapSprite_m47B5B7E137329732B9FC52CCEB0DB345E1EBC201,
	SpriteSwapper__ctor_m505D95F671585A86CA726162C7D748878B1697FC,
	SpriteSwapper_U3CStartU3Eb__23_0_mE17275B5E6260770FA4B7BCF0AA1F398A1038A1D,
	ToggleBase_get_animationDuration_m789775EE0CF2FD9C89EE27FAF63F5D39DCD46CA1,
	ToggleBase_set_animationDuration_mF65FEF608CC124C7543C1032CCA47643E0AA6747,
	ToggleBase_get_onColor_mC6186142820492783801A6A1FCC38A2474A042F0,
	ToggleBase_set_onColor_m738907C6BD910D12B43619C4867F3E00D2F385B5,
	ToggleBase_get_offColor_m723A794F2A3EE2E63C3A08BDFDC4574BB362D871,
	ToggleBase_set_offColor_m632A260D8A6784CC12860461D47217CE6756F779,
	ToggleBase_get_disabledColor_m10C5638D3CA8D7B5035754B43F35F5DC7702A611,
	ToggleBase_set_disabledColor_mF0CE39B97D4380DB9178CC4764105EC1CE7737D6,
	ToggleBase_get_changeGraphicColor_mB3C92D0A6590D068623E54914CF3FB1BF8A02C96,
	ToggleBase_set_changeGraphicColor_m7BFDA25B9311E25CC4BFF31978265D005713A3F3,
	ToggleBase_get_graphicOnColor_m834D801726E792B63D6460D964F7295691E59DE8,
	ToggleBase_set_graphicOnColor_mC9E281BE32C39AD3987D5CB6D94157A3E7305710,
	ToggleBase_get_graphicOffColor_m3281E3AF881431E90FE92BC489722F78C02C56C1,
	ToggleBase_set_graphicOffColor_m8CCE1B37893E55014B2C7E1D384137EEEEBFB5F4,
	ToggleBase_get_graphicDisabledColor_m18BFE6E833048C4754ACF84FB12678F373275192,
	ToggleBase_set_graphicDisabledColor_m07DE84E9D39039EB182C6C08CC1CA3A692D7845F,
	ToggleBase_get_changeRippleColor_mEC4C9252D3280F902A1C735D9D81BD6BCA71C0CE,
	ToggleBase_set_changeRippleColor_mAFA22732E1C857C2ADF50BDB97740A2059BF58DD,
	ToggleBase_get_rippleOnColor_m6491A995C8C1CB82E12ED9AD2ADF346D3CDDABEA,
	ToggleBase_set_rippleOnColor_mB552804F6898FEF025527A5A8632876134C2E141,
	ToggleBase_get_rippleOffColor_mE5C4FCA403C06CC3FE219438A0B9CA1B52736FCD,
	ToggleBase_set_rippleOffColor_m748BB117385BD2DEA1819CDE927C86287B965610,
	ToggleBase_get_graphic_m0BDDE8BF8805C0B7D8548195ABAAC348733A2C4F,
	ToggleBase_set_graphic_mE2B99F9DC01F1D84816A51172126AB870AFC2A7B,
	ToggleBase_get_toggleGraphic_mA30488E5C407B18399EEE0C1EFBD5C52983F6215,
	ToggleBase_set_toggleGraphic_m433B8D68EC9E122E1B49794FE921AC09CDC6FD02,
	ToggleBase_get_toggleOnLabel_mEFF5D31F62241500C633FC2E6772FF09729F6941,
	ToggleBase_set_toggleOnLabel_m01AB942302CD3DC1B016DE9A54C096E7A155B0B2,
	ToggleBase_get_toggleOffLabel_m7366C043A301CC419F9E3654978489439FE00427,
	ToggleBase_set_toggleOffLabel_m02BC510254A81A4B8B65FADDFDF537F22228B4CF,
	ToggleBase_get_toggleOnIcon_mFB84DC18B4DFAD741E259805EA0644FBFE500E65,
	ToggleBase_set_toggleOnIcon_mEF42848DF7E26A7B3F9A5D2B291B8A817BBEB4D5,
	ToggleBase_get_toggleOffIcon_mE39A65731D0AFBAB1297B33441EF51E85CACAD59,
	ToggleBase_set_toggleOffIcon_m43415A44544FCB5A2EADD01CDD9FA1FEAD2806CD,
	ToggleBase_get_materialRipple_mA3734736D861D92DAE7DF904CB83301D776EFC54,
	ToggleBase_set_materialRipple_m50F7543C273835B976EE156414AD910BFDA66C00,
	ToggleBase_get_toggle_mCFAD1FC2AD44632B1BB933C583F6D237B5D3FA15,
	ToggleBase_set_toggle_m56F61160A4B917232B67304B825F189197CB267B,
	ToggleBase_get_labelText_m0DB76AAA9C089898686FB77463FFD690E7C5F118,
	ToggleBase_set_labelText_m8884F192999C13039692D17F226644D61757287B,
	ToggleBase_get_icon_mF6E2C235BEB4DBEB51B2B8A4548BFA89FB4D6281,
	ToggleBase_set_icon_m76F7475A49EF54CC9501D5787211911B390AF658,
	ToggleBase_get_canvasGroup_mAD9DDF303117794F3ADF68A47A0CCBF8F5F19C94,
	ToggleBase_get_interactable_m4C0DB5013AEF451D193975DABBE693C5E5688CE1,
	ToggleBase_set_interactable_m8428723738209E6D83F54561ED484F9C15185B7A,
	ToggleBase_OnEnable_m24BAD161D03C79032CB2E46D98A3B1C36D8A8717,
	ToggleBase_Start_mE3CC2C0B5DD7DA8567646D78F780A0D386DBD743,
	ToggleBase_Toggle_mEAFC13965C59FEBA755AC1E1C67FCB855D9F3036,
	ToggleBase_UpdateGraphicToggleState_m2C912F8565C59069009C20711393D05AB2F943CA,
	ToggleBase_UpdateIconDataType_m60C7CB13A54849F7BF1C0D1B7969E2A3164BB85A,
	ToggleBase_TurnOn_mDC885CE7900A31F6EB41A25BF9D3DAED112177B3,
	ToggleBase_TurnOnInstant_m2A39216359268053C120D61A426199C99EAFA41E,
	ToggleBase_TurnOff_mC85B842D92B85E23C6E595CAB09B1FC7F37DE189,
	ToggleBase_TurnOffInstant_m497FF3FCD2EA389C47A30736E9595F7E1BDBC7A3,
	ToggleBase_Enable_mD50857192A855CCD6276B01A82917D93A1657DDF,
	ToggleBase_Disable_m8FCB21DE87D4DA0CED110C7C257745392E2E28FB,
	ToggleBase_Update_m73ED0D5F00911E9505DBB4F33C29CCC947A5DFE4,
	ToggleBase_AnimOn_m0D90C84AFF8B54A86567634F0C5867724C3B5EBD,
	ToggleBase_AnimOnComplete_mE81E3D1B180176E580E48E47CEC18A0AD9FA7DAC,
	ToggleBase_AnimOff_m2E0D15E232AE28A8F8C0700235A42A6827F9011D,
	ToggleBase_AnimOffComplete_m1731EAF49C177F2276A784BAC34B56062499D37D,
	ToggleBase_SetOnColor_m6D57AA9ADABF4C33CF12858C77240EE01848BC1B,
	ToggleBase_SetOffColor_mD0E2EC03A8B5B4D78B6187D5C9271ED4FCD3C61F,
	ToggleBase__ctor_m227C61686A955FCE2FF6C3DB249122C80222CF93,
	VerticalScrollLayoutElement_get_maxHeight_m85AAA0D3800636C76BA0CD00BF3C630877A9848F,
	VerticalScrollLayoutElement_set_maxHeight_m31633131A56AF1791856932078990BB3E28D60F2,
	VerticalScrollLayoutElement_get_contentRectTransform_m87FC0BB1D5CF521C84051644CF645135CB69DFE9,
	VerticalScrollLayoutElement_set_contentRectTransform_m7F2F1A2B89C3721271065C4FA90FB014ECEF6C0A,
	VerticalScrollLayoutElement_get_scrollRect_m07F2DC33CF4C2787CB9043A6453E886B666634D6,
	VerticalScrollLayoutElement_set_scrollRect_mFE8A53C20B391DA9BE13DC2DBB900A392CAA9210,
	VerticalScrollLayoutElement_get_scrollRectTransform_mB0872616662FA697921EFC740EB82BEC58CA2AFD,
	VerticalScrollLayoutElement_set_scrollRectTransform_m17B17ED0782D12B6C4ABDCF195137D9D553A60A7,
	VerticalScrollLayoutElement_get_scrollHandleImage_m06CBF64CCDB063728433AD8D249725A9E431D33A,
	VerticalScrollLayoutElement_set_scrollHandleImage_mDF0CD019A50ED849FA02FB6D7EDDE9EAB29CC5C4,
	VerticalScrollLayoutElement_get_movementTypeWhenScrollable_mC4B0B83676D7A10B3A64BDB31FEF4DCC2CB98BEA,
	VerticalScrollLayoutElement_set_movementTypeWhenScrollable_m0D9EB06AA24F84853377C2E9669ECF32630F9427,
	VerticalScrollLayoutElement_get_scrollEnabled_m343BF753498009B8E75ECFBEE78E7E3B507F1EAB,
	VerticalScrollLayoutElement_RefreshLayout_m281482B846463D17DA28DC3FDDA30C1657CE870B,
	VerticalScrollLayoutElement_CalculateLayoutInputHorizontal_m0BAD423F3ECC29DAC59577F847CB37C0062FB351,
	VerticalScrollLayoutElement_CalculateLayoutInputVertical_mCE243AD84307C5BBB786514250E6366D0E42677F,
	VerticalScrollLayoutElement_get_minWidth_m7AA28AE2BC17388A7CA17557A6D354380F22E32D,
	VerticalScrollLayoutElement_get_preferredWidth_mE26636931D4D0D9DB888A30AEEBC4183274127C8,
	VerticalScrollLayoutElement_get_flexibleWidth_m89143B6FAD6A74B0A3FEB9672D09930501D6FBB3,
	VerticalScrollLayoutElement_get_minHeight_mDADD4AA4BBA27F00FF5A07C38EE146194AD57CD6,
	VerticalScrollLayoutElement_get_preferredHeight_mED91FB17E2F951F55C39D579FDC95C573F3D6E83,
	VerticalScrollLayoutElement_get_flexibleHeight_mA74D5E83707885981AA6AD80A4D0DDBA68F7CEA5,
	VerticalScrollLayoutElement_get_layoutPriority_mB5A3A6E75C64B0D6EB1745CC498A7995F93AD286,
	VerticalScrollLayoutElement__ctor_m3A09A8D49CDB8AC7F2FCB8FD5CDDCAAFB4580DC4,
	MaterialButton_get_rectTransform_m5E64A8CD44F3D12F8268DFEBFCA3EF944BA1A91F,
	MaterialButton_get_contentRectTransform_m35CA1E3008A7537CF66C0F46F513775A8637732F,
	MaterialButton_set_contentRectTransform_m6915C9C44D9A7FB032E286975C47E69BC28B07B3,
	MaterialButton_get_buttonObject_m4B9474F9F2DC4A70E678F1AAFDB23273E81BFCE4,
	MaterialButton_get_backgroundImage_mD217DA4D42C0142417E35E77865140107E2BB3A9,
	MaterialButton_set_backgroundImage_mEF7DDEE56383B8A56BBA304480D620BE3146CFFB,
	MaterialButton_get_text_m7F9F611FF7C59EE2A8016DA39C813A70032D96C3,
	MaterialButton_set_text_m7F83A74769D5AF439FC2776A2C916D97EA6EC64F,
	MaterialButton_get_icon_m3ECC5B065C3666693E3E51A4F45F8E781DD21872,
	MaterialButton_set_icon_mE2DBDA172D41EC108BE987E64584E7926F7EE74B,
	MaterialButton_get_materialRipple_m4DA6B5AC31A9A5A882C9F135D6B9BE49FF5DBFC8,
	MaterialButton_get_materialShadow_m29A77180D187967B0D5E18ECFD1211F4FD4AE608,
	MaterialButton_get_canvasGroup_mFE7CE83D4B3F76D985BC0FD778C440AF160B8DF4,
	MaterialButton_get_shadowsCanvasGroup_mD63594CB4BDC42A4E0DE9FF066AB2F6F9473E209,
	MaterialButton_set_shadowsCanvasGroup_m1490ABF5E67DFA0C9AC9A7641EE4D6662E265690,
	MaterialButton_get_interactable_m7414D7FB51A9794B1DE6E8580BAAC9844FE03101,
	MaterialButton_set_interactable_mDDD3461029EC1282C45F5783697F7663D4B7A21C,
	MaterialButton_get_contentPadding_mE50D5C6127E2D3652FE26EE787E88F7500FB9F8B,
	MaterialButton_set_contentPadding_m5CEED1A0A453F854FE781703C50AFBE9A12F48D1,
	MaterialButton_get_contentSize_m7C825DB236CD44549746BD4869F74268A2A0ACDF,
	MaterialButton_get_size_mB8FE03E6D7479567D6A994FD34EB04F9BA2D76DD,
	MaterialButton_get_fitWidthToContent_m8F7B336A62300190CF930A475DCF2C05A008BE01,
	MaterialButton_set_fitWidthToContent_m03CADBFEA5FAC388EA889E8BA6922F62BD621E09,
	MaterialButton_get_fitHeightToContent_m44982D032E209F2FAC873C845A03810D14252752,
	MaterialButton_set_fitHeightToContent_mA4908F3EF522A1407F45FA6B187EB33C9481F84C,
	MaterialButton_get_isCircularButton_mC5763BF4405350F78F7A41C3ACCAF42ED84A4419,
	MaterialButton_set_isCircularButton_m06C41DA812E119B2BA7D75B5FBAE9EC7A97A2AC9,
	MaterialButton_get_isRaisedButton_m6DBECFB46D70217E1B917A9B4BDA8BB249848F9F,
	MaterialButton_set_isRaisedButton_m83AE762A5150B3C8E5BB7A984BB84778B41C7BE4,
	MaterialButton_get_resetRippleOnDisable_m563CA504B49E9AC81B70D87295C9F177BC696BF4,
	MaterialButton_set_resetRippleOnDisable_m63F90BC081C3A9C89F2A587D9D4C2E738AF573F4,
	MaterialButton_get_textText_mF49745CCE42B1C4348C059EF1997E2C932A2D1A5,
	MaterialButton_set_textText_mE40AEC2C82AB0D243A74B527E5AEB672D3D92D46,
	MaterialButton_get_textColor_mF011CA6FD9883581AE7FD79894E9AD5236A2EE6C,
	MaterialButton_set_textColor_mD016C7A3DF135625BB4ECA652B7D42C6713980C3,
	MaterialButton_get_iconVectorImageData_m6E1A9F142AE3C2D32F66331869D5CA103D3DE68D,
	MaterialButton_set_iconVectorImageData_m5187CA1F1AC6DDC9FDE83474FBC1772644187DC1,
	MaterialButton_get_iconSprite_mC4E81C0E3C8ED5FCD70C2904718B9F214D1E77DC,
	MaterialButton_set_iconSprite_mE2E6EB088A8E169EABF7830E7C7F7973F70AE945,
	MaterialButton_get_iconColor_mA96BBEB9F53A721C7C703835CB159F8FBE173729,
	MaterialButton_set_iconColor_m1AEDFF73588462A057A1AA9D403506B1B2A2E1D0,
	MaterialButton_get_backgroundVectorImageData_m3604A5487D42FA564D9327819A1527929346C6A6,
	MaterialButton_set_backgroundVectorImageData_m8852EAA1E2488119B8D9D815C1CF3216DE0953A5,
	MaterialButton_get_backgroundSprite_m0A50E28402632ACAB44B46915BBE0573FEA4DEC0,
	MaterialButton_set_backgroundSprite_m5E5DA22F55B7BB5C200B06541D66406F66B7C2BD,
	MaterialButton_get_backgroundColor_mC387CC14DBAA864A07D043B942499BE2D2C4654A,
	MaterialButton_set_backgroundColor_mB7F48329F2AE0E6E65B402289AB91D66C24E0A9E,
	MaterialButton_OnEnable_mB8623931A17AE0A80CE8770DC936280D3584DCB8,
	MaterialButton_OnDisable_mA16AA783FCFDC092C36081B46C581D129AD33E48,
	MaterialButton_SetButtonBackgroundColor_mE24C57C77F1A83EDDC7F7C1528528F6DDCDB00D7,
	MaterialButton_RefreshRippleMatchColor_mABAF9E058E35CBE4E1CD92B87D01F72A3388F02E,
	MaterialButton_Convert_m0493B971DF444781C913A41FCF672BAB37FCE79B,
	MaterialButton_ClearTracker_m61F75ECCA4B1F6A2E227E662A6F6F5D1BB4CA4D0,
	MaterialButton_OnRectTransformDimensionsChange_m13F5BBEFFE46FB9FA4EF822C42D8BA84DB3696F1,
	MaterialButton_OnCanvasGroupChanged_m7E1EF22B8C7AAA291291D135518EA261C841A67B,
	MaterialButton_OnDidApplyAnimationProperties_mF6ED99F08903794876AA3C9D50AD6C9DA88A9E19,
	MaterialButton_SetLayoutDirty_mB96F9E9847F8F1921484EDB44868DD084DA54AE0,
	MaterialButton_SetLayoutHorizontal_m456880B41A1DD44BDDA1B98D781B1860BF63BFF2,
	MaterialButton_SetLayoutVertical_mF8B2AFDB3CB40C3C2B8E3B9512B1E76C19299F32,
	MaterialButton_CalculateLayoutInputHorizontal_m4BDF7344FD01DDA9BAEDFDCD65B4B276C0B80AD3,
	MaterialButton_CalculateLayoutInputVertical_mFE5ABA62D5BBBC8D378AF53208BE6865DEA923DA,
	MaterialButton_get_minWidth_mC55EF57BEEA5666A89A197E04B21BA66543D2521,
	MaterialButton_get_preferredWidth_m59F980B858551BC887C49C3DC4CABB37FC059460,
	MaterialButton_get_flexibleWidth_m90EDB76272422EA4E797A54F0A1283CDAE128EB6,
	MaterialButton_get_minHeight_m51B579BD880BCEF8BDB5ADC31E7428AFE076C813,
	MaterialButton_get_preferredHeight_mC496F027DB48C5D4AF9C824E9F2B8C610A4C5BC7,
	MaterialButton_get_flexibleHeight_mDEEB7EC2819F5E47D13B4DD83B5DFDB1341C786A,
	MaterialButton_get_layoutPriority_mCC325D37115AB44D44774501F94FAE2572022B6F,
	MaterialButton__ctor_m63CD75E9FB49528B89249AF469BA03332A233443,
	MaterialButton_U3CSetButtonBackgroundColorU3Eb__98_0_m2C26CD7C6C37BF6167FE043DDAA000C67771EC3A,
	MaterialMovableFab__ctor_mC5FB1EBF455DE44B8CDA6F2CFB92C3C6C42C9BDB,
	DialogAlert_get_titleSection_m7D231955F76C8F469F9DDA30E9452E49A29D02EF,
	DialogAlert_set_titleSection_m728A9786DC9F44BCECB96A7B09E902F68960EA26,
	DialogAlert_get_buttonSection_m14BFF6531A220CCA9E7652950820A915EDBFA56B,
	DialogAlert_set_buttonSection_m6F04489497FCBAA0D563F9B2F71888D5C3CD239A,
	DialogAlert_get_bodyText_m0316C16A662EB9854DE10C418B52529ED2D3096F,
	DialogAlert_Initialize_mFAF8ACADF44D6C7081E4462C83F1DD0320E2E244,
	DialogAlert_AffirmativeButtonClicked_mA560BF87DB543A8E8C3683F34DA7D0F7C656FCA9,
	DialogAlert_DismissiveButtonClicked_m97F78BA783253E258CEB40E51B92AEFB40E58D60,
	DialogAlert_Update_m914AC74B808092FE432641718873478732BFCF61,
	DialogAlert__ctor_m29A30F133139C89808D2F9E5209082D3A2AC529B,
	DialogAnimator_get_dialog_m45E17A840A43AF0E8E673A788B13AED4C4AF185D,
	DialogAnimator_set_dialog_mDEA43FA3AA0BB022F9192F92E54853337EE586B8,
	DialogAnimator_get_animationDuration_mB9965BEDE699588AE07F976E26C2C85F81064FEA,
	DialogAnimator_get_background_m25CD93B1D2D7B79AB012CF9F57EB087E3AB0893A,
	DialogAnimator_get_backgroundAlpha_m330435C6C898867973EDA49ACD9408769AFF1448,
	DialogAnimator_set_backgroundAlpha_m3FBF1CDC28F0C4DD711FE8BFBDF88598FA92FCDA,
	DialogAnimator__ctor_m6B04E6047DCB4EA9542EF762C7FDFEE513728F4E,
	DialogAnimator_AnimateShow_mCEACF2523F179CF65C6002136F641F0F01E07B44,
	DialogAnimator_AnimateShowBackground_mE9E52B439CA64BB48633EA074E06450BB932DB10,
	DialogAnimator_AnimateHide_mFA4D71C00B9304D259A421FF42B722214BEE18D4,
	DialogAnimator_AnimateHideBackground_m257EAC9532F3EC592C22296064A950732E826436,
	DialogAnimator_OnBackgroundClick_mC3F74EA2B9FCBEA302DF932FFD8E12CE6DC972CB,
	DialogAnimator_U3CAnimateShowBackgroundU3Eb__16_0_mF2275F818B762045D5E3635F84FFCEB814BA8C69,
	DialogAnimator_U3CAnimateHideBackgroundU3Eb__18_0_m07A3CBCD580B40538B114D882465FE2EDEED3FA3,
	DialogAnimator_U3CAnimateHideBackgroundU3Eb__18_1_m063ACA47A31367A824C990CE6CF005B0B8DE6853,
	DialogAnimatorSlide_get_slideInDirection_m8F7118770EF616E247588DC60BCA8F8FFF901055,
	DialogAnimatorSlide_set_slideInDirection_m9BC6CAC2ED974D85B127CE7AA1F9B8A2ACA6A045,
	DialogAnimatorSlide_get_slideOutDirection_m6906940D4A7C5C4745595E033CB75A9D0A049CEA,
	DialogAnimatorSlide_set_slideOutDirection_m411B179DB7BD5E511E13CA97859574DB02923C0B,
	DialogAnimatorSlide_get_slideInTweenType_m3DDAF3206848B3EA2F238A9A9850E95A76A6B39B,
	DialogAnimatorSlide_set_slideInTweenType_m463239879FB2A7B8FF9346C39D71042919944A8F,
	DialogAnimatorSlide_get_slideOutTweenType_mCA017E7856B594BFDD746904A6B5CE31D3EDD2B2,
	DialogAnimatorSlide_set_slideOutTweenType_m0BE7FF1CEE27DD6B28E78D2A7ED4DCC195B2097A,
	DialogAnimatorSlide__ctor_mDBDC97AC0C2860AAF295E6B5E913AEEA6614003B,
	DialogAnimatorSlide__ctor_m97655F332E3A9FCDF2EF88FE2B47CCCFCFDBCA2B,
	DialogAnimatorSlide__ctor_m26E3C2717BFC50D9B0CAED6270404D7C67E1919C,
	DialogAnimatorSlide_AnimateShow_mF7D6122860064E3267CA83794E555E20FDAED1EC,
	DialogAnimatorSlide_AnimateHide_mD704526374FFCCFE5E927D134D77FB62F39F9E72,
	DialogAnimatorSlide_GetSlidePosition_m74BB32127E3D849E5C753CD020AEFF829FE59673,
	DialogAnimatorSlide_U3CAnimateShowU3Eb__20_0_m98CD909703262E1112514D1A378027F50BFB4A2B,
	DialogAnimatorSlide_U3CAnimateHideU3Eb__21_0_mF80375750BA8EFB03D08DCB4A32366F0E6A057BD,
	DialogBackground_get_dialogAnimator_m4244F4415D43DB34C9C9A654D9548423DCB4099A,
	DialogBackground_set_dialogAnimator_mFD37FE722FDF53AAD46BD63003A58CA231A3E256,
	DialogBackground_OnPointerClick_m72CF24985ABC90870055DE77AF2EE3410B2E1D92,
	DialogBackground__ctor_m69DFC747DA3A83BA5F2560F1D31EAAE2C33833B3,
	DialogCheckboxList_get_titleSection_m98D602D9376D0DA5105A752D3349505811F60721,
	DialogCheckboxList_set_titleSection_mB1A90127E7CCC5A1759F9FF1F91FB0760154C414,
	DialogCheckboxList_get_buttonSection_m4509ADC7CAC230898A574AC4E95C30776B3201B6,
	DialogCheckboxList_set_buttonSection_m08D8B5240F21E30F61C58652C74B5840CD38D3E4,
	DialogCheckboxList_get_listScrollLayoutElement_m5F6233236EB4C302639128E61EE2B5F1ABA36E3D,
	DialogCheckboxList_set_listScrollLayoutElement_m8B141DFE2E6A7259C57B1AD324FF7595F89764BE,
	DialogCheckboxList_get_selectionItems_mD52215EB812723549F779CCFCE26BBA499789F8F,
	DialogCheckboxList_get_selectedIndexes_m16AEC9146297DB57968203E0485E5B3964620315,
	DialogCheckboxList_set_selectedIndexes_mCD274BC8A87E83650D2BDCF7A09CCDDC7336A58E,
	DialogCheckboxList_get_optionList_mEF90E5627B1D6CE026D6F0D001FC20EBCBA82866,
	DialogCheckboxList_set_optionList_mE57F3470035F593E4A9A1CCD810A17739083891F,
	DialogCheckboxList_get_onAffirmativeButtonClicked_m3691656DA4D3FDD5510749F953497F5DBBD7424D,
	DialogCheckboxList_set_onAffirmativeButtonClicked_mB9B915328FA4C1AE7989137110FD00E270E6435D,
	DialogCheckboxList_OnEnable_mF5B460E79315B1E869BB80749ACBD6BBA5B97401,
	DialogCheckboxList_Initialize_m841860D0A8E2EB2A5CF7A773A5F7ACA00813C909,
	DialogCheckboxList_CreateSelectionItem_mF54613617A857F55146458DB4A155E312B2AAD92,
	DialogCheckboxList_OnItemClick_m788EFE97BCD4FD2442DD9FF82D5E0DB56AAFB974,
	DialogCheckboxList_AffirmativeButtonClicked_m15DBA13F79A667A71943789EF72F05F756F416C7,
	DialogCheckboxList_DismissiveButtonClicked_m70A134B4644AF7D2BB5DF062BAD9C746730BE9C4,
	DialogCheckboxList_Update_mE12D4A267B8C4317AE58AB86174C046961120738,
	DialogCheckboxList__ctor_mD082DC0CF49A6BC44EBEDB02E1BA345A4D4C30F8,
	DialogCheckboxOption_get_itemText_mBD61156BB292C1826E1A90043133A1A47D0A3C75,
	DialogCheckboxOption_get_itemCheckbox_m484EEE168109E39ED78A4DCA967B49DF931D714A,
	DialogCheckboxOption_get_itemRipple_mB6DF6F2F90E6BC55D374857A7DDA015945EBA75A,
	DialogCheckboxOption_get_rectTransform_mE06808213C76A1A15930E54E17A9CAD455055013,
	DialogCheckboxOption__ctor_m60674C553C4C42F11D84B50D9842D950C563F831,
	DialogClickableOption_get_onClickAction_m38C86788AF1A10F81661DB5AEDFB81F78B51473A,
	DialogClickableOption_set_onClickAction_mEA0F51699EB1EB6DE1DEC54C6CB431CC568D2808,
	DialogClickableOption_get_index_m9790D73D068D99F4CA8C6FD5F8BB7AAAA0CF18BF,
	DialogClickableOption_set_index_m0729CB4FA073C8CB4C6823037E1A5858C2388001,
	DialogClickableOption_OnPointerClick_m9B82D7099656E98011095742C1DCDA9CECA6D0AF,
	DialogClickableOption_OnSubmit_mE84097DF6C2705631F8DE1467121F60B0DA4289D,
	DialogClickableOption__ctor_mE4F32E0D5DC35CB91116460C15DCFE43F74C2027,
	DialogProgress_get_titleSection_m0FD1888E87087C722469024DA1191D4D52FCC246,
	DialogProgress_set_titleSection_mF9C83AC73DEE27E003B2787B9DC640EF03973046,
	DialogProgress_get_bodyText_m037CADE3421C3155F91A4A7C7FA6DDD6F616DEBA,
	DialogProgress_get_progressIndicator_m5E54A901E77C10D7A13C67B47EADF84F07F35DF8,
	DialogProgress_set_progressIndicator_mDA69B13613198C9D307E2625347D1851B3B358FE,
	DialogProgress_Initialize_m274EA1A197F618F96F395B1D0F362A5B3F51B337,
	DialogProgress_SetupIndicator_m507F71F01E88D5666E9B9BB28AF65C0403EE2AC0,
	DialogProgress__ctor_m05ED2526C7D9B191F7E7696B84BBA0B30A9B0C93,
	DialogPrompt_get_titleSection_m9F8ED03981F5768FAA5778542421AFAF069CD716,
	DialogPrompt_set_titleSection_m30E362ACACF682CE8833ABBDBE3A91168F9C9FE5,
	DialogPrompt_get_buttonSection_mC58CEAF8DE2F9961250C2226D7ECF016F853666D,
	DialogPrompt_set_buttonSection_mF0BF419DD3BE6AAE1ABC634DF6C7CF1B00A0E804,
	DialogPrompt_get_firstInputField_m940B8A09D06F1C65200774BD453869CCEA6BE1A4,
	DialogPrompt_get_secondInputField_mF71172567149BCFFE304AADD3C5C8DF206E73252,
	DialogPrompt_get_onAffirmativeOneButtonClicked_mD3230CA4F73A3D08B5F8F310393E987CDD750694,
	DialogPrompt_set_onAffirmativeOneButtonClicked_m8FA53FC9E85D9410D87D0240C66DC1DA6FDC31C1,
	DialogPrompt_get_onAffirmativeTwoButtonClicked_m9E861E8CF464D90AC018854363E4C67224208A97,
	DialogPrompt_set_onAffirmativeTwoButtonClicked_mE3F46EFA1D445E757A08A3C3CC0EF6AEE5735724,
	DialogPrompt_Initialize_m29DFAA2E62808A39A49444B5949670DDB25B84D1,
	DialogPrompt_Initialize_m549B17DDC3EDFFC4FC617B035B33D1D1ACCF2A07,
	DialogPrompt_CommonInitialize_mC64620683A217E6BE3FED6F3DC0B67F55FE20291,
	DialogPrompt_AffirmativeButtonClicked_mBB000A81CBFDAD4C0D51F07B381228BE40B77EC0,
	DialogPrompt_DismissiveButtonClicked_m795117BFC145A5DB72FD0B35B881563AC75958B5,
	DialogPrompt_UpdateAffirmativeButtonState_mD1EE2C94E7222CB079889D6E8C9EF83DB781F72C,
	DialogPrompt_Show_m180B4F5E7D5E90A18F7C5D73E4DBD9F4F3E0B079,
	DialogPrompt_Update_mF1E55DD52844F3531A91C426926AA48B4B194F15,
	DialogPrompt__ctor_m3EE3E85C31CD1F667D86D7DDC65A0CA085AA4108,
	DialogRadioList_get_titleSection_m9357ABC75B049AE7FCA99127B46CB7CD1DB2D28C,
	DialogRadioList_set_titleSection_m79925AAE613D66E555340B56FB39B00888FDB301,
	DialogRadioList_get_buttonSection_m3E0F3EDDFD4EF668A074FA9CB0F55A74707B6C27,
	DialogRadioList_set_buttonSection_m3530DDBF0F0B820CCF6EC58B88BD5D30FB38B4F0,
	DialogRadioList_get_listScrollLayoutElement_m8DB7379A78AC2642C7E4488C985838BA75E1D8DD,
	DialogRadioList_set_listScrollLayoutElement_m7AB0F703BF1518F06B820E3B8C1969D6B41156CA,
	DialogRadioList_get_toggleGroup_m6B15C3A74171A2FCAF24507CDFB2D95AF976B8F0,
	DialogRadioList_set_toggleGroup_m00363A1C46D474EFBA3698823D02E8CEF5C24285,
	DialogRadioList_get_selectionItems_mE9AC2085B6EA430DAE3A92A21F173BAE8A109177,
	DialogRadioList_get_selectedIndex_m41CD79EAEF1EC9FFB76D3A0F2C7F95F54295D071,
	DialogRadioList_get_optionList_m0792BEA617DC9668BC1EA111CDA0462F0ACC32CC,
	DialogRadioList_set_optionList_mDCEA9EF009365FDF4FF8B01BA0208389DC30F33B,
	DialogRadioList_get_onAffirmativeButtonClicked_mB8FEF10EC3DC46D721D5DF2DE4ACC1884FB444A0,
	DialogRadioList_set_onAffirmativeButtonClicked_mFEC2D1B7F7AD73CE7594FB379B1D10AD2DAC3FE0,
	DialogRadioList_OnEnable_mE8804C2430007520D25B1F5390E02E93FF6BED07,
	DialogRadioList_Initialize_m6858AF6155A56726EDE9D3E9CA845011633F6155,
	DialogRadioList_CreateSelectionItem_m486F75972AA33A952A0BA39982A73600797636FB,
	DialogRadioList_OnItemClick_m1C37DA9D16A9CC7D256A44B8A31ABECEDDB62CFD,
	DialogRadioList_AffirmativeButtonClicked_m7394A75C5606BDC59D1D4B53E8E464413F399A58,
	DialogRadioList_DismissiveButtonClicked_mBAB480BDCB7B631835C690D48039906B697B0EFA,
	DialogRadioList_Update_m0027643722FB3AFBE0743D6B1AE5A02F7DA32C9B,
	DialogRadioList__ctor_m3CB41482A9AED1AF09C554AFD8166E64D3D7F220,
	DialogRadioOption_get_itemText_mD4B9F9ADB78BD07DD5EE9BFFFA560CEC1E0660ED,
	DialogRadioOption_get_itemRadio_m486C5D530266FF80087B9DE9F6230A7914F307B5,
	DialogRadioOption_get_itemRipple_m15424F2228CB807FA43A66C02E20B6A1FB13CAF2,
	DialogRadioOption_get_rectTransform_mB0215F7D06A4B6C1A50E8E19067D136C8F0738C8,
	DialogRadioOption__ctor_mE3F035E2F7E21D0C49964EA891B3370F73602151,
	DialogTitleSection_get_text_m36FA01FCF5DA9330E1DE2F097AFCC04BFC7265FD,
	DialogTitleSection_get_sprite_m7448084AEF47C075BB4117835A16FDC85A2AEFC0,
	DialogTitleSection_get_vectorImageData_m3C4614171938BEBFAF81AD253882C6490D77190F,
	DialogTitleSection_SetTitle_m9C9CE192E5E58CA27785BDD176B1BA6C2294585A,
	DialogTitleSection__ctor_m89DE42B7B8B6501CDAD3EB1AE5CB8C457CC7CC73,
	DialogButtonSection_get_onAffirmativeButtonClicked_m88DEFB2684E897A5D2022E5979D8CAD82332A55A,
	DialogButtonSection_get_onDismissiveButtonClicked_m41C012E7EE0A182CA6B99BFAD57E8717795A37FC,
	DialogButtonSection_get_affirmativeButton_m7A81169691F69C85AFB9486A46BD43213553EB71,
	DialogButtonSection_get_dismissiveButton_m55370A5058BFAF9677CABAA953F0CFDF2FFCE48A,
	DialogButtonSection_SetButtons_mA7A64BB3474B745D2B11F9A9418F3CE1753F9084,
	DialogButtonSection_SetAffirmativeButton_mF7119CFDEB892795F64E6F295355E80F2ECFB24A,
	DialogButtonSection_SetDismissiveButton_mF7352F4377D4F6A76260617328E75D6CB397DDF5,
	DialogButtonSection_SetupButtonLayout_m4737547EE897E7765EC7D482F4A8AE724DFC6264,
	DialogButtonSection_OnAffirmativeButtonClicked_m81FC95F6380C833927C8190E7A29A820A8AB0CA2,
	DialogButtonSection_OnDismissiveButtonClicked_mBCDBA53589EB3D12B27A47F763183EB89771D40D,
	DialogButtonSection__ctor_mCE14BDD7757909803FD3467820BF7AA01244FDB9,
	DialogSimpleList_get_titleSection_mAA52D0255CC5AE5FB7A51949F6FF95778F14B9FE,
	DialogSimpleList_set_titleSection_m5875C39557678C8BE3A76DE083A9330B7D0E2490,
	DialogSimpleList_get_listScrollLayoutElement_mCA82FAC38AB410521CC7B343344A3C390B898948,
	DialogSimpleList_set_listScrollLayoutElement_mA1E78ED085C48F672E20B2C0F6653955AD95D956,
	DialogSimpleList_get_selectionItems_mBF6786F505C7C79EA4E63B501E002A824AB52FEF,
	DialogSimpleList_get_optionDataList_m4B045E7A6E6CE1A20E8B14EBDE226101E2F50D90,
	DialogSimpleList_set_optionDataList_mEDA4EEDA300F508990FAFA62B15C5875FAFDB696,
	DialogSimpleList_Start_m8A986BD351EF5A07861250FBFEF16E32E663C67C,
	DialogSimpleList_Initialize_m58D489BD007D42DACDCC89FB8806315EF14E85E6,
	DialogSimpleList_AddItem_m0CC9C28E486271130BD3C0105073A75F1222ECB8,
	DialogSimpleList_ClearItems_m4F61171EEA30221495D949524DDB0AB3B29FAB30,
	DialogSimpleList_CreateSelectionItem_m5D69575DEADEAB34F262EBD750E486C638DA07CE,
	DialogSimpleList_OnItemClick_m80B1049DF30417E47C32C534465632CDF9411882,
	DialogSimpleList_Update_m03BAD789561603CB484508D7CC5C697880F7A5CF,
	DialogSimpleList__ctor_m7F19644CDFD26EB8FCA27B038CB8CB77F4C44BBB,
	DialogSimpleOption_get_itemText_m8B949036062EDD7803792E3C2A8EC821493202D2,
	DialogSimpleOption_set_itemText_mFB558A7F998D3E3BCB31D6E08BAD8E0D156A9AA6,
	DialogSimpleOption_get_itemIcon_m01B961985AF071155549ED0A0E0666085B3DAC3A,
	DialogSimpleOption_set_itemIcon_m2248F4A9F9263AC94FBA14D91B2FFADEEBC210C6,
	DialogSimpleOption_get_itemRipple_m544D67CEDC28A32CC7183B32362E2C7D71AD25E3,
	DialogSimpleOption_set_itemRipple_m928BFD571FA7DE1458D3FD03F0923E3F2DDED139,
	DialogSimpleOption_get_rectTransform_m48876E3ECBB144F2BA172627F84A1D69B0BAE392,
	DialogSimpleOption__ctor_m524D45B309DB1138EBE6779185E25F09E1359301,
	MaterialDialog_get_rectTransform_m0F28B786CF86FCCE6B1E316D8E875EC63F1A32CA,
	MaterialDialog_get_canvasGroup_mAB57D6E94F34004A019C9251D778539FCF77A78B,
	MaterialDialog_get_dialogAnimator_m3B05D16E6E106E3BD1044F497571A78BBD168F3B,
	MaterialDialog_set_dialogAnimator_m405908524FF4C5963692AE42FACD93C002603A9A,
	MaterialDialog_get_backgroundCanvasGroup_mF9552A1E6E9E5E28D4773DE097BA0D6882F39F83,
	MaterialDialog_get_isModal_mA1B86CEB40800CCF7B5A263474EBF127747FE5AF,
	MaterialDialog_set_isModal_mEA462642107A238AC9208B5610B5ABA4E98BA03D,
	MaterialDialog_get_destroyOnHide_mCC81AFA19155EC499F17B7E852BE9CA976E5761B,
	MaterialDialog_set_destroyOnHide_mBD7DE5EF59FA6F42EFEB20AD6B6CBB814E102D1D,
	MaterialDialog_get_callbackShowAnimationOver_mF96219A25F3A70F6FDBCFA5032BB9C92978C83E8,
	MaterialDialog_set_callbackShowAnimationOver_m95FC47E532E9A258DBC9A20A1867198AFA40BD2D,
	MaterialDialog_get_callbackHideAnimationOver_mED92A160CB662D7066DD8810D3BCB7150C77C41A,
	MaterialDialog_set_callbackHideAnimationOver_m968B61DF155DC1D0F7D791836AF4DEE258013F55,
	MaterialDialog_Initialize_mBCBAC6E50D654896852DE7487CB00AED77A2068A,
	MaterialDialog_ShowModal_m3F6546BB2BE3E0D14A1FB6F24D8154208C388A3C,
	MaterialDialog_Show_mF4FA5A7BE4344B2CBD1A69D28C35855AED0391BD,
	MaterialDialog_Hide_m74AE9042B27A8449BD6B7679D9346844CE96ECCE,
	MaterialDialog_OnBackgroundClick_mF396ECCD1E6E02AEDB1C66E6F9445B8558FDF7F9,
	MaterialDialog__ctor_m1B3C6627F12CB4F4C23CA82179149E320D2CEB11,
	MaterialDialog_U3CHideU3Eb__31_0_m12951E35A5BAE6A4C88D2438E509E63EFD59015E,
	DialogDatePicker_get_textDate_m2D01A25A732C6A67D227708508D704674BE1D08B,
	DialogDatePicker_get_textMonth_mEEEA33353C11E7A5E5B97B2045C3A89530951D5A,
	DialogDatePicker_get_textYear_m4FAA71344833E12C5DBACEF1AD70FE5741208095,
	DialogDatePicker_get_header_m9E4329A50E3410FD579FA1B0DEDE415C3CBB9F8C,
	DialogDatePicker_set_header_mCC064E348855CD04FC03A44D3F623CDA17F420EE,
	DialogDatePicker_get_currentDate_m1CA210DF9071F4240AD19273748F3A5A0492AD47,
	DialogDatePicker_set_currentDate_m6028FE227248B3B3AF296C1C2DFB8391AACAFD4D,
	DialogDatePicker_get_dayOfWeek_mA4AEB47613A4F927D9047CDF99C6CCCA9EE06538,
	DialogDatePicker_get_cultureInfo_m48226C6FE55BD63CDB9F26097415B8413FECE293,
	DialogDatePicker_get_dateFormatPattern_mA03B1AD24194A82714696B13DF62007D1C526B91,
	DialogDatePicker_Awake_m1741AA54DF259248FB3C791539610D005D95D6C9,
	DialogDatePicker_Initialize_m3DCB5324DE649F8B0554F9D2AB96295C1F057DC4,
	DialogDatePicker_SetColor_m6E4DC3638ECB7DD52DA8F093D8600E2E98B16A4F,
	DialogDatePicker_SetDate_m4992D3538A128D5AE2A61D2F2AE5A9CBA9DB89A3,
	DialogDatePicker_SetYear_m59180395BB24CFDF556168AE3349F395FD691E57,
	DialogDatePicker_SetDayOfWeek_m0B37DB206965FEF58BDC82D5573A6D751A920AA3,
	DialogDatePicker_SetDateFormatPattern_m1304D9EEEEF2AEDEA65B55194B39BE62DE0793D2,
	DialogDatePicker_UpdateDateList_m0EBDA7198A01FB1F52AA0B5B4A2D93D8F59D4498,
	DialogDatePicker_UpdateDatesText_m2721ABA8EDECA8426FC019449DE2C3D16891A35C,
	DialogDatePicker_UpdateDaysText_mF0715BD082AE8E5E575D5CF99D4AA942DAA92D59,
	DialogDatePicker_OnDayItemValueChanged_m774EB71C5A8645F28162B9D22E59873670827EFE,
	DialogDatePicker_OnPreviousMonthClicked_m80129F31E65DE9395D8729DA1147ACC88742F299,
	DialogDatePicker_OnNextMonthClicked_m41D3D5760AF2C6F46AC27958630E791D904FC30A,
	DialogDatePicker_GetFormattedDate_m44221E9ABBB1EB0C41A771A7668252D596500F4B,
	DialogDatePicker_GetMonthDateList_mFC249F9FFB42569320F70E7C11CDEFA713833999,
	DialogDatePicker_OnYearClicked_mDC6F3093B46B50A4F4FB05C7EFC51A136FDDC0AF,
	DialogDatePicker_OnDateClicked_m68969A7AB03D2B3FF5FFD49850ABDF1227DFB958,
	DialogDatePicker_OnButtonOkClicked_m700722032A30BBFC3C9DA9879705972CC2CDC9BC,
	DialogDatePicker_OnButtonCancelClicked_mF32E5873972F195268052488DB8FA418617547C5,
	DialogDatePicker__ctor_m1AE855D50C7ADECD6FECD7FDD787BCDD011E5B51,
	DialogDatePicker_U3COnYearClickedU3Eb__49_0_m6D44BE70B670153208895532CFA189E1E0492297,
	DialogDatePicker_U3COnYearClickedU3Eb__49_1_m355006C55BFC069943A69B4ACB7A49E40B021C0E,
	DialogDatePicker_U3COnDateClickedU3Eb__50_0_mD00BABD99DA916D95A1775B6F6C6A98AF54C9E8D,
	DialogDatePicker_U3COnDateClickedU3Eb__50_1_m384CFD508F9C7C510926EA4DF39D08B0AF9B0F69,
	DialogDatePicker_U3COnDateClickedU3Eb__50_2_m4025AFEC6B55B252F137B9F9AC978B8904B83020,
	DialogDatePickerDayItem_get_text_mD776CD087FF36E45029955654AB1963ABC51901E,
	DialogDatePickerDayItem_get_toggle_m86E70902A2B3041EAFB1A5D30609943BCC0BC2CF,
	DialogDatePickerDayItem_get_selectedImage_mB973E4F3E0C8C13AE62C692BD99518D50D45F214,
	DialogDatePickerDayItem_get_dateTime_m48D9B1C0FC28A5C056988F972AD5DA5E647BE852,
	DialogDatePickerDayItem_Init_m61D4FB6743B70A8E42170D7C302D0207DAA329FA,
	DialogDatePickerDayItem_UpdateState_m8860063417D3FD10D4B7AE4679E906D8C131C264,
	DialogDatePickerDayItem_OnItemValueChange_m9FE3DB877E4E90BED1DA761B34B7AF2E123AC5AD,
	DialogDatePickerDayItem__ctor_m44B7709581D1D7C92D12EFAEB88A31C4C3FB6A26,
	DialogDatePickerYearItem_get_text_mE26D3A607F6679F2F735EB7908AF1232C83F8729,
	DialogDatePickerYearItem_get_toggle_m641FEA53B31FCCEE1D666718C750C1B4446481EA,
	DialogDatePickerYearItem_get_selectedImage_m8755E7ABDAF42B9A4370C219B0A0D357F7EA3EE0,
	DialogDatePickerYearItem_get_year_mB81B72955A03729BA70B4B5BA8E64613E6FDB0C2,
	DialogDatePickerYearItem_set_year_m0473BF894A74020A648EE8EF5DB34394EC0D5914,
	DialogDatePickerYearItem_UpdateState_mB74109FF9D2C57787E75FDA84AAFDAADD99223B8,
	DialogDatePickerYearItem_OnItemValueChange_m64FF73FFFA4AF2092A1F44EE62816E3F61D36FC4,
	DialogDatePickerYearItem__ctor_m2E67669A869C042226FC5BB5C19B5E03FD1B61BB,
	DialogDatePickerYearList_Awake_mFA11F31EE2C2D4E025CC92415ECB34906AC9C657,
	DialogDatePickerYearList_CreateYearItem_m3F2787A3A664E4AFE47484645A6D05FC39718611,
	DialogDatePickerYearList_SetColor_m3BC09A18F21A663C71B4E5F39710D67C7612C441,
	DialogDatePickerYearList_OnItemClick_mF7BBEC177672063288F206E00C343C84991E22CE,
	DialogDatePickerYearList_CenterToSelectedYear_m143C342DD710C2DA40609F9D7CEB26B64D154E5D,
	DialogDatePickerYearList__ctor_m322259CBB518DFEF91B3E4ADA0ACD61514C801D0,
	DialogTimePicker_get_clockNeedle_m5A6DF0DE6AD5A7C7A3D105581A83B72E607ED4D2,
	DialogTimePicker_get_clockTextArray_m3614CC2EF8B72B7D11EEEAE389150D4CE4EF943F,
	DialogTimePicker_get_textAM_mB57536E8587C4C143AE8B03F5AB57B9BC756BC49,
	DialogTimePicker_get_textPM_m201BD0E6AC7D7DB614BF292D6872E0B8251F83AA,
	DialogTimePicker_get_textHours_m99F830D4108BE6F64825D90BC49572F7CAC10CE6,
	DialogTimePicker_get_textMinutes_m4105629C7424CBB9C6A4D154C1CB46DCBAE30863,
	DialogTimePicker_get_currentHour_m4CBF661A9BDEEEECD2395A3BCA71B91A06252C0A,
	DialogTimePicker_set_currentHour_mDB4D0AFEB8CAAA12DD020E70BF1D8C44724D81E6,
	DialogTimePicker_get_currentMinute_m716D822EC1681AE1DF20234F51936B0629D97364,
	DialogTimePicker_set_currentMinute_m92392932EEBC66DF7E0D425340345EBDE2A0C9C0,
	DialogTimePicker_get_isAM_m75FC0D6E54A4827D256B3F4532D1ED721F18B97A,
	DialogTimePicker_set_isAM_mA6C4F96EC2884A343B0E0DA365BB34BE9F13F64A,
	DialogTimePicker_get_isHoursSelected_m10A76B39897705C0656230CBD0CED53407213388,
	DialogTimePicker_set_isHoursSelected_m52D3F6AE5F5C37356E5D7468E0FBC823494752F6,
	DialogTimePicker_get_onAffirmativeClicked_m3BE417644371422556B2CB3B9276FA9E4F39B1A3,
	DialogTimePicker_set_onAffirmativeClicked_m7B44B7F9EBF817F5F43D6EA13B5C453C2ED87DD5,
	DialogTimePicker_get_affirmativeButton_m961612D71DFC616195A55F2737993C200CAD4DB1,
	DialogTimePicker_set_affirmativeButton_mA2198F37F334712216007172BF7A70482AD8560C,
	DialogTimePicker_get_dismissiveButton_m6F0D5E158D59201F78F0CAC200E580FC3003A13E,
	DialogTimePicker_set_dismissiveButton_mE36692BDB20648A93FF6C61CA3F1DFDA4C23567E,
	DialogTimePicker_get_header_m72FF339C9E6E63E33B55FA85DF97BBF8DABE1F7F,
	DialogTimePicker_set_header_m29269B2F41F389E41BE09083A72ABAB2F5EB5476,
	DialogTimePicker_get_needleCenter_mB9A95C17A6DEF95782F454298C09952E1A384CCA,
	DialogTimePicker_set_needleCenter_m0DF7EAF276F0908DD02975D598038E0370AB1E50,
	DialogTimePicker_get_needleEnd_m42305735FE2C91E999A407FEE7BEDB67D7F3C5E5,
	DialogTimePicker_set_needleEnd_mFED735B88EEB20A36E4E1C9A2542D5E8CF5B9083,
	DialogTimePicker_Initialize_mBE1A3A5A3965EAF3CB885A15862FAD89F141D504,
	DialogTimePicker_Show_m9D2CDDE965038412B91E9A97CE41215582AD8BC3,
	DialogTimePicker_SetColor_m5A9A67CDC8A0735D7326F5B5A47E7FCD04DBD843,
	DialogTimePicker_Update_mC39CA2F62529EC8CF75EB9C31FEC44E67E0E8823,
	DialogTimePicker_GetAngleFromHour_m176FD43BD1F042EE5C1B88A8FFD3BCE4EF48CA65,
	DialogTimePicker_GetHourFromAngle_m55CAD730FC99C9AB7B425B68D52449C19DD6AE74,
	DialogTimePicker_GetAngleFromMinute_mEB8C423923D5DA1B3A1168104A72FC3D119E50E1,
	DialogTimePicker_GetMinuteFromAngle_m864D478970FECEFD5C9206BA07443E1564FFCA64,
	DialogTimePicker_SetAngle_m942DC9C092D0799C2131684E014EB556E6AEF345,
	DialogTimePicker_SetHours_mCEB5809D286FAD00C838CAFF1C572B9D8E3D1C2F,
	DialogTimePicker_SetMinutes_mCD15DD7735CF8C7BDBD08BBC2A1ECA8ED7CE2233,
	DialogTimePicker_SetTime_m0AED0A0E7056BA8DA8F78A779B475DC1FE0FE8E9,
	DialogTimePicker_UpdateNeedle_mBE1D8E909A7F93A2AE4B0B351D818B983284F77F,
	DialogTimePicker_UpdateNeedleAngle_m2B24D066C58CDE8DE3B53F6B36FAAB3B396D0073,
	DialogTimePicker_UpdateClockTextArray_m6EA4ACDAF2AF052561A6BC77DD40563E4AC186C6,
	DialogTimePicker_OnAMClicked_mFEE8AE86BCFB1043DCB1187A82D264F485B284AA,
	DialogTimePicker_OnPMClicked_mD34D45E1F3888D8CEEDBA938B9664DB05FE6E93B,
	DialogTimePicker_OnHoursClicked_m603987F5C6D940F200F45C1C39E716E6986D9284,
	DialogTimePicker_SelectHours_mF38B888720646362FF490F39C62F86E7E9B4506B,
	DialogTimePicker_OnMinutesClicked_m920CFB727BDBAC188013EDB6EE92AC6EEC48A525,
	DialogTimePicker_SelectMinutes_m8E1A158420539CFCF393ABC53540FBEA4E2089FA,
	DialogTimePicker_OnButtonOkClicked_m4F02B0EA767CBD64FE37E71A45628EEEC42D2C2C,
	DialogTimePicker__ctor_mBD350B2FB7D794D37E0CC7FD8B01E1BA8543FAD1,
	DialogTimePicker_U3CShowU3Eb__63_0_m25B07553B4C130B4BBB5021B92D02F526FC15921,
	DialogTimePickerClock_Start_m80EA5AB016A2132B685571F7DD72AE4631273FF4,
	DialogTimePickerClock_Init_mB947053CF8C325A401EFC39F33D56CE85D47F6E4,
	DialogTimePickerClock_OnDrag_mDEBB5CE153F3CDAE38DFCACC8C76BDA71B54CD82,
	DialogTimePickerClock_OnPointerClick_mE7DA03E39B2BF4EA6D66C44A14904E9E78CFB239,
	DialogTimePickerClock_handleData_mB351EE182699CC2795F661D9BCD5F56A9AE4CA45,
	DialogTimePickerClock__ctor_mB35FF991104936E43AD04E372CEE3F3D8AFBA5F4,
	MaterialAppBar_get_titleText_m3AEDBF46DAE3B654BECD7110B88C96116F0FC460,
	MaterialAppBar_set_titleText_mF3FA16D3A9A6C58827463FF8DBB0E0C2E76CD686,
	MaterialAppBar_get_panelGraphic_m1EDF82B28B1365FEE126E54FBECCC1DCCEE52D28,
	MaterialAppBar_set_panelGraphic_mDB7FD3742A2A44363626019908279E211B872C77,
	MaterialAppBar_get_shadow_mCB74E5E3D09497262D901D7661B09427DD9BF0AC,
	MaterialAppBar_set_shadow_m5D44687D2940E242324420B9A1D101DFADC30FDE,
	MaterialAppBar_get_buttons_m2132C36A312C9A960A2E81A722E3E24F9D36F49C,
	MaterialAppBar_set_buttons_mDD9AEBFDBD782FC5A8AAD85495B4B543110A913B,
	MaterialAppBar_get_animationDuration_mDF12690FF9B35F4335D039741CC7EE56BC62C9EF,
	MaterialAppBar_set_animationDuration_m308ADA38BFEFC2A18B4AF20E8C1FC382DCA34DDD,
	MaterialAppBar_get_shadowGraphic_m6E60B92333A00A4093FBD8F9F2DCAFD1A8102C7C,
	MaterialAppBar_SetPanelColor_mBB39A77E9E999BE03C1E0161821F8FE3EBD5677F,
	MaterialAppBar_SetTitleTextColor_m26E13225039763551D642CF8AF77306F37ABEF72,
	MaterialAppBar_SetButtonGraphicColor_m23D63D075607866E5ACB210E6BCCF7CC86E86A4D,
	MaterialAppBar_SetButtonsGraphicColors_mB106C54333F41CBBBA47005DECF51FDA3586E244,
	MaterialAppBar_TweenGraphicColor_m5CF427E3FBFB06285FAB17FC1F3EEB34B2961372,
	MaterialAppBar__ctor_m0D5AB530ACA2C8BE743148392DE7990BD40B4B08,
	MaterialAppBar_U3CSetPanelColorU3Eb__24_0_mA957D6BD4703CBEF4C9296CB3ED31421B49378A5,
	MaterialCheckbox_get_checkImage_m6C59140FBE9EF5A19A8AD08CFACA04055840602E,
	MaterialCheckbox_set_checkImage_m97BD6482E31357E49C598E9F09AAD1A1285FA100,
	MaterialCheckbox_get_frameImage_m80DB1A82DAFD8A013C39F2F8195D59695BB9D668,
	MaterialCheckbox_set_frameImage_mB5EAF2985702AABC945A7D0D0259A4FF0B4BD2E7,
	MaterialCheckbox_get_checkRectTransform_mE5C34D6E49B828F7BBA882CC58D4676E71908518,
	MaterialCheckbox_OnEnable_mE0FC45FCC13913CC46A66679729EAB8B8D134066,
	MaterialCheckbox_TurnOn_m30C008CEC9AB61EC54D15E9EFB4DB62354F997DC,
	MaterialCheckbox_TurnOnInstant_m26473D385FA03044B001C671131CCAC6249C0F8D,
	MaterialCheckbox_TurnOff_m651FD9B9E11A6740FC386B03E92CE67799F5B389,
	MaterialCheckbox_TurnOffInstant_m053F4446A14A9524D7F4E0C38AD2EF4693F149B4,
	MaterialCheckbox_Enable_mF8FEA23D4B91BB1575141D8E5DC51BF71FF78F40,
	MaterialCheckbox_Disable_m33C5DA458A48F1AC6238B6EC8B6C6C7E9399BF28,
	MaterialCheckbox_AnimOn_m6866E3FAA6F8A8193691414971E474518019497A,
	MaterialCheckbox_AnimOnComplete_m6DD3110A0E254224E867417DF0A1F41E6F092658,
	MaterialCheckbox_AnimOff_m784DAA17EB9A3B57C193AD926BC994FFF322A544,
	MaterialCheckbox_AnimOffComplete_m7E0A47509F4AFAAFCC9AF6F2CC8E695AB133960E,
	MaterialCheckbox__ctor_m2CF29A6D68E4DFCC71543437FBEDB5A6BD9F6720,
	MaterialDropdown_get_verticalPivotType_m17C662780078435F773516AEB8E085C6C8F9C02E,
	MaterialDropdown_set_verticalPivotType_mF3678DAF56BE849BBB0DC2193424D701BA867114,
	MaterialDropdown_get_horizontalPivotType_m8F6789A54EE273B7E5717E64286DAAA4433821A6,
	MaterialDropdown_set_horizontalPivotType_m51B2BEAFC02E02989214F733D8D7F34B51A97F4C,
	MaterialDropdown_get_expandStartType_m045FA5893701D4131786B4C5122549C220D064F3,
	MaterialDropdown_set_expandStartType_mA34428BA5D84F92B9D1DC9D52E6F429EE79B6475,
	MaterialDropdown_get_ignoreInputAfterShowTimer_m0C7A5B9A845DB3EC765DCE8C1A6AE35FE1D22157,
	MaterialDropdown_set_ignoreInputAfterShowTimer_m1838845EED46DB772DD6397CB3F7D7DCA1CFD4BD,
	MaterialDropdown_get_maxHeight_m3A49672E7EAE90F5A81781E3D07F1A9CE9CE7ADF,
	MaterialDropdown_set_maxHeight_m1753703B49A2C0F5169EEB79042F52B6AD5661E4,
	MaterialDropdown_get_capitalizeButtonText_m5E2329D46984067C90BECF71697DF86390BF8C51,
	MaterialDropdown_set_capitalizeButtonText_m7BFA1C7160BD9D3339BB4D843A5F446C605CEFD9,
	MaterialDropdown_get_highlightCurrentlySelected_m5CB56F8002D860C432D7EDA26F02F7CCE878943E,
	MaterialDropdown_set_highlightCurrentlySelected_mD095CDFBD951A0A3E2EFF562F8CCE586698CDA11,
	MaterialDropdown_get_updateHeader_m00669CB538C243327CEE7E8787D26F47E3EA3286,
	MaterialDropdown_set_updateHeader_mCD02153D9AC8FB3335236983D0AF8B899B75C0B5,
	MaterialDropdown_get_animationDuration_m2C624B71CF3D14BFB0BD2EA763D2A9903DE1B65E,
	MaterialDropdown_set_animationDuration_m717E0DA7219DF1F7D91E039D15B2DC540762F622,
	MaterialDropdown_get_minDistanceFromEdge_m49B6F75C01F15ECD0C0FC7032FA8EB7B0EC64D64,
	MaterialDropdown_set_minDistanceFromEdge_m17F5EDFF8E54F4EED57216A19529520CB6DE3A08,
	MaterialDropdown_get_panelColor_mE18F4C0E14045A8A6123DCDBC346DDDF6FAD717C,
	MaterialDropdown_set_panelColor_m32ED9280418487ECC2EFE832368FAF66B79A9824,
	MaterialDropdown_get_itemRippleData_m34FB18C0E092D8B24F985D13941C23941D2FA9C6,
	MaterialDropdown_set_itemRippleData_m14A2CE10B2C5771DC25A5E4EB44EE37E3F0D28BA,
	MaterialDropdown_get_itemTextColor_m3E212358B84399CB77CFD1A826B6F6F1FAE50541,
	MaterialDropdown_set_itemTextColor_m93C209EE2417233AB0ECB9C12A552B58E8CB74D5,
	MaterialDropdown_get_itemIconColor_mD004FAB58449ECFBF57680B7D1CA267A5D7816DF,
	MaterialDropdown_set_itemIconColor_m1C9B247B1A89ADCC89A5924F799811C14DE5ABC1,
	MaterialDropdown_get_baseTransform_mE3C48B59B1B6AFBC5884E79E1B36FEFED0635BD0,
	MaterialDropdown_set_baseTransform_mA2F250D8ED1418788B1D84DD965F638693A14F52,
	MaterialDropdown_get_baseSelectable_mDBD10CD59A0EBF0F5EE0CE5A51C5BDEA60381186,
	MaterialDropdown_set_baseSelectable_m3C305A879C9B07D59B0C154B8B5718FC5B889F86,
	MaterialDropdown_get_buttonTextContent_m8A21088B652FE50D04B43411D20AD79671861E7A,
	MaterialDropdown_set_buttonTextContent_mABA2983B63661A13131A4F36525172087EEBFDE6,
	MaterialDropdown_get_buttonImageContent_mA4F41CDA76F705C4BD9D5739F0FF8FF0FF7ACE43,
	MaterialDropdown_set_buttonImageContent_mE4429F440D93725963CDA9D0BA8210309458EC91,
	MaterialDropdown_get_currentlySelected_m93D85C4FF1471B23A48557B45A8F4452947DCDE7,
	MaterialDropdown_set_currentlySelected_m75BBD1FD7942A6B5CA943B27E1DFE8A9339E5E20,
	MaterialDropdown_get_optionDataList_m8E17EF465851F6E97E049DDCE026C3C9CADB4602,
	MaterialDropdown_set_optionDataList_m9FE200014B83DB38EF2769691BA3F9C5118E8DEE,
	MaterialDropdown_get_onItemSelected_m7A90E8CD07E72CEB5BF268373F40BEFB2791739C,
	MaterialDropdown_set_onItemSelected_m4BAA097E067E9235DD82067918BC2760C819390E,
	MaterialDropdown_get_listItems_mF5DADC9EC229C54E3225A236254DC8353E94013A,
	MaterialDropdown_set_listItems_m2F5120EA27E0F067F6337ACF48778F770F3FC064,
	MaterialDropdown_get_scaler_m3D65B4BDD18D5A64F7F2D993BEC0F8438E63B7D3,
	MaterialDropdown_AddData_m1682785E3D52952E1BFD750C1B58791F2AA07FA1,
	MaterialDropdown_AddData_mBBF08979E6D33F52358492794E6364EDA91BEA4B,
	MaterialDropdown_RemoveData_m13CC57B5CA6459BF56AB69C146E665F39A826038,
	MaterialDropdown_RemoveData_m57B85402A4B2EF4D799259AADE0F6DCDC6EA22B0,
	MaterialDropdown_ClearData_mCCF133EBAA73AB8C40E9DF231C87828CECE7E131,
	MaterialDropdown_Start_mBB08DBAE150163C0C8E447B2A5CF788B0547769E,
	MaterialDropdown_Show_mB2F741A1C1DD657F20A52C7D89218DF300379305,
	MaterialDropdown_UpdateDropdownPos_m7379B5B5B5B74125A9F82DBAB75E14B083EE5557,
	MaterialDropdown_Hide_m16E5770C70C6321C8739B710ADCD1161341B9D54,
	MaterialDropdown_Select_m609C6DE961D858BDAEEC96F1560F395E303EE39F,
	MaterialDropdown_CalculatedPosition_mF9ADDE56E870FC9E7BF71690419FC5829AAA4DCC,
	MaterialDropdown_CreateItem_mEAF6225465C0819152DEF279D27B85FB635CD2F8,
	MaterialDropdown_CalculateMaxItemWidth_m0802186074516CCE3E563DF79822034C6DD7DC5A,
	MaterialDropdown__ctor_m0CD30CEC255789A166F8C52BF8E8B6A0D8F49291,
	MaterialDropdown_U3CShowU3Eb__119_0_m34BFF04B0E6803966B9A114ED6E4C9C02C1BBD27,
	MaterialDropdown_U3CShowU3Eb__119_1_mA4BADFD9104BFD3AF69B8EE2C603B6CDBF054D25,
	MaterialDropdown_U3CShowU3Eb__119_2_m49CF4D091F5A80AAFA2427B3F0EF4AF249FF778F,
	MaterialDropdown_U3CShowU3Eb__119_3_m4E7C1046285B95AC996379BD8E9FD15CA997591C,
	MaterialDropdown_U3CHideU3Eb__121_0_mAFF538678E30C3BB864743002389A4FB2060A002,
	MaterialDropdown_U3CHideU3Eb__121_1_m022FE1498C1FAD14AFB01A5A99AA61DF46F12374,
	MaterialDropdown_U3CHideU3Eb__121_2_m1461A49AA9E379B2518368EF3053DB38BCA8592E,
	MaterialInputField_get_hintText_mD7D7EA1ACFFF7D966A22A16A0B38BB248FCBA9B5,
	MaterialInputField_set_hintText_m4527530061D3F7FE91311AE1B1BD5EAF60A2DA19,
	MaterialInputField_get_floatingHint_m2400EDB7EB5BE8214751C6B039AFC12972F709F9,
	MaterialInputField_set_floatingHint_m1AA672759F860F1BBD0DF68D4FBB9D3014A5C83C,
	MaterialInputField_get_hasValidation_m64A5E7609A4D4C5625F5A660737A8361E9205EDE,
	MaterialInputField_set_hasValidation_mC65E0F9BA60C5E8AF83FF8E786F5690CBF1E4D45,
	MaterialInputField_get_validateOnStart_m59B6D71C2D6EB1772048480DE286B33E6537AE35,
	MaterialInputField_set_validateOnStart_m435EFCA782A90FA60E70EDCE4398A49FDF7AD813,
	MaterialInputField_get_hasCharacterCounter_m9CF4AC8D40047B6CAA19AFD75DBFDB0037789C10,
	MaterialInputField_set_hasCharacterCounter_m3EBF832ABB5FB113648A5D9C0314997A8B80DBDB,
	MaterialInputField_get_matchInputFieldCharacterLimit_mE2141B2382186FE0511F3DA43FA616685D60E9F3,
	MaterialInputField_set_matchInputFieldCharacterLimit_m77187F6CF09612FD5BF86C7B5A19A182E73BF5DD,
	MaterialInputField_get_characterLimit_m219CCF8417F404E2E5B7C6738ED6A6B20191BF86,
	MaterialInputField_set_characterLimit_mA409CA83A88C366C5907EDA8EEE350492F73EDA6,
	MaterialInputField_get_floatingHintFontSize_m8FD079509604526853A5716C2824E31A58EDBA59,
	MaterialInputField_set_floatingHintFontSize_m86EE03AD42165FDAEDEDE8907052FAC8BE406D43,
	MaterialInputField_get_fitHeightToContent_m9A0A28A90325B3DFFD925D374854D5ADB07971A4,
	MaterialInputField_set_fitHeightToContent_mA6DCC74BE3FE7FCBECCEA7588961B2A9FC5D3000,
	MaterialInputField_get_manualPreferredWidth_m883F176AA63ED228ACCB71C6D503690BDE86036C,
	MaterialInputField_set_manualPreferredWidth_mCB37D14C33CC9ECC95FBECCF71F03FAF22058A94,
	MaterialInputField_get_manualPreferredHeight_m66A253FF070E44BAB68835E729DD3335BB88E5D4,
	MaterialInputField_set_manualPreferredHeight_m9CBDED4368FDEA0AE8D17BA797D2F439FB49706F,
	MaterialInputField_get_manualSize_mE667A10DD749615BEF468B9E6F9005BD001F4698,
	MaterialInputField_set_manualSize_m807077C824A0DA967305C36EEB8BD1F6E1D4438B,
	MaterialInputField_get_textValidator_m6BD3E4471F17A26FFE75113F16B814C9DDD0B42D,
	MaterialInputField_set_textValidator_m8B1B6255C664A50A8A96184B8624EDFEB93460FE,
	MaterialInputField_get_rectTransform_mFF2770EB5DCA5CAF84578B2C163CB36E6F43AA64,
	MaterialInputField_get_inputField_m52E7ABF11424893314DB7769D8120DA0B3E3585A,
	MaterialInputField_get_inputTextTransform_mA7BCF3F1D9881F6238E2C0924CDBF1BEAF90DB95,
	MaterialInputField_get_hintTextTransform_mD4D74FD71A309E32B66B83809EF49EFCE7734959,
	MaterialInputField_set_hintTextTransform_m03809CAAA1B2D552E335E06917341674433DA4CF,
	MaterialInputField_get_counterTextTransform_m40682FFA11C6A6A9C8BD1A0BB0D3FF54EADB4D8C,
	MaterialInputField_set_counterTextTransform_m338945E5F755AA6AD678B116319631C0DC6A141C,
	MaterialInputField_get_validationTextTransform_mAB056E230EC57A500D0A942754FBB4636E329C9E,
	MaterialInputField_set_validationTextTransform_m729E72343066CB655D03FD2169FAB406B6E6F1F7,
	MaterialInputField_get_lineTransform_mF9B15F82F6FF4C005D3A1CEBEC4D8B6FC87B4ACF,
	MaterialInputField_set_lineTransform_mD3FC092C1AF26B63DB5469793BAA93773B7F417C,
	MaterialInputField_get_activeLineTransform_m88B0AA748398C698808BD9D5593143A4270AE063,
	MaterialInputField_set_activeLineTransform_m0C1C751474D1C7C69087D09F5E2F3A55681BD77D,
	MaterialInputField_get_leftContentTransform_m569F184F03F307BA7B6953988369A1A2CE7157C6,
	MaterialInputField_set_leftContentTransform_m7ED8E259C09AC5956624EAF5E363D924B9B89FF7,
	MaterialInputField_get_rightContentTransform_m4866979CC010D5B3EB0A87DC5E953C84C6D9035F,
	MaterialInputField_set_rightContentTransform_mF28CB0BA33A227F2799148E8E304236726B7E3EC,
	MaterialInputField_get_inputText_m3E760959D1931D5414BEF07DB87EA0613D4AB2B0,
	MaterialInputField_get_hintTextObject_m932E3E61375913A0BEBAEE793CB5CA19AC2BD194,
	MaterialInputField_get_counterText_mB577F8956F35F0F954A2DEBC90AAADADF7CFA536,
	MaterialInputField_get_validationText_m5AD7D30B37D09D203A25CA4338061B70C255ED51,
	MaterialInputField_get_lineImage_m996EF593260F7ADA286DE3AE34A9BF3F8936C27A,
	MaterialInputField_get_activeLineCanvasGroup_m74A7CB1450BA3ED7F074CE96B7CE0C98C0C1070B,
	MaterialInputField_get_hintTextCanvasGroup_m8C258450B3F6525739A9E6699DD46CB458F882AC,
	MaterialInputField_get_validationCanvasGroup_m3870EEB2ACFEFFE3E1D5E7E33A58C930DE27F7B7,
	MaterialInputField_get_caretTransform_mD7487ADF05E103710ADAA0E20BEE705EAEE6BB21,
	MaterialInputField_get_leftContentActiveColor_m12174114C8484803F45165CFDD89B506338DADBF,
	MaterialInputField_set_leftContentActiveColor_mABC5E1EDA41B739972FC33A6099BF6B507AF4975,
	MaterialInputField_get_leftContentInactiveColor_mFCBFCAD4199D931EEAB4340F82BAF7D0E6F0E421,
	MaterialInputField_set_leftContentInactiveColor_mA5B21FE31F2C96F37078BB5A42E6DC7F33AD7BF3,
	MaterialInputField_get_rightContentActiveColor_mCA71FF17A3A05B347A4BEC68BBCFEF1EFD36D5F9,
	MaterialInputField_set_rightContentActiveColor_mB878310FB5AD5B3096EE68335571319C72422D8B,
	MaterialInputField_get_rightContentInactiveColor_m3D515021C8AA4A23309B98BB83865928C3597E60,
	MaterialInputField_set_rightContentInactiveColor_m42D29F849A612074EFC2C5A1CFEF85154264CD50,
	MaterialInputField_get_hintTextActiveColor_m5E847DDBD2038C524C1FB2D2FB71F91980E7C8F1,
	MaterialInputField_set_hintTextActiveColor_mDD99F51BA4B92A64AD8E180A4E5F8E5FC1FC0CCF,
	MaterialInputField_get_hintTextInactiveColor_mFF55B15989313996A456D562DDA6765028FE4033,
	MaterialInputField_set_hintTextInactiveColor_m313A417C675B8F259E09E69D23B9AB3D6946B13C,
	MaterialInputField_get_lineActiveColor_m9CDC890E838427D77882BFC499CAC6D245A91C42,
	MaterialInputField_set_lineActiveColor_m0D113FB1A9DC97E17B20BDB0386D977D6D2C9A95,
	MaterialInputField_get_lineInactiveColor_mA4FBB56B6C656648D9625A6B2E8F78467033628F,
	MaterialInputField_set_lineInactiveColor_mF4FF28BEBE39DA468B3B2D63A3EDC2AC20C91BC3,
	MaterialInputField_get_validationActiveColor_mB474DB7EBCB30340EB1BF67AA60278ADE800240C,
	MaterialInputField_set_validationActiveColor_mE4294F3BF61C769E1E777E9FE2E241D77E27FB20,
	MaterialInputField_get_validationInactiveColor_m3269957D63201E7F9CF1A6E194DE7DF129E9D504,
	MaterialInputField_set_validationInactiveColor_m49D94CCC7174675903944094A87B52D3F07876E9,
	MaterialInputField_get_counterActiveColor_m44CA05C05A1E897EF99A68F4DAB446B3A3CA1829,
	MaterialInputField_set_counterActiveColor_m5373B7EAF13C41AEFD9EBDF1412CE041663CF370,
	MaterialInputField_get_counterInactiveColor_mB02EA59BBE066C2ADC7E3B5CFF0DF3CFF7FF3D4E,
	MaterialInputField_set_counterInactiveColor_mD4FEB4D55843F2A784B761D8021E52EAA0C49E7F,
	MaterialInputField_get_leftContentGraphic_mBB46170B0F6694BE61A9E649BB4003171414AB3E,
	MaterialInputField_set_leftContentGraphic_mA266D66CF87190635D9D82EBF701C7FAAD4601D0,
	MaterialInputField_get_rightContentGraphic_m66318058F3C2927CC24D8251B718F2008FE100EF,
	MaterialInputField_set_rightContentGraphic_m5966630D98A996085EF2DD4627757231AA0C7AD7,
	MaterialInputField_get_hintTextFloatingValue_mFE5DCEF0713A8FAC3DE3648809EC078D7CECE744,
	MaterialInputField_set_hintTextFloatingValue_m8130177AE53019D637EAAD938331820702745D2A,
	MaterialInputField_get_interactable_mF5B3A86883E6CF791F19402ED5610C8A9AC7650F,
	MaterialInputField_set_interactable_m3C79F100EE85123B70289E7A806AA04F9C89A042,
	MaterialInputField_get_canvasGroup_mAB126D7F0F1DD4993E21BADC45B9A90D4FA15704,
	MaterialInputField_get_lineDisabledSprite_m7D581041627C9C50AC4A3EDD567D45A90E81D8CA,
	MaterialInputField_get_customTextValidator_m1A916B82045C0F1368C9EA776B6A03B1E85250A1,
	MaterialInputField_set_customTextValidator_m7A7F87F6D0DF84C266E39C1E77FB8B79F658C727,
	MaterialInputField_OnEnable_m4E84D2D65F1D5D79A03C2FA969343D1AFB24196F,
	MaterialInputField_OnDisable_m476483B7E8C551A92BA357332DC442410381F768,
	MaterialInputField_OnRectTransformDimensionsChange_m35E975BD56F8AC956B9CB8714B588FCB9F3457DC,
	MaterialInputField_OnDidApplyAnimationProperties_m89AB275957EA1EC57BD7D6EBED305CAA3C629EB6,
	MaterialInputField_Start_m4D85F5022350CCE1362C7CA02F07BFB5280FD125,
	MaterialInputField_Update_mAF79BA6B4BC77CF851F99186AC38EA3CECEE4E82,
	MaterialInputField_ClearText_m480F9813599E7C0691C737C473EADAAABD16B2D6,
	MaterialInputField_OnTextChanged_m64280BF78883F9E54E67FDA828556E50B3CD27A9,
	MaterialInputField_CheckHintText_m9E0E6305F4C7654B2F4B14A342128FD02FB6E709,
	MaterialInputField_ValidateText_m56C91C9BEA5ADCF2D4B6FBEF4559390CF03C65AB,
	MaterialInputField_UpdateCounter_m01821AC75EDB5F0303C498726CBA3E9FEF52B1E9,
	MaterialInputField_UpdateSelectionState_mA4CE8CBD3272BD732DC4A9DB570B339E1873F01A,
	MaterialInputField_IsSelected_mA13A07690790084C5168DFDA9B861E7F25CEB523,
	MaterialInputField_GetTextHeight_m98D7FB6D0C4F58001C32FCD0234CA5431D3BBED1,
	MaterialInputField_GetSmallHintTextHeight_mAC2456CFA6EBE44BFB8287AAC42A1EFE01DD5D0B,
	MaterialInputField_OnSelect_mE476E728A6BAEB2BA7D356B3B2EC6FB7E2622D72,
	MaterialInputField_OnDeselect_m122855A0633D3FAB15ACEBEF102024AED834E265,
	MaterialInputField_AnimateHintTextSelect_mAC999772BFACB376B9C57F6E22F82DD7B4A26CBB,
	MaterialInputField_AnimateHintTextDeselect_mB5D3E76A94408D4D9D437D8C1BEA528A1F612680,
	MaterialInputField_AnimateActiveLineSelect_m7E6457C1305CB92C44FE1D6D97F7525CC20396C7,
	MaterialInputField_AnimateActiveLineDeselect_mF19B713EF197D7E0FF17C3AFE65705288076776B,
	MaterialInputField_CancelHintTextTweeners_mCC1D9083D89D7F4170E30404152AEB81046BB3FC,
	MaterialInputField_CancelActivelineTweeners_m8FBAFC6490376387239E5994BC18B89058D7F818,
	MaterialInputField_SetHintLayoutToFloatingValue_m2F1E1E92C0D626881FFD113E52AF6DDB3712EDB5,
	MaterialInputField_SetLayoutDirty_mFC3C4A7E8E6369051CFDB698B00F5D3EC0EB6018,
	MaterialInputField_SetLayoutHorizontal_m92389656A1A4F6E79D22613A49B9588C386ADC81,
	MaterialInputField_SetLayoutVertical_mEEF045AB6724DD8753FBF85B008B7524E0D439D5,
	MaterialInputField_CalculateLayoutInputHorizontal_m16BE33FDB662E595EC21BD2CF24A3BB153D417AC,
	MaterialInputField_CalculateLayoutInputVertical_m4D18350A23BC4BB6F47FB81028CFA19DBA9A4224,
	MaterialInputField_get_minWidth_m336FE02CC4AC457F46D97AAAAE5C2A7B10DFE1A6,
	MaterialInputField_get_preferredWidth_m422D63AE6153BE882EF3072C115A723EDC7A41F7,
	MaterialInputField_get_flexibleWidth_mCE6967E565E76215361BCE8FD859FE6D83913313,
	MaterialInputField_get_minHeight_m926D908CCFA4D463CF22014687839C985BA2C629,
	MaterialInputField_get_preferredHeight_mF7E5052787083CEFA0013645B5A84F5BC55EC1C6,
	MaterialInputField_get_flexibleHeight_mF939B7BC79A2EB8F93783BD81F3D46DD6938F77C,
	MaterialInputField_get_layoutPriority_mB0E0895AFA04E20F5317F3DAC040A3960A678A78,
	MaterialInputField__ctor_mB5BECE341CD3AE2195E4A24DE429638B8868B6DD,
	MaterialInputField_U3CValidateTextU3Eb__227_0_mABD94A5F92EB90136E7F076D81A748F54B2757EF,
	MaterialInputField_U3CValidateTextU3Eb__227_1_m1984B492C9EBB8E981B7F0940D8AAD098CF0CD50,
	MaterialInputField_U3CValidateTextU3Eb__227_2_mB5D70A5446B70B15E959617967F5866C11C1967B,
	MaterialInputField_U3CUpdateSelectionStateU3Eb__229_0_m80FC1AE38219B4BBEF61DD4A4DEAE3507DEEBD14,
	MaterialInputField_U3CUpdateSelectionStateU3Eb__229_1_mA50F5DB22AC1DDED9C8497C1AD74BE7D978D1ED8,
	MaterialInputField_U3CUpdateSelectionStateU3Eb__229_2_m52BC0ABCC87E6DDD071A1BFD34997BE4656872C0,
	MaterialInputField_U3CUpdateSelectionStateU3Eb__229_3_mFDAB06986DA51EB4831279F61E910F887865C518,
	MaterialInputField_U3CUpdateSelectionStateU3Eb__229_4_m5FE28BABF1DA48DCA47FF40C33A995FE511CB3AE,
	MaterialInputField_U3CAnimateHintTextSelectU3Eb__235_0_mBEC514013FE557EA08452200748D54433EE3187B,
	MaterialInputField_U3CAnimateHintTextSelectU3Eb__235_1_m6C61624A06974A7299F23F071729E0865AE9E5F7,
	MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_0_mA091D32D25668370201D117F8644B6BEA692BF8E,
	MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_1_m8D2795BB2769E8C3D04315B17A0977FEB6D52E3F,
	MaterialInputField_U3CAnimateHintTextDeselectU3Eb__236_2_m3D312FA172553A173D527E88C5027ABC2C24B3C2,
	MaterialInputField_U3CAnimateActiveLineSelectU3Eb__237_0_mB2F174A25CA6A91986A1AE3AAE7B0F11B8D6B67E,
	MaterialInputField_U3CAnimateActiveLineSelectU3Eb__237_1_mD9E44D515BB02CEC06E886CD7AF68121B85E9AFF,
	MaterialInputField_U3CAnimateActiveLineDeselectU3Eb__238_0_m881F0E4AFEFDA146F89551D0A4791CFC9F9DF21E,
	MaterialNavDrawer_get_backgroundImage_m38665550C3518A408917E915265C0D763140BB14,
	MaterialNavDrawer_set_backgroundImage_m95C6BBF9BB5F9670A1CBA15DE1C97BE47253077C,
	MaterialNavDrawer_get_shadowImage_m9ECAA73E301103F77EF7ACBBC2200572B92AB764,
	MaterialNavDrawer_set_shadowImage_m2456A1CC531E35FEF2D42A8A3951CD0EE7170A97,
	MaterialNavDrawer_get_panelLayer_m85E1AEC378E3994933D638957EE4E74C2D352FF9,
	MaterialNavDrawer_set_panelLayer_mA23CD9EE358C7FF8FB414DC81D467E4374728F27,
	MaterialNavDrawer_get_darkenBackground_m2A104524718D9E53B2A9C3CB0E9CD5CA38E5E16A,
	MaterialNavDrawer_set_darkenBackground_mA17F02309DBC6827018B84C036118574FFFCEB5A,
	MaterialNavDrawer_get_tapBackgroundToClose_m5C449A0A64456B3A92000E9A71D0D94F1EDC71C0,
	MaterialNavDrawer_set_tapBackgroundToClose_mF7E224E193E0B9F3873E3D6F20EA0AB7E82F8246,
	MaterialNavDrawer_get_openOnStart_mC057CCE1A07979D02FA530BA6CFCD24CE88B3D69,
	MaterialNavDrawer_set_openOnStart_m8701AA5D45F8EEFD459F9A15F83E5098A12961A6,
	MaterialNavDrawer_get_animationDuration_mFD8169B1C644F2034CE556EDAF1062112B0BC2C6,
	MaterialNavDrawer_set_animationDuration_m26F98E6A5052B0FB0505015A832022983A010F10,
	MaterialNavDrawer_get_scaler_mDF698382F2E757BD53FCEFF1100D3F802B8A27A0,
	MaterialNavDrawer_Awake_mE0656B97EE702A735806EB4B4930754BE8EE60D9,
	MaterialNavDrawer_Start_m7486A0F452839FAED7000EDB79B0C0A814106368,
	MaterialNavDrawer_BackgroundTap_m499FD5EC77C2A0233C9E21274F34E2A87BA38431,
	MaterialNavDrawer_Open_m724056666BCBE2912BDC3C8164D6F3AC75B123E0,
	MaterialNavDrawer_Close_m4AA1D11297AED08DDDFACA479205BE305B569260,
	MaterialNavDrawer_RefreshBackgroundSize_m3AB383A34865B3461892E20020A00D648487E1CE,
	MaterialNavDrawer_Update_m911697701248B7CC2133E6CA51D80D6A32A11AB3,
	MaterialNavDrawer_OnBeginDrag_m7D4BB338B615A7F07903C5098D8F6B8904501DBC,
	MaterialNavDrawer_OnDrag_m252B94D6D840E89A669C7C7CD45EC293B1F04E63,
	MaterialNavDrawer_OnEndDrag_mA8CD2A7BCBBD0BAF2DF0169FFDB35D4009200194,
	MaterialNavDrawer__ctor_mDE03AB3B114CE7B6D8BE266867EF2E82AE8A8E14,
	MaterialSlider_get_hasPopup_mA7F51BECD9D8AAB3971852290FAA30CEF3BC0BAB,
	MaterialSlider_set_hasPopup_mC387FAD581D9F6FF2C921B8564C480A11939AA58,
	MaterialSlider_get_hasDots_m53F42F5F6675209B8CCEC140C34078B3815E00AC,
	MaterialSlider_set_hasDots_mC3BF4E6DEFDAF4822B484896BED309E3543D739E,
	MaterialSlider_get_animationDuration_m4B8BDD8CA96B35708E6FEE253678D5D4D0B5637F,
	MaterialSlider_set_animationDuration_m12C9060170F3D40C02DF27E67493400BDFFE6279,
	MaterialSlider_get_enabledColor_m669DBF3F7584825144DB05784322042DB79D6D2C,
	MaterialSlider_set_enabledColor_m3C65412BCB5B099AD24EA1E3AEF4D05072A14370,
	MaterialSlider_get_disabledColor_m6C5D604E21B52DED1902250B2382A6DE2965F634,
	MaterialSlider_set_disabledColor_m44235DDF301CF3CCF647C8B4726AD38A916FA400,
	MaterialSlider_get_backgroundColor_mB6767D2A7460A1750FD9BCD3CD3474F2D1E2420F,
	MaterialSlider_set_backgroundColor_m35E02D03F20260020186CED7EF2613B754D764CB,
	MaterialSlider_get_sliderHandleTransform_m92D7E0AC05FD33898D80F7CAE67E6F6940D2497D,
	MaterialSlider_set_sliderHandleTransform_mD5A4D60BBF55C848545301F4CA2FEE729667C7BA,
	MaterialSlider_get_handleGraphic_m16E7B2A94BD54C4348F1D596124CC8A56A3C7304,
	MaterialSlider_set_handleGraphic_m9E37FE4FCF088790F700BFE58576CFDEDCE668AD,
	MaterialSlider_get_handleGraphicTransform_m58D2AABF86B28017510735B83ABA88E61A62EBFB,
	MaterialSlider_get_popupTransform_m6AA070C4010E4D1BC98F52F58F2A401238B550C3,
	MaterialSlider_set_popupTransform_m51A0C986A069035F111E1EF254B3589F27073968,
	MaterialSlider_get_popupText_mB3C7034D78797780E47E47E53AFD289007AFC27F,
	MaterialSlider_set_popupText_mC0E5B65BADF2EA973EE2D4F250B96F7C8933146B,
	MaterialSlider_get_valueText_m074C4D0B6FAA867B841979F367B05C1FC38B4A98,
	MaterialSlider_set_valueText_mC343C3B9073E80A0FD8DED5D2B2515B3CA70D4EF,
	MaterialSlider_get_inputField_mD0F9D929D3C4284BAF8832A7A92D5003993722C7,
	MaterialSlider_set_inputField_m0A45DBE2999439ED1443E6425CC0FC9E07A7CE19,
	MaterialSlider_get_fillTransform_m052A286BE20710BB0B1A7F8E77E267B7CB9E6613,
	MaterialSlider_set_fillTransform_mFC1E5953A5BD4F715B6C2B16525CA446AD403203,
	MaterialSlider_get_backgroundGraphic_m4C421C21BD1B7B91382ED15FA9B75C0343DC6F88,
	MaterialSlider_set_backgroundGraphic_m8C55E3A028787F949E1BF66988F15C37998F4AD5,
	MaterialSlider_get_leftContentTransform_mC52E6714292AA904843F68F0DACB34779BE072AA,
	MaterialSlider_set_leftContentTransform_m76F338C3E030E0CA4911D7C8A6432511A8BC72DE,
	MaterialSlider_get_rightContentTransform_m9948E636EE00B40D630B8FCB1863E45CE6A3264A,
	MaterialSlider_set_rightContentTransform_m9AF62291FCA5B72D9368A91837BEF2F224FCEBD2,
	MaterialSlider_get_sliderContentTransform_mCCF21D2C3AF342648404F59B977F09CCC1F337A3,
	MaterialSlider_set_sliderContentTransform_mB0C36C4890C15BB0F358E2C654D0001BBB960A43,
	MaterialSlider_get_rectTransform_m1F38832F7D07453C3CA9C871F3D3CD9C89011739,
	MaterialSlider_set_rectTransform_m48CB1B25666F62C49506CA15B3B39F480BB70AAB,
	MaterialSlider_get_dotTemplateIcon_mD329531FB0AC2909775E384C3CAD1A0E617F10B2,
	MaterialSlider_set_dotTemplateIcon_mA58DC8962A53EE142A6D1D7D771B61176CB105E0,
	MaterialSlider_get_fillAreaTransform_mACAE7EB37D28FF2A5783F8221D2D4A5B1BDAD760,
	MaterialSlider_get_slider_m13B05D63FB530567CEBF8AE48D3A783602924C92,
	MaterialSlider_get_canvasGroup_m89EE82A8F9D8646EF690C001B09003D857CD6691,
	MaterialSlider_get_rootCanvas_m4EC0492BC4BD39E470C8AC492EF5D300B1A63757,
	MaterialSlider_get_isSelected_m65E969F28406BF8C1AD518CA0763663C78786A40,
	MaterialSlider_get_hasManualPreferredWidth_m3D339E57D87CCB606BCA7F3B1884E80F173CA972,
	MaterialSlider_set_hasManualPreferredWidth_m348B3BD1AC8D1AA6ADC4C38C7E81CEBFEB847575,
	MaterialSlider_get_manualPreferredWidth_m9C2E7F16F2D1F6AF9D43ED6D045996AE6BE87078,
	MaterialSlider_set_manualPreferredWidth_mCEED1678DE1CE36D027F0A2145DB33DAFBDCF912,
	MaterialSlider_get_interactable_mDCE8974AEE002C12330276DF42B47534215C84FC,
	MaterialSlider_set_interactable_mD59D3F8CCBB4B5A580B3FCBA8C4B83F69AA81566,
	MaterialSlider_get_lowLeftDisabledOpacity_mA3B9FC9E75201D7F52BEB9C244DDBE36BC2B298E,
	MaterialSlider_set_lowLeftDisabledOpacity_m0CD7602DCC41331FFC61D6597969E48137AFB527,
	MaterialSlider_get_lowRightDisabledOpacity_m4CC599B6599AEA75B76131C9B86FF02D1AB6CEBA,
	MaterialSlider_set_lowRightDisabledOpacity_m2EFC33592818278EC2981729F804D9888F281BD8,
	MaterialSlider_get_leftCanvasGroup_m7CBFB37ACDFD4E99EE58E93BF3263D29C7FF2CBE,
	MaterialSlider_get_rightCanvasGroup_mC1E090D8ACF023DE4BED7491F667F77CB0FA62B7,
	MaterialSlider_OnEnable_m269031EF9EC4BB1550FE8F59838EB80E5B086454,
	MaterialSlider_OnDisable_mF670192FCF48D4363BFF3ECCAB34D0F409FD855E,
	MaterialSlider_OnCanvasChanged_m50FC2C95D9715904977A9C5B906DCF2583A6553F,
	MaterialSlider_Start_m68452B2F8C2ECE88C5B9421062DB741A35991B5A,
	MaterialSlider_SetTracker_m7587498C16DF6ADCC21C1F018A3DFCE1806D2495,
	MaterialSlider_Update_mA81C6832CC704FACE48DCAE9B6EE4F45CB11E7A1,
	MaterialSlider_DestroyDots_m91CA45D346E106EE6198569B3325C242B55F9344,
	MaterialSlider_RebuildDots_mB5910D654C45D747085A39FA8B5930C91C96BACB,
	MaterialSlider_SliderValueRange_m32305C3760A68BB2C4839B5C7576380C5ABE4B04,
	MaterialSlider_CreateDot_m40E54B6B05B381FD1A267676A43BD816E0B6C9EA,
	MaterialSlider_AnimateOn_m3AC3BD7F6049C5BDB6948235771C10C2B0FA23A0,
	MaterialSlider_AnimateOff_mA8D2D5DEE7C63D6E27C24D3F299297494B22F31E,
	MaterialSlider_OnInputChange_m464F5F2848F61A6FE5B21A67D9172B059F892C4D,
	MaterialSlider_OnInputEnd_m8D1F15F6BE335AC75A007833324D18506B9D1B9F,
	MaterialSlider_OnSliderValueChanged_m792054C56964A5F5DD4892982132A401A73A96D8,
	MaterialSlider_OnSliderValueInit_m548D723EAC9B8EC1590DD8AF35FACD267E2A68AC,
	MaterialSlider_OnBeforeValidate_mA3E83AE03AD80C97A47CF4384FB9D634E0D28CAE,
	MaterialSlider_UpdateColors_mAA4F1117D3C81B4F30AF8F34E44784F376C90311,
	MaterialSlider_OnRectTransformDimensionsChange_mE1B5897FD6A770662EB1C70F1D4448BD814AC57C,
	MaterialSlider_OnTransformParentChanged_m0E13A3E1C3B0FACD87E3E02691EFEDACB7C8A30B,
	MaterialSlider_OnPointerDown_m1E7F694B846E1AD508C98195E39B14A3921324EC,
	MaterialSlider_OnPointerUp_mAB94DE44B788B9422BB08E7126AD3DCCDEA37E46,
	MaterialSlider_OnSelect_mEB0F3EB2D09BC69B53550114EBB494EB0DF05416,
	MaterialSlider_OnDeselect_mB64B010FBA4241A846CC78A77C3BBCB9297F7405,
	MaterialSlider_CalculateLayoutInputHorizontal_m29866A977CB5C3AAFD64CC1DBAA79658F3A2F894,
	MaterialSlider_SetLayoutHorizontal_m8946BE08F87963953EEA06DDD8B75E208E72FEEC,
	MaterialSlider_CalculateLayoutInputVertical_mB92BAFD80907B2D005277EB6E025D49E138AA38B,
	MaterialSlider_SetLayoutVertical_mAE2BC6B629E77BAB61AB203FE8C969D2EA2BE762,
	MaterialSlider_get_minWidth_m8D5B6AA07B070B4CC75271A6DE1BFC211E7C7750,
	MaterialSlider_get_preferredWidth_m4E173C9287DCD28706FDAD387C645A25E02A8071,
	MaterialSlider_get_flexibleWidth_m440B3260E6E626F697396862CBEEDDAA80495969,
	MaterialSlider_get_minHeight_m5C71D867A290910484EBDBD63C15DC23363C83D0,
	MaterialSlider_get_preferredHeight_m35060A443CE3B65D385CAB2174C3F09E17D8216F,
	MaterialSlider_get_flexibleHeight_m6D9D2D3B75E472A7A0A169FB17ED160523254F58,
	MaterialSlider_get_layoutPriority_m1DBE4660FD38DE652E29A1CF4C343100B4E1064A,
	MaterialSlider__ctor_m05943E880A4B15B8B5B534368D57EC340B6AB764,
	MaterialSlider_U3CAnimateOnU3Eb__148_0_m99D07FD861D03D6F835BF41B80CB2FE0C593F1D9,
	MaterialSlider_U3CAnimateOnU3Eb__148_1_m86A6967A8AA545A1718EEA6D227205B431F3FF4F,
	MaterialSlider_U3CAnimateOnU3Eb__148_2_mE9A95CDD647BCF7EE1978D9646137B8E7836017A,
	MaterialSlider_U3CAnimateOnU3Eb__148_3_m1E6A41BDAC8B9918450F9BFF6992BBB4961DE9FE,
	MaterialSlider_U3CAnimateOnU3Eb__148_4_mCBB5763602705B5482C6BBD91DF30AB4751A751D,
	MaterialSlider_U3CAnimateOnU3Eb__148_5_mACF568E4E7E07B954BEFB94E870A708912EEBE0C,
	MaterialSlider_U3CAnimateOnU3Eb__148_6_mCBE4E34BCCE2DEFA712A8429AD8003707B62D232,
	MaterialSlider_U3CAnimateOffU3Eb__149_0_mCE07447C9E435010A6CFB6C8710028D113500676,
	MaterialSlider_U3CAnimateOffU3Eb__149_1_m388C5B5AF6C0CE3954C2509E6079AF622BC3198C,
	MaterialSlider_U3CAnimateOffU3Eb__149_2_m259B1DEFE757E68AEB98AE1BDEA1B59B37C4C4D6,
	MaterialSlider_U3CAnimateOffU3Eb__149_3_m40501AD9D864448BD7B730BADF739FF195AA018E,
	MaterialSlider_U3COnSliderValueChangedU3Eb__152_0_mA6E2CCEBB5F59C07B52B12F7A7E2C20D7E1056F4,
	MaterialSlider_U3COnSliderValueChangedU3Eb__152_1_m872E05D29A049570BEE72B3503F9D37C0A7A340D,
	MaterialSwitch_get_backOnColor_m2BE9FCEDAABCBA3934F35F315865AFE86EE0D558,
	MaterialSwitch_set_backOnColor_m8774D715E17361901711A631C84C817708CCEFC6,
	MaterialSwitch_get_backOffColor_m8E695C0868DE2A5EADD92481AD7185094B292F51,
	MaterialSwitch_set_backOffColor_m61621341F2E64C8ECE0E41B94EB6681C1CA5A1B4,
	MaterialSwitch_get_backDisabledColor_mCA2980C93CAB38C8B13980C02C3AB0AB19C1F46D,
	MaterialSwitch_set_backDisabledColor_mDF281851AE6A51E8BA506395B9C81623B5C5CED6,
	MaterialSwitch_get_switchImage_m093967040952CF97470078B558F398A123798A38,
	MaterialSwitch_set_switchImage_m5B563DB692EDDF08C30C18D6BE9C973F7B6963EF,
	MaterialSwitch_get_backImage_mAC533D10EAF06D1BB0012019FAFB70F16A057E5E,
	MaterialSwitch_set_backImage_mE16FFE2AAB6C059F8548C5B800AAB80A88CC7859,
	MaterialSwitch_get_shadowImage_mC2538D015E2E37A637B3D9D28B7AA7CA0BB94C78,
	MaterialSwitch_set_shadowImage_m1C7A0652E1CE3B8790F5861AB901F3B125E1A989,
	MaterialSwitch_get_switchRectTransform_mA06F378F695F1B87C884E44F9C4E1C78907623E9,
	MaterialSwitch_TurnOn_m64AB5303944616080CE5CEADFC815EDA3A5A667E,
	MaterialSwitch_TurnOnInstant_m8817803E289FDBB2BB175AF0341699DBC443107A,
	MaterialSwitch_TurnOff_m42A8D0A523DDA15EAB3DFC16F4A03960A70B1847,
	MaterialSwitch_TurnOffInstant_mE7CFAB4399367BA094BC7507444C026227332E69,
	MaterialSwitch_Enable_m5C2DC314E70655FB69F004F66BCD0FC49F886BDE,
	MaterialSwitch_Disable_m09EC17F4EF419C7DBAF47C1C909A9F3BC2685EC7,
	MaterialSwitch_AnimOn_m553ECBAB5311AB3DA77BC9BAAC7DDE1481B81435,
	MaterialSwitch_AnimOnComplete_m874BCC11366A00F8AE0ACC42BBF74C9A6ABCEACC,
	MaterialSwitch_AnimOff_mE181274158A5915BBBB9C038D96519C81AA6E688,
	MaterialSwitch_AnimOffComplete_mCFCE3E1004AC8A1FE9F8C4574B87FDBA1D6AD298,
	MaterialSwitch__ctor_m522D6C7FEC1A047F08A275256C185D850B13A4A6,
	DropdownTrigger_get_dropdown_m0456496201237CE88EE97A8C8A4E3433AC8114F4,
	DropdownTrigger_set_dropdown_mF9FCC458902D8715FF564E0EFB3C35758248B347,
	DropdownTrigger_get_index_m40CBD4E15CBF4B65399E41F202F47A6EFE3F6E8E,
	DropdownTrigger_set_index_mD9FA457F745982FF1767545751E3FB9CCBA9C20A,
	DropdownTrigger_OnPointerClick_mAF41EAEA99B52E3D612DF1694718D40AC9FA3055,
	DropdownTrigger_OnSubmit_mBB1153E3F449C8A9CE11291E0958291DFDB56798,
	DropdownTrigger__ctor_mB70A92B5DB4459E4ADA5555D28578359B83DD90C,
	OptionDataList_get_imageType_mBAA5E1149AA101ED05D371AC9CDF7FD5C6CE55C8,
	OptionDataList_set_imageType_m95757887742DE03FF8C92BD321365FC2EB4159B8,
	OptionDataList_get_options_m6453FDB449402EED08C92E2967343F7FE2226602,
	OptionDataList_set_options_m82010DEBA8EEE55C6E2402F7C63B322A81789399,
	OptionDataList__ctor_m24D431B4381ED9495C7260B07BE35B411834E9D1,
	OptionData_get_text_m33F35E0912BE59359B07438B9B3773B71623DC42,
	OptionData_set_text_m3D2FA9356D95D8956DBA2415EBB1A9F806AA5952,
	OptionData_get_imageData_m127F2E0B8699F7F33DAA4E08799A4B723CBBA6EE,
	OptionData_set_imageData_mAFE0CD6C453D805F27772A93EA5D8400B23CDE1C,
	OptionData_get_onOptionSelected_m97C97275E208D17D45807499A44596188BD46BFC,
	OptionData_set_onOptionSelected_m0D06F1D7F129930D456714CD500C28B35090D77D,
	OptionData__ctor_m2CBEC14C14B913BCDE679FCCEB47A5DAA34FDD29,
	OptionData__ctor_m5B8450E2A63947CF4ED7F4BB64ECD5FF7E5F106A,
	OptionData__ctor_mB20CEA44658D2CA3F7CA1E345EE0A23BEB001474,
	OverscrollConfig_get_overscrollColor_mFF29DD6ADB11A88DEC1F590FEC7FE80B83244C82,
	OverscrollConfig_set_overscrollColor_m0C0365F8555350942633684A455E279A7D0A9E89,
	OverscrollConfig_get_overscrollScale_m439E08F52994C260228C5C810FB9AECC9DCB52EB,
	OverscrollConfig_set_overscrollScale_m2EE7F4AF6F068F6BA37BC01DA354D50244E9A12F,
	OverscrollConfig_Start_m236D411077F597AF014927B91814F26CC37A8D3E,
	OverscrollConfig_CreateOverscroll_mBB10C80D2B287435559F3244C6B61CA8C6AAEBA9,
	OverscrollConfig_Setup_mA74DCD72C76CC176F47C0134588CBBCC21FDB81F,
	OverscrollConfig_Update_mF68509542AD293CE29E543F4464C01FCC3C40787,
	OverscrollConfig_OnEdgeHit_m0322D48AE0207AA84D03C4FEDD3BD3125541AF13,
	OverscrollConfig_OnEdgeStray_m84CD26F97625A598467B79721994E6AE9E2F9397,
	OverscrollConfig_SetupPointer_m79BA59D1E933F167A88348E0F8948D871751A715,
	OverscrollConfig_GetDelayedSize_m6A28E8CECDF9F48ECD1C568547D3C52AC826FE8C,
	OverscrollConfig_OnInitializePotentialDrag_m4340F0A07BD7A9EEFF04187700B31D44F92089F2,
	OverscrollConfig__ctor_m0AA67AE743AF2998BDF16A75315F237E36402100,
	PanelExpander_get_expandMode_mDC726A9E97D6D5E9B3C92D381C1E94C57DC2F48C,
	PanelExpander_set_expandMode_m0982DDF78E2B77D4257EBD468567C72EA097145D,
	PanelExpander_get_panelRootRectTransform_m7221471AD31BF00A4240B35D1AD275E98AA29253,
	PanelExpander_set_panelRootRectTransform_mDD1824337AD40D80716CD62ED39CDFBE6D95A6E1,
	PanelExpander_get_panelContentCanvasGroup_m77F0B0F82F73977EB8D8D3CD2C081F1D209FC3A6,
	PanelExpander_set_panelContentCanvasGroup_mE27E96BD5EA809398223F831146F83A976F6366F,
	PanelExpander_get_panelShadowCanvasGroup_m9A387739D61CD01305D572A0AAEA4ECB8990A682,
	PanelExpander_set_panelShadowCanvasGroup_mBF5D134E1E7308DB4DC2B889DE71C75944371180,
	PanelExpander_get_baseRectTransform_mC09EF747EBD2CD6D502D2471D268B0B29C0005FD,
	PanelExpander_set_baseRectTransform_m3DCA95C40D48E2A96F4B6FA4142E060167D891E1,
	PanelExpander_get_autoExpandedSize_m1DF6914C640BDF93B6CB90B11D684C9AA3C33CC8,
	PanelExpander_set_autoExpandedSize_m36CA645A5F7BD96D9CA1A5973381BEF40F27E760,
	PanelExpander_get_expandedSize_mBAE2672351FAA4F9E5E2634AC69ACB200DCDAEF9,
	PanelExpander_set_expandedSize_mF267E5DB37331E3C7632BAEB078E756F3E6752C9,
	PanelExpander_get_autoExpandedPosition_m871AFA3D08FDD72132912630B91012FCCE0CDC77,
	PanelExpander_set_autoExpandedPosition_mEB0C420E495A3147EF71BADA240623C60377FACE,
	PanelExpander_get_expandedPosition_m0278F5D73C32396EC78B40E0802BAE2F848D10C8,
	PanelExpander_set_expandedPosition_m4FCEC9149C5A245549B887CD84C0D6960DE1813B,
	PanelExpander_get_animationDuration_mB5BBAD96A0DDBE5DF877641E3F4A9355AEEDFD73,
	PanelExpander_set_animationDuration_m57FBCF73C1671BF038345884B8C0A9C9394AFCB5,
	PanelExpander_get_rippleMask_mED903E9DFD3D2151144949791C2F8E411DC678F8,
	PanelExpander_get_rippleHasShadow_m5844A04DCC7A20B3F0789D05F04AA2665B4E99C3,
	PanelExpander_set_rippleHasShadow_m45E30C9F7AED30AA1AD0FE6AE90A6842DD77937C,
	PanelExpander_get_darkenBackground_m13F4902717A5B884F6ECC6ABFDF6C016E34E86F1,
	PanelExpander_set_darkenBackground_mF982E511F1A80D3D62843A49079524BDF198B373,
	PanelExpander_get_clickBackgroundToClose_m4DABC7CBE9AF2392D60653957FAD99AB8ADAB304,
	PanelExpander_set_clickBackgroundToClose_m2B315C0E834A92A92C68C47D56427FE07D4D40DD,
	PanelExpander_Show_m2A1063D81409755E103CD6F6026D9C7617501D30,
	PanelExpander_Hide_m927B364D5390E18F5EADB435BB875569BE9823CF,
	PanelExpander_OnBackgroundClick_mA8ADBB22DC258E91268024DDF47B37DD50BCEDF2,
	PanelExpander_CalculateExpandMode_mD7C4CAC82ADBAC7188B85F8543D91B19BFB5FB6E,
	PanelExpander__ctor_mA2581EDCBAF15B05AD3CA0B97CCC5426A1DD66B6,
	PanelExpander_U3CShowU3Eb__67_2_m2663F4E486CB95E945036544745367456541B672,
	PanelExpander_U3CShowU3Eb__67_0_m0F0CA86DA1EF5B24BC5EBEF6AF847227F926135C,
	PanelExpander_U3CShowU3Eb__67_1_m8E06C002B20EBBBB0ACE7E37DEFFB0EDFCA8D614,
	PanelExpander_U3CShowU3Eb__67_4_mB08545C6CB1E9977F18CA696FEAC52669BA6CA3F,
	PanelExpander_U3CHideU3Eb__68_0_m4E450A9DD4368DFD991E0FBBCCE6E4AF5DDBA924,
	PanelExpander_U3CHideU3Eb__68_1_m30A967DE325906F834986D04AF8BDFC664865E98,
	PanelExpander_U3CHideU3Eb__68_2_m2FCB98121B446BB8EE65DBD40C0E1DB33A45D2F6,
	PanelExpander_U3CHideU3Eb__68_3_mB759E1CBAF6AED5016ADA27674000FA398899795,
	PanelExpander_U3CHideU3Eb__68_8_mF059801F091D93DFC0E7B67CE00260EE53BDA12C,
	PanelExpander_U3CHideU3Eb__68_4_m39CC8AB117E1D81364E949275A75D68C515D9E93,
	PanelExpander_U3CHideU3Eb__68_5_m4633D83E5574D2E424E94C3CD567BB78AB84E71A,
	PanelExpander_U3CHideU3Eb__68_6_mA82040F740658E2A5DB8F0ED919DA65ED9CDBEBA,
	CircularProgressIndicator_get_circleRectTransform_mF2E7FF9F7E321305A6B7C92F9DFFDFFFFEAD846B,
	CircularProgressIndicator_set_circleRectTransform_m73EC2EF7BF8D4AF2A76CB2E11E411DC37E908423,
	CircularProgressIndicator_get_circleImage_m73C902760C2B02AE883B76F0261B1E82FF8C6427,
	CircularProgressIndicator_get_circleIcon_mEDC8973EC243098B98B8C8654ADBF1496B92ECBF,
	CircularProgressIndicator_Start_m48DB1888BBCF4D9CF85F1E0A276443328ED45CEB,
	CircularProgressIndicator_Update_m0B39BAA271F8CD4BB2B8A6AC3EA2611BC8747C67,
	CircularProgressIndicator_UpdateAnimCircle_m09A5F9A2A89BDB00900C540C8A02E8F0DF398C41,
	CircularProgressIndicator_UpdateAnimColor_m191C8F9128BDFDAE96CD4FE8B69571DB81BAB019,
	CircularProgressIndicator_UpdateAnimSize_mCD05E525D0205FF0F38F204A1FFBD1EB088B8148,
	CircularProgressIndicator_Show_m6A069D0BC724FDDD05070225CA9D0572050C34D2,
	CircularProgressIndicator_Hide_m4CA267708419474384DBA136367BB8CA670AAA35,
	CircularProgressIndicator_StartIndeterminate_mC54F1EE1CBB595BF32BE6BF29790336DA6329F80,
	CircularProgressIndicator_SetProgress_m8FB3A88C440FB8AC04093C743D6F44AB99BDE68D,
	CircularProgressIndicator_SetColor_m0B0ED5C0A16534A2A2C243AA4A0BE1C1BE5493B6,
	CircularProgressIndicator_SetAnimCurrents_m1BCC46235E5372A9013E49DBC34EB6C2F241B35C,
	CircularProgressIndicator_FlipCircle_m714A108C613F80F22E9D6E34FAC8149ECA1CB74C,
	CircularProgressIndicator_GetMinWidth_m85B88CC912262C66654FE4A3B879819B3B956B4E,
	CircularProgressIndicator_GetMinHeight_m34BD5FD6788FBEF7F94E0A16198532E3ED9D247A,
	CircularProgressIndicator__ctor_m442F974C6648D14A955B2B5AC90C8100AFAB4130,
	LinearProgressIndicator_get_barRectTransform_mB3F49AFC8B25DA2FC8E634B43624EF518CFDD5A9,
	LinearProgressIndicator_set_barRectTransform_mBC8D3167AD0B13C252CF5C674781DCF7AD4B1156,
	LinearProgressIndicator_get_barImage_m66CADF15494103552FD9F4C290C5219F1EA5B4F4,
	LinearProgressIndicator_get_backgroundImage_m5B64C28F588C41ACAB1F651188126F98A79E0D5F,
	LinearProgressIndicator_set_backgroundImage_m240FEA58428E73E6D430BAEE93008D2C4307154F,
	LinearProgressIndicator_Start_m06AE8EF60990F7A2B94544B539D792384063761B,
	LinearProgressIndicator_Update_m4E6CA3844A340FAB16D28076112357303075EE57,
	LinearProgressIndicator_UpdateAnimSize_m06FB22D76CB82ABD1928872ABD38958DC7BA531E,
	LinearProgressIndicator_UpdateAnimColor_m4E8B49C8A0917181DAB1D9ED2DB559E6AC819FF6,
	LinearProgressIndicator_UpdateAnimBar_mED200F4DFF8C3772B647FA479689D322B7F49BFF,
	LinearProgressIndicator_Show_mB9108D5F03C2C812143A4C3F69590676E60F6EE5,
	LinearProgressIndicator_Hide_m0039A20071553B4727DA61B67A7C61F4E7E9491C,
	LinearProgressIndicator_StartIndeterminate_m86819E0F4139704379907701A526B2292435AD72,
	LinearProgressIndicator_SetProgress_m3EF6DA971FDABAAB7BC96763A1B887F1FFFA29C6,
	LinearProgressIndicator_SetColor_mAA6261BFC399C3507FFF81342F7412AFE4723F21,
	LinearProgressIndicator_GetMinWidth_m62AD6DB674279713589D042FB60A14D7CA1F36EB,
	LinearProgressIndicator_GetMinHeight_mCADBB51C8A29D92C03FE6F9A4B88BD8D641C2389,
	LinearProgressIndicator__ctor_m7B32C087D0BF40BB6FF4E59B271B645092D52A36,
	ProgressIndicator_get_currentProgress_m2DD33B30CB5B4CFA49BD935D3ADC5DCD3E84D871,
	ProgressIndicator_get_baseObjectOverride_m3304DC64E4AA85ECC044F57B0B656E8FD706E79B,
	ProgressIndicator_set_baseObjectOverride_m22A3231FB59A12DBC5BE4C4C5338530EC7C178FD,
	ProgressIndicator_get_scaledRectTransform_mF28792495254C8444462D209FE045AD25A62DFCD,
	ProgressIndicator_get_rectTransform_m274E157F1465A5FCA34B4345C9AB9E3692FBF800,
	ProgressIndicator_Show_mC48A9F4779CD7AB36ABBF9FC51D082A44F5C5940,
	ProgressIndicator_Hide_m14A4B8C8F962F80AA2BCE6847828CB96CFABD114,
	ProgressIndicator_StartIndeterminate_mA7AC785F3643FF5A0FA6BCAEFA67AE34C424C955,
	ProgressIndicator_SetProgress_mE3D66EC4E8D95697F228EBD02DD41A1D6F16B253,
	ProgressIndicator_SetColor_m1E2300571EFAEB7B5C8AB6D9775F1BDB4DFCC634,
	ProgressIndicator_GetMinWidth_m2D9833E5AC92B68CC44A7EA5410E000F5F0CB4A6,
	ProgressIndicator_GetMinHeight_m7608CD61CED66E64942806A1A7DA2ADC850DFCC4,
	ProgressIndicator_CalculateLayoutInputHorizontal_m99EC4DA7D45AC1C0ABBF60AEF6DF9D310FBA3615,
	ProgressIndicator_CalculateLayoutInputVertical_m30078A475F10CFFC3B3F8406BFA83FBF279FBEE1,
	ProgressIndicator_get_preferredWidth_m6F583EDCA8ADBC49D113AE66DCE3F8C07542A7C9,
	ProgressIndicator_get_minWidth_m6D97455DBF85A8DD85E258899D5B9C9ECBCACA32,
	ProgressIndicator_get_flexibleWidth_m3AEE2FC255EA9BCE704A9FABDCE63F7E96279A2C,
	ProgressIndicator_get_preferredHeight_m0174135911A565E1E7CB86DC2CF1E9961CCCDD4C,
	ProgressIndicator_get_minHeight_m8F8D055C8F93CA73EBF2BB5C4E86AF2A5CB7BE30,
	ProgressIndicator_get_flexibleHeight_m01C327AC8C7F5552965628CAC21E514673F061F2,
	ProgressIndicator_get_layoutPriority_mD0895CC071C02AABD1ACA7A66F6E84C5C632C2E2,
	ProgressIndicator__ctor_mEDFDEA8357D36C4F21EF63CACBC2BED7F1F4458B,
	MaterialRadio_TurnOn_m5FA4A706AED9414DF2D32816EE281B8BCAE85E29,
	MaterialRadio_TurnOnInstant_m604B91CDA0E32568FA7952A60FFFD8018FFA322B,
	MaterialRadio_TurnOff_m416383476DBA9448D08A45274A1AB1C3A0DCC747,
	MaterialRadio_TurnOffInstant_m42FB0646FA2FCB4D6FDBDFCE8C7D629D7EA14EDA,
	MaterialRadio_Enable_mAB541A3B02D87AA9A6CA0FD34936DE252C5CF02C,
	MaterialRadio_Disable_m728F24DD5ACDBB8DDE766182D1B1D9923982B788,
	MaterialRadio_AnimOn_m173F26A98F01292010C146D3B34D04A9ECF0CFF5,
	MaterialRadio_AnimOnComplete_m2A89A0A3DE4B6A11597EF0EEBBC2555E624F76F9,
	MaterialRadio_AnimOff_m1EFA680A5A37117AF4C7BAD9EEE0868712C41F58,
	MaterialRadio_AnimOffComplete_m77346F7E4D6EF7CD83B4783CC5201B4DD3B34577,
	MaterialRadio__ctor_mF393AEECB82353B1D7F73D7A53F226255B8FC411,
	MaterialRadioGroup_get_isControllingChildren_m0C508039585ADCB1E1F9601D01A9AE1CC19A6FD8,
	MaterialRadioGroup_set_isControllingChildren_m92C4B496B7069F9A946D71FC618B270D7D4CC9DA,
	MaterialRadioGroup_get_animationDuration_m44538EBC94BD78BBAAE3116AC96C28DD135DA297,
	MaterialRadioGroup_set_animationDuration_m6F966984932FDDED218259948AB73B497685297B,
	MaterialRadioGroup_get_onColor_m689F4FA38FD83AF1AF7A2EFA8B6737186AC80C45,
	MaterialRadioGroup_set_onColor_mC2789FE37D50B71D203A40DBCF046777057093BC,
	MaterialRadioGroup_get_offColor_m4E6C000D3B539A067616DB1490162A68CD3BBAB3,
	MaterialRadioGroup_set_offColor_mEAA358EF185978F3C746DA669D90859E8CC4C844,
	MaterialRadioGroup_get_disabledColor_mE152A7B35DE2BC062E0E437D4C25D4D6C59C6D05,
	MaterialRadioGroup_set_disabledColor_m14634FF58CA4F00D5BB9F07FEB2192E9831998E7,
	MaterialRadioGroup_get_changeGraphicColor_m3596CFEE052E7F4E8E152EFFB66D48C24ED5F16E,
	MaterialRadioGroup_set_changeGraphicColor_m23721D018BEDCB1A39F64E1D2A371E1449CD0C09,
	MaterialRadioGroup_get_graphicOnColor_m4AEEF42D853A4D8A1254AEAFFFD0CF25F68F761A,
	MaterialRadioGroup_set_graphicOnColor_m7DD5D1BA8DBF8AB770C56EF690F2A363398A31BD,
	MaterialRadioGroup_get_graphicOffColor_m665D3FED5EF2BDB57D251C0D433CE32645FC9EDE,
	MaterialRadioGroup_set_graphicOffColor_m41F76CFA3B451A354635FA557014F6E7B7475E14,
	MaterialRadioGroup_get_graphicDisabledColor_m779EF0B15906E13BAF76EE5511A974C93F14604E,
	MaterialRadioGroup_set_graphicDisabledColor_mCBAC0CE34C7506C7278C3770DD2C7812E471242C,
	MaterialRadioGroup_get_changeRippleColor_m131DAF3DD62048544A100B83277384EDF7831C72,
	MaterialRadioGroup_set_changeRippleColor_m9531BA7A1F6E2031B43696CC576C3BC3A0EDCACF,
	MaterialRadioGroup_get_rippleOnColor_m9D334103C8F262BA34DB56B087CCBF4B21C6056E,
	MaterialRadioGroup_set_rippleOnColor_mF79368E3D8A0BFB4D27668A894202E9487981832,
	MaterialRadioGroup_get_rippleOffColor_mBF313DB1F00B09816EA5ABE9A9895B746D30121A,
	MaterialRadioGroup_set_rippleOffColor_mB1D9186052B8E9682452C21BBC55149A1E833748,
	MaterialRadioGroup_Refresh_m01A0A0AF7443B05EF46A83A015E13E9905B67B73,
	MaterialRadioGroup_Start_m24F44BE6D2DDDD366E3CF532F52E5D590CD48B8C,
	MaterialRadioGroup__ctor_m93C889643B561A36AF6F2CD7C447F62D3EAD8953,
	MaterialScreen_get_optionsControlledByScreenView_m8C9AF4E6728035A022BC5E6BA064E83D51E22835,
	MaterialScreen_set_optionsControlledByScreenView_mBB7509C793760AE49D066222945DCDE03B160985,
	MaterialScreen_get_disableWhenNotVisible_m565391D21838509FFB9C07642F541CFFF2F3AA16,
	MaterialScreen_set_disableWhenNotVisible_mAF9DE63F335A5B6D558FA44D71F3235A7C5840C2,
	MaterialScreen_get_fadeIn_mD386173FFCFAC0E0D30C8E8C0C14A0E073E2AFC3,
	MaterialScreen_set_fadeIn_mD59986635AC4F9BB3D2784BB0B294BF5D5EED050,
	MaterialScreen_get_fadeInTweenType_m028EDA4FC3E0B5989C671B9730FEBF35342A7C67,
	MaterialScreen_set_fadeInTweenType_mD754A792B958DF9959B5A05F5020FE1E516E12E3,
	MaterialScreen_get_fadeInAlpha_m94B5939770F256BEB3C35C31C2B7ED5D539E0C00,
	MaterialScreen_set_fadeInAlpha_mCF946CD83F23207A32BC7ACE1A95AC56F5C949CB,
	MaterialScreen_get_fadeInCustomCurve_mA1B1FE3383BF28F65C50DBC2D4CBF0A7FB11D1CF,
	MaterialScreen_set_fadeInCustomCurve_mC9581F4B447DED184C7B5092311215BECE4151F3,
	MaterialScreen_get_scaleIn_m155B87B2C3D3AADED2E713D2A912C849A32F39B0,
	MaterialScreen_set_scaleIn_m9ADDDDB9A7527AE86A19184E55CEEF9ED077FDCA,
	MaterialScreen_get_scaleInTweenType_m7C40133127E35173B70BA55811D79B3FBC6420AA,
	MaterialScreen_set_scaleInTweenType_m34A6AD414B5EAEC164C5D389D78CA5BA15EC5689,
	MaterialScreen_get_scaleInScale_m2F72FB01827FA074773F21D46A7F99C7D93A1376,
	MaterialScreen_set_scaleInScale_m24E05B438E8764810DF327EC638D0B7335567AF1,
	MaterialScreen_get_scaleInCustomCurve_m1E0505A7E24FA24B547AC0B6DF1017454880171A,
	MaterialScreen_set_scaleInCustomCurve_m93BD32ED5904B2A89AD938539B8D2FECC972E40B,
	MaterialScreen_get_slideIn_m20AB3006703AD4B79DD4511FCB0426C701A74A62,
	MaterialScreen_set_slideIn_mD8E7A48F2B32BCA2BCB0541B3105DC00ADD02D12,
	MaterialScreen_get_slideInTweenType_m90706B218BD4F130940A79C97611BC2D3D9E5B85,
	MaterialScreen_set_slideInTweenType_m7E387230917F26F9B79E4FA3412A631E62BF9E94,
	MaterialScreen_get_slideInDirection_m47A6BED321BCCFDCAA78AF133F77C71CA42E832D,
	MaterialScreen_set_slideInDirection_m433AAB96891EF3EC37239840D2D7B271015107C1,
	MaterialScreen_get_autoSlideInAmount_m8B17D8FC23F03DF0A7DD2C6685A053C8E8CE7445,
	MaterialScreen_set_autoSlideInAmount_mED2E07993A8865EC36DA24417CE3EF23FB2B94D4,
	MaterialScreen_get_slideInAmount_m4F2EE292C2D653AA1E40B12207DF59D328A1DB28,
	MaterialScreen_set_slideInAmount_mFC0481F46A13C778334AFB60531DF49C5B406569,
	MaterialScreen_get_slideInPercent_m3CD3BF6EF149207A1AAA463EC41F57EA46A93AFC,
	MaterialScreen_set_slideInPercent_m722606E17FEC3F64803AA5E53F7A18D18B921ABA,
	MaterialScreen_get_slideInCustomCurve_m2889F45DDDEBB33705D6ABADF3CBAE73C66D1A7C,
	MaterialScreen_set_slideInCustomCurve_mE99E8F14E2B555DA58AB21412125CAEF6F8AEDBA,
	MaterialScreen_get_rippleIn_mE83C408E37BB976C8A7F916C3A070CB6646E13E6,
	MaterialScreen_set_rippleIn_mB5C44F0CFAA6D577360D2B112473E807C9A2B11B,
	MaterialScreen_get_rippleInTweenType_m5884B16DE52BE15A365817512DD953586E0383C4,
	MaterialScreen_set_rippleInTweenType_m24D29308F95A5057E6B87B22AFA1C3115744BAB6,
	MaterialScreen_get_rippleInType_m8713A7392D577348F9F63AC7F6C0F253E0C0893A,
	MaterialScreen_set_rippleInType_m5710701B43ADA8CA5DCE0E07D280E1AB9F09E9EF,
	MaterialScreen_get_rippleInPosition_m53FBC20467C84A02F2439DC181C15B04CD02A784,
	MaterialScreen_set_rippleInPosition_m10494943025CD1B526709DDCB820B43D77999B86,
	MaterialScreen_get_rippleInCustomCurve_m1C97B8EC417442382068EF3F7007F9362C3FE85B,
	MaterialScreen_set_rippleInCustomCurve_mA2E1958362D27DC91F873D9FCE22DC6C32D10A4D,
	MaterialScreen_get_fadeOut_m63E43181609D88F255897FA1B13C82A3E43B106C,
	MaterialScreen_set_fadeOut_m60219EB0A990E917A0F2E733306B522EBA977680,
	MaterialScreen_get_fadeOutTweenType_mF22EEEDB87DABE84CC9B9BCBD8B3B723DC4A5F72,
	MaterialScreen_set_fadeOutTweenType_m54238F9613B1BA64A3AB0F2AE53F6711EC9E344D,
	MaterialScreen_get_fadeOutAlpha_m5BF6C8703F4B920C13FDA950A04662AD002F983A,
	MaterialScreen_set_fadeOutAlpha_mB78F24F4C9D42DB41F53CCEF274607B477E1BFAB,
	MaterialScreen_get_fadeOutCustomCurve_m558914C1E7C467FB4A49E30222C7CDC85895108C,
	MaterialScreen_set_fadeOutCustomCurve_m3BBEA4A9E4DE2EEC34FB55E712633C848F8E352F,
	MaterialScreen_get_scaleOut_m037C8B56ABD51CEE89DF252DE52F18AB6116C1CA,
	MaterialScreen_set_scaleOut_mBFEFFEB951DD1F7E86A213480D11C4936056B157,
	MaterialScreen_get_scaleOutTweenType_m0B41CA5C8896E85DEB90F2A6EE91F980A5A869F7,
	MaterialScreen_set_scaleOutTweenType_m7B0AFDF662059FBAB7E2263EDDE670623FD7BF35,
	MaterialScreen_get_scaleOutScale_m1DCCA8FE7F946AD853C1F8DB6B70A62FC5901811,
	MaterialScreen_set_scaleOutScale_mF2D6CA337E7FC4A7E3B530C5321B33A0AEC22AB4,
	MaterialScreen_get_scaleOutCustomCurve_mBEF670F50E8142411EF9AF57A22017AC72704530,
	MaterialScreen_set_scaleOutCustomCurve_m344C176BBF41E2F34C4AAD45607414846246A532,
	MaterialScreen_get_slideOut_mEAC0FF8EF86893A069FEC6FCAFA72A4BA29BB726,
	MaterialScreen_set_slideOut_m64E206E169B6D606CD585D49704654BBBD71EC79,
	MaterialScreen_get_slideOutTweenType_mDEADABA9E877CB345577C801E6F10602B7E92645,
	MaterialScreen_set_slideOutTweenType_mAC28AF16462F2B3BC7B0DDF0058BD1AF60228AFD,
	MaterialScreen_get_slideOutDirection_m7EE939ECD255D8D509EE8FAE1EFA150E8FD29D84,
	MaterialScreen_set_slideOutDirection_mBDDB59543D38584035D50F1D39708FF71C117E09,
	MaterialScreen_get_autoSlideOutAmount_m06971E28D9F2A41CD08FC0B8942ABEC4781883DC,
	MaterialScreen_set_autoSlideOutAmount_mCFA75194A916410273AE4BDE5535BB72E2CF5144,
	MaterialScreen_get_slideOutAmount_m619EA66BF2E76ADCED0A492416F07B2239AB2FA6,
	MaterialScreen_set_slideOutAmount_mBA4594C5D2C6B8480E871D3E43A4093A0CCE7CAA,
	MaterialScreen_get_slideOutPercent_mC79A64DE6266DA2844A1F128699B28A73F8B73BD,
	MaterialScreen_set_slideOutPercent_mFEDFB3197ED578378D052ADC89EEE2E8662908EF,
	MaterialScreen_get_slideOutCustomCurve_m8B771CC33E3FF342DA665251E5EE737226E981CC,
	MaterialScreen_set_slideOutCustomCurve_m1EC1038737580903205F73B7A11F9601DAA4826A,
	MaterialScreen_get_rippleOut_m2E61AF7B55FEDDD961CAFA3703E945611BE67A1C,
	MaterialScreen_set_rippleOut_m71447291C32328334B995526E0B06140905279C2,
	MaterialScreen_get_rippleOutTweenType_m23CC2EECD0F7AD05849EB94F729AC94C720F8EEC,
	MaterialScreen_set_rippleOutTweenType_m34D35C2C95E35224FE3F2D1A0D1ECA4CB9EE7C78,
	MaterialScreen_get_rippleOutType_mEEDFDC956CFD849CE5082710A293156FB77BE127,
	MaterialScreen_set_rippleOutType_mF0D9E5298A2014B58E02F1793529C67178A76ADD,
	MaterialScreen_get_rippleOutPosition_mCBBCC44B95C94191580BBE9412A7C24130168332,
	MaterialScreen_set_rippleOutPosition_m8585FA98F124C3AEBCC912AC81CFD540C054CFB9,
	MaterialScreen_get_rippleOutCustomCurve_mAC10EC568B347ABFDBE5CF728132006D9ADA159D,
	MaterialScreen_set_rippleOutCustomCurve_m9C5F83F6790CE8F1E43B6D1F1143FB2821B9CC8F,
	MaterialScreen_get_transitionDuration_m5B455DFD6225501FC8A3D53915557545E1C4BB4A,
	MaterialScreen_set_transitionDuration_m28B916C7C00974577C57B35C91047319A3AAF7E6,
	MaterialScreen_get_ripple_mC8C2233E5599F22599CCC69492CD408B7A7374A8,
	MaterialScreen_get_screenView_mAA0F9BC36AACCCBA69DE1623EAF2A10E5BFB92EE,
	MaterialScreen_get_rectTransform_mA59CB3F132FE37EE0E5752C199A9195034A2255A,
	MaterialScreen_get_canvasGroup_mFAB2691E52F1DEF51B883F34C3D1A3DAA2FC4F20,
	MaterialScreen_get_screenIndex_m82779D8F23D5C684D5CAE8F52B1B56B13584B0C1,
	MaterialScreen_set_screenIndex_mEDA40476FD8AD87F6DB2A36B405E19C6D9D0A07B,
	MaterialScreen_CheckValues_mE67E05A57542B5B0CA484EB38043D3BF7C55DE56,
	MaterialScreen_SetupRipple_m16F0C641140A9282D09D81A065BD93E578A9A992,
	MaterialScreen_TransitionIn_mB6F9C838AD6901474BEBFB5E1BE22A545FEE5093,
	MaterialScreen_TransitionOut_m932239AC67020C10FB0E0A3318DD9C10740A71AD,
	MaterialScreen_TransitionOutWithoutTransition_m17E78A4B6C11F448CFBB8402C50B7F18E95A71FE,
	MaterialScreen_Update_mCDCF4E73513DBF9BD13A9883F414152891682378,
	MaterialScreen_GetRipplePosition_mF2CE22A50679EA253D8E85E8F4B38793F173D024,
	MaterialScreen_GetRippleTargetSize_m5D52724B97F39C9CAD4D29E5A477834CDFA8DB89,
	MaterialScreen_GetRippleTargetPosition_mCFCAD34C893E01D68FC9F6A1FD51B5F4DB86F55C,
	MaterialScreen_Interrupt_mDF89B9202350B09234EFAEA32EF6D5E5D65AD13E,
	MaterialScreen__ctor_m407B2B587259AC2C77BC0B64547A52D865E84132,
	ScreenView_get_materialScreen_mC040184521061D840DE905C6B0FA1343DD66EF3D,
	ScreenView_set_materialScreen_mBF6D27B5E44B9371B928E9B8B5F21E1D2325ECF2,
	ScreenView_get_currentScreen_mE68140E10C85B299BB8D4FE8F08F1221F1680B56,
	ScreenView_set_currentScreen_m161C465E81EB717A96D862ECDDEE069FB60AB54B,
	ScreenView_get_lastScreen_m9D32B27FE9E66CFE508F6B8AAE5A9DA2D0595426,
	ScreenView_get_fadeIn_m448908128DD9DEF5BA22C8C09B0F73A0F3F885AA,
	ScreenView_set_fadeIn_m1DECDC95DE03799D16F8038BC56873270B0795AA,
	ScreenView_get_fadeInTweenType_mB7F0568BB2879A97C3DCDCC5EB5CFEF09084FFE5,
	ScreenView_set_fadeInTweenType_m1E44954340A2D1A72FEC36575D7BC956158DD594,
	ScreenView_get_fadeInAlpha_mA11BF52E9444099B879CEA1926BD9246A0E3B9A4,
	ScreenView_set_fadeInAlpha_m60D51F7C270FE535317BBD4D9621FB3DF82F8F21,
	ScreenView_get_fadeInCustomCurve_mC9925269A84FBF31BC3D3EDB3014DC62813B39A7,
	ScreenView_set_fadeInCustomCurve_m0CC30AC3EC837651B06695EB93C79D2CF203F02C,
	ScreenView_get_scaleIn_m5A9AD92BD2B792F5B81E5E1004D13306D7039F6D,
	ScreenView_set_scaleIn_m372E17173379F8EB97B8C4C446B032B7001ED068,
	ScreenView_get_scaleInTweenType_m587B8A07EB5F324BBAB0A77D4E8934AADEBB9A29,
	ScreenView_set_scaleInTweenType_mC954E31AC8CF9C759ED4BF603EF227E48286B976,
	ScreenView_get_scaleInScale_m23B299F9091235C89C833A23CAD2D7801C803772,
	ScreenView_set_scaleInScale_m0C93A22E80925A6DC2E8D54C51A95BC06A47EC54,
	ScreenView_get_scaleInCustomCurve_m870C5E47B730A4F4FF253AE9486781127E2D583E,
	ScreenView_set_scaleInCustomCurve_m3F2B01399238DCBD1363824D95291FA0433AEDA1,
	ScreenView_get_slideIn_mB76CEC39B3501B8F320C5DF6D7369F862F4DCF24,
	ScreenView_set_slideIn_mB9B8339B7C4CF391EA4FE70F93678D39F10CEF9A,
	ScreenView_get_slideInTweenType_m10963CC9F24EA7DDB696E15732B893D3DE2DC680,
	ScreenView_set_slideInTweenType_mD8A8E652185D1AE213F6EE17A7CFB0E06ECD0370,
	ScreenView_get_slideInDirection_m027147453FC600A887409192C11CC5C7DB9F0228,
	ScreenView_set_slideInDirection_m4D99D1C31F5B80736CAA806E2D3557829F77E696,
	ScreenView_get_autoSlideInAmount_m24B8748BC9856C3D513E225883982B2251E794B0,
	ScreenView_set_autoSlideInAmount_mBFEB0E81DC37E9F978C2B3D474182919201D2443,
	ScreenView_get_slideInAmount_mCCF93643C9E542F7845EB75AB12AB08A6040C36E,
	ScreenView_set_slideInAmount_mB2F14FCBEB2865F2F055169AF9C0798FB3DBACFB,
	ScreenView_get_slideInPercent_m30B87BE1AAD1BD1500CCB9985C8A6A7FBC07C11E,
	ScreenView_set_slideInPercent_mA46A64706CD582E1C7137EDC9C50EBC23E442DB3,
	ScreenView_get_slideInCustomCurve_m8D9170EBA0263141FF37B33F54F1240575783EEA,
	ScreenView_set_slideInCustomCurve_mB6EDBA583ADCC24B451CD250BAD2DAE3F0084BC6,
	ScreenView_get_rippleIn_m564BD7AE70A32FA65D45F76E52EC2AB6E62D89CE,
	ScreenView_set_rippleIn_mB0DF425B603F68140A1ECAC2544A5E666E5B20E9,
	ScreenView_get_rippleInTweenType_m6B1BD7BC706C7DE478FD2CC8ACBF8AA92F28ADB2,
	ScreenView_set_rippleInTweenType_m6A393091E852AE5BE3BEF580704AC0EBB4A67E7F,
	ScreenView_get_rippleInType_mEE5649B9810F5D04D222A7723BF2FA4D0E4C1FC8,
	ScreenView_set_rippleInType_mDAD063E9AFB1B7A921A2FD3ED115C7A3EFAE610C,
	ScreenView_get_rippleInPosition_mC069B22275D70959BC9ED9BE0222D906F9FE194B,
	ScreenView_set_rippleInPosition_m7B45A137E1F2E129A5EF4F13962DE4EF87EA9002,
	ScreenView_get_rippleInCustomCurve_m5B0780D2BFC6ECF0CCBA95153961BA0B84763DB0,
	ScreenView_set_rippleInCustomCurve_m31866CBE97167AEA8CF73E3DB089453E22D122EE,
	ScreenView_get_fadeOut_m298313FE709B654985943EF9295C4C4F1DC71BAB,
	ScreenView_set_fadeOut_mC2C35D06ECF760DCB3498264A316C6BB5B4EA397,
	ScreenView_get_fadeOutTweenType_m21C7C85F99C9BF76CD9FF9C5E15E8EE6B764CE50,
	ScreenView_set_fadeOutTweenType_mDF7E60A8419894027674AF9260B2894F4A783BFA,
	ScreenView_get_fadeOutAlpha_m23D8FAC77F98D54E8412417292F217C25E823084,
	ScreenView_set_fadeOutAlpha_m97835883865F95019771F2F72E27C7E5E6B3728A,
	ScreenView_get_fadeOutCustomCurve_m7E64D45F73D369A0A183C46E8B022050B20C78EE,
	ScreenView_set_fadeOutCustomCurve_mAB5404C0B151205F7AB86D140E6EE706BBF121D1,
	ScreenView_get_scaleOut_m8C6D35CDD364BFF392839D7E45B2D64A1B49F145,
	ScreenView_set_scaleOut_m3056BC3F1B6238131C91557070B83B078882A66F,
	ScreenView_get_scaleOutTweenType_m5836753A04BFF07643BBEB53CC12DFC71D1252AB,
	ScreenView_set_scaleOutTweenType_m1A34F297050D88113A700D959F3913862C5A3F18,
	ScreenView_get_scaleOutScale_mD9D8DF7E864AF3A4C70EA5BBFAA6A389B2E6237E,
	ScreenView_set_scaleOutScale_mA62CB704399899E41901A1D38BF3DBB1B8077EB1,
	ScreenView_get_scaleOutCustomCurve_m8BCB567F745FAE3D8B0F832C0ECD6B378AC1C18B,
	ScreenView_set_scaleOutCustomCurve_m4CAABC799CE5D0B0C8605F822F2E4D18811D367D,
	ScreenView_get_slideOut_m33E34413073D6E054440A797BE21D9F25F053720,
	ScreenView_set_slideOut_m4493DD5CDC18EE4C1BD4A0B5BF17CBEE11366506,
	ScreenView_get_slideOutTweenType_mA3169EE1295EA60390FBD281DF4986D67EBFBFB5,
	ScreenView_set_slideOutTweenType_m6537B9613890FC5EAE05E56DED7F78D8E0AC1035,
	ScreenView_get_slideOutDirection_mF79BC144A20A6F6A33BD95A2565EB8AC9223505C,
	ScreenView_set_slideOutDirection_m5EF9FBCAD203BBC5B2AA65519DBABC06C269AE36,
	ScreenView_get_autoSlideOutAmount_m3DC088A0EF45C6975484E8C9A6EE07669E97DE94,
	ScreenView_set_autoSlideOutAmount_m133C5F0BDDD0CF38F91AD270014C10BE4EBDE167,
	ScreenView_get_slideOutAmount_m16D04247CA653FD9A5E3EFF0E127313351ACEA5D,
	ScreenView_set_slideOutAmount_m262DB81070004C64C2C2A61C86758C8C7170E976,
	ScreenView_get_slideOutPercent_m5F3A72ACE4DFFD579637140BDC329CF07C4636E3,
	ScreenView_set_slideOutPercent_m0CF36F53F1AF6DBD13BA820FE09BE9DEBE568BCF,
	ScreenView_get_slideOutCustomCurve_m20AA838B18BD726EA93A84C63ADC4BA30A140661,
	ScreenView_set_slideOutCustomCurve_m987E68D8FC632C389E302C3568807FB9CE46ED4C,
	ScreenView_get_rippleOut_m506D46423A528EED8B9BA6F558854020BDB020D6,
	ScreenView_set_rippleOut_m285970BF64DBB1703FE22431217D83D957A61651,
	ScreenView_get_rippleOutTweenType_mF05CE39068906519435A7C6980F97ABEA215E68D,
	ScreenView_set_rippleOutTweenType_m99EC35E8E9BE3381E19D63C65F39231758A65728,
	ScreenView_get_rippleOutType_m1749C1337E907F7497C48B0DD8B23F547875E896,
	ScreenView_set_rippleOutType_m34D0A4DCADA36BEBDA538F2B6928230AF4ABC481,
	ScreenView_get_rippleOutPosition_m635E9CD024B36D93BAB0D28F26C7B74BBF929231,
	ScreenView_set_rippleOutPosition_m6DB91438DAE53DC051DB135F0DE9CE80176970BC,
	ScreenView_get_rippleOutCustomCurve_mEE4E7B649255B7D13698DD18B53564E5ACD6058D,
	ScreenView_set_rippleOutCustomCurve_mA7D4824CCB5FC71098B7B49B73D6B681751DCC53,
	ScreenView_get_transitionDuration_m713421A9156B8548E458C22373B44B324044FFD1,
	ScreenView_set_transitionDuration_m75209A2B6E159D264AEF709AFB4E8A3D247909D5,
	ScreenView_get_transitionType_mD90A20112553B932A036A73FC46FA7D5865C2367,
	ScreenView_set_transitionType_m5BFE2FF1CABA86F8C12E72AAA8BF7E9FB3D393F0,
	ScreenView_get_canvas_m90CD99FC5F6C13E336A2ABB7D02F8E1A90B7FCFA,
	ScreenView_set_canvas_m558E8AC9E140B92A06BAFF06D28D5A5A8B0EC570,
	ScreenView_get_graphicRaycaster_m7086A4A54E591555AE8B1F92EC3CBDAFF1337481,
	ScreenView_set_graphicRaycaster_mC478F36C944497E4FDE0014FD690AF63273E047D,
	ScreenView_get_onScreenEndTransition_mAAD64C6D83CBEB6A49653826583590B8420301A5,
	ScreenView_set_onScreenEndTransition_mE6A8391586AD252280AEA5872B89DCE0D067CE79,
	ScreenView_get_onScreenBeginTransition_mD786325A01B1FC7400654A83845CE3F3AE1AF394,
	ScreenView_set_onScreenBeginTransition_m5C45CD074E45BF5C9FB3DDA0FA2EBBFE22BF3965,
	ScreenView_Start_m0A85BC40968AD95F7281294C6C1F013161BFD3F5,
	ScreenView_Back_m64EF03911E611F7C51B961AF633F6CDCB01F3AE6,
	ScreenView_Back_m60AE0A9F884EA58B0FD2452035BEF75B35B86DB5,
	ScreenView_Transition_m3F4C90D7F4F6DFEC701AD31319F63DF1B9595DFF,
	ScreenView_Transition_m7D34F0D19A709BDBAED70D8588BEB97296F92798,
	ScreenView_NextScreen_m59E15077E2AFC5CFCDFE24051ADCE2A059D5116E,
	ScreenView_PreviousScreen_m65FD7EF62F0BBA8F8C3C3FE0AAF136A6C7A4BF3C,
	ScreenView_Transition_m7658081324584E36CE2ED2A57EA7E67170BCB79A,
	ScreenView_Transition_m14C443DCFB616B427A409DC735BF0D36249C4788,
	ScreenView_OnScreenEndTransition_mCA1051BDBE8AB8E1122A40E01DCA663C1ACA7039,
	ScreenView__ctor_m5CB9E8E76D011B0714922E7CCD0D1DEA64AF16B0,
	Snackbar_get_actionName_m62160401F151F9A0026769C5B69FE66CCE7F9F4D,
	Snackbar_set_actionName_mC5E29574483A474A45D135096769E00F473C4FB5,
	Snackbar_get_onActionButtonClicked_mA986C52C76C13F37439323D2BCB91C3D98B9F2AF,
	Snackbar_set_onActionButtonClicked_m2547F28A17DD5D5515B9E20695A67EA6F32A5FDD,
	Snackbar__ctor_m114AB2D290FC0CCF79125DC20050C2404FD02B4B,
	SnackbarAnimator_Show_mB8C1F0ED6967B2D8DF055F56F9DF14034DB3A44F,
	SnackbarAnimator_OnActionButtonClicked_m0F30748BDD4C21E32C0042A2AFA5099DF4AD253C,
	SnackbarAnimator_OnAnimDone_mBF6543BF5524836B3FD76F0775FA23D9AD1563D4,
	SnackbarAnimator_Setup_mADF8AFBBED028A6B0C12EDCF22D22CFE74A98A5C,
	SnackbarAnimator__ctor_m623953092C3C3BB55FAE2DC507D765F34DD4D2A4,
	TabItem_get_itemText_mE7DAC31087DA178EFB3E1E501CE478B74D248203,
	TabItem_set_itemText_m6763075A8F21C19388D305DFF16AB5F6F1EBD941,
	TabItem_get_itemIcon_m934A2883FA835085F33148A58252D0BCC5D4728B,
	TabItem_set_itemIcon_m29482407C2012482599350CAD0DEA635E53123DC,
	TabItem_get_tabView_mB5167D4BA17AAAD87373DEB59547AE39589A5C16,
	TabItem_set_tabView_m67D37C974798469DB56725D7C317C12597807F17,
	TabItem_get_id_m7626B93139145EEC7C29A7C27056C8795B67F516,
	TabItem_set_id_mC3AD27509A4E25BCFA5EB553A90AE810F6B20F35,
	TabItem_get_rectTransform_m0495C42140EDCC36AA5E99605EBD8BDEFF2CD62B,
	TabItem_get_canvasGroup_m9EDFE25E0BE74AFAC7ECC38B362A3A7A2D71692C,
	TabItem_OnPointerClick_mC27F85C266B5BE6E2BC91D4BE033D9C0A96C9A69,
	TabItem_OnPointerDown_mDDB9259629FF0BE02EC1E2CDA287855DECCE9D36,
	TabItem_OnSubmit_mC7F0CFCE6EC74B73456E4F12F04F1BECFE29FCFD,
	TabItem_SetupGraphic_mB00FA1B526059DE12EB2A31A600852BED90692EA,
	TabItem__ctor_mD8B029B0E665FAE69A0906ED405FF49ADA06A72A,
	TabPage_get_interactable_m9977D2392F9E9E469D04F1250D9205DFC7522A9D,
	TabPage_set_interactable_mD4BEDCA3AF533B89FD1E83E7DC5A539C88627A10,
	TabPage_get_disableWhenNotVisible_mBED55F54FBC9F3AE6DD223CA5015559231F93325,
	TabPage_set_disableWhenNotVisible_m569E7B98AA9A6D883EA3B3CEC8B2AB8A1A8B41E8,
	TabPage_get_showDisabledPanel_m08C2BC74A43B1ED005A1CDCBA5E05B6A4B9E4B15,
	TabPage_set_showDisabledPanel_mD2A0737FF382C220C42EF45FF0048E4A443E349D,
	TabPage_get_tabName_m01DE9F8D8B721F14F679B372634D9AAC5343F2CD,
	TabPage_set_tabName_m9A1F01C28E065023C5B5F9F6735DA56F09809B0B,
	TabPage_get_tabIcon_m8C407B6AFC979584D34A5861715215CDA371252B,
	TabPage_set_tabIcon_m9157EBC799ACE4F510038940856ACBB64808A0B7,
	TabPage_get_canvasGroup_m79BB00D6937A3BAE2A2ED50318D0AD085F5E1F49,
	TabPage_get_rectTransform_m7FD3EF48B9513E639C3AC48A0C1B84C4397CB92B,
	TabPage_get_tabView_mF03EA859EA99C262C804D198392BC89F42CCC213,
	TabPage_Update_m7F9FA872676CD52ADD1A8151A70849EAE8CE86BD,
	TabPage_DisableIfAllowed_m7184ED4DB6CB40F49B424797F8FE6576538FFD95,
	TabPage__ctor_m53BB6C58D417BCC1151245469343A311CCE87CB6,
	TabPagesScrollDetector_get_tabView_m3E92F768CB31EDB608C7B9573DE01717871C38C5,
	TabPagesScrollDetector_set_tabView_m41D50A31B1C79C331301131AA198973C1E4F4EC7,
	TabPagesScrollDetector_OnDrag_mD30C341F4645A1AE615287A0960632E049C4CCAF,
	TabPagesScrollDetector_OnEndDrag_mF8416A58556275E4A8E3A08E14ADA3E7E64041B5,
	TabPagesScrollDetector__ctor_m0C007B32B70A1B0B4CE712863F9E399915692C0F,
	TabView_get_shrinkTabsToFitThreshold_mC6C9F0177BAF7A2BF453CF9CC84C047B4ED2F3C0,
	TabView_set_shrinkTabsToFitThreshold_m67C818D71EB868A3F73D36845CCCBFDE5F72F2F8,
	TabView_get_forceStretchTabsOnLanscape_m70EC84EF69AA4D66037506292305EC8558A72E3D,
	TabView_set_forceStretchTabsOnLanscape_m56C9316375B1B336D7AAC3C00170B0FF5F88460B,
	TabView_get_tabsContainer_m19EAF248E07156315A149F34E1E46274D38C35A0,
	TabView_set_tabsContainer_m62BDE762695B0B8926A1CF9E50953821707E1D98,
	TabView_get_pages_m06C444FF56847286EFBC4943AAE47161A6BEB73A,
	TabView_set_pages_mFB52BFB4CD929CD0685AAB8D62F1C9307F36DE13,
	TabView_get_currentPage_mF2999B7C3E943DB35DB48517B06BC79F3C741A31,
	TabView_set_currentPage_m2A67312FB80A58B65AB011774E1C7AAE8BA0F1D8,
	TabView_get_tabItemTemplate_m95D4A1A9419FFE6F67078CD224503BFE3762293B,
	TabView_set_tabItemTemplate_mEE9620143B81FC6EB3BB92668CC1F30D2712D327,
	TabView_get_pagesContainer_mE80479D1347D0D0502C157E0824CDB03840BC318,
	TabView_set_pagesContainer_mEF23535A0C695084BBA2BDD23959FE359C2A9359,
	TabView_get_pagesRect_m52C364640DD97FE6861955ACB08EAFDBA897A5F5,
	TabView_set_pagesRect_mBDC561CBCB65EC6AC77E27C1B360C044F0079295,
	TabView_get_indicator_mB60B18F071BA6816719DC7C07128BC34FFA123C9,
	TabView_set_indicator_mEA5EDC6B6B2F74EE1DA3E257FB0F11224EC5F565,
	TabView_get_pagesScrollRect_m49013587C6AFFBAC208D89AB704B547D0732B38D,
	TabView_get_tabWidth_m35E5CAFF0402C57548C04FB05220290962CADB07,
	TabView_get_tabPadding_m0AB02555FCC3A0F6B6491CC4A8107873CA67FAC4,
	TabView_set_tabPadding_mFC219C82517A8EDE22602280DDBCC54780F1F971,
	TabView_get_tabs_m529D78B6DF112B5D69C2A1331B7221E0A663A2A7,
	TabView_get_lowerUnselectedTabAlpha_m29B6C62C43882348BFC71650CF9016D32038311D,
	TabView_set_lowerUnselectedTabAlpha_m1ED66C4185EF4253C465B34C2FB353053BB17450,
	TabView_get_canScrollBetweenTabs_m872E1C9D249E4690A1A7C34EEE4C61921C73105E,
	TabView_set_canScrollBetweenTabs_m4B8E691E3B36E4BDA56F0181AFC8FFA46DB67B28,
	TabView_get_rectTransform_mD0CB9C7DCB4F627FD28FD37BB6E7BA0D15E5B7F7,
	TabView_Start_m5485712005B71BE43126D4CCC6258EDB97801388,
	TabView_InitializeTabs_m42FC15E13CF2050EE8312FDC9DDD580E5328726B,
	TabView_InitializePages_mA4A41FE711D423AB9219CD6FF1214EE1D8065A57,
	TabView_GetMaxTabTextWidth_m2DA1C8B432EC69F509463ECE8627353A8D51F772,
	TabView_SetPage_mB18A347A0ACAB08A81B6CBCEE9DB03251465DBDD,
	TabView_SetPage_mC92977AF09073AEA9BCC12808E49E9E75A5FDE0F,
	TabView_OnPagesTweenEnd_m19386D005D62E60AAA34A015B699F5752B2697D8,
	TabView_TweenPagesContainer_m5967FFC235AC3E57ABE3DCAE6DC2501F301FCB30,
	TabView_TweenTabsContainer_mEDBA8926BCD52963EF26685AE8AA73FA5AB8E864,
	TabView_TweenIndicator_m6219BE14AABD7AFC966A43C5702D7C1AEB021D2B,
	TabView_TabItemPointerDown_mE6E069C323E721E868A60208363BD070A7C17E99,
	TabView_TabPagePointerUp_mF63755B4CCA10664D6B5C121565F4FA66F804662,
	TabView_NearestPage_m9A543F5F0AFB22C00CEE988A49DF13C3105900F6,
	TabView_TabPageDrag_m35267E68287CDABB17628F0F200C460068ADE6E1,
	TabView__ctor_m2593687D496413F7BF4D2EADCFF9683CD837D98E,
	TabView_U3CStartU3Eb__65_0_m3CCCEADB12B10B18AE5BCB33FB3FB4101CC20155,
	TabView_U3CTweenPagesContainerU3Eb__72_0_m30D8BF203C99A7973ACA8E88ABECD6F7CD572D9E,
	TabView_U3CTweenTabsContainerU3Eb__73_0_mB3FD5113B97BB1AC5E9A36D68D6336D69B4CFF15,
	TabView_U3CTweenIndicatorU3Eb__74_0_mDD09C72E055114CE6C26791D93CA21A1FB23DEB0,
	Toast_get_content_m7B012246101CE3EF06A36EA61140CD1934777619,
	Toast_set_content_mD4893957DA443B4EF8D15C4418643363A623C194,
	Toast_get_duration_mF2F87F12A6B1A8C7215B704E12181673BE5C2500,
	Toast_set_duration_m8E015BFB9B59BE62D0CBA20D0ABB18AA6A659899,
	Toast_get_panelColor_mF8028C6A94B86D706DEFE659A727A5DFAC28D7F1,
	Toast_set_panelColor_mC6E17512913651C966DE0739C0C3CC8BEEA01EE1,
	Toast_get_textColor_mF6A151AD1B8941538689B8E6BC702014FF64243E,
	Toast_set_textColor_mA9D85D6F30765C88F174D350B5FFFAE6A8E3B165,
	Toast_get_fontSize_m96E918F2521EF576B3FF5A0219B9CE181E6BF810,
	Toast_set_fontSize_m258C8FA35B4C61805172FB3D3C5322CF9B6B4B37,
	Toast__ctor_m1F6B22FDBA19BA9F8F6E3A952758B7CC2709C59D,
	ToastAnimator_OnEnable_m990C84FEC9C5A9AC75E1F5312B354043C3CE49C6,
	ToastAnimator_Init_m0B199E08986B8D64DEAD32DAC492982765D6103B,
	ToastAnimator_Show_mC58A7169924D27D09B9B1A35F24F055CA64C60FA,
	ToastAnimator_Update_m4B879865789893A83B5BB03095A8195072FF5CFB,
	ToastAnimator_WaitTime_mD40FBEE27423178B80EDD99415761A93C81D1749,
	ToastAnimator_OnAnimDone_m7D7E50EF39F9C0FD128E8C19DB63C33428AC443D,
	ToastAnimator__ctor_mBF4EC0B86503845E0093F14CDCB7E173AEF76121,
	ImageData_get_imageDataType_m47313CB0BAD4F965EFF74BBBD5F252A29694C5FA,
	ImageData_set_imageDataType_m8C4F7454B557B765011CC835F163228DDAA1D3DB,
	ImageData_get_sprite_mD879D608D11E479E951D4A7F775351B59B4D7728,
	ImageData_set_sprite_m35EEDF6A0CD2F31F12554CB668DA677414BE2A81,
	ImageData_get_vectorImageData_m0E1361FBAC6654A1ADDD95B5DED4E8E33F25839D,
	ImageData_set_vectorImageData_mA74576ADC12796EF7C2EB5F2578FDF147AC31AF3,
	ImageData__ctor_m78D727641D91DB9969C4FB6AA1E92F34F51D0535,
	ImageData__ctor_m4E7F6F5C356D56F6071493A723FAA510023646AF,
	ImageData_ContainsData_m66874F2A15C8666BD895BA0B64EB79E430738562,
	ImageData_ArrayFromSpriteArray_mC7B1F93314D8C6E40E8408C917B383CBE9244FA8,
	ImageData_ArrayFromVectorArray_m41C04A8B80B87332AF92EF34B53369B49A03740F,
	MaterialIconHelper__cctor_m60E76BF6A1272DE818B769245C02F5A234FF700A,
	MaterialIconHelper_GetIcon_m85A699DEE71B8D3FC2CE9CDF1BF33167A25CEAD4,
	MaterialIconHelper_GetIcon_mB176DAFE7FCBE29645FA5D98783F5E618FEEC7BA,
	MaterialIconHelper_GetRandomIcon_m0CCD248F149343A45FD5A83C0AEF94044AF0349B,
	MaterialUIIconHelper__cctor_m0FD12AE1E9D6820F875B518380A9B4FB7C31023E,
	MaterialUIIconHelper_GetIcon_m4F07CACDC8DB96481AB121BD9337C0B91F50C2A7,
	MaterialUIIconHelper_GetRandomIcon_mA5F414298E7B477781C28AE098166076C6CB4223,
	Glyph_get_name_m24CF8983933215030054129B7374338FAA73F2F1,
	Glyph_set_name_m569CEAA0EC3961E389BE7CAED46FAC0324DB71B5,
	Glyph_get_unicode_m3B39A95853A6E6D2706FD917B983BC78D38AB396,
	Glyph_set_unicode_mD53B42E95FD3519602798C220DD4417345D6BBBB,
	Glyph__ctor_m7DCC1F26CC1FD50F5C2FF1211A54E455DDF6CECB,
	Glyph__ctor_m1D933159347D47533906412C8CB55EA76DC09C3B,
	VectorImageData_get_glyph_mDC19C14D55ECAD0FE0AAE5B12E3C7BD4BD38A824,
	VectorImageData_set_glyph_m1096F14DD58633D67F620955EC5FC78CD9E199C2,
	VectorImageData_get_font_m899CA8BF0BD2C3022DA71F712AA41C381394E628,
	VectorImageData_set_font_m5FDF140F1A0F0AD2C9C9B7FE1F65B3D412E5646B,
	VectorImageData__ctor_m1E793056B33C6B2BBFD643EDBAB30FB69D070294,
	VectorImageData__ctor_m8D8AD84C4DB510E6EE7E44E913B128D5F674F7A8,
	VectorImageData_ContainsData_m3279973291C19A1B25F86543EE559CE90E16200A,
	VectorImageSet_get_iconGlyphList_m8D55B11E23FF7CD7FCD438E1ADAD9951B9D1B012,
	VectorImageSet_set_iconGlyphList_m97F54AC38A6B8D77129810BE113B4F86CD47D94D,
	VectorImageSet__ctor_m95FC5BD43B0D5F8C3B104673DF2E0EECD1814B73,
	VectorImage_get_size_mF574C0DC7A298B548D4DC5F26B6D17AF72DED59D,
	VectorImage_set_size_mE6C08D268FDE8850339E331AFD92E7CA17BD18BA,
	VectorImage_get_scaledSize_m75762E8C407954D1F533C6C6847FFE64058F8BD2,
	VectorImage_get_sizeMode_mFD01469CE229B1648B1E6B5F2A5C30485A3714A7,
	VectorImage_set_sizeMode_mDB2A16EFB133C46A97F8BCFB05CC9B202691455B,
	VectorImage_get_materialUiScaler_m11BFD68684F76F71CCFB21267AC839F5D395D986,
	VectorImage_get_vectorImageData_m14D2D4213409747DB38FC7F15237C233306AC71E,
	VectorImage_set_vectorImageData_mEC90C234A36F541263A5490E9C724636B6051157,
	VectorImage_Refresh_mA5A6D06D7A2762C6217346AFAF558E3407DE6D82,
	VectorImage_OnPopulateMesh_mF1B639EE71B659863637B496EA2BB3980EFA650E,
	VectorImage_SetLayoutDirty_m14543F897C3C56AAF0EAD79E8CD66F7007BD9912,
	VectorImage_SetVerticesDirty_m47C754A22E51BE60A3630AD4423788718EC245E6,
	VectorImage_SetMaterialDirty_m9CF3CB39A412CCF583CB5584EB922C73379C461A,
	VectorImage_OnEnable_m4D927807590B8AEF0F130AFF58E0FF855FACD92D,
	VectorImage_OnDisable_mEE83D0F8A2EB094D4FBF22EF92CF58489BF3B7C0,
	VectorImage_Start_m6E88727D61D68C4C969634A3C9811CCE0C36CE0E,
	VectorImage_updateFontAndText_m5E8921737A015221C211D6FE13DF0AF6DA902380,
	VectorImage_RefreshScale_m2FB255BC3B2CE587BB03F7C9695BFB37EB138036,
	VectorImage_CalculateLayoutInputHorizontal_mB1411BAE9CC3AAD81969ADCD2ACAE9A598DAF07B,
	VectorImage_CalculateLayoutInputVertical_m396F651C0185BB756E557003AA5701A83DD9A038,
	VectorImage_OnRectTransformDimensionsChange_m6E9A6200E8EACB92F17590667F1F3FE286048EC8,
	VectorImage_get_preferredWidth_mFDFE2865A2B2D3BEAFD781170DD7780411FB765B,
	VectorImage_get_minWidth_m2DA71460CEE7762AC998EE9846E2F97C4659E6AC,
	VectorImage_get_flexibleWidth_m1D81950AB1B56E639E9F89E1E22F74FA01DF6D7A,
	VectorImage_get_preferredHeight_m86454825B4D89248010B418C8120A00F0A194744,
	VectorImage_get_minHeight_mF0440EEB591318E983CFBD6A168C3C0008D362EA,
	VectorImage_get_flexibleHeight_m064131D4CBAF4FC842C85A202AEBC88969AE1651,
	VectorImage__ctor_m5998DA07058E2A7D9692318A0331D7C064DB04AB,
	VectorImageManager_get_fontDestinationFolder_m593D67BC61263C4AADF130D94A4FDF5CF206AEEE,
	VectorImageManager_set_fontDestinationFolder_m4C3A650BC9A681EA0D0BEC6ACD4B64D5C51CDBD1,
	VectorImageManager_FontDirExists_mBC2684C4A459A522DF9D76127A7D508D3569FCB7,
	VectorImageManager_GetFontDirectories_m5407B1B01F15BBC199DE021D2633E0828CB412AB,
	VectorImageManager_GetAllIconSetNames_m69F906F5577B03118E24CD387C3580CAB7558C4B,
	VectorImageManager_IsMaterialDesignIconFont_m846EA0561CF94EC46D25D6CDE89B245980B60A2E,
	VectorImageManager_IsMaterialUIIconFont_mCC274AA72860D7D95E45B06A23536E461BA52F4A,
	VectorImageManager_GetIconFont_mF1661A8EBF37E15FADCD951B4A7690704CC72682,
	VectorImageManager_GetIconSet_m048F90EEE183139E7A845F2F690947D9A658159F,
	VectorImageManager_GetIconCodeFromName_mFDEEB3408B9048D8F81A0D22D8D272CE5D507B21,
	VectorImageManager_GetIconNameFromCode_mFD6CF0E466CACBEE4EBA45D913DFC19968313EDD,
	DialogManager_get_instance_m22ED7F0A01751ABE570608AE12FB61756C3DF78F,
	DialogManager_get_rectTransform_mDADFA4E6948231FA946C06AA11AC88958DE79B5B,
	DialogManager_Awake_mC85DD11AAE18C922891C2E98A45BB0654591992E,
	DialogManager_OnDestroy_m781E83283D74E7199B766924D2A059419A3ADC46,
	DialogManager_OnApplicationQuit_m606F0CDC2A82A41DF345BC3F0BB28A71183865BB,
	DialogManager_InitDialogSystem_m21E369EBACA453F530B7E857591766E30252A515,
	DialogManager_ShowAlert_m2829121D289E78A92207395289B8F040E96FBA99,
	DialogManager_ShowAlert_m23B6EDACE245E1DDA81179452C49C60B8BC341A1,
	DialogManager_ShowAlert_mE4151C148D1295E7C62CFF96251B5BEC59C70C7F,
	DialogManager_CreateAlert_mA9763F548323FCD023772D166CA664544B5CC108,
	DialogManager_ShowProgressLinear_m11CF02166B721B6DF2BD0771019F8C4A4CD4E145,
	DialogManager_ShowProgressLinear_mB48E62492BD838CE1F4C58D66265AAABE3017C79,
	DialogManager_CreateProgressLinear_mBDF636C1BACA83AC4ECBE98762D041CE0A946F82,
	DialogManager_ShowProgressCircular_m5292B379D23DD26B8371A71539BC9D1D1BC20490,
	DialogManager_ShowProgressCircular_mA6F92A6E42790FAFC5DDA893BFE956308AD35EBD,
	DialogManager_CreateProgressCircular_m6FEB1A22C04557F20328D3DA72F43BC297698BC0,
	DialogManager_ShowSimpleList_mA63F80EC91E05E2CFC62524BEAE114B59281814B,
	DialogManager_ShowSimpleList_mCFEF0E0CF95D94A62121980E4B25D3179CAB2C7C,
	DialogManager_ShowSimpleList_mD6B7804759858040B45387E49A4D54239825C95E,
	DialogManager_ShowSimpleList_m717EAC54A8337787333AF80364E00CD9C7882B13,
	DialogManager_CreateSimpleList_m9842FF60A17B38AB45DBC0D498F833D166910AC0,
	DialogManager_ShowCheckboxList_m5B61D23A6FDF1E61171C4D27180E4A9CBAECB5C0,
	DialogManager_ShowCheckboxList_m7A06AA47DD966A444A0D8B8927A89626F4B7980A,
	DialogManager_ShowCheckboxList_m40DB453D26A379978503EA1C9B47505314D9ED4F,
	DialogManager_CreateCheckboxList_m2678974FFCEB77354FEA6C11A7FF201859C07A92,
	DialogManager_ShowRadioList_m73E13686EE5FDD0EFAF74914D389A136C3205C41,
	DialogManager_ShowRadioList_m478FCC175862370C2A78DB97BA100DFAFA1E8AE1,
	DialogManager_ShowRadioList_m5969ECF70E5AEBD7F8EA29FD953F6A577F234A23,
	DialogManager_ShowRadioList_m81E72E44D27E7C476D7E5231CC63D7C114F6DFBF,
	DialogManager_ShowRadioList_m0520AC54309AFD5D385BAE9DAF415D741D72F9E4,
	DialogManager_CreateRadioList_m5DF2264908C94EAEB7FD138593E1D6830040050B,
	NULL,
	DialogManager_ShowTimePicker_m935478769E5E541A1FE17D2852B34BF9F10E61BE,
	DialogManager_ShowTimePicker_m88B70FFAB385912BA010887F9E534834639D1FC0,
	DialogManager_ShowDatePicker_mF8B3FF5418B7F9893EA3BCA98C8F16C75C39E556,
	DialogManager_ShowDatePicker_m4135F30E11D7C74FBB108BE8FFD14E0C4C79ECE2,
	DialogManager_ShowDatePicker_m0DC469804501CDC5DCC7A89454DA50038834D204,
	DialogManager_CreateDatePicker_m595B917B30591F8AE3398CEBD30DDA43EDE2A224,
	DialogManager_ShowPrompt_mE0ED6CD86B71305CA27C51DC6F6FEE4C13E2C07C,
	DialogManager_ShowPrompt_m57C4FB44223F8DB78514085089971B797B3914E7,
	DialogManager_CreatePrompt_m399C6705B363ED50D0B764347AEEB482FA1321E6,
	DialogManager__ctor_m83CBCDA1E6A70CE6B695F064C8DA6373323F3C0C,
	PrefabManager_GetGameObject_mC174023A3A249D32A39050333D4C38E4DA975205,
	PrefabManager_InstantiateGameObject_m3D752D58512A2D0DD3AE11BF673F2F8B24A1114F,
	PrefabManager__cctor_m7915A436FFDF70673395E6BF16AACFD2EF25F17C,
	SnackbarManager_get_instance_m1C28645751BA8CCE73D7F7891BF58D1EE1C24D87,
	SnackbarManager_Awake_m31F47D9B103B3AD8903AB9C94091E78FE5C223F0,
	SnackbarManager_InitSystem_m1485DE07CA0FA0D3594A9D71E2614826F63157BC,
	SnackbarManager_OnDestroy_m97D8D653701E2851D851F66CC80C4B42AAD96444,
	SnackbarManager_OnApplicationQuit_mD5E6BE460A9B15E3328AE716EA16F1BC343F037A,
	SnackbarManager_Show_m5F28849C225070C506EDE516A721E5F83FE0687E,
	SnackbarManager_Show_m21AD84AD759E00B91ECA7AD94E46C1BA6AF657CB,
	SnackbarManager_Show_m750E1DDFE6F9E9225A9E661E152A34E9916D1C0B,
	SnackbarManager_Show_m533CB17677D274426361DFEFB95A6AADB2EDB433,
	SnackbarManager_StartQueue_m327D02E6C87E96B35FA936B313429D90FCA3F820,
	SnackbarManager_OnSnackbarCompleted_mE058A637AFBF7A7C1A253E4D82296F70E8611205,
	SnackbarManager__ctor_m278B4BC398CA439DE589750D88450D4F137FC58F,
	ToastManager_get_instance_mC43DA8EC276470007603B6731535433B071044E6,
	ToastManager_Awake_m1B6CF9E9DCAFBE4C39533AEB830D6D0A17A4284E,
	ToastManager_InitSystem_mABCD74DFC8931B93117234E7F2551FCF6C7AF27E,
	ToastManager_OnDestroy_mC3B7DDDE805D7BDB1F6A6205BE0AA7EFCBF264B7,
	ToastManager_OnApplicationQuit_m91B9773FCB5C3802745D894AD6191A4480E9BF20,
	ToastManager_Show_m39893BEDCA74BAAA061846AF9CF699991E02A5E3,
	ToastManager_Show_m51A22CE667803D12627E244AF40C445EFA963003,
	ToastManager_Show_m303D0CDBBB576A7F1EE94375FC3CDDBA5E97A879,
	ToastManager_StartQueue_m595D629590B1C550FC30DD772963EB96EC8C20D7,
	ToastManager_OnToastCompleted_m529657B7FDC4B4BF1CC0CF8BEEB596D46F328542,
	ToastManager_SetCanvas_m92C8B8640A8014E95A7047BB6D85F6D980EBFD73,
	ToastManager__ctor_mAE31DC367E893A31CAC02F1CB74919E30145CB25,
	TweenManager_get_instance_m9A152A1F20DF156747878FD98A199CA2BDE7DAF2,
	TweenManager_get_totalTweenCount_m9CB02CC7CCCF147B13C45DA2A7782A5A86817640,
	TweenManager_get_activeTweenCount_mB4280A182522F9E76D0A61AEC7B756950AB7F354,
	TweenManager_get_dormantTweenCount_m9069304026F9F77A3491AEFF45D2BD03736CA996,
	TweenManager_Update_mD3BD66C4B074527EC9640062DA9403B8F1491CB5,
	TweenManager_Release_m1F2695AC40D9F706C184D63A70EE58BDF4730892,
	TweenManager_TweenIsActive_m0BA0CE7E131F1DC8BCC44978F5E0A65A20F95BB8,
	TweenManager_GetAutoTween_mA81DFB7079BB217CD92BE998543A1A2DF3871863,
	TweenManager_EndTween_m20AC8289C4096080AA5CBF7CA22F7BBC849E0D4D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TweenManager_TimedCallback_mB541383FE665EF815BA9F53A7EF7D62FA1CC3B4E,
	TweenManager_TweenFloat_m744720F392448C4BDC8CE03C7A79CF349C73234C,
	TweenManager_TweenFloat_m9B007ED204004C43BA4F3A720971050556FD75E3,
	TweenManager_TweenFloat_m2B9C33285BEFF96BF956A2EE76D89E446270445A,
	TweenManager_TweenFloatCustom_m88A39A8536E732814AA1E2325B8433E61CC5D330,
	TweenManager_TweenFloatCustom_m4ABB23405A0CD4F140E6564D6B928FCA7EE3B599,
	TweenManager_TweenFloatCustom_m5E07C267F0571CE1B03D459687DA50D5CD7812DB,
	TweenManager_TweenInt_mE32E0068F383CD490B0DC06CEBAF757E28BC3054,
	TweenManager_TweenInt_m9D25F498E087B63BA35480956510A0F2C3D476FA,
	TweenManager_TweenInt_mD6B444B938FB0774D94883922F093F818B4BB1A1,
	TweenManager_TweenIntCustom_m51A7D57D60EB2E5AC432CC6E0E95124BBB210D49,
	TweenManager_TweenIntCustom_m7106C9D654DE7835028AAC6EE758AECBEEB4902F,
	TweenManager_TweenIntCustom_mD01C5F0C7F2BEA422669A56A60F002E57DAFAD3D,
	TweenManager_TweenVector2_m02EC0A8881E671F79B8401E037100F84720AE07B,
	TweenManager_TweenVector2_m0ADCB30ADAF136F98EF10719E446414988B8D893,
	TweenManager_TweenVector2_m7AC80E782CF3CC8145C07049614601066A83E576,
	TweenManager_TweenVector2Custom_m8B6B7E0DE223C9033950B6497ECD76BA23248BC2,
	TweenManager_TweenVector2Custom_m2019BBE13640FD218551FA3BB20A430F8C4495DE,
	TweenManager_TweenVector2Custom_mA1F1CDAC17E81E8FB15ADAEFE6236815AAD67DD3,
	TweenManager_TweenVector3_m4315F197A1ADD1ADFEE398F2C7C3C5E870D7136E,
	TweenManager_TweenVector3_m38451C5A3A522A56B65C6A27EA9A47129F2353EA,
	TweenManager_TweenVector3_mC487EB784CB701227D4CA7B63475E2BDC4FC0FDD,
	TweenManager_TweenVector3Custom_m37C5D0C68E09F4EDD93505FB25E46C038129E861,
	TweenManager_TweenVector3Custom_m047DF0EE2B84D60E7321E52015BE762C702DA44C,
	TweenManager_TweenVector3Custom_mDC8FEB45BDCC6118DCD88D5557C190AE909B1FCE,
	TweenManager_TweenVector4_mC1D01A0A744E7A5D40DA697D5B992B7C5DD927D4,
	TweenManager_TweenVector4_mD0670D086735CD0801A03CBBBA794A13FB678C14,
	TweenManager_TweenVector4_m44B74C219D06EAC08F466739099D9E6328D5B9E5,
	TweenManager_TweenVector4Custom_m035E503686E0E9EE57E031BC3BDEE3FB55FD73F8,
	TweenManager_TweenVector4Custom_mC40DA5DABAE59B13393171741BAD6CA5387A96F2,
	TweenManager_TweenVector4Custom_m79B26099141901B996A541C63E186C08035BBF98,
	TweenManager_TweenColor_m4EC58BA73468504FA55F0B4DF9932150CC147E84,
	TweenManager_TweenColor_m30E4A7A9B9E703687677F16E4D3992850019D9BD,
	TweenManager_TweenColor_mB0CE851306D1B6B2D904E3D07F203F1D8E640AAB,
	TweenManager_TweenColorCustom_m92565E6AA5925EDE0227DD21D3B8C6E485217594,
	TweenManager_TweenColorCustom_mB054D6306EEF0DCAB19FE642A97991A1CC37203A,
	TweenManager_TweenColorCustom_mB639D5D8D84221742814917293ABEB36A12F9B46,
	TweenManager__ctor_mAF7C25C4540A9388A5659936D768232DA2269140,
	AdaptiveUIGrid_OnEnable_m1274B5D3FC6819F94D2B5C7B96F98A6FEC4B9E72,
	AdaptiveUIGrid_GenerateGutterTexture_mAB3F215F5F758DD364408012F9A2706B9E1B8B91,
	AdaptiveUIGrid_OnDrawGizmos_mE6366281F527267F23D7E40D1EB686A6489994EE,
	AdaptiveUIGrid_OnGUI_m22AF7E148EB16A27008F7784E1EB3BEA06CB4A56,
	AdaptiveUIGrid_DrawGrid_m6BC378B6262A4540B5BD55AD20831A66B521A316,
	AdaptiveUIGrid_DrawTexture_mB074BCFB4F5D7559B04E168DFA98B484E0DD8BBF,
	AdaptiveUIGrid__ctor_m848FC68562546EA1AE77B9F71EA860936BDECBB9,
	NULL,
	TransformExtension_SetParentAndScale_m0E90D7E926614408BDBFCBD00F263EC6934ED91B,
	TransformExtension_GetRootCanvas_mC59DE68EF3C0A27C97FF2BC93ED27F5E7CA78B99,
	CanvasExtension_Copy_mCB5E802C66834AA3AB3378ED8E3A51FF7B8D5484,
	CanvasExtension_CopySettingsToOtherCanvas_m51D55283246266D95D2E954CA7EA8907ED900795,
	ActionExtension_InvokeIfNotNull_mC655B3DD6C373D3ED7F6D9E923545C3B3837F00E,
	NULL,
	UnityEventExtension_InvokeIfNotNull_m02B03D6CBB3849775E75617F0546BF081DF659D8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ColorExtension_WithAlpha_m9A1BB3387063A83CD2B74A668A54EAA666D514D4,
	ColorExtension_Approximately_mB20B67B8D7520676264FC987B10AD0E78D8F3AA5,
	RectTransformExtension_GetProperSize_m30C93C3C7BE6E504FDB9DDEF2C097250D380CCF2,
	RectTransformExtension_GetPositionRegardlessOfPivot_m6890137D5D55BA8E20567F96350DA9FBB367E332,
	RectTransformExtension_GetLocalPositionRegardlessOfPivot_mEA111DE1457830D2B209B0E9ED013FA2483429AC,
	RectTransformExtension_SetPositionRegardlessOfPivot_mF6A232F9C6981A7DCFFB5057AA6A728C7726ECC4,
	RectTransformExtension_SetLocalPositionRegardlessOfPivot_m8E19A703C4F6742768A53C26B73E1457815B9C8B,
	RectTransformExtension_SetAnchorX_mE90F60D541391DC605A7B9F02706E905AFAAFE87,
	RectTransformExtension_SetAnchorY_mF2D505605C4E66FC122BD2027CEF73DDC5731399,
	GraphicExtension_IsSpriteOrVectorImage_mEBF55B77068CAD11EFCD66789A339F8DB4E63458,
	GraphicExtension_SetImage_mD6F432644C5B0B6DA6073407A236715DB7D403B6,
	GraphicExtension_SetImage_m611A2F3D4A72A88B74B7591E6456317D8E77EE30,
	GraphicExtension_SetImage_m13582C55F3DE6C1D552FB741A3E4E72406E574C8,
	GraphicExtension_GetSpriteImage_m9163D20201F6B1FBB01861BA5C7142DD8BC2E134,
	GraphicExtension_GetVectorImage_mA29743B009A6BE5BC5DB2D6FA768F30BCBFA7D6B,
	GraphicExtension_GetImageData_m6FE79402EBA4D3D7B70A76FD72C2160F5EE63D41,
	FPSCounter_get_updateInterval_mE89AA3AFF6A42E34E1859FFF7B28497AACAA89AC,
	FPSCounter_set_updateInterval_m65437A1F890446AF771664FDB04E56296CB5372F,
	FPSCounter_get_text_mFDF23AAF363D27649BCF70AE0386FF07E7044A60,
	FPSCounter_set_text_m8A016248AAB9C92CB05EE1F1E65C3299EB65EEB4,
	FPSCounter_Start_m912D9D3FBAF682073418FFBAD0AEB95B2007F46D,
	FPSCounter_Update_m34039710F4F1BD7CAD483868D1AF75E758217464,
	FPSCounter__ctor_m709BC45C9391B25E97ECA17F009D475157419FD7,
	HSBColor__ctor_m772835AA5BD96FF559F6019FBB5C8E4CB9286AB1_AdjustorThunk,
	HSBColor__ctor_m976D67139060126C84DA81EA8C830A4CBD158E37_AdjustorThunk,
	HSBColor__ctor_m2A5BB5E6CCE9F8FA86C7BBC1FB379C0E09BAF854_AdjustorThunk,
	HSBColor_get_h_m33B7AB6E834B2D7B8E0A0A5E9929C00B4BF468F7_AdjustorThunk,
	HSBColor_set_h_mFBE97BE339ACFFA77E657CE3B53DEF7AE42B1815_AdjustorThunk,
	HSBColor_get_s_m53DA357D05660713B04F089D48BD6D49FBDB527A_AdjustorThunk,
	HSBColor_set_s_mDD7480DDD5DFC4C9D9A8A4F550127742E9FBF8E2_AdjustorThunk,
	HSBColor_get_b_m4C70FCFCC2587E4A05B499E8B8A915F2B10973E0_AdjustorThunk,
	HSBColor_set_b_m588E9E39867FC93E6333A50759D0CF16A63E0B3D_AdjustorThunk,
	HSBColor_get_a_m25C5F3835222CB3B4B9A0CE548D81133FA5E4F67_AdjustorThunk,
	HSBColor_set_a_m6051236D225BFD7ED3CA7C7AAC649B92E68AE096_AdjustorThunk,
	HSBColor_FromColor_mBCDCE686EC0884660A71148D69C7CE8EEF53B3DC,
	HSBColor_ToColor_m0D24B6C1262E9E4DD0F50F07F09D9A044735020C,
	HSBColor_ToColor_mCC905901A4D1116304060F3F3A0A3BA82C863C38_AdjustorThunk,
	HSBColor_ToString_mFB2FCA5179D3683853B9EBDA16A07CBB809BC382_AdjustorThunk,
	HSBColor_Lerp_m4DC6E58BF94A442B956F856BE230579CAF8CB3FE,
	BaseTextValidator_Init_m601DB1ACAF56B3074C2E5A11508778D53AB447EE,
	BaseTextValidator__ctor_m2DF8A7FE53ABA3464F20F1BC624445438BDB9E34,
	EmptyTextValidator__ctor_m7FD0CB3A0250F7EC5C77721E06FBEFB194992F55,
	EmptyTextValidator_IsTextValid_m25E9C03911DB607E2BFEE1D46290009125FF4006,
	EmailTextValidator__ctor_m057A098D1035CAE5DE214A3B738709B4CB86044C,
	EmailTextValidator_IsTextValid_m01DC9082E6B99FEDC9CCCE6956ED35D53A1A4872,
	NameTextValidator__ctor_m5E2E64F58A8521815462BB126EEE988158D3F962,
	NameTextValidator__ctor_m2749FC10ACBBA4AF162657527CF12984EAC68EA7,
	NameTextValidator_IsTextValid_m023C4FBDBB17EE6900BDE1A2932721154712FF52,
	MinLengthTextValidator__ctor_m7E3AFC87545FF4F8D94C9BF9AE15E4CAA109414A,
	MinLengthTextValidator_IsTextValid_mB5D4DD99254B3F58B8769D500053FBDBC2D01A5C,
	MaxLengthTextValidator__ctor_m2EB16AD4733BB40820FE5B99BC24B68D3AB7CC27,
	MaxLengthTextValidator_IsTextValid_m358B3DF61484DE7348AC1CC983958B7777C827EC,
	ExactLengthTextValidator__ctor_m846F2C99D47C1FB52A39068617FA604F7396DB36,
	ExactLengthTextValidator_IsTextValid_mE36FF8DEAA07ECF6CDE769AE434329944B409C81,
	SamePasswordTextValidator__ctor_m743CA8EC5D68B8435172D7649D1AC824BFCB5CC5,
	SamePasswordTextValidator_IsTextValid_mD5388AC31FCE886FA5DAFCD70E14C5BEC5464F23,
	DirectoryExistTextValidator__ctor_mAE2642639568EF5E4AB2AE6F6BBC73BAB2A8115E,
	DirectoryExistTextValidator_IsTextValid_m23AB3899784DC71BF49A6DC705C534211EE806E4,
	FileExistTextValidator__ctor_m05DC492B9CAD42DF9231B23921B7D75AA91C3CB2,
	FileExistTextValidator_IsTextValid_mD9F288687B716DFAD8A9F87557F23E7116D40D1C,
	ButtonRectInstantiationHelper_HelpInstantiate_mBA32856DF39D65E79F412D836FE5D80E50535220,
	ButtonRectInstantiationHelper__ctor_m3BBCDD1C6F1EA87C0FD917D3B3BE6A82C6C56A23,
	ButtonRoundInstantiationHelper_HelpInstantiate_m1156A7EE075B7851B974A27CA095634CB013070B,
	ButtonRoundInstantiationHelper__ctor_mBC333F2EC9A2E7A342FA7909725A797D6504F7FF,
	CircleProgressIndicatorInstantiationHelper_HelpInstantiate_m1F5D1BC244DA5F7E6A2FFED75E9D02E37D8A7DFA,
	CircleProgressIndicatorInstantiationHelper__ctor_m4C4D53CC3604A7DE4AE6EA6E78FEB22EA1630558,
	DividerInstantiationHelper_HelpInstantiate_m37B29EEE9FE1252D4ADFB786876DFBABEEFEBC4E,
	DividerInstantiationHelper__ctor_m39C51410301BFA7642CD0517CBA3E3C97E202766,
	EmptyUIObjectInstantiationHelper_HelpInstantiate_m04A15C9619F4A8BFCBD22C14C4852832EDD6DA6E,
	EmptyUIObjectInstantiationHelper__ctor_m1248609778688DC36FB58DDA46E55230F62D2D50,
	InputFieldInstantiationHelper_HelpInstantiate_mD88ABF56B1B849BB9EFF257B34DDCDC6E0806429,
	InputFieldInstantiationHelper__ctor_mE73C7253748EEABD715A556C582C3F120DE705B2,
	InstantiationHelper_HelpInstantiate_m23A58367913DC3CE8CC1120511D16C953C7B14FB,
	InstantiationHelper__ctor_m2D3753FD7A095F02A7498C163E22814E814C8A67,
	MaterialSliderInstantiationHelper_HelpInstantiate_mCD0FF2CE291BB2AE41F19D0022C44296007C5526,
	MaterialSliderInstantiationHelper__ctor_m29A8CCE5A4C2C9DBFF3E99F99517BBFA2D4D4A16,
	PanelInstantiationHelper_HelpInstantiate_mC248C7EC6C33690069FDC5B43F38F899ECC5E26C,
	PanelInstantiationHelper__ctor_mD022181B71E5BE9C1E9AA19D592E5C77C9AEBC28,
	RadioInstantiationHelper_HelpInstantiate_mD07414F26B281B93BFDC2E9F5D6488868C05B29F,
	RadioInstantiationHelper__ctor_m852B28210C695BC3B34A6E450601D96A1E540E8B,
	TabViewInstantiationHelper_HelpInstantiate_m2584BA502A26A08DCB262B643A89CB65AC110B27,
	TabViewInstantiationHelper__ctor_m5EC7D4C89DB8F1AFD9CE655D29C0FAD19120DF4F,
	ToggleInstantiationHelper_HelpInstantiate_mB55FD3BCEEE9734E979D75592A09073766BE8295,
	ToggleInstantiationHelper__ctor_m795E52372D8C4E5AE28C7C678F171D5A734BE44B,
	MaterialColor_ColorToHex_mD323C0E6F28D8A29AEA9E779A789926AF702BFA7,
	MaterialColor_HexToColor_mEFC850CD0BC92CC61BB3B83C45AE06455442B26E,
	MaterialColor_HighlightColor_m80D7E786BC97914D7FB6A5C89F1FD14247867FAF,
	MaterialColor_LightOrDarkElements_m78B209D5C38FAAC439E0523F2FEEC59B7A14CFF7,
	MaterialColor_ColorDistance_m9BD3BEB242D9E6F112D5A6F722D77FF228C26232,
	MaterialColor_ClosestMaterialColor_m7D2BF24B2A2CABC5E3E34D81795680A90F61A96D,
	MaterialColor_ClosestMaterialColorSet_m4A9B54917ED7332935C6C121BCDFCA2FACA5F199,
	MaterialColor_OffsetShade_m33344B87553368932CB1CC25BA2ABFFA501482B7,
	MaterialColor_Random500_m065D6112F963670392819D590EA019BD965C00AA,
	MaterialColor_get_colorSets_mFEC6A736AB44A8229AD487FCA463B90A6429C695,
	MaterialColor_get_redSet_m4FBA4EE4FD2B30D45A6FA0990054E79D4D94E70C,
	MaterialColor_get_red50_mC1F3A822F07BE9A224B45BD7311B470AC2E14172,
	MaterialColor_get_red100_m5A902F6CE379D8BF3CDA90ADE9B0D2535F6835A0,
	MaterialColor_get_red200_m63D4B31459FD1B7B8E6F018BBAFA9A3B5CFEA74A,
	MaterialColor_get_red300_m2DA30DB6A9CEB819E16CD57D9C9A8FA0DDAA445E,
	MaterialColor_get_red400_mC2181312D6A036C50BCC113B4EB79DF06D571A4E,
	MaterialColor_get_red500_m4C6122F2225CD4C2C6669E0AC5FDED522527C874,
	MaterialColor_get_red600_m05CDADC3FE0CDB7CD6E58CED857B128475E22EE9,
	MaterialColor_get_red700_m0B39F82EE49F36CAE12EEFD898094507EA7E141C,
	MaterialColor_get_red800_m3BAAD19403134CD7957710BAD0F05C3200F8C167,
	MaterialColor_get_red900_m8DC522AE45240385DF8DB0C761C874B56E3D015D,
	MaterialColor_get_redA100_m2070DBF783ABA6A2424B5705F0AF18BC0A796A58,
	MaterialColor_get_redA200_m79A85B96434BFA9AFA99633B8BC18A700EFD87E0,
	MaterialColor_get_redA400_m126B6E80B12C9BFDF3606EE77F3B00F11ED8226A,
	MaterialColor_get_redA700_mADF55A59EDED941BFB3BE77702EE5B4F123425E5,
	MaterialColor_get_pinkSet_mABC51C7B6C981D987F4D50E3D9590D9AA5D55962,
	MaterialColor_get_pink50_mEEE568642CD3620D88AB21DA0D0223035A106FE9,
	MaterialColor_get_pink100_m0D22BCD015FEA9CF1BC5DE91A3359824CEAE0EE9,
	MaterialColor_get_pink200_m3E7237C90DD804390F2802F265E9E1F57787F3CC,
	MaterialColor_get_pink300_mB0A7C9DCBA61CC94BFBF9850206DACDD862BB4F7,
	MaterialColor_get_pink400_mEFBE6F67583C1C8061F76D82C6DCADB633AF979B,
	MaterialColor_get_pink500_mE5FABEDF9DB73250AF6D893D94460A5FA7CE4844,
	MaterialColor_get_pink600_mD55E447DAFE3A91ACE24FBEC86C5C4FF432ABD81,
	MaterialColor_get_pink700_m64774BFADD166CB304A24BBF33CC73BBFF4828E6,
	MaterialColor_get_pink800_m7539A468C120EF99319F82C6B3F3661C1AD942A4,
	MaterialColor_get_pink900_m5BF5D30F93FEAAE751DCA850CD70B9F1B3942503,
	MaterialColor_get_pinkA100_mF07F1125F624F27DC9D4BCFCF136B8C8461E9619,
	MaterialColor_get_pinkA200_m12E15292B94739E1C4EC91EA6421EA245CD2BA0B,
	MaterialColor_get_pinkA400_m3C915B5E21A0CCD2518D6735F7936D75DD32740C,
	MaterialColor_get_pinkA700_m02C62D9DF5369CF20812F60C4FBC6BAF7A9D7648,
	MaterialColor_get_purpleSet_mC6B7B4FAF82AC29555909B3835E3287C5C251B02,
	MaterialColor_get_purple50_mEBDC8DA27C472D63F93ED8F22BA73A631CDB010F,
	MaterialColor_get_purple100_m0D011D6656571A8BD5B0BA7FB9F903C82F3C6944,
	MaterialColor_get_purple200_m96AEA5FD6C4AA68269E75012E721A30A49B29588,
	MaterialColor_get_purple300_m9AE3218E492453574C23CBBC785D5F3DCEFB1A0C,
	MaterialColor_get_purple400_mB62DBD1DF2A2173DE866D872D6739C1CDCD19A66,
	MaterialColor_get_purple500_mBE59AF1CF61372072579F959F08869666B76DBCA,
	MaterialColor_get_purple600_m38B2963DC2B00ACE3810191983CFA64BAF454D82,
	MaterialColor_get_purple700_m4505B4A1BC82372DA8918B01CB1A15F65E11735F,
	MaterialColor_get_purple800_mD7B5B49F21D005C4AEEC2E8485765ECDA33C59D8,
	MaterialColor_get_purple900_mC4115C73F59667764062054AB0E34169E25945B9,
	MaterialColor_get_purpleA100_m76FD783DE8646CE0384FC4B12287D91EAD84F66A,
	MaterialColor_get_purpleA200_mAF0E0341045E54E276E3D55F109B256984388B7F,
	MaterialColor_get_purpleA400_mFF1A154B6DB3BE8E2EE6843FD2FFE0E94BEFC46B,
	MaterialColor_get_purpleA700_m42C7E6C1C19D833419B2A11AB4966C348379CB0F,
	MaterialColor_get_deepPurpleSet_m4EDF675DDEBF2F61F4B377D74815EF76EE330A76,
	MaterialColor_get_deepPurple50_mCE604EEE0942409EAD60AAB560A29A02C55D633B,
	MaterialColor_get_deepPurple100_m8F54C61BE0F1F9A5075660F7B575599AE37E688C,
	MaterialColor_get_deepPurple200_mFD540B3D3F801354CCCC78D2496FB8762C36ABA8,
	MaterialColor_get_deepPurple300_mAE14F120C9DD5D74107C3BBA3DE982D2C2D97164,
	MaterialColor_get_deepPurple400_m4DE43AA68E64C30EFD9FEF4FF2390AF28C284CC5,
	MaterialColor_get_deepPurple500_m0EA54BDA447A405025F7CD145276CECB06592858,
	MaterialColor_get_deepPurple600_mC20234C64814AFBB69AD654E03127720C30CC74B,
	MaterialColor_get_deepPurple700_mBA7B909A9220A01B3215234EA18B37A9DD9DE056,
	MaterialColor_get_deepPurple800_mA0C14ED4D2FFE4AF36C8BC646FF06E370763ECE1,
	MaterialColor_get_deepPurple900_m3AAAF826F8EB4446CB4B818A38001FD3FD5A2311,
	MaterialColor_get_deepPurpleA100_mFE8D39A80E715F3396D49F2D095DB0FCCBB3A1B8,
	MaterialColor_get_deepPurpleA200_mD6E4F1BBCDB3ED8CC35225A9A946F1FA4B60878F,
	MaterialColor_get_deepPurpleA400_mEC4C107494926F39432FF1EB39802E75F347D22C,
	MaterialColor_get_deepPurpleA700_m39ACB1C61B0D700FC7FE9ABB86A83EA1F3DD6835,
	MaterialColor_get_indigoSet_m778817FE00A889574C97CED25ECA076103E15C21,
	MaterialColor_get_indigo50_m30E1A0AD1E777573693249EB46A206654E46B0CF,
	MaterialColor_get_indigo100_mE53BF0A8884D500F889CC948B24135401755F377,
	MaterialColor_get_indigo200_mCB084E39A0CDF16492B8DDC13CE9128F0EA717EC,
	MaterialColor_get_indigo300_mA09FB010F478DA6F31BF93802B68D7BD1CA1F31E,
	MaterialColor_get_indigo400_m45FBD9349D613200A3C637497FB2061D2E85571F,
	MaterialColor_get_indigo500_m8AF9072DFCC9588359BD8BE85420429CCFEBE510,
	MaterialColor_get_indigo600_m4ED0ACB258B9EC713AEF370AFF1581C7724681ED,
	MaterialColor_get_indigo700_m0F9D42BFEAD6E34F8BFE31C68B886A9A374FA6D7,
	MaterialColor_get_indigo800_m533476A558D73BFB0D77741D3B53162E22A9236D,
	MaterialColor_get_indigo900_m7497C4729CFD91FF040FC36CCE900EB64EDF7060,
	MaterialColor_get_indigoA100_m2973710490C19DF288E798BDC8421EA38712AFD7,
	MaterialColor_get_indigoA200_m6B66B10EDD1CB874D4A27C56670FAF4CD469D411,
	MaterialColor_get_indigoA400_mD7D2C96A14266F2D11AC71B41BF8875AB65A6D9E,
	MaterialColor_get_indigoA700_m844ECE88DE125EEFF9DA8D1FC6C600B0246C3491,
	MaterialColor_get_blueSet_mDEB923A9CC934A11126BB2FC92DC78F0FEA16456,
	MaterialColor_get_blue50_m3EFA34B523B6D4FF418A4B33C3DE58A7A867CABA,
	MaterialColor_get_blue100_mF000256D400575720F78FBE18D367DD97B35A49A,
	MaterialColor_get_blue200_m487930DE4CFAC387BF6620413CF30CBB1558EE06,
	MaterialColor_get_blue300_m39850FAA6017D01F8D9DE096183FB3B170A08C8C,
	MaterialColor_get_blue400_mD549033B7178D2B9E917AA73A09C729FE38C563A,
	MaterialColor_get_blue500_m7B36BD820D5DE617F1FA1EC4B7C20979D31A977E,
	MaterialColor_get_blue600_m081858F60A782C6D97E036F5FC04099570D048E1,
	MaterialColor_get_blue700_m92D1F79CD3C3C29E14F08A6245743E479679838E,
	MaterialColor_get_blue800_mECB5BFB4BDEFB883880537EBAC3619A058E31594,
	MaterialColor_get_blue900_mFBF14DEB4F8AC051FF39D6EC62DFF3E10D243632,
	MaterialColor_get_blueA100_m96C00ED1931E421B9E40DAC7ADC78A50AB1288F0,
	MaterialColor_get_blueA200_m4E2C56995B3BD58299FA0B0F314F3E9A3C4358FE,
	MaterialColor_get_blueA400_m76C3F5615927C06008D53B98FDBC8DC880B07F07,
	MaterialColor_get_blueA700_mBA6CFDC470697853CD80ECFA45F215473056373D,
	MaterialColor_get_lightBlueSet_mE5A9AE8CCC82790784E7B0BB67DF24A575238E16,
	MaterialColor_get_lightBlue50_m2A1C047619EC7E47019AF210AE4359400A2EB2B6,
	MaterialColor_get_lightBlue100_mEC7111ECC649EB51A73EF216018556DD348861B2,
	MaterialColor_get_lightBlue200_m935A78268591EC2CEA3445A599CDB5231B48F3B1,
	MaterialColor_get_lightBlue300_m219C3F7508C394701AA5EADFA0993D660365583E,
	MaterialColor_get_lightBlue400_mC4BF862C492F7E162A9583E5018A5F9DB3844CB4,
	MaterialColor_get_lightBlue500_m6812A06A2B128E3A19355521D1EDD64F2F045BDB,
	MaterialColor_get_lightBlue600_m1CD706409E090D225BB16396240425F7BD6153B1,
	MaterialColor_get_lightBlue700_mB3AFDF626EB2AE98E9208BBA7C377D8DC42CF39B,
	MaterialColor_get_lightBlue800_m7909E760A3150FB4CD6E5257379666501895D10A,
	MaterialColor_get_lightBlue900_m989946385603C91C4E20A2469A5F04147BC0444C,
	MaterialColor_get_lightBlueA100_m06EE54795DD6175564182330C470D55FF36D8E00,
	MaterialColor_get_lightBlueA200_mCAAEBA8C4E2815C73F866E6AFF41F3BEC3A8C0CA,
	MaterialColor_get_lightBlueA400_m52F7F7C0F28FA29C9C77F65E2FA24EFF4762FCED,
	MaterialColor_get_lightBlueA700_mE137E14C9CDC9C4DFA27488CE8E6B4E37D6DE284,
	MaterialColor_get_cyanSet_mE82879D96F961D7FD1CB32B7C3757F438A687A7D,
	MaterialColor_get_cyan50_mB949CC1F7F89DDB82D6521CCC9685F4D9A1AF775,
	MaterialColor_get_cyan100_m1DE4C2C948FF35F1582C22CBE0306480AE60E665,
	MaterialColor_get_cyan200_m6B2251465184C11ACE05AC63DD2D88C9D61CF97D,
	MaterialColor_get_cyan300_mF5E13ABDA9747DA6F9D8A3ED21612BDE91793B51,
	MaterialColor_get_cyan400_mBBDC816895B33DE9D76AE15E0E0B070DFEC81169,
	MaterialColor_get_cyan500_m95E1261D8E7620DB4C12B4E531A034954E276BFE,
	MaterialColor_get_cyan600_mE72132DBA7810A265C23447D33DE06F2ECDE6F1D,
	MaterialColor_get_cyan700_m34F3E27F8286FA75C17F7A7B1BC1B3BA120BCFB7,
	MaterialColor_get_cyan800_mB90C903853113FECF3BA5AA8AB4F678E485F02B7,
	MaterialColor_get_cyan900_mB1643997366965BE2A3EF4CFAA84439DD21B18E7,
	MaterialColor_get_cyanA100_m6DB11574BF70230633F05E3AA153598207EF04C5,
	MaterialColor_get_cyanA200_m80FA3F29CC8DDA217BA47E6F56C4F6C23A8F38E8,
	MaterialColor_get_cyanA400_mD2C650C9D2050671C2618B6DAC516A81C65BA9ED,
	MaterialColor_get_cyanA700_m376C2EEFD9A9B7B17ABF16AFD77345FDD77A18B4,
	MaterialColor_get_tealSet_m455D6725636AB2DE35BCC4A4606B5CC95C5D5DBF,
	MaterialColor_get_teal50_m26DE4526E9F96EDACA843D0A94F8CE3126AE7394,
	MaterialColor_get_teal100_mA6F3C82F21FD29F43257650660A6622055E0134D,
	MaterialColor_get_teal200_mB3C62175E36F5321D06962BE3FB1B0B893C3C201,
	MaterialColor_get_teal300_m4B4DC2DB654C2092834A45CE045CFD083F1DE2D2,
	MaterialColor_get_teal400_mF582883C1AE2EE40CE49D86AFB84AC31720061C1,
	MaterialColor_get_teal500_mFB2EA7078E56DF7B8A0F9C91699B5B5A1493334C,
	MaterialColor_get_teal600_m54CB988684954F62104F5B0264639C8D4241CF51,
	MaterialColor_get_teal700_mBF03658D91AFFA71ECE40B0C6C997EE8E3B051FD,
	MaterialColor_get_teal800_m0F9D822F8B0BF6DF06283A24E5D8767C08D66A05,
	MaterialColor_get_teal900_mA9F9168FF0A04172169C7A92C026C38A9B3DA6D3,
	MaterialColor_get_tealA100_mE7D5AC541A997B4EB07FE12A2E93E8C718E56D8F,
	MaterialColor_get_tealA200_m0F9F8F6CFFFC420014A8F99B96CF3133CC59C6B7,
	MaterialColor_get_tealA400_m0906872E245C6E397963E1F71F50997DB2A66D7D,
	MaterialColor_get_tealA700_mBB4F2199C147D77C1D13AF5B3A671D7E241C0248,
	MaterialColor_get_greenSet_m8EDC5182EC5111ACEEF5B108C6581C02FD55F8BE,
	MaterialColor_get_green50_m911BAC8F5826EE311807BE5273240F266D5C4C8E,
	MaterialColor_get_green100_m92102FE960DC60BC9D63DCC8C72261A0A93FE3C1,
	MaterialColor_get_green200_m3F9ADAA188FD7D2303A8C249CA352FC40C7B8F27,
	MaterialColor_get_green300_m314AE96B35BB75F63929AF1FE783708B1DE332B2,
	MaterialColor_get_green400_m1B261146F6D3818B9C16CB65CAA8DDDBF41A037D,
	MaterialColor_get_green500_mD4EDB14B7D253057BD35B21791565251BD211433,
	MaterialColor_get_green600_m028972BE9342A397716BFBA0D04B6CC99E04CE20,
	MaterialColor_get_green700_mF1524F14175AED207BA9E5DCAC159002E8382BC5,
	MaterialColor_get_green800_mD63002889401B4B17463B41967424AEFC106705E,
	MaterialColor_get_green900_m1CE69E6FD779FE7639CE651A877B8B07C1B1C570,
	MaterialColor_get_greenA100_mA640F9AC43B84F6E24D18065753745590976D24E,
	MaterialColor_get_greenA200_m7344617CE54AB497D57DEFE9FC21A84D1841FB78,
	MaterialColor_get_greenA400_mC768CBF00C0ABE237FC82C90EE11C0EFE962F876,
	MaterialColor_get_greenA700_m977F5CDC1A82F7A838E1EC456CF472B1CF3AA8C0,
	MaterialColor_get_lightGreenSet_m46BC95BB5C3BA3944EC6ADC61F592387BE169BC7,
	MaterialColor_get_lightGreen50_mEEAB7334DA1446DBC4CC28BE5B3E37FF3BE7D8DB,
	MaterialColor_get_lightGreen100_m538BAF8F6AB5AB0735C0D7149E3693AF77F18CEF,
	MaterialColor_get_lightGreen200_m7B780A6D199028C3E7BAAB66A741D0874177B27D,
	MaterialColor_get_lightGreen300_m89199F853548EF2EF8F0290B9E56E8606A96A0AC,
	MaterialColor_get_lightGreen400_m35ED5523D2F3077DF05F560C458F3F1F1E2C2AFA,
	MaterialColor_get_lightGreen500_mAFB10CA5E7028AB093F8A599E23A1429C1775156,
	MaterialColor_get_lightGreen600_mF55EA343FD7E3A29A591DEA577FA702C81F47EDE,
	MaterialColor_get_lightGreen700_mBF13C128FAB03F2A66BBC15D04529D365C837F13,
	MaterialColor_get_lightGreen800_m5E15973E1C162DC6ABEB249DD8CD50831C3F06AE,
	MaterialColor_get_lightGreen900_m286D3FC9992DE58EBE0B90523AE8D246860BD511,
	MaterialColor_get_lightGreenA100_mC32494F0139C83F1320D68EA9A2880A21205C57C,
	MaterialColor_get_lightGreenA200_mBDBCCD697647D2F3E734F859B89606B8B89827FE,
	MaterialColor_get_lightGreenA400_m8016DE01BB2F56632197AB4C3495E1E7BB3F14E0,
	MaterialColor_get_lightGreenA700_mDF6332C3D0909DF61C32EE4329B28EA0A01A3195,
	MaterialColor_get_limeSet_mB6A27A38119C064E7F831303379D9DCA27DF72AE,
	MaterialColor_get_lime50_m144C40817CCA794374DB460D19DDD332E14E6E59,
	MaterialColor_get_lime100_mCE51652D619893F72F4257F63DE95F822CA6F3AB,
	MaterialColor_get_lime200_mE560A875D15152CDE68D1045847AB14365207E8C,
	MaterialColor_get_lime300_mE0AFF873667FC70088563A827D452134901646D1,
	MaterialColor_get_lime400_m6236D031ECFF485929F2FF958CCAB71D63C6B45A,
	MaterialColor_get_lime500_mA8894D1E8B98BDFEA4E12CE5B928B94FD88734CC,
	MaterialColor_get_lime600_mE8DFF78473C623FE9D9A304D503D7C098342E25C,
	MaterialColor_get_lime700_mA1210828EEBB2727EFAF2524FEEF91DC4656A277,
	MaterialColor_get_lime800_m794056780FF9E0DDAFD90FA887D76357F4C953F7,
	MaterialColor_get_lime900_m9FEE09DFF780AC2C7D7D95B29DE539E267E9A7B8,
	MaterialColor_get_limeA100_m16B2C148DC9DC4F85A90DDB26E1E1F94AE76EABA,
	MaterialColor_get_limeA200_m471BA6BD2D26CFDF662306D1D811218924CAB387,
	MaterialColor_get_limeA400_m6E55E37E778DE96702C674AFC10E81B4D5E86489,
	MaterialColor_get_limeA700_m5557797514A264DC862904FED0AA0DF701406DDC,
	MaterialColor_get_yellowSet_mC1E2993B63F0A50D91B0AFAA80DB5881F3F04DA8,
	MaterialColor_get_yellow50_mF388C22ED5732F843BAE75E22BF1239759D6693B,
	MaterialColor_get_yellow100_mE9522B786F2501A3998B626C00A186373F170BCD,
	MaterialColor_get_yellow200_m0F15EBF53FA70C6FAD586097E145FD5A119460DB,
	MaterialColor_get_yellow300_mE9E3F7CE241E08568CD2E6B46BAD09B6A019744F,
	MaterialColor_get_yellow400_m155634FF7BF88525D779F8942E438EB471FE844C,
	MaterialColor_get_yellow500_m184876D6952E647B02DCF994E07B8E6479C0A5DE,
	MaterialColor_get_yellow600_mCCC525E03C625D201026C41224231E03E782E140,
	MaterialColor_get_yellow700_m8EF1F3EB735CCDF56A87B6B2C334BEB0007F0923,
	MaterialColor_get_yellow800_mD9ED35F32EF2B7EF1DBFC3D0F346255A9E6533AB,
	MaterialColor_get_yellow900_mDDCCF6EBDF633FE4BF52DCA51D7B0AD018C428ED,
	MaterialColor_get_yellowA100_m16A1454C2C1AF7342E6524A8F2D6D145F069B94E,
	MaterialColor_get_yellowA200_m9CF4882B4BACCFBBB7DE0F670F3138EC20E92ABB,
	MaterialColor_get_yellowA400_m962146CFBC6ED1A92AA4965F239D494E1E23AA65,
	MaterialColor_get_yellowA700_m3A392A3641FCDCF772ADC8BD51A60BD3DA0B7880,
	MaterialColor_get_amberSet_mE5CA0FD1795B796CFD49A6A64C6799F1D8898C02,
	MaterialColor_get_amber50_m29B2DC6012CE85321A74B13608081322441EFEC4,
	MaterialColor_get_amber100_mD2A247D525F0B023D8D51B7C91A24537B6324AF4,
	MaterialColor_get_amber200_mF82C563A65B23D7C3FF575E6ECB20130D36C8E54,
	MaterialColor_get_amber300_m7EB50E2B61542D87004DBC318C822B6541EB522F,
	MaterialColor_get_amber400_mE7FC372641B70805285739ECA81582D8415C40EE,
	MaterialColor_get_amber500_m67801E7F39F52C37B66C50ED5D10548FF710CB46,
	MaterialColor_get_amber600_mA37C23C761B2525719161F2012B56C19A66D68B7,
	MaterialColor_get_amber700_m86B3F6C219C68DCB87CC68CCF8F2C86EEB5CFCEE,
	MaterialColor_get_amber800_m7B720C7FD9D8A9995FAAB908EDAC8EABA51DA387,
	MaterialColor_get_amber900_mFE0F10869212E53A4DE81D9FADEFD1E2E66D765B,
	MaterialColor_get_amberA100_m26B9C3474F715F74B22E273E2C83CA7B8FFCB98D,
	MaterialColor_get_amberA200_m63A6FA635EB76F61B6762F590B6EEA24FD68481F,
	MaterialColor_get_amberA400_m9D14E5BFD85B710816556A9C3C77B5ECBCC5346E,
	MaterialColor_get_amberA700_mF7213F85C239123A66A4912B05E1C3DE4D45E211,
	MaterialColor_get_orangeSet_m95800A61E8B9AB4E1B6C3411A5D4E464965C4D41,
	MaterialColor_get_orange50_mA7D9FC4E49DAD79833665ECEBF0F106B46CB682B,
	MaterialColor_get_orange100_m6592815A30E62D238B8DC71F66AF06E1B7E9E505,
	MaterialColor_get_orange200_mA311F4C6F269BF05622F79386F25E1CEE80289EC,
	MaterialColor_get_orange300_m4547220EFA3C2A05BAFB1132321BD1EF67FB5B0A,
	MaterialColor_get_orange400_m49DA383A952ADA0EE56EAB5212509E94FCEDAC2F,
	MaterialColor_get_orange500_m2195A678B68F941D1623855176512DDE84B9B6B7,
	MaterialColor_get_orange600_m4FBD12C4419FBDE09A28C33F6014842C58314311,
	MaterialColor_get_orange700_m8E015D3A2F5542849EB588148921F8CEF1FF9E5F,
	MaterialColor_get_orange800_mF710144EE6FD5A401541062374DD2E2B940A70C8,
	MaterialColor_get_orange900_m63BE33AA8F1D88D751CCAAEF29AB6358F8815596,
	MaterialColor_get_orangeA100_m40E94EC5AFE4868A4C589E17DDA77543FBEB9737,
	MaterialColor_get_orangeA200_m79E2AA105531B6C51C21998B49FE90C2644E225B,
	MaterialColor_get_orangeA400_mB51AE5A92153AC9C2B3D0200B868F9CB07C7CCC9,
	MaterialColor_get_orangeA700_mED213A63C3BE473D263D80138CA919A5779654DB,
	MaterialColor_get_deepOrangeSet_mC47709C121C72AF5E61E22DD18B09F00DD3C7A33,
	MaterialColor_get_deepOrange50_m4E27DF59AF9FE1B2844402E5F810A7901AAD1608,
	MaterialColor_get_deepOrange100_mFEC63CC28307C2EF1E73042B10E9BD00C8F6A5FC,
	MaterialColor_get_deepOrange200_mD3DAC9103CF558841816F023E1F3C970DBFB2BBA,
	MaterialColor_get_deepOrange300_mA80B6AD77C253D9FE2C58E3CAB6045A1B9A69BEE,
	MaterialColor_get_deepOrange400_mA72119B4EB016BDFEDA3A917078A9CD0FA463221,
	MaterialColor_get_deepOrange500_m07509EC1CF525878A94366632BA77BA8D50640F2,
	MaterialColor_get_deepOrange600_mCFDE75ADAEB2A007B2D0D772664EAAFC62BB5967,
	MaterialColor_get_deepOrange700_m465EA2F8DDC4A7F86084538E7423AF5CE73153F1,
	MaterialColor_get_deepOrange800_m332AC1B98425BFB4AD9D4A3F7AAF5D3D3F069F95,
	MaterialColor_get_deepOrange900_mB08D138B0593DA4D9C2D9AC8D249A1C13E3993B5,
	MaterialColor_get_deepOrangeA100_mC78455634E75A1AA602BDF80DD2118EC2B257727,
	MaterialColor_get_deepOrangeA200_mC9C6075004A7024FDCAD93F014C8A9E024D4B6C1,
	MaterialColor_get_deepOrangeA400_mEE163B046530AAC6D22C5D011011A8DBCA7E9949,
	MaterialColor_get_deepOrangeA700_m6133342287404F10F161DB04F58814CC6F94A0C4,
	MaterialColor_get_brownSet_mA6ED0DB4086455A71D747BEA55ADEC27B7593CE9,
	MaterialColor_get_brown50_mEDE9315DA77FE2C44B5B1AD880C6040A7D2A143D,
	MaterialColor_get_brown100_mAB6DE0459EF7F2D17DB2CF40A241C35FA2089F3E,
	MaterialColor_get_brown200_mF003F25EB252548A1907A44B635D44B4B5A8F9FB,
	MaterialColor_get_brown300_m98AA200E5F2BBE88D4AC5FDDBFC5B3ECE22CACE0,
	MaterialColor_get_brown400_m320A15878B6FB839570CF55A2305422D3CA61A9F,
	MaterialColor_get_brown500_mEB1750DE64F6F0B45DCDB5DF332F1D9DBB629FE5,
	MaterialColor_get_brown600_m2E532051A0FEE41338B1AFF1F552D4319CC71E49,
	MaterialColor_get_brown700_mC7EC73C6B0D81941445FB71ADFF722D2F6202592,
	MaterialColor_get_brown800_mDB2E765D880C08908120433E84E89B635DA5BD0E,
	MaterialColor_get_brown900_mE2C41A94BF3F7BC3F17E805B635329854F988070,
	MaterialColor_get_blueGreySet_mDFFF537AEF52077F5849D443A5F92E3979C2410C,
	MaterialColor_get_blueGrey50_mAEF651155746726758D83C0A742C055390769DC9,
	MaterialColor_get_blueGrey100_m73B5BED568510B350F211497771BDB485942C0EC,
	MaterialColor_get_blueGrey200_mC4B4D0190ED5BA514E2383D4874BAE2873C4AC8E,
	MaterialColor_get_blueGrey300_mCA2FCBF6D5A20C7A3C5A1E2948B47939FFC6B2C9,
	MaterialColor_get_blueGrey400_m0254E82B9A45AE32768082FBB77930469B8B4F6B,
	MaterialColor_get_blueGrey500_m6CCFA86892904A14FA130D1882F4AA8F67D34A40,
	MaterialColor_get_blueGrey600_m14FA4D93E3C9A7A3DC97C81750320F230E5C1EDC,
	MaterialColor_get_blueGrey700_mA00014C2865932E102048261CBB056476C2D778A,
	MaterialColor_get_blueGrey800_m03E9E3DDF9BF9DDEB7681ECE9DBAA2A8B8081931,
	MaterialColor_get_blueGrey900_m1359F2F0B21E5AE0DEAB855009136325C89C17AB,
	MaterialColor_get_greySet_m53EDFC208D71FB414DB38AD956B541679C3BAA8F,
	MaterialColor_get_grey50_m159A28A34EBD92DF24737631DFD83DA3A9F035DD,
	MaterialColor_get_grey100_m19F79899EB584B3ECEFAE64768751CA0B02D6468,
	MaterialColor_get_grey200_m947A94AC106A8789052E381CFD188168123990EB,
	MaterialColor_get_grey300_mA3833153DDB558A942F79908C25BA546863BA946,
	MaterialColor_get_grey400_mA45DF946E8678BFF3E8AA09C48867B97B95C7CF3,
	MaterialColor_get_grey500_m16D9E213BD57E5A1A04FC5010A5B6E992B6D4E32,
	MaterialColor_get_grey600_m95B26E6D3E8F2FB9B6BAD5D46870B3671001BCA6,
	MaterialColor_get_grey700_m964F61914E4C484B928616731DC24A1A7980373B,
	MaterialColor_get_grey800_m71864204E4E661A131CF8A0262DDE191FACE50A7,
	MaterialColor_get_grey900_m06C3FADD71EA559A6F2527E84F59107F2801EE96,
	MaterialColor_get_textDark_m012169304CE2197EF97994EE8AA5F928B6FEE3ED,
	MaterialColor_get_textLight_m629377FED1B4B75ADCE0A60B50708CEAD637814F,
	MaterialColor_get_textSecondaryDark_mBB97307B47AFE4144A3E98616B4F8C3006B893E6,
	MaterialColor_get_textSecondaryLight_m36BF646132A333ACD6155ABD3D06324EDD10A317,
	MaterialColor_get_textHintDark_mBA7C5D0D85B7323D39E6F40ED440816F121EAF73,
	MaterialColor_get_textHintLight_mEA222F1BF75B47A6EAE1357BE63C1B23E687361B,
	MaterialColor_get_iconDark_m1287A696051182AC94BDC0E9EFACE4742F766679,
	MaterialColor_get_iconLight_m8132EA5B320822A41D67033CBFE7C99D6164E708,
	MaterialColor_get_disabledDark_m42E1D2082F85FCAEFCEF7BBF1AAE0FBCBFAA4EBD,
	MaterialColor_get_disabledLight_mD43647D7BBD898D51372ECFE4218FBB1B23DAC78,
	MaterialColor_get_dividerDark_m7008AFFA8A93E007FBD8234D6013770337A5D884,
	MaterialColor_get_dividerLight_mFA7C528CDF0E2E8371D4FF60553DFC4D32EC0935,
	MaterialColor__cctor_m03618B8A3DF292C8761A494427ADCA36007A9989,
	IconDecoder_Decode_mCD51CC440E3477DF92FDFFB26A9736F45A573D10,
	IconDecoder__cctor_m91B85BCB52F443E2565788F1E00F2939A468BA16,
	Utils_SetBoolValueIfTrue_mB103E1205C482E2BECBE2CBF90F1266AD57F70D9,
	Utils_GetScreenDiagonal_m83D6DA09188FEEA63EDDD9DF6C310833BC730E95,
	Utils_GetScreenDiagonal_m601A7359E0D83F4020113FCFCE253416FF37C7E3,
	ObserveAttribute__ctor_m786E6FE614B4C5A0DD99F67F0C766C18F96249C1,
	PopupAttribute__ctor_m6370173710EC04E4814E9AD8A4FE98107CF97E1A,
	MethodExtensions_RemoveQuotes_m0A45D9C0EAFE4572A802D73C1061B1A643E14341,
	RootObjectModel_get_NodeId_mAB0ECD7CB7F64A00D2313A6F23703FD73472DFF6,
	RootObjectModel_set_NodeId_mD4299216768ABDCF0E7EEDA63C3105F1953A246A,
	RootObjectModel_get_PresentationType_m7E2477BA1380049E7D9C0E7D3295E2EAF097F03E,
	RootObjectModel_set_PresentationType_mFA0F03ED1118C178CC6A5954D273C197C45AE70E,
	RootObjectModel_get_MessageType_m9E0E46C473AB0242591C1476A828AE3C9E27BB15,
	RootObjectModel_set_MessageType_m8B14EE8D13A1953224B1B2FAF1A1E110EE7DABEB,
	RootObjectModel_get_ResponseField_m09366F7FFAC7B1E77F28B6E0BAFE23C2C5E6A961,
	RootObjectModel_set_ResponseField_mCFEAAF40F8718E13801B72DC4A9496C7F630E84C,
	RootObjectModel_get_AzureFunction_m8273AD4799022D28BFE4F898CDD434C77715FA66,
	RootObjectModel_set_AzureFunction_m540B40FBAF462385033BE67B29C27FC2D75AA092,
	RootObjectModel_get_ChartType_m9EA17EAF1E6BF132304D759CCBFA241FE38FABA0,
	RootObjectModel_set_ChartType_m024D530D40E8843893BC4F05387F6C4FFB9412A7,
	RootObjectModel__ctor_mABA6F6206DF45965F954F75EA680CE0CAD748015,
	Param_get_username_m9B67898E1D5A53366972F4D419A6F2907DAB333C,
	Param_set_username_m47DFA1551C7AF21F85E7B8AF0DC5667482F8F4B8,
	Param_get_packname_m0279216ADE8CD31E9211A9AEA89DEF84E2D5CC89,
	Param_set_packname_mB120077CF38EFC99DD9C967A38A9A8C7E8F01296,
	Param_get_queryname_mD448447BC897717C106CDA22F59ADE3D2651C5A7,
	Param_set_queryname_m3600723B2F314D4EA5247E10A454B764F624586F,
	Param__ctor_m160CF74A60840E5351A687C0290C2D42E777BA89,
	AzureFunction_get_AuthType_m366C325126730CF30BD5B3D842CCA039B9CDF30F,
	AzureFunction_set_AuthType_m89109D26E790A668E9A60BCEAB6D2E30A3C748E7,
	AzureFunction_get_RequestURL_m7A294224FA9593C26846C8548265DCC3E64C13AB,
	AzureFunction_set_RequestURL_mFDD34A7D1F3708476453A1948D5C045495944457,
	AzureFunction_get_FunctionKey_m830D04C07B69B128A3D4493DF8522D6BD3E0652B,
	AzureFunction_set_FunctionKey_m40338133C493B38BA7B1317C6CBA1C0811034A69,
	AzureFunction_get_FunctionName_mC98A2500CBAFB1B8A80719F5FF1C48F0E8E498DC,
	AzureFunction_set_FunctionName_m33D4E8D43420EFD1A3DCDF9E7865AB8BCAF6F74D,
	AzureFunction_get_Params_mD98613E9388D0F27AB9E52133A2430CD5281C6E4,
	AzureFunction_set_Params_m48179295EBAAD068085C83F8A949F20ACB6E5541,
	AzureFunction__ctor_m885B50DD6FD3F22470F4D0477BA8958AB41196E9,
	ProcessDeepLinkMngr_get_Instance_m1C7ED9BF8721C711BF4DCCED9A542D5521F7B5AC,
	ProcessDeepLinkMngr_set_Instance_mD1772020E3D01F1FDFB8FB5CBAAFDA39A1BF0522,
	ProcessDeepLinkMngr_Awake_m014D94E89B580FC209E61CC885EBFD701D3FBE9A,
	ProcessDeepLinkMngr_RenderRoomObJ_mC667BD98EB023911CCAEF15A436925E5B800ED82,
	ProcessDeepLinkMngr_RenderRoom_mF4F8EE32E922697E5835853B5098566FF0A4D08F,
	ProcessDeepLinkMngr__ctor_mCC4EB0B29DD959A72A67C5C20CD16E8F9392EE33,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m082960ACC7C4FD71A9A2131DE82E8147B002CE2C,
	U3CDelayFunctionU3Ed__6__ctor_mD360CCE2B6876E130EA9CFE00CCF5AB4C0415AA3,
	U3CDelayFunctionU3Ed__6_System_IDisposable_Dispose_m2E9D7D26F12DBE60AE806E44C96F1C9A0AFBBCFD,
	U3CDelayFunctionU3Ed__6_MoveNext_m9CB0E7F7963A4311450342F18613E03C971219B4,
	U3CDelayFunctionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A759CAE3225E79B58AEDE42D4A557C58879CE13,
	U3CDelayFunctionU3Ed__6_System_Collections_IEnumerator_Reset_mD6D45010081B0E1928694B244F9E8E188DEDD009,
	U3CDelayFunctionU3Ed__6_System_Collections_IEnumerator_get_Current_m21F3388D688B0291A485B77BFFEC791A7CFC0213,
	U3CPostReqU3Ed__4__ctor_m8BCAAAB2493A7D925150C66278E7CDDA020C6524,
	U3CPostReqU3Ed__4_System_IDisposable_Dispose_m8552799457FCBD3BD23DA788C21627643C6D223E,
	U3CPostReqU3Ed__4_MoveNext_mED2A7980C743BEDC2091C094C2C3AEF2D6A342E3,
	U3CPostReqU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05F86707AA6717C4ABED2E8F529C1C69D74BFCD8,
	U3CPostReqU3Ed__4_System_Collections_IEnumerator_Reset_mE43979B4BC71C9E79FEE52FAE5F1EF92B52A62B6,
	U3CPostReqU3Ed__4_System_Collections_IEnumerator_get_Current_m25F11D2D0EEB70D770C51660CF36CD7821B183DE,
	U3CGetReqU3Ed__8__ctor_m28118B5B585069D7F8F3A1813B9CEB6D5AAF672B,
	U3CGetReqU3Ed__8_System_IDisposable_Dispose_mAB1221A457E756973DF16F9BF7F2CFFC0869BABA,
	U3CGetReqU3Ed__8_MoveNext_m03FA422DC0727ADF61B8D6E08DAEC316E86A5622,
	U3CGetReqU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF3C1E18F79B952DE0320B39BDD02CD0842C91A3,
	U3CGetReqU3Ed__8_System_Collections_IEnumerator_Reset_mF9C2862616D2FFBD6D2D5504137EC1EE3C8A8075,
	U3CGetReqU3Ed__8_System_Collections_IEnumerator_get_Current_m39B7611399CEC839B2C999A579734E66D1E57134,
	U3CUploadMultipleU3Ed__10__ctor_m06D3B56CEA09F5669C4A43465FCB1880508CEB4A,
	U3CUploadMultipleU3Ed__10_System_IDisposable_Dispose_mC0F1F21AC0428632151641116E5DDC64171463B6,
	U3CUploadMultipleU3Ed__10_MoveNext_mC7333759E6F261F087C12B8398A2A7534B0EB5C3,
	U3CUploadMultipleU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m715061F2DF31681F5B5A59A5B68D829B78526FB4,
	U3CUploadMultipleU3Ed__10_System_Collections_IEnumerator_Reset_m65EC30DA5F8C72B38871966B5B83F346CF0A0A0C,
	U3CUploadMultipleU3Ed__10_System_Collections_IEnumerator_get_Current_m94859A019F3CA780B4B337C90D82559268EF42A2,
	U3CGetFileUrlU3Ed__11__ctor_mAEE3FAA626051F32715CC7293DDDECBDECE5A7FF,
	U3CGetFileUrlU3Ed__11_System_IDisposable_Dispose_mED43604786B4EC885DF0D69817ECBD774838F814,
	U3CGetFileUrlU3Ed__11_MoveNext_m9F4C764C6602EA5E907DCD011A6D8C2C3E7EDC30,
	U3CGetFileUrlU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1F28127B95DC44EAC230670EE2AD37D875D6D5F,
	U3CGetFileUrlU3Ed__11_System_Collections_IEnumerator_Reset_m870EB6DD83530A05E89E4DD41A403C879167569B,
	U3CGetFileUrlU3Ed__11_System_Collections_IEnumerator_get_Current_mBFF932F85B75E1D84B97451F1400FA2747449201,
	U3C_DeleteReqU3Ed__13__ctor_m5211A75B03D44129D5D886170EBF061F2FF9212E,
	U3C_DeleteReqU3Ed__13_System_IDisposable_Dispose_m6109D992D8F8D05F6AE398D98379324C20D5496F,
	U3C_DeleteReqU3Ed__13_MoveNext_m6440B2D8E47F0E9FC7F718039F39EF04A569DB52,
	U3C_DeleteReqU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22FE3C19043FAAF58E0A32D31027EECDBF73D664,
	U3C_DeleteReqU3Ed__13_System_Collections_IEnumerator_Reset_m15219ECD0530AB8BC764ECFB51EBB0BB9B534CD5,
	U3C_DeleteReqU3Ed__13_System_Collections_IEnumerator_get_Current_mDF160E969EA0D59143527D9210A5E61387C41533,
	U3CU3Ec__DisplayClass24_0__ctor_mD8944BEE5D8067BD0037429BC8342BAFF8FA37E6,
	U3CU3Ec__DisplayClass24_0_U3CRenderRoomU3Eb__0_mC499BA271045B15194C0EA137374764A7E5DF096,
	U3CU3Ec__DisplayClass24_0_U3CRenderRoomU3Eb__1_m96158D9BC3F6361E0136479AA7F7F9F76FC9E901,
	U3CU3Ec__DisplayClass25_0__ctor_m5D15F4EBF887E3A9B66715D64EFEE58E8F38B517,
	U3CU3Ec__DisplayClass25_0_U3CRenderRoomU3Eb__0_m0061CCD09FE6D4115ABF1FB33C198DC105C0757E,
	U3CU3Ec__DisplayClass29_0__ctor_mADE8DEA52C89DAE66645688B83B0AABF5C66A0EC,
	U3CU3Ec__DisplayClass29_0_U3CRenderTitleU3Eb__2_m1CEA2C4A9EBE3FE89D1CD4F911C953551A1296DF,
	U3CU3Ec__cctor_m00F951C0A9356AA1577DE7F4F0D3A395CAF69CBE,
	U3CU3Ec__ctor_mC56523C54D2645AD702B2B2CC48B93F9B59ADB0C,
	U3CU3Ec_U3CRenderTitleU3Eb__29_0_m1A4AB48044F03827AD9FDD8F8653DE17D986E5CC,
	U3CU3Ec_U3CRenderTitleU3Eb__29_1_mA175809561622227F7C726B1D3017EAE6073C60E,
	U3CU3Ec_U3CRoomsRenderViewU3Eb__30_0_mD8831F29B3106CFD0F3BF79CF4DF0FC5CFF9419A,
	U3CU3Ec_U3CopenRoomU3Eb__32_0_m0EA551B444782651AF9FF3DD8A296953A0D68050,
	U3CU3Ec__DisplayClass30_0__ctor_mAD7867C3720D2AA1385467A484E12599C40E4688,
	U3CU3Ec__DisplayClass30_0_U3CRoomsRenderViewU3Eb__1_mB354AEE4B125F99ED44848CF28D99148D4551A43,
	U3CU3Ec__DisplayClass30_1__ctor_m38197037B136250897B7427CEB636A3B2727921F,
	U3CU3Ec__DisplayClass30_1_U3CRoomsRenderViewU3Eb__2_mAA8CD92163251368146658A3378F7A0DF8DCFCBE,
	U3CU3Ec__DisplayClass34_0__ctor_mEFBAB32A244B20D743464E5E99F102B63A9F8B76,
	U3CU3Ec__DisplayClass34_0_U3CDeleteRoomListU3Eb__0_m098E88A527B15AB829B903E3DC8787D8A629A3EE,
	U3CU3Ec__cctor_m8DF4342C36C41F5C04F721524826E0C9A8D1C239,
	U3CU3Ec__ctor_m5C2551A553AB80D833244667085AFFC46DE1A306,
	U3CU3Ec_U3CRenderRoomObjectU3Eb__12_0_m3A3D691E4761FCEAE888BB48EF3445387A7ADBD5,
	Actor_get_mbox_m5BA24E43EF37B6E89674F0B238FB80181BFDFE97,
	Actor_set_mbox_m6A46636907977416D66AADE1564B24765CFFFA18,
	Actor__ctor_mB456A84F1494F15D72922DE8C858ADDA0209C690,
	Display_get_Username_m65504F10897013C56A66E256C0010C4E5FED4A0E,
	Display_set_Username_m760AA8C358AFAF94C2ED0FB77D6A4E7C702EFF64,
	Display_get_Domain_mD70D5E987EAC0C0DE5CBCA022F2DEA265DB7D545,
	Display_set_Domain_m652E07EFB3A1E91BC5799A0A5CEF9F608014A5E9,
	Display_get_Type_m0771BD1BBFCB38D8B3B5EBBCC4027947B5C2CCD0,
	Display_set_Type_m24FBBFFF4388DB70F52AE1B8E8AA2A5A1765FCAF,
	Display__ctor_m84BEB549B6DFE019BB5294D9640F2EFE920E4403,
	Verb_get_display_m88056E16B980985355C37CCE79D4427C5E5120E0,
	Verb_set_display_mFF7618CC2E4415DA1DDB35A9735AA8374F1A022B,
	Verb_get_id_mD5E8F10B8F2BB1A09590DC21E15AB1B55576F637,
	Verb_set_id_m7E78808817A2658BD09E75F41CDF484D253A2CD5,
	Verb__ctor_m1CFB28D89507D67F7713DDCCD1A236C63C7AA547,
	Object_get_id_m5B6CF1BE1333FB8189548D26F1C8A6D709C4672B,
	Object_set_id_m74234E18FA198B8827D836D3357C17BD830EEA5A,
	Object__ctor_m0D9B45E92A7F8E511C49877A2BD7239A318B631A,
	DataModel_get_actor_mADBADFE9A03A3E9518652BFEF459349E548E9D7A,
	DataModel_set_actor_m92F0A1CCF72835D50308414F3CD92BBB9DA2792D,
	DataModel_get_verb_m9AE1F34E985660E00D09001A62913530B61B3C22,
	DataModel_set_verb_m3A0CD9319DA89EC0B06AFE7785C746706D5E8C1D,
	DataModel_get_object_m757124F4E20673B2617B1484ECAC7035BE11FA5D,
	DataModel_set_object_m711B3D4B46158052E276E938D94B877DA18C54F8,
	DataModel__ctor_m4304E75B7C98FBF0CAFAE1D78B6B87F5E2DF5E50,
	NULL,
	U3CU3Ec__DisplayClass17_0__ctor_m104E2E0E93C4ADD4C21939E43CD448A2C9B077DB,
	U3CU3Ec__DisplayClass17_0_U3CUpateRoomDetailsU3Eb__0_mF439D16A24B9775B00DD4447BB3AC63B4F8D8A37,
	U3CU3Ec__DisplayClass4_0__ctor_m2EC6150ABD7BFC381C9E7E4B775CB9662BAD6822,
	U3CU3Ec__DisplayClass4_0_U3CGetIconFromIconFontU3Eb__0_mA7208A3AB0C7BC1779665EFB09A135855B89C924,
	U3CU3Ec__cctor_m1790DD2ECA68B3BA7A020611414E649920678643,
	U3CU3Ec__ctor_mC5B45206BF481A77279A84C119D6BEC10344BD00,
	U3CU3Ec_U3COnSimpleSnackbarButtonClickedU3Eb__2_0_m20117736ED22E5198639D5DA74727F31850EFED9,
	U3CU3Ec_U3COnCustomSnackbarButtonClickedU3Eb__3_0_mA4B64326D431ACAA5B4BBC24345B9040BD017902,
	U3CHideWindowAfterSecondsU3Ed__5__ctor_m12B6C6F2521697E380F54062203BF3AD8A28460B,
	U3CHideWindowAfterSecondsU3Ed__5_System_IDisposable_Dispose_mF7CA6E5C9070500D325AFCD96EDDD1F843C24AEF,
	U3CHideWindowAfterSecondsU3Ed__5_MoveNext_m5E202C26D21746905ACF710424E4BF8EF802F984,
	U3CHideWindowAfterSecondsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7723BEC00D7BC9EA6621D96A7ECB7EF5AC86FD48,
	U3CHideWindowAfterSecondsU3Ed__5_System_Collections_IEnumerator_Reset_m38A45590143A544FD9463F0D938ACF3F6762A5B3,
	U3CHideWindowAfterSecondsU3Ed__5_System_Collections_IEnumerator_get_Current_mB921526FC4A4DAE6E49394D7F35464B0F3FFE991,
	U3CU3Ec__DisplayClass7_0__ctor_m995D6CF811ACEA4F99C08787D38ECC8F9FFE301D,
	U3CU3Ec__DisplayClass7_0_U3COnSimpleListBigVectorButtonClickedU3Eb__1_mF43729E42673F0490929B4A4E7F95D18E9994AFE,
	U3CU3Ec__DisplayClass8_0__ctor_m5DB4AE4DD92B13303AB79BACE63B3D2778E82895,
	U3CU3Ec__DisplayClass8_0_U3COnSimpleListBigSpriteButtonClickedU3Eb__1_m3DACFA927719CC5B54658319C4D8CF06B7F6918D,
	U3CU3Ec__cctor_m26ADB8F2A075B1EE3EC9BA1A1D0C6CCD815F1B7D,
	U3CU3Ec__ctor_mC7D97BD561D058ECB1C2472C804B9D5180B9EA94,
	U3CU3Ec_U3COnAlertOneCallbackButtonClickedU3Eb__10_0_m78D47AB8D0A89C3E4913AE365297921B2B479DB5,
	U3CU3Ec_U3COnAlertTwoCallbacksButtonClickedU3Eb__11_0_m3132C30710BB3E939D058807B584674413BF6D8A,
	U3CU3Ec_U3COnAlertTwoCallbacksButtonClickedU3Eb__11_1_mEF8898A0A84196396AE15A0F22783B7B8856E9A3,
	U3CU3Ec_U3COnCheckboxListBigButtonClickedU3Eb__14_0_mA6FD074FCAA1D495A3DECA792952860746E7FE72,
	U3CU3Ec_U3COnRadioListBigButtonClickedU3Eb__17_1_m107A409FE98145BF66BAFF2B2EAC87EE63B40B93,
	U3CU3Ec_U3COnOneFieldPromptButtonClickedU3Eb__18_0_m6637AFBC4FCEA64C2ECF92679794A55B021B8B9A,
	U3CU3Ec_U3COnOneFieldPromptButtonClickedU3Eb__18_1_m27CE77DDA9AAB1B8BEBF3CA97A96962C56DD971E,
	U3CU3Ec_U3COnTwoFieldsPromptButtonClickedU3Eb__19_0_m68C1D992C6BF62D749CFDF77A9FE0C7C04D5BB7F,
	U3CU3Ec_U3COnTwoFieldsPromptButtonClickedU3Eb__19_1_m1273B26834082176CEA6F1EB5CF43B6D7BE99BA2,
	U3CU3Ec__cctor_m96EC7144DC87AFEF2531C1872238552C049843F2,
	U3CU3Ec__ctor_m8625221AC73F595EE90B38A2E0C1F9F27585AEFA,
	U3CU3Ec_U3COnTimePickerButtonClickedU3Eb__0_0_m5D8BB3CB988E592BFDB5DF550DB1EDAFE7E6A1E4,
	U3CU3Ec_U3COnDatePickerButtonClickedU3Eb__1_0_mD0320E02D837F37AB6935E03608230D43B09E65C,
	U3CU3Ec__cctor_m14F55AC3428BC22869C089F29DF32F5765972BD8,
	U3CU3Ec__ctor_m16811C3F529FB124BFE3C61CF87F8156D697A5F7,
	U3CU3Ec_U3CSimple2U3Eb__10_0_mF947AA11BA5AACB6F3C81511BDE8D869F733CAF3,
	U3CU3Ec__DisplayClass14_0__ctor_mFF6E605208EC19F64C1367D87696B73D3A50777A,
	U3CU3Ec__DisplayClass14_0_U3CIssueCoroutineU3Eb__0_mFF9DAD2D37F68A74D1D30290D9A5966E871430A9,
	U3CIssueCoroutineU3Ed__14__ctor_mC01A4B229FFA051D3BFAF5612E8448B7F1DBB3C2,
	U3CIssueCoroutineU3Ed__14_System_IDisposable_Dispose_mE3F549DCFFC54A1419EAC0B64742C7171CBDDC5A,
	U3CIssueCoroutineU3Ed__14_MoveNext_m4EEF3440A7392B857C17DD0F4F761EA721D41B8A,
	U3CIssueCoroutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D63A2AC7D57FE94B35AB01DC486D0C1A8F0D682,
	U3CIssueCoroutineU3Ed__14_System_Collections_IEnumerator_Reset_m77C6DBB0764589F07DBC1D93C30F843C3BB39DB9,
	U3CIssueCoroutineU3Ed__14_System_Collections_IEnumerator_get_Current_m34E251F4ABA18E51A66FDED6E68602A3B4A5D6AB,
	U3CU3Ec__DisplayClass15_0__ctor_mF57255707EA9485C80EBA3C3D30B85E5B62CD64E,
	U3CU3Ec__DisplayClass15_0_U3CWorkaround1U3Eb__0_mFD6D54D85C0164D7BC25804FF388E15AF2EAB4C2,
	U3CWorkaround2CoroutineU3Ed__17__ctor_m27070577FF40EBC117F3880F7FAB9394488B958E,
	U3CWorkaround2CoroutineU3Ed__17_System_IDisposable_Dispose_mD2BA7E50E4BD83171B2068C0E4FF245B2800B586,
	U3CWorkaround2CoroutineU3Ed__17_MoveNext_mD0AB09C1ABA46EC255AC46A2827F36C0E7DE856A,
	U3CWorkaround2CoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DA2847FE3EAD41FBD5639A409D6E065945FCE69,
	U3CWorkaround2CoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m7147C14CF3F89AA753CE06AD93FA25ACE0B1442F,
	U3CWorkaround2CoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_mB6DF43445D31CDDE78FE63F7819D7A759016955F,
	U3CU3Ec__DisplayClass18_0__ctor_m230FACE807584EDC858AF5534E1A7F51FC572E9C,
	U3CU3Ec__DisplayClass18_0_U3CWorkaround2WorkaroundU3Eb__0_m0AD415CF9BEEC3447EE3AA8E14D924113E15E348,
	U3CU3Ec__DisplayClass1_0__ctor_m2837CA93BA76BEA1BD6FF63999DE70C7ED15F4AB,
	U3CU3Ec__DisplayClass1_0_U3CDiceCoefficientU3Eb__0_mFBE3CDAD5A6DE64DECE4579D23C116832D7E0849,
	MetaphoneData_get_Alternative_mB7DCE3926C57C63BDB92FFA4A16D572220A20ED3,
	MetaphoneData_set_Alternative_m6E48BC766512AEE587E67802AF09B824ED28C1ED,
	MetaphoneData_get_PrimaryLength_m9C8B0E5C544B947612C7AA2C6FF107B2128C3B1B,
	MetaphoneData_get_SecondaryLength_m2CF482E0662748FFF71F51BB6812A5D55CFE2ED4,
	MetaphoneData_Add_m063CA3F76C76D075B469F9349BC7FACE77A54DE3,
	MetaphoneData_Add_m3C16654A0FD5C5959417727B43622DDBEEC8DBB1,
	MetaphoneData_ToString_m50EA25D33F652819761907A496CC276AC214163A,
	MetaphoneData__ctor_m0B206E6640950721D07F237B9CA093F3C9AD61ED,
	EasyTweenObject_get_tag_mB7F6F3B4A5F245D5649CF025A9E5093A5AF7D7CB,
	EasyTweenObject_set_tag_m4A7DFEA930D471F87967FEA683F33F2FB42EBEF9,
	EasyTweenObject_get_targetGameObject_m2E81D115122199C97E01E822FAE6DA33358BDCF7,
	EasyTweenObject_set_targetGameObject_m8A2978935CAA2130FE5CA7CF3780045A30D57766,
	EasyTweenObject_get_tweenType_mDB55CD224BD88CD8CF6B3A5099FF43249ECC28BE,
	EasyTweenObject_set_tweenType_m7DDF4A67C4B6A44836F0B2B349E1526B51797339,
	EasyTweenObject_get_customCurve_mE1EA3412B706357F2329857F525ADB0E36A4FF30,
	EasyTweenObject_set_customCurve_m78218F01DA677100919D4DD3E1817CA1FD0466F3,
	EasyTweenObject_get_duration_m0414BBEEEBC5D90970249D905E8BBD996175AD75,
	EasyTweenObject_set_duration_m3E5AB832D1FD0097C96BAF2863B176DE50B3462C,
	EasyTweenObject_get_delay_m124E944832DAD76ABD3822AC3A8596686399003C,
	EasyTweenObject_set_delay_mBB95AFA19446E69F2AC56DC6E4624A75E3C0BBAE,
	EasyTweenObject_get_tweenOnStart_m237F587F656041136EEBC233981AAAB6CB4AD55C,
	EasyTweenObject_set_tweenOnStart_mFB7B816E110F568143E5190DEDE00A3F33B37431,
	EasyTweenObject_get_hasCallback_m0DEC5BFA8CE503F21EDFEDDE43DA81456756DAC0,
	EasyTweenObject_set_hasCallback_mF00D299024C6C4F7F4C93A9A374A544B9345EF24,
	EasyTweenObject_get_callbackGameObject_m94C03DD13E6527C15AB9609DA8AC0CC4CFEE3B0D,
	EasyTweenObject_set_callbackGameObject_m00054AFED8B835F29020791CA5902A3F0A4F048F,
	EasyTweenObject_get_callbackComponent_mFF87264010BE6C7EB996439D8E467477000E32BB,
	EasyTweenObject_set_callbackComponent_m261A30F7329E76EE8CAC6E54F89FB00BAC706382,
	EasyTweenObject_get_callbackComponentName_mEC21E28ECE90A89F0324E47A31E92A8731D93C01,
	EasyTweenObject_set_callbackComponentName_m980B6E4DC4BBD5072FF36ADDCDADB9578E381EED,
	EasyTweenObject_get_callbackMethodInfo_m2FE080A708F82A8AD01B21BA7B70CCB621B30528,
	EasyTweenObject_set_callbackMethodInfo_mD63F3F285B73F8C23AC9864109B4C67486601869,
	EasyTweenObject_get_callbackName_m5D7BB57F83D96D04D7A4D8F46D9296CD4A3B2C7D,
	EasyTweenObject_set_callbackName_mE44918E296BDAB31B23E289649C64265D7DC70AD,
	EasyTweenObject_get_optionsVisible_mFDE15582E80A159EE0F2E6301AF63E81154BBD21,
	EasyTweenObject_set_optionsVisible_m0600F570A0A745B0B8164257C747866F6CBF9A2B,
	EasyTweenObject_get_subOptionsVisible_m92AEC389A821012D0CB70921159289A7550B67D3,
	EasyTweenObject_set_subOptionsVisible_m59A813D36638AB0512EE081EFDC509695115B73E,
	EasyTweenObject_get_subTweens_m753F140D1B884BE703450B74BC103D6CA32C1284,
	EasyTweenObject_set_subTweens_m65806B8EEE40627A5048D53DD7F6F7DFE9F35E81,
	EasyTweenObject__ctor_m26134C5A8BBCE86D816F2548515987FF5B48AAAF,
	EasyTweenSubObject__ctor_m91337A01947BC8BC8E7FCD819DDA2B2753B5FA99,
	CanvasAreaChangedEvent__ctor_m4B47E0343D9CE36876F2624E2DFBE1F77E7DE4DD,
	U3CScrollCheckU3Ed__87__ctor_m93041A2516A537D2D5CE533022B154E01FB82100,
	U3CScrollCheckU3Ed__87_System_IDisposable_Dispose_m68F97647762E3FD93FBF18E76096271F53D3DDE4,
	U3CScrollCheckU3Ed__87_MoveNext_m1AC9266F333B8F4B373188A80E2B698D6E858043,
	U3CScrollCheckU3Ed__87_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD1F945824F15275D1D4DF987FD51CFB5DFF69E8,
	U3CScrollCheckU3Ed__87_System_Collections_IEnumerator_Reset_m2BAC06A86295781D34E35EC601567200797CCC40,
	U3CScrollCheckU3Ed__87_System_Collections_IEnumerator_get_Current_m276EBFBECF986346CC62B421117B94C77AB3E968,
	U3CScrollCheckUpU3Ed__88__ctor_m2348E7E34C648EB5C794E6DFE5A3592E90E1093A,
	U3CScrollCheckUpU3Ed__88_System_IDisposable_Dispose_mF1728237B624520402C0FD2181BE4B9A4F2B4EFD,
	U3CScrollCheckUpU3Ed__88_MoveNext_mD0CB4B7A60AD6CA6DF7AEFB3B1E0FC3047F59A36,
	U3CScrollCheckUpU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5237017C88568EF6AE59B134F1AF99847C9D771F,
	U3CScrollCheckUpU3Ed__88_System_Collections_IEnumerator_Reset_m1BD1E0558471B053CCB14430A5EA841117081F2A,
	U3CScrollCheckUpU3Ed__88_System_Collections_IEnumerator_get_Current_mE4403AB1E675D8C58B20D56DA5F968D2C559270A,
	U3CSelectCheckU3Ed__89__ctor_m1B9C6DFDD4271ACD9AAEFC6E9AB55DEC5F892EC4,
	U3CSelectCheckU3Ed__89_System_IDisposable_Dispose_m27A55D0F23A580F785A511AAD1CB46EDC7EA9B7B,
	U3CSelectCheckU3Ed__89_MoveNext_m90633F7FDEE2E56F256FC3BA72782818A01F7E2D,
	U3CSelectCheckU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07F877A5B5580F6BF84F52883D92D261AAD73E03,
	U3CSelectCheckU3Ed__89_System_Collections_IEnumerator_Reset_m0A4A2F52E80347823C2FC3356BAF4A9B9DD52659,
	U3CSelectCheckU3Ed__89_System_Collections_IEnumerator_get_Current_m1EEF5059625583902A4233AFB5452FB1F2BB79D4,
	U3CU3Ec__DisplayClass9_0__ctor_m03C07A33B5D823C60094492F9E60804AADE837A2,
	U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__0_m6FBD666837B59BE0E9844B83EE4BAE8E322395A0,
	U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__1_m527F32F5310536F0DA70842483BB94CDFE9A6ED4,
	U3CU3Ec__DisplayClass9_0_U3CSetShadowU3Eb__2_m3E17B0A7F1F847AD641FB83BB7B35230D45DBDFC,
	OptionSelectedEvent__ctor_m7E28B0533205A202D02FE9C7B27A8C5F8BCAB9D0,
	OptionSelectedEvent_Invoke_m75921165B03B2D023EF938926EB0DC1DF45FF8C8,
	OptionSelectedEvent_BeginInvoke_mE717D1AFA0FD3DC9D7CF5AEF9F5E5A5238E60B25,
	OptionSelectedEvent_EndInvoke_mDA3FB3F0E657DA05C0E9C6D6B75457E3857017D0,
	U3CU3Ec__DisplayClass26_0__ctor_mFF214DCE157AF69C016BF8E8D3E9E77C5C838796,
	U3CU3Ec__DisplayClass26_0_U3CSetButtonGraphicColorU3Eb__0_m2B651E1927CBF59ED54DCCC04155AC17159DE12F,
	U3CU3Ec__DisplayClass28_0__ctor_m9927BA2CAF233C9944282F9662A7A9A4304DC082,
	U3CU3Ec__DisplayClass28_0_U3CTweenGraphicColorU3Eb__0_mF6D6068F2C6B2A7D107553B9F5F54561B91C5D37,
	DropdownListItem_get_rectTransform_m0A3AACF957244A012D5FF8C92F2E3E2A523A76EE,
	DropdownListItem_set_rectTransform_m26AC866743D3718C9F6C5C026B128634A4700605,
	DropdownListItem_get_canvasGroup_m80A500FF71E0A28B01C91B87B489D9D2CD669AD9,
	DropdownListItem_set_canvasGroup_mDD5028057F5453E90F936441DAF72B1AB2F8672E,
	DropdownListItem_get_text_mB385D9F41B2A2A6887917F07CBE431F1EFEBD19D,
	DropdownListItem_set_text_m78C44D3B233933D98E56D6CFCFDA5EACAD1ABF9C,
	DropdownListItem_get_image_m5259F5FB879993432D0389DAB609076DFBE5D5F9,
	DropdownListItem_set_image_mA3241B16B6DF60753316AC0E1B905CF6DDA509CB,
	DropdownListItem__ctor_m4AE51D9DF3181E8AD9C54EF9EF6C85A5C9C8C4FB,
	MaterialDropdownEvent__ctor_m78433955EB50200DB3D7377B2FABA811278EA950,
	U3CU3Ec__DisplayClass119_0__ctor_m98C6CF07209D33159F9B8FC8E39A2C1248E2A935,
	U3CU3Ec__DisplayClass119_0_U3CShowU3Eb__4_m60149DA2754B77310A15FBE7C7EC1D96137744AC,
	U3CU3Ec__DisplayClass121_0__ctor_m9876C9DD88945A400A8CFFBA3B13C1862749DF5B,
	U3CU3Ec__DisplayClass121_0_U3CHideU3Eb__3_m435B4AE95D8FA2BC085D6A66FD0DC9495F89F82E,
	U3COnSliderValueInitU3Ed__153__ctor_m7F1D86BDB11478FD11BA8E56B0B2ED1313642760,
	U3COnSliderValueInitU3Ed__153_System_IDisposable_Dispose_m7416FF901C6E466FC250131C5F59FE6CE0223395,
	U3COnSliderValueInitU3Ed__153_MoveNext_m9EE1C81D2B8794FE961873153F2C510C562F88D5,
	U3COnSliderValueInitU3Ed__153_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84D9F0CC64EEF54529870C623F2A35707865F9EB,
	U3COnSliderValueInitU3Ed__153_System_Collections_IEnumerator_Reset_mCB5872AED1977316436CD0C04909E9CD284AC21E,
	U3COnSliderValueInitU3Ed__153_System_Collections_IEnumerator_get_Current_m0ACC57CDBA987EFD3C76EFFD6A219B708DFDA907,
	U3CGetDelayedSizeU3Ed__38__ctor_m40F5922DE4DAF7B8E3638675A91ADE7D1C76380A,
	U3CGetDelayedSizeU3Ed__38_System_IDisposable_Dispose_m5BBA2330173A126A1877E1970CFD0D65AFDF7E07,
	U3CGetDelayedSizeU3Ed__38_MoveNext_m199F04D577B4AE9CA38A04FF326256FFD71A47AA,
	U3CGetDelayedSizeU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAA6C7EDFCFBDE1D5E1766526DC49D7FB38A036F,
	U3CGetDelayedSizeU3Ed__38_System_Collections_IEnumerator_Reset_m6F1C0BED5594727D0D5DE962BDAA994090772AA4,
	U3CGetDelayedSizeU3Ed__38_System_Collections_IEnumerator_get_Current_mE6693B9C174CE84C8558C5BBE491F21E16277DB5,
	U3CU3Ec__DisplayClass67_0__ctor_m8CFFFEE81BECA1E2B3F4969336AF0C78007BA2EA,
	U3CU3Ec__DisplayClass67_0_U3CShowU3Eb__3_mB98B5A04445F624EDC687EF5417A661716AF43B8,
	U3CU3Ec__DisplayClass68_0__ctor_m7B9B7E29F4FF9F2A5EB6F4B1CEA1F03D21836C3D,
	U3CU3Ec__DisplayClass68_0_U3CHideU3Eb__7_m217ADBAE7846F86410C3BDFD71295EEE376B24CE,
	OnScreenTransitionUnityEvent__ctor_mAC57095099E84EC944ECCE2F7601D4B9D0811094,
	U3CSetupU3Ed__6__ctor_m9200961092E3FFD2AAFE4F0EE5597D839C1A5BEF,
	U3CSetupU3Ed__6_System_IDisposable_Dispose_mEE88D17E611A41225A7F584B730F152ACA412C41,
	U3CSetupU3Ed__6_MoveNext_mB7584537923AE0FF8E19233C59C4624EF96523B3,
	U3CSetupU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BE424FDA23426B7C62524DF164681EB5FA3D385,
	U3CSetupU3Ed__6_System_Collections_IEnumerator_Reset_m4E600DF3B1633D7BE632FDAAC4520A04DAF1E5DF,
	U3CSetupU3Ed__6_System_Collections_IEnumerator_get_Current_m83A63929A06057E3AEFF4F507B41C6846E30E8C7,
	U3CU3Ec__DisplayClass70_0__ctor_m2ACD24A5A82F392E55941DDAA5571A409E1AED4C,
	U3CU3Ec__DisplayClass70_1__ctor_mFAC930CF73C498EEF4C48E1246F4FA010D14A2A2,
	U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__0_mF0D7714DD80BC60F670A2E002E08312842B7AFF8,
	U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__1_mFDD2F7F450CDA9F047A6FE2B0B7D9E6BCDF277AF,
	U3CU3Ec__DisplayClass70_1_U3CSetPageU3Eb__2_mEB84AE71EFC28B7A3F9CF6F23C3CF734C8E8AE0B,
	U3CWaitTimeU3Ed__23__ctor_m70E166FF4CB68CD4DE52D36E0A13763E85907996,
	U3CWaitTimeU3Ed__23_System_IDisposable_Dispose_m6DE32DB29176AA4A70757C2DDD69B26D5A3D07DC,
	U3CWaitTimeU3Ed__23_MoveNext_m3D25B9847BC8CB9BF1A9650149E07D176C641F2F,
	U3CWaitTimeU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EBAEE344A682F12394BA86E16BFC3448238B0C3,
	U3CWaitTimeU3Ed__23_System_Collections_IEnumerator_Reset_mB0FB99C99C6C404B5F3BA308605982574C8B4D07,
	U3CWaitTimeU3Ed__23_System_Collections_IEnumerator_get_Current_m354F94063D3DFCCB477B0D0581F5268160FB657B,
	U3CU3Ec__DisplayClass4_0__ctor_m526A4A9EA0A60E6D81F5447F33DE71515CEA4C1D,
	U3CU3Ec__DisplayClass4_0_U3CGetIconU3Eb__0_mD24EA926AF896937C4FFA8DBB38C1009B59A36C6,
	U3CU3Ec__DisplayClass3_0__ctor_m8672F4EC45FA31085C4E9B27C927124CBBA18054,
	U3CU3Ec__DisplayClass3_0_U3CGetIconU3Eb__0_mBDDFE4B33384B35DAF8B87B9BEDC97946212355E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mF665BC097A41CA8FC3894E71D0C5303B26FDA7C0,
	U3CU3Ec__ctor_mDBE7C576B0AB15B1028E34ADFEB74441A9BFD9C1,
	U3CU3Ec_U3CTimedCallbackU3Eb__24_0_m3BD2C4BDCE4483B2721AB23C89BB34CE1EA35679,
	U3CU3Ec__DisplayClass26_0__ctor_m272952AC1C7BFA2F4A9224B4FE7500BD6BD4FF30,
	U3CU3Ec__DisplayClass26_0_U3CTweenFloatU3Eb__0_mF45C909FDD5420FB35FD767D6F1226B2AB3E95F0,
	U3CU3Ec__DisplayClass26_0_U3CTweenFloatU3Eb__1_m19667DC3922012DA4D52FED525E0F5FD8069780D,
	U3CU3Ec__DisplayClass27_0__ctor_m032723C67D74381580F394A026B4CC279B6EA2B0,
	U3CU3Ec__DisplayClass27_0_U3CTweenFloatU3Eb__0_mF6E0BF7110761FE5877621F1D68C298891DEC185,
	U3CU3Ec__DisplayClass29_0__ctor_mAEB30B2325EFA86E7E4EBFD4C2250DB0E7AC22C8,
	U3CU3Ec__DisplayClass29_0_U3CTweenFloatCustomU3Eb__0_mA5ED021D0D6164959893B3F93879C3C35FF93809,
	U3CU3Ec__DisplayClass29_0_U3CTweenFloatCustomU3Eb__1_m8902E16FE21D7F811E4967A11BC2CA8BC0ED2462,
	U3CU3Ec__DisplayClass30_0__ctor_m597E943A45A66A6F3A62CA7B2B3562CDBAA36D31,
	U3CU3Ec__DisplayClass30_0_U3CTweenFloatCustomU3Eb__0_mDAD50934A0DAE978FFFFF6CDC721EA50F53921B7,
	U3CU3Ec__DisplayClass33_0__ctor_m21843AAFC1095C81F4EAE1C686B7F5DAB1A2E720,
	U3CU3Ec__DisplayClass33_0_U3CTweenIntU3Eb__0_m5834C37B4DB3BABC4297E316D56E67949B6C42C2,
	U3CU3Ec__DisplayClass33_0_U3CTweenIntU3Eb__1_m8D1A4D914C6EB3FB8A491C9FB7F7CB8B72F0F44E,
	U3CU3Ec__DisplayClass34_0__ctor_m6C512F0B77F16750E6985EB16466FB7538C1E237,
	U3CU3Ec__DisplayClass34_0_U3CTweenIntU3Eb__0_m9E831B956695EBF8C1BBD10DEB93D57636845194,
	U3CU3Ec__DisplayClass36_0__ctor_mDBD077B697AF2073D5CF77B0634821D87D08D861,
	U3CU3Ec__DisplayClass36_0_U3CTweenIntCustomU3Eb__0_mB81D0865345746B37B2EA788737AB12AA41E4BF3,
	U3CU3Ec__DisplayClass36_0_U3CTweenIntCustomU3Eb__1_mCC7F3D7271DC1C4DDC69E51BE14958DD5ED63909,
	U3CU3Ec__DisplayClass37_0__ctor_m8BFEC34A8B82FDB368F968CE93EC56DF5FC59041,
	U3CU3Ec__DisplayClass37_0_U3CTweenIntCustomU3Eb__0_mF2B634416D16DD554026BD8F9AFF07E98CE30087,
	U3CU3Ec__DisplayClass40_0__ctor_mB981D307D7DF6B6EE16EE1453028043BBEC76D32,
	U3CU3Ec__DisplayClass40_0_U3CTweenVector2U3Eb__0_m7E39A4BE609F823456631A3F3617918D2E045FF1,
	U3CU3Ec__DisplayClass40_0_U3CTweenVector2U3Eb__1_m69EEA4DB74F642553392265870303ACDEC1DE380,
	U3CU3Ec__DisplayClass41_0__ctor_mC302124A0E4A79795F192B63C68CCBCA7EEEB940,
	U3CU3Ec__DisplayClass41_0_U3CTweenVector2U3Eb__0_m8B1C28687BFADC12A2DD9855ABAE5CB95D2072BF,
	U3CU3Ec__DisplayClass43_0__ctor_m29BFF7B4D0BAE35C0E2E149C2D249B07AFD88186,
	U3CU3Ec__DisplayClass43_0_U3CTweenVector2CustomU3Eb__0_m4310BB95FE6254A9FA8DB8351830D3F1AEEAB1EC,
	U3CU3Ec__DisplayClass43_0_U3CTweenVector2CustomU3Eb__1_m53974F90739EDD03331F34F7D05821151684DA67,
	U3CU3Ec__DisplayClass44_0__ctor_m34D7420FB10380F8F332DF7F45A53AB90540BFA0,
	U3CU3Ec__DisplayClass44_0_U3CTweenVector2CustomU3Eb__0_m4288DA26BCD3D5393663C6296F2B78E0514176C6,
	U3CU3Ec__DisplayClass47_0__ctor_m174737F2C03B3C9E7E166E28425D4D5110C0B814,
	U3CU3Ec__DisplayClass47_0_U3CTweenVector3U3Eb__0_m154428AD77878642CB083E65F207673405777A2A,
	U3CU3Ec__DisplayClass47_0_U3CTweenVector3U3Eb__1_m6DDA20967266BFCE36ED32365D3E53A3CA1A8842,
	U3CU3Ec__DisplayClass48_0__ctor_m36EC395ED756CB8599B4CF7082D3B6E95A19DBBD,
	U3CU3Ec__DisplayClass48_0_U3CTweenVector3U3Eb__0_m24682362285B5EADB23BA63CEFD875BE2CE1238B,
	U3CU3Ec__DisplayClass50_0__ctor_mB76ACD26212574CCBD9BB6FAA38B75972790571D,
	U3CU3Ec__DisplayClass50_0_U3CTweenVector3CustomU3Eb__0_mAE91C5AC66A663A8FB0CB532DA0DCD1C88DA1541,
	U3CU3Ec__DisplayClass50_0_U3CTweenVector3CustomU3Eb__1_m7744D53C1AADB493922600E6002DCD77503FCD38,
	U3CU3Ec__DisplayClass51_0__ctor_m66322FB784B6CB7990F9FCF36DE01D797BA378B7,
	U3CU3Ec__DisplayClass51_0_U3CTweenVector3CustomU3Eb__0_mA191E6DFDEB5D769BC61C5C28D3DF8074802C18C,
	U3CU3Ec__DisplayClass54_0__ctor_m73908220ECB8349B9CDB280240C32398C435757C,
	U3CU3Ec__DisplayClass54_0_U3CTweenVector4U3Eb__0_m3817E490A6EB9AFBB907176F5EFBE024AF51299F,
	U3CU3Ec__DisplayClass54_0_U3CTweenVector4U3Eb__1_m1C3AFBAC969CCA8B26AFEEB62EB8222C82DE2675,
	U3CU3Ec__DisplayClass55_0__ctor_m60E751A4F9D1AF2ECE9E6C8D6E6521AA2FFFE1C6,
	U3CU3Ec__DisplayClass55_0_U3CTweenVector4U3Eb__0_m60033FAABDC8217B0738B83CEEBBBC5CF0DC66E2,
	U3CU3Ec__DisplayClass57_0__ctor_mC6A5416D56FC7B53D2B3889F277C6F3D5DF4C7B3,
	U3CU3Ec__DisplayClass57_0_U3CTweenVector4CustomU3Eb__0_m5B13C5523A6B905037520D9E73C12FC5B8D8E103,
	U3CU3Ec__DisplayClass57_0_U3CTweenVector4CustomU3Eb__1_m65C6FE9544BFFB8847991DCC19D4A0F55880F0DB,
	U3CU3Ec__DisplayClass58_0__ctor_mB073F944DA40D2AFBF8D5CC2029F66AF6C946A61,
	U3CU3Ec__DisplayClass58_0_U3CTweenVector4CustomU3Eb__0_m517112B11D2A59947AC0A3EDDCC567EDA8E6E19E,
	U3CU3Ec__DisplayClass61_0__ctor_mBE2AB1C949FECA762AD5858927C7751C63A954EB,
	U3CU3Ec__DisplayClass61_0_U3CTweenColorU3Eb__0_m4899ACD811932106797C41399A01516CAAC4989D,
	U3CU3Ec__DisplayClass61_0_U3CTweenColorU3Eb__1_mD7DC9D667B29FC57322C796D789F116915FC892F,
	U3CU3Ec__DisplayClass62_0__ctor_m0C100D5A36426B07CCEDF17BF3D2EF885890DFA9,
	U3CU3Ec__DisplayClass62_0_U3CTweenColorU3Eb__0_m4ECDB3EA506B3B9FC21AC0D1D23B9B71D2C80D2D,
	U3CU3Ec__DisplayClass64_0__ctor_mB97593B69E45571632CBD1CF661D528C1B363163,
	U3CU3Ec__DisplayClass64_0_U3CTweenColorCustomU3Eb__0_mE8D366A3FDFE11A711248C2CAF08D5AE37068690,
	U3CU3Ec__DisplayClass64_0_U3CTweenColorCustomU3Eb__1_m22CF1ECD9E95AFF1766613142D0FF1DD95DC7E48,
	U3CU3Ec__DisplayClass65_0__ctor_m2E19E057C9E9FE4EC61B68A0B8C578D38EBE820D,
	U3CU3Ec__DisplayClass65_0_U3CTweenColorCustomU3Eb__0_m745AE216E29F1C2F4B77093234C0EDEF51696827,
	U3CU3Ec__cctor_m4BFCF57DC71897088892FDD9551A80917D5B8121,
	U3CU3Ec__ctor_mCB8F94A1CA144C35FB02113C7DFBE0CDC999E724,
	U3CU3Ec_U3CDecodeU3Eb__1_0_m84EEDBA70E67F935D67C1BCBD05706A1B018B597,
	U3CU3Ec__DisplayClass9_0__ctor_mA8FF201A577C09BE5FBD251CB96DF006EDA209E3,
	U3CU3Ec__DisplayClass9_0_U3CRenderRoomU3Eb__0_m9A427F27FF22A753C64A7EEE559AA7C3DFE164D4,
	U3CU3Ec__DisplayClass9_0_U3CRenderRoomU3Eb__1_m8DC507B17E2100F97B3F9931468E805B12D23FA2,
};
static const int32_t s_InvokerIndices[3411] = 
{
	23,
	23,
	23,
	23,
	2582,
	201,
	2583,
	2583,
	2037,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	32,
	32,
	32,
	23,
	23,
	23,
	23,
	3,
	14,
	26,
	105,
	105,
	28,
	28,
	28,
	105,
	105,
	28,
	28,
	28,
	23,
	23,
	3,
	4,
	163,
	49,
	869,
	23,
	26,
	23,
	23,
	9,
	26,
	26,
	23,
	26,
	23,
	31,
	23,
	26,
	23,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	469,
	344,
	469,
	344,
	469,
	344,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	110,
	331,
	14,
	26,
	14,
	26,
	23,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	163,
	49,
	869,
	4,
	163,
	49,
	869,
	49,
	869,
	4,
	4,
	365,
	4,
	163,
	4,
	163,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	14,
	26,
	23,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	110,
	331,
	14,
	26,
	10,
	32,
	89,
	31,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	746,
	343,
	746,
	343,
	746,
	343,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	23,
	181,
	209,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	14,
	26,
	10,
	32,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	110,
	331,
	110,
	331,
	14,
	26,
	10,
	32,
	89,
	31,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	10,
	32,
	110,
	331,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	-1,
	0,
	-1,
	-1,
	4,
	4,
	4,
	163,
	3,
	4,
	163,
	163,
	163,
	3,
	163,
	1709,
	4,
	163,
	4,
	163,
	4,
	163,
	4,
	163,
	4,
	163,
	26,
	89,
	23,
	26,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1651,
	23,
	23,
	23,
	23,
	105,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	1651,
	23,
	23,
	23,
	2584,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	32,
	32,
	32,
	32,
	32,
	23,
	23,
	23,
	31,
	31,
	31,
	32,
	31,
	123,
	31,
	31,
	123,
	465,
	23,
	2037,
	2037,
	2037,
	2037,
	2037,
	23,
	23,
	26,
	23,
	281,
	281,
	0,
	0,
	119,
	0,
	48,
	239,
	239,
	509,
	94,
	2585,
	21,
	2586,
	281,
	0,
	281,
	3,
	10,
	14,
	26,
	10,
	23,
	23,
	2587,
	2588,
	23,
	23,
	31,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	2587,
	10,
	2589,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	32,
	26,
	26,
	23,
	43,
	0,
	2590,
	2591,
	2592,
	2593,
	2594,
	1728,
	1732,
	1703,
	1692,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	2591,
	2592,
	2593,
	2594,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	26,
	26,
	26,
	23,
	23,
	23,
	14,
	26,
	89,
	14,
	26,
	14,
	10,
	32,
	746,
	746,
	343,
	746,
	343,
	746,
	343,
	746,
	343,
	746,
	343,
	10,
	32,
	14,
	26,
	14,
	10,
	14,
	26,
	23,
	10,
	10,
	746,
	746,
	23,
	23,
	23,
	2189,
	23,
	23,
	23,
	23,
	23,
	0,
	23,
	42,
	42,
	14,
	14,
	26,
	1620,
	1621,
	1620,
	1621,
	89,
	31,
	1620,
	1621,
	1620,
	1621,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	1651,
	2037,
	14,
	26,
	10,
	32,
	89,
	31,
	1651,
	2037,
	10,
	32,
	89,
	31,
	10,
	14,
	14,
	746,
	343,
	23,
	23,
	23,
	2229,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	32,
	2595,
	23,
	31,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	26,
	26,
	26,
	23,
	10,
	32,
	14,
	14,
	14,
	23,
	3,
	62,
	23,
	2596,
	746,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	2597,
	26,
	23,
	3,
	14,
	23,
	4,
	14,
	23,
	14,
	23,
	26,
	26,
	23,
	89,
	31,
	14,
	42,
	2598,
	23,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	31,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	343,
	23,
	42,
	746,
	343,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	89,
	31,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	89,
	31,
	1651,
	2037,
	1651,
	2037,
	14,
	26,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	343,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	89,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	14,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	1620,
	1621,
	1620,
	1620,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	1651,
	2037,
	14,
	26,
	14,
	26,
	1651,
	2037,
	14,
	26,
	14,
	26,
	1651,
	2037,
	23,
	23,
	2229,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	2037,
	23,
	14,
	26,
	14,
	26,
	14,
	986,
	23,
	23,
	23,
	23,
	14,
	26,
	746,
	14,
	746,
	343,
	343,
	26,
	23,
	26,
	23,
	23,
	343,
	343,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	343,
	2599,
	2600,
	26,
	26,
	2526,
	1621,
	1621,
	14,
	26,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	986,
	34,
	32,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	23,
	14,
	26,
	10,
	32,
	26,
	26,
	23,
	14,
	26,
	14,
	14,
	26,
	967,
	31,
	23,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	26,
	14,
	26,
	986,
	2601,
	986,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	10,
	14,
	26,
	14,
	26,
	23,
	2602,
	34,
	32,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	23,
	14,
	14,
	14,
	27,
	23,
	14,
	14,
	14,
	14,
	452,
	27,
	27,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	23,
	452,
	26,
	23,
	32,
	32,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	23,
	14,
	14,
	14,
	26,
	14,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	26,
	110,
	331,
	10,
	14,
	14,
	23,
	2603,
	2037,
	331,
	32,
	62,
	26,
	26,
	23,
	23,
	465,
	23,
	23,
	561,
	199,
	23,
	23,
	23,
	23,
	23,
	343,
	343,
	343,
	343,
	23,
	14,
	14,
	14,
	110,
	2604,
	331,
	23,
	23,
	14,
	14,
	14,
	10,
	32,
	32,
	23,
	23,
	23,
	199,
	2037,
	32,
	32,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	10,
	32,
	10,
	32,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	2605,
	23,
	2037,
	23,
	1389,
	495,
	1389,
	495,
	343,
	133,
	133,
	129,
	23,
	343,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	746,
	343,
	14,
	2229,
	2229,
	2606,
	2607,
	2608,
	23,
	2037,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	746,
	343,
	746,
	343,
	89,
	31,
	89,
	31,
	89,
	31,
	746,
	343,
	746,
	343,
	1651,
	2037,
	14,
	26,
	1651,
	2037,
	1651,
	2037,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	1612,
	23,
	133,
	1611,
	58,
	746,
	23,
	343,
	343,
	1621,
	23,
	343,
	343,
	23,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	10,
	32,
	89,
	31,
	89,
	31,
	89,
	31,
	1620,
	1621,
	14,
	26,
	14,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	14,
	26,
	14,
	26,
	746,
	343,
	89,
	31,
	14,
	4,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	746,
	746,
	26,
	26,
	23,
	23,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	343,
	343,
	23,
	2037,
	2037,
	2037,
	2037,
	2037,
	343,
	23,
	343,
	746,
	23,
	343,
	343,
	343,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	746,
	343,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	89,
	31,
	89,
	31,
	746,
	343,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	14,
	89,
	89,
	31,
	746,
	343,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	14,
	23,
	23,
	42,
	23,
	23,
	23,
	23,
	23,
	10,
	14,
	23,
	23,
	26,
	23,
	343,
	14,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	1621,
	343,
	1612,
	1621,
	2037,
	1651,
	1651,
	343,
	1612,
	1621,
	2037,
	343,
	343,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	10,
	32,
	26,
	26,
	23,
	10,
	32,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	26,
	206,
	1651,
	2037,
	746,
	343,
	23,
	32,
	23,
	23,
	32,
	31,
	32,
	14,
	26,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	1620,
	1621,
	89,
	31,
	1620,
	1621,
	746,
	343,
	4,
	89,
	31,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	26,
	343,
	343,
	2037,
	343,
	23,
	343,
	23,
	23,
	2037,
	23,
	23,
	14,
	26,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	2219,
	2037,
	23,
	31,
	746,
	746,
	23,
	14,
	26,
	14,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	2219,
	2037,
	746,
	746,
	23,
	746,
	14,
	26,
	14,
	14,
	31,
	23,
	23,
	2219,
	2037,
	746,
	746,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	31,
	746,
	343,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	89,
	31,
	1651,
	2037,
	1651,
	2037,
	1651,
	2037,
	89,
	31,
	1651,
	2037,
	1651,
	2037,
	23,
	23,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	89,
	31,
	746,
	343,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	1620,
	1621,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	89,
	31,
	746,
	343,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	1620,
	1621,
	14,
	26,
	746,
	343,
	14,
	14,
	14,
	14,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	1620,
	1620,
	1611,
	23,
	23,
	14,
	26,
	10,
	32,
	10,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	89,
	31,
	746,
	343,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	1620,
	1621,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	89,
	31,
	746,
	343,
	746,
	343,
	14,
	26,
	89,
	31,
	10,
	32,
	10,
	32,
	1620,
	1621,
	14,
	26,
	746,
	343,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	32,
	32,
	129,
	31,
	31,
	26,
	130,
	32,
	23,
	14,
	26,
	14,
	26,
	2609,
	26,
	23,
	23,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	14,
	26,
	26,
	26,
	32,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	26,
	26,
	26,
	23,
	746,
	343,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	746,
	746,
	343,
	14,
	89,
	31,
	89,
	31,
	14,
	23,
	23,
	23,
	746,
	32,
	133,
	23,
	133,
	133,
	133,
	32,
	343,
	37,
	23,
	23,
	42,
	1621,
	1621,
	1621,
	14,
	26,
	746,
	343,
	1651,
	2037,
	1651,
	2037,
	10,
	32,
	2610,
	23,
	23,
	26,
	23,
	14,
	23,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	26,
	26,
	225,
	0,
	0,
	3,
	43,
	0,
	4,
	3,
	0,
	4,
	14,
	26,
	14,
	26,
	23,
	156,
	14,
	26,
	14,
	26,
	23,
	27,
	89,
	14,
	26,
	23,
	746,
	343,
	746,
	10,
	32,
	14,
	14,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	746,
	746,
	746,
	746,
	746,
	746,
	23,
	4,
	163,
	49,
	0,
	4,
	114,
	114,
	0,
	0,
	1,
	1,
	4,
	4,
	23,
	23,
	23,
	23,
	2,
	520,
	2611,
	4,
	0,
	579,
	4,
	0,
	579,
	4,
	1,
	365,
	1,
	365,
	4,
	2,
	520,
	2611,
	4,
	2,
	1062,
	520,
	2612,
	2613,
	4,
	-1,
	1434,
	2614,
	2615,
	2616,
	2617,
	4,
	2611,
	2618,
	4,
	23,
	0,
	1,
	3,
	4,
	23,
	23,
	23,
	23,
	163,
	195,
	2619,
	2620,
	23,
	49,
	23,
	4,
	23,
	23,
	23,
	23,
	137,
	2621,
	2622,
	23,
	49,
	26,
	23,
	4,
	10,
	10,
	10,
	23,
	163,
	46,
	43,
	920,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2623,
	2624,
	2625,
	2626,
	2627,
	2628,
	2629,
	2630,
	2631,
	2626,
	2632,
	2633,
	2629,
	2634,
	2635,
	2626,
	2636,
	2637,
	2629,
	2638,
	2639,
	2626,
	2640,
	2641,
	2629,
	2642,
	2643,
	2626,
	2644,
	2645,
	2629,
	2646,
	2647,
	2626,
	2648,
	2649,
	2629,
	23,
	23,
	23,
	23,
	23,
	31,
	2181,
	23,
	-1,
	2650,
	0,
	1,
	137,
	163,
	-1,
	163,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1690,
	2651,
	1779,
	2652,
	2652,
	2653,
	2653,
	2654,
	2654,
	114,
	137,
	137,
	137,
	0,
	0,
	0,
	746,
	343,
	14,
	26,
	23,
	23,
	23,
	1617,
	1687,
	2037,
	746,
	343,
	746,
	343,
	746,
	343,
	746,
	343,
	2655,
	2656,
	1651,
	14,
	2657,
	26,
	23,
	26,
	89,
	26,
	89,
	26,
	62,
	89,
	62,
	89,
	62,
	89,
	62,
	89,
	27,
	89,
	26,
	89,
	26,
	89,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	2658,
	2659,
	1692,
	2660,
	2661,
	2662,
	2663,
	2664,
	2665,
	4,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	4,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	1694,
	3,
	0,
	3,
	22,
	1639,
	2666,
	26,
	26,
	0,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	4,
	163,
	23,
	26,
	26,
	23,
	94,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	9,
	9,
	23,
	9,
	23,
	9,
	3,
	23,
	233,
	28,
	233,
	9,
	23,
	9,
	23,
	23,
	23,
	9,
	3,
	23,
	9,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	-1,
	23,
	9,
	23,
	9,
	3,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	27,
	23,
	3,
	23,
	331,
	331,
	3,
	23,
	23,
	23,
	2037,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	2037,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	2037,
	23,
	9,
	89,
	31,
	10,
	10,
	26,
	27,
	14,
	23,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	746,
	343,
	746,
	343,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	14,
	26,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	343,
	746,
	23,
	124,
	32,
	603,
	26,
	23,
	23,
	23,
	2037,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	343,
	23,
	343,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	343,
	23,
	343,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	343,
	746,
	746,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	9,
	23,
	9,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	23,
	343,
	23,
	746,
	746,
	23,
	746,
	23,
	746,
	746,
	23,
	746,
	23,
	10,
	10,
	23,
	10,
	23,
	10,
	10,
	23,
	10,
	23,
	1620,
	1620,
	23,
	1620,
	23,
	1620,
	1620,
	23,
	1620,
	23,
	1611,
	1611,
	23,
	1611,
	23,
	1611,
	1611,
	23,
	1611,
	23,
	1770,
	1770,
	23,
	1770,
	23,
	1770,
	1770,
	23,
	1770,
	23,
	1651,
	1651,
	23,
	1651,
	23,
	1651,
	1651,
	23,
	1651,
	3,
	23,
	28,
	23,
	9,
	9,
};
static const Il2CppTokenRangePair s_rgctxIndices[19] = 
{
	{ 0x02000133, { 33, 5 } },
	{ 0x06000239, { 0, 1 } },
	{ 0x0600023B, { 1, 1 } },
	{ 0x0600023C, { 2, 2 } },
	{ 0x060009DF, { 4, 1 } },
	{ 0x06000A0E, { 5, 7 } },
	{ 0x06000A0F, { 12, 6 } },
	{ 0x06000A10, { 18, 1 } },
	{ 0x06000A11, { 19, 7 } },
	{ 0x06000A12, { 26, 6 } },
	{ 0x06000A13, { 32, 1 } },
	{ 0x06000A41, { 38, 1 } },
	{ 0x06000A47, { 39, 1 } },
	{ 0x06000A49, { 40, 1 } },
	{ 0x06000A4A, { 41, 3 } },
	{ 0x06000A4B, { 44, 2 } },
	{ 0x06000A4C, { 46, 3 } },
	{ 0x06000A4D, { 49, 1 } },
	{ 0x06000A4E, { 50, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[51] = 
{
	{ (Il2CppRGCTXDataType)2, 28935 },
	{ (Il2CppRGCTXDataType)3, 20528 },
	{ (Il2CppRGCTXDataType)2, 30369 },
	{ (Il2CppRGCTXDataType)3, 20529 },
	{ (Il2CppRGCTXDataType)3, 20530 },
	{ (Il2CppRGCTXDataType)2, 30370 },
	{ (Il2CppRGCTXDataType)3, 20531 },
	{ (Il2CppRGCTXDataType)3, 20532 },
	{ (Il2CppRGCTXDataType)2, 30371 },
	{ (Il2CppRGCTXDataType)3, 20533 },
	{ (Il2CppRGCTXDataType)3, 20534 },
	{ (Il2CppRGCTXDataType)3, 20535 },
	{ (Il2CppRGCTXDataType)2, 30372 },
	{ (Il2CppRGCTXDataType)3, 20536 },
	{ (Il2CppRGCTXDataType)3, 20537 },
	{ (Il2CppRGCTXDataType)2, 29450 },
	{ (Il2CppRGCTXDataType)3, 20538 },
	{ (Il2CppRGCTXDataType)3, 20539 },
	{ (Il2CppRGCTXDataType)1, 29452 },
	{ (Il2CppRGCTXDataType)2, 30373 },
	{ (Il2CppRGCTXDataType)3, 20540 },
	{ (Il2CppRGCTXDataType)3, 20541 },
	{ (Il2CppRGCTXDataType)2, 30374 },
	{ (Il2CppRGCTXDataType)3, 20542 },
	{ (Il2CppRGCTXDataType)3, 20543 },
	{ (Il2CppRGCTXDataType)3, 20544 },
	{ (Il2CppRGCTXDataType)2, 30375 },
	{ (Il2CppRGCTXDataType)3, 20545 },
	{ (Il2CppRGCTXDataType)3, 20546 },
	{ (Il2CppRGCTXDataType)2, 29458 },
	{ (Il2CppRGCTXDataType)3, 20547 },
	{ (Il2CppRGCTXDataType)3, 20548 },
	{ (Il2CppRGCTXDataType)1, 29460 },
	{ (Il2CppRGCTXDataType)3, 20549 },
	{ (Il2CppRGCTXDataType)3, 20550 },
	{ (Il2CppRGCTXDataType)3, 20551 },
	{ (Il2CppRGCTXDataType)2, 29472 },
	{ (Il2CppRGCTXDataType)3, 20552 },
	{ (Il2CppRGCTXDataType)3, 20553 },
	{ (Il2CppRGCTXDataType)3, 20554 },
	{ (Il2CppRGCTXDataType)3, 20555 },
	{ (Il2CppRGCTXDataType)3, 20556 },
	{ (Il2CppRGCTXDataType)2, 29563 },
	{ (Il2CppRGCTXDataType)3, 20557 },
	{ (Il2CppRGCTXDataType)3, 20558 },
	{ (Il2CppRGCTXDataType)2, 29564 },
	{ (Il2CppRGCTXDataType)3, 20559 },
	{ (Il2CppRGCTXDataType)2, 29567 },
	{ (Il2CppRGCTXDataType)3, 20560 },
	{ (Il2CppRGCTXDataType)3, 20561 },
	{ (Il2CppRGCTXDataType)3, 20562 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	3411,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	19,
	s_rgctxIndices,
	51,
	s_rgctxValues,
	NULL,
};
