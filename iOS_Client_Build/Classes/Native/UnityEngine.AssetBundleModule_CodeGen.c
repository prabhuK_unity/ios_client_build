﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::UnloadAllAssetBundles(System.Boolean)
extern void AssetBundle_UnloadAllAssetBundles_mB49AAB07A97A3383897CF55F81EC096620969363 ();
static Il2CppMethodPointer s_methodPointers[1] = 
{
	AssetBundle_UnloadAllAssetBundles_mB49AAB07A97A3383897CF55F81EC096620969363,
};
static const int32_t s_InvokerIndices[1] = 
{
	869,
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
