﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void MenuScript::Awake()
extern void MenuScript_Awake_mA33B0EB58FAFDEB55A90B1CD71033CB6F098A0B6 ();
// 0x00000002 System.Void MenuScript::Start()
extern void MenuScript_Start_m5AE71CA86E0D0ABD34A9AE132E4D6D32B424E10A ();
// 0x00000003 System.Void MenuScript::PrintDeviceDebug()
extern void MenuScript_PrintDeviceDebug_m68D354157F305EF37F28BB0271D2A75C3A74A8F4 ();
// 0x00000004 System.Void MenuScript::Update()
extern void MenuScript_Update_m19A50B38E1ED3C767EB24BEE703A4CAEBA5780C0 ();
// 0x00000005 System.Void MenuScript::Initialize()
extern void MenuScript_Initialize_m704CD615E8D8F6056152082956239C4A11FDC027 ();
// 0x00000006 System.Void MenuScript::ExampleUi()
extern void MenuScript_ExampleUi_m396469AEAB6F9B5FF0669E0F80D96F0709257822 ();
// 0x00000007 System.Void MenuScript::MenuUi()
extern void MenuScript_MenuUi_mEB2234F36985E9C50159A848C90ADF68C2F70B89 ();
// 0x00000008 System.Void MenuScript::LoadExample(System.String)
extern void MenuScript_LoadExample_m3B45C230C206A89CE081E2DAE19F14DA48C11255 ();
// 0x00000009 System.Void MenuScript::LoadChatExample()
extern void MenuScript_LoadChatExample_m0BB9A380651118A8E6F9C5BFCF5C301A61004F41 ();
// 0x0000000A System.Void MenuScript::LoadCallExample()
extern void MenuScript_LoadCallExample_m11FA93AC71DDB29A41DC7E12974611B7B0BA9E73 ();
// 0x0000000B System.Void MenuScript::LoadConference()
extern void MenuScript_LoadConference_mFFB2358A163B52BC9E26F4BDA5133544171B9B8B ();
// 0x0000000C System.Void MenuScript::LoadMenu()
extern void MenuScript_LoadMenu_m6386F0D61D025A78799D7D6CBAAB75670A4E16EF ();
// 0x0000000D System.Void MenuScript::.ctor()
extern void MenuScript__ctor_m661378E3B0CF8824BD45D2F409C3B836CB58475B ();
// 0x0000000E System.Void MenuScript::.cctor()
extern void MenuScript__cctor_m1D6ABF5AB88C2FCF2ABF3EC29D5F461E0174C42F ();
// 0x0000000F System.Void CallApp::Awake()
extern void CallApp_Awake_mA390CA53B3E3C6120C3345F47CD4E695F6B86663 ();
// 0x00000010 System.Void CallApp::Start()
extern void CallApp_Start_mFAB0551D6BE0A4271705C866F463413FC2249123 ();
// 0x00000011 System.Void CallApp::OnCallFactoryReady()
extern void CallApp_OnCallFactoryReady_mC566566C272C9DF6041BB4BFBF79D85E25A8B571 ();
// 0x00000012 System.Void CallApp::OnCallFactoryFailed(System.String)
extern void CallApp_OnCallFactoryFailed_m5CA93D81AAA7E2C08378C785CF0D39C0334D858D ();
// 0x00000013 System.Void CallApp::OnDestroy()
extern void CallApp_OnDestroy_mB81E5B8625385904C690D7C902BFF99BA879D8EA ();
// 0x00000014 System.Void CallApp::Update()
extern void CallApp_Update_m947A5D1902D6704452D2F80D7A34413BCF8DB20A ();
// 0x00000015 Byn.Awrtc.NetworkConfig CallApp::CreateNetworkConfig()
extern void CallApp_CreateNetworkConfig_m84E66C05AD9BD3C2C8EF146B151671FB06ECBC61 ();
// 0x00000016 System.Void CallApp::SetupCall()
extern void CallApp_SetupCall_m060DB8EF8DA417267A871655C7762A11B5287318 ();
// 0x00000017 Byn.Awrtc.ICall CallApp::CreateCall(Byn.Awrtc.NetworkConfig)
extern void CallApp_CreateCall_mA3C3C9B367DCE0D1BBED1ED92BB1B4CD6566EF82 ();
// 0x00000018 System.Void CallApp::Call_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void CallApp_Call_CallEvent_m1D1CBE40591F528C00BE3BF12071C10F9E7B1A2C ();
// 0x00000019 System.Void CallApp::CleanupCall()
extern void CallApp_CleanupCall_m8F5ED4E9181B51A97727DF9D041E328CCCE60958 ();
// 0x0000001A Byn.Awrtc.MediaConfig CallApp::CreateMediaConfig()
extern void CallApp_CreateMediaConfig_m36481A7523D0BEF94B8006E8AE066845773AD1FF ();
// 0x0000001B System.Void CallApp::ResetCall()
extern void CallApp_ResetCall_m23FD10964DB672BC57F841508688E6AC3537AFCE ();
// 0x0000001C System.Void CallApp::InternalResetCall()
extern void CallApp_InternalResetCall_m153BB35DEF39BBC28166BFCFFD49FA4E83C16955 ();
// 0x0000001D System.Void CallApp::SetRemoteVolume(System.Single)
extern void CallApp_SetRemoteVolume_mC3BBDBF465CD4DDB30EF7E65CE6598F207EE9C78 ();
// 0x0000001E System.String[] CallApp::GetVideoDevices()
extern void CallApp_GetVideoDevices_m08983BEA0886D0157B754EE8D5AC37DF466B22FA ();
// 0x0000001F System.Boolean CallApp::CanSelectVideoDevice()
extern void CallApp_CanSelectVideoDevice_mDB20B9F51BCE136EF0DAF658412CF0935FC20E90 ();
// 0x00000020 System.Void CallApp::Join(System.String)
extern void CallApp_Join_mBFDBD97F6386A7EB1A9552283E9F1A9D043A7C6C ();
// 0x00000021 System.Void CallApp::InternalJoin()
extern void CallApp_InternalJoin_m881F47C548398396243B871B877D3C8176CB04E2 ();
// 0x00000022 System.Collections.IEnumerator CallApp::CoroutineRejoin()
extern void CallApp_CoroutineRejoin_m93ED87BB92A881D947193C7055243071508665E1 ();
// 0x00000023 System.Void CallApp::Send(System.String)
extern void CallApp_Send_m36CAF9AC69322612B2488BA9B5D34B1412556231 ();
// 0x00000024 System.Void CallApp::SetAudio(System.Boolean)
extern void CallApp_SetAudio_mEA0AAAFF9B540F3027E822F2F3F7CDAAEF916EED ();
// 0x00000025 System.Void CallApp::SetVideo(System.Boolean)
extern void CallApp_SetVideo_m35F2399DF3D74346EA980A57E545846AABB573E0 ();
// 0x00000026 System.Void CallApp::SetVideoDevice(System.String)
extern void CallApp_SetVideoDevice_mB0ECAC56197F8E21BE6A551FE1CF0D748B5611D1 ();
// 0x00000027 System.Void CallApp::SetIdealResolution(System.Int32,System.Int32)
extern void CallApp_SetIdealResolution_mF9F798AAE77F6167561217E6A2BBB8F8F4AD3FED ();
// 0x00000028 System.Void CallApp::SetIdealFps(System.Int32)
extern void CallApp_SetIdealFps_mBA9B22F12B7235D20A7FB74566695D5043E234D0 ();
// 0x00000029 System.Void CallApp::SetShowLocalVideo(System.Boolean)
extern void CallApp_SetShowLocalVideo_m7174A2E84201A0F2644524F958D3171DA3BA5014 ();
// 0x0000002A System.Void CallApp::SetAutoRejoin(System.Boolean,System.Single)
extern void CallApp_SetAutoRejoin_m52FF3427328D18526F14E4A4CE876273C1EE9197 ();
// 0x0000002B System.Boolean CallApp::GetLoudspeakerStatus()
extern void CallApp_GetLoudspeakerStatus_m477CFD136C49914FB5F2C4AC4AB313EC081F16FD ();
// 0x0000002C System.Void CallApp::SetLoudspeakerStatus(System.Boolean)
extern void CallApp_SetLoudspeakerStatus_mEFDF2689F4B568A17BDDF911ED02C13F1E54191F ();
// 0x0000002D System.Void CallApp::SetMute(System.Boolean)
extern void CallApp_SetMute_m97AD18E0880622F66BD325D80B7517DF454EECE9 ();
// 0x0000002E System.Boolean CallApp::IsMute()
extern void CallApp_IsMute_m3BC5714771935410B0F5019D0305B1663D123D9D ();
// 0x0000002F System.Void CallApp::UpdateFrame(Byn.Awrtc.FrameUpdateEventArgs)
extern void CallApp_UpdateFrame_m6B942EA3630E2C505B44C3F3CBF3305B4556A1CE ();
// 0x00000030 System.Void CallApp::Append(System.String)
extern void CallApp_Append_m0B565CBF03C1CBA19703E4BAB6C2B1C22D471ACF ();
// 0x00000031 System.Void CallApp::.ctor()
extern void CallApp__ctor_m491742871D4A65E756C5E9AD67248CF94E3897A0 ();
// 0x00000032 System.Void CallAppUi::Awake()
extern void CallAppUi_Awake_mD13B04646FD8711E9B835631ED336777708A28A8 ();
// 0x00000033 System.Void CallAppUi::Start()
extern void CallAppUi_Start_m51F99808455AF5194136D19A63D12DF57951527B ();
// 0x00000034 System.Void CallAppUi::SaveSettings()
extern void CallAppUi_SaveSettings_m47BEDF75B0FDDEA561EE300846AB4ED95077BE39 ();
// 0x00000035 System.Void CallAppUi::LoadSettings()
extern void CallAppUi_LoadSettings_m51D05A79A1DF3A2ED95CA410F9A8CAB51EAB029D ();
// 0x00000036 System.Void CallAppUi::ResetSettings()
extern void CallAppUi_ResetSettings_mFB45BD6F3637B04BFBE4EDC0DEF318D2B524E2FC ();
// 0x00000037 System.Void CallAppUi::CheckSettings()
extern void CallAppUi_CheckSettings_m841568C6380D8A6CB77888A390737EA0401F7D23 ();
// 0x00000038 System.Void CallAppUi::OnAudioSettingsChanged()
extern void CallAppUi_OnAudioSettingsChanged_mB722644EA28EB799D35F63F2C1AE435C077FA1FE ();
// 0x00000039 System.Void CallAppUi::OnVideoSettingsChanged()
extern void CallAppUi_OnVideoSettingsChanged_mE8E3112163FAAD063A1C701980B04BB0977D96A9 ();
// 0x0000003A System.Collections.IEnumerator CallAppUi::RequestAudioPermissions()
extern void CallAppUi_RequestAudioPermissions_m641C0393B73F9A00336F3220D9D1AD38CE9B93F2 ();
// 0x0000003B System.Collections.IEnumerator CallAppUi::RequestVideoPermissions()
extern void CallAppUi_RequestVideoPermissions_m7DBB2E55BC770696AEBC7282AA12B831805AF9AB ();
// 0x0000003C System.Boolean CallAppUi::PlayerPrefsGetBool(System.String,System.Boolean)
extern void CallAppUi_PlayerPrefsGetBool_m04EA6B094E3EC7C53272837A1DC12E587CA891BD ();
// 0x0000003D System.Void CallAppUi::PlayerPrefsSetBool(System.String,System.Boolean)
extern void CallAppUi_PlayerPrefsSetBool_mFFD9DCF57125E23ADB8EC6B10FCDC6B307581240 ();
// 0x0000003E System.String CallAppUi::GetSelectedVideoDevice()
extern void CallAppUi_GetSelectedVideoDevice_m4735849D5A1D482F68C2BA8267216BC12D05236C ();
// 0x0000003F System.Int32 CallAppUi::TryParseInt(System.String,System.Int32)
extern void CallAppUi_TryParseInt_mE3BA12A40BDB63217AFA59E88BB3947CB3114462 ();
// 0x00000040 System.Void CallAppUi::SetupCallApp()
extern void CallAppUi_SetupCallApp_m94FD669EE866373A849F1333FFA69F37A96F8D4C ();
// 0x00000041 System.Void CallAppUi::ToggleSettings()
extern void CallAppUi_ToggleSettings_mE0A543A7F02585FE0977D21D81092B22966FE600 ();
// 0x00000042 System.Void CallAppUi::ToggleSetup()
extern void CallAppUi_ToggleSetup_m598C4B40285808DFEB292E40B7C696A0449A1359 ();
// 0x00000043 System.Void CallAppUi::UpdateLocalTexture(Byn.Awrtc.IFrame,Byn.Awrtc.FramePixelFormat)
extern void CallAppUi_UpdateLocalTexture_m0B1B88BDFFB58B52F60710819ED976D2C42A7370 ();
// 0x00000044 System.Void CallAppUi::UpdateRemoteTexture(Byn.Awrtc.IFrame,Byn.Awrtc.FramePixelFormat)
extern void CallAppUi_UpdateRemoteTexture_m14B0B2709FC6615A95FA1C8ECF4A47611B66D909 ();
// 0x00000045 System.Void CallAppUi::UpdateVideoDropdown()
extern void CallAppUi_UpdateVideoDropdown_mD09DFB271783FAB213316553A93086472E01D486 ();
// 0x00000046 System.Void CallAppUi::VideoDropdownOnValueChanged(System.Int32)
extern void CallAppUi_VideoDropdownOnValueChanged_mA134DBD254ED14DA4E8E15571338BB766AABE7D6 ();
// 0x00000047 System.Void CallAppUi::Append(System.String)
extern void CallAppUi_Append_m76BB3D59E514B8DF593E1D0EAC1C0A2393104A58 ();
// 0x00000048 System.Void CallAppUi::SetFullscreen(System.Boolean)
extern void CallAppUi_SetFullscreen_m03B1D14080F7CEBBA79DA6D545F1D3CC2BC0E2CF ();
// 0x00000049 System.Void CallAppUi::Fullscreen()
extern void CallAppUi_Fullscreen_mBC337359E0EED2DFBED91D32574D0299D6E6199C ();
// 0x0000004A System.Void CallAppUi::ShowOverlay()
extern void CallAppUi_ShowOverlay_m3302CDEB331AEFDCFFAF88CC07258351FF570B54 ();
// 0x0000004B System.Void CallAppUi::SetGuiState(System.Boolean)
extern void CallAppUi_SetGuiState_m7F9223EC7706A73D63FA22508EB45DB68AF87934 ();
// 0x0000004C System.Void CallAppUi::JoinButtonPressed()
extern void CallAppUi_JoinButtonPressed_m86D7577CBE43040ED032A2188BAE4002A2B97D55 ();
// 0x0000004D System.Void CallAppUi::EnsureLength()
extern void CallAppUi_EnsureLength_mC30EE5D573E2623CA9C7EDD92FD07CAEE9574471 ();
// 0x0000004E System.String CallAppUi::GetRoomname()
extern void CallAppUi_GetRoomname_m592EF5403E2537254451202D6D4AB620A9805AA4 ();
// 0x0000004F System.Void CallAppUi::SendButtonPressed()
extern void CallAppUi_SendButtonPressed_mD137E3BA43B87D80F497E31482681CCED8C8E47D ();
// 0x00000050 System.Void CallAppUi::InputOnEndEdit()
extern void CallAppUi_InputOnEndEdit_m4CCE1DFF0F877B13FFD9410E9F1F6818DA6C8D2A ();
// 0x00000051 System.Void CallAppUi::SendMsg(System.String)
extern void CallAppUi_SendMsg_m80FD1955E11FF919435B509BB97CC88D2724D7C5 ();
// 0x00000052 System.Void CallAppUi::ShutdownButtonPressed()
extern void CallAppUi_ShutdownButtonPressed_mC094512441733A06630EC0D1EF3EEE7913EA9997 ();
// 0x00000053 System.Void CallAppUi::OnVolumeChanged(System.Single)
extern void CallAppUi_OnVolumeChanged_mFE4C566731AEA535B19D4375D67AAC7FE0B8FC0E ();
// 0x00000054 System.Void CallAppUi::OnLoudspeakerToggle()
extern void CallAppUi_OnLoudspeakerToggle_m966ACEE3F2D177FE74640098249B53F3ED7CADFB ();
// 0x00000055 System.Void CallAppUi::RefreshLoudspeakerToggle()
extern void CallAppUi_RefreshLoudspeakerToggle_m2663B1F2CFB2438B691B4BFEA5C8CA309A904409 ();
// 0x00000056 System.Void CallAppUi::OnMuteToggle()
extern void CallAppUi_OnMuteToggle_mC9785BFE67CA920B91A5A4F06990897B16C792D6 ();
// 0x00000057 System.Void CallAppUi::RefreshMuteToggle()
extern void CallAppUi_RefreshMuteToggle_mD3B5093787E58C35AA916634CD83AD62443F015D ();
// 0x00000058 System.Void CallAppUi::Update()
extern void CallAppUi_Update_mC18B04E5C852D8DEA1CAF67641ECE2038B80439C ();
// 0x00000059 System.Void CallAppUi::.ctor()
extern void CallAppUi__ctor_m7A594459FFDCA245FD56036C0D997F4D077709A8 ();
// 0x0000005A System.Void CallAppUi::.cctor()
extern void CallAppUi__cctor_mFD464EDABF2BC6FB6154E96FA1DE4B7A90CDD529 ();
// 0x0000005B System.Void ImgFitter::Update()
extern void ImgFitter_Update_m61E62AC0EE9A6EEFC5E06D6989F34C5F9C47EE9C ();
// 0x0000005C UnityEngine.Vector2 ImgFitter::Abs(UnityEngine.Vector2)
extern void ImgFitter_Abs_m0B5094618DBC042A9AF02CD6BF2BDD55BF267B34 ();
// 0x0000005D System.Void ImgFitter::.ctor()
extern void ImgFitter__ctor_m3804438C3B758EFDFAAC1BD83491A497E1DD9C24 ();
// 0x0000005E System.Void VideoPanelEventHandler::Start()
extern void VideoPanelEventHandler_Start_m298E99DDCBA3DFE98B2432B3B3B504418DCA2F47 ();
// 0x0000005F System.Void VideoPanelEventHandler::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void VideoPanelEventHandler_OnPointerClick_m32D502BDBF6EADB644B08F0ED733126F04ED33B0 ();
// 0x00000060 System.Void VideoPanelEventHandler::.ctor()
extern void VideoPanelEventHandler__ctor_m24F7289F2349E9CDF9FD4AB6A56835CEC4C6800C ();
// 0x00000061 System.Void ChatApp::Start()
extern void ChatApp_Start_mBF9CD536023C6C4A4D1834D9EFF585BE44749EC5 ();
// 0x00000062 System.Void ChatApp::Setup()
extern void ChatApp_Setup_mB65C55B10ABC16045F353F3AB3278A140698C201 ();
// 0x00000063 System.Void ChatApp::Reset()
extern void ChatApp_Reset_mA5DA83EC38550B085C93830D94FD95B4C4D8AF36 ();
// 0x00000064 System.Void ChatApp::Cleanup()
extern void ChatApp_Cleanup_m5D41E4A5D726FC39516768B154D1E3011853CF46 ();
// 0x00000065 System.Void ChatApp::OnDestroy()
extern void ChatApp_OnDestroy_mD5FA54180D1363CE78E62BCA4A24EE79FD58E5D3 ();
// 0x00000066 System.Void ChatApp::FixedUpdate()
extern void ChatApp_FixedUpdate_mF086688D74970182E7DE31D761DBEF875510A28C ();
// 0x00000067 System.Void ChatApp::HandleNetwork()
extern void ChatApp_HandleNetwork_mB0C8CF174DC0871D44E614C27867F26608883EC1 ();
// 0x00000068 System.Void ChatApp::HandleIncommingMessage(Byn.Awrtc.NetworkEvent&)
extern void ChatApp_HandleIncommingMessage_m2587664A0FB6C2806EBACFEC90D4B77E50A1270F ();
// 0x00000069 System.Void ChatApp::SendString(System.String,System.Boolean)
extern void ChatApp_SendString_m7131233BF68865A227D6C35B970F1F13811302B0 ();
// 0x0000006A System.Void ChatApp::Append(System.String)
extern void ChatApp_Append_m439CEF4B1783BDD7580FA2AFAFB628C928147FE7 ();
// 0x0000006B System.Void ChatApp::SetGuiState(System.Boolean)
extern void ChatApp_SetGuiState_m4B9A975948F1DC16A33799337FF1DA3D2A05964C ();
// 0x0000006C System.Void ChatApp::JoinRoomButtonPressed()
extern void ChatApp_JoinRoomButtonPressed_m54E3F962894ABBE52B943C4C83EF733BFA1264F4 ();
// 0x0000006D System.Void ChatApp::EnsureLength()
extern void ChatApp_EnsureLength_m38E14457833C6B239638D4E90CA25DC80AFF1280 ();
// 0x0000006E System.Void ChatApp::LeaveButtonPressed()
extern void ChatApp_LeaveButtonPressed_m56E3D10F2DA3C0B847052C5EB55DD8C50AE94025 ();
// 0x0000006F System.Void ChatApp::OpenRoomButtonPressed()
extern void ChatApp_OpenRoomButtonPressed_mD9A38AE6141BBE0252513CC263C99A91EC1DC5DC ();
// 0x00000070 System.Void ChatApp::InputOnEndEdit()
extern void ChatApp_InputOnEndEdit_mEA5E2718DA0B6C799176A0E5500CC666E50B84D6 ();
// 0x00000071 System.Void ChatApp::SendButtonPressed()
extern void ChatApp_SendButtonPressed_m5765902C8332144D75EB36DC020FCBCC27DC62B8 ();
// 0x00000072 System.Void ChatApp::.ctor()
extern void ChatApp__ctor_m3C2B32278520EAF4F697D49576BF6DDD7F9BF837 ();
// 0x00000073 System.Void MessageList::Awake()
extern void MessageList_Awake_m4781F986E39C5766E6A3A9901A2AE7DF97A898F3 ();
// 0x00000074 System.Void MessageList::Start()
extern void MessageList_Start_mF0C05354CB53CE8FFD53B9F85796755710EB5A5E ();
// 0x00000075 System.Void MessageList::AddTextEntry(System.String)
extern void MessageList_AddTextEntry_m29B65540BDE535E00560031F7A3766BFF3B5898D ();
// 0x00000076 System.Void MessageList::Update()
extern void MessageList_Update_m58BE8170943CF42A281BBCFD14B2792ADA85500E ();
// 0x00000077 System.Void MessageList::.ctor()
extern void MessageList__ctor_mDC3DB0C027072C804BAAB1460F55C9207DA3A6C9 ();
// 0x00000078 System.Void GridManager::Update()
extern void GridManager_Update_mCEBBAD267A803E6B9CD9AD5C7AE7CC78494DAF26 ();
// 0x00000079 System.Void GridManager::Refresh()
extern void GridManager_Refresh_m23E05976600CF5BE206076FD41AC964794C7996F ();
// 0x0000007A System.Void GridManager::.ctor()
extern void GridManager__ctor_m0C0B6E2FFF0CFA78E5C9C090A1590AB81E19D73C ();
// 0x0000007B Byn.Awrtc.Unity.UnityCallFactory Byn.Awrtc.Unity.UnityCallFactory::get_Instance()
extern void UnityCallFactory_get_Instance_m0EB74D642FE08CE87B0A5B4303092061996B784D ();
// 0x0000007C System.Void Byn.Awrtc.Unity.UnityCallFactory::AddInitCallbacks(System.Action,System.Action`1<System.String>)
extern void UnityCallFactory_AddInitCallbacks_m2C02DCF8C1B9EE13F2BB866C5F24B61D39A31899 ();
// 0x0000007D System.Void Byn.Awrtc.Unity.UnityCallFactory::EnsureInit(System.Action)
extern void UnityCallFactory_EnsureInit_m4CF59D37727912824F0446056631EA6B0B0A2E09 ();
// 0x0000007E System.Void Byn.Awrtc.Unity.UnityCallFactory::EnsureInit(System.Action,System.Action`1<System.String>)
extern void UnityCallFactory_EnsureInit_m08767FCA47AB7502F90AE88470CD52AD544BD55B ();
// 0x0000007F System.Void Byn.Awrtc.Unity.UnityCallFactory::Init()
extern void UnityCallFactory_Init_m5359AB034B24C0F2AA4470065BE37501DA0CEF48 ();
// 0x00000080 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerOnInitFailed(System.Exception)
extern void UnityCallFactory_TriggerOnInitFailed_m53620CA810CA943034A5FC1FB67DFA54411D1DC7 ();
// 0x00000081 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerInitFailed(System.String)
extern void UnityCallFactory_TriggerInitFailed_m0BD17DA470FAAC8FD51A417F36353911E1488039 ();
// 0x00000082 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerInitSuccess()
extern void UnityCallFactory_TriggerInitSuccess_m374D7A85B5587BE2B1DC295EC76AAA4F50B0BDE7 ();
// 0x00000083 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerFailedCallback(System.Action`1<System.String>,System.String)
extern void UnityCallFactory_TriggerFailedCallback_m21316F9BF3376C5EB0667365CEC98CE2A3B31A9C ();
// 0x00000084 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerSuccessCallback(System.Action)
extern void UnityCallFactory_TriggerSuccessCallback_mD44AFD6C4591167D594B95285E29B7DDBE8454EC ();
// 0x00000085 System.Collections.IEnumerator Byn.Awrtc.Unity.UnityCallFactory::CoroutineInitAsync()
extern void UnityCallFactory_CoroutineInitAsync_m8A5680BA9807081220F47E1BA1F7E67EED3B8A86 ();
// 0x00000086 System.Void Byn.Awrtc.Unity.UnityCallFactory::CreateSingleton()
extern void UnityCallFactory_CreateSingleton_m04C4A9B70CE9DBFF59822325715D0FEE45BDBC15 ();
// 0x00000087 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::InitStatic()
extern void UnityCallFactory_InitStatic_m9CBADB7A0FC5C35DAE0C74612E8234B5FD15D23D ();
// 0x00000088 Byn.Awrtc.Unity.UnityCallFactory_InitStatusUpdate Byn.Awrtc.Unity.UnityCallFactory::IsInitStaticComplete()
extern void UnityCallFactory_IsInitStaticComplete_m86C12DCA840650FB992FDF0E2439A6A6390A7EC1 ();
// 0x00000089 System.Void Byn.Awrtc.Unity.UnityCallFactory::InitObj()
extern void UnityCallFactory_InitObj_mD48DB538773197FE88E73BA30FD18EF65F9CD3EC ();
// 0x0000008A System.Void Byn.Awrtc.Unity.UnityCallFactory::OnDestroy()
extern void UnityCallFactory_OnDestroy_m0362CD8EB67E7AAA0A14D9589F3BCC8C4166A496 ();
// 0x0000008B Byn.Awrtc.IAwrtcFactory Byn.Awrtc.Unity.UnityCallFactory::get_InternalFactory()
extern void UnityCallFactory_get_InternalFactory_m1F6CFCD39ECA5137F81E3B17149D094BB20AB0EF ();
// 0x0000008C Byn.Awrtc.Native.NativeVideoInput Byn.Awrtc.Unity.UnityCallFactory::get_VideoInput()
extern void UnityCallFactory_get_VideoInput_m78E14A1A779DAEC9021605F06F7AE06B74AD0E2D ();
// 0x0000008D System.Void Byn.Awrtc.Unity.UnityCallFactory::Awake()
extern void UnityCallFactory_Awake_m4B64D394A915AD1B3C87B18CAC8B3314AB979B6A ();
// 0x0000008E System.Void Byn.Awrtc.Unity.UnityCallFactory::Start()
extern void UnityCallFactory_Start_mCB79F4AC0F9387BA2170EC45C8D0384965FEEB2B ();
// 0x0000008F System.Void Byn.Awrtc.Unity.UnityCallFactory::Update()
extern void UnityCallFactory_Update_mB9FD6C8B518249874812C84176EC3FBCC0D9A759 ();
// 0x00000090 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::IsNativePlatform()
extern void UnityCallFactory_IsNativePlatform_m97B33983D94C5D85D5CA57D22A0E1F1E6162740E ();
// 0x00000091 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::StaticInitBrowser()
extern void UnityCallFactory_StaticInitBrowser_mFD619A72E0F8B55BF7E17073D046A23C0753991E ();
// 0x00000092 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::StaticInitAndroid()
extern void UnityCallFactory_StaticInitAndroid_m7B478A21EED5DBEDC9AF9B8ED70060EA9EA90DEA ();
// 0x00000093 Byn.Awrtc.ICall Byn.Awrtc.Unity.UnityCallFactory::Create(Byn.Awrtc.NetworkConfig)
extern void UnityCallFactory_Create_mE17AFB1185D4EF5507F849106D4EF92AE75BC0AF ();
// 0x00000094 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Unity.UnityCallFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
extern void UnityCallFactory_CreateMediaNetwork_mEF3702498F2052816C46A2381A0ACF7EBD18E4C0 ();
// 0x00000095 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Unity.UnityCallFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
extern void UnityCallFactory_CreateBasicNetwork_m6A7417D798623AF3FFE5428AF732AB2DD104AC30 ();
// 0x00000096 System.String[] Byn.Awrtc.Unity.UnityCallFactory::GetVideoDevices()
extern void UnityCallFactory_GetVideoDevices_m2E3C4A038415E64A6C9295067F325EEF668BEA98 ();
// 0x00000097 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::CanSelectVideoDevice()
extern void UnityCallFactory_CanSelectVideoDevice_mDBA7869B06A5FF5AF609FB712ECA5ECEDC24EE9B ();
// 0x00000098 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::IsFrontFacing(System.String)
extern void UnityCallFactory_IsFrontFacing_m4E2CCBD32F1130C0486EE02DDFB773F27D66B993 ();
// 0x00000099 System.String Byn.Awrtc.Unity.UnityCallFactory::GetDefaultVideoDevice()
extern void UnityCallFactory_GetDefaultVideoDevice_m509882C764FCE11EFF2B94E5ADA28386A26C3EAF ();
// 0x0000009A System.Void Byn.Awrtc.Unity.UnityCallFactory::EnterActiveCallState()
extern void UnityCallFactory_EnterActiveCallState_m59E67D66EF29546D2B5540A20857D7A57773942F ();
// 0x0000009B System.Void Byn.Awrtc.Unity.UnityCallFactory::LeaveActiveCallState()
extern void UnityCallFactory_LeaveActiveCallState_m814E768370265AC986DE52F4574B6CED1278B70C ();
// 0x0000009C System.Void Byn.Awrtc.Unity.UnityCallFactory::Dispose()
extern void UnityCallFactory_Dispose_m0EF73E1A00FE92FC99FBAAE5950CDB449E550959 ();
// 0x0000009D System.Void Byn.Awrtc.Unity.UnityCallFactory::SetLoudspeakerStatus(System.Boolean)
extern void UnityCallFactory_SetLoudspeakerStatus_m586B5C1CB504D0422E7D5E07463119EC716489ED ();
// 0x0000009E System.Boolean Byn.Awrtc.Unity.UnityCallFactory::GetLoudspeakerStatus()
extern void UnityCallFactory_GetLoudspeakerStatus_mED5D8C4C252D0228847D2F8022FE1C8C95A22EA9 ();
// 0x0000009F System.Void Byn.Awrtc.Unity.UnityCallFactory::RequestLogLevel(Byn.Awrtc.Unity.UnityCallFactory_LogLevel)
extern void UnityCallFactory_RequestLogLevel_mC66C3ECDE43C19C710F87A4EDEFCFB6265291EEB ();
// 0x000000A0 System.Void Byn.Awrtc.Unity.UnityCallFactory::SetDefaultLogger(System.Boolean,System.Boolean)
extern void UnityCallFactory_SetDefaultLogger_mB560FDCD97B84F33E11FBEA0B2EACF170D6F9874 ();
// 0x000000A1 System.Void Byn.Awrtc.Unity.UnityCallFactory::UpdateLogLevel_WebGl()
extern void UnityCallFactory_UpdateLogLevel_WebGl_m27799888D5A77D36A40F4548DB5C34C733277E98 ();
// 0x000000A2 System.Void Byn.Awrtc.Unity.UnityCallFactory::OnLog(System.Object,System.String[])
extern void UnityCallFactory_OnLog_m0BF0592883807FA198839182601D400D284A69BF ();
// 0x000000A3 System.Void Byn.Awrtc.Unity.UnityCallFactory::UnityLog(System.String,System.Action`1<System.Object>)
extern void UnityCallFactory_UnityLog_mBF4048FB900E0BB8A1537C22DDD0FD3375D61BF4 ();
// 0x000000A4 System.String[] Byn.Awrtc.Unity.UnityCallFactory::SplitLongMsgs(System.String)
extern void UnityCallFactory_SplitLongMsgs_mB5B9BE121C7209DB08C504F027AD04495E7E8E55 ();
// 0x000000A5 System.Void Byn.Awrtc.Unity.UnityCallFactory::.ctor()
extern void UnityCallFactory__ctor_m2CA8CB1B19087BE218AC06BCE756011363C2381D ();
// 0x000000A6 System.Void Byn.Awrtc.Unity.UnityCallFactory::.cctor()
extern void UnityCallFactory__cctor_m6BA06AF9C3AFD471F00A91E97DC2FDEDBDB69D46 ();
// 0x000000A7 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(UnityEngine.Texture2D&,Byn.Awrtc.RawFrame,Byn.Awrtc.FramePixelFormat)
extern void UnityMediaHelper_UpdateTexture_mD002B5D7A43163E66AF3ADAC54CC4A8ACCE25AD1 ();
// 0x000000A8 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(Byn.Awrtc.IFrame,UnityEngine.Texture2D&)
extern void UnityMediaHelper_UpdateTexture_m4F0F65B86771B43BEADCE5ACFABCA52EFE4651BD ();
// 0x000000A9 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::EnsureTex(System.Int32,System.Int32,UnityEngine.TextureFormat,UnityEngine.Texture2D&)
extern void UnityMediaHelper_EnsureTex_m38018256FBDE55738E2407E6A551B9B2D071CD6C ();
// 0x000000AA System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(Byn.Awrtc.IDirectMemoryFrame,UnityEngine.Texture2D&,UnityEngine.Texture2D&,UnityEngine.Texture2D&)
extern void UnityMediaHelper_UpdateTexture_m2C308FAD6F5EAA683C96109F51C6236A38451E41 ();
// 0x000000AB System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateRawImage(UnityEngine.UI.RawImage,Byn.Awrtc.IFrame)
extern void UnityMediaHelper_UpdateRawImage_m7698CD80D54D0E5B8E76B8CFEBE88A2F6C64D7A0 ();
// 0x000000AC System.Void Byn.Awrtc.Unity.UnityMediaHelper::.ctor()
extern void UnityMediaHelper__ctor_m7C413E23BF0205C57CEF54C32BF7C34C303F1BA8 ();
// 0x000000AD System.Void Byn.Awrtc.Unity.UnityMediaHelper::.cctor()
extern void UnityMediaHelper__cctor_m0CC84612F20FC859022A281E2B89880AAE42C32A ();
// 0x000000AE T Byn.Awrtc.Unity.UnitySingleton`1::get_Instance()
// 0x000000AF System.Void Byn.Awrtc.Unity.UnitySingleton`1::OnDestroy()
// 0x000000B0 System.Void Byn.Awrtc.Unity.UnitySingleton`1::.ctor()
// 0x000000B1 System.Void Byn.Awrtc.Unity.UnitySingleton`1::.cctor()
// 0x000000B2 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsFrontFacing(System.String)
extern void AndroidHelper_IsFrontFacing_mBBCD7755BEC6FF374D671A9387EE660AF8E4D64D ();
// 0x000000B3 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsBackFacing(System.String)
extern void AndroidHelper_IsBackFacing_mA88CA45333A6FE44475C3915149ABB165F10DC14 ();
// 0x000000B4 System.Void Byn.Awrtc.Unity.AndroidHelper::SetSpeakerOn(System.Boolean)
extern void AndroidHelper_SetSpeakerOn_m8E14A23214B9B081DD54DDBA5BBCE3E17FEB6776 ();
// 0x000000B5 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsSpeakerOn()
extern void AndroidHelper_IsSpeakerOn_m291F45F32AC253660D91CB05F956CC92B09E9D14 ();
// 0x000000B6 System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetMode()
extern void AndroidHelper_GetMode_m5ADF731CC4C8219C697B465F2E13DADDF93B3687 ();
// 0x000000B7 System.Void Byn.Awrtc.Unity.AndroidHelper::SetMode(System.Int32)
extern void AndroidHelper_SetMode_m88D2873451755C222BE41011AB8FADE686F8930E ();
// 0x000000B8 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsModeInCommunication()
extern void AndroidHelper_IsModeInCommunication_m6AE018363B43BF872E3A2FFCF9C5C428AF3AA20C ();
// 0x000000B9 System.Void Byn.Awrtc.Unity.AndroidHelper::SetModeInCommunicaion()
extern void AndroidHelper_SetModeInCommunicaion_m80898E40E8CB1E84B1A46D85D3AFC267FC28BA2B ();
// 0x000000BA System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetAudioManagerMode()
extern void AndroidHelper_GetAudioManagerMode_m206ADB726EFA7F05D839B6360D1B92989A52B647 ();
// 0x000000BB System.Void Byn.Awrtc.Unity.AndroidHelper::SetModeNormal()
extern void AndroidHelper_SetModeNormal_mA117943A21312F345B67B781AD154950A31042E9 ();
// 0x000000BC System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetStreamVolume()
extern void AndroidHelper_GetStreamVolume_m3DF5BF71B29A2E8D62277E15EE0A5E995EC77590 ();
// 0x000000BD System.Void Byn.Awrtc.Unity.AndroidHelper::SetStreamVolume(System.Int32)
extern void AndroidHelper_SetStreamVolume_m5C5241165229F2EEB91EF2A3DFDD9C2064942973 ();
// 0x000000BE System.Void Byn.Awrtc.Unity.AndroidHelper::SetMute(System.Boolean)
extern void AndroidHelper_SetMute_mEF425DD30C7EB3FE57D96DAD15C67E232BE0A9C1 ();
// 0x000000BF System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsMute()
extern void AndroidHelper_IsMute_mEE2021992145508FF421C4F69C69C52942A78632 ();
// 0x000000C0 UnityEngine.AndroidJavaObject Byn.Awrtc.Unity.AndroidHelper::GetAudioManager()
extern void AndroidHelper_GetAudioManager_mB3A9BB29AF7D647D85BB892E827524D4A90FB58A ();
// 0x000000C1 UnityEngine.AndroidJavaObject Byn.Awrtc.Unity.AndroidHelper::GetActivity()
extern void AndroidHelper_GetActivity_mA6CCCF658D9625D73255FAA804195F6699D2CDC0 ();
// 0x000000C2 System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetAudioManagerFlag(System.String)
extern void AndroidHelper_GetAudioManagerFlag_mD9696C8D3B64DD844069E8373F9A15B822CD9BDA ();
// 0x000000C3 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionMicrophone()
extern void AndroidHelper_CheckPermissionMicrophone_mF0E5117F5296ADFD77E75CCDDC5EB9BB217DC3F9 ();
// 0x000000C4 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionCamera()
extern void AndroidHelper_CheckPermissionCamera_m4DA70A2D449D82C21766C90A0E6F7A307A439EA8 ();
// 0x000000C5 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionAudioSettings()
extern void AndroidHelper_CheckPermissionAudioSettings_mDC41A404AA4E48C3A06FA95DD2170F1EE2DAE84A ();
// 0x000000C6 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionNetwork()
extern void AndroidHelper_CheckPermissionNetwork_mDC72B6D76B28FDDBEF016465589A87DF2AA50A20 ();
// 0x000000C7 System.Boolean Byn.Awrtc.Unity.AndroidHelper::HasRuntimePermissions()
extern void AndroidHelper_HasRuntimePermissions_mF695F557262EC1332761D513823214B12B2277C7 ();
// 0x000000C8 System.Void Byn.Awrtc.Unity.AndroidHelper::RequestPermissions(System.Boolean,System.Boolean,System.Boolean,System.Int32)
extern void AndroidHelper_RequestPermissions_m8CC42EA64C4E6647FE666DD2B742D8A51434DACB ();
// 0x000000C9 System.Void Byn.Awrtc.Unity.AndroidHelper::OpenPermissionView()
extern void AndroidHelper_OpenPermissionView_mA2E083D4A1F5F4E032E080935F8D160B62AE0BAF ();
// 0x000000CA System.Void Byn.Awrtc.Unity.AndroidHelper::setBluetoothScoOn(System.Boolean)
extern void AndroidHelper_setBluetoothScoOn_mEEB8FCEA4BE546C8849AAB8B89B44610450A809D ();
// 0x000000CB System.Void Byn.Awrtc.Unity.AndroidHelper::startBluetoothSco()
extern void AndroidHelper_startBluetoothSco_m33DB41982CF99898DBBA1740AF162A1AA22004DF ();
// 0x000000CC System.Void Byn.Awrtc.Unity.AndroidHelper::stopBluetoothSco()
extern void AndroidHelper_stopBluetoothSco_m1BE3870445DC3E4E283743117CD1A6D02F578E7F ();
// 0x000000CD System.Void Byn.Awrtc.Unity.AndroidHelper::SetBluetoothOn(System.Boolean)
extern void AndroidHelper_SetBluetoothOn_m23BD86C2F034FD627999052E9191D1FC7826D781 ();
// 0x000000CE System.Void Byn.Awrtc.Unity.AndroidHelper::.ctor()
extern void AndroidHelper__ctor_m35DCD9189CA6C348F6401D832ACDB0D4339DF7D5 ();
// 0x000000CF System.Void Byn.Awrtc.Unity.AndroidHelper::.cctor()
extern void AndroidHelper__cctor_mDEA91B3AF7C0DF68CE2E964981785BC2B609E893 ();
// 0x000000D0 System.Void Byn.Awrtc.Unity.UnityHelper::PtrFromColor32(UnityEngine.Color32[],System.Action`2<System.IntPtr,System.UInt32>)
extern void UnityHelper_PtrFromColor32_mE72707EB200D366A9412330D602C18E17F1E63A6 ();
// 0x000000D1 System.String Byn.Unity.Examples.ExampleGlobals::get_SignalingProtocol()
extern void ExampleGlobals_get_SignalingProtocol_m05BA332C69E3BD6DCC47392A6C47CDE8A4479409 ();
// 0x000000D2 System.String Byn.Unity.Examples.ExampleGlobals::get_Signaling()
extern void ExampleGlobals_get_Signaling_m0A592F1C8A3EDE87ECB877480E83D739173B74DF ();
// 0x000000D3 System.String Byn.Unity.Examples.ExampleGlobals::get_SharedSignaling()
extern void ExampleGlobals_get_SharedSignaling_m1E26D21095CDEBF3508E5A38966487A609BCDCC3 ();
// 0x000000D4 System.String Byn.Unity.Examples.ExampleGlobals::get_SignalingConference()
extern void ExampleGlobals_get_SignalingConference_m1322E8CBE8EF30AFEFC1EF22CCD9B864325FC75B ();
// 0x000000D5 Byn.Awrtc.IceServer Byn.Unity.Examples.ExampleGlobals::get_DefaultIceServer()
extern void ExampleGlobals_get_DefaultIceServer_mEE3396133F4B37738F54C122E24282B94C3FE828 ();
// 0x000000D6 System.Boolean Byn.Unity.Examples.ExampleGlobals::HasAudioPermission()
extern void ExampleGlobals_HasAudioPermission_mACD64F059371DC8A0B7ABED1096FA84CC141108C ();
// 0x000000D7 System.Boolean Byn.Unity.Examples.ExampleGlobals::HasVideoPermission()
extern void ExampleGlobals_HasVideoPermission_mE8CD50EE7F64B7AE13CA9499811BB9CE27A68C84 ();
// 0x000000D8 System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestAudioPermission()
extern void ExampleGlobals_RequestAudioPermission_mCADB802925238D69496F8F26BF57D8B35E128353 ();
// 0x000000D9 System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestVideoPermission()
extern void ExampleGlobals_RequestVideoPermission_mCA3062586C833EBF3E30B1EC5B3DDB4F03ED4C03 ();
// 0x000000DA System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestPermissions(System.Boolean,System.Boolean)
extern void ExampleGlobals_RequestPermissions_mCC101C3BF1590B802DD0D7BAC35A2FAA456F7889 ();
// 0x000000DB System.Void Byn.Unity.Examples.ExampleGlobals::.ctor()
extern void ExampleGlobals__ctor_m755D4032F2D26E112979E46E24BA66883548E1CF ();
// 0x000000DC System.Void Byn.Unity.Examples.ExampleGlobals::.cctor()
extern void ExampleGlobals__cctor_m690520B08DF4243835A4B6BF1BC12F458246B074 ();
// 0x000000DD System.Void Byn.Unity.Examples.MinimalCall::Start()
extern void MinimalCall_Start_mC52ACA3C3B205287C4369CFDD2AF50A6A8FB597E ();
// 0x000000DE System.Void Byn.Unity.Examples.MinimalCall::SetupReceiver()
extern void MinimalCall_SetupReceiver_m04BB918D6E870F580F6D633A7035C115BB89EAC6 ();
// 0x000000DF System.Void Byn.Unity.Examples.MinimalCall::Update()
extern void MinimalCall_Update_m01F0E8947BA7B1FD3659EBA6AC8A8CEF865D1B95 ();
// 0x000000E0 System.Void Byn.Unity.Examples.MinimalCall::Receiver_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalCall_Receiver_CallEvent_m967A3EAE6AA368D1211434DCE69774BF555F0587 ();
// 0x000000E1 System.Void Byn.Unity.Examples.MinimalCall::SenderSetup()
extern void MinimalCall_SenderSetup_m3F57FE8DA4DFC511FC877BE82E88E8C90FC0CCEA ();
// 0x000000E2 System.Void Byn.Unity.Examples.MinimalCall::Sender_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalCall_Sender_CallEvent_mCE8A2E535658E6DFBBFA2B50DFC43842F91EA8FB ();
// 0x000000E3 System.Void Byn.Unity.Examples.MinimalCall::OnDestroy()
extern void MinimalCall_OnDestroy_m5E51DEEFEA1F626D2139FE5450A82332176052FF ();
// 0x000000E4 System.Void Byn.Unity.Examples.MinimalCall::.ctor()
extern void MinimalCall__ctor_m288F3DF23BF70FE9D90B66B56954D5113B77505D ();
// 0x000000E5 System.Void Byn.Unity.Examples.MinimalConference::Start()
extern void MinimalConference_Start_m1C8FEF3AA8CA077036F722CAA35FECA45EE877DD ();
// 0x000000E6 System.Void Byn.Unity.Examples.MinimalConference::SetupCalls()
extern void MinimalConference_SetupCalls_mAD9D28B9E80583D924C103DCE57DE4A7F8B4E95F ();
// 0x000000E7 System.Void Byn.Unity.Examples.MinimalConference::OnCallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalConference_OnCallEvent_m2BC4EAD580DA8D43C88518A74EB7747D0F7A86D0 ();
// 0x000000E8 System.Void Byn.Unity.Examples.MinimalConference::OnDestroy()
extern void MinimalConference_OnDestroy_mD709383539CAEB6926361FC4A027CCD0BCA71452 ();
// 0x000000E9 System.Void Byn.Unity.Examples.MinimalConference::Update()
extern void MinimalConference_Update_m1E543A877B5F5901CB623B0B41140C5F4B14A612 ();
// 0x000000EA System.Void Byn.Unity.Examples.MinimalConference::.ctor()
extern void MinimalConference__ctor_m70C9B2070A40A2182600AF14341195C1563CF212 ();
// 0x000000EB System.Void Byn.Unity.Examples.MinimalMediaNetwork::Start()
extern void MinimalMediaNetwork_Start_m67A8E4255492E101A1B9B01A0A76FE0785D1CF48 ();
// 0x000000EC System.Void Byn.Unity.Examples.MinimalMediaNetwork::SetupReceiver()
extern void MinimalMediaNetwork_SetupReceiver_mF9169D17C93A77272ABED14991AE0A930DB729FB ();
// 0x000000ED System.Void Byn.Unity.Examples.MinimalMediaNetwork::UpdateReceiver()
extern void MinimalMediaNetwork_UpdateReceiver_m88A7CAA251F68F45EE0151FCB65225273D6F7DC0 ();
// 0x000000EE System.Void Byn.Unity.Examples.MinimalMediaNetwork::SenderSetup()
extern void MinimalMediaNetwork_SenderSetup_m3B9EE7116873C0680F3DE19824A59CD08DE5E15F ();
// 0x000000EF System.Void Byn.Unity.Examples.MinimalMediaNetwork::UpdateSender()
extern void MinimalMediaNetwork_UpdateSender_m31E047FF50C0DE17FB14D97EDEA292C7A83EB756 ();
// 0x000000F0 System.Void Byn.Unity.Examples.MinimalMediaNetwork::OnDestroy()
extern void MinimalMediaNetwork_OnDestroy_mF845845D92F4AB959741E8354D5E2C8F26475B68 ();
// 0x000000F1 System.Void Byn.Unity.Examples.MinimalMediaNetwork::Update()
extern void MinimalMediaNetwork_Update_mAFCFCEDAA268878C5107181FD968ABB1EA389E06 ();
// 0x000000F2 System.Void Byn.Unity.Examples.MinimalMediaNetwork::.ctor()
extern void MinimalMediaNetwork__ctor_m9C0F657161CB6D124DA95DC0D0B003AC547F5360 ();
// 0x000000F3 System.Void Byn.Unity.Examples.SimpleCall::Start()
extern void SimpleCall_Start_m5F7263DF1F2D97B5F56944103DF0E5989F3795CB ();
// 0x000000F4 System.Void Byn.Unity.Examples.SimpleCall::Configure()
extern void SimpleCall_Configure_m05205E971355C90BBDC6A080B43AF667F19A1C6D ();
// 0x000000F5 System.Collections.IEnumerator Byn.Unity.Examples.SimpleCall::ConfigureDelayed(System.Single)
extern void SimpleCall_ConfigureDelayed_m773B6E987830FC3B5CE43E3FF2B96C2C98731352 ();
// 0x000000F6 System.Void Byn.Unity.Examples.SimpleCall::Update()
extern void SimpleCall_Update_mB4B1D6991716C9E7263E0536AC4716DB1E3DBB80 ();
// 0x000000F7 System.Void Byn.Unity.Examples.SimpleCall::OnDestroy()
extern void SimpleCall_OnDestroy_m32FC313760CFAE6C26E1F3E7155F870E0CAD27D1 ();
// 0x000000F8 System.Void Byn.Unity.Examples.SimpleCall::Cleanup()
extern void SimpleCall_Cleanup_m11E6121FE4FC3C1E85C3B79682057B3F1CB28D15 ();
// 0x000000F9 System.Void Byn.Unity.Examples.SimpleCall::Call_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void SimpleCall_Call_CallEvent_m7190B7ED6A86BB56F111E170A493575E140D47D7 ();
// 0x000000FA System.Void Byn.Unity.Examples.SimpleCall::Call()
extern void SimpleCall_Call_m66A64DAB81546558B2245DE673CB36C2C1AE2FC3 ();
// 0x000000FB System.Void Byn.Unity.Examples.SimpleCall::Error(System.String)
extern void SimpleCall_Error_mEE0E0AC62DECDC8A65C883F41DB5F17C16143667 ();
// 0x000000FC System.Void Byn.Unity.Examples.SimpleCall::Log(System.String)
extern void SimpleCall_Log_m15F7DD2B12450F342BDDC82D660A9AFEEEA48809 ();
// 0x000000FD System.Void Byn.Unity.Examples.SimpleCall::.ctor()
extern void SimpleCall__ctor_m9B81D63BE5DEE3856ED907D3632DA0C27366B3C2 ();
// 0x000000FE System.Collections.IEnumerator Byn.Unity.Examples.OneToMany::Start()
extern void OneToMany_Start_m5E66134B6AC6BD19EA8169CBE17B66CA58DA89E9 ();
// 0x000000FF System.Void Byn.Unity.Examples.OneToMany::OnDestroy()
extern void OneToMany_OnDestroy_m9C3D4D4F25E5753F1C5C8A70DA7290DD5140B2A5 ();
// 0x00000100 System.Void Byn.Unity.Examples.OneToMany::Update()
extern void OneToMany_Update_mEC42CBD18B8AEA2BB3CBD6762F0A3C4AAFE360C7 ();
// 0x00000101 System.Void Byn.Unity.Examples.OneToMany::HandleMediaEvents()
extern void OneToMany_HandleMediaEvents_m21948326AD1CE64AAA97A827DEBE70E29F0AAD2A ();
// 0x00000102 System.Void Byn.Unity.Examples.OneToMany::Log(System.String)
extern void OneToMany_Log_mECC565C25EF0ECC3864B9AB6F47E8C99C1ADF94B ();
// 0x00000103 System.Void Byn.Unity.Examples.OneToMany::HandleNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void OneToMany_HandleNetworkEvent_mCBC83E6D7FF2CD6ADAE65094ADC374DC23DB4B78 ();
// 0x00000104 System.Void Byn.Unity.Examples.OneToMany::UpdateTexture(Byn.Awrtc.IFrame)
extern void OneToMany_UpdateTexture_mD9570467CE0922B017796B54F640C0C5D12A0FB3 ();
// 0x00000105 System.Boolean Byn.Unity.Examples.OneToMany::UpdateTexture(UnityEngine.Texture2D&,Byn.Awrtc.IFrame)
extern void OneToMany_UpdateTexture_m35992CA437FDFC457E758018CC51370E15864BB5 ();
// 0x00000106 System.Void Byn.Unity.Examples.OneToMany::.ctor()
extern void OneToMany__ctor_m06808C84149389978ACF34E1B6FB34CE90BFB28A ();
// 0x00000107 System.Void Byn.Unity.Examples.OneToMany::.cctor()
extern void OneToMany__cctor_m30316A72605004BAE3CAB4EBD8A2E661C474F9ED ();
// 0x00000108 System.Void Byn.Unity.Examples.ConferenceApp::Start()
extern void ConferenceApp_Start_mE18918E7A96C352B334233F7589B972A927F5153 ();
// 0x00000109 System.Void Byn.Unity.Examples.ConferenceApp::Setup(System.Boolean,System.Boolean)
extern void ConferenceApp_Setup_m9A69B2CA704D2DD01AADDF785DFB0085FD57920F ();
// 0x0000010A System.Void Byn.Unity.Examples.ConferenceApp::ResetCall()
extern void ConferenceApp_ResetCall_m670ECF09AD192DAFAED9337F4435AD728F998D72 ();
// 0x0000010B System.Void Byn.Unity.Examples.ConferenceApp::Call_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void ConferenceApp_Call_CallEvent_mFD5EE4ACE82E59052C24095DB8BC7D01E0FA2442 ();
// 0x0000010C System.Void Byn.Unity.Examples.ConferenceApp::OnNewCall(Byn.Awrtc.CallAcceptedEventArgs)
extern void ConferenceApp_OnNewCall_mE6F85ECDA4804F10B751CD95220349AFB4E9B4AE ();
// 0x0000010D System.Void Byn.Unity.Examples.ConferenceApp::SetupVideoUi(Byn.Awrtc.ConnectionId)
extern void ConferenceApp_SetupVideoUi_m767C5959177609E4A292CC85829D237B1D0C4AC4 ();
// 0x0000010E System.Void Byn.Unity.Examples.ConferenceApp::OnCallEnded(Byn.Awrtc.CallEndedEventArgs)
extern void ConferenceApp_OnCallEnded_m6CC967DD3B728EB8A3BA077F204FDD154F40D207 ();
// 0x0000010F System.Void Byn.Unity.Examples.ConferenceApp::UpdateTexture(UnityEngine.Texture2D&,Byn.Awrtc.IFrame)
extern void ConferenceApp_UpdateTexture_m45D3CF38450E57E0022F9C39D3B1BA1061DFE420 ();
// 0x00000110 System.Void Byn.Unity.Examples.ConferenceApp::UpdateFrame(Byn.Awrtc.ConnectionId,Byn.Awrtc.IFrame)
extern void ConferenceApp_UpdateFrame_mB55A69160707D1E5E4C4AF1B799E70FBC503BB18 ();
// 0x00000111 System.Void Byn.Unity.Examples.ConferenceApp::CleanupCall()
extern void ConferenceApp_CleanupCall_mF26C3A64CAD0AE23317BB241E09D2020754D2577 ();
// 0x00000112 System.Void Byn.Unity.Examples.ConferenceApp::OnDestroy()
extern void ConferenceApp_OnDestroy_mD87B8BBF8065AAA648573D5D7C663C434B4F55E8 ();
// 0x00000113 System.Void Byn.Unity.Examples.ConferenceApp::AudioToggle(System.Boolean)
extern void ConferenceApp_AudioToggle_mB0DF08112D962F90747B639457769DAC83FE96F6 ();
// 0x00000114 System.Void Byn.Unity.Examples.ConferenceApp::VideoToggle(System.Boolean)
extern void ConferenceApp_VideoToggle_m53FB09CC4C0D76FB5B2D905AA92E38A9325FB69F ();
// 0x00000115 System.Void Byn.Unity.Examples.ConferenceApp::Append(System.String)
extern void ConferenceApp_Append_m147E3DAA00A096BC19CCB6DC47654D4F54655937 ();
// 0x00000116 System.Void Byn.Unity.Examples.ConferenceApp::Update()
extern void ConferenceApp_Update_mA6AED841801DBB80480BF4B2A3904882C6CD8AE5 ();
// 0x00000117 System.Void Byn.Unity.Examples.ConferenceApp::SetGuiState(System.Boolean)
extern void ConferenceApp_SetGuiState_m2F0FFFE8B39F1BF9718FBEBDBCCB8393370DF297 ();
// 0x00000118 System.Void Byn.Unity.Examples.ConferenceApp::JoinButtonPressed()
extern void ConferenceApp_JoinButtonPressed_m52041004447BDF68FB624694DDF5ADFBD70B43FD ();
// 0x00000119 System.Void Byn.Unity.Examples.ConferenceApp::EnsureLength()
extern void ConferenceApp_EnsureLength_m55439CDE9A52BA557B64C08F219926067C1BF0BD ();
// 0x0000011A System.Void Byn.Unity.Examples.ConferenceApp::SendButtonPressed()
extern void ConferenceApp_SendButtonPressed_m1C76D3E7213E564E8948275306B68A212114EB11 ();
// 0x0000011B System.Void Byn.Unity.Examples.ConferenceApp::InputOnEndEdit()
extern void ConferenceApp_InputOnEndEdit_m0E5045AC71F40DF694D7D15525ED0709B07DF311 ();
// 0x0000011C System.Void Byn.Unity.Examples.ConferenceApp::SendMsg(System.String)
extern void ConferenceApp_SendMsg_mCA215518E76BBD2BD50C2E2F287021439C9029CD ();
// 0x0000011D System.Void Byn.Unity.Examples.ConferenceApp::ShutdownButtonPressed()
extern void ConferenceApp_ShutdownButtonPressed_m32DA79B781B719D4379399CF609D439435DEB0F7 ();
// 0x0000011E System.Void Byn.Unity.Examples.ConferenceApp::.ctor()
extern void ConferenceApp__ctor_m66FC69875CA6EBE652F556F1FE9E185DF1B99A71 ();
// 0x0000011F System.Void Byn.Unity.Examples.VideoInputApp::Start()
extern void VideoInputApp_Start_m9FEC6A7E4F0029494496828E50AF0F0D59308BD9 ();
// 0x00000120 System.Collections.IEnumerator Byn.Unity.Examples.VideoInputApp::CoroutineRefreshLater()
extern void VideoInputApp_CoroutineRefreshLater_m0DC0FB9BC73DD2346ADDC2FA8FD8F67658014A20 ();
// 0x00000121 System.Void Byn.Unity.Examples.VideoInputApp::.ctor()
extern void VideoInputApp__ctor_m84F9D04A9D54B1D3AA457751A0EF45898A9E835E ();
// 0x00000122 System.Void Byn.Unity.Examples.VirtualCamera::Awake()
extern void VirtualCamera_Awake_mDB6D605995BA4E272444329B233589FB1B200AF0 ();
// 0x00000123 System.Void Byn.Unity.Examples.VirtualCamera::Start()
extern void VirtualCamera_Start_m878B27DC5834874DC83A441F0DE0A47EA79B46B5 ();
// 0x00000124 System.Void Byn.Unity.Examples.VirtualCamera::OnDestroy()
extern void VirtualCamera_OnDestroy_mA81268441D178303DD568DE19FA35154728D3DFA ();
// 0x00000125 System.Void Byn.Unity.Examples.VirtualCamera::Update()
extern void VirtualCamera_Update_mD56F0D77FFDA77A6B32EF7AE278BFBB94C4A731F ();
// 0x00000126 System.Void Byn.Unity.Examples.VirtualCamera::.ctor()
extern void VirtualCamera__ctor_m4C826FC90090C6BC41B74A86011618536ADE48CE ();
// 0x00000127 System.Void CallApp_<CoroutineRejoin>d__37::.ctor(System.Int32)
extern void U3CCoroutineRejoinU3Ed__37__ctor_mD1B502E34B1D61EC4A7B3FD4695B18C00500D1B2 ();
// 0x00000128 System.Void CallApp_<CoroutineRejoin>d__37::System.IDisposable.Dispose()
extern void U3CCoroutineRejoinU3Ed__37_System_IDisposable_Dispose_mA27BE44645CD76FA4D57A1A1464E1FFFAE347527 ();
// 0x00000129 System.Boolean CallApp_<CoroutineRejoin>d__37::MoveNext()
extern void U3CCoroutineRejoinU3Ed__37_MoveNext_m71D5FB4CE137B3410136088D522FC695CF2A2D5A ();
// 0x0000012A System.Object CallApp_<CoroutineRejoin>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineRejoinU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C61FF934D7A8B5B6D386BCD665E10FA62459C2A ();
// 0x0000012B System.Void CallApp_<CoroutineRejoin>d__37::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineRejoinU3Ed__37_System_Collections_IEnumerator_Reset_m9AC7FBC17FD724D82A1F47FD32617D9D6C92AADC ();
// 0x0000012C System.Object CallApp_<CoroutineRejoin>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineRejoinU3Ed__37_System_Collections_IEnumerator_get_Current_m62F63F8E714FC48722E1150FA0C92BFEFFDD7359 ();
// 0x0000012D System.Void CallAppUi_<RequestAudioPermissions>d__74::.ctor(System.Int32)
extern void U3CRequestAudioPermissionsU3Ed__74__ctor_m060EFD836B4B03C51ACF5DA3112629319C581859 ();
// 0x0000012E System.Void CallAppUi_<RequestAudioPermissions>d__74::System.IDisposable.Dispose()
extern void U3CRequestAudioPermissionsU3Ed__74_System_IDisposable_Dispose_m82AA31223CD369B823A934BF55EA25777001B676 ();
// 0x0000012F System.Boolean CallAppUi_<RequestAudioPermissions>d__74::MoveNext()
extern void U3CRequestAudioPermissionsU3Ed__74_MoveNext_m59864EFAA1DB04F899F8D676D9EC435AA68A1F25 ();
// 0x00000130 System.Object CallAppUi_<RequestAudioPermissions>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m041FA6BF7F528AC7C49DBC8D10AC418B6FEDFA68 ();
// 0x00000131 System.Void CallAppUi_<RequestAudioPermissions>d__74::System.Collections.IEnumerator.Reset()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_Reset_mD2B94CB57809935EF3D74E095F23C8ABAE03FEBE ();
// 0x00000132 System.Object CallAppUi_<RequestAudioPermissions>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_get_Current_mFD848DA9A308905F9A88E65BDEF37828CB440E1F ();
// 0x00000133 System.Void CallAppUi_<RequestVideoPermissions>d__75::.ctor(System.Int32)
extern void U3CRequestVideoPermissionsU3Ed__75__ctor_mAADB433D086D489AC6552794C770B50FB533ACC8 ();
// 0x00000134 System.Void CallAppUi_<RequestVideoPermissions>d__75::System.IDisposable.Dispose()
extern void U3CRequestVideoPermissionsU3Ed__75_System_IDisposable_Dispose_mD2BE8DAE78D08CBBFDB9BF022F4C03E92F4F84D2 ();
// 0x00000135 System.Boolean CallAppUi_<RequestVideoPermissions>d__75::MoveNext()
extern void U3CRequestVideoPermissionsU3Ed__75_MoveNext_mC298BA7B0CF49F838882196B545BCFE0B0BE5A11 ();
// 0x00000136 System.Object CallAppUi_<RequestVideoPermissions>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0551FDBD80A0E439FDE322CD1C821B0F0558E5CA ();
// 0x00000137 System.Void CallAppUi_<RequestVideoPermissions>d__75::System.Collections.IEnumerator.Reset()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_Reset_mCBBD4467FA5265B0CC56E4F06960C23C3EDC112F ();
// 0x00000138 System.Object CallAppUi_<RequestVideoPermissions>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_get_Current_mD6001BB9D7DA7B7624138CD630E5F266E753CD4A ();
// 0x00000139 System.Void Byn.Awrtc.Unity.UnityCallFactory_InitCallbacks::.ctor(System.Action,System.Action`1<System.String>)
extern void InitCallbacks__ctor_mA2B350544DA42945B9970C005AB03E4A5681830A ();
// 0x0000013A System.Void Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::.ctor(System.Int32)
extern void U3CCoroutineInitAsyncU3Ed__28__ctor_m8F57AF53374BEFC030571B725AE1293DCCA33BFE ();
// 0x0000013B System.Void Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::System.IDisposable.Dispose()
extern void U3CCoroutineInitAsyncU3Ed__28_System_IDisposable_Dispose_m2C384741A453655830EA579DC283BB7E1701655D ();
// 0x0000013C System.Boolean Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::MoveNext()
extern void U3CCoroutineInitAsyncU3Ed__28_MoveNext_m678ACA5E99BC41894B7E3383BBDE152DC6CF475A ();
// 0x0000013D System.Object Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E0F539E7CAF66A337918471031A394006F21DF ();
// 0x0000013E System.Void Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_Reset_mE707AC23887A894941744BAAEB292C74040ADB0F ();
// 0x0000013F System.Object Byn.Awrtc.Unity.UnityCallFactory_<CoroutineInitAsync>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_get_Current_m51E575B356036674708FD4BA079C37FD10480D74 ();
// 0x00000140 System.Void Byn.Awrtc.Unity.UnityCallFactory_<>c::.cctor()
extern void U3CU3Ec__cctor_m2C1230BA4890438D20836803A46E28EDE7DBD225 ();
// 0x00000141 System.Void Byn.Awrtc.Unity.UnityCallFactory_<>c::.ctor()
extern void U3CU3Ec__ctor_m4F98D6B5EB922D5BBB4B492490011C28638B29D8 ();
// 0x00000142 System.Boolean Byn.Awrtc.Unity.UnityCallFactory_<>c::<InitStatic>b__30_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3CInitStaticU3Eb__30_0_mB69BABE67F3A11F204B85AA3690D987470CFC3C9 ();
// 0x00000143 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::.ctor(System.Int32)
extern void U3CRequestAudioPermissionU3Ed__17__ctor_m8689010E40196D033215FB469771180EBE6414EE ();
// 0x00000144 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::System.IDisposable.Dispose()
extern void U3CRequestAudioPermissionU3Ed__17_System_IDisposable_Dispose_m0F2ED97AD82D011E6BD84E1CF74A043C3AA09E9B ();
// 0x00000145 System.Boolean Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::MoveNext()
extern void U3CRequestAudioPermissionU3Ed__17_MoveNext_mE8D7A424F8EA7129F2FDF184B1732BBBFAB6E09A ();
// 0x00000146 System.Object Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE7A6F2DE321FD8E63CE6B875EF82ACE56EEE8F6 ();
// 0x00000147 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::System.Collections.IEnumerator.Reset()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_Reset_m8CCB8694BE39456024CDA20FAB3C0B83B723E2BC ();
// 0x00000148 System.Object Byn.Unity.Examples.ExampleGlobals_<RequestAudioPermission>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_get_Current_m4A34AB671AFC805BF200375F156C3B6193AEAB66 ();
// 0x00000149 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::.ctor(System.Int32)
extern void U3CRequestVideoPermissionU3Ed__18__ctor_mEA740FC978D2A36C2E3E5891FF9AAC709940A4EB ();
// 0x0000014A System.Void Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::System.IDisposable.Dispose()
extern void U3CRequestVideoPermissionU3Ed__18_System_IDisposable_Dispose_m7F906171EA17401A8ADA2BD88AC0F9C2B3954649 ();
// 0x0000014B System.Boolean Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::MoveNext()
extern void U3CRequestVideoPermissionU3Ed__18_MoveNext_m8D5C47BFFCCB208397761411004689D919255894 ();
// 0x0000014C System.Object Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C8593402350FDF1080DB009DEF4A65CFBB2BC7F ();
// 0x0000014D System.Void Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::System.Collections.IEnumerator.Reset()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_Reset_m1A3B9B76771748D4B26D66C3B512EA8A7B44CDE8 ();
// 0x0000014E System.Object Byn.Unity.Examples.ExampleGlobals_<RequestVideoPermission>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_get_Current_m4ABDAD82F796C06C4A1BCBD9F566D0F5979F8CD8 ();
// 0x0000014F System.Void Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::.ctor(System.Int32)
extern void U3CRequestPermissionsU3Ed__19__ctor_mA6A1B53AABCBB0614AC02BF583D941F068F836EC ();
// 0x00000150 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::System.IDisposable.Dispose()
extern void U3CRequestPermissionsU3Ed__19_System_IDisposable_Dispose_m54DD727AEC651927261A4F7C37ADFD52ACF81779 ();
// 0x00000151 System.Boolean Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::MoveNext()
extern void U3CRequestPermissionsU3Ed__19_MoveNext_m5E19DCDAFA7DF393E6AA9DF150C97916C632F9AF ();
// 0x00000152 System.Object Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m850D371AD31B7F4368E442628840E1B01E849F23 ();
// 0x00000153 System.Void Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::System.Collections.IEnumerator.Reset()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_Reset_m01C71C986D0CAD438443FC8B7ECD74B553981497 ();
// 0x00000154 System.Object Byn.Unity.Examples.ExampleGlobals_<RequestPermissions>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_get_Current_m59CB0613E289C9C4D85CF6AFF2BDD62B0064C065 ();
// 0x00000155 System.Void Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::.ctor(System.Int32)
extern void U3CConfigureDelayedU3Ed__9__ctor_m37F18A3293A76751FEE52E94A4FF2FE3E5063D00 ();
// 0x00000156 System.Void Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::System.IDisposable.Dispose()
extern void U3CConfigureDelayedU3Ed__9_System_IDisposable_Dispose_mF66E1B1F0C8825871630313FBC3C301BA6DA92C6 ();
// 0x00000157 System.Boolean Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::MoveNext()
extern void U3CConfigureDelayedU3Ed__9_MoveNext_m4FCC498C6A00A081E55AB0568377617C3B425BB2 ();
// 0x00000158 System.Object Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConfigureDelayedU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AD69A1471309A6821A3F2C2E0F95EE662F96EC6 ();
// 0x00000159 System.Void Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::System.Collections.IEnumerator.Reset()
extern void U3CConfigureDelayedU3Ed__9_System_Collections_IEnumerator_Reset_m6A6B3B3EE9136E2B5B48587D121502BB4E955A55 ();
// 0x0000015A System.Object Byn.Unity.Examples.SimpleCall_<ConfigureDelayed>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CConfigureDelayedU3Ed__9_System_Collections_IEnumerator_get_Current_m38C668EA7EDD1445F5F8B910AD5EC31F525AE0C0 ();
// 0x0000015B System.Void Byn.Unity.Examples.OneToMany_<Start>d__11::.ctor(System.Int32)
extern void U3CStartU3Ed__11__ctor_m9384D44968255D74485EF9DF1DBCE197FCEB0AC8 ();
// 0x0000015C System.Void Byn.Unity.Examples.OneToMany_<Start>d__11::System.IDisposable.Dispose()
extern void U3CStartU3Ed__11_System_IDisposable_Dispose_m6E992EE555DAD47ADC9C9D0F91A01BF1A717E163 ();
// 0x0000015D System.Boolean Byn.Unity.Examples.OneToMany_<Start>d__11::MoveNext()
extern void U3CStartU3Ed__11_MoveNext_mC0383BC521EC359E4374BD4828A32E71C2991340 ();
// 0x0000015E System.Object Byn.Unity.Examples.OneToMany_<Start>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC05C50BFFB86E3CA5ADA6CD3FBB940143F1975 ();
// 0x0000015F System.Void Byn.Unity.Examples.OneToMany_<Start>d__11::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__11_System_Collections_IEnumerator_Reset_mD3BD3F43A7CC10ED092EB99274D90D9CC7B9FD23 ();
// 0x00000160 System.Object Byn.Unity.Examples.OneToMany_<Start>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__11_System_Collections_IEnumerator_get_Current_mD1948AF9EDD4C398E2E4923B225AF7E8D1AE9BE7 ();
// 0x00000161 System.Void Byn.Unity.Examples.ConferenceApp_VideoData::.ctor()
extern void VideoData__ctor_m5E2A8054A546CE82A868E96E38EB0DFDDB95C966 ();
// 0x00000162 System.Void Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::.ctor(System.Int32)
extern void U3CCoroutineRefreshLaterU3Ed__1__ctor_m916AC23DC1B5C4BDBC05393A3EC7610798B82DA8 ();
// 0x00000163 System.Void Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::System.IDisposable.Dispose()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_IDisposable_Dispose_m8530188A2DD24E617730B016B26A9A1A667DE68B ();
// 0x00000164 System.Boolean Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::MoveNext()
extern void U3CCoroutineRefreshLaterU3Ed__1_MoveNext_m06CE2E9B96FD2225138519D1848947C5ADB616D6 ();
// 0x00000165 System.Object Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D1CA13D00CC51A772B7839514307DD21324CAE ();
// 0x00000166 System.Void Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_Reset_m93927830566AF43778CBB194F4B359E1F8758B42 ();
// 0x00000167 System.Object Byn.Unity.Examples.VideoInputApp_<CoroutineRefreshLater>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_get_Current_mAB7374B9D918A1322B2C172C2612BD34A9503BA7 ();
static Il2CppMethodPointer s_methodPointers[359] = 
{
	MenuScript_Awake_mA33B0EB58FAFDEB55A90B1CD71033CB6F098A0B6,
	MenuScript_Start_m5AE71CA86E0D0ABD34A9AE132E4D6D32B424E10A,
	MenuScript_PrintDeviceDebug_m68D354157F305EF37F28BB0271D2A75C3A74A8F4,
	MenuScript_Update_m19A50B38E1ED3C767EB24BEE703A4CAEBA5780C0,
	MenuScript_Initialize_m704CD615E8D8F6056152082956239C4A11FDC027,
	MenuScript_ExampleUi_m396469AEAB6F9B5FF0669E0F80D96F0709257822,
	MenuScript_MenuUi_mEB2234F36985E9C50159A848C90ADF68C2F70B89,
	MenuScript_LoadExample_m3B45C230C206A89CE081E2DAE19F14DA48C11255,
	MenuScript_LoadChatExample_m0BB9A380651118A8E6F9C5BFCF5C301A61004F41,
	MenuScript_LoadCallExample_m11FA93AC71DDB29A41DC7E12974611B7B0BA9E73,
	MenuScript_LoadConference_mFFB2358A163B52BC9E26F4BDA5133544171B9B8B,
	MenuScript_LoadMenu_m6386F0D61D025A78799D7D6CBAAB75670A4E16EF,
	MenuScript__ctor_m661378E3B0CF8824BD45D2F409C3B836CB58475B,
	MenuScript__cctor_m1D6ABF5AB88C2FCF2ABF3EC29D5F461E0174C42F,
	CallApp_Awake_mA390CA53B3E3C6120C3345F47CD4E695F6B86663,
	CallApp_Start_mFAB0551D6BE0A4271705C866F463413FC2249123,
	CallApp_OnCallFactoryReady_mC566566C272C9DF6041BB4BFBF79D85E25A8B571,
	CallApp_OnCallFactoryFailed_m5CA93D81AAA7E2C08378C785CF0D39C0334D858D,
	CallApp_OnDestroy_mB81E5B8625385904C690D7C902BFF99BA879D8EA,
	CallApp_Update_m947A5D1902D6704452D2F80D7A34413BCF8DB20A,
	CallApp_CreateNetworkConfig_m84E66C05AD9BD3C2C8EF146B151671FB06ECBC61,
	CallApp_SetupCall_m060DB8EF8DA417267A871655C7762A11B5287318,
	CallApp_CreateCall_mA3C3C9B367DCE0D1BBED1ED92BB1B4CD6566EF82,
	CallApp_Call_CallEvent_m1D1CBE40591F528C00BE3BF12071C10F9E7B1A2C,
	CallApp_CleanupCall_m8F5ED4E9181B51A97727DF9D041E328CCCE60958,
	CallApp_CreateMediaConfig_m36481A7523D0BEF94B8006E8AE066845773AD1FF,
	CallApp_ResetCall_m23FD10964DB672BC57F841508688E6AC3537AFCE,
	CallApp_InternalResetCall_m153BB35DEF39BBC28166BFCFFD49FA4E83C16955,
	CallApp_SetRemoteVolume_mC3BBDBF465CD4DDB30EF7E65CE6598F207EE9C78,
	CallApp_GetVideoDevices_m08983BEA0886D0157B754EE8D5AC37DF466B22FA,
	CallApp_CanSelectVideoDevice_mDB20B9F51BCE136EF0DAF658412CF0935FC20E90,
	CallApp_Join_mBFDBD97F6386A7EB1A9552283E9F1A9D043A7C6C,
	CallApp_InternalJoin_m881F47C548398396243B871B877D3C8176CB04E2,
	CallApp_CoroutineRejoin_m93ED87BB92A881D947193C7055243071508665E1,
	CallApp_Send_m36CAF9AC69322612B2488BA9B5D34B1412556231,
	CallApp_SetAudio_mEA0AAAFF9B540F3027E822F2F3F7CDAAEF916EED,
	CallApp_SetVideo_m35F2399DF3D74346EA980A57E545846AABB573E0,
	CallApp_SetVideoDevice_mB0ECAC56197F8E21BE6A551FE1CF0D748B5611D1,
	CallApp_SetIdealResolution_mF9F798AAE77F6167561217E6A2BBB8F8F4AD3FED,
	CallApp_SetIdealFps_mBA9B22F12B7235D20A7FB74566695D5043E234D0,
	CallApp_SetShowLocalVideo_m7174A2E84201A0F2644524F958D3171DA3BA5014,
	CallApp_SetAutoRejoin_m52FF3427328D18526F14E4A4CE876273C1EE9197,
	CallApp_GetLoudspeakerStatus_m477CFD136C49914FB5F2C4AC4AB313EC081F16FD,
	CallApp_SetLoudspeakerStatus_mEFDF2689F4B568A17BDDF911ED02C13F1E54191F,
	CallApp_SetMute_m97AD18E0880622F66BD325D80B7517DF454EECE9,
	CallApp_IsMute_m3BC5714771935410B0F5019D0305B1663D123D9D,
	CallApp_UpdateFrame_m6B942EA3630E2C505B44C3F3CBF3305B4556A1CE,
	CallApp_Append_m0B565CBF03C1CBA19703E4BAB6C2B1C22D471ACF,
	CallApp__ctor_m491742871D4A65E756C5E9AD67248CF94E3897A0,
	CallAppUi_Awake_mD13B04646FD8711E9B835631ED336777708A28A8,
	CallAppUi_Start_m51F99808455AF5194136D19A63D12DF57951527B,
	CallAppUi_SaveSettings_m47BEDF75B0FDDEA561EE300846AB4ED95077BE39,
	CallAppUi_LoadSettings_m51D05A79A1DF3A2ED95CA410F9A8CAB51EAB029D,
	CallAppUi_ResetSettings_mFB45BD6F3637B04BFBE4EDC0DEF318D2B524E2FC,
	CallAppUi_CheckSettings_m841568C6380D8A6CB77888A390737EA0401F7D23,
	CallAppUi_OnAudioSettingsChanged_mB722644EA28EB799D35F63F2C1AE435C077FA1FE,
	CallAppUi_OnVideoSettingsChanged_mE8E3112163FAAD063A1C701980B04BB0977D96A9,
	CallAppUi_RequestAudioPermissions_m641C0393B73F9A00336F3220D9D1AD38CE9B93F2,
	CallAppUi_RequestVideoPermissions_m7DBB2E55BC770696AEBC7282AA12B831805AF9AB,
	CallAppUi_PlayerPrefsGetBool_m04EA6B094E3EC7C53272837A1DC12E587CA891BD,
	CallAppUi_PlayerPrefsSetBool_mFFD9DCF57125E23ADB8EC6B10FCDC6B307581240,
	CallAppUi_GetSelectedVideoDevice_m4735849D5A1D482F68C2BA8267216BC12D05236C,
	CallAppUi_TryParseInt_mE3BA12A40BDB63217AFA59E88BB3947CB3114462,
	CallAppUi_SetupCallApp_m94FD669EE866373A849F1333FFA69F37A96F8D4C,
	CallAppUi_ToggleSettings_mE0A543A7F02585FE0977D21D81092B22966FE600,
	CallAppUi_ToggleSetup_m598C4B40285808DFEB292E40B7C696A0449A1359,
	CallAppUi_UpdateLocalTexture_m0B1B88BDFFB58B52F60710819ED976D2C42A7370,
	CallAppUi_UpdateRemoteTexture_m14B0B2709FC6615A95FA1C8ECF4A47611B66D909,
	CallAppUi_UpdateVideoDropdown_mD09DFB271783FAB213316553A93086472E01D486,
	CallAppUi_VideoDropdownOnValueChanged_mA134DBD254ED14DA4E8E15571338BB766AABE7D6,
	CallAppUi_Append_m76BB3D59E514B8DF593E1D0EAC1C0A2393104A58,
	CallAppUi_SetFullscreen_m03B1D14080F7CEBBA79DA6D545F1D3CC2BC0E2CF,
	CallAppUi_Fullscreen_mBC337359E0EED2DFBED91D32574D0299D6E6199C,
	CallAppUi_ShowOverlay_m3302CDEB331AEFDCFFAF88CC07258351FF570B54,
	CallAppUi_SetGuiState_m7F9223EC7706A73D63FA22508EB45DB68AF87934,
	CallAppUi_JoinButtonPressed_m86D7577CBE43040ED032A2188BAE4002A2B97D55,
	CallAppUi_EnsureLength_mC30EE5D573E2623CA9C7EDD92FD07CAEE9574471,
	CallAppUi_GetRoomname_m592EF5403E2537254451202D6D4AB620A9805AA4,
	CallAppUi_SendButtonPressed_mD137E3BA43B87D80F497E31482681CCED8C8E47D,
	CallAppUi_InputOnEndEdit_m4CCE1DFF0F877B13FFD9410E9F1F6818DA6C8D2A,
	CallAppUi_SendMsg_m80FD1955E11FF919435B509BB97CC88D2724D7C5,
	CallAppUi_ShutdownButtonPressed_mC094512441733A06630EC0D1EF3EEE7913EA9997,
	CallAppUi_OnVolumeChanged_mFE4C566731AEA535B19D4375D67AAC7FE0B8FC0E,
	CallAppUi_OnLoudspeakerToggle_m966ACEE3F2D177FE74640098249B53F3ED7CADFB,
	CallAppUi_RefreshLoudspeakerToggle_m2663B1F2CFB2438B691B4BFEA5C8CA309A904409,
	CallAppUi_OnMuteToggle_mC9785BFE67CA920B91A5A4F06990897B16C792D6,
	CallAppUi_RefreshMuteToggle_mD3B5093787E58C35AA916634CD83AD62443F015D,
	CallAppUi_Update_mC18B04E5C852D8DEA1CAF67641ECE2038B80439C,
	CallAppUi__ctor_m7A594459FFDCA245FD56036C0D997F4D077709A8,
	CallAppUi__cctor_mFD464EDABF2BC6FB6154E96FA1DE4B7A90CDD529,
	ImgFitter_Update_m61E62AC0EE9A6EEFC5E06D6989F34C5F9C47EE9C,
	ImgFitter_Abs_m0B5094618DBC042A9AF02CD6BF2BDD55BF267B34,
	ImgFitter__ctor_m3804438C3B758EFDFAAC1BD83491A497E1DD9C24,
	VideoPanelEventHandler_Start_m298E99DDCBA3DFE98B2432B3B3B504418DCA2F47,
	VideoPanelEventHandler_OnPointerClick_m32D502BDBF6EADB644B08F0ED733126F04ED33B0,
	VideoPanelEventHandler__ctor_m24F7289F2349E9CDF9FD4AB6A56835CEC4C6800C,
	ChatApp_Start_mBF9CD536023C6C4A4D1834D9EFF585BE44749EC5,
	ChatApp_Setup_mB65C55B10ABC16045F353F3AB3278A140698C201,
	ChatApp_Reset_mA5DA83EC38550B085C93830D94FD95B4C4D8AF36,
	ChatApp_Cleanup_m5D41E4A5D726FC39516768B154D1E3011853CF46,
	ChatApp_OnDestroy_mD5FA54180D1363CE78E62BCA4A24EE79FD58E5D3,
	ChatApp_FixedUpdate_mF086688D74970182E7DE31D761DBEF875510A28C,
	ChatApp_HandleNetwork_mB0C8CF174DC0871D44E614C27867F26608883EC1,
	ChatApp_HandleIncommingMessage_m2587664A0FB6C2806EBACFEC90D4B77E50A1270F,
	ChatApp_SendString_m7131233BF68865A227D6C35B970F1F13811302B0,
	ChatApp_Append_m439CEF4B1783BDD7580FA2AFAFB628C928147FE7,
	ChatApp_SetGuiState_m4B9A975948F1DC16A33799337FF1DA3D2A05964C,
	ChatApp_JoinRoomButtonPressed_m54E3F962894ABBE52B943C4C83EF733BFA1264F4,
	ChatApp_EnsureLength_m38E14457833C6B239638D4E90CA25DC80AFF1280,
	ChatApp_LeaveButtonPressed_m56E3D10F2DA3C0B847052C5EB55DD8C50AE94025,
	ChatApp_OpenRoomButtonPressed_mD9A38AE6141BBE0252513CC263C99A91EC1DC5DC,
	ChatApp_InputOnEndEdit_mEA5E2718DA0B6C799176A0E5500CC666E50B84D6,
	ChatApp_SendButtonPressed_m5765902C8332144D75EB36DC020FCBCC27DC62B8,
	ChatApp__ctor_m3C2B32278520EAF4F697D49576BF6DDD7F9BF837,
	MessageList_Awake_m4781F986E39C5766E6A3A9901A2AE7DF97A898F3,
	MessageList_Start_mF0C05354CB53CE8FFD53B9F85796755710EB5A5E,
	MessageList_AddTextEntry_m29B65540BDE535E00560031F7A3766BFF3B5898D,
	MessageList_Update_m58BE8170943CF42A281BBCFD14B2792ADA85500E,
	MessageList__ctor_mDC3DB0C027072C804BAAB1460F55C9207DA3A6C9,
	GridManager_Update_mCEBBAD267A803E6B9CD9AD5C7AE7CC78494DAF26,
	GridManager_Refresh_m23E05976600CF5BE206076FD41AC964794C7996F,
	GridManager__ctor_m0C0B6E2FFF0CFA78E5C9C090A1590AB81E19D73C,
	UnityCallFactory_get_Instance_m0EB74D642FE08CE87B0A5B4303092061996B784D,
	UnityCallFactory_AddInitCallbacks_m2C02DCF8C1B9EE13F2BB866C5F24B61D39A31899,
	UnityCallFactory_EnsureInit_m4CF59D37727912824F0446056631EA6B0B0A2E09,
	UnityCallFactory_EnsureInit_m08767FCA47AB7502F90AE88470CD52AD544BD55B,
	UnityCallFactory_Init_m5359AB034B24C0F2AA4470065BE37501DA0CEF48,
	UnityCallFactory_TriggerOnInitFailed_m53620CA810CA943034A5FC1FB67DFA54411D1DC7,
	UnityCallFactory_TriggerInitFailed_m0BD17DA470FAAC8FD51A417F36353911E1488039,
	UnityCallFactory_TriggerInitSuccess_m374D7A85B5587BE2B1DC295EC76AAA4F50B0BDE7,
	UnityCallFactory_TriggerFailedCallback_m21316F9BF3376C5EB0667365CEC98CE2A3B31A9C,
	UnityCallFactory_TriggerSuccessCallback_mD44AFD6C4591167D594B95285E29B7DDBE8454EC,
	UnityCallFactory_CoroutineInitAsync_m8A5680BA9807081220F47E1BA1F7E67EED3B8A86,
	UnityCallFactory_CreateSingleton_m04C4A9B70CE9DBFF59822325715D0FEE45BDBC15,
	UnityCallFactory_InitStatic_m9CBADB7A0FC5C35DAE0C74612E8234B5FD15D23D,
	UnityCallFactory_IsInitStaticComplete_m86C12DCA840650FB992FDF0E2439A6A6390A7EC1,
	UnityCallFactory_InitObj_mD48DB538773197FE88E73BA30FD18EF65F9CD3EC,
	UnityCallFactory_OnDestroy_m0362CD8EB67E7AAA0A14D9589F3BCC8C4166A496,
	UnityCallFactory_get_InternalFactory_m1F6CFCD39ECA5137F81E3B17149D094BB20AB0EF,
	UnityCallFactory_get_VideoInput_m78E14A1A779DAEC9021605F06F7AE06B74AD0E2D,
	UnityCallFactory_Awake_m4B64D394A915AD1B3C87B18CAC8B3314AB979B6A,
	UnityCallFactory_Start_mCB79F4AC0F9387BA2170EC45C8D0384965FEEB2B,
	UnityCallFactory_Update_mB9FD6C8B518249874812C84176EC3FBCC0D9A759,
	UnityCallFactory_IsNativePlatform_m97B33983D94C5D85D5CA57D22A0E1F1E6162740E,
	UnityCallFactory_StaticInitBrowser_mFD619A72E0F8B55BF7E17073D046A23C0753991E,
	UnityCallFactory_StaticInitAndroid_m7B478A21EED5DBEDC9AF9B8ED70060EA9EA90DEA,
	UnityCallFactory_Create_mE17AFB1185D4EF5507F849106D4EF92AE75BC0AF,
	UnityCallFactory_CreateMediaNetwork_mEF3702498F2052816C46A2381A0ACF7EBD18E4C0,
	UnityCallFactory_CreateBasicNetwork_m6A7417D798623AF3FFE5428AF732AB2DD104AC30,
	UnityCallFactory_GetVideoDevices_m2E3C4A038415E64A6C9295067F325EEF668BEA98,
	UnityCallFactory_CanSelectVideoDevice_mDBA7869B06A5FF5AF609FB712ECA5ECEDC24EE9B,
	UnityCallFactory_IsFrontFacing_m4E2CCBD32F1130C0486EE02DDFB773F27D66B993,
	UnityCallFactory_GetDefaultVideoDevice_m509882C764FCE11EFF2B94E5ADA28386A26C3EAF,
	UnityCallFactory_EnterActiveCallState_m59E67D66EF29546D2B5540A20857D7A57773942F,
	UnityCallFactory_LeaveActiveCallState_m814E768370265AC986DE52F4574B6CED1278B70C,
	UnityCallFactory_Dispose_m0EF73E1A00FE92FC99FBAAE5950CDB449E550959,
	UnityCallFactory_SetLoudspeakerStatus_m586B5C1CB504D0422E7D5E07463119EC716489ED,
	UnityCallFactory_GetLoudspeakerStatus_mED5D8C4C252D0228847D2F8022FE1C8C95A22EA9,
	UnityCallFactory_RequestLogLevel_mC66C3ECDE43C19C710F87A4EDEFCFB6265291EEB,
	UnityCallFactory_SetDefaultLogger_mB560FDCD97B84F33E11FBEA0B2EACF170D6F9874,
	UnityCallFactory_UpdateLogLevel_WebGl_m27799888D5A77D36A40F4548DB5C34C733277E98,
	UnityCallFactory_OnLog_m0BF0592883807FA198839182601D400D284A69BF,
	UnityCallFactory_UnityLog_mBF4048FB900E0BB8A1537C22DDD0FD3375D61BF4,
	UnityCallFactory_SplitLongMsgs_mB5B9BE121C7209DB08C504F027AD04495E7E8E55,
	UnityCallFactory__ctor_m2CA8CB1B19087BE218AC06BCE756011363C2381D,
	UnityCallFactory__cctor_m6BA06AF9C3AFD471F00A91E97DC2FDEDBDB69D46,
	UnityMediaHelper_UpdateTexture_mD002B5D7A43163E66AF3ADAC54CC4A8ACCE25AD1,
	UnityMediaHelper_UpdateTexture_m4F0F65B86771B43BEADCE5ACFABCA52EFE4651BD,
	UnityMediaHelper_EnsureTex_m38018256FBDE55738E2407E6A551B9B2D071CD6C,
	UnityMediaHelper_UpdateTexture_m2C308FAD6F5EAA683C96109F51C6236A38451E41,
	UnityMediaHelper_UpdateRawImage_m7698CD80D54D0E5B8E76B8CFEBE88A2F6C64D7A0,
	UnityMediaHelper__ctor_m7C413E23BF0205C57CEF54C32BF7C34C303F1BA8,
	UnityMediaHelper__cctor_m0CC84612F20FC859022A281E2B89880AAE42C32A,
	NULL,
	NULL,
	NULL,
	NULL,
	AndroidHelper_IsFrontFacing_mBBCD7755BEC6FF374D671A9387EE660AF8E4D64D,
	AndroidHelper_IsBackFacing_mA88CA45333A6FE44475C3915149ABB165F10DC14,
	AndroidHelper_SetSpeakerOn_m8E14A23214B9B081DD54DDBA5BBCE3E17FEB6776,
	AndroidHelper_IsSpeakerOn_m291F45F32AC253660D91CB05F956CC92B09E9D14,
	AndroidHelper_GetMode_m5ADF731CC4C8219C697B465F2E13DADDF93B3687,
	AndroidHelper_SetMode_m88D2873451755C222BE41011AB8FADE686F8930E,
	AndroidHelper_IsModeInCommunication_m6AE018363B43BF872E3A2FFCF9C5C428AF3AA20C,
	AndroidHelper_SetModeInCommunicaion_m80898E40E8CB1E84B1A46D85D3AFC267FC28BA2B,
	AndroidHelper_GetAudioManagerMode_m206ADB726EFA7F05D839B6360D1B92989A52B647,
	AndroidHelper_SetModeNormal_mA117943A21312F345B67B781AD154950A31042E9,
	AndroidHelper_GetStreamVolume_m3DF5BF71B29A2E8D62277E15EE0A5E995EC77590,
	AndroidHelper_SetStreamVolume_m5C5241165229F2EEB91EF2A3DFDD9C2064942973,
	AndroidHelper_SetMute_mEF425DD30C7EB3FE57D96DAD15C67E232BE0A9C1,
	AndroidHelper_IsMute_mEE2021992145508FF421C4F69C69C52942A78632,
	AndroidHelper_GetAudioManager_mB3A9BB29AF7D647D85BB892E827524D4A90FB58A,
	AndroidHelper_GetActivity_mA6CCCF658D9625D73255FAA804195F6699D2CDC0,
	AndroidHelper_GetAudioManagerFlag_mD9696C8D3B64DD844069E8373F9A15B822CD9BDA,
	AndroidHelper_CheckPermissionMicrophone_mF0E5117F5296ADFD77E75CCDDC5EB9BB217DC3F9,
	AndroidHelper_CheckPermissionCamera_m4DA70A2D449D82C21766C90A0E6F7A307A439EA8,
	AndroidHelper_CheckPermissionAudioSettings_mDC41A404AA4E48C3A06FA95DD2170F1EE2DAE84A,
	AndroidHelper_CheckPermissionNetwork_mDC72B6D76B28FDDBEF016465589A87DF2AA50A20,
	AndroidHelper_HasRuntimePermissions_mF695F557262EC1332761D513823214B12B2277C7,
	AndroidHelper_RequestPermissions_m8CC42EA64C4E6647FE666DD2B742D8A51434DACB,
	AndroidHelper_OpenPermissionView_mA2E083D4A1F5F4E032E080935F8D160B62AE0BAF,
	AndroidHelper_setBluetoothScoOn_mEEB8FCEA4BE546C8849AAB8B89B44610450A809D,
	AndroidHelper_startBluetoothSco_m33DB41982CF99898DBBA1740AF162A1AA22004DF,
	AndroidHelper_stopBluetoothSco_m1BE3870445DC3E4E283743117CD1A6D02F578E7F,
	AndroidHelper_SetBluetoothOn_m23BD86C2F034FD627999052E9191D1FC7826D781,
	AndroidHelper__ctor_m35DCD9189CA6C348F6401D832ACDB0D4339DF7D5,
	AndroidHelper__cctor_mDEA91B3AF7C0DF68CE2E964981785BC2B609E893,
	UnityHelper_PtrFromColor32_mE72707EB200D366A9412330D602C18E17F1E63A6,
	ExampleGlobals_get_SignalingProtocol_m05BA332C69E3BD6DCC47392A6C47CDE8A4479409,
	ExampleGlobals_get_Signaling_m0A592F1C8A3EDE87ECB877480E83D739173B74DF,
	ExampleGlobals_get_SharedSignaling_m1E26D21095CDEBF3508E5A38966487A609BCDCC3,
	ExampleGlobals_get_SignalingConference_m1322E8CBE8EF30AFEFC1EF22CCD9B864325FC75B,
	ExampleGlobals_get_DefaultIceServer_mEE3396133F4B37738F54C122E24282B94C3FE828,
	ExampleGlobals_HasAudioPermission_mACD64F059371DC8A0B7ABED1096FA84CC141108C,
	ExampleGlobals_HasVideoPermission_mE8CD50EE7F64B7AE13CA9499811BB9CE27A68C84,
	ExampleGlobals_RequestAudioPermission_mCADB802925238D69496F8F26BF57D8B35E128353,
	ExampleGlobals_RequestVideoPermission_mCA3062586C833EBF3E30B1EC5B3DDB4F03ED4C03,
	ExampleGlobals_RequestPermissions_mCC101C3BF1590B802DD0D7BAC35A2FAA456F7889,
	ExampleGlobals__ctor_m755D4032F2D26E112979E46E24BA66883548E1CF,
	ExampleGlobals__cctor_m690520B08DF4243835A4B6BF1BC12F458246B074,
	MinimalCall_Start_mC52ACA3C3B205287C4369CFDD2AF50A6A8FB597E,
	MinimalCall_SetupReceiver_m04BB918D6E870F580F6D633A7035C115BB89EAC6,
	MinimalCall_Update_m01F0E8947BA7B1FD3659EBA6AC8A8CEF865D1B95,
	MinimalCall_Receiver_CallEvent_m967A3EAE6AA368D1211434DCE69774BF555F0587,
	MinimalCall_SenderSetup_m3F57FE8DA4DFC511FC877BE82E88E8C90FC0CCEA,
	MinimalCall_Sender_CallEvent_mCE8A2E535658E6DFBBFA2B50DFC43842F91EA8FB,
	MinimalCall_OnDestroy_m5E51DEEFEA1F626D2139FE5450A82332176052FF,
	MinimalCall__ctor_m288F3DF23BF70FE9D90B66B56954D5113B77505D,
	MinimalConference_Start_m1C8FEF3AA8CA077036F722CAA35FECA45EE877DD,
	MinimalConference_SetupCalls_mAD9D28B9E80583D924C103DCE57DE4A7F8B4E95F,
	MinimalConference_OnCallEvent_m2BC4EAD580DA8D43C88518A74EB7747D0F7A86D0,
	MinimalConference_OnDestroy_mD709383539CAEB6926361FC4A027CCD0BCA71452,
	MinimalConference_Update_m1E543A877B5F5901CB623B0B41140C5F4B14A612,
	MinimalConference__ctor_m70C9B2070A40A2182600AF14341195C1563CF212,
	MinimalMediaNetwork_Start_m67A8E4255492E101A1B9B01A0A76FE0785D1CF48,
	MinimalMediaNetwork_SetupReceiver_mF9169D17C93A77272ABED14991AE0A930DB729FB,
	MinimalMediaNetwork_UpdateReceiver_m88A7CAA251F68F45EE0151FCB65225273D6F7DC0,
	MinimalMediaNetwork_SenderSetup_m3B9EE7116873C0680F3DE19824A59CD08DE5E15F,
	MinimalMediaNetwork_UpdateSender_m31E047FF50C0DE17FB14D97EDEA292C7A83EB756,
	MinimalMediaNetwork_OnDestroy_mF845845D92F4AB959741E8354D5E2C8F26475B68,
	MinimalMediaNetwork_Update_mAFCFCEDAA268878C5107181FD968ABB1EA389E06,
	MinimalMediaNetwork__ctor_m9C0F657161CB6D124DA95DC0D0B003AC547F5360,
	SimpleCall_Start_m5F7263DF1F2D97B5F56944103DF0E5989F3795CB,
	SimpleCall_Configure_m05205E971355C90BBDC6A080B43AF667F19A1C6D,
	SimpleCall_ConfigureDelayed_m773B6E987830FC3B5CE43E3FF2B96C2C98731352,
	SimpleCall_Update_mB4B1D6991716C9E7263E0536AC4716DB1E3DBB80,
	SimpleCall_OnDestroy_m32FC313760CFAE6C26E1F3E7155F870E0CAD27D1,
	SimpleCall_Cleanup_m11E6121FE4FC3C1E85C3B79682057B3F1CB28D15,
	SimpleCall_Call_CallEvent_m7190B7ED6A86BB56F111E170A493575E140D47D7,
	SimpleCall_Call_m66A64DAB81546558B2245DE673CB36C2C1AE2FC3,
	SimpleCall_Error_mEE0E0AC62DECDC8A65C883F41DB5F17C16143667,
	SimpleCall_Log_m15F7DD2B12450F342BDDC82D660A9AFEEEA48809,
	SimpleCall__ctor_m9B81D63BE5DEE3856ED907D3632DA0C27366B3C2,
	OneToMany_Start_m5E66134B6AC6BD19EA8169CBE17B66CA58DA89E9,
	OneToMany_OnDestroy_m9C3D4D4F25E5753F1C5C8A70DA7290DD5140B2A5,
	OneToMany_Update_mEC42CBD18B8AEA2BB3CBD6762F0A3C4AAFE360C7,
	OneToMany_HandleMediaEvents_m21948326AD1CE64AAA97A827DEBE70E29F0AAD2A,
	OneToMany_Log_mECC565C25EF0ECC3864B9AB6F47E8C99C1ADF94B,
	OneToMany_HandleNetworkEvent_mCBC83E6D7FF2CD6ADAE65094ADC374DC23DB4B78,
	OneToMany_UpdateTexture_mD9570467CE0922B017796B54F640C0C5D12A0FB3,
	OneToMany_UpdateTexture_m35992CA437FDFC457E758018CC51370E15864BB5,
	OneToMany__ctor_m06808C84149389978ACF34E1B6FB34CE90BFB28A,
	OneToMany__cctor_m30316A72605004BAE3CAB4EBD8A2E661C474F9ED,
	ConferenceApp_Start_mE18918E7A96C352B334233F7589B972A927F5153,
	ConferenceApp_Setup_m9A69B2CA704D2DD01AADDF785DFB0085FD57920F,
	ConferenceApp_ResetCall_m670ECF09AD192DAFAED9337F4435AD728F998D72,
	ConferenceApp_Call_CallEvent_mFD5EE4ACE82E59052C24095DB8BC7D01E0FA2442,
	ConferenceApp_OnNewCall_mE6F85ECDA4804F10B751CD95220349AFB4E9B4AE,
	ConferenceApp_SetupVideoUi_m767C5959177609E4A292CC85829D237B1D0C4AC4,
	ConferenceApp_OnCallEnded_m6CC967DD3B728EB8A3BA077F204FDD154F40D207,
	ConferenceApp_UpdateTexture_m45D3CF38450E57E0022F9C39D3B1BA1061DFE420,
	ConferenceApp_UpdateFrame_mB55A69160707D1E5E4C4AF1B799E70FBC503BB18,
	ConferenceApp_CleanupCall_mF26C3A64CAD0AE23317BB241E09D2020754D2577,
	ConferenceApp_OnDestroy_mD87B8BBF8065AAA648573D5D7C663C434B4F55E8,
	ConferenceApp_AudioToggle_mB0DF08112D962F90747B639457769DAC83FE96F6,
	ConferenceApp_VideoToggle_m53FB09CC4C0D76FB5B2D905AA92E38A9325FB69F,
	ConferenceApp_Append_m147E3DAA00A096BC19CCB6DC47654D4F54655937,
	ConferenceApp_Update_mA6AED841801DBB80480BF4B2A3904882C6CD8AE5,
	ConferenceApp_SetGuiState_m2F0FFFE8B39F1BF9718FBEBDBCCB8393370DF297,
	ConferenceApp_JoinButtonPressed_m52041004447BDF68FB624694DDF5ADFBD70B43FD,
	ConferenceApp_EnsureLength_m55439CDE9A52BA557B64C08F219926067C1BF0BD,
	ConferenceApp_SendButtonPressed_m1C76D3E7213E564E8948275306B68A212114EB11,
	ConferenceApp_InputOnEndEdit_m0E5045AC71F40DF694D7D15525ED0709B07DF311,
	ConferenceApp_SendMsg_mCA215518E76BBD2BD50C2E2F287021439C9029CD,
	ConferenceApp_ShutdownButtonPressed_m32DA79B781B719D4379399CF609D439435DEB0F7,
	ConferenceApp__ctor_m66FC69875CA6EBE652F556F1FE9E185DF1B99A71,
	VideoInputApp_Start_m9FEC6A7E4F0029494496828E50AF0F0D59308BD9,
	VideoInputApp_CoroutineRefreshLater_m0DC0FB9BC73DD2346ADDC2FA8FD8F67658014A20,
	VideoInputApp__ctor_m84F9D04A9D54B1D3AA457751A0EF45898A9E835E,
	VirtualCamera_Awake_mDB6D605995BA4E272444329B233589FB1B200AF0,
	VirtualCamera_Start_m878B27DC5834874DC83A441F0DE0A47EA79B46B5,
	VirtualCamera_OnDestroy_mA81268441D178303DD568DE19FA35154728D3DFA,
	VirtualCamera_Update_mD56F0D77FFDA77A6B32EF7AE278BFBB94C4A731F,
	VirtualCamera__ctor_m4C826FC90090C6BC41B74A86011618536ADE48CE,
	U3CCoroutineRejoinU3Ed__37__ctor_mD1B502E34B1D61EC4A7B3FD4695B18C00500D1B2,
	U3CCoroutineRejoinU3Ed__37_System_IDisposable_Dispose_mA27BE44645CD76FA4D57A1A1464E1FFFAE347527,
	U3CCoroutineRejoinU3Ed__37_MoveNext_m71D5FB4CE137B3410136088D522FC695CF2A2D5A,
	U3CCoroutineRejoinU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C61FF934D7A8B5B6D386BCD665E10FA62459C2A,
	U3CCoroutineRejoinU3Ed__37_System_Collections_IEnumerator_Reset_m9AC7FBC17FD724D82A1F47FD32617D9D6C92AADC,
	U3CCoroutineRejoinU3Ed__37_System_Collections_IEnumerator_get_Current_m62F63F8E714FC48722E1150FA0C92BFEFFDD7359,
	U3CRequestAudioPermissionsU3Ed__74__ctor_m060EFD836B4B03C51ACF5DA3112629319C581859,
	U3CRequestAudioPermissionsU3Ed__74_System_IDisposable_Dispose_m82AA31223CD369B823A934BF55EA25777001B676,
	U3CRequestAudioPermissionsU3Ed__74_MoveNext_m59864EFAA1DB04F899F8D676D9EC435AA68A1F25,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m041FA6BF7F528AC7C49DBC8D10AC418B6FEDFA68,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_Reset_mD2B94CB57809935EF3D74E095F23C8ABAE03FEBE,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_get_Current_mFD848DA9A308905F9A88E65BDEF37828CB440E1F,
	U3CRequestVideoPermissionsU3Ed__75__ctor_mAADB433D086D489AC6552794C770B50FB533ACC8,
	U3CRequestVideoPermissionsU3Ed__75_System_IDisposable_Dispose_mD2BE8DAE78D08CBBFDB9BF022F4C03E92F4F84D2,
	U3CRequestVideoPermissionsU3Ed__75_MoveNext_mC298BA7B0CF49F838882196B545BCFE0B0BE5A11,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0551FDBD80A0E439FDE322CD1C821B0F0558E5CA,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_Reset_mCBBD4467FA5265B0CC56E4F06960C23C3EDC112F,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_get_Current_mD6001BB9D7DA7B7624138CD630E5F266E753CD4A,
	InitCallbacks__ctor_mA2B350544DA42945B9970C005AB03E4A5681830A,
	U3CCoroutineInitAsyncU3Ed__28__ctor_m8F57AF53374BEFC030571B725AE1293DCCA33BFE,
	U3CCoroutineInitAsyncU3Ed__28_System_IDisposable_Dispose_m2C384741A453655830EA579DC283BB7E1701655D,
	U3CCoroutineInitAsyncU3Ed__28_MoveNext_m678ACA5E99BC41894B7E3383BBDE152DC6CF475A,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E0F539E7CAF66A337918471031A394006F21DF,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_Reset_mE707AC23887A894941744BAAEB292C74040ADB0F,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_get_Current_m51E575B356036674708FD4BA079C37FD10480D74,
	U3CU3Ec__cctor_m2C1230BA4890438D20836803A46E28EDE7DBD225,
	U3CU3Ec__ctor_m4F98D6B5EB922D5BBB4B492490011C28638B29D8,
	U3CU3Ec_U3CInitStaticU3Eb__30_0_mB69BABE67F3A11F204B85AA3690D987470CFC3C9,
	U3CRequestAudioPermissionU3Ed__17__ctor_m8689010E40196D033215FB469771180EBE6414EE,
	U3CRequestAudioPermissionU3Ed__17_System_IDisposable_Dispose_m0F2ED97AD82D011E6BD84E1CF74A043C3AA09E9B,
	U3CRequestAudioPermissionU3Ed__17_MoveNext_mE8D7A424F8EA7129F2FDF184B1732BBBFAB6E09A,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE7A6F2DE321FD8E63CE6B875EF82ACE56EEE8F6,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_Reset_m8CCB8694BE39456024CDA20FAB3C0B83B723E2BC,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_get_Current_m4A34AB671AFC805BF200375F156C3B6193AEAB66,
	U3CRequestVideoPermissionU3Ed__18__ctor_mEA740FC978D2A36C2E3E5891FF9AAC709940A4EB,
	U3CRequestVideoPermissionU3Ed__18_System_IDisposable_Dispose_m7F906171EA17401A8ADA2BD88AC0F9C2B3954649,
	U3CRequestVideoPermissionU3Ed__18_MoveNext_m8D5C47BFFCCB208397761411004689D919255894,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C8593402350FDF1080DB009DEF4A65CFBB2BC7F,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_Reset_m1A3B9B76771748D4B26D66C3B512EA8A7B44CDE8,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_get_Current_m4ABDAD82F796C06C4A1BCBD9F566D0F5979F8CD8,
	U3CRequestPermissionsU3Ed__19__ctor_mA6A1B53AABCBB0614AC02BF583D941F068F836EC,
	U3CRequestPermissionsU3Ed__19_System_IDisposable_Dispose_m54DD727AEC651927261A4F7C37ADFD52ACF81779,
	U3CRequestPermissionsU3Ed__19_MoveNext_m5E19DCDAFA7DF393E6AA9DF150C97916C632F9AF,
	U3CRequestPermissionsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m850D371AD31B7F4368E442628840E1B01E849F23,
	U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_Reset_m01C71C986D0CAD438443FC8B7ECD74B553981497,
	U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_get_Current_m59CB0613E289C9C4D85CF6AFF2BDD62B0064C065,
	U3CConfigureDelayedU3Ed__9__ctor_m37F18A3293A76751FEE52E94A4FF2FE3E5063D00,
	U3CConfigureDelayedU3Ed__9_System_IDisposable_Dispose_mF66E1B1F0C8825871630313FBC3C301BA6DA92C6,
	U3CConfigureDelayedU3Ed__9_MoveNext_m4FCC498C6A00A081E55AB0568377617C3B425BB2,
	U3CConfigureDelayedU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AD69A1471309A6821A3F2C2E0F95EE662F96EC6,
	U3CConfigureDelayedU3Ed__9_System_Collections_IEnumerator_Reset_m6A6B3B3EE9136E2B5B48587D121502BB4E955A55,
	U3CConfigureDelayedU3Ed__9_System_Collections_IEnumerator_get_Current_m38C668EA7EDD1445F5F8B910AD5EC31F525AE0C0,
	U3CStartU3Ed__11__ctor_m9384D44968255D74485EF9DF1DBCE197FCEB0AC8,
	U3CStartU3Ed__11_System_IDisposable_Dispose_m6E992EE555DAD47ADC9C9D0F91A01BF1A717E163,
	U3CStartU3Ed__11_MoveNext_mC0383BC521EC359E4374BD4828A32E71C2991340,
	U3CStartU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC05C50BFFB86E3CA5ADA6CD3FBB940143F1975,
	U3CStartU3Ed__11_System_Collections_IEnumerator_Reset_mD3BD3F43A7CC10ED092EB99274D90D9CC7B9FD23,
	U3CStartU3Ed__11_System_Collections_IEnumerator_get_Current_mD1948AF9EDD4C398E2E4923B225AF7E8D1AE9BE7,
	VideoData__ctor_m5E2A8054A546CE82A868E96E38EB0DFDDB95C966,
	U3CCoroutineRefreshLaterU3Ed__1__ctor_m916AC23DC1B5C4BDBC05393A3EC7610798B82DA8,
	U3CCoroutineRefreshLaterU3Ed__1_System_IDisposable_Dispose_m8530188A2DD24E617730B016B26A9A1A667DE68B,
	U3CCoroutineRefreshLaterU3Ed__1_MoveNext_m06CE2E9B96FD2225138519D1848947C5ADB616D6,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D1CA13D00CC51A772B7839514307DD21324CAE,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_Reset_m93927830566AF43778CBB194F4B359E1F8758B42,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_get_Current_mAB7374B9D918A1322B2C172C2612BD34A9503BA7,
};
static const int32_t s_InvokerIndices[359] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	26,
	23,
	23,
	14,
	23,
	28,
	27,
	23,
	14,
	23,
	23,
	343,
	14,
	89,
	26,
	23,
	14,
	26,
	31,
	31,
	26,
	129,
	32,
	31,
	2577,
	89,
	31,
	31,
	89,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	632,
	625,
	14,
	131,
	23,
	23,
	23,
	130,
	130,
	23,
	32,
	26,
	31,
	23,
	23,
	31,
	23,
	23,
	14,
	23,
	23,
	26,
	23,
	343,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	2185,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	6,
	465,
	26,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	4,
	137,
	163,
	137,
	3,
	26,
	163,
	3,
	137,
	163,
	14,
	3,
	49,
	106,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	49,
	49,
	49,
	28,
	28,
	105,
	14,
	89,
	9,
	14,
	23,
	23,
	23,
	31,
	89,
	32,
	42,
	23,
	137,
	137,
	0,
	23,
	3,
	2578,
	227,
	2579,
	1060,
	135,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	114,
	114,
	869,
	49,
	106,
	173,
	49,
	3,
	106,
	3,
	106,
	173,
	869,
	49,
	4,
	4,
	94,
	49,
	49,
	49,
	49,
	49,
	2580,
	3,
	869,
	3,
	3,
	869,
	23,
	3,
	137,
	4,
	4,
	4,
	4,
	4,
	49,
	49,
	4,
	4,
	2581,
	23,
	3,
	23,
	23,
	23,
	27,
	23,
	27,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1523,
	23,
	23,
	23,
	27,
	23,
	26,
	26,
	23,
	14,
	23,
	23,
	23,
	26,
	2132,
	26,
	1507,
	23,
	3,
	23,
	42,
	23,
	27,
	26,
	2134,
	26,
	151,
	2136,
	23,
	23,
	31,
	31,
	26,
	23,
	31,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	27,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	23,
	1183,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0200000C, { 0, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 30367 },
	{ (Il2CppRGCTXDataType)1, 28675 },
	{ (Il2CppRGCTXDataType)2, 28675 },
	{ (Il2CppRGCTXDataType)3, 20527 },
};
extern const Il2CppCodeGenModule g_wrtcCodeGenModule;
const Il2CppCodeGenModule g_wrtcCodeGenModule = 
{
	"wrtc.dll",
	359,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
