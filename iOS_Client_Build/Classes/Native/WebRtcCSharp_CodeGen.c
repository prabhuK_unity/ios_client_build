﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m1D732F4D9549A9FB1DA50DA762B2654B20063A73_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m368B5D4C63E86C43CE106603050911E291D5D617_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m6DE9AADDDC1F011384305637300C164CE60BCDD6_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mFCA9579E7231D7003A858823EC5C7D30F1C264C8_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m092E8AEF96772437EFD6AED91D128AFDE4EEADF7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mB462CAC04A1999435BBDD8139EDEC497180DB9E0_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mB633286E8207B43FD91C776E8527229CFB658541_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m32C03AABDBCCC678D6623940786BF9E4E000A0E9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m51618405FC71EC50EF527181A9917F2C1605B3ED_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_mC3F7067BE05DE8889D0C3A94EA14D65C9A13F62D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_mBD9EBECDD2AAB2B6C6C860EB5BBB70493DD8ADD7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mC7A9B4275612D4E3EF7BFDCA32631F4A37FBCAD7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m4E33A6A38F46A196B28D7DD57E992F962206D23E_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mE812F52691F824C08657FA12198C907BF9B543C9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_mB3D09EFC7B3E365D2CC68129042C971EC28EACA4_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void WebRtcCSharp.AudioOptions::.ctor(System.IntPtr,System.Boolean)
extern void AudioOptions__ctor_mFAEA047777EF2908648034ECA94CC3D56D250796 ();
// 0x00000002 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AudioOptions::getCPtr(WebRtcCSharp.AudioOptions)
extern void AudioOptions_getCPtr_mAE35D3665C7CD12947096DD1885E36F90B4B1A9E ();
// 0x00000003 System.Void WebRtcCSharp.AudioOptions::Finalize()
extern void AudioOptions_Finalize_mBC7C3C648867BE5B635935BED85B9AEC8E748077 ();
// 0x00000004 System.Void WebRtcCSharp.AudioOptions::Dispose()
extern void AudioOptions_Dispose_m518328AF2A13F9E55C23AB469EC9CA5600ECBB32 ();
// 0x00000005 System.Void WebRtcCSharp.AudioOptions::set_echo_cancellation_set(System.Boolean)
extern void AudioOptions_set_echo_cancellation_set_mF065D706C78220E5B89FAF61BC6A44E002D5225A ();
// 0x00000006 System.Void WebRtcCSharp.AudioOptions::set_echo_cancellation(System.Boolean)
extern void AudioOptions_set_echo_cancellation_m2834F19E9D9CEC34E9D067ECCEA953CA45A71AC7 ();
// 0x00000007 System.Void WebRtcCSharp.AudioOptions::set_extended_filter_aec_set(System.Boolean)
extern void AudioOptions_set_extended_filter_aec_set_m1134F448B58447048342E947E2AAE0423C541C9C ();
// 0x00000008 System.Void WebRtcCSharp.AudioOptions::set_extended_filter_aec(System.Boolean)
extern void AudioOptions_set_extended_filter_aec_m6589AE053DD8156EF272D90DBBECC0C28FD09610 ();
// 0x00000009 System.Void WebRtcCSharp.AudioOptions::set_delay_agnostic_aec_set(System.Boolean)
extern void AudioOptions_set_delay_agnostic_aec_set_m708605275E5D26D1A7061DE65E269AA7D39107C7 ();
// 0x0000000A System.Void WebRtcCSharp.AudioOptions::set_delay_agnostic_aec(System.Boolean)
extern void AudioOptions_set_delay_agnostic_aec_mD91993EB68AAD1D56F7844C05949E75FE66C061E ();
// 0x0000000B System.Void WebRtcCSharp.AudioOptions::set_noise_suppression_set(System.Boolean)
extern void AudioOptions_set_noise_suppression_set_m04A7BA57442F45A1EB491429F244892C866486AB ();
// 0x0000000C System.Void WebRtcCSharp.AudioOptions::set_noise_suppression(System.Boolean)
extern void AudioOptions_set_noise_suppression_mE2F3CFE2E4D10833E65E56F2A55AEFFC54AF085A ();
// 0x0000000D System.Void WebRtcCSharp.AudioOptions::set_auto_gain_control_set(System.Boolean)
extern void AudioOptions_set_auto_gain_control_set_m71F7F93574EA6836D5E04AEC345FCAC7B8992ADA ();
// 0x0000000E System.Void WebRtcCSharp.AudioOptions::set_auto_gain_control(System.Boolean)
extern void AudioOptions_set_auto_gain_control_m6C3875331208CB0877CDDD71C13DE299C83CF62F ();
// 0x0000000F System.Void WebRtcCSharp.AudioOptions::.ctor()
extern void AudioOptions__ctor_m22D45ADFFD00181ED576043F794BB4AA2B27D11A ();
// 0x00000010 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(System.IntPtr,System.Boolean)
extern void CopyOnWriteBuffer__ctor_m4DF49CE47EC174E5EEE85E22B19BE81A9290756D ();
// 0x00000011 System.Runtime.InteropServices.HandleRef WebRtcCSharp.CopyOnWriteBuffer::getCPtr(WebRtcCSharp.CopyOnWriteBuffer)
extern void CopyOnWriteBuffer_getCPtr_m21FEFB1BAB7FD2C35C6A99F1868C007F9262D1BF ();
// 0x00000012 System.Void WebRtcCSharp.CopyOnWriteBuffer::Finalize()
extern void CopyOnWriteBuffer_Finalize_mDC9597971B4FCF6354C1D74760C5145C300CB076 ();
// 0x00000013 System.Void WebRtcCSharp.CopyOnWriteBuffer::Dispose()
extern void CopyOnWriteBuffer_Dispose_m8AAE881B17994ADAB5D144FF655DC4CA1E2AA532 ();
// 0x00000014 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor()
extern void CopyOnWriteBuffer__ctor_mDA6F002BA63A84E2C907ACD8EADAB5FD8E4D00AE ();
// 0x00000015 System.Void WebRtcCSharp.DataBuffer::.ctor(System.IntPtr,System.Boolean)
extern void DataBuffer__ctor_m05CB8EC80B3B6744524D06CB141BC62220A82A7F ();
// 0x00000016 System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataBuffer::getCPtr(WebRtcCSharp.DataBuffer)
extern void DataBuffer_getCPtr_m94D683A5F1CD85051BEC28C45F63793CED517770 ();
// 0x00000017 System.Void WebRtcCSharp.DataBuffer::Finalize()
extern void DataBuffer_Finalize_m8BBA2E5759E1B39E497DB734FF708ED507D56B94 ();
// 0x00000018 System.Void WebRtcCSharp.DataBuffer::Dispose()
extern void DataBuffer_Dispose_m14F88960EC9E309501B24949D5E27BD2187D06BA ();
// 0x00000019 System.Void WebRtcCSharp.DataBuffer::.ctor(WebRtcCSharp.CopyOnWriteBuffer,System.Boolean)
extern void DataBuffer__ctor_m2DF1B6EC80D43DC164EB0B787AA1AB4E11030428 ();
// 0x0000001A System.UInt32 WebRtcCSharp.DataBuffer::size()
extern void DataBuffer_size_m15C602D5E26A3C7AAFABD0028A3BE5C763BFFE9B ();
// 0x0000001B System.Void WebRtcCSharp.DataChannelInit::.ctor(System.IntPtr,System.Boolean)
extern void DataChannelInit__ctor_m552C91940E5D4BE7FF0BF4DE72598280110B17BF ();
// 0x0000001C System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataChannelInit::getCPtr(WebRtcCSharp.DataChannelInit)
extern void DataChannelInit_getCPtr_m2C68AFDC8E98BE584A2A9D80C2D36C23680D233F ();
// 0x0000001D System.Void WebRtcCSharp.DataChannelInit::Finalize()
extern void DataChannelInit_Finalize_m07280CEBB738B05661043393C9F0E5937DB9DB60 ();
// 0x0000001E System.Void WebRtcCSharp.DataChannelInit::Dispose()
extern void DataChannelInit_Dispose_m3EF2A21550D1DC0EF642B4437C52FA9086A7AEBD ();
// 0x0000001F System.Void WebRtcCSharp.DataChannelInit::set_ordered(System.Boolean)
extern void DataChannelInit_set_ordered_m1C03A4AE0411446608B82CCDB2C7025104489EC2 ();
// 0x00000020 System.Void WebRtcCSharp.DataChannelInit::set_maxRetransmits(System.Int32)
extern void DataChannelInit_set_maxRetransmits_m1026E22C98640BC7B09180E9955675EBD2E15438 ();
// 0x00000021 System.Void WebRtcCSharp.DataChannelInit::.ctor()
extern void DataChannelInit__ctor_m4A50538ABD89E1B3B31BE0028120900CD5134069 ();
// 0x00000022 System.Void WebRtcCSharp.IceServers::.ctor(System.IntPtr,System.Boolean)
extern void IceServers__ctor_mE4B4F73B22B7533640E5C2A6713858437AE8FE64 ();
// 0x00000023 System.Void WebRtcCSharp.IceServers::Finalize()
extern void IceServers_Finalize_mD466DAE6CCA3EF1EF3F83E37AF5083403A9BFB72 ();
// 0x00000024 System.Void WebRtcCSharp.IceServers::Dispose()
extern void IceServers_Dispose_mE0FF4FF9981F074C773C7F88A2E666CDA58D48DE ();
// 0x00000025 WebRtcCSharp.PeerConnectionInterface_IceServer WebRtcCSharp.IceServers::get_Item(System.Int32)
extern void IceServers_get_Item_m9BC1725651C0B359322E8943B803E832A1582BD9 ();
// 0x00000026 System.Int32 WebRtcCSharp.IceServers::get_Count()
extern void IceServers_get_Count_m96E606CB26E444FAE56C4DA62ADF5069600A413E ();
// 0x00000027 System.Collections.Generic.IEnumerator`1<WebRtcCSharp.PeerConnectionInterface_IceServer> WebRtcCSharp.IceServers::global::System.Collections.Generic.IEnumerable<WebRtcCSharp.PeerConnectionInterface.IceServer>.GetEnumerator()
extern void IceServers_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CWebRtcCSharp_PeerConnectionInterface_IceServerU3E_GetEnumerator_mA92A12F31F4E4CFB7F25DDA786795B965E63285D ();
// 0x00000028 System.Collections.IEnumerator WebRtcCSharp.IceServers::global::System.Collections.IEnumerable.GetEnumerator()
extern void IceServers_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mAB843F7BE364E347FA61DD50B07B063BBA4BD40D ();
// 0x00000029 System.Void WebRtcCSharp.IceServers::Add(WebRtcCSharp.PeerConnectionInterface_IceServer)
extern void IceServers_Add_m7E50E23039CAA7B58408522FAA6553E19462062F ();
// 0x0000002A System.UInt32 WebRtcCSharp.IceServers::size()
extern void IceServers_size_m860F19DD8BF1F73B95259A5CB585F44195BB3965 ();
// 0x0000002B WebRtcCSharp.PeerConnectionInterface_IceServer WebRtcCSharp.IceServers::getitem(System.Int32)
extern void IceServers_getitem_mA8B9509010E4C6CE2116FF5F25515F70CC9CACA8 ();
// 0x0000002C System.Void WebRtcCSharp.IceServers_IceServersEnumerator::.ctor(WebRtcCSharp.IceServers)
extern void IceServersEnumerator__ctor_m06FAC38E86D18623A690D35D92309E7F411E1AD3 ();
// 0x0000002D WebRtcCSharp.PeerConnectionInterface_IceServer WebRtcCSharp.IceServers_IceServersEnumerator::get_Current()
extern void IceServersEnumerator_get_Current_m0580DD75DDF1049B0756070B86AF19CAC49CE6BA ();
// 0x0000002E System.Object WebRtcCSharp.IceServers_IceServersEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void IceServersEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_m02988FFD4B817F61A95F8F7E930A2424EE498E96 ();
// 0x0000002F System.Boolean WebRtcCSharp.IceServers_IceServersEnumerator::MoveNext()
extern void IceServersEnumerator_MoveNext_m6BD69E6B55F478F8F53A62E0645BD5858031DC55 ();
// 0x00000030 System.Void WebRtcCSharp.IceServers_IceServersEnumerator::Reset()
extern void IceServersEnumerator_Reset_mF58F2488F345ACFE3266273B89E07F0C21B74850 ();
// 0x00000031 System.Void WebRtcCSharp.IceServers_IceServersEnumerator::Dispose()
extern void IceServersEnumerator_Dispose_mFDF5D773A05F4F87F059458DF93CE2D2CEF29DAC ();
// 0x00000032 System.Void WebRtcCSharp.IosHelper::InitAudioLayer()
extern void IosHelper_InitAudioLayer_mF1E553E641DEE87E61ECE2C4B1849413538DA07F ();
// 0x00000033 System.Void WebRtcCSharp.IosHelper::SetLoudspeakerStatus(System.Boolean)
extern void IosHelper_SetLoudspeakerStatus_m6F4685FFEFBED32FB9A52124323D1FADED7499BF ();
// 0x00000034 System.Boolean WebRtcCSharp.IosHelper::GetLoudspeakerStatus()
extern void IosHelper_GetLoudspeakerStatus_mAB53F842FA46CE90E0F29A1423FA5A75201642DB ();
// 0x00000035 System.Void WebRtcCSharp.IosHelper::IosKeepAudioActive(System.Boolean)
extern void IosHelper_IosKeepAudioActive_mCD7603E848CE01FDC52C71D5CA6935A3859DD6D5 ();
// 0x00000036 System.Void WebRtcCSharp.MediaConstraints::.ctor(System.IntPtr,System.Boolean)
extern void MediaConstraints__ctor_m35596F07BB20899DE4AF6B95A1CC19E2416EC617 ();
// 0x00000037 System.Runtime.InteropServices.HandleRef WebRtcCSharp.MediaConstraints::getCPtr(WebRtcCSharp.MediaConstraints)
extern void MediaConstraints_getCPtr_m363E06092B0CEBA544C22F22325030531400DA35 ();
// 0x00000038 System.Void WebRtcCSharp.MediaConstraints::Finalize()
extern void MediaConstraints_Finalize_m9F52B2E61B47535EDC48E5391E7FEDB116D39BAD ();
// 0x00000039 System.Void WebRtcCSharp.MediaConstraints::Dispose()
extern void MediaConstraints_Dispose_mA1BDDF0E72D6AE83174A40F7C7180FC5C16464DE ();
// 0x0000003A System.Void WebRtcCSharp.MediaConstraints::set_audio(System.Boolean)
extern void MediaConstraints_set_audio_m50C6049898E9C3BED1D7285A310AA141B1A6D960 ();
// 0x0000003B System.Void WebRtcCSharp.MediaConstraints::set_video(System.Boolean)
extern void MediaConstraints_set_video_m2B803CD8C91FA41C650C46F6E786B7F689F0364C ();
// 0x0000003C System.Void WebRtcCSharp.MediaConstraints::set_videoDeviceName(System.String)
extern void MediaConstraints_set_videoDeviceName_m1769EFA80565DE8CA0E43E609742CCD31D99511B ();
// 0x0000003D System.Void WebRtcCSharp.MediaConstraints::set_idealWidth(System.Int32)
extern void MediaConstraints_set_idealWidth_mB123DC822845C8211C9A5E94ABD6F8170C186922 ();
// 0x0000003E System.Void WebRtcCSharp.MediaConstraints::set_idealHeight(System.Int32)
extern void MediaConstraints_set_idealHeight_mCE66CACD040D850CB07B77C1460C306AF5227B8F ();
// 0x0000003F System.Void WebRtcCSharp.MediaConstraints::set_minWidth(System.Int32)
extern void MediaConstraints_set_minWidth_m66F60C9F6502CF635B23E71A080FE58F30F2B19F ();
// 0x00000040 System.Void WebRtcCSharp.MediaConstraints::set_minHeight(System.Int32)
extern void MediaConstraints_set_minHeight_m502E4E006E84F55A5CE5C9B4EF69B5933BB892B8 ();
// 0x00000041 System.Void WebRtcCSharp.MediaConstraints::set_maxWidth(System.Int32)
extern void MediaConstraints_set_maxWidth_m7BEE2B12EA41FBDB39BACE7A54BD524B8B4D4460 ();
// 0x00000042 System.Void WebRtcCSharp.MediaConstraints::set_maxHeight(System.Int32)
extern void MediaConstraints_set_maxHeight_mA87B1E2DC80B38C3D46A482C1C3F17F7A73A5438 ();
// 0x00000043 System.Void WebRtcCSharp.MediaConstraints::set_idealFrameRate(System.Int32)
extern void MediaConstraints_set_idealFrameRate_m8C86FF03027568AA48A8A7CBF716901743A41EF1 ();
// 0x00000044 System.Void WebRtcCSharp.MediaConstraints::set_maxFrameRate(System.Int32)
extern void MediaConstraints_set_maxFrameRate_m985813A3EDEA85BB6D2002E8D0A3695040DD4F76 ();
// 0x00000045 System.Void WebRtcCSharp.MediaConstraints::set_minFrameRate(System.Int32)
extern void MediaConstraints_set_minFrameRate_m90EA64896876B132DCBD17DF857FA95051D35A3A ();
// 0x00000046 System.Void WebRtcCSharp.MediaConstraints::.ctor()
extern void MediaConstraints__ctor_m5E842982127B45813B0C8A80C551FF7471C404A8 ();
// 0x00000047 System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::.ctor(System.IntPtr,System.Boolean)
extern void IceServer__ctor_m70EB54EDE9F0F45AD037B3D57A918529FC6D848C ();
// 0x00000048 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface_IceServer::getCPtr(WebRtcCSharp.PeerConnectionInterface_IceServer)
extern void IceServer_getCPtr_m95781F8BFC96A69210E9EBDA69F70B69C0E3B2C9 ();
// 0x00000049 System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::Finalize()
extern void IceServer_Finalize_m323796017FF96D3D0827D189D492B9CA33258548 ();
// 0x0000004A System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::Dispose()
extern void IceServer_Dispose_m6CAA9279B9FA18CED43FACFBF7EE34EC825D0C6C ();
// 0x0000004B System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::.ctor()
extern void IceServer__ctor_mBAB6B2CB1913D7F51DCBCE1ED311D33E0125C6A2 ();
// 0x0000004C WebRtcCSharp.StringVector WebRtcCSharp.PeerConnectionInterface_IceServer::get_urls()
extern void IceServer_get_urls_m7A741EF4CB24DCF57DFB93A41E3AF33D2516F072 ();
// 0x0000004D System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::set_username(System.String)
extern void IceServer_set_username_m04A297404B448FB5C87E8092FBC2DA549063452A ();
// 0x0000004E System.Void WebRtcCSharp.PeerConnectionInterface_IceServer::set_password(System.String)
extern void IceServer_set_password_mA6FEBD231EB7BD34BD65318B1652753485E40005 ();
// 0x0000004F System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::.ctor(System.IntPtr,System.Boolean)
extern void RTCConfiguration__ctor_mFF6BD417737A1BE6DEEB458DFA5D7FDBFFA7F1B0 ();
// 0x00000050 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::getCPtr(WebRtcCSharp.PeerConnectionInterface_RTCConfiguration)
extern void RTCConfiguration_getCPtr_m8A5675EE83BDF9AB14AA93440ACD2C17F443E800 ();
// 0x00000051 System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::Finalize()
extern void RTCConfiguration_Finalize_m518753A136DEDE4FB8832D4D89A0EBFF82B3B350 ();
// 0x00000052 System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::Dispose()
extern void RTCConfiguration_Dispose_m2F4C3BCF618EA14F525EC98F542CCBB130683936 ();
// 0x00000053 System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::.ctor()
extern void RTCConfiguration__ctor_mE52F30ACA223FECB0604D8C73FFAC6DE21518A9D ();
// 0x00000054 WebRtcCSharp.IceServers WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::get_servers()
extern void RTCConfiguration_get_servers_mDB3FA8927701C429BEB816585F16D8BE9F34FBE3 ();
// 0x00000055 System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::set_sdp_semantics(WebRtcCSharp.SdpSemantics)
extern void RTCConfiguration_set_sdp_semantics_mF2EBEF33D1855F9F7ECBE35901F35E6BF3E0C7D5 ();
// 0x00000056 System.Void WebRtcCSharp.PeerConnectionInterface_RTCConfiguration::.cctor()
extern void RTCConfiguration__cctor_mC066F3E9741E2A391475D0279A8CAD79AC516910 ();
// 0x00000057 System.Void WebRtcCSharp.PollingMediaStream::.ctor(System.IntPtr,System.Boolean)
extern void PollingMediaStream__ctor_mB60B33F3521A6C03CBF78160FE284007DF20115C ();
// 0x00000058 System.Void WebRtcCSharp.PollingMediaStream::Finalize()
extern void PollingMediaStream_Finalize_m43AFBD14CC4C2341D4795D73C00CEC4DB19C226D ();
// 0x00000059 System.Void WebRtcCSharp.PollingMediaStream::Dispose()
extern void PollingMediaStream_Dispose_mAAEAB096CDE5489B33042F3D1D15A2ECE3453564 ();
// 0x0000005A System.Void WebRtcCSharp.PollingMediaStreamRef::.ctor(System.IntPtr,System.Boolean)
extern void PollingMediaStreamRef__ctor_m2E30B4A68CA5FA6639D2F22BCBA3D9FECC80549F ();
// 0x0000005B System.Runtime.InteropServices.HandleRef WebRtcCSharp.PollingMediaStreamRef::getCPtr(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingMediaStreamRef_getCPtr_m159E3326B6A4693974BC5ED0CC53D932332E4D21 ();
// 0x0000005C System.Void WebRtcCSharp.PollingMediaStreamRef::Finalize()
extern void PollingMediaStreamRef_Finalize_mDEEB9D5139C93C5B5789C19462003EDB0E829AFC ();
// 0x0000005D System.Void WebRtcCSharp.PollingMediaStreamRef::Dispose()
extern void PollingMediaStreamRef_Dispose_m9DA693F21FA6FA0C837C674D423B561A95885C5E ();
// 0x0000005E WebRtcCSharp.PollingMediaStream WebRtcCSharp.PollingMediaStreamRef::get()
extern void PollingMediaStreamRef_get_m586412FBB8B70A5B01EFFFD86B23FCBB34630B53 ();
// 0x0000005F System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasVideoTrack()
extern void PollingMediaStreamRef_HasVideoTrack_m8D959D16B3BF5E814D437CA0D7FA8DADB62B3C8F ();
// 0x00000060 System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasAudioTrack()
extern void PollingMediaStreamRef_HasAudioTrack_m957BEFBB24FD29444CA934CD4D69C13C3FBEF178 ();
// 0x00000061 System.Boolean WebRtcCSharp.PollingMediaStreamRef::IsMute()
extern void PollingMediaStreamRef_IsMute_m4FA981A985E93E1996B19BD8ACCB9676BA33D7B8 ();
// 0x00000062 System.Void WebRtcCSharp.PollingMediaStreamRef::SetMute(System.Boolean)
extern void PollingMediaStreamRef_SetMute_mA140316BF7FD8F1E77ED4C74E08E691A3CD9C4F6 ();
// 0x00000063 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::CalculateByteSize(WebRtcCSharp.VideoType)
extern void PollingMediaStreamRef_CalculateByteSize_m32B944086F51223B943C5275BA1198D2604DF8EF ();
// 0x00000064 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::GetImageData(WebRtcCSharp.VideoType,System.Byte[],System.UInt32)
extern void PollingMediaStreamRef_GetImageData_m7DE6BBF9E21599A51432D12FD89ABF3DE6C17657 ();
// 0x00000065 System.Boolean WebRtcCSharp.PollingMediaStreamRef::SupportsI420Access()
extern void PollingMediaStreamRef_SupportsI420Access_mD2B605440EE99C4E8D5910D3C8BC27905C88006E ();
// 0x00000066 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::GetI420Size()
extern void PollingMediaStreamRef_GetI420Size_m4ABCC7E0792108A3C1872E72859C6E73447FA43D ();
// 0x00000067 System.IntPtr WebRtcCSharp.PollingMediaStreamRef::GetI420Ptr()
extern void PollingMediaStreamRef_GetI420Ptr_m9B73D34A3B081747A7C22AAA0758B82678F1B54B ();
// 0x00000068 System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasFrame()
extern void PollingMediaStreamRef_HasFrame_m8ACDB4BEC0BB22E647D559C102A19D157089B8AE ();
// 0x00000069 System.Void WebRtcCSharp.PollingMediaStreamRef::FreeCurrentImage()
extern void PollingMediaStreamRef_FreeCurrentImage_mBFD0B06395E578008C63EA7D0938E81BCC5658F3 ();
// 0x0000006A System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetWidth()
extern void PollingMediaStreamRef_GetWidth_m4798136B83F3EEE77739A4A1C954D14F785CC84D ();
// 0x0000006B System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetHeight()
extern void PollingMediaStreamRef_GetHeight_mF0F05A6E05BC1B460EDC8F130AEF337F959FCC36 ();
// 0x0000006C System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetRotation()
extern void PollingMediaStreamRef_GetRotation_m92673094551575B73D46D9580B30D980F6C62A0A ();
// 0x0000006D System.Void WebRtcCSharp.PollingMediaStreamRef::SetVolume(System.Double)
extern void PollingMediaStreamRef_SetVolume_m0C920071A92B14F98BC3AA3552D3FFB96352CE10 ();
// 0x0000006E System.Void WebRtcCSharp.PollingPeerRef::.ctor(System.IntPtr,System.Boolean)
extern void PollingPeerRef__ctor_mC7766F24A69E69FD2F5C5C9FCC97824511DC0FB2 ();
// 0x0000006F System.Void WebRtcCSharp.PollingPeerRef::Finalize()
extern void PollingPeerRef_Finalize_mBB6B084BC8EF2E8EDEA1322FE85DCA80BEA6D219 ();
// 0x00000070 System.Void WebRtcCSharp.PollingPeerRef::Dispose()
extern void PollingPeerRef_Dispose_m8807DD8D3F913C78484B653C830A6D6A4A2AEF42 ();
// 0x00000071 System.Void WebRtcCSharp.PollingPeerRef::Update()
extern void PollingPeerRef_Update_mE53DF570792F3E80C906B2B2AE5AFD08262E5DE3 ();
// 0x00000072 WebRtcCSharp.PollingPeer_ConnectionState WebRtcCSharp.PollingPeerRef::GetConnectionState()
extern void PollingPeerRef_GetConnectionState_mEB401B212252155F424BCA22C47D4DC90207EA65 ();
// 0x00000073 System.Boolean WebRtcCSharp.PollingPeerRef::HasSignalingMessage()
extern void PollingPeerRef_HasSignalingMessage_m0187B46B014828C1CA28E95B8661C7B093374E85 ();
// 0x00000074 System.String WebRtcCSharp.PollingPeerRef::DequeueSignalingMessage()
extern void PollingPeerRef_DequeueSignalingMessage_mEC154DE5D8ADC5EDF0D1D35C93EC554C9C2DB6EC ();
// 0x00000075 System.Void WebRtcCSharp.PollingPeerRef::AddSignalingMessage(System.String)
extern void PollingPeerRef_AddSignalingMessage_mC9F046F95A9DC7A7F0E4EB63C06DCDC39AB08D5B ();
// 0x00000076 System.Void WebRtcCSharp.PollingPeerRef::CreateOffer()
extern void PollingPeerRef_CreateOffer_m8334BB80CE03177A758E71CD02E00D64A3708BCF ();
// 0x00000077 System.Void WebRtcCSharp.PollingPeerRef::Close()
extern void PollingPeerRef_Close_m4D16C66A1BF377A9C8979BEE5CC569BF78846F9F ();
// 0x00000078 System.Void WebRtcCSharp.PollingPeerRef::Cleanup()
extern void PollingPeerRef_Cleanup_m108F2D01929951E0CE4C85ED576D283DE8998965 ();
// 0x00000079 System.Int32 WebRtcCSharp.PollingPeerRef::CountOpenedDataChannels()
extern void PollingPeerRef_CountOpenedDataChannels_m998C2D6094BA5DFAAC13F90823FD2602B3A34F65 ();
// 0x0000007A System.Boolean WebRtcCSharp.PollingPeerRef::DequeueDataChannelMessage(System.Int32&,WebRtcCSharp.DataBuffer)
extern void PollingPeerRef_DequeueDataChannelMessage_m528F953AC6AE1107FDD41FB86D3FBAEED8DB02F9 ();
// 0x0000007B System.Boolean WebRtcCSharp.PollingPeerRef::Send(System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void PollingPeerRef_Send_m2700BD6C653595AE9AE42835706FF2AB79605C90 ();
// 0x0000007C System.Void WebRtcCSharp.PollingPeerRef::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void PollingPeerRef_CreateDataChannel_m52BE3F5236CDDBA23B3C97B5EEAEFB542EE0CBC3 ();
// 0x0000007D System.String WebRtcCSharp.PollingPeerRef::GetDataChannelLabel(System.Int32)
extern void PollingPeerRef_GetDataChannelLabel_m298A4A622BBF5409FBA6CBBEE9DCD663F2B91F32 ();
// 0x0000007E WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.PollingPeerRef::GetRemoteStream()
extern void PollingPeerRef_GetRemoteStream_mBDE8B144F7B7A12C7BB9820D0CA1F0A851D0BC28 ();
// 0x0000007F System.Void WebRtcCSharp.PollingPeerRef::AddLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeerRef_AddLocalStream_m4FC018E8189963797935FCFEEE8205EB7CBFE42B ();
// 0x00000080 System.Void WebRtcCSharp.PollingPeerRef::RemoveLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeerRef_RemoveLocalStream_m15FDD700A19F8F5D06C7E30FDB3057CC4BBCFFE3 ();
// 0x00000081 WebRtcCSharp.RTCPeerConnectionFactoryRef WebRtcCSharp.RTCPeerConnectionFactory::Create()
extern void RTCPeerConnectionFactory_Create_m7713D71742494AC619F1AA0C5878C9BA749CA136 ();
// 0x00000082 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor(System.IntPtr,System.Boolean)
extern void RTCPeerConnectionFactoryRef__ctor_mE890B8A436B3A79AF60D003F05FFB729ACC797E5 ();
// 0x00000083 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::Finalize()
extern void RTCPeerConnectionFactoryRef_Finalize_mFDED3319E5BDB3753EA5DC80FF18874A3E5BDED1 ();
// 0x00000084 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::Dispose()
extern void RTCPeerConnectionFactoryRef_Dispose_m22B70E7F21A3D72CC018EFD9460D52C9DDE49E45 ();
// 0x00000085 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor()
extern void RTCPeerConnectionFactoryRef__ctor_mFE4794D4A39131CAEB3CEACD31BFAA4F6CEBC4BD ();
// 0x00000086 System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::Initialize()
extern void RTCPeerConnectionFactoryRef_Initialize_mB5233601E411451FEAA941BC9FEB9B4BBBE8FDEE ();
// 0x00000087 WebRtcCSharp.PollingPeerRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreatePollingPeer(WebRtcCSharp.PeerConnectionInterface_RTCConfiguration)
extern void RTCPeerConnectionFactoryRef_CreatePollingPeer_m266740E00EB6EAF74E989C3D8EDE733250C00A43 ();
// 0x00000088 System.String WebRtcCSharp.RTCPeerConnectionFactoryRef::GetDebugDeviceInfo_Old()
extern void RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_mAB6CE8C3FE34D82B238373EC61A78DEAD0DF002C ();
// 0x00000089 WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreatePollingMediaStream(WebRtcCSharp.MediaConstraints,WebRtcCSharp.AudioOptions)
extern void RTCPeerConnectionFactoryRef_CreatePollingMediaStream_m7317DF193656ED6E49B1E43C6BFAAFDE520F7147 ();
// 0x0000008A WebRtcCSharp.StringVector WebRtcCSharp.RTCPeerConnectionFactoryRef::GetVideoDevices()
extern void RTCPeerConnectionFactoryRef_GetVideoDevices_m4D20EE383EE861F7D9923531D4F76F8F4B4E42A4 ();
// 0x0000008B WebRtcCSharp.VideoInputRef WebRtcCSharp.RTCPeerConnectionFactoryRef::GetVideoInput()
extern void RTCPeerConnectionFactoryRef_GetVideoInput_m8B2D7A4B0AAAC7ACB3895F004E538C74D7FE4EF8 ();
// 0x0000008C System.Void WebRtcCSharp.RefCountInterface::.ctor(System.IntPtr,System.Boolean)
extern void RefCountInterface__ctor_mB69E6E40CFF44075A19D646CE3A428719F3F277B ();
// 0x0000008D System.Void WebRtcCSharp.RefCountInterface::Dispose()
extern void RefCountInterface_Dispose_mD4EC246D6DBA2801793AB67BC280341EF07286C9 ();
// 0x0000008E System.Void WebRtcCSharp.StringVector::.ctor(System.IntPtr,System.Boolean)
extern void StringVector__ctor_mCDCC866578BE55E593E4351AEBC3FD77E4C63312 ();
// 0x0000008F System.Void WebRtcCSharp.StringVector::Finalize()
extern void StringVector_Finalize_m118E0E69DFDDC55640AB9BDF13ED413705C12C48 ();
// 0x00000090 System.Void WebRtcCSharp.StringVector::Dispose()
extern void StringVector_Dispose_mAB4497D03BD64BD798B31364634B2DBB09B42E88 ();
// 0x00000091 System.Boolean WebRtcCSharp.StringVector::get_IsReadOnly()
extern void StringVector_get_IsReadOnly_mC04CE3A1DE27BD36333E4B3FDC4C114F3BAAF12E ();
// 0x00000092 System.String WebRtcCSharp.StringVector::get_Item(System.Int32)
extern void StringVector_get_Item_m8C417BBBBCDCB602C16C80233B585F264FBE62ED ();
// 0x00000093 System.Void WebRtcCSharp.StringVector::set_Item(System.Int32,System.String)
extern void StringVector_set_Item_m585723332EB2B5D2C9A2412C1DCC34062D91AB09 ();
// 0x00000094 System.Int32 WebRtcCSharp.StringVector::get_Count()
extern void StringVector_get_Count_m506E911F037B57BB9943BF2C891BB1A7289941EF ();
// 0x00000095 System.Void WebRtcCSharp.StringVector::CopyTo(System.String[],System.Int32)
extern void StringVector_CopyTo_m7A1529D3D4C74B1DB49384BF3AC0023F6EEC64BF ();
// 0x00000096 System.Void WebRtcCSharp.StringVector::CopyTo(System.Int32,System.String[],System.Int32,System.Int32)
extern void StringVector_CopyTo_m49A283845D794743856EEB68E22349250126DE0B ();
// 0x00000097 System.Collections.Generic.IEnumerator`1<System.String> WebRtcCSharp.StringVector::global::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void StringVector_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mCFE91D5FCB73661D7ABF8015D445434583B6D6F7 ();
// 0x00000098 System.Collections.IEnumerator WebRtcCSharp.StringVector::global::System.Collections.IEnumerable.GetEnumerator()
extern void StringVector_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mEF5379B5BD0CA4833E16440432B697B850B88A6F ();
// 0x00000099 System.Void WebRtcCSharp.StringVector::Clear()
extern void StringVector_Clear_m0056AF7131E195D983FCF0AE871C67B165756F7F ();
// 0x0000009A System.Void WebRtcCSharp.StringVector::Add(System.String)
extern void StringVector_Add_mDFB7606314323EEFE9B8DB3BCE23698863A58F6F ();
// 0x0000009B System.UInt32 WebRtcCSharp.StringVector::size()
extern void StringVector_size_m3049013AECD8EDA16641E188DB654EBA11989F9B ();
// 0x0000009C System.String WebRtcCSharp.StringVector::getitemcopy(System.Int32)
extern void StringVector_getitemcopy_mE359E5CDC871AC0542A08402CA916CF0FF10D476 ();
// 0x0000009D System.String WebRtcCSharp.StringVector::getitem(System.Int32)
extern void StringVector_getitem_mAB77F5E1003497A6F9709A4D0E287112552616CA ();
// 0x0000009E System.Void WebRtcCSharp.StringVector::setitem(System.Int32,System.String)
extern void StringVector_setitem_mF3567A4A4F2814C56051D076E54B4A981B7C6C81 ();
// 0x0000009F System.Void WebRtcCSharp.StringVector::Insert(System.Int32,System.String)
extern void StringVector_Insert_mA998B82EB1BB5B9BAECBBFAFB994CE855E9B251E ();
// 0x000000A0 System.Void WebRtcCSharp.StringVector::RemoveAt(System.Int32)
extern void StringVector_RemoveAt_m77F55623227192D0946302EDE32DECB03C63C467 ();
// 0x000000A1 System.Boolean WebRtcCSharp.StringVector::Contains(System.String)
extern void StringVector_Contains_m99DB800C42DC9B0B8EB664EB6CA2C617C1542372 ();
// 0x000000A2 System.Int32 WebRtcCSharp.StringVector::IndexOf(System.String)
extern void StringVector_IndexOf_mBB91681AA7B3EAAED280203AEB1EABCCE896D3BA ();
// 0x000000A3 System.Boolean WebRtcCSharp.StringVector::Remove(System.String)
extern void StringVector_Remove_mE1549F9D5B8B12A5F92DC8B7AD8363D5E44EE86F ();
// 0x000000A4 System.Void WebRtcCSharp.StringVector_StringVectorEnumerator::.ctor(WebRtcCSharp.StringVector)
extern void StringVectorEnumerator__ctor_mB1D0684EA4C36C95898B1BB4792565C9B0EB0A3C ();
// 0x000000A5 System.String WebRtcCSharp.StringVector_StringVectorEnumerator::get_Current()
extern void StringVectorEnumerator_get_Current_m224CFCCB93A03512181DBB932B797249FE341C5A ();
// 0x000000A6 System.Object WebRtcCSharp.StringVector_StringVectorEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void StringVectorEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mB3E90D83846547655AB5C4F8B7CE29845C4485EC ();
// 0x000000A7 System.Boolean WebRtcCSharp.StringVector_StringVectorEnumerator::MoveNext()
extern void StringVectorEnumerator_MoveNext_m56B59EAA1A1F5081012E799165389C35A312D6C0 ();
// 0x000000A8 System.Void WebRtcCSharp.StringVector_StringVectorEnumerator::Reset()
extern void StringVectorEnumerator_Reset_m8B6E6A72363631AD54FCD4AAD5AAB61C3324D672 ();
// 0x000000A9 System.Void WebRtcCSharp.StringVector_StringVectorEnumerator::Dispose()
extern void StringVectorEnumerator_Dispose_m1A130FBD51EA21CE08A02B22F3DF5CD610B67B35 ();
// 0x000000AA System.Void WebRtcCSharp.VideoInputRef::.ctor(System.IntPtr,System.Boolean)
extern void VideoInputRef__ctor_m20BDC4BEDCA2E6D42F02DFFC1F0A279EA972505A ();
// 0x000000AB System.Void WebRtcCSharp.VideoInputRef::Finalize()
extern void VideoInputRef_Finalize_mE15A25A3E062872141F74E258A8B5B2E3DDCABC1 ();
// 0x000000AC System.Void WebRtcCSharp.VideoInputRef::Dispose()
extern void VideoInputRef_Dispose_mE589AC6C4A54948CABAF87E45FEA5561177BA7CC ();
// 0x000000AD System.Void WebRtcCSharp.VideoInputRef::AddDevice(System.String,System.Int32,System.Int32,System.Int32)
extern void VideoInputRef_AddDevice_m036E030CE0619D4F362058D467D5367E35B8F259 ();
// 0x000000AE System.Boolean WebRtcCSharp.VideoInputRef::UpdateFrame(System.String,System.Byte[],System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void VideoInputRef_UpdateFrame_m82DC48BB2CBFC6B2644893438C0FD376F8FFB046 ();
// 0x000000AF System.Void WebRtcCSharp.VideoInputRef::RemoveDevice(System.String)
extern void VideoInputRef_RemoveDevice_m31FDBB3CCABB0AC61D298C4E50049B7CF5CF78C5 ();
// 0x000000B0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::.cctor()
extern void WebRtcSwigPINVOKE__cctor_mC87FE8A9492A9B56325FAF738C9AAB6A34BB472A ();
// 0x000000B1 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_0()
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_0_mCD20D3B24A668D85C9AF736CB33CA947F7942135 ();
// 0x000000B2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_CopyOnWriteBuffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_CopyOnWriteBuffer_m290670244CAB789F08B4E0B3434F48EC5BD639E3 ();
// 0x000000B3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_ordered_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_DataChannelInit_ordered_set_m65F44BCE5436B14ECDAC67EC6630D607B9AD0D0F ();
// 0x000000B4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_maxRetransmits_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_set_mA3B5C288EACB8496C36661DB12060E6DCD92B0CB ();
// 0x000000B5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_DataChannelInit()
extern void WebRtcSwigPINVOKE_new_DataChannelInit_m01E35C22D5648A4C92533584C25BD11C1996F724 ();
// 0x000000B6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_DataChannelInit(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_DataChannelInit_m87433CFBC17205397752C081BB26DAA3B508FBDA ();
// 0x000000B7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_DataBuffer__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_new_DataBuffer__SWIG_0_m1CD4C83825C3A91FCA6312B150A53E17D5BD20D6 ();
// 0x000000B8 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataBuffer_size_m7B4DE6F61E24A0DA06814DCB697E68DDC8F16427 ();
// 0x000000B9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_DataBuffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_DataBuffer_m1D01AB3426333384A8D5F245F82008525FC7F8F3 ();
// 0x000000BA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Clear(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_Clear_mAFBACC220C1D846132D9AB46DAA00138B2D022A1 ();
// 0x000000BB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Add(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Add_m052048414BA779B952E96DA9CA0D4A7A18F65253 ();
// 0x000000BC System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_size_m719A27E6DAA3EE975C9E98D2E59A333672ECF51F ();
// 0x000000BD System.String WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_getitemcopy_mEF6BA9C514CB023E2E3E9E1D68E8FBC7283D49EF ();
// 0x000000BE System.String WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_getitem_mFC48945E4C1568C87A2275A04715DC0FF8A853FF ();
// 0x000000BF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void WebRtcSwigPINVOKE_StringVector_setitem_m2155B99870B628C10623B96A464BC7A91D567E8D ();
// 0x000000C0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Insert_m8DBF40881B458C11C33230DCD470447F03554C42 ();
// 0x000000C1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_RemoveAt_m6E76EA8EB6B7D790618020A85017F860EAF9DEF6 ();
// 0x000000C2 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Contains(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Contains_m18F673B99725C20982460FC9038ADA1AA0E5B3DD ();
// 0x000000C3 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_IndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_IndexOf_mF2C339BCDE2DF642483D73AA83C35B09FD94FC34 ();
// 0x000000C4 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Remove_mD79B4ABC0C3442E9A07EFF6CCA40F992078F3E1E ();
// 0x000000C5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_StringVector(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_StringVector_m3BBEFE8FF916F605A8199CB337E5F0C6B36548DE ();
// 0x000000C6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_Add_m51C223B7E54AB2ADA3F2457C2B41F9A8A456D789 ();
// 0x000000C7 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_size_m80580DE016DED7010F49BD7BED75703B4F8AC8B1 ();
// 0x000000C8 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_getitem_mFFD1E6A7F23195AABD4EEC6551C79574BC7E3D45 ();
// 0x000000C9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_IceServers(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_IceServers_m7D816381DE0B8990D5DA7BCDE14AF5201DFDF5C1 ();
// 0x000000CA System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_IceServer__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_0_m3BF3E47C322DFE2138CA391B46F90458C70EB554 ();
// 0x000000CB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_IceServer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_IceServer_mCEA6989BCA4A3DE9A36F26B0B6ED075369C73750 ();
// 0x000000CC System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_urls_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_get_m18ABC3FEE0B680AAC5F592F244A10D01F9497AD9 ();
// 0x000000CD System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_username_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_set_m9CF2C94EBF3DFB739D3BC14EC80FE4A9896AA545 ();
// 0x000000CE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_password_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_set_mDCE11E71C39A68E08F5C1AE0FF81D78FFFB02C17 ();
// 0x000000CF System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCConfiguration__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_0_m838ED7F5C0838C1598261980CA096DA76CA02BE1 ();
// 0x000000D0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_RTCConfiguration(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCConfiguration_m8D956437DE79EAB6FDA4A30617DB734AEE2CEE0E ();
// 0x000000D1 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kUndefined_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kUndefined_get_m03B83FFF228DB385CBD6BA92A4D174F433E953CC ();
// 0x000000D2 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get_mC83E2885954E25509300251DA1C531668E178E70 ();
// 0x000000D3 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get_mDE2FAE17C517C3A28B5E355C70384A376648D793 ();
// 0x000000D4 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_servers_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_get_mD43030AFE129AA9394A552860FEB2F9D523CB82D ();
// 0x000000D5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_sdp_semantics_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_set_mB88857EEBAF0E48068C9FA67EBD25D054E6D618B ();
// 0x000000D6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_SetDebugLogLevel(System.Int32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_SetDebugLogLevel_m5EB7F6561F7F1A6315CB5AA03E18E6A43307A33A ();
// 0x000000D7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_Copy__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UInt32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_0_m3B8BBC320079F84F7DCDCF7601A3DACAB468B766 ();
// 0x000000D8 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_GetWebRtcVersion()
extern void WebRtcSwigPINVOKE_WebRtcWrap_GetWebRtcVersion_m2C4930A68C2D63648C5D0D2D82EC69CD3C3AA5FA ();
// 0x000000D9 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_GetVersion()
extern void WebRtcSwigPINVOKE_WebRtcWrap_GetVersion_m29426BCAF2B878F29C308C3349B68FBB7E96D542 ();
// 0x000000DA System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_IsDebugBuild()
extern void WebRtcSwigPINVOKE_WebRtcWrap_IsDebugBuild_m33E6C695075C2E4D16F8F566B7E2CEB415210426 ();
// 0x000000DB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_set_mA13F94546D27EAEA49C43A88EC237CF515274339 ();
// 0x000000DC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_m338D9F6EDE61BBE6B2EC7CB1B22CCF2D8DEF070A ();
// 0x000000DD System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_set_mE662CB1CB7D9B59C4238BF097CB23809A90AB85B ();
// 0x000000DE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_m8BF7B024013D234299AD613790A78EE7A4C33BEB ();
// 0x000000DF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_set_m1A21288FB643A031B8C5E59FB4AA6356C6A7B9ED ();
// 0x000000E0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_m0839A3D32A4B6798FD1D322B27158A658FD4DB60 ();
// 0x000000E1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_set_mCCF1957E73747493F5BE4C1AE867F8F62B1388E7 ();
// 0x000000E2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_mE15BB08E9CEF19FBEB177009C5506A6980D28006 ();
// 0x000000E3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_set_mBC520CFDB72839E11FFEF07434EDC268DE2CBF1E ();
// 0x000000E4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_mBD7F3DC478D5468692866DC842A2007AEE1A80D5 ();
// 0x000000E5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AudioOptions()
extern void WebRtcSwigPINVOKE_new_AudioOptions_m6A5D545EB1C743B39989F267E34DD38E5C801C6C ();
// 0x000000E6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AudioOptions(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AudioOptions_m1930E395FF6A9F9CD064871BB9CE0037C38807A5 ();
// 0x000000E7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_audio_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_MediaConstraints_audio_set_m1CDC042ABE39986546C3F813D05BE31BA878750A ();
// 0x000000E8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_video_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_MediaConstraints_video_set_m229494B0CFB29294E5CDE4F9AD2231127D685813 ();
// 0x000000E9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_videoDeviceName_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_set_m1D810FA617F036F3BBE16D3F297450C7A0640963 ();
// 0x000000EA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealWidth_set_mDBF2C65866B1D7F382F132980D41B73A8D69E2BF ();
// 0x000000EB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealHeight_set_mE642441FF25150D85ACACB5A9DD8B884F2C19C58 ();
// 0x000000EC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minWidth_set_m06EAD6E1069042CA81334D6465858B35746065FD ();
// 0x000000ED System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minHeight_set_m6FEC7466D7F3445B1AEDE10B70B58F5759622501 ();
// 0x000000EE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxWidth_set_m94DFAF81BA810C970D0CFD72E25FCA09613734F6 ();
// 0x000000EF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxHeight_set_m60B4408F3BD1E91FF3CAEF67D3D8FB6DA3DCCA55 ();
// 0x000000F0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_set_m1F293365B4905F2C63D6E2A3F9D7C51863326122 ();
// 0x000000F1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_set_mFF4CE0137EA3A37B2F23FB2DA89068028E8204C1 ();
// 0x000000F2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_set_m538AE730FC5E54742066D0DE91E64BFF7D4664C1 ();
// 0x000000F3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_MediaConstraints()
extern void WebRtcSwigPINVOKE_new_MediaConstraints_m6B5C3368975384B4ADB3358A6FE2D3BBA6E10FAE ();
// 0x000000F4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_MediaConstraints(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_MediaConstraints_mB2018234D45FCFB4B9667777E546648138249F31 ();
// 0x000000F5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingMediaStreamRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingMediaStreamRef_m538945522373A063101F811D499C2AB0616064DD ();
// 0x000000F6 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_get_mA74FD95F2DE3E21BD5C865C66CF9F45EE0B7018D ();
// 0x000000F7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasVideoTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasVideoTrack_mCF372D139AB108F2E957925F95209153ACA0962C ();
// 0x000000F8 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasAudioTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasAudioTrack_mBEFD2BF1A06167364A65A355B218B3C95783B4D7 ();
// 0x000000F9 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_IsMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_IsMute_m1EECFF31E29A43264C673EC56A17A8D2B6C2D221 ();
// 0x000000FA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SetMute(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SetMute_mB3FA569F8D5BA6C5E642F35B57EAB930C8702965 ();
// 0x000000FB System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_CalculateByteSize(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_CalculateByteSize_m1A9ADFA8D59A47C7C6869B1BF0F795CCB2F3776C ();
// 0x000000FC System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetImageData(System.Runtime.InteropServices.HandleRef,System.Int32,System.IntPtr,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetImageData_m1DB7DED25566361FED6CC39758ED92F2608BA12E ();
// 0x000000FD System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SupportsI420Access(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SupportsI420Access_mDEFCAB0B3BDFA4ACF2811FC5AA82E4522F9DB201 ();
// 0x000000FE System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetI420Size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Size_m509585A2891F25E52669BBE03E34CCB0D6AD1C50 ();
// 0x000000FF System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetI420Ptr(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Ptr_mD2E1724D4445C538723AF6FC9E8DC81CD2950088 ();
// 0x00000100 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasFrame(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasFrame_mDFE7EB63B107B2F428CFD28AA219752F24B132C2 ();
// 0x00000101 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_FreeCurrentImage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_FreeCurrentImage_m525CBF0504D29FD73480075D85F521C8D02A060C ();
// 0x00000102 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetWidth(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetWidth_m4B66772EFDAE289E299A60AA67D52ADCF3E779CF ();
// 0x00000103 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetHeight(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetHeight_m6387210F6F1E68E43E8F2060563233099CA27F92 ();
// 0x00000104 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetRotation(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetRotation_m49F466DF2709DAF94A3BC6AA26CDF1739EA27C5A ();
// 0x00000105 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SetVolume(System.Runtime.InteropServices.HandleRef,System.Double)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SetVolume_m56A99BB0DCCCDCB00021649D49A58BB4A863D155 ();
// 0x00000106 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingMediaStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingMediaStream_mDD21B245C783794D892ABBC115BA89B9C0B081A1 ();
// 0x00000107 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingPeerRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingPeerRef_m1CE16DC02F2DB2E468D8D33CFC01E0690693BEFA ();
// 0x00000108 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Update(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Update_mB0F9F8DBED5247884C349AD55B8BC1726DD6C9C3 ();
// 0x00000109 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetConnectionState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetConnectionState_mC85BD7026CC5313029B09499F8F1A63C8E0048A4 ();
// 0x0000010A System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_HasSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingMessage_m1B74F4C85D8041F1DA8F80EEA9F51FAB9C5FD467 ();
// 0x0000010B System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingMessage_mC2FF9DD72893A67BF3FE29E8A7ED0C35E17B4232 ();
// 0x0000010C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_AddSignalingMessage(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PollingPeerRef_AddSignalingMessage_mE9EB5A2F086AE4BC5DB8655033FDE40E8BF3B0C6 ();
// 0x0000010D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CreateOffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CreateOffer_m3FEED7FAEDD9424A65F6E6144D0F5B3E1729013F ();
// 0x0000010E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Close_m1DEBE6693E8E0DCB92B19D7664602E949D747543 ();
// 0x0000010F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Cleanup(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Cleanup_mD8CFA99AFE3756134F75C5BB4A17BFF2AF92CAE9 ();
// 0x00000110 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CountOpenedDataChannels(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CountOpenedDataChannels_m073D7A584F51DBA28C45594CD52668B986A30A7D ();
// 0x00000111 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueDataChannelMessage(System.Runtime.InteropServices.HandleRef,System.Int32&,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueDataChannelMessage_mD37E9C5036397C4546E6DA2CF88177290A77AA7B ();
// 0x00000112 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Send(System.Runtime.InteropServices.HandleRef,System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Send_m4B83E1F56272FD7B5776AF9F17B93C913A5B99A6 ();
// 0x00000113 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CreateDataChannel_m9A90A69FDF284BE7F6101DFCD8A572A80AF537E7 ();
// 0x00000114 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetDataChannelLabel(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetDataChannelLabel_m92E2EC81C43DF4DF7BE906AF61D155EB76261C11 ();
// 0x00000115 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetRemoteStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetRemoteStream_m31F804F52EFE5FD4B8CC7DDB7B0928A9AD8E36F8 ();
// 0x00000116 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_AddLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_AddLocalStream_m06384E01816BF6BFFBB2D098F3807ABD9C940FBA ();
// 0x00000117 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_RemoveLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_RemoveLocalStream_m1C8E2A0280A727C5ADECB90CAA4EDFA36BC4D7BB ();
// 0x00000118 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_VideoInputRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_VideoInputRef_m2BFD3237E30983386AB19F4C1420C711D64FA376 ();
// 0x00000119 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_AddDevice(System.Runtime.InteropServices.HandleRef,System.String,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_VideoInputRef_AddDevice_mAB87353DA099E2C5CF3FD34B7690746BEA9D3E64 ();
// 0x0000011A System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_UpdateFrame(System.Runtime.InteropServices.HandleRef,System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame_mFC54E0AF6DE9DAB46D18F61B2B5802F0A5643D6D ();
// 0x0000011B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_RemoveDevice(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_VideoInputRef_RemoveDevice_m2BB2C3F282E0428C104656D237C505DFED6BF85B ();
// 0x0000011C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_RTCPeerConnectionFactoryRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_0_m4603500723C65BD80D3BE19D061827F0BA5D7453 ();
// 0x0000011D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_RTCPeerConnectionFactoryRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactoryRef_m27530984B6A1FB7F1E5BB943A90848F92C3628F8 ();
// 0x0000011E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_Initialize(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Initialize_m187F4C5B88196A8027C60A59DA5A74CFC540B14B ();
// 0x0000011F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreatePollingPeer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingPeer_mDDBA5B7360FAAD6C7D17176EFE10814006AC390F ();
// 0x00000120 System.String WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_mFB8E3AB201B8C530201453C15D55A4FA9E55F447 ();
// 0x00000121 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1_m766C161C75204469BF3F5C251639139D9F536C06 ();
// 0x00000122 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetVideoDevices(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoDevices_m10000397F20F632A96868B7F4AC16A6579ECCE36 ();
// 0x00000123 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetVideoInput(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoInput_m5DBE190A9AECA6D46B1EF25B5453A0212BAE63D4 ();
// 0x00000124 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_Create()
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Create_m41BBF3FF5D6F145FC0831A1625EC498F7D0F9403 ();
// 0x00000125 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_InitAudioLayer()
extern void WebRtcSwigPINVOKE_IosHelper_InitAudioLayer_mE3DD0EC7C074710BAB91E8FA24A89A4962BB2AA9 ();
// 0x00000126 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_SetLoudspeakerStatus(System.Boolean)
extern void WebRtcSwigPINVOKE_IosHelper_SetLoudspeakerStatus_mA46D0DFB32996116C47237A2DAE1CBF71D9544FF ();
// 0x00000127 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_GetLoudspeakerStatus()
extern void WebRtcSwigPINVOKE_IosHelper_GetLoudspeakerStatus_mD1EF9BF2B7264E7BF872ADF8486CD721B4402B5F ();
// 0x00000128 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_IosKeepAudioActive(System.Boolean)
extern void WebRtcSwigPINVOKE_IosHelper_IosKeepAudioActive_m49C2018DC7ACAD5BAC75B837032C89290B87C701 ();
// 0x00000129 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_PollingMediaStream_SWIGUpcast_m5F5317F463AFABD3D3160CB11D5323095616FBBC ();
// 0x0000012A System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_WebRtcSwig_m5B304F5D4CA72F6272736536C39D3BA217AC6CB4 ();
// 0x0000012B System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_WebRtcSwig_mCB96017A989D0299B62DBC2DF6C037692C695E21 ();
// 0x0000012C System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m1D732F4D9549A9FB1DA50DA762B2654B20063A73 ();
// 0x0000012D System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m092E8AEF96772437EFD6AED91D128AFDE4EEADF7 ();
// 0x0000012E System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mB462CAC04A1999435BBDD8139EDEC497180DB9E0 ();
// 0x0000012F System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m32C03AABDBCCC678D6623940786BF9E4E000A0E9 ();
// 0x00000130 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m51618405FC71EC50EF527181A9917F2C1605B3ED ();
// 0x00000131 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_mC3F7067BE05DE8889D0C3A94EA14D65C9A13F62D ();
// 0x00000132 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_mB633286E8207B43FD91C776E8527229CFB658541 ();
// 0x00000133 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_mBD9EBECDD2AAB2B6C6C860EB5BBB70493DD8ADD7 ();
// 0x00000134 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mC7A9B4275612D4E3EF7BFDCA32631F4A37FBCAD7 ();
// 0x00000135 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m4E33A6A38F46A196B28D7DD57E992F962206D23E ();
// 0x00000136 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_mE812F52691F824C08657FA12198C907BF9B543C9 ();
// 0x00000137 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m368B5D4C63E86C43CE106603050911E291D5D617 ();
// 0x00000138 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_m6DE9AADDDC1F011384305637300C164CE60BCDD6 ();
// 0x00000139 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mFCA9579E7231D7003A858823EC5C7D30F1C264C8 ();
// 0x0000013A System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m5317B1531AD8E6BAFA96FFFC4B0E67CACEDFE25A ();
// 0x0000013B System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_mF83A5CBE8504298E4494E84B061180583D0A7DBA ();
// 0x0000013C System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m51C6FB7039A69CB3F37EA7DBE6AEFF92027BB4E4 ();
// 0x0000013D System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_m26ED00A2E03AA5C41798C060DDBF6B1FD882F296 ();
// 0x0000013E System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_m15C7E8DD6F20E3BD93BC61AF7B295F8B05E6713B ();
// 0x0000013F System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_m57FFB28E01195CD1B138C0FA767BC11044408368 ();
// 0x00000140 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m8BF7FAB7DE7EC5C97C93A8CB537E5E74F5878971 ();
// 0x00000141 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m79EA78B66628CF88ACB2A1A9B0F2F36DB78A3C47 ();
// 0x00000142 System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_m940BAF7AEF0369D0CBFD68A1A5A32A319B5D3AB8 ();
// 0x00000143 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_m2A7B93A231805EA10102F388F93C22FE7634B7D0 ();
// 0x00000144 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE_SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_mB53F25610102D16A4FA5BE7C2A7EC566FC80ED3D ();
// 0x00000145 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m838596ED86F8019AA8128876A6CE6507A7676796 ();
// 0x00000146 System.Exception WebRtcCSharp.WebRtcSwigPINVOKE_SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m782926834E50157DB79DB9887CAE80416E3FA039 ();
// 0x00000147 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper::SWIGRegisterStringCallback_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper_SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_WebRtcSwig_m3892765253EB3BB5E9D7F05BE4412D5BCB0AA9EB ();
// 0x00000148 System.String WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_mB3D09EFC7B3E365D2CC68129042C971EC28EACA4 ();
// 0x00000149 System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_m3B9F8EABD842426183E3A3E2F23FD90F448F6A05 ();
// 0x0000014A System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_m20ADD060890875804F29CF4E2D3E60366CFE54ED ();
// 0x0000014B System.Void WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper_SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_mBB891E4042D6F16ABF153AAD0EB62BB0B529C571 ();
// 0x0000014C System.String WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper_SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mF8D32F1CA1DC2E398A4151B0758E447DBE1FACB6 ();
// 0x0000014D System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper_SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_m35098ACD189C98545289BD8B2D0C81F82E77039C ();
// 0x0000014E System.String WebRtcCSharp.WebRtcSwigPINVOKE_SWIGStringHelper_SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m35BA3A8E09FDA2CEE5C79CE913727B159086DAAD ();
// 0x0000014F System.Void WebRtcCSharp.WebRtcWrap::SetDebugLogLevel(WebRtcCSharp.LoggingSeverity)
extern void WebRtcWrap_SetDebugLogLevel_m837C2654C29A74A57BCA073E1C99B8DA60EE603E ();
// 0x00000150 System.Void WebRtcCSharp.WebRtcWrap::Copy(WebRtcCSharp.DataBuffer,System.Byte[],System.UInt32)
extern void WebRtcWrap_Copy_m8901788687FAA782066D162A460DDD957E652556 ();
// 0x00000151 System.String WebRtcCSharp.WebRtcWrap::GetWebRtcVersion()
extern void WebRtcWrap_GetWebRtcVersion_m2840B51D9D15040E31803A2EA18DC4A62B95ED3A ();
// 0x00000152 System.String WebRtcCSharp.WebRtcWrap::GetVersion()
extern void WebRtcWrap_GetVersion_m83EB4EABA56C7EC7E44337016C3B3B23A8D3FA88 ();
// 0x00000153 System.Boolean WebRtcCSharp.WebRtcWrap::IsDebugBuild()
extern void WebRtcWrap_IsDebugBuild_mA8A9E502B68A2DD81F814776DFB0EC6B3FEF803F ();
// 0x00000154 System.Void WebRtcCSharp.LogCallback::.ctor(System.Object,System.IntPtr)
extern void LogCallback__ctor_m9D1F7E15A8B3A2F6B8F2C60A36B1EC09512AEC0E ();
// 0x00000155 System.Void WebRtcCSharp.LogCallback::Invoke(System.String)
extern void LogCallback_Invoke_m13A80BD61CF98EF15519BF019D1D0682DF838933 ();
// 0x00000156 System.IAsyncResult WebRtcCSharp.LogCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void LogCallback_BeginInvoke_mACA577B965295450DFBC43607F508BB83C73899D ();
// 0x00000157 System.Void WebRtcCSharp.LogCallback::EndInvoke(System.IAsyncResult)
extern void LogCallback_EndInvoke_m166F83B8A4244F3129B345665F7E730CA3EDD4E0 ();
// 0x00000158 System.Void WebRtcCSharp.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mF28B4940B8D9384B772AFB289CA8DF2734798FFE ();
static Il2CppMethodPointer s_methodPointers[344] = 
{
	AudioOptions__ctor_mFAEA047777EF2908648034ECA94CC3D56D250796,
	AudioOptions_getCPtr_mAE35D3665C7CD12947096DD1885E36F90B4B1A9E,
	AudioOptions_Finalize_mBC7C3C648867BE5B635935BED85B9AEC8E748077,
	AudioOptions_Dispose_m518328AF2A13F9E55C23AB469EC9CA5600ECBB32,
	AudioOptions_set_echo_cancellation_set_mF065D706C78220E5B89FAF61BC6A44E002D5225A,
	AudioOptions_set_echo_cancellation_m2834F19E9D9CEC34E9D067ECCEA953CA45A71AC7,
	AudioOptions_set_extended_filter_aec_set_m1134F448B58447048342E947E2AAE0423C541C9C,
	AudioOptions_set_extended_filter_aec_m6589AE053DD8156EF272D90DBBECC0C28FD09610,
	AudioOptions_set_delay_agnostic_aec_set_m708605275E5D26D1A7061DE65E269AA7D39107C7,
	AudioOptions_set_delay_agnostic_aec_mD91993EB68AAD1D56F7844C05949E75FE66C061E,
	AudioOptions_set_noise_suppression_set_m04A7BA57442F45A1EB491429F244892C866486AB,
	AudioOptions_set_noise_suppression_mE2F3CFE2E4D10833E65E56F2A55AEFFC54AF085A,
	AudioOptions_set_auto_gain_control_set_m71F7F93574EA6836D5E04AEC345FCAC7B8992ADA,
	AudioOptions_set_auto_gain_control_m6C3875331208CB0877CDDD71C13DE299C83CF62F,
	AudioOptions__ctor_m22D45ADFFD00181ED576043F794BB4AA2B27D11A,
	CopyOnWriteBuffer__ctor_m4DF49CE47EC174E5EEE85E22B19BE81A9290756D,
	CopyOnWriteBuffer_getCPtr_m21FEFB1BAB7FD2C35C6A99F1868C007F9262D1BF,
	CopyOnWriteBuffer_Finalize_mDC9597971B4FCF6354C1D74760C5145C300CB076,
	CopyOnWriteBuffer_Dispose_m8AAE881B17994ADAB5D144FF655DC4CA1E2AA532,
	CopyOnWriteBuffer__ctor_mDA6F002BA63A84E2C907ACD8EADAB5FD8E4D00AE,
	DataBuffer__ctor_m05CB8EC80B3B6744524D06CB141BC62220A82A7F,
	DataBuffer_getCPtr_m94D683A5F1CD85051BEC28C45F63793CED517770,
	DataBuffer_Finalize_m8BBA2E5759E1B39E497DB734FF708ED507D56B94,
	DataBuffer_Dispose_m14F88960EC9E309501B24949D5E27BD2187D06BA,
	DataBuffer__ctor_m2DF1B6EC80D43DC164EB0B787AA1AB4E11030428,
	DataBuffer_size_m15C602D5E26A3C7AAFABD0028A3BE5C763BFFE9B,
	DataChannelInit__ctor_m552C91940E5D4BE7FF0BF4DE72598280110B17BF,
	DataChannelInit_getCPtr_m2C68AFDC8E98BE584A2A9D80C2D36C23680D233F,
	DataChannelInit_Finalize_m07280CEBB738B05661043393C9F0E5937DB9DB60,
	DataChannelInit_Dispose_m3EF2A21550D1DC0EF642B4437C52FA9086A7AEBD,
	DataChannelInit_set_ordered_m1C03A4AE0411446608B82CCDB2C7025104489EC2,
	DataChannelInit_set_maxRetransmits_m1026E22C98640BC7B09180E9955675EBD2E15438,
	DataChannelInit__ctor_m4A50538ABD89E1B3B31BE0028120900CD5134069,
	IceServers__ctor_mE4B4F73B22B7533640E5C2A6713858437AE8FE64,
	IceServers_Finalize_mD466DAE6CCA3EF1EF3F83E37AF5083403A9BFB72,
	IceServers_Dispose_mE0FF4FF9981F074C773C7F88A2E666CDA58D48DE,
	IceServers_get_Item_m9BC1725651C0B359322E8943B803E832A1582BD9,
	IceServers_get_Count_m96E606CB26E444FAE56C4DA62ADF5069600A413E,
	IceServers_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CWebRtcCSharp_PeerConnectionInterface_IceServerU3E_GetEnumerator_mA92A12F31F4E4CFB7F25DDA786795B965E63285D,
	IceServers_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mAB843F7BE364E347FA61DD50B07B063BBA4BD40D,
	IceServers_Add_m7E50E23039CAA7B58408522FAA6553E19462062F,
	IceServers_size_m860F19DD8BF1F73B95259A5CB585F44195BB3965,
	IceServers_getitem_mA8B9509010E4C6CE2116FF5F25515F70CC9CACA8,
	IceServersEnumerator__ctor_m06FAC38E86D18623A690D35D92309E7F411E1AD3,
	IceServersEnumerator_get_Current_m0580DD75DDF1049B0756070B86AF19CAC49CE6BA,
	IceServersEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_m02988FFD4B817F61A95F8F7E930A2424EE498E96,
	IceServersEnumerator_MoveNext_m6BD69E6B55F478F8F53A62E0645BD5858031DC55,
	IceServersEnumerator_Reset_mF58F2488F345ACFE3266273B89E07F0C21B74850,
	IceServersEnumerator_Dispose_mFDF5D773A05F4F87F059458DF93CE2D2CEF29DAC,
	IosHelper_InitAudioLayer_mF1E553E641DEE87E61ECE2C4B1849413538DA07F,
	IosHelper_SetLoudspeakerStatus_m6F4685FFEFBED32FB9A52124323D1FADED7499BF,
	IosHelper_GetLoudspeakerStatus_mAB53F842FA46CE90E0F29A1423FA5A75201642DB,
	IosHelper_IosKeepAudioActive_mCD7603E848CE01FDC52C71D5CA6935A3859DD6D5,
	MediaConstraints__ctor_m35596F07BB20899DE4AF6B95A1CC19E2416EC617,
	MediaConstraints_getCPtr_m363E06092B0CEBA544C22F22325030531400DA35,
	MediaConstraints_Finalize_m9F52B2E61B47535EDC48E5391E7FEDB116D39BAD,
	MediaConstraints_Dispose_mA1BDDF0E72D6AE83174A40F7C7180FC5C16464DE,
	MediaConstraints_set_audio_m50C6049898E9C3BED1D7285A310AA141B1A6D960,
	MediaConstraints_set_video_m2B803CD8C91FA41C650C46F6E786B7F689F0364C,
	MediaConstraints_set_videoDeviceName_m1769EFA80565DE8CA0E43E609742CCD31D99511B,
	MediaConstraints_set_idealWidth_mB123DC822845C8211C9A5E94ABD6F8170C186922,
	MediaConstraints_set_idealHeight_mCE66CACD040D850CB07B77C1460C306AF5227B8F,
	MediaConstraints_set_minWidth_m66F60C9F6502CF635B23E71A080FE58F30F2B19F,
	MediaConstraints_set_minHeight_m502E4E006E84F55A5CE5C9B4EF69B5933BB892B8,
	MediaConstraints_set_maxWidth_m7BEE2B12EA41FBDB39BACE7A54BD524B8B4D4460,
	MediaConstraints_set_maxHeight_mA87B1E2DC80B38C3D46A482C1C3F17F7A73A5438,
	MediaConstraints_set_idealFrameRate_m8C86FF03027568AA48A8A7CBF716901743A41EF1,
	MediaConstraints_set_maxFrameRate_m985813A3EDEA85BB6D2002E8D0A3695040DD4F76,
	MediaConstraints_set_minFrameRate_m90EA64896876B132DCBD17DF857FA95051D35A3A,
	MediaConstraints__ctor_m5E842982127B45813B0C8A80C551FF7471C404A8,
	IceServer__ctor_m70EB54EDE9F0F45AD037B3D57A918529FC6D848C,
	IceServer_getCPtr_m95781F8BFC96A69210E9EBDA69F70B69C0E3B2C9,
	IceServer_Finalize_m323796017FF96D3D0827D189D492B9CA33258548,
	IceServer_Dispose_m6CAA9279B9FA18CED43FACFBF7EE34EC825D0C6C,
	IceServer__ctor_mBAB6B2CB1913D7F51DCBCE1ED311D33E0125C6A2,
	IceServer_get_urls_m7A741EF4CB24DCF57DFB93A41E3AF33D2516F072,
	IceServer_set_username_m04A297404B448FB5C87E8092FBC2DA549063452A,
	IceServer_set_password_mA6FEBD231EB7BD34BD65318B1652753485E40005,
	RTCConfiguration__ctor_mFF6BD417737A1BE6DEEB458DFA5D7FDBFFA7F1B0,
	RTCConfiguration_getCPtr_m8A5675EE83BDF9AB14AA93440ACD2C17F443E800,
	RTCConfiguration_Finalize_m518753A136DEDE4FB8832D4D89A0EBFF82B3B350,
	RTCConfiguration_Dispose_m2F4C3BCF618EA14F525EC98F542CCBB130683936,
	RTCConfiguration__ctor_mE52F30ACA223FECB0604D8C73FFAC6DE21518A9D,
	RTCConfiguration_get_servers_mDB3FA8927701C429BEB816585F16D8BE9F34FBE3,
	RTCConfiguration_set_sdp_semantics_mF2EBEF33D1855F9F7ECBE35901F35E6BF3E0C7D5,
	RTCConfiguration__cctor_mC066F3E9741E2A391475D0279A8CAD79AC516910,
	PollingMediaStream__ctor_mB60B33F3521A6C03CBF78160FE284007DF20115C,
	PollingMediaStream_Finalize_m43AFBD14CC4C2341D4795D73C00CEC4DB19C226D,
	PollingMediaStream_Dispose_mAAEAB096CDE5489B33042F3D1D15A2ECE3453564,
	PollingMediaStreamRef__ctor_m2E30B4A68CA5FA6639D2F22BCBA3D9FECC80549F,
	PollingMediaStreamRef_getCPtr_m159E3326B6A4693974BC5ED0CC53D932332E4D21,
	PollingMediaStreamRef_Finalize_mDEEB9D5139C93C5B5789C19462003EDB0E829AFC,
	PollingMediaStreamRef_Dispose_m9DA693F21FA6FA0C837C674D423B561A95885C5E,
	PollingMediaStreamRef_get_m586412FBB8B70A5B01EFFFD86B23FCBB34630B53,
	PollingMediaStreamRef_HasVideoTrack_m8D959D16B3BF5E814D437CA0D7FA8DADB62B3C8F,
	PollingMediaStreamRef_HasAudioTrack_m957BEFBB24FD29444CA934CD4D69C13C3FBEF178,
	PollingMediaStreamRef_IsMute_m4FA981A985E93E1996B19BD8ACCB9676BA33D7B8,
	PollingMediaStreamRef_SetMute_mA140316BF7FD8F1E77ED4C74E08E691A3CD9C4F6,
	PollingMediaStreamRef_CalculateByteSize_m32B944086F51223B943C5275BA1198D2604DF8EF,
	PollingMediaStreamRef_GetImageData_m7DE6BBF9E21599A51432D12FD89ABF3DE6C17657,
	PollingMediaStreamRef_SupportsI420Access_mD2B605440EE99C4E8D5910D3C8BC27905C88006E,
	PollingMediaStreamRef_GetI420Size_m4ABCC7E0792108A3C1872E72859C6E73447FA43D,
	PollingMediaStreamRef_GetI420Ptr_m9B73D34A3B081747A7C22AAA0758B82678F1B54B,
	PollingMediaStreamRef_HasFrame_m8ACDB4BEC0BB22E647D559C102A19D157089B8AE,
	PollingMediaStreamRef_FreeCurrentImage_mBFD0B06395E578008C63EA7D0938E81BCC5658F3,
	PollingMediaStreamRef_GetWidth_m4798136B83F3EEE77739A4A1C954D14F785CC84D,
	PollingMediaStreamRef_GetHeight_mF0F05A6E05BC1B460EDC8F130AEF337F959FCC36,
	PollingMediaStreamRef_GetRotation_m92673094551575B73D46D9580B30D980F6C62A0A,
	PollingMediaStreamRef_SetVolume_m0C920071A92B14F98BC3AA3552D3FFB96352CE10,
	PollingPeerRef__ctor_mC7766F24A69E69FD2F5C5C9FCC97824511DC0FB2,
	PollingPeerRef_Finalize_mBB6B084BC8EF2E8EDEA1322FE85DCA80BEA6D219,
	PollingPeerRef_Dispose_m8807DD8D3F913C78484B653C830A6D6A4A2AEF42,
	PollingPeerRef_Update_mE53DF570792F3E80C906B2B2AE5AFD08262E5DE3,
	PollingPeerRef_GetConnectionState_mEB401B212252155F424BCA22C47D4DC90207EA65,
	PollingPeerRef_HasSignalingMessage_m0187B46B014828C1CA28E95B8661C7B093374E85,
	PollingPeerRef_DequeueSignalingMessage_mEC154DE5D8ADC5EDF0D1D35C93EC554C9C2DB6EC,
	PollingPeerRef_AddSignalingMessage_mC9F046F95A9DC7A7F0E4EB63C06DCDC39AB08D5B,
	PollingPeerRef_CreateOffer_m8334BB80CE03177A758E71CD02E00D64A3708BCF,
	PollingPeerRef_Close_m4D16C66A1BF377A9C8979BEE5CC569BF78846F9F,
	PollingPeerRef_Cleanup_m108F2D01929951E0CE4C85ED576D283DE8998965,
	PollingPeerRef_CountOpenedDataChannels_m998C2D6094BA5DFAAC13F90823FD2602B3A34F65,
	PollingPeerRef_DequeueDataChannelMessage_m528F953AC6AE1107FDD41FB86D3FBAEED8DB02F9,
	PollingPeerRef_Send_m2700BD6C653595AE9AE42835706FF2AB79605C90,
	PollingPeerRef_CreateDataChannel_m52BE3F5236CDDBA23B3C97B5EEAEFB542EE0CBC3,
	PollingPeerRef_GetDataChannelLabel_m298A4A622BBF5409FBA6CBBEE9DCD663F2B91F32,
	PollingPeerRef_GetRemoteStream_mBDE8B144F7B7A12C7BB9820D0CA1F0A851D0BC28,
	PollingPeerRef_AddLocalStream_m4FC018E8189963797935FCFEEE8205EB7CBFE42B,
	PollingPeerRef_RemoveLocalStream_m15FDD700A19F8F5D06C7E30FDB3057CC4BBCFFE3,
	RTCPeerConnectionFactory_Create_m7713D71742494AC619F1AA0C5878C9BA749CA136,
	RTCPeerConnectionFactoryRef__ctor_mE890B8A436B3A79AF60D003F05FFB729ACC797E5,
	RTCPeerConnectionFactoryRef_Finalize_mFDED3319E5BDB3753EA5DC80FF18874A3E5BDED1,
	RTCPeerConnectionFactoryRef_Dispose_m22B70E7F21A3D72CC018EFD9460D52C9DDE49E45,
	RTCPeerConnectionFactoryRef__ctor_mFE4794D4A39131CAEB3CEACD31BFAA4F6CEBC4BD,
	RTCPeerConnectionFactoryRef_Initialize_mB5233601E411451FEAA941BC9FEB9B4BBBE8FDEE,
	RTCPeerConnectionFactoryRef_CreatePollingPeer_m266740E00EB6EAF74E989C3D8EDE733250C00A43,
	RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_mAB6CE8C3FE34D82B238373EC61A78DEAD0DF002C,
	RTCPeerConnectionFactoryRef_CreatePollingMediaStream_m7317DF193656ED6E49B1E43C6BFAAFDE520F7147,
	RTCPeerConnectionFactoryRef_GetVideoDevices_m4D20EE383EE861F7D9923531D4F76F8F4B4E42A4,
	RTCPeerConnectionFactoryRef_GetVideoInput_m8B2D7A4B0AAAC7ACB3895F004E538C74D7FE4EF8,
	RefCountInterface__ctor_mB69E6E40CFF44075A19D646CE3A428719F3F277B,
	RefCountInterface_Dispose_mD4EC246D6DBA2801793AB67BC280341EF07286C9,
	StringVector__ctor_mCDCC866578BE55E593E4351AEBC3FD77E4C63312,
	StringVector_Finalize_m118E0E69DFDDC55640AB9BDF13ED413705C12C48,
	StringVector_Dispose_mAB4497D03BD64BD798B31364634B2DBB09B42E88,
	StringVector_get_IsReadOnly_mC04CE3A1DE27BD36333E4B3FDC4C114F3BAAF12E,
	StringVector_get_Item_m8C417BBBBCDCB602C16C80233B585F264FBE62ED,
	StringVector_set_Item_m585723332EB2B5D2C9A2412C1DCC34062D91AB09,
	StringVector_get_Count_m506E911F037B57BB9943BF2C891BB1A7289941EF,
	StringVector_CopyTo_m7A1529D3D4C74B1DB49384BF3AC0023F6EEC64BF,
	StringVector_CopyTo_m49A283845D794743856EEB68E22349250126DE0B,
	StringVector_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mCFE91D5FCB73661D7ABF8015D445434583B6D6F7,
	StringVector_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mEF5379B5BD0CA4833E16440432B697B850B88A6F,
	StringVector_Clear_m0056AF7131E195D983FCF0AE871C67B165756F7F,
	StringVector_Add_mDFB7606314323EEFE9B8DB3BCE23698863A58F6F,
	StringVector_size_m3049013AECD8EDA16641E188DB654EBA11989F9B,
	StringVector_getitemcopy_mE359E5CDC871AC0542A08402CA916CF0FF10D476,
	StringVector_getitem_mAB77F5E1003497A6F9709A4D0E287112552616CA,
	StringVector_setitem_mF3567A4A4F2814C56051D076E54B4A981B7C6C81,
	StringVector_Insert_mA998B82EB1BB5B9BAECBBFAFB994CE855E9B251E,
	StringVector_RemoveAt_m77F55623227192D0946302EDE32DECB03C63C467,
	StringVector_Contains_m99DB800C42DC9B0B8EB664EB6CA2C617C1542372,
	StringVector_IndexOf_mBB91681AA7B3EAAED280203AEB1EABCCE896D3BA,
	StringVector_Remove_mE1549F9D5B8B12A5F92DC8B7AD8363D5E44EE86F,
	StringVectorEnumerator__ctor_mB1D0684EA4C36C95898B1BB4792565C9B0EB0A3C,
	StringVectorEnumerator_get_Current_m224CFCCB93A03512181DBB932B797249FE341C5A,
	StringVectorEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mB3E90D83846547655AB5C4F8B7CE29845C4485EC,
	StringVectorEnumerator_MoveNext_m56B59EAA1A1F5081012E799165389C35A312D6C0,
	StringVectorEnumerator_Reset_m8B6E6A72363631AD54FCD4AAD5AAB61C3324D672,
	StringVectorEnumerator_Dispose_m1A130FBD51EA21CE08A02B22F3DF5CD610B67B35,
	VideoInputRef__ctor_m20BDC4BEDCA2E6D42F02DFFC1F0A279EA972505A,
	VideoInputRef_Finalize_mE15A25A3E062872141F74E258A8B5B2E3DDCABC1,
	VideoInputRef_Dispose_mE589AC6C4A54948CABAF87E45FEA5561177BA7CC,
	VideoInputRef_AddDevice_m036E030CE0619D4F362058D467D5367E35B8F259,
	VideoInputRef_UpdateFrame_m82DC48BB2CBFC6B2644893438C0FD376F8FFB046,
	VideoInputRef_RemoveDevice_m31FDBB3CCABB0AC61D298C4E50049B7CF5CF78C5,
	WebRtcSwigPINVOKE__cctor_mC87FE8A9492A9B56325FAF738C9AAB6A34BB472A,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_0_mCD20D3B24A668D85C9AF736CB33CA947F7942135,
	WebRtcSwigPINVOKE_delete_CopyOnWriteBuffer_m290670244CAB789F08B4E0B3434F48EC5BD639E3,
	WebRtcSwigPINVOKE_DataChannelInit_ordered_set_m65F44BCE5436B14ECDAC67EC6630D607B9AD0D0F,
	WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_set_mA3B5C288EACB8496C36661DB12060E6DCD92B0CB,
	WebRtcSwigPINVOKE_new_DataChannelInit_m01E35C22D5648A4C92533584C25BD11C1996F724,
	WebRtcSwigPINVOKE_delete_DataChannelInit_m87433CFBC17205397752C081BB26DAA3B508FBDA,
	WebRtcSwigPINVOKE_new_DataBuffer__SWIG_0_m1CD4C83825C3A91FCA6312B150A53E17D5BD20D6,
	WebRtcSwigPINVOKE_DataBuffer_size_m7B4DE6F61E24A0DA06814DCB697E68DDC8F16427,
	WebRtcSwigPINVOKE_delete_DataBuffer_m1D01AB3426333384A8D5F245F82008525FC7F8F3,
	WebRtcSwigPINVOKE_StringVector_Clear_mAFBACC220C1D846132D9AB46DAA00138B2D022A1,
	WebRtcSwigPINVOKE_StringVector_Add_m052048414BA779B952E96DA9CA0D4A7A18F65253,
	WebRtcSwigPINVOKE_StringVector_size_m719A27E6DAA3EE975C9E98D2E59A333672ECF51F,
	WebRtcSwigPINVOKE_StringVector_getitemcopy_mEF6BA9C514CB023E2E3E9E1D68E8FBC7283D49EF,
	WebRtcSwigPINVOKE_StringVector_getitem_mFC48945E4C1568C87A2275A04715DC0FF8A853FF,
	WebRtcSwigPINVOKE_StringVector_setitem_m2155B99870B628C10623B96A464BC7A91D567E8D,
	WebRtcSwigPINVOKE_StringVector_Insert_m8DBF40881B458C11C33230DCD470447F03554C42,
	WebRtcSwigPINVOKE_StringVector_RemoveAt_m6E76EA8EB6B7D790618020A85017F860EAF9DEF6,
	WebRtcSwigPINVOKE_StringVector_Contains_m18F673B99725C20982460FC9038ADA1AA0E5B3DD,
	WebRtcSwigPINVOKE_StringVector_IndexOf_mF2C339BCDE2DF642483D73AA83C35B09FD94FC34,
	WebRtcSwigPINVOKE_StringVector_Remove_mD79B4ABC0C3442E9A07EFF6CCA40F992078F3E1E,
	WebRtcSwigPINVOKE_delete_StringVector_m3BBEFE8FF916F605A8199CB337E5F0C6B36548DE,
	WebRtcSwigPINVOKE_IceServers_Add_m51C223B7E54AB2ADA3F2457C2B41F9A8A456D789,
	WebRtcSwigPINVOKE_IceServers_size_m80580DE016DED7010F49BD7BED75703B4F8AC8B1,
	WebRtcSwigPINVOKE_IceServers_getitem_mFFD1E6A7F23195AABD4EEC6551C79574BC7E3D45,
	WebRtcSwigPINVOKE_delete_IceServers_m7D816381DE0B8990D5DA7BCDE14AF5201DFDF5C1,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_0_m3BF3E47C322DFE2138CA391B46F90458C70EB554,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_IceServer_mCEA6989BCA4A3DE9A36F26B0B6ED075369C73750,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_get_m18ABC3FEE0B680AAC5F592F244A10D01F9497AD9,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_set_m9CF2C94EBF3DFB739D3BC14EC80FE4A9896AA545,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_set_mDCE11E71C39A68E08F5C1AE0FF81D78FFFB02C17,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_0_m838ED7F5C0838C1598261980CA096DA76CA02BE1,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCConfiguration_m8D956437DE79EAB6FDA4A30617DB734AEE2CEE0E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kUndefined_get_m03B83FFF228DB385CBD6BA92A4D174F433E953CC,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get_mC83E2885954E25509300251DA1C531668E178E70,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get_mDE2FAE17C517C3A28B5E355C70384A376648D793,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_get_mD43030AFE129AA9394A552860FEB2F9D523CB82D,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_set_mB88857EEBAF0E48068C9FA67EBD25D054E6D618B,
	WebRtcSwigPINVOKE_WebRtcWrap_SetDebugLogLevel_m5EB7F6561F7F1A6315CB5AA03E18E6A43307A33A,
	WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_0_m3B8BBC320079F84F7DCDCF7601A3DACAB468B766,
	WebRtcSwigPINVOKE_WebRtcWrap_GetWebRtcVersion_m2C4930A68C2D63648C5D0D2D82EC69CD3C3AA5FA,
	WebRtcSwigPINVOKE_WebRtcWrap_GetVersion_m29426BCAF2B878F29C308C3349B68FBB7E96D542,
	WebRtcSwigPINVOKE_WebRtcWrap_IsDebugBuild_m33E6C695075C2E4D16F8F566B7E2CEB415210426,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_set_mA13F94546D27EAEA49C43A88EC237CF515274339,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_m338D9F6EDE61BBE6B2EC7CB1B22CCF2D8DEF070A,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_set_mE662CB1CB7D9B59C4238BF097CB23809A90AB85B,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_m8BF7B024013D234299AD613790A78EE7A4C33BEB,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_set_m1A21288FB643A031B8C5E59FB4AA6356C6A7B9ED,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_m0839A3D32A4B6798FD1D322B27158A658FD4DB60,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_set_mCCF1957E73747493F5BE4C1AE867F8F62B1388E7,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_mE15BB08E9CEF19FBEB177009C5506A6980D28006,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_set_mBC520CFDB72839E11FFEF07434EDC268DE2CBF1E,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_mBD7F3DC478D5468692866DC842A2007AEE1A80D5,
	WebRtcSwigPINVOKE_new_AudioOptions_m6A5D545EB1C743B39989F267E34DD38E5C801C6C,
	WebRtcSwigPINVOKE_delete_AudioOptions_m1930E395FF6A9F9CD064871BB9CE0037C38807A5,
	WebRtcSwigPINVOKE_MediaConstraints_audio_set_m1CDC042ABE39986546C3F813D05BE31BA878750A,
	WebRtcSwigPINVOKE_MediaConstraints_video_set_m229494B0CFB29294E5CDE4F9AD2231127D685813,
	WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_set_m1D810FA617F036F3BBE16D3F297450C7A0640963,
	WebRtcSwigPINVOKE_MediaConstraints_idealWidth_set_mDBF2C65866B1D7F382F132980D41B73A8D69E2BF,
	WebRtcSwigPINVOKE_MediaConstraints_idealHeight_set_mE642441FF25150D85ACACB5A9DD8B884F2C19C58,
	WebRtcSwigPINVOKE_MediaConstraints_minWidth_set_m06EAD6E1069042CA81334D6465858B35746065FD,
	WebRtcSwigPINVOKE_MediaConstraints_minHeight_set_m6FEC7466D7F3445B1AEDE10B70B58F5759622501,
	WebRtcSwigPINVOKE_MediaConstraints_maxWidth_set_m94DFAF81BA810C970D0CFD72E25FCA09613734F6,
	WebRtcSwigPINVOKE_MediaConstraints_maxHeight_set_m60B4408F3BD1E91FF3CAEF67D3D8FB6DA3DCCA55,
	WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_set_m1F293365B4905F2C63D6E2A3F9D7C51863326122,
	WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_set_mFF4CE0137EA3A37B2F23FB2DA89068028E8204C1,
	WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_set_m538AE730FC5E54742066D0DE91E64BFF7D4664C1,
	WebRtcSwigPINVOKE_new_MediaConstraints_m6B5C3368975384B4ADB3358A6FE2D3BBA6E10FAE,
	WebRtcSwigPINVOKE_delete_MediaConstraints_mB2018234D45FCFB4B9667777E546648138249F31,
	WebRtcSwigPINVOKE_delete_PollingMediaStreamRef_m538945522373A063101F811D499C2AB0616064DD,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_get_mA74FD95F2DE3E21BD5C865C66CF9F45EE0B7018D,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasVideoTrack_mCF372D139AB108F2E957925F95209153ACA0962C,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasAudioTrack_mBEFD2BF1A06167364A65A355B218B3C95783B4D7,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_IsMute_m1EECFF31E29A43264C673EC56A17A8D2B6C2D221,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SetMute_mB3FA569F8D5BA6C5E642F35B57EAB930C8702965,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_CalculateByteSize_m1A9ADFA8D59A47C7C6869B1BF0F795CCB2F3776C,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetImageData_m1DB7DED25566361FED6CC39758ED92F2608BA12E,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SupportsI420Access_mDEFCAB0B3BDFA4ACF2811FC5AA82E4522F9DB201,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Size_m509585A2891F25E52669BBE03E34CCB0D6AD1C50,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Ptr_mD2E1724D4445C538723AF6FC9E8DC81CD2950088,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasFrame_mDFE7EB63B107B2F428CFD28AA219752F24B132C2,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_FreeCurrentImage_m525CBF0504D29FD73480075D85F521C8D02A060C,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetWidth_m4B66772EFDAE289E299A60AA67D52ADCF3E779CF,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetHeight_m6387210F6F1E68E43E8F2060563233099CA27F92,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetRotation_m49F466DF2709DAF94A3BC6AA26CDF1739EA27C5A,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SetVolume_m56A99BB0DCCCDCB00021649D49A58BB4A863D155,
	WebRtcSwigPINVOKE_delete_PollingMediaStream_mDD21B245C783794D892ABBC115BA89B9C0B081A1,
	WebRtcSwigPINVOKE_delete_PollingPeerRef_m1CE16DC02F2DB2E468D8D33CFC01E0690693BEFA,
	WebRtcSwigPINVOKE_PollingPeerRef_Update_mB0F9F8DBED5247884C349AD55B8BC1726DD6C9C3,
	WebRtcSwigPINVOKE_PollingPeerRef_GetConnectionState_mC85BD7026CC5313029B09499F8F1A63C8E0048A4,
	WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingMessage_m1B74F4C85D8041F1DA8F80EEA9F51FAB9C5FD467,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingMessage_mC2FF9DD72893A67BF3FE29E8A7ED0C35E17B4232,
	WebRtcSwigPINVOKE_PollingPeerRef_AddSignalingMessage_mE9EB5A2F086AE4BC5DB8655033FDE40E8BF3B0C6,
	WebRtcSwigPINVOKE_PollingPeerRef_CreateOffer_m3FEED7FAEDD9424A65F6E6144D0F5B3E1729013F,
	WebRtcSwigPINVOKE_PollingPeerRef_Close_m1DEBE6693E8E0DCB92B19D7664602E949D747543,
	WebRtcSwigPINVOKE_PollingPeerRef_Cleanup_mD8CFA99AFE3756134F75C5BB4A17BFF2AF92CAE9,
	WebRtcSwigPINVOKE_PollingPeerRef_CountOpenedDataChannels_m073D7A584F51DBA28C45594CD52668B986A30A7D,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueDataChannelMessage_mD37E9C5036397C4546E6DA2CF88177290A77AA7B,
	WebRtcSwigPINVOKE_PollingPeerRef_Send_m4B83E1F56272FD7B5776AF9F17B93C913A5B99A6,
	WebRtcSwigPINVOKE_PollingPeerRef_CreateDataChannel_m9A90A69FDF284BE7F6101DFCD8A572A80AF537E7,
	WebRtcSwigPINVOKE_PollingPeerRef_GetDataChannelLabel_m92E2EC81C43DF4DF7BE906AF61D155EB76261C11,
	WebRtcSwigPINVOKE_PollingPeerRef_GetRemoteStream_m31F804F52EFE5FD4B8CC7DDB7B0928A9AD8E36F8,
	WebRtcSwigPINVOKE_PollingPeerRef_AddLocalStream_m06384E01816BF6BFFBB2D098F3807ABD9C940FBA,
	WebRtcSwigPINVOKE_PollingPeerRef_RemoveLocalStream_m1C8E2A0280A727C5ADECB90CAA4EDFA36BC4D7BB,
	WebRtcSwigPINVOKE_delete_VideoInputRef_m2BFD3237E30983386AB19F4C1420C711D64FA376,
	WebRtcSwigPINVOKE_VideoInputRef_AddDevice_mAB87353DA099E2C5CF3FD34B7690746BEA9D3E64,
	WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame_mFC54E0AF6DE9DAB46D18F61B2B5802F0A5643D6D,
	WebRtcSwigPINVOKE_VideoInputRef_RemoveDevice_m2BB2C3F282E0428C104656D237C505DFED6BF85B,
	WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_0_m4603500723C65BD80D3BE19D061827F0BA5D7453,
	WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactoryRef_m27530984B6A1FB7F1E5BB943A90848F92C3628F8,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Initialize_m187F4C5B88196A8027C60A59DA5A74CFC540B14B,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingPeer_mDDBA5B7360FAAD6C7D17176EFE10814006AC390F,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_mFB8E3AB201B8C530201453C15D55A4FA9E55F447,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1_m766C161C75204469BF3F5C251639139D9F536C06,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoDevices_m10000397F20F632A96868B7F4AC16A6579ECCE36,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoInput_m5DBE190A9AECA6D46B1EF25B5453A0212BAE63D4,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Create_m41BBF3FF5D6F145FC0831A1625EC498F7D0F9403,
	WebRtcSwigPINVOKE_IosHelper_InitAudioLayer_mE3DD0EC7C074710BAB91E8FA24A89A4962BB2AA9,
	WebRtcSwigPINVOKE_IosHelper_SetLoudspeakerStatus_mA46D0DFB32996116C47237A2DAE1CBF71D9544FF,
	WebRtcSwigPINVOKE_IosHelper_GetLoudspeakerStatus_mD1EF9BF2B7264E7BF872ADF8486CD721B4402B5F,
	WebRtcSwigPINVOKE_IosHelper_IosKeepAudioActive_m49C2018DC7ACAD5BAC75B837032C89290B87C701,
	WebRtcSwigPINVOKE_PollingMediaStream_SWIGUpcast_m5F5317F463AFABD3D3160CB11D5323095616FBBC,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_WebRtcSwig_m5B304F5D4CA72F6272736536C39D3BA217AC6CB4,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_WebRtcSwig_mCB96017A989D0299B62DBC2DF6C037692C695E21,
	SWIGExceptionHelper_SetPendingApplicationException_m1D732F4D9549A9FB1DA50DA762B2654B20063A73,
	SWIGExceptionHelper_SetPendingArithmeticException_m092E8AEF96772437EFD6AED91D128AFDE4EEADF7,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mB462CAC04A1999435BBDD8139EDEC497180DB9E0,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m32C03AABDBCCC678D6623940786BF9E4E000A0E9,
	SWIGExceptionHelper_SetPendingInvalidCastException_m51618405FC71EC50EF527181A9917F2C1605B3ED,
	SWIGExceptionHelper_SetPendingInvalidOperationException_mC3F7067BE05DE8889D0C3A94EA14D65C9A13F62D,
	SWIGExceptionHelper_SetPendingIOException_mB633286E8207B43FD91C776E8527229CFB658541,
	SWIGExceptionHelper_SetPendingNullReferenceException_mBD9EBECDD2AAB2B6C6C860EB5BBB70493DD8ADD7,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mC7A9B4275612D4E3EF7BFDCA32631F4A37FBCAD7,
	SWIGExceptionHelper_SetPendingOverflowException_m4E33A6A38F46A196B28D7DD57E992F962206D23E,
	SWIGExceptionHelper_SetPendingSystemException_mE812F52691F824C08657FA12198C907BF9B543C9,
	SWIGExceptionHelper_SetPendingArgumentException_m368B5D4C63E86C43CE106603050911E291D5D617,
	SWIGExceptionHelper_SetPendingArgumentNullException_m6DE9AADDDC1F011384305637300C164CE60BCDD6,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mFCA9579E7231D7003A858823EC5C7D30F1C264C8,
	SWIGExceptionHelper__cctor_m5317B1531AD8E6BAFA96FFFC4B0E67CACEDFE25A,
	SWIGExceptionHelper__ctor_mF83A5CBE8504298E4494E84B061180583D0A7DBA,
	ExceptionDelegate__ctor_m51C6FB7039A69CB3F37EA7DBE6AEFF92027BB4E4,
	ExceptionDelegate_Invoke_m26ED00A2E03AA5C41798C060DDBF6B1FD882F296,
	ExceptionDelegate_BeginInvoke_m15C7E8DD6F20E3BD93BC61AF7B295F8B05E6713B,
	ExceptionDelegate_EndInvoke_m57FFB28E01195CD1B138C0FA767BC11044408368,
	ExceptionArgumentDelegate__ctor_m8BF7FAB7DE7EC5C97C93A8CB537E5E74F5878971,
	ExceptionArgumentDelegate_Invoke_m79EA78B66628CF88ACB2A1A9B0F2F36DB78A3C47,
	ExceptionArgumentDelegate_BeginInvoke_m940BAF7AEF0369D0CBFD68A1A5A32A319B5D3AB8,
	ExceptionArgumentDelegate_EndInvoke_m2A7B93A231805EA10102F388F93C22FE7634B7D0,
	SWIGPendingException_get_Pending_mB53F25610102D16A4FA5BE7C2A7EC566FC80ED3D,
	SWIGPendingException_Set_m838596ED86F8019AA8128876A6CE6507A7676796,
	SWIGPendingException_Retrieve_m782926834E50157DB79DB9887CAE80416E3FA039,
	SWIGStringHelper_SWIGRegisterStringCallback_WebRtcSwig_m3892765253EB3BB5E9D7F05BE4412D5BCB0AA9EB,
	SWIGStringHelper_CreateString_mB3D09EFC7B3E365D2CC68129042C971EC28EACA4,
	SWIGStringHelper__cctor_m3B9F8EABD842426183E3A3E2F23FD90F448F6A05,
	SWIGStringHelper__ctor_m20ADD060890875804F29CF4E2D3E60366CFE54ED,
	SWIGStringDelegate__ctor_mBB891E4042D6F16ABF153AAD0EB62BB0B529C571,
	SWIGStringDelegate_Invoke_mF8D32F1CA1DC2E398A4151B0758E447DBE1FACB6,
	SWIGStringDelegate_BeginInvoke_m35098ACD189C98545289BD8B2D0C81F82E77039C,
	SWIGStringDelegate_EndInvoke_m35BA3A8E09FDA2CEE5C79CE913727B159086DAAD,
	WebRtcWrap_SetDebugLogLevel_m837C2654C29A74A57BCA073E1C99B8DA60EE603E,
	WebRtcWrap_Copy_m8901788687FAA782066D162A460DDD957E652556,
	WebRtcWrap_GetWebRtcVersion_m2840B51D9D15040E31803A2EA18DC4A62B95ED3A,
	WebRtcWrap_GetVersion_m83EB4EABA56C7EC7E44337016C3B3B23A8D3FA88,
	WebRtcWrap_IsDebugBuild_mA8A9E502B68A2DD81F814776DFB0EC6B3FEF803F,
	LogCallback__ctor_m9D1F7E15A8B3A2F6B8F2C60A36B1EC09512AEC0E,
	LogCallback_Invoke_m13A80BD61CF98EF15519BF019D1D0682DF838933,
	LogCallback_BeginInvoke_mACA577B965295450DFBC43607F508BB83C73899D,
	LogCallback_EndInvoke_m166F83B8A4244F3129B345665F7E730CA3EDD4E0,
	MonoPInvokeCallbackAttribute__ctor_mF28B4940B8D9384B772AFB289CA8DF2734798FFE,
};
static const int32_t s_InvokerIndices[344] = 
{
	172,
	2266,
	23,
	23,
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	23,
	172,
	2266,
	23,
	23,
	23,
	172,
	2266,
	23,
	23,
	465,
	10,
	172,
	2266,
	23,
	23,
	31,
	32,
	23,
	172,
	23,
	23,
	34,
	10,
	14,
	14,
	26,
	10,
	34,
	26,
	14,
	14,
	89,
	23,
	23,
	3,
	869,
	49,
	869,
	172,
	2266,
	23,
	23,
	31,
	31,
	26,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	23,
	172,
	2266,
	23,
	23,
	23,
	14,
	26,
	26,
	172,
	2266,
	23,
	23,
	23,
	14,
	32,
	3,
	172,
	23,
	23,
	172,
	2266,
	23,
	23,
	14,
	89,
	89,
	89,
	31,
	37,
	2267,
	89,
	10,
	15,
	89,
	23,
	10,
	10,
	10,
	344,
	172,
	23,
	23,
	23,
	10,
	89,
	14,
	26,
	23,
	23,
	23,
	10,
	1507,
	2268,
	27,
	34,
	14,
	26,
	26,
	4,
	172,
	23,
	23,
	23,
	89,
	28,
	14,
	105,
	14,
	14,
	172,
	23,
	172,
	23,
	23,
	89,
	34,
	62,
	10,
	130,
	499,
	14,
	14,
	23,
	26,
	10,
	34,
	34,
	62,
	62,
	32,
	9,
	112,
	9,
	26,
	14,
	14,
	89,
	23,
	23,
	172,
	23,
	23,
	201,
	2269,
	26,
	3,
	783,
	2270,
	2271,
	2272,
	783,
	2270,
	2273,
	2274,
	2270,
	2270,
	2275,
	2274,
	2276,
	2276,
	2277,
	2277,
	2272,
	2278,
	2279,
	2278,
	2270,
	2280,
	2274,
	2281,
	2270,
	783,
	2270,
	2282,
	2275,
	2275,
	783,
	2270,
	106,
	106,
	106,
	2282,
	2272,
	173,
	2283,
	4,
	4,
	49,
	2271,
	2271,
	2271,
	2271,
	2271,
	2271,
	2271,
	2271,
	2271,
	2271,
	783,
	2270,
	2271,
	2271,
	2275,
	2272,
	2272,
	2272,
	2272,
	2272,
	2272,
	2272,
	2272,
	2272,
	783,
	2270,
	2270,
	2282,
	2284,
	2284,
	2284,
	2271,
	2285,
	2286,
	2284,
	2274,
	2282,
	2284,
	2270,
	2274,
	2274,
	2274,
	2287,
	2270,
	2270,
	2270,
	2274,
	2284,
	2288,
	2275,
	2270,
	2270,
	2270,
	2274,
	2289,
	2290,
	2291,
	2276,
	2282,
	2280,
	2280,
	2270,
	2292,
	2293,
	2275,
	783,
	2270,
	2284,
	2294,
	2288,
	2295,
	2282,
	2282,
	783,
	3,
	869,
	49,
	869,
	1018,
	2296,
	195,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	137,
	137,
	137,
	3,
	23,
	124,
	26,
	214,
	26,
	124,
	27,
	125,
	26,
	49,
	163,
	4,
	163,
	0,
	3,
	23,
	124,
	28,
	214,
	28,
	173,
	204,
	4,
	4,
	49,
	124,
	26,
	214,
	26,
	26,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[15] = 
{
	{ 0x0600012C, 8,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m1D732F4D9549A9FB1DA50DA762B2654B20063A73_RuntimeMethod_var, 0 },
	{ 0x0600012D, 9,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m092E8AEF96772437EFD6AED91D128AFDE4EEADF7_RuntimeMethod_var, 0 },
	{ 0x0600012E, 10,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mB462CAC04A1999435BBDD8139EDEC497180DB9E0_RuntimeMethod_var, 0 },
	{ 0x0600012F, 11,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m32C03AABDBCCC678D6623940786BF9E4E000A0E9_RuntimeMethod_var, 0 },
	{ 0x06000130, 12,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m51618405FC71EC50EF527181A9917F2C1605B3ED_RuntimeMethod_var, 0 },
	{ 0x06000131, 13,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_mC3F7067BE05DE8889D0C3A94EA14D65C9A13F62D_RuntimeMethod_var, 0 },
	{ 0x06000132, 14,  (void**)&SWIGExceptionHelper_SetPendingIOException_mB633286E8207B43FD91C776E8527229CFB658541_RuntimeMethod_var, 0 },
	{ 0x06000133, 15,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_mBD9EBECDD2AAB2B6C6C860EB5BBB70493DD8ADD7_RuntimeMethod_var, 0 },
	{ 0x06000134, 16,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mC7A9B4275612D4E3EF7BFDCA32631F4A37FBCAD7_RuntimeMethod_var, 0 },
	{ 0x06000135, 17,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m4E33A6A38F46A196B28D7DD57E992F962206D23E_RuntimeMethod_var, 0 },
	{ 0x06000136, 18,  (void**)&SWIGExceptionHelper_SetPendingSystemException_mE812F52691F824C08657FA12198C907BF9B543C9_RuntimeMethod_var, 0 },
	{ 0x06000137, 19,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m368B5D4C63E86C43CE106603050911E291D5D617_RuntimeMethod_var, 0 },
	{ 0x06000138, 20,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_m6DE9AADDDC1F011384305637300C164CE60BCDD6_RuntimeMethod_var, 0 },
	{ 0x06000139, 21,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mFCA9579E7231D7003A858823EC5C7D30F1C264C8_RuntimeMethod_var, 0 },
	{ 0x06000148, 22,  (void**)&SWIGStringHelper_CreateString_mB3D09EFC7B3E365D2CC68129042C971EC28EACA4_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_WebRtcCSharpCodeGenModule;
const Il2CppCodeGenModule g_WebRtcCSharpCodeGenModule = 
{
	"WebRtcCSharp.dll",
	344,
	s_methodPointers,
	s_InvokerIndices,
	15,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
